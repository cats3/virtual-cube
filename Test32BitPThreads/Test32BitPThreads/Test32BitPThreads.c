﻿#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/stat.h>

const char* semName = "/test";

void* myThreadFunction(void* arg)
{
	sem_t* wait_semaphore = (sem_t*)arg;
	int i = 0;
	for (;;)
	{
		sem_wait(arg);
		printf("%s: i is %d\n", __FUNCTION__, i);
		i++;
	}
}

void* postingThread(void* arg)
{
	sem_t* the_semaphore;
	int i;

	the_semaphore = sem_open(semName, O_CREAT | O_WRONLY, DEFFILEMODE, 0);

	if (the_semaphore == SEM_FAILED)
	{
		perror("sem_init");
		exit(1);
	}

	for (i = 0; i < 10; ++i)
	{
		sleep(1);
		sem_post(the_semaphore);
	}
}

int main()
{
	pthread_t the_postingThread;
	pthread_t myThread;
	sem_t* the_semaphore;
	int ret;

	printf("Hello World 123\n");

	the_semaphore = sem_open(semName, O_CREAT, DEFFILEMODE, 0);
	if (the_semaphore == SEM_FAILED)
	{
		perror("sem_open");
		exit(1);
	}

	ret = pthread_create(&myThread, NULL, &myThreadFunction, the_semaphore);
	if (ret != 0)
	{
		perror("starting Thread");
		exit(1);
	}
	ret = pthread_create(&the_postingThread, NULL, &postingThread, NULL);
	if (ret != 0)
	{
		perror("posting Thread");
		exit(1);
	}

	pthread_join(the_postingThread, NULL);
	sem_close(the_semaphore);
	sem_unlink(semName);

	printf("goodbye!\n");

	return 0;
}
