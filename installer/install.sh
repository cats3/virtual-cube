#!/bin/bash

FILE=vcube_1.0.5.tar
echo "Virtual Cube installer"

apt-get install -y lib32z1 lib32stdc++6

if [ -f "$FILE" ]; then
    echo "Found $FILE ..."
    tar xf $FILE -C$HOME
    mv $HOME/bin/vcube.sh /usr/local/bin
    chmod +x /usr/local/bin/vcube.sh
    mv $HOME/bin/vcubeweb.sh /usr/local/bin
    chmod +x /usr/local/bin/vcubeweb.sh
    mv $HOME/vcube.conf /etc/vcube.conf
    mv $HOME/bin/make-onewire-eeprom /usr/local/bin
    chmod +x /usr/local/bin/make-onewire-eeprom
    mv $HOME/bin/virtual-cube /usr/local/bin
    chmod +x /usr/local/bin/virtual-cube
    mv $HOME/bin/vcube-web /usr/local/bin
    chmod +x /usr/local/bin/vcube-web
    cat $HOME/version.txt
    echo " installation complete"
else
    echo "Cannot find file $FILE"
    echo "installation has failed."
fi
read -p "Press ENTER key..."
