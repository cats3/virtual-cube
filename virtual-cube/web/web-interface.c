#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>

#include <civetweb.h>

#define TEST_WITHOUT_SSL
#undef USE_SSL_DH

#define DOCUMENT_ROOT "./html"

#define PORT "8888"

struct mg_form_data_handler start_cube_data_handler;

int pipelink[2];
pid_t pid;

char vcube_ui_fifo[] = "/tmp/vcubeUI";

int cubeInstances = 0;
int cubePID = 0;

int sv_count = 0;
int tx_count = 0;
char fw_version[8] = "1.4.27";

struct panel_configuration {
	char name[8];
	char value[8];
};

#define MAX_PANELS	2
struct panel_configuration panel_configuration[MAX_PANELS] = {
	{.name = "panel1", .value="1sv2tx", },
	{.name = "panel2", .value="blank", },
};

const char web_cube_config_filename[] = "web_cube.conf";

static void reset_cube_configuration(void)
{
	sv_count = 0;
	tx_count = 0;
}

static void add_tx(int count)
{
	if (count > 0)
		tx_count += count;
}

static void add_sv(int count)
{
	if (count > 0)
		sv_count += count;
}

static void set_panel_value(const char* key, const char* value)
{
	int i;
	for (i = 0; i < MAX_PANELS; i++)
	{
		if (strcmp(panel_configuration[i].name, key) == 0)
		{
			strncpy(panel_configuration[i].value, value, 8);
		}
	}
}

static void configure_panels(FILE* fp)
{
	int i;
	if (fp)
	{
		for (i = 0; i < MAX_PANELS; i++)
		{
			fprintf(fp, "%s=%s\n", panel_configuration[i].name, panel_configuration[i].value);
		}
	}
}

static void configure_cube(void)
{
	int cards_required = (tx_count >> 1) - 1;
	int i;

	if ((sv_count - 1) > cards_required)
		cards_required = (sv_count - 1);
	// write conf file
	FILE* fp = fopen(web_cube_config_filename, "w+");
	if (fp)
	{
		fprintf(fp, "firmware=%s\n", fw_version);
		configure_panels(fp);

		for (i = 1; i < 4; i++)
		{
			fprintf(fp, "slot%d=", i);
			if (cards_required)
			{
				fprintf(fp, "1sv2tx\n");
				cards_required--;
			}
			else
				fprintf(fp, "none\n");
		}
		fflush(fp);
		fclose(fp);
	}
}

static void make_onewire_eeprom(void)
{
	pid_t pid;
	if (cubeInstances == 0)
	{
		pid = fork();
		if (pid == 0)
		{
			execlp("make-onewire-eeprom", "make-onewire-eeprom", web_cube_config_filename, (char*)0);
			perror("make-onewire-eeprom");
		}
	}
}


static void start_cube(void)
{
	if (cubeInstances == 0)
	{
		if (pipe(pipelink) == -1)
		{
			perror("pipe");
			exit(EXIT_FAILURE);
		}

		pid = fork();

		cubeInstances++;
		if (pid == 0)
		{
			dup2(pipelink[1], STDOUT_FILENO);
			close(pipelink[0]);
			close(pipelink[1]);
			execlp("virtual-cube", "virtual-cube", vcube_ui_fifo, web_cube_config_filename, (char*)0);
			perror("start_cube");
		}
		else {
			cubePID = pid;
			close(pipelink[1]);
		}
	}
}

static int inst = 0;
int StartCubeHandler(struct mg_connection* conn, void* cbdata)
{
	if (inst == 0)
	{
		inst++;
		reset_cube_configuration();
		if (mg_handle_form_request(conn, &start_cube_data_handler) > 0)
		{
			configure_cube();
			make_onewire_eeprom();
			start_cube();
			mg_printf(conn,
				"HTTP/1.1 204 No Content\r\nConnection: close\r\n\r\n");
		}
		else
		{
			mg_printf(conn,
				"HTTP/1.1 400 Bad Request\r\nContent-Type: text/html\r\nConnection: "
				"close\r\n\r\n");
			mg_printf(conn, "<html><body>");
			mg_printf(conn, "<h2>Virtual Cube could not be configured</h2>");
			mg_printf(conn, "</body></html>\n");
		}
		inst--;
	}
	else
	{
		mg_printf(conn,
			"HTTP/1.1 400 Bad Request\r\nContent-Type: text/html\r\nConnection: "
			"close\r\n\r\n");
		mg_printf(conn, "<html><body>");
		mg_printf(conn, "<h2>Busy processing another request</h2>");
		mg_printf(conn, "</body></html>\n");
	}
	return 1;
}

int StopCubeHandler(struct mg_connection* conn, void* cbdata)
{
	if (cubeInstances > 0)
	{
		kill(cubePID, SIGTERM);
		cubeInstances--;
	}

	mg_printf(conn,
		"HTTP/1.1 204 No Content\r\nConnection: close\r\n\r\n");

	return 1;
}

int RunningCubesHandler(struct mg_connection* conn, void* cbdata)
{
	mg_printf(conn,
		"HTTP/1.1 200 OK\r\nContent-Type: application/json\r\nConnection: "
		"close\r\n\r\n");
	mg_printf(conn, "{\"runningCubes\":%d}", cubeInstances);
	return 1;
}

int CubeConfigurationHandler(struct mg_connection* conn, void* cbdata)
{
	mg_printf(conn,
		"HTTP/1.1 200 OK\r\nContent-Type: application/json\r\nConnection: "
		"close\r\n\r\n");
	mg_printf(conn, "{\"panels\":[\"%s\",\"%s\"],", panel_configuration[0].value, panel_configuration[1].value);
	mg_printf(conn, "\"swversion\":\"%s\"}", fw_version);
	return 1;
}

int DefaultHandler(struct mg_connection* conn, void* cbdata)
{
	char redirect_path[200];
	const struct mg_request_info* req_info = mg_get_request_info(conn);
	char* path = (char*)req_info->local_uri;
	if (strcmp(path, "/") == 0)
		path = "cube.html";
	snprintf(redirect_path, 198, "./html/%s", path);
	printf("+++++ redirect_path: %s\n", redirect_path);
	mg_send_file(conn, redirect_path);

	return 200;
}

int log_message(const struct mg_connection* conn, const char* message)
{
	puts(message);
	return 1;
}

static int keyIsPanel(const char* key)
{
	return (strncmp(key, "panel", 5) == 0);
}

static int keyIsSwVersion(const char* key)
{
	return (strcmp(key, "swVersion") == 0);
}

static int start_cube_field_found(const char* key, const char* filename, char* path, size_t pathlen, void* user_data)
{
	if ( keyIsPanel(key) || keyIsSwVersion(key) )
		return MG_FORM_FIELD_STORAGE_GET;
	else
		return MG_FORM_FIELD_STORAGE_ABORT;
}

static int start_cube_field_get(const char* key, const char* value, size_t valuelen, void* user_data)
{
	if (keyIsPanel(key))
	{
		set_panel_value(key, value);
		if (strcmp(value, "1sv2tx") == 0)
		{
			add_sv(1); add_tx(2);
		}
		else if (strcmp(value, "1sv4tx") == 0)
		{
			add_sv(1); add_tx(4);
		}
		else if (strcmp(value, "2sv4tx") == 0)
		{
			add_sv(2); add_tx(4);
		}
		return MG_FORM_FIELD_HANDLE_NEXT;
	}
	else if (keyIsSwVersion(key))
	{
		if (valuelen > 8)
			valuelen = 8;
		strncpy(fw_version, value, valuelen);
		return MG_FORM_FIELD_HANDLE_NEXT;
	}
	else
		return MG_FORM_FIELD_HANDLE_ABORT;
}


/********************************************************************************************************************/
/* MAX_WS_CLIENTS defines how many clients can connect to a websocket at the
 * same time. The value 5 is very small and used here only for demonstration;
 * it can be easily tested to connect more than MAX_WS_CLIENTS clients.
 * A real server should use a much higher number, or better use a dynamic list
 * of currently connected websocket clients. */
#define MAX_WS_CLIENTS (5)

struct t_ws_client {
	struct mg_connection* conn;
	int state;
} static ws_clients[MAX_WS_CLIENTS];


#define ASSERT(x)                                                              \
	{                                                                          \
		if (!(x)) {                                                            \
			fprintf(stderr,                                                    \
			        "Assertion failed in line %u\n",                           \
			        (unsigned)__LINE__);                                       \
		}                                                                      \
	}

int WebSocketConnectHandler(const struct mg_connection* conn, void* cbdata)
{
	struct mg_context* ctx = mg_get_context(conn);
	int reject = 1;
	int i;

	mg_lock_context(ctx);
	for (i = 0; i < MAX_WS_CLIENTS; i++) {
		if (ws_clients[i].conn == NULL) {
			ws_clients[i].conn = (struct mg_connection*)conn;
			ws_clients[i].state = 1;
			mg_set_user_connection_data(ws_clients[i].conn,
				(void*)(ws_clients + i));
			reject = 0;
			break;
		}
	}
	mg_unlock_context(ctx);

	fprintf(stdout,
		"Websocket client %s\r\n\r\n",
		(reject ? "rejected" : "accepted"));
	return reject;
}

void WebSocketReadyHandler(struct mg_connection* conn, void* cbdata)
{
	struct t_ws_client* client = mg_get_user_connection_data(conn);

	ASSERT(client->conn == conn);
	ASSERT(client->state == 1);

	client->state = 2;
}

int WebsocketDataHandler(struct mg_connection* conn,
	int bits,
	char* data,
	size_t len,
	void* cbdata)
{
	struct t_ws_client* client = mg_get_user_connection_data(conn);
	ASSERT(client->conn == conn);
	ASSERT(client->state >= 1);

	fprintf(stdout, "Websocket got %lu bytes of ", (unsigned long)len);
	switch (((unsigned char)bits) & 0x0F) {
	case MG_WEBSOCKET_OPCODE_CONTINUATION:
		fprintf(stdout, "continuation");
		break;
	case MG_WEBSOCKET_OPCODE_TEXT:
		fprintf(stdout, "text");
		break;
	case MG_WEBSOCKET_OPCODE_BINARY:
		fprintf(stdout, "binary");
		break;
	case MG_WEBSOCKET_OPCODE_CONNECTION_CLOSE:
		fprintf(stdout, "close");
		break;
	case MG_WEBSOCKET_OPCODE_PING:
		fprintf(stdout, "ping");
		break;
	case MG_WEBSOCKET_OPCODE_PONG:
		fprintf(stdout, "pong");
		break;
	default:
		fprintf(stdout, "unknown(%1xh)", ((unsigned char)bits) & 0x0F);
		break;
	}
	fprintf(stdout, " data:\r\n");
	fwrite(data, len, 1, stdout);
	fprintf(stdout, "\r\n\r\n");

	return 1;
}

void WebSocketCloseHandler(const struct mg_connection* conn, void* cbdata)
{
	struct mg_context* ctx = mg_get_context(conn);
	struct t_ws_client* client = mg_get_user_connection_data(conn);
	ASSERT(client->conn == conn);
	ASSERT(client->state >= 1);

	mg_lock_context(ctx);
	client->state = 0;
	client->conn = NULL;
	mg_unlock_context(ctx);

	fprintf(stdout,
		"Client dropped from the set of webserver connections\r\n\r\n");
}


void update_web_clients(struct mg_context* ctx)
{
	char foo[256];
	int i;

	if (cubeInstances > 0)
	{
		int nbytes = read(pipelink[0], foo, sizeof(foo));
		mg_lock_context(ctx);
		for (i = 0; i < MAX_WS_CLIENTS; i++)
		{
			if (ws_clients[i].state == 2) {
				mg_websocket_write(ws_clients[i].conn, MG_WEBSOCKET_OPCODE_TEXT, foo, nbytes);
			}
		}
		mg_unlock_context(ctx);
	}
}

void* vcube_ui_update(void* args)
{
	int fd = 0;
	char ui_string[1024];
	int bytes;
	struct mg_context* ctx = (struct mg_context*)args;
	int i;

	mkfifo(vcube_ui_fifo, 0666);
	for (;;)
	{
		if (fd == 0)
		{
			fd = open(vcube_ui_fifo, O_RDONLY);
			if (fd < 0)
			{
				perror("vcube_ui_update(open)");
				break;
			}
		}
		bytes = read(fd, ui_string, sizeof(ui_string));
		if (bytes > 0)
		{
			mg_lock_context(ctx);
			for (i = 0; i < MAX_WS_CLIENTS; i++)
			{
				if (ws_clients[i].state == 2) {
					mg_websocket_write(ws_clients[i].conn, MG_WEBSOCKET_OPCODE_TEXT, ui_string, bytes);
				}
			}
			mg_unlock_context(ctx);
		}
		else if (bytes == 0)
		{
			/* EOF or other end of the pipe is closed */
			close(fd);
			fd = 0;
		}
		else
		{
			perror("vcube_ui_update(read)");
			break;
		}
	}
	close(fd);
	return NULL;
}

int main(char** argv, int argc)
{
	pthread_t vcube_ui_thread;

	const char* options[] = {
#if !defined(NO_FILES)
		"document_root",
		DOCUMENT_ROOT,
#endif
		"listening_ports",
		PORT,
		"request_timeout_ms",
		"10000",
		"error_log_file",
		"error.log",
		"enable_auth_domain_check",
		"no",
		0
	};
	struct mg_callbacks callbacks;
	struct mg_context* ctx;
	struct mg_server_port ports[32];
	int port_cnt, n;
	int err = 0;

	/* silently reap children */
	signal(SIGCHLD, SIG_IGN);

	
	/* Start CivetWeb web server */
	callbacks.log_message = log_message;

	ctx = mg_start(NULL, 0, options);

	/* Check return value: */
	if (ctx == NULL) {
		fprintf(stderr, "Cannot start CivetWeb - mg_start failed.\n");
		return EXIT_FAILURE;
	}

	err = pthread_create(&vcube_ui_thread, NULL, &vcube_ui_update, (void*)ctx);
	if (err != 0)
	{
		perror("vcube_ui_thread");
		exit(1);
	}

	start_cube_data_handler.field_found = start_cube_field_found;
	start_cube_data_handler.field_get = start_cube_field_get;
	start_cube_data_handler.field_store = NULL;
	start_cube_data_handler.user_data = NULL;

	mg_set_request_handler(ctx, "/startCube", StartCubeHandler, 0);
	mg_set_request_handler(ctx, "/stopCube", StopCubeHandler, 0);
	mg_set_request_handler(ctx, "/", DefaultHandler, 0);
	mg_set_request_handler(ctx, "/runningCubes", RunningCubesHandler, 0);
	mg_set_request_handler(ctx, "/cubeConfiguration", CubeConfigurationHandler, 0);
	
	/* base websocket */
	mg_set_websocket_handler(ctx,
		"/websocket",
		WebSocketConnectHandler,
		WebSocketReadyHandler,
		WebsocketDataHandler,
		WebSocketCloseHandler,
		0);

	/* List all listening ports */
	memset(ports, 0, sizeof(ports));
	port_cnt = mg_get_server_ports(ctx, 32, ports);
	printf("\n%i listening ports:\n\n", port_cnt);

	/* Wait until the server should be closed */
	while (1) {
		sleep(1);
		//update_web_clients(ctx);
	}

	/* Stop the server */
	mg_stop(ctx);

	return EXIT_SUCCESS;
}