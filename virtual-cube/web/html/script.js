$(document).ready(function () {
    console.log("document ready");
    var $form = $('form');
    $form.submit(function () {
        $.post($(this).attr('action'), $(this).serialize(), function reponse() {
            console.log('form posted');
            getRunningCubes();
        }, 'json');
    });

    //$form = $('stopCubeForm');
    //$form.submit(function () {
    //    $.put($(this).attr('action'), $(this).serialize(), function reponse() {
    //        getRunningCubes();
    //    }, 'json');
    //});

});

function setCubePowerLED(isOn) {
    var cp = document.getElementById("cubePanel");
    var subdoc = getSubDocument(cp);
    if (subdoc) {
        if (isOn) {
            subdoc.getElementById("pwrLED").style.fill = "limegreen";
        }
        else {
            subdoc.getElementById("pwrLED").style.fill = "white";
        }
    }
}

function setCubePowerState(isOn) {
    var e = document.getElementById("cubeOptions");
    if (isOn) {
        e.style.visibility = 'hidden';
        $(".stopbutton").show();
        $(".startbutton").hide();
    }
    else {
        e.style.visibility = 'visible';
        $(".stopbutton").hide();
        $(".startbutton").show();
    }
    setCubePowerLED(isOn);
}

function getCubeConfiguration() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'cubeConfiguration');
    xhr.responseType = 'json';
    xhr.send(null);

    xhr.onload = function () {
        const obj = xhr.response;
        var panels = obj['panels'];
        $("#panel1").val(panels[0]).change();
        change_panel("panel1");
        $("#panel2").val(panels[1]).change();
        change_panel("panel2");
        var swversion = obj['swversion'];
        $(".swVersion").val(swversion).change();
    } 

}

function getRunningCubes() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'runningCubes');
    xhr.responseType = 'json';
    xhr.send(null);

    xhr.onload = function () {
        const obj = xhr.response;
        var foo = obj['runningCubes'];
        console.log('running cubes = ' + foo);
        var isOn = (foo > 0) ? true : false;
        setCubePowerState(isOn);
    } 
}

//function doCubeOperation(op) {
//    console.log(op);
//    var xhr = new XMLHttpRequest();
//    xhr.open('GET', op);
//    xhr.send(null);
//    xhr.onload = function () {
//        getRunningCubes();
//    }
//}

function startCube() {
    console.log('Start Cube');
    $.post('/startCube', $("#cubeconfigform").serialize(), function reponse() {
        console.log('form posted POST');
        getRunningCubes();
    });
}

function stopCube() {
    console.log('Stop Cube');
    $.get('/stopCube', function response() {
        console.log('form posted GET');
        getRunningCubes();
    });
}

function toggleConsole() {
    console.log("toggle cube console");
    var e = document.getElementById("cubeConsole");
    if (e.style.visibility == 'visible')
        e.style.visibility = 'hidden';
    else
        e.style.visibility = 'visible';
}

// fetches the document for the given embedding_element
function getSubDocument(embedding_element) {
    if (embedding_element.contentDocument) {
        return embedding_element.contentDocument;
    }
    else {
        var subdoc = null;
        try {
            subdoc = embedding_element.getSVGDocument();
        } catch (e) { }
        return subdoc;
    }
}

function change_panel(panel) {
    var s = document.getElementById(panel);
    var value = s.options[s.selectedIndex].value;
    var i = document.getElementById(panel + "img");
    i.src = value + ".svg";
}

$(function () {

    var wsproto = (location.protocol === 'https:') ? 'wss:' : 'ws:';
    connection = new WebSocket(wsproto + '//' + window.location.host + '/websocket');
    connection.onmessage = function (e) {
        try {
            var obj = JSON.parse(e.data);
            if (obj.fpled == 2)
                doAnimate();
        }
        catch (error) {
            if (error instanceof SyntaxError) {
                var c = document.getElementById("cubeConsole");
                var str = e.data;
                str = str.replace(/(?:\r\n|\r|\n)/g, '<br />');
                c.innerHTML += str;
                c.scrollTop = c.scrollHeight;
            }
            else {
                throw error;
            }

        }
    };

    connection.onerror = function (error) {
        alert('WebSocket error')
        connection.close();
    };

    var last;
    var s = document.getElementById("flashComLED");

    function update_com_led() {
        fetch('../virtual-cube/hardware.json', { cache: "no-store" })
            .then(response => response.json())
            .then(data => foo(data));
    };

    function doAnimate() {
        var cp = document.getElementById("cubePanel");
        var subdoc = getSubDocument(cp);
        if (subdoc) {
            subdoc.getElementById("flashComLED").beginElement();
        }
    }

    function foo(data) {
        if (last != data.i) {
            doAnimate();
        }
        last = data.i;
    }



    function load() {
        console.log("load");
        var p = document.getElementById("panel1");
        p.addEventListener("change", function () { change_panel("panel1") });
        getCubeConfiguration();
        change_panel("panel1");
        p = document.getElementById("panel2");
        p.addEventListener("change", function () { change_panel("panel2") });
        change_panel("panel2");
    }

    document.onreadystatechange = function () {
        if (document.readyState === 'complete') {
            console.log('really ready');
            getRunningCubes();
        }
    }

    load();
});
