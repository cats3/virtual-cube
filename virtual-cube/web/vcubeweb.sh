#!/bin/sh

cd $HOME

# check for factory reset option
while test $# -gt 0
do
	case "$1" in
		--factory) echo "Factory resetting virtual cube"
			rm *.dat
			;;
	esac
	shift
done

if [ ! -f eeprom_onewire.dat ]; then
	echo "No onewire_eeprom file found, creating..."
	make-onewire-eeprom
fi
vcube-web

