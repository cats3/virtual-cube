﻿#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "make-onewire-eeprom.h"
#include "config.h"

/* Page numbers used within 1-wire EEPROM */
/* cut/paste from eeprom.h from Cube */

#define PAGE_MAC_ADDRESS	0		/* Page 0 used for MAC address					*/
#define PAGE_BOARDSN		1		/* Page 1 used for mainboard serial number		*/
#define PAGE_NETCONFIG		2		/* Page 2 used for network configuration data	*/
#define PAGE_CTRLSN			3		/* Page 3 used for controller serial number		*/
#define PAGE_CTRLNAME		4		/* Page 4 used for controller name string		*/
#define PAGE_PANELCFG		5		/* Page 5 used for rear panel configuration map	*/
#define PAGE_RIGNAME		6		/* Page 6 used for rig name	string				*/

#define PAGE_SIZE			32
#define PAGE_OFFSET(x)		(PAGE_SIZE * x)

#define PANELCFG_PROCESS_FLAG	0x80
#define PANELCFG_SLOTNUM_FLAG	0x40

const char* eepromFilename = "eeprom_onewire.dat";
const size_t eepromSize = 256;

static void write_macaddress(char* mem)
{
	int i;
	mem = mem + (PAGE_SIZE * PAGE_MAC_ADDRESS);

	for (i = 0; i < 6; i++)
	{
		mem[i] = 0;
	}
}

static void write_board_serialnumber(char* mem, const char* serialno)
{
	mem = mem + PAGE_OFFSET(PAGE_BOARDSN);
	strncpy(mem, serialno, 16);
}

static void write_controller_serialnumber(char* mem, const char* serialno)
{
	mem = mem + PAGE_OFFSET(PAGE_CTRLSN);
	strncpy(mem, serialno, 8);
}

static void write_controller_name(char* mem, const char* name)
{
	mem = mem + PAGE_OFFSET(PAGE_CTRLNAME);
	strncpy(mem, name, 31);
}

static void write_rig_name(char* mem, const char* name)
{
	mem = mem + PAGE_OFFSET(PAGE_RIGNAME);
	strncpy(mem, name, 31);
}

//static int get_slot_for_connection(const int conn_id, const int panel, const int last_slot)
//{
//	int ret = 0;
//	if ((conn_id < 1) || (conn_id > 2))
//		return 0;
//	switch (panel)
//	{
//	case Panel1SV2TX:
//		ret = (conn_id == 1) ? 
//		if (conn_id == 1)
//	}
//}

static void write_panel_config(char* mem, const int panel1, const int panel2)
{
	int i = 0;
	int next_slot = 1;

	printf("panel1 = %d, panel2 = %d\n", panel1, panel2);
	mem = mem + PAGE_OFFSET(PAGE_PANELCFG);
	memset(mem, 0, 31);
	/* first panel is always hydraulic on slot 1 (main board) */
	mem[i++] = PanelHydraulic;		
	mem[i++] = PANELCFG_PROCESS_FLAG | PANELCFG_SLOTNUM_FLAG | 0x01;
	
	mem[i++] = panel1;
	switch (panel1)
	{
		case Panel1SV2TX:
			mem[i++] = PANELCFG_PROCESS_FLAG | PANELCFG_SLOTNUM_FLAG | next_slot++;	// slot number 1 = main board
			break;
		case Panel1SV4TX:
		case Panel2SV4TX:
			mem[i++] = PANELCFG_SLOTNUM_FLAG | next_slot++;
			mem[i++] = PANELCFG_PROCESS_FLAG | PANELCFG_SLOTNUM_FLAG | next_slot++;	// slot number 2 = first slot
			break;
		default:
		case BlankPanel:
			mem[i++] = PANELCFG_PROCESS_FLAG | PANELCFG_SLOTNUM_FLAG | 0x0;		// slot number 0 = N/A
			break;
	}
	mem[i++] = panel2;
	switch (panel2)
	{
		case Panel1SV2TX:
			mem[i++] = PANELCFG_PROCESS_FLAG | PANELCFG_SLOTNUM_FLAG | next_slot++;	// slot number 1 = main board
			break;
		case Panel1SV4TX:
		case Panel2SV4TX:
			mem[i++] = PANELCFG_SLOTNUM_FLAG | next_slot++;
			mem[i++] = PANELCFG_PROCESS_FLAG | PANELCFG_SLOTNUM_FLAG | next_slot++;	// slot number 2 = first slot
			break;
		default:
		case BlankPanel:
			mem[i++] = PANELCFG_PROCESS_FLAG | PANELCFG_SLOTNUM_FLAG | 0x0;		// slot number 0 = N/A
			break;
	}
}

int main(int argc, char** argv)
{
	if (argc > 1)
	{
		puts(argv[1]);
		config_read(argv[1]);
	}
	else
		config_read(NULL);

	char* eeprom = malloc(eepromSize);
	memset(eeprom, 0xff, eepromSize);

	write_macaddress(eeprom);
	write_controller_serialnumber(eeprom, "123456");
	write_controller_name(eeprom, "Virtual_Cube");
	write_rig_name(eeprom, "Demo Rig");
	write_panel_config(eeprom, config_get_panel_type(1), config_get_panel_type(2));

	int f = open(eepromFilename, O_CREAT | O_RDWR, 0666);
	if (f < 0)
	{
		perror("open file");
		exit(1);
	}
	
	write(f, eeprom, eepromSize);
	close(f);

	printf("%s file has been created\n", eepromFilename);
	return 0;
}
