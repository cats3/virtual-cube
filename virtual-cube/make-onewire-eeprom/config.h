#ifndef __CONFIG_H
#define __CONFIG_H

#include <stdint.h>

enum {
	BlankPanel = 0,
	PanelHydraulic = 1,
	PanelAIPower = 2,
	Panel1SV2TX,
	Panel1SV4TX,
	Panel2SV4TX,
	Panel2TX,
	Panel4TX,
	Panel2DA4AD,
	PanelDigIO,
	PanelKiNet,
	Panel4AD,
	PanelCANBus,
	PanelDigitalTx,
};

struct configuration {
	uint8_t panel[2];
};

void config_read(const char* conf_filename);
uint8_t config_get_panel_type(const int panel);

#endif
