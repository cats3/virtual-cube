#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>

#include "config.h"

struct configuration panel_configuration =
{
	.panel[0] = Panel1SV2TX,
	.panel[1] = BlankPanel,
};

static void config_set_panel_type(const int panel, const uint8_t type)
{
	if (panel > 0 && panel < 3)
		panel_configuration.panel[panel - 1] = type;
}

static void config_parse_panel(const int panel, const char* value)
{
	printf("config_parse_panel: panel=%d, value=%s\n", panel, value);
	if (strncmp(value, "1sv2tx", 6) == 0)
		config_set_panel_type(panel, Panel1SV2TX);
	else if (strncmp(value, "1sv4tx", 6) == 0)
		config_set_panel_type(panel, Panel1SV4TX);
	else if (strncmp(value, "2sv4tx", 6) == 0)
		config_set_panel_type(panel, Panel2SV4TX);
	else if (strncmp(value, "blank", 5) == 0)
		config_set_panel_type(panel, BlankPanel);
	else
		fprintf(stderr, "config: unknown type for panel %d: %s\n", panel, value);
}

static void config_parse_line(char* line)
{
	char* parameter;
	char* value;

	if (strlen(line) > 0)
	{
		parameter = strtok(line, "=");
		if (parameter)
		{
			value = strtok(NULL, "");
			if (strncmp("panel", parameter, 5) == 0)
				config_parse_panel(atoi(&parameter[5]), value);
			else if (strncmp("slot", parameter, 4) == 0)
				fprintf(stdout, "ignoring 'slot' parameter\n");
			else if (strncmp("firmware", parameter, 8) == 0)
				fprintf(stdout, "ignoring 'firmware' parameter\n");
			else
				fprintf(stderr, "config: unrecognised parameter %s\n", parameter);
		}
	}
}

const char* default_conf_filename = "/etc/vcube.conf";
void config_read(const char* conf_filename)
{
	FILE* f = NULL;
	char* line = NULL;
	size_t len = 0;
	size_t read;

	if (conf_filename == NULL)
		conf_filename = default_conf_filename;

	f = fopen(conf_filename, "r");
	if (f != NULL)
	{
		while ((read = getline(&line, &len, f) != -1))
		{
			if (line[0] == '#' || !isalnum(line[0]))
				continue;	// a comment or blank line
			config_parse_line(line);
		}
		fclose(f);
		if (line)
			free(line);
	}
	else
	{
		fprintf(stderr, "config: could not find configuration file '%s'.  Using defaults.\n", conf_filename);
	}
}

uint8_t config_get_panel_type(const int panel)
{
	if (panel > 0 && panel < 3)
		return panel_configuration.panel[panel - 1];
	else
		return BlankPanel;
}

