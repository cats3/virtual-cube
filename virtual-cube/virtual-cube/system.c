/********************************************************************************
 * MODULE NAME       : system.c													*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : General system configuration routines					*
 ********************************************************************************/

#include <stdint.h>
#include <string.h>

#include "porting.h"

#include "defines.h"
#include "eeprom.h"
#include "variables.h"
#include "hydraulic.h"
#include "system.h"
#include "msgserver.h"

 /* Security level */

static uint32_t security_level = 0;
extern int sysmem;

/********************************************************************************
  FUNCTION NAME   	: system_initialise
  FUNCTION DETAILS  : General system initialisation.
********************************************************************************/

void system_initialise(void)
{
#ifdef CONTROLCUBE
	char defaultname[32] = "ControlCube_VIRTUAL";
#define NAME_PREFIX 12
#endif
#ifdef AICUBE
	char defaultname[32] = "AICube_";
#define NAME_PREFIX 12
#endif
#ifdef SIGNALCUBE
	char defaultname[32] = "SignalCube_";
#define NAME_PREFIX 11
#endif
	uint8_t macaddr[6];

	/* Set default controller name. This is initially set to the prefix string
	   followed by the final six hex nibbles of the MAC address. This will provide
	   a unique name if the user does not define one. The default name will be
	   overwritten by the name restored from the non-volatile memory, if valid.
	*/

	// TODO: get mac address and append to Cube name?
	// 	   or force virtual cube name?
	// read_mac_address(macaddr);
	// printmac(macaddr, defaultname + NAME_PREFIX, 3, NOSEP | ZEROTERM);
	// memcpy(sysname, defaultname, sizeof(sysname));
	strncpy(sysname, defaultname, 31);
	
	/* Read controller name from 1-wire memory */
	dsp_1w_readpage(PAGE_CTRLNAME, (uint8_t*)sysname, 31, CHK_CHKSUM);
}

/********************************************************************************
  FUNCTION NAME   	: system_set_security
  FUNCTION DETAILS  : Set security level.
********************************************************************************/
void system_set_security(uint32_t security)
{
	security_level = security;
}

/********************************************************************************
  FUNCTION NAME   	: system_get_security
  FUNCTION DETAILS  : Get security level.
********************************************************************************/
uint32_t system_get_security(void)
{
	return(security_level);
}

/********************************************************************************
  FUNCTION NAME   	: system_set_semaphore
  FUNCTION DETAILS  : Set semaphore state.
********************************************************************************/
int system_set_semaphore(connection* conn, uint32_t state)
{
	int istate;

	// TODO: disabling interrupts - needed?
	// istate = disable_int();

	/* If the semaphore is in use by another connection, then we cannot modify
	   the state. So return an error.
	*/
	if ((semaphore != NULL) && (semaphore != conn)) {
		// restore_int(istate);
		return(ERROR_SEMAPHORE_DENIED);
	}

	/* Semaphore is either not in use, or is owned by this connection, so we
	   can modify the state.
	*/
	semaphore = (state) ? conn : NULL;
	// TODO: restoring interrupts - needed?
	//restore_int(istate);
	return(NO_ERROR);
}

/********************************************************************************
  FUNCTION NAME   	: system_get_semaphore
  FUNCTION DETAILS  : Get semaphore state.

					  A return value of FALSE indicates that the defined
					  connection does not own the semaphore (it may be free)
********************************************************************************/
uint32_t system_get_semaphore(connection* conn)
{
	if (semaphore == NULL) return(SEMAPHORE_FREE);
	if (semaphore == conn) return(SEMAPHORE_VALID);
	return(SEMAPHORE_INVALID);
}

/********************************************************************************
  FUNCTION NAME   	: system_check_security
  FUNCTION DETAILS  : Check for valid security level.
********************************************************************************/
uint32_t system_check_security(uint32_t required)
{
	return(security_level >= required);
}

#if 1
/********************************************************************************
  FUNCTION NAME   	: system_check_allowed
  FUNCTION DETAILS  : Check if the required conditions are met.

					  In order to generate an allowed state, all the conditions
					  in the must_have list must be valid and none of the
					  conditions in the must_not_have list must be valid.

					  Returns:

					  NO_ERROR if all required conditions are met
					  ERROR	   if one or more conditions are not met

********************************************************************************/
uint32_t system_check_allowed(uint32_t must_have, uint32_t must_not_have)
{
	uint32_t hyd;
	uint32_t state;
	uint32_t positive;
	uint32_t negative;

	hyd = hyd_get_status() & HYD_STATUS_MASK;

	/* Build overall status for checking */

	state = ((hyd > HYD_ACK_1) ? ALLOW_PR_PILOT : 0) |
		((hyd > HYD_ACK_2) ? ALLOW_PR_LOW : 0) |
		((hyd > HYD_ACK_3) ? ALLOW_PR_HIGH : 0);

	positive = ((state & must_have) == must_have);
	negative = ((~state & must_not_have) == must_not_have);

	if (positive && negative) return(NO_ERROR);

	if (state & must_not_have & ALLOW_PR_ANY) return(HYDRAULICS_ACTIVE);

	return(ERROR);
}
#endif

/********************************************************************************
  FUNCTION NAME   	: task_create
  FUNCTION DETAILS  : Wrapper for DSP/BIOS TSK_create function.

					  Creates environment block for the task and initialises
					  its timeslot counter.
********************************************************************************/

//TSK_Handle task_create(Fxn fxn, TSK_Attrs* attrs, connection* c, int timeslots)
//{
//	TSK_Handle t;
//	taskinfo* info;
//
//	t = TSK_create(fxn, attrs, c);
//	info = MEM_alloc(sysmem, sizeof(taskinfo), 4);
//
//	info->timeslots = timeslots;
//	info->ts_ctr = 0;
//
//	/* Allocate the taskinfo structure to the task */
//
//	TSK_setenv(t, info);
//
//	return(t);
//}
