// Limit.cpp - Limit Class module

#include "C3Std.h"

#ifdef SUPGEN

static const Uint32 actionTable[]={
	0,																// Nothing
	TSRStartInh,													// Start inhibit
	TSRWarning,														// Warning
	TSRFreeze,														// Hold
	TSRStopDyn,														// Pause
	TSRStopDyn+TSRStopWR+TSRModeChStop,								// Stop
	TSRStopDyn+TSRStopWR+TSRModeChStop+TSRUnloadWR,					// Stop and unload
	TSRStopDyn+TSRStopStat+TSRModeChStop+TSRDump					// Unload immediately
};

static const Uint32 isSpFreezeTable[]={
	False,															// Nothing
	False,															// Start inhibit
	False,															// Warning
	True,															// Hold
	True,															// Pause
	True,															// Stop
	True,															// Stop and unload
	True															// Unload immediately
};


#ifdef CONTROLCUBE
extern bool SPTrack();
#ifndef PCTEST
extern void Dump();
extern bool Dumped();
extern bool hyd_off();
#endif
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Limit system

LimitSysICntxt LimitSys::IContext;

void LimitSys::operator()(Parameter *par, char *n, SmpLimitSys &smp)
{
	static const char *(AbleStr[])={"Disabled", "Enabled", NULL};
	static const char *(InhStr[])={"Enabled", "Inhibited", NULL};
	static const char *(GenContrStr[])={"Stop_on_dump", "Free_run", NULL};
//	static const char *(XcodeStr[])={"At_baseline", "Static_fade", "At_bias", "Dwelling", "Transitioning", "Cycling", "External_command", "Direct_cyclic_start", "Load_Transition_buffer",
//		"Load_cyclic_buffer", "Load_dwell_buffer", "At_bias_function_complete", "At_bias_baseline_track", "Synchronise"};
	int i;

	smpbs=&smp;
	Parameter::operator()(par, n);
	GlobalLimitEnable(this, "Global_limit_enable", "rdl", Smp(globalLimitEnable), 0, 1, "yellow");
	ParameterLock    (this, "Parameter_lock",      "rl",  NULL,                   0, 1, "yellow");
	ConfigInhibit    (this, "Start_inhibit",       "rl",  NULL,                   0, 1, "yellow");
	SimulationMode   (this, "Simulation_mode",     "py",  NULL, (char **)AbleStr);
	InnerLoopTune    (this, "Inner_loop_tune",     "py",  NULL, (char **)AbleStr);
	FlowLimitSetup   (this, "Flow_lim_on_setup",   "pny", NULL, (char **)AbleStr);
#ifdef PCTEST
	Dumped(this, "Dumped", "", NULL, (char **)AbleStr);
#endif
	Integration(this, "Integration");
	Integration.GenControl(&Integration, "Generator_control", "py", NULL, (char **)GenContrStr);
	Integration.KinetInt(&Integration, "KiNet_Integration");
	Integration.KinetInt.setFlags("g");
	Integration.KinetInt.Enable  (&Integration.KinetInt, "Enable",    "pny", NULL, (char **)AbleStr);
	Integration.KinetInt.TimeSlot(&Integration.KinetInt, "Time_slot", "pny", NULL, 0, C3NTS-1, "", 0);
	Integration.KinetInt.Stop    (&Integration.KinetInt, "Stop",      "rl",  NULL, 0, 1, "red");
	Integration.HandConInt(&Integration, "Hand_Controller_Integration");
	Integration.HandConInt.setFlags("g");
	Integration.HandConInt.Enable     (&Integration.HandConInt, "Enable",       "pny", Smp(handConEn),   (char **)AbleStr);
	Integration.HandConInt.TimeSlot   (&Integration.HandConInt, "Time_slot",    "pny", Smp(handConSlot), 0, C3NTS-1, "", 0);
	Integration.HandConInt.DMSwitch   (&Integration.HandConInt, "DM_switch",    "rl",  NULL, 0, 1, "red");
	Integration.HandConInt.SetupSwitch(&Integration.HandConInt, "Setup_switch", "rl",  NULL, 0, 1, "red");
	TSRIn(this, "TSR_In", smpbs->tsrIn);
	TSROut(this, "TSR_Out", smpbs->tsrOut);
    ChannelInhibits(this, "Channel_Inhibits");
	for(i=0; i<C3AppNChans; i++)
	{
		sprintf(ChannelInhibits.names[i], "%d", i+1);
		ChannelInhibits.Member[i](&ChannelInhibits, ChannelInhibits.names[i], "y", Smp(chanInhibits[i+1]), (char **)InhStr);
	}
	GenbusMonitor(this, "Genbus_Monitor");
	GenbusMonitor.Xcode      (&GenbusMonitor, "xcode",         "dr",  (Uint32 *)(Smp(genbusBuf.xcode)));
	GenbusMonitor.Fade       (&GenbusMonitor, "fade",          "drv", Smp(genbusBuf.fade),         0.0f,   100.0f, "%");
	GenbusMonitor.Delta      (&GenbusMonitor, "delta",         "dr",  Smp(genbusBuf.delta),        0.0f,   400.0f, "Hz");
	GenbusMonitor.Delta.setMap(1.0f/SysFs);
	GenbusMonitor.Theta      (&GenbusMonitor, "theta",         "drv", Smp(genbusBuf.theta),        0.0f,   360.0f, "degrees");
	GenbusMonitor.Span       (&GenbusMonitor, "span",          "drv", Smp(genbusBuf.span),         0.0f,   100.0f, "%");
	GenbusMonitor.SetPointInc(&GenbusMonitor, "set_point_inc", "drv", Smp(genbusBuf.setPointInc), -100.0f, 100.0f, "%");
	pTSRIn=NULL;
	pTSROut=NULL;
	pCnetTsLo=NULL;
	pCnetTsHi=NULL;
	pCnetA=pCnetB=NULL;
	pGenbus=NULL;
	StartInhibited=0;
	SimEn=0;
	ILEn=0;
	WdogState=0;
//	TSRDAVState=0;
}

void LimitSys::parPrimeSignal()
{
	Poke(smpbs->pTSRIn, pTSRIn);
	Poke(smpbs->pTSROut, pTSROut);
	Poke(smpbs->pCnetTsLo, pCnetTsLo);
	Poke(smpbs->pCnetTsHi, pCnetTsHi);
	Poke(smpbs->pCnet, pCnetB);
	Poke(smpbs->pGenbus, pGenbus);
	Poke(smpbs->tsrTPDAVOut, 0);
	Parameter::parPrimeSignal();
}

void LimitSys::parPollSignal()
{
	mopUp();
	Parameter::parPollSignal();
}

/********************************************************************************
  FUNCTION NAME   	: TSRPoll
  FUNCTION DETAILS  : Replacement for the TSR parts of function parPollSignal.
					  This function is called from the 5ms instead of relying on
					  the background parameter polling code and therefore
					  provides faster updates for TSR related flags, in 
					  particular the TSRServoEn flag, which previously could
					  take in excess of 100ms to update leading to unwanted
					  jumps in actuator position when selecting pressure.

					  Note that a simplified parPollSignal is still used to 
					  allow the normal recursive polling to occur for other
					  LimitSys related parameters.

********************************************************************************/

void LimitSys::TSRPoll()
{
	Uint32 tsrinLo, hydassertin, hydassertout;
	Integer i;
#ifdef CONTROLCUBE
	bool dumped;
	bool pressureOff;
#endif

	tsrinLo=Peek(smpbs->tsrInBufLo);
	i=(tsrinLo&TSRParLock)?1:0;
	ParLock=i;
	ParameterLock=i;
	StartInhibited=tsrinLo&TSRStartInh;
	hydassertin=hydassertout=0;
#ifdef CONTROLCUBE
#ifdef PCTEST
	dumped=(Dumped()==1);
	//pressureOff=(hyd_off()==1);
#else
	dumped=Dumped();
	pressureOff=hyd_off();
#endif
#ifndef PCTEST
	if((tsrinLo&TSRDump)||((tsrinLo&(TSRUnloadWR|TSRStatPend))==TSRUnloadWR))
		Dump();
#endif
//	LOG_LED(1, SPTrack()?1:0);
	hydassertin=SPTrack()?0:TSRSmoothEn|TSRIntEn|TSRServoEn|TSRParLock;
	if(!Integration.GenControl())
		hydassertin|=((tsrinLo&TSRStatPend)&&dumped)?TSRStopDyn+TSRStopWR:0;
	if(!dumped)
		hydassertin|=TSRInch;
#endif /* CONTROLCUBE */
	if (WdogState) {
	    hydassertout |= WdogState;
	}
#ifdef AICUBE
	i=0;
	if(Integration.KinetInt.Enable()&&pCnetA)
	{
		i=(int)pCnetA[Integration.KinetInt.TimeSlot()];
		i=(     ((i&(KGSWSim|KGSWStop|KGSWPause)) != KGSWSim)
			&&	((i&(KGSWDump|KGSWStop|KGSWPause|KGSWPilotP|KGSWLowP|KGSWHighP)) != (KGSWPilotP|KGSWLowP|KGSWHighP))
			&&	(tsrinLo&TSRStatPend) );
		if(i)
			hydassertin|=(TSRStopDyn+TSRStopWR);
	}
	Integration.KinetInt.Stop=i;
#endif
	i=(Integer)Peek(smpbs->handConDelta);
	Integration.HandConInt.DMSwitch=((i&SigHCDMBit)?1:0);
	Integration.HandConInt.SetupSwitch=((i&SigHCSetupBit)?1:0);
	if(FlowLimitSetup()&&(tsrinLo&TSRORedSetup))
		hydassertout|=TSRFlowLim;
	i=pConfigOkay?!*pConfigOkay:0;
	ConfigInhibit=(i?1:0);
	if(i)
		hydassertout|=TSRStartInh;
	#ifdef CONTROLCUBE
		if(pressureOff)
			hydassertout|=TSRPressureOff;
	#endif
	Poke(smpbs->hydAssertIn, hydassertin);
	Poke(smpbs->hydAssertOut, hydassertout);
}

#if 0

void LimitSys::parPollSignal()
{
	Uint32 tsrinLo, hydassertin, hydassertout;
	Integer i;
#ifdef CONTROLCUBE
	bool dumped;
	bool pressureOff;
#endif

	tsrinLo=Peek(smpbs->tsrInBufLo);
	i=(tsrinLo&TSRParLock)?1:0;
	ParLock=i;
	ParameterLock=i;
	StartInhibited=tsrinLo&TSRStartInh;
	hydassertin=hydassertout=0;
#ifdef CONTROLCUBE
#ifdef PCTEST
	dumped=(Dumped()==1);
	pressureOff=(hyd_off()==1);
#else
	dumped=Dumped();
	pressureOff=hyd_off();
#endif
#ifndef PCTEST
	if((tsrinLo&TSRDump)||((tsrinLo&(TSRUnloadWR|TSRStatPend))==TSRUnloadWR))
		Dump();
#endif
	hydassertin=SPTrack()?0:TSRSmoothEn|TSRIntEn|TSRServoEn|TSRParLock;
	if(!Integration.GenControl())
		hydassertin|=((tsrinLo&TSRStatPend)&&dumped)?TSRStopDyn+TSRStopWR:0;
	if(!dumped)
		hydassertin|=TSRInch;
#endif /* CONTROLCUBE */
	if (WdogState) {
	    hydassertout |= WdogState;
	}
//	hydassertout |= TSRDAVState;
	i=0;
	if(Integration.KinetInt.Enable()&&pCnetA)
	{
		i=(int)pCnetA[Integration.KinetInt.TimeSlot()];
		i=(     ((i&(KGSWSim|KGSWStop|KGSWPause)) != KGSWSim)
			&&	((i&(KGSWDump|KGSWStop|KGSWPause|KGSWPilotP|KGSWLowP|KGSWHighP)) != (KGSWPilotP|KGSWLowP|KGSWHighP))
			&&	(tsrinLo&TSRStatPend) );
		if(i)
			hydassertin|=(TSRStopDyn+TSRStopWR);
	}
	Integration.KinetInt.Stop=i;
	i=(Integer)Peek(smpbs->handConDelta);
	Integration.HandConInt.DMSwitch=((i&SigHCDMBit)?1:0);
	Integration.HandConInt.SetupSwitch=((i&SigHCSetupBit)?1:0);
	if(FlowLimitSetup()&&(tsrinLo&TSRORedSetup))
		hydassertout|=TSRFlowLim;
	i=pConfigOkay?!*pConfigOkay:0;
	ConfigInhibit=(i?1:0);
	if(i)
		hydassertout|=TSRStartInh;
	#ifdef CONTROLCUBE
		if(pressureOff)
			hydassertout|=TSRPressureOff;
	#endif
	Poke(smpbs->hydAssertIn, hydassertin);
	Poke(smpbs->hydAssertOut, hydassertout);
	mopUp();
	Parameter::parPollSignal();
}

#endif

int LimitSys::GetSetupState()
{
	return (Peek(smpbs->tsrInBufLo)&TSRORedSetup)?1:0;
}

void LimitSys::ForceSetup()
{
	Poke(smpbs->forceSetup, (Sint32)SysFs);
}

void LimitSys::WdogOut(Uint32 out)
{
	WdogState = out;
}

void LimitSys::TSRDAVOut(Uint32 out)
{
	Poke(smpbs->tsrTPDAVOut, (Sint32)out);
//	TSRDAVState = out;
}

Uint32 LimitSys::GetActionFlags(Uint32 action)
{
	if (action <= (sizeof(actionTable) / sizeof(Uint32)))
	{
		return (actionTable[action] | (isSpFreezeTable[action] ? (TSRSpFreezePos | TSRSpFreezeNeg) : 0));
	}
	return(0);
}

Uint32 LimitSys::GetDirectionFreezeFlags(Uint32 dirn)
{
	switch(dirn) 
	{
		case 0: /* Both */
			return(TSRSpFreezePos | TSRSpFreezeNeg);
		case 1: /* Positive */
			return(TSRSpFreezePos);
		case 2: /* Negative */
			return(TSRSpFreezeNeg);
	}
	return(0);
}

void LimitSys::mopUp()
{
	SmpLimit *p;
	Limit *q;
	Float vio;
	Uint32 tslo, tshi;

	if((p=(SmpLimit *)Peek(*((Uint32 *)&smpbs->waiting))) && !Peek(smpbs->nextTrip))
	{
		q=(Limit *)Peek(p->supHandle);
		vio=q->Details.Violation();
		tslo=q->Details.TimeStampLo();
		tshi=q->Details.TimeStampHi();
		Syslog(LogTypeLimitTrip, tslo, tshi, q, *((Uint32 *)&vio));
		Poke(smpbs->nextTrip, 1);
	}
}

void LimitSys::parAlteredEvent(Parameter *sce)
{
	if(sce==&SimulationMode)
		SimEn=SimulationMode();
	if(sce==&InnerLoopTune)
		ILEn=InnerLoopTune();
}

void LSAllTSRClass::operator ()(Parameter *par, char *n, SmpLSAllTSR &smp)
{
	smpbs=&smp;
	Parameter::operator()(par, n);
	/* TSR0 */
	Dump              (this, "Dump");
	StartInhibit      (this, "Start_Inhibit");
	Warning           (this, "Warning");
	Freeze            (this, "Freeze");
	StopDynamic       (this, "Stop_Dynamic");
	StopStatic        (this, "Stop_Static");
	FunctionPending   (this, "Function_Pending");
	DynamicPending    (this, "Dynamic_Pending");
	StaticPending     (this, "Static_Pending");
	SmoothAdjustEnable(this, "Smooth_Enable");
	IntegratorEnable  (this, "Integrator_Enable");
	FlowLimit         (this, "Flow_Limit");
	ServoEnable       (this, "Servo_Enable");
	LimitEnable       (this, "Limit_Enable");
	StopWhenReady     (this, "Stop_When_Ready");
	UnloadWhenReady   (this, "Unload_When_Ready");
	ParameterLock     (this, "Parameter_Lock");
	AcquisitionEnable (this, "Acquisition_Enable");
	AcquisitionPulse  (this, "Acquisition_Pulse");
	Unanimity         (this, "Unanimity");
	ORedSetup         (this, "ORed_Setup");
	ForceSetup        (this, "Force_Setup");
	ModeChStop        (this, "Mode_Change_Stop");
	GenSync           (this, "Generator_Sync");
	Inch              (this, "Inching");
	AFCCheck          (this, "AFC_Check");
	TPDAV             (this, "TPDAV");
	TPACK             (this, "TPACK");
	SpFreezePos       (this, "SpFreezePos");
	SpFreezeNeg       (this, "SpFreezeNeg");
	PressureOff       (this, "Pressure_off");
	/* TSR2 */
	MReady            (this, "MReady");
	MAck              (this, "MAck");
	SNotReady         (this, "SNotReady");
	SNotAck           (this, "SNotAck");
}

const LSAllTSRClass::tsrVectorType LSAllTSRClass::tsrVector[]={
	/* TSR0 */
	&LSAllTSRClass::Dump,
	&LSAllTSRClass::StartInhibit,
	&LSAllTSRClass::Warning,
	&LSAllTSRClass::Freeze,
	&LSAllTSRClass::StopDynamic,
	&LSAllTSRClass::StopStatic,
	&LSAllTSRClass::FunctionPending,
	&LSAllTSRClass::DynamicPending,
	&LSAllTSRClass::StaticPending,
	&LSAllTSRClass::SmoothAdjustEnable,
	&LSAllTSRClass::IntegratorEnable,
	&LSAllTSRClass::FlowLimit,
	&LSAllTSRClass::ServoEnable,
	&LSAllTSRClass::LimitEnable,
	&LSAllTSRClass::StopWhenReady,
	&LSAllTSRClass::UnloadWhenReady,
	&LSAllTSRClass::ParameterLock,
	&LSAllTSRClass::AcquisitionEnable,
	&LSAllTSRClass::AcquisitionPulse,
	&LSAllTSRClass::Unanimity,
	&LSAllTSRClass::ORedSetup,
	&LSAllTSRClass::ForceSetup,
	&LSAllTSRClass::ModeChStop,
	&LSAllTSRClass::GenSync,
	&LSAllTSRClass::Inch,
	&LSAllTSRClass::AFCCheck,
	&LSAllTSRClass::TPDAV,
	&LSAllTSRClass::TPACK,
	&LSAllTSRClass::SpFreezePos,
	&LSAllTSRClass::SpFreezeNeg,
	&LSAllTSRClass::PressureOff,
	NULL,
	/* TSR2 */
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	&LSAllTSRClass::MReady,
	&LSAllTSRClass::MAck,
	&LSAllTSRClass::SNotReady,
	&LSAllTSRClass::SNotAck,
};

void LSAllTSRClass::parPrimeSignal()
{
	Poke(smpbs->andMskLo, 0xffffffff);
	Poke(smpbs->andMskHi, 0xffffffff);
}

void LSAllTSRClass::parPollSignal()
{
	Uint32 smpl, msk, i;

	smpl=Peek(smpbs->stateLo);
	msk=1<<STARTTSR0;
	for(i=STARTTSR0; i<STARTTSR0+NTSR0; i++)
	{
		(this->*tsrVector[i]).State=(smpl&msk)?1:0;
		msk<<=1;
	}

	smpl=Peek(smpbs->stateHi);
	msk=1<<STARTTSR2;
	for(i=32+STARTTSR2; i<32+STARTTSR2+NTSR2; i++)
	{
		(this->*tsrVector[i]).State=(smpl&msk)?1:0;
		msk<<=1;
	}
}

void LSAllTSRClass::parAlteredEvent(Parameter *sce)
{
	Uint32 andMskLo, orMskLo, andMskHi, orMskHi, msk, i;

	andMskLo=0xffffffff;
	orMskLo=0;

	andMskHi=0xffffffff;
	orMskHi=0;

	msk=1<<STARTTSR0;
	for(i=STARTTSR0; i<STARTTSR0+NTSR0; i++)
	{
		switch((this->*tsrVector[i]).Manual())
		{
		case 1:
			orMskLo|=msk;
			break;
		case 2:
			andMskLo&=~msk;
			break;
		}
		msk<<=1;
	}

	msk=1<<STARTTSR2;
	for(i=32+STARTTSR2; i<32+STARTTSR2+NTSR2; i++)
	{
		switch((this->*tsrVector[i]).Manual())
		{
		case 1:
			orMskHi|=msk;
			break;
		case 2:
			andMskHi&=~msk;
			break;
		}
		msk<<=1;
	}

	Poke(smpbs->andMskLo, andMskLo);
	Poke(smpbs->orMskLo, orMskLo);

	Poke(smpbs->andMskHi, andMskHi);
	Poke(smpbs->orMskHi, orMskHi);
}

void LSTSRClass::operator()(Parameter *par, char *n)
{
	static const char *(ManualStr[])={"None", "Assert", "Deassert", NULL};

	Parameter::operator()(par, n);
	this->setFlags("g");
	State (this, "State",  "rdl", NULL, 0, 1, "red");
	Manual(this, "Manual", "y",   NULL, (char **)ManualStr);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Limit stuff

void Limit::operator()(Parameter *par, char *n, SmpLimit &smp, Sint32 *smpPOwnChan)
{
	static const char *(EnableStr[])={"Disabled", "Enabled", NULL};
	static const char *(ActionStr[])={"Nothing", "Start_inhibit", "Warning", "Hold", "Pause",
		"Stop", "Stop_then_unload", "Unload_immediately", NULL};
	static const char *(DirectionStr[])={"Both", "Positive", "Negative","None", NULL};

	smpbs=&smp;
	Parameter::operator()(par, n);
	this->setFlags("g");
	State    (this, "State",     "rdl",  Smp(state),     0, 1, "red");
	Tripped  (this, "Tripped",   "rdl",  Smp(trip),      0, 1, "red");
	Reset    (this, "Reset",     "b",    Smp(reset),     0, 1);
	Details  (this, "Details");
	Details.Violation  (&Details, "Violation",       "rmdv", Smp(violation), MinFloat, MaxFloat);
	Details.Enable     (&Details, "Enable",          "iny",  Smp(enable),    (char **)EnableStr);
	Details.Action     (&Details, "Action",          "incy", NULL,           (char **)ActionStr);
	Details.Direction  (&Details, "Direction",       "incy", NULL,           (char **)DirectionStr);
	Details.TimeStampHi(&Details, "Time_stamp_high", "dr",   Smp(tshi), 0, MaxUns);
	Details.TimeStampLo(&Details, "Time_stamp_low",  "dr",   Smp(tslo), 0, MaxUns);
	pTsrOut=LimitSys::IContext.smpCntxt?((LimitSys::IContext.smpCntxt)->tsrOutBufLo):NULL;
	pSmpLimitSys=LimitSys::IContext.smpCntxt;
	pSmpOwnChan=smpPOwnChan;
}

void Limit::parPrimeSignal()
{
	Parameter::parPrimeSignal();
	Poke(smpbs->pTsrOut, pTsrOut);
	Poke(smpbs->supHandle, (Uint32)this);

	Poke(*((Uint32 *)(&smpbs->owningSys)), (Uint32)pSmpLimitSys);
	Poke(smpbs->pGlobalEnable, pSmpLimitSys?&pSmpLimitSys->globalLimitEnable:NULL);
	Poke(smpbs->pOwnChan, pSmpOwnChan);
	newAction();
}

void Limit::parAlteredEvent(Parameter *sce)
{
	if(sce==&Details.Action)
		newAction();
	Parameter::parAlteredEvent(sce);
}

void Limit::newAction()
{
	Uint32 action = Details.Action();
	Uint32 flags = actionTable[Details.Action()];

	if (isSpFreezeTable[action]) flags |= LimitSys::GetDirectionFreezeFlags(Details.Direction());
	Poke(smpbs->action, flags);
}

#endif

#ifdef SMPGEN

void SmpLimitSys::deQOne()
{
	if(tripQHead)
	{
		tripQHead->queued=0;
		tripQHead=tripQHead->tripQNext;
		if(!tripQHead)
			tripQTail=NULL;
	}
}

#endif
