// Limit.h - Limit Class header

#include "Generator.h"
#include "Events.h"

// KiNet Group status word
#define KGSWDump		0x00000002
#define KGSWStop		0x00000004
#define KGSWPause		0x00000008
#define KGSWPilotP		0x00000100
#define KGSWLowP		0x00000200
#define KGSWHighP		0x00000400
#define KGSWSim			0x00000800

// Hand controller slot
#define	SigHCDMBit		0x80000000
#define	SigHCSetupBit	0x40000000

#define	LSMaxChans		16

//#define CNET_DATASLOT_GENSTAT	229
//#define CNET_DATASLOT_FLAGS	230

class SmpLimit;

class SmpLSAllTSR {
public:
	Uint32 stateLo;
	Uint32 stateHi;
	Uint32 andMskLo;
	Uint32 andMskHi;
	Uint32 orMskLo;
	Uint32 orMskHi;
};

class SmpLimitSys {
public:
	void iterBegin()
	{
		int i;

		tsrInBufLo=tsrIn.stateLo=(pTSRIn?((hydAssertIn|*pTSRIn)&tsrIn.andMskLo):0)|tsrIn.orMskLo;
		HSIn = tsrInBufHi=tsrIn.stateHi=(pTSRIn?((pTSRIn[1])&tsrIn.andMskHi):0)|tsrIn.orMskHi;
		for(i=0; i<=C3AppNChans; i++)
			tsrOutBufLo[i]=0;
		globalLimitEnable=tsrInBufLo&TSRLimitEn;
		if(pCnetTsLo)
			cnetTsLo=*pCnetTsLo;
		if(pCnetTsHi)
			cnetTsHi=*pCnetTsHi;
		if(nextTrip)
		{
			deQOne();
			nextTrip=0;
		}
		waiting=tripQHead;
		if((tsrInBufLo&TSRUnloadWR)&&!(tsrInBufLo&TSRStatPend))
			tsrOutBufLo[0]|=TSRDump;
		#ifndef AICUBE
			handConDelta=(handConEn&&pCnet)?pCnet[handConSlot]:0;
		#endif
		if(pGenbus)
		{
			genbusBuf.xcode=((GenGenBus *)pGenbus)->xcode;
			genbusBuf.fade =((GenGenBus *)pGenbus)->fade;
			genbusBuf.delta=((GenGenBus *)pGenbus)->delta;
			genbusBuf.theta=((GenGenBus *)pGenbus)->theta;
			genbusBuf.span =((GenGenBus *)pGenbus)->span;
			genbusBuf.setPointInc=((GenGenBus *)pGenbus)->setPointInc;
		}
	}
	void iterEnd()
	{
		int i;

		if(forceSetup)
		{
			hydAssertOut|=TSRForceSetup;
			forceSetup--;
		}
		for(i=1; i<=C3AppNChans; i++)
			if(!chanInhibits[i])
				tsrOutBufLo[0]|=tsrOutBufLo[i];
		tsrOut.stateLo=(tsrOutBufLo[0]&tsrOut.andMskLo)|tsrOut.orMskLo|hydAssertOut|tsrTPDAVOut;
		tsrOut.stateHi=(tsrOutBufHi&tsrOut.andMskHi)|tsrOut.orMskHi|HSOut;
#ifdef PCTEST
		if(pTSROut)
		{
			*pTSROut|=tsrOut.stateLo;
			pTSROut[1]|=tsrOut.stateHi;
		}
#else
		if(pTSROut)
		{
			*pTSROut=tsrOut.stateLo;
			pTSROut[1]=tsrOut.stateHi;
		}
#endif
	}
	void deQOne();
// Inputs:
	Uint32 *pTSRIn;
	Uint32 *pTSROut;
	Uint32 *pCnetTsLo;
	Uint32 *pCnetTsHi;
	Uint32 *pCnet;
	Uint32 *pGenbus;
// Supervisory comms:
	Uint32 tsrInBufLo;
	Uint32 tsrInBufHi;
	Uint32 tsrOutBufLo[C3AppNChans+1];
	Uint32 tsrOutBufHi;
	Uint32 manualAnd;
	Uint32 manualOr;
	Uint32 hydAssertIn;
	Uint32 hydAssertOut;
	Uint32 tsrTPDAVOut;
	Uint32 HSOut;
	Uint32 HSIn;
	Sint32 globalLimitEnable;
	Sint32 nextTrip;
	SmpLimit *waiting;
	Uint32 cnetTsLo, cnetTsHi;
	SmpLimit *tripQHead, *tripQTail;
	SmpLSAllTSR tsrIn;
	SmpLSAllTSR tsrOut;
	Sint32 handConEn;
	Uint32 handConSlot;
	Uint32 handConDelta;
	Sint32 forceSetup;
	Sint32 chanInhibits[C3AppNChans+1];
	GenGenBus genbusBuf;
};

class SmpLimit {
public:
	void iter()
	{
		Sint32 en;
		register SmpLimitSys *p;

		en=enable;
		if(pGlobalEnable)
			en=en&&(*pGlobalEnable);
		if(reset||(!en))
		{
			reset=trip=0;
			violation=0.0f;
		}
		if(en&&state)
		{
			if((!trip)&&(!queued))
			{
				if(p=owningSys)
				{
					tslo=p->cnetTsLo;
					tshi=p->cnetTsHi;
					tripQNext=NULL;
					if(p->tripQTail)
						p->tripQTail->tripQNext=this;
					p->tripQTail=this;
					if(!p->tripQHead)
						p->tripQHead=this;
				}
				queued=1;
			}
			trip=1;
		}
//		if(trip&&pTsrOut)
//			if(pOwnChan)
//				(pTsrOut[*pOwnChan])|=action;
//			else
		if (trip&&pTsrOut)
			(pTsrOut[0])|=action;
	}
	void update(Uint32 newState, float vio)
	{
		state=newState;
		if(state&&(fabs(vio)>fabs(violation)))
			violation=vio;
	}
	Uint32 *pTsrOut;
	Sint32 *pGlobalEnable;
	Sint32 state;
	Sint32 trip;
	Sint32 queued;
	Sint32 reset;
	float  violation;
	Sint32 enable;
	Uint32 action;
	Uint32 tslo, tshi;
	SmpLimit *tripQNext;
	Uint32 supHandle;
	SmpLimitSys *owningSys;
	Sint32 *pOwnChan;
};

#ifdef SUPGEN

class LSTSRClass: public Parameter {
public:
	void operator()(Parameter *par, char *n);
	IntegerPar	State;
	EnumPar		Manual;
};

class LSAllTSRClass: public Parameter {
public:
	/* TSR0 */
	LSTSRClass Dump;
	LSTSRClass StartInhibit;
	LSTSRClass Warning;
	LSTSRClass Freeze;
	LSTSRClass StopDynamic;
	LSTSRClass StopStatic;
	LSTSRClass FunctionPending;
	LSTSRClass DynamicPending;
	LSTSRClass StaticPending;
	LSTSRClass SmoothAdjustEnable;
	LSTSRClass IntegratorEnable;
	LSTSRClass FlowLimit;
	LSTSRClass ServoEnable;
	LSTSRClass LimitEnable;
	LSTSRClass StopWhenReady;
	LSTSRClass UnloadWhenReady;
	LSTSRClass ParameterLock;
	LSTSRClass AcquisitionEnable;
	LSTSRClass AcquisitionPulse;
	LSTSRClass Unanimity;
	LSTSRClass ORedSetup;
	LSTSRClass ForceSetup;
	LSTSRClass ModeChStop;
	LSTSRClass GenSync;
	LSTSRClass Inch;
	LSTSRClass AFCCheck;
	LSTSRClass TPDAV;
	LSTSRClass TPACK;
	LSTSRClass SpFreezePos;
	LSTSRClass SpFreezeNeg;
	LSTSRClass PressureOff;
	/* TSR2 */
	LSTSRClass MReady;
	LSTSRClass MAck;
	LSTSRClass SNotReady;
	LSTSRClass SNotAck;
	void operator()(Parameter *par, char *n, SmpLSAllTSR &smp);
private:
	virtual void parPrimeSignal();
	virtual void parPollSignal();
	virtual void parAlteredEvent(Parameter *sce);
	typedef LSTSRClass LSAllTSRClass::*tsrVectorType;
	static const tsrVectorType tsrVector[];
	Uint32 *pStateLo, *pAndLo, *pOrLo;
	Uint32 *pStateHi, *pAndHi, *pOrHi;
	SmpLSAllTSR *smpbs;
};

class LSKinetIntClass: public Parameter {
public:
	EnumPar     Enable;
	UnsignedPar	TimeSlot;
	IntegerPar	Stop;
};

class LSHandConIntClass: public Parameter {
public:
	EnumPar     Enable;
	UnsignedPar	TimeSlot;
	IntegerPar  DMSwitch;
	IntegerPar  SetupSwitch;
};

class LSIntClass: public Parameter {
public:
	EnumPar           GenControl;
	LSKinetIntClass   KinetInt;
	LSHandConIntClass HandConInt;
};

class LSGenBusMonClass: public Parameter {
public:
	UnsignedPar	Xcode;
	FloatPar Fade;
	FloatPar Delta;
	FloatPar Theta;
	FloatPar Span;
	FloatPar SetPointInc;
};

class LSChanInhClass: public Parameter {
public:
	EnumPar Member[LSMaxChans];
	char    names[LSMaxChans][4];
};

class LimitSys;
typedef struct {
	LimitSys    *supCntxt;
	SmpLimitSys *smpCntxt;
} LimitSysICntxt;

class LimitSys: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpLimitSys &smp);
	static           LimitSysICntxt IContext;
	IntegerPar		 GlobalLimitEnable;
	IntegerPar		 ParameterLock;
	IntegerPar		 ConfigInhibit;
	EnumPar			 SimulationMode;
	EnumPar			 InnerLoopTune;
	EnumPar			 FlowLimitSetup;
#ifdef PCTEST
	EnumPar Dumped;
#endif
	LSIntClass       Integration;
	LSAllTSRClass    TSRIn;
	LSAllTSRClass    TSROut;
	LSChanInhClass   ChannelInhibits;
	LSGenBusMonClass GenbusMonitor;
	Uint32           *pTSRIn;
	Uint32			 *pTSROut;
	Uint32			 *pCnetTsLo;
	Uint32			 *pCnetTsHi;
	Uint32			 *pCnetA, *pCnetB;
	Uint32           *pGenbus;
	int				 *pConfigOkay;
	Uint32			 StartInhibited;
	Integer			 ParLock;
	Integer          SimEn;
	Integer          ILEn;
	Uint32			 WdogState;
//	Uint32			 TSRDAVState;
	int GetSetupState();
	void ForceSetup();
	void WdogOut(Uint32 out);
	void TSRDAVOut(Uint32 out);
	Uint32 GetActionFlags(Uint32 action);
	static Uint32 GetDirectionFreezeFlags(Uint32 dirn);
	virtual void parAlteredEvent(Parameter *sce);
	void TSRPoll();
private:
	virtual void parPrimeSignal();
	virtual void parPollSignal();
	SmpLimitSys *smpbs;
	void mopUp();
};

class LimitDetails: public Parameter {
public:
	EnumPar			Enable;
	EnumPar			Action;
	EnumPar			Direction;
	FloatPar		Violation;
	UnsignedPar		TimeStampHi;
	UnsignedPar		TimeStampLo;
};

class Limit: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpLimit &smp, Sint32 *smpPOwnChan=NULL);
	IntegerPar		State;
	IntegerPar		Tripped;
	IntegerPar		Reset;
	LimitDetails	Details;
private:
	virtual void parAlteredEvent(Parameter *sce);
	virtual void parPrimeSignal();
	void newAction();
//	static const Uint32 actionTable[];
	SmpLimit *smpbs;
	Uint32 *pTsrOut;
	SmpLimitSys *pSmpLimitSys;
	Sint32 *pSmpOwnChan;
};

#endif
