// SignalCube.h - Signal Cube application header

#define	C3TransPathSz			256
#define	C3NormalisationBlock	100

#ifndef PCTEST
extern "C" {
extern volatile Uint32 tracking_request;
}
#endif

class SmpC3Channel {
public:
	SmpGenerator  FunctionGenerator;
	SmpErrorPath  Controller;
};

class SmpC3App {
public:
	void init();
	void iter()
	{
		LimitSystem.iterBegin();
		Transducers.iterBegin();
		Transducers.iterChans(0, SigNCh);
		VirtualTransducers.iter(0);
		AdjustmentMechanism.iter();
		LimitSystem.iterEnd();
	}
	void iterA()
	{
		LimitSystem.iterBegin();
		Transducers.iterBegin();
		if(ACnt)
			Transducers.iterChans(0, ACnt);
		VirtualTransducers.iter(0);
	}
	void iterB()
	{

		if(BCnt)
			Transducers.iterChans(ACnt, BCnt);
		AdjustmentMechanism.iter();
		LimitSystem.iterEnd();
	}
	int ACnt, BCnt;
	SmpSignal          Transducers;
	SmpExpressionArray VirtualTransducers;
	SmpMonitor         Monitors;
	SmpAdjustment      AdjustmentMechanism;
	SmpLimitSys        LimitSystem;
	SmpC3Channel       Channel[C3AppNChans];
};

#ifdef SUPGEN

class C3SignalsClass: public Parameter {
public:
	SignalArray	      Transducers;
	ExpressionArray   VirtualTransducers;
	MonitorArray      Monitors;
};

class C3ChannelClass: public Parameter {
public:
	Generator   FunctionGenerator;
	ErrorPath   Controller;
};

class C3ServoChClass: public Parameter {
public:
	C3ChannelClass Member[C3AppNChans];
};

class C3TranslationClass: public Parameter {
public:
	UnsignedPar Handle;
	TextPar     Path;
};

class C3LogClass: public Parameter {
public:
	TextPar    LogLine;
	IntegerPar Reset;
	C3TranslationClass Translation;
};

class C3NormalisationClass: public Parameter {
public:
	IntegerPar Inhibited;
	IntegerPar Start;
	FloatPar   Progress;
	IntegerPar LeafCount;
};

class C3App: public Parameter {
public:
	void setSnvBs(void *p)
	{
		snvBs=(char *)p;
		formatSNV(p);
		snvSz=sizeSNV(0);
		snvEnd=(char *)p+snvSz-1;
	}
	void setDnvBs(void *p)
	{
		dnvBs=(char *)p;
		formatDNV(p);
		dnvSz=sizeDNV(0);
		dnvEnd=(char *)p+dnvSz-1;
	}
	void setSmpBs(Uint32 p)
	{
		smpBs=(char *)p;
		smpSz=sizeof(SmpC3App);
		smpEnd=smpBs+sizeof(SmpC3App)-1;
		smpbs=(SmpC3App *)p;
	}
	bool badSnv(void *p){return (((char *)p)<snvBs)||(((char *)p)>snvEnd);}
	bool badDnv(void *p){return (((char *)p)<dnvBs)||(((char *)p)>dnvEnd);}
	bool badSmp(void *p){return (((char *)p)<smpBs)||(((char *)p)>smpEnd);}
	void init();
	int SafeToStart();
	virtual void parPrimeSignal();
	virtual void parConfigureSignal();
	virtual void parPollSignal();
	C3SignalsClass       Signals;
	Adjustment           AdjustmentFeeder;
	LimitSys             LimitSystem;
	C3ServoChClass       ServoChannel;
	C3NormalisationClass Normalisation;
	C3LogClass           Log;
	int goodConfig, goodExpressions, combinedFlags;
private:
	virtual void parSNVValidateUREvent(Parameter *sce);
	virtual void parSNVValidateOREvent(Parameter *sce);
	virtual void parSNVValidateNANEvent(Parameter *sce);
	virtual void parDNVValidateUREvent(Parameter *sce);
	virtual void parDNVValidateOREvent(Parameter *sce);
	virtual void parDNVValidateNANEvent(Parameter *sce);
	virtual void parSNVModifiedEvent(Parameter *sce);
	virtual void parDNVModifiedEvent(Parameter *sce);
	virtual void parSetROEvent(Parameter *sce);
	virtual void parSetUREvent(Parameter *sce);
	virtual void parSetOREvent(Parameter *sce);
	virtual void parSetNanEvent(Parameter *sce);
	virtual void parSetLockEvent(Parameter *sce);
	virtual void parConfigChangedEvent(Parameter *sce);
	virtual void parSetEvent(Parameter *sce);
	virtual void parIdentifierChangedEvent(Parameter *sce);
	virtual void parCompileEvent(Parameter *sce);
	virtual void parDACChangeEvent(Parameter *sce);
	void doConfigure();
	void countLeafParameters();
	void startNormalisation();
	void doSomeNormalisation();
	SmpC3App  *smpbs;
	char      chName[C3AppNChans][4];
	char      *snvBs, *dnvBs, *snvEnd, *dnvEnd, *smpBs, *smpEnd;
	Uint32	  snvSz, dnvSz, smpSz;
	bool      configMeta;
	int       virtualBs;
	int       nLeaves;
	Parameter *normalisationPoint;
	int       normalisationCount;
};

#endif
