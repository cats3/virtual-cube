// Command.cpp - Command Class module

#include "C3Std.h"

#ifdef SUPGEN

void Command::dumpbranch(Parameter *p, void *vfp, int indent)
{
#ifdef PCTEST
	FILE *fp=(FILE *)vfp;
	Integer i, j, k;
	char *s;

	/* SJE modified */
	while(p && (indent < 2))
	{
		i=indent;
		while(i--)
			fprintf(fp, "    ");
		if(p->getType()=='c')
		{
			fprintf(fp, "%s:\n", p->getName());
			dumpbranch(((Parameter *)p)->getSubList(), fp, indent+1);
		}
		else
		{
			current=p;
			displayType(current, True);
			fprintf(fp, "%-32s%s\n", p->getName(), rspbuf);
			rspcnt=0;
			if(p->getType()=='e')
			{
				i=0;
				p->getMin(&k, sizeof(k), 0);
				while(s=(*current)[i])
				{
					j=indent+1;
					while(j--)
						fprintf(fp, "    ");
					fprintf(fp, "%d:%s\n", i+k, s);
					i++;
				}
			}
		}
		p=p->getNext();
	}
#endif
}

void Command::dump(char *fn)
{
#ifdef PCTEST
	FILE *fp;

	if(!(fp=fopen(fn, "w")))
		return;
	fprintf(fp, "C3 Virtual Structure Dump\n\n");
	rspcnt=0;
	dumpbranch(path[0], fp, 0);
	fclose(fp);
	current=path[pathcnt-1];
	rspcnt=0;
#endif
	rspEol();
}

void Command::rspPrintf(char *fmt, ...)
{
	va_list ap;
	char *p;

	va_start(ap, fmt);
	if(rspcnt<Hiwater)
	{
		p=rspbuf+rspcnt;
		rspcnt+=vsprintf(p, fmt, ap);
		if(rspcnt>=Hiwater)
		{
			p=rspbuf+rspcnt;
			rspcnt+=sprintf(p, "...%s", eorsp);
		}
	}
	va_end(ap);
}

void Command::help()
{
	if(!lngprmpt)
		displayPath();
	else
		rspPrintf("%s", current->getName());
	rspPrintf(":");
	displayType(current);
	displayValue(current);
	if((*current)[0])
	{
		switch(current->getType())
		{
		case 'c':
			if(isdigit((*current)[0][0]))
			{
				rspEol();
				break;
			}
		default:
			rspPrintf(":%s", eorsp);
			list();
			break;
		}
	}
	else
		rspEol();
}

void Command::list()
{
	char *s;
	Parameter *p;
	Integer i, j;

	if(current->getType()=='c')
	{
		p=current->getSubList();
		while(p)
		{
			s=p->getName();
			if(isdigit(s[0]))
				rspPrintf("    [%s]:", s);
			else
				rspPrintf("    .%s:", s);
			displayType(p);
			displayValue(p);
			rspPrintf("%s", eorsp);
			p=p->getNext();
		}
	}
	if(current->getType()=='e')
	{
		current->getMin(&j, sizeof(j), 0);
		i=0;
		while(s=(*current)[i])
		{
			rspPrintf("    %d:%s%s", i+j, s, eorsp);
			i++;
		}
	}
}

bool Command::developPath(char *t)
{
	Parameter *p, *q;
	Result r;

	if(!*t)
		return True;
	if(assgnbl)
	{
		rspPrintf("Terminal path%s", eorsp);
		return False;
	}
	if(pathcnt>=Pathsz)
	{
		rspPrintf("Path too long%s", eorsp);
		return False;
	}
	p=current;
	r=p->setValue(t, strlen(t)+1, 0);
	if(r!=ParOkay)
	{
		rspPrintf("%s%s", Parameter::GetResultStr(r), eorsp);
		return False;
	}
	p->getValue(&q, sizeof(q));
	if(!q)
	{
		rspPrintf("Member not found%s", eorsp);
		return False;
	}
	path[pathcnt++]=q;
	checkAssignable();
	return True;
}

void Command::processCommand()
{
	char *p, *q;

	p=cmdbuf;
	while(q=strtok(p, assgnbl?"\t\n\r ":".;[]\t\n\r "))
	{
		switch(q[0])
		{
		case '/':
			pathcnt=1;
			checkAssignable();
			if(!developPath(q+1))
				return;
			break;
		case '=':
			if(!assignment(q+1))
				return;
			if(pathcnt>1)
			{
				pathcnt--;
				checkAssignable();
			}
			break;
		case ':':
			if(pathcnt>1)
			{
				pathcnt--;
				checkAssignable();
				if(!developPath(q+1))
					return;
			}
			break;
		case '?':
			help();
			break;
		case '#':
			dump("dump.txt");
			break;
		default:
			if(!developPath(q))
				return;
		}
		p=NULL;
	}
//	if(assgnbl&&(pathcnt>1))
//	{
//		pathcnt--;
//		checkAssignable();
//	}
}

void Command::displayPath()
{
	int i;
	char *s;

	rspPrintf("/");
	for(i=1; i<pathcnt; i++)
	{
		s=(path[i]->getName());
		if(isdigit(s[0]))
			rspPrintf("[%s]", s);
		else
			rspPrintf(".%s", s);
	}
}

bool Command::assignment(char *t)
{
	Result r;
	Integer i, j;
	Unsigned u;
	Float f;
	char *s;

	switch(current->getType())
	{
	case 'i':
		if(!sscanf(t, "%d", &i))
		{
			rspPrintf("Integer expected%s", eorsp);
			return False;
		}
		r=current->setValue(&i, sizeof(i), 0);
		break;
	case 'u':
		if(!sscanf(t, "%u", &u))
		{
			rspPrintf("Unsigned expected%s", eorsp);
			return False;
		}
		r=current->setValue(&u, sizeof(u), 0);
		break;

	case'f':
		if(!sscanf(t, "%f", &f))
		{
			rspPrintf("Float expected%s", eorsp);
			return False;
		}
		r=current->setValue(&f, sizeof(f), 0);
		break;
	case't':
		r=current->setValue(t, strlen(t)+1, 0);
		break;
	case 'e':
		if(sscanf(t, "%d", &i))
			r=current->setValue(&i, sizeof(i), 0);
		else
		{
			i=0;
			while((s=(*current)[i])&&strcmp(s, t))
				i++;
			if(s)
			{
				current->getMin(&j, sizeof(j), 0);
				i+=j;
				r=current->setValue(&i, sizeof(i), 0);
			}
			else
			{
				rspPrintf("%sUnknown option - %s%s", eorsp, t, eorsp);
				return False;
			}
		}
		break;
	default:
		rspPrintf("%sUnknown type%s", eorsp, eorsp);
		return False;
	}
	rspPrintf("%s%s", Parameter::GetResultStr(r), eorsp);
	return r==ParOkay;
}

void Command::displayValue(Parameter *p)
{
	Integer i, j;
	Unsigned u;
	Float f;
	int s;
	char txtbuf[80];

	switch(p->getType())
	{
	case 'i':
		p->getValue(&i, sizeof(i));
		if(i==MinInt)
			rspPrintf("=Min");
		else if(i==MaxInt)
			rspPrintf("=Max");
		else
			rspPrintf("=%d", i);
		break;
	case 'u':
		p->getValue(&u, sizeof(u));
		if(u==MaxUns)
			rspPrintf("=Max");
		else
			rspPrintf("=%u", u);
		break;
	case'f':
		p->getValue(&f, sizeof(f));
		if(f==MinFloat)
			rspPrintf("=Min");
		else if(f==MaxFloat)
			rspPrintf("=Max");
		else
			rspPrintf("=%g", f);
		break;
	case't':
		p->getMax(&s, sizeof(s));
		if(s>80)
		{
			rspPrintf("=Can't display value");
			break;
		}
		p->getValue(txtbuf, sizeof(txtbuf));
		txtbuf[79]=0;
		rspPrintf("=%s", txtbuf);
		break;
	case 'e':
		p->getValue(&i, sizeof(i));
		p->getMin(&j, sizeof(j), 0);
		rspPrintf("=%d:%s", i, (*p)[i-j]);
		break;
	}
}

void Command::displayType(Parameter *p, bool showM)
{
	Integer i, j;
	Unsigned u, v;
	Float x, y;
	int s;

	if((strchr(p->getFlags(), 'm')==NULL)
		&&(strchr(p->getFlags(), 'k')==NULL))
		showM=False;
	switch(p->getType())
	{
	case 'c':
		if((*p)[0])
		{
			s=0;
			while((*p)[s])
				s++;
//			if(isdigit((*p)[0][0]))
				rspPrintf("[%s..%s]", (*p)[0], (*p)[s-1]);
//			else
//				rspPrintf("[.%s...%s]", (*p)[0], (*p)[s-1]);
		}
		else
			rspPrintf("[]");
		break;
	case 'i':
		p->getMin(&i, sizeof(i), 0);
		p->getMax(&j, sizeof(j));
		if(showM)
		{
			rspPrintf("Integer{%s}[Var]", p->getFlags());
		}
		else
		{
			if(i==MinInt)
				rspPrintf("Integer{%s}[Min..", p->getFlags());
			else
				rspPrintf("Integer{%s}[%d..", p->getFlags(), i);
			if(j==MaxInt)
				rspPrintf("Max]");
			else
				rspPrintf("%d]", j);
		}
		break;
	case 'u':
		p->getMin(&u, sizeof(u), 0);
		p->getMax(&v, sizeof(v));
		if(showM)
			rspPrintf("Unsigned{%s}[Var]", p->getFlags());
		else
		{
			rspPrintf("Unsigned{%s}[%u..", p->getFlags(), u);
			if(v==MaxUns)
				rspPrintf("Max]");
			else
				rspPrintf("%u]", v);
		}
		break;
	case'f':
		p->getMin(&x, sizeof(x), 0);
		p->getMax(&y, sizeof(y));
		if(showM)
			rspPrintf("Float{%s}[Var]", p->getFlags());
		else
		{
			if(x==MinFloat)
				rspPrintf("Float{%s}[Min..", p->getFlags());
			else
				rspPrintf("Float{%s}[%g..", p->getFlags(), x);
			if(y==MaxFloat)
				rspPrintf("Max]");
			else
				rspPrintf("%g]", y);
		}
		break;
	case't':
		p->getMax(&s, sizeof(s));
		rspPrintf("Text{%s}[%d]", p->getFlags(), s);
		break;
	case 'e':
		p->getMin(&i, sizeof(i), 0);
		p->getMax(&j, sizeof(j));
		rspPrintf("Enum{%s}[%d..%d]", p->getFlags(), i, j);
//		rspPrintf("Enum{%s}[%d:%s..%d:%s]", p->getFlags(),
//			i, (*p)[0], j, (*p)[j-i]);
		break;
	default:
		rspPrintf("Unknown type");
	}
}

void Command::issuePrompt()
{
	if(lngprmpt)
	{
		displayPath();
		rspPrintf(":");
		displayType(current);
		displayValue(current);
		rspPrintf(" > ");
	}
	else
		rspPrintf("> ");
}

void Command::checkAssignable()
{
	current=path[pathcnt-1];
	assgnbl=(current->getType()!='c');
}

void Command::init(Parameter *root, char eoc, char *eor, bool lp)
{
	eocmd=eoc;
	eorsp=eor;
	lngprmpt=lp;
	path[0]=root;
	pathcnt=1;
	checkAssignable();
	cmdcnt=0;
	cmdbuf[0]=0;
	rspcnt=0;
	rspbuf[0]=0;
	rspptr=rspbuf;
	rspPrintf("C3 Command V1%s%s", eor, eor);
	issuePrompt();
}

void Command::BSEdit()
{
	char *pin, *pout, c;
	int i;

	i=0;
	pin=pout=cmdbuf;
	while(c=*pin++)
	{
		if(c==8)
		{
			if(i>0)
			{
				pout--;
				i--;
			}
		}
		else
		{
			*pout++=c;
			i++;
		}
	}
	*pout=0;
}

bool Command::cmd(char *txt, int cnt)
{
	char *p;

	rspcnt=0;
	rspbuf[0]=0;
	rspptr=rspbuf;
	if(cnt<1)
		cnt=strlen(txt);
	if(cnt>(Cmdsz-1-cmdcnt))
	{
		rspPrintf("%sCommand too long%s", eorsp, eorsp);
		issuePrompt();
		cmdcnt=0;
		cmdbuf[0]=0;
		return True;
	}
	p=cmdbuf+cmdcnt;
	memcpy(p, txt, cnt);
	p[cnt]=0;
	cmdcnt+=strlen(p);
	if(eocmd&&!(p=strchr(p, eocmd)))
		return False;
	if(eocmd)
		*p=0;
	rspEol();
	BSEdit();
	processCommand();
	issuePrompt();
	cmdcnt=0;
	cmdbuf[0]=0;
	return True;
}

int Command::rsp(char *txt, int cnt)
{
	int sz;

	if(cnt<=0 || !rspcnt)
		return 0;
	sz=cnt;
	if(sz>rspcnt)
		sz=rspcnt;
	memcpy(txt, rspptr, sz);
	rspptr+=sz;
	rspcnt-=sz;
	return sz;
}

char *Command::rspStr()
{
	if(rspcnt)
	{
		rspcnt=0;
		return rspbuf;
	}
	else
		return NULL;
}

#endif
