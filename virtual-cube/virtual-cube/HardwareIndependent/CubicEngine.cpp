// CubicEngine.cpp - Cubic Engine Class module

#include "C3Std.h"

extern "C" {
#ifndef PCTEST
#include "../include/cnet.h"
#include "../include/debug.h"
#endif
Uint32 hpi_read4(Uint32 addr);
void   hpi_write4(Uint32 addr, Uint32 data);
void ctrl_rtctrl_control(Uint32 flags);
void ctrl_rtctrl_pause(Uint32 flags);
void ctrl_rtctrl_resume(void);
}

/* Generic type used for various Cubic Engine arithmetic operations */

typedef union generic {
	Sint32 i;
	Uint32 u;
	float  f;
} generic;

/* Function to check the validity of a channel index */

SigInputChannel *CtrlChkSignal(int c);

#ifdef SUPGEN

CubicEngine *CubicEngine::IContext;

void CubicEngine::operator()(Parameter *par, char *n, CubInstruction *pPrgrm, SmpLimitSys &smp)
{
	static const char *(RunStatusStr[])={"Halt", "Run", NULL};
	static const char *(ExceptionStr[])={"None", "Illegal_opcode", "Illegal_parameter", "Illegal_value", "Stack_overflow", "Nesting_error", "Pause", 
										 "Halt", "Bad_pointer", "Zero_range", NULL};
	static const char *(StackTypeStr[])={"None", "Loop", "Call", "Interrupt", NULL};
	static const char *(ConditionTypeStr[])={"Disabled", "Change", "EQ", "NE", "LT", "GT", NULL};
	static const char *(ResetLoopStateStr[])={"Disabled", "Enabled", NULL};
	int i;

	smpbsLimitSys=&smp;

	Parameter::operator()(par, n);
	RunMode       (this, "Run_mode",         "",    NULL, (char **)RunStatusStr);
	ProgramCounter(this, "Program_counter",  "dn",  NULL, 0, CubPrgrmSz-1);
	ProgramCap    (this, "Program_capacity", "r",   NULL, 0, CubPrgrmSz);
	PauseTag      (this, "Pause_tag",        "ni",  NULL, 0, MaxUns);
	StackPointer  (this, "Stack_pointer",    "dn",  NULL, 0, CubStackSz);
	ResultRegister(this, "Result_Register",  "dn",  NULL, MinInt, MaxInt);
	CompareEpsilon(this, "Compare_Epsilon",  "dn",  NULL, 0.00001, 0.5,"",0.01);
	BaseTimerLow  (this, "Wait_Timer_low",   "r",   NULL, 0, MaxUns);
	BaseTimerHigh (this, "Wait_Timer_high",  "r",   NULL, 0, MaxUns);
	WaitTime      (this, "Wait_Time",        "dn",  NULL, 0, MaxUns);
	CurrentTime   (this, "Current_Time",     "r",   NULL, 0, MaxUns);
	Exception	  (this, "Exception",        "nc",  NULL, (char **)ExceptionStr);
	Reset         (this, "Reset",            "b",   NULL, 0, 1);
	Step          (this, "Step",             "b",   NULL, 0, 1);
	ResetLoopState(this, "Reset_Loop_State", "n",   NULL, (char **)ResetLoopStateStr, 1);
	ConditionTable(this, "Condition_Table");
	IntTable      (this, "Int_Table");
	Stack         (this, "Stack_Frames");
	for(i=0; i<CubStackSz; i++)
	{
		sprintf(Stack.name[i], "%d", i+1);
		Stack.stackFrame[i](&Stack, Stack.name[i]);
		Stack.stackFrame[i].setFlags("g");
		Stack.stackFrame[i].Type (&Stack.stackFrame[i], "Type",            "dnc", NULL, (char **)StackTypeStr);
		Stack.stackFrame[i].Start(&Stack.stackFrame[i], "Start_or_return", "dn",  NULL, 0, CubPrgrmSz-1);
		Stack.stackFrame[i].Reqd (&Stack.stackFrame[i], "Required",        "dn",  NULL);
		Stack.stackFrame[i].Count(&Stack.stackFrame[i], "Completed",       "dn",  NULL);
		Stack.stackFrame[i].Delay(&Stack.stackFrame[i], "Delay",           "dn",  NULL);
		Stack.stackFrame[i].Tag  (&Stack.stackFrame[i], "Tag",             "dn",  NULL);
	}
	MemoryRegister(this, "Memory_Register", "dn", NULL, CubNMemRegs, false, MinFloat, MaxFloat);
	IntegerRegister(this, "Integer_Register", "dn", NULL, CubNMemRegs, false, MinInt, MaxInt);
	Instruction   (this, "Instruction");
	Instruction.Address   (&Instruction, "Address",   "r", NULL, 0, CubPrgrmSz-1);
	Instruction.Opcode    (&Instruction, "Opcode",    "r", NULL, 0, MaxUns);
	Instruction.OperandPar(&Instruction, "Parameter", "r", NULL, 0, MaxUns);
	Instruction.OperandVal(&Instruction, "Value",     "r", NULL, 0, MaxUns);
	FlagsWord	  (this, "FlagsWord", "u",   NULL, 0, 0x3FFFFFF);

#if 1

	for(i=0; i<CubConditionTableSz; i++)
	{
		sprintf(ConditionTable.name[i], "%d", i+1);
		ConditionTable.Table[i](&ConditionTable, ConditionTable.name[i]);
		ConditionTable.Table[i].setFlags("g");
		ConditionTable.Table[i].Type (&ConditionTable.Table[i], "Type",      "c", NULL, (char **)ConditionTypeStr);
		ConditionTable.Table[i].State(&ConditionTable.Table[i], "State", 	 "x", NULL, 0, MaxUns);
		ConditionTable.Table[i].Par  (&ConditionTable.Table[i], "Parameter", "x", NULL, 0, MaxUns);
		ConditionTable.Table[i].Val  (&ConditionTable.Table[i], "Value",     "x", NULL, 0, MaxUns);
		ConditionTable.Table[i].Par = NULL;
	}

	for(i=0; i<CubIntTableSz; i++)
	{
		sprintf(IntTable.name[i], "%d", i+1);
		IntTable.Table[i](&IntTable, IntTable.name[i]);
		IntTable.Table[i].setFlags("g");
		IntTable.Table[i].Mask   (&IntTable.Table[i], "Mask",    "x", NULL, 0, MaxUns);
		IntTable.Table[i].Handler(&IntTable.Table[i], "Handler", "", NULL, 0, MaxUns);
	}

#endif

	pProgram=pPrgrm;
	seekPointer=0;

	ConditionStatus = 0;
	InterruptStatus = 0;

	WIMask = 0;
	WIState = 0;

	pCnetTsLo=(Uint32 *)cnet_get_slotaddr(CNET_DATASLOT_TIMELO, PROC_DSPA);
	pCnetTsHi=(Uint32 *)cnet_get_slotaddr(CNET_DATASLOT_TIMEHI, PROC_DSPA);

	pProgram[0].opCode=CubHalt;
	RunMode = 0;
} 

void CubicEngine::parPrimeSignal()
{
	Parameter::parPrimeSignal();
	ProgramCap=CubPrgrmSz;
	GroupOn=False;
	RunMode=0;
}

void CubicEngine::parPollSignal()
{
	Parameter::parPollSignal();
}

void CubicEngine::doPoll()
{
	if(RunMode())
		doInstruction();
}

void CubicEngine::doIter()
{
	if(Delay) Delay--;
	doInterrupt();
}

void CubicEngine::doInterrupt()
{
#if 1
	if(RunMode())
	{
		int i;
		float epsilon = CompareEpsilon();
		Uint32 cstate;
		Uint32 mask;

		for(i=0; i<CubConditionTableSz; i++)
		{
			IntegerPar *p = (IntegerPar *)((UnsignedPar *)&ConditionTable.Table[i].Par)->fastRdRaw();
			generic pval;
			Uint32 type;
			ParVal val;
			ParVal prev;
			UnsignedPar *state;
			int res;

			if ((p == NULL) || (!p->isGood())) break;

			type = (Uint32)((UnsignedPar *)&ConditionTable.Table[i].Type)->fastRdRaw();
			val.u = (Uint32)((UnsignedPar *)&ConditionTable.Table[i].Val)->fastRdRaw();
			state = (UnsignedPar *)&ConditionTable.Table[i].State;
			prev.u = ConditionTable.Table[i].Prev;

			switch(p->getType())
			{
				case 'i':
					pval.i = getIntegerPar(p);

					switch(type) /* Comparison type */
					{
						case CubConditionTypeChange:
							res = pval.i != prev.i;
							break;
						case CubConditionTypeEQ:
							res = pval.i == val.i;
							break;
						case CubConditionTypeNE:
							res = pval.i != val.i;
							break;
						case CubConditionTypeLT:
							res = pval.i < val.i;
							break;
						case CubConditionTypeGT:
							res = pval.i > val.i;
							break;
					}
					ConditionTable.Table[i].Prev = pval.u;
					break;

				case 'u':
				case 'e':
					pval.u = getUnsignedPar(p);

					switch(type) /* Comparison type */
					{
						case CubConditionTypeChange:
							res = pval.u != prev.u;
							break;
						case CubConditionTypeEQ:
							res = pval.u == val.u;
							break;
						case CubConditionTypeNE:
							res = pval.u != val.u;
							break;
						case CubConditionTypeLT:
							res = pval.u < val.u;
							break;
						case CubConditionTypeGT:
							res = pval.u > val.u;
							break;
					}
					ConditionTable.Table[i].Prev = pval.u;
					break;

				case 'f':
					pval.f = getFloatPar(p);

					switch(type) /* Comparison type */
					{
						case CubConditionTypeChange:
							res = fabs(pval.f - prev.f) >= epsilon;
							break;
						case CubConditionTypeEQ:
							res = fabs(pval.f - val.f) < epsilon;
							break;
						case CubConditionTypeNE:
							res = fabs(pval.f - val.f) >= epsilon;
							break;
						case CubConditionTypeLT:
							res = pval.f < val.f;
							break;
						case CubConditionTypeGT:
							res = pval.f > val.f;
							break;
					}
					ConditionTable.Table[i].Prev = pval.u;
					break;
			}
			state->fastWrRaw(res);	/* No parameter events generated by this write, so don't try to
									   use events from ConditionTable.Table[x].State parameter.
									*/

			/* Set or clear the bits in the condition status word based on the current condition. */

			mask = 0x101 << i;
			ConditionStatus = (ConditionStatus & ~mask) | ((res) ? mask : 0);
		}

		/* Update the interrupt state bits based on the condition status and the mask bits in the
		   interrupt table.
		*/

		for(i = 0; i < CubIntTableSz; i++)
		{
			Uint32 mask;
			UnsignedPar *p = (UnsignedPar *)(UnsignedPar *)&IntTable.Table[i].Mask;

			if ((p == NULL) || (!p->isGood())) break;

			mask = p->fastRdRaw();

			if (mask == 0) break;

			cstate = ConditionStatus & mask;

			/* The mask bits have the following meaning:

			   Bits 0 to 7 indicate the condition flags corresponding to an AND condition.
			   I.e. all of the condition flags corresponding to the mask bits must be set 
			   to generate an interrupt.

			   Bits 8 to 15 indicate the condition flags corresponding to an OR condition.
			   I.e. any of the condition flags corresponding to the mask bits that are set 
			   will generate an interrupt.

			*/


			if ((InterruptMask & (1 << i)) && ((cstate & 0xFF00) || ((cstate & 0xFF) && ((cstate & 0xFF) == (mask & 0xFF)))))
			{
				InterruptStatus |= (1 << i);
			}
		}

		/* Update WaitInt state */

		if (WIMask)
		{
			cstate = ConditionStatus & WIMask;
			if ((cstate & 0xFF00) || ((cstate & 0xFF) && ((cstate & 0xFF) == (WIMask & 0xFF))))
			{
				WIState = 1;
			}
		}

	}
#endif
}

void CubicEngine::doInstruction()
{
	register CubInstruction *p;
	Parameter *q, *q2;
	SigInputChannel *s;
	Uint32 pc, sp;
	int  i;
    float fval = 0.0;
    void *fvbuf = (void *) &fval;
	u64_t cnetTime, baseTime;
	Uint32 mask;
//	Uint32 cstate;

	updateInstruction();

#if 1

		/* Test for interrupt */

	if (InterruptStatus)
	{
		pc=ProgramCounter();
		sp=StackPointer();

			for(i = 0; i < CubIntTableSz; i++)
			{
				mask = ((UnsignedPar *)&IntTable.Table[i].Mask)->fastRdRaw();
				if (mask == 0) break;

				if (InterruptStatus & (1 << i))
				{
					InterruptStatus &= ~(1 << i);	/* Clear state of this interrupt */
					if(sp>=CubStackSz)
						doException(CubExcptnStackOF);
					else
					{
						InterruptMask &= ~(1 << i); /* Disable this interrupt */
					Stack.stackFrame[sp].Delay=Delay;
						Stack.stackFrame[sp].Reqd=0;
						Stack.stackFrame[sp].Count=0;
						Stack.stackFrame[sp].Start=pc;
						Stack.stackFrame[sp].Tag=i;
						Stack.stackFrame[sp].Type=CubStackTypeInt;
						pc=((UnsignedPar *)&IntTable.Table[i].Handler)->fastRdRaw();
						ProgramCounter=pc;
						StackPointer=sp+1;
						Delay = 0;
						break;
					}
				}
			}
		}

#endif

	/* If a delay is currently executing, return immediately */

	if (Delay)
	{
		return;
	}

	do
	{

		pc=ProgramCounter();
		if(pc>=CubPrgrmSz)
		{
			doException(CubExcptnHalt);
			return;
		}
		sp=StackPointer();

		p=pProgram+pc;
		q=(Parameter *)p->par;
		switch(p->opCode)
		{
		case CubHalt:
			doException(CubExcptnHalt);
			break;
		case CubWaitI:
			if(goodPar(q, 'i')&&(getIntegerPar(q)==*((Sint32 *)&p->val)))
				ProgramCounter=pc+1;
			break;
		case CubWaitINE:
			if (goodPar(q, 'i') && (getIntegerPar(q) != *((Sint32 *)&p->val)))
				ProgramCounter = pc + 1;
			break;
		case CubWaitU:
			if (goodPar(q, 'u') && (getUnsignedPar(q) == *((Uint32 *)&p->val)))
				ProgramCounter = pc + 1;
			break;
		case CubAssignI:
			if(goodPar(q, 'i'))
			{
				setIntegerPar(q, *((Sint32 *)&(p->val)));
				ProgramCounter=pc+1;
			}
			break;
		case CubAssignU:
			if(goodPar(q, 'u'))
			{
				(*((UnsignedPar *)q))(*((Uint32 *)&p->val),0);
				ProgramCounter=pc+1;
			}
			break;
		case CubAssignF:
			if(goodPar(q, 'f'))
			{
				(*((FloatPar *)q))(*((float *)(&p->val)), 0, false);
				ProgramCounter=pc+1;
			}
			break;
		case CubFPlusEqual:
			if (goodPar(q, 'f'))
			{
				(*((FloatPar *)q))(getFloatPar(q) + *((float *)(&p->val)), 0, false);
				ProgramCounter = pc + 1;
			}
			break;
		case CubAssignFInstant:
			if(goodPar(q, 'f'))
			{
				q->setInstant(&p->val, sizeof(Float), 0);
				ProgramCounter=pc+1;
			}
			break;
		case CubRepeatBegin:
			if(sp>=CubStackSz)
				doException(CubExcptnStackOF);
			else
			{
				Stack.stackFrame[sp].Delay=0;
				Stack.stackFrame[sp].Reqd=p->par;
				Stack.stackFrame[sp].Count=p->val;
				Stack.stackFrame[sp].Start=pc+1;
				Stack.stackFrame[sp].Tag=p->val;
				Stack.stackFrame[sp].Type=CubStackTypeLoop;
				ProgramCounter=pc+1;
				StackPointer=sp+1;
			}
			break;
		case CubRepeatEnd:
			if((sp<1)||(Stack.stackFrame[sp-1].Type()!=CubStackTypeLoop))
				doException(CubExcptnNest);
			else
			{
				Stack.stackFrame[sp-1].Count=Stack.stackFrame[sp-1].Count()+1;
				if(Stack.stackFrame[sp-1].Count()>=Stack.stackFrame[sp-1].Reqd())
				{
					ProgramCounter=pc+1;
					if (ResetLoopState())
					{
						Stack.stackFrame[sp-1].Delay=0;
					Stack.stackFrame[sp-1].Reqd=0;
					Stack.stackFrame[sp-1].Count=0;
					Stack.stackFrame[sp-1].Start=0;
					Stack.stackFrame[sp-1].Tag=0;
					Stack.stackFrame[sp-1].Type=CubStackTypeNone;
					}
					StackPointer=sp-1;
				}
				else
					ProgramCounter=Stack.stackFrame[sp-1].Start();
			}
			break;
		case CubCondIBegin:
			if(goodPar(q, 'i'))
			{
				if(getIntegerPar(q)==*((Sint32 *)&p->val))
					ProgramCounter=pc+1;
				else
				{
					i=1;
					while(i&&((++pc)<(CubPrgrmSz-1)))
					{
						switch(pProgram[pc].opCode)
						{
						case CubCondIBegin:
							i++;
							break;
						case CubCondUBegin:
							i++;
							break;
						case CubCondEnd:
							i--;
							break;
						}
					}
					if(pc>=(CubPrgrmSz-1))
						doException(CubExcptnNest);
					else
						ProgramCounter=pc+1;
				}
			}
			break;
		case CubCondUBegin:
			if (goodPar(q, 'u'))
			{
				if (getUnsignedPar(q) == *((Uint32 *)&p->val))
					ProgramCounter = pc + 1;
				else
				{
					i = 1;
					while (i && ((++pc)<(CubPrgrmSz - 1)))
					{
						switch (pProgram[pc].opCode)
						{
						case CubCondIBegin:
							i++;
							break;
						case CubCondUBegin:
							i++;
							break;
						case CubCondEnd:
							i--;
							break;
						}
					}
					if (pc >= (CubPrgrmSz - 1))
						doException(CubExcptnNest);
					else
						ProgramCounter = pc + 1;
				}
			}
			break;
		case CubCondEnd:
			ProgramCounter=pc+1;
			break;
		case CubPause:
			ProgramCounter=pc+1;
			PauseTag=p->par;
			doException(CubExcptnPause);
			break;
		case CubAssignMemF:
			if((p->val)>=CubNMemRegs)
			{
				doException(CubExcptnIllVal);
				break;
			}
			(*MemoryRegister.member[p->val])=*((float *)(&p->par));
			ProgramCounter=pc+1;
			break;
		case CubSaveMemF:
			if(!goodPar(q, 'f'))
				break;
			if((p->val)>=CubNMemRegs)
			{
				doException(CubExcptnIllVal);
				break;
			}
			(*MemoryRegister.member[p->val])=(*((FloatPar *)q))();
			ProgramCounter=pc+1;
			break;
		case CubAddMemF:
			if (!goodPar(q, 'f'))
				break;
			if ((p->val) >= CubNMemRegs)
			{
				doException(CubExcptnIllVal);
				break;
			}
			(*MemoryRegister.member[p->val]) = (*MemoryRegister.member[p->val])() + getFloatPar(q);
			ProgramCounter = pc + 1;
			break;
		case CubRecallMemF:
			if(!goodPar(q, 'f'))
				break;
			if((p->val)>=CubNMemRegs)
			{
				doException(CubExcptnIllVal);
				break;
			}
			(*((FloatPar *)q))((*MemoryRegister.member[p->val])(), 0, false);
			ProgramCounter=pc+1;
			break;
		case CubRecallMemFInstant:
			if(!goodPar(q, 'f'))
				break;
			if((p->val)>=CubNMemRegs)
			{
				doException(CubExcptnIllVal);
				break;
			}
			fval = (*MemoryRegister.member[p->val])();
			((FloatPar *)q)->setInstant(fvbuf, sizeof(Float), 0);
			ProgramCounter=pc+1;
			break;
		case CubCallSub:
			if(sp>=CubStackSz)
				doException(CubExcptnStackOF);
			else
			{
				Stack.stackFrame[sp].Delay=0;
				Stack.stackFrame[sp].Reqd=0;
				Stack.stackFrame[sp].Count=0;
				Stack.stackFrame[sp].Start=pc+1;
				Stack.stackFrame[sp].Tag=p->val;
				Stack.stackFrame[sp].Type=CubStackTypeCall;
				ProgramCounter=p->par;
				StackPointer=sp+1;
			}
			break;
		case CubReturnSub:
			if((sp<1)||(Stack.stackFrame[sp-1].Type()!=CubStackTypeCall))
				doException(CubExcptnNest);
			else
			{
				ProgramCounter=Stack.stackFrame[sp-1].Start();
				Stack.stackFrame[sp-1].Delay=0;
				Stack.stackFrame[sp-1].Reqd=0;
				Stack.stackFrame[sp-1].Count=0;
				Stack.stackFrame[sp-1].Start=0;
				Stack.stackFrame[sp-1].Tag=0;
				Stack.stackFrame[sp-1].Type=CubStackTypeNone;
				StackPointer=sp-1;
			}
			break;
		case CubParCopy:
			if(!Parameter::blockCopy(q, (Parameter *)p->val))
				doException(CubExcptnIllPar);
			else
				ProgramCounter=pc+1;
			break;
		case CubWriteHS:
			Poke(smpbsLimitSys->HSOut, p->par);
			ProgramCounter=pc+1;
			break;
		case CubWaitHS0:
			if ((Peek(smpbsLimitSys->HSIn) & p->par) == 0) ProgramCounter=pc+1;
			break;
		case CubWaitHS1:
			if ((Peek(smpbsLimitSys->HSIn) & p->par) != 0) ProgramCounter=pc+1;
			break;

		case CubGotoLine:
			ProgramCounter = p->par;
			break;

		case CubGotoParLine:
			if(goodPar(q, 'u')) {
				ProgramCounter = getUnsignedPar(q);
			}
			break;

		case CubCompParF:
			q2 = (Parameter *)p->val;
			if (!goodPar(q2, 'f'))
				break;
			if (!goodPar(q, 'f'))
				break;
			if (getFloatPar(q2) < getFloatPar(q))
			{
				if ((getFloatPar(q) - getFloatPar(q2)) > CompareEpsilon())
					ResultRegister = 1;
				else
					ResultRegister = 0;
			}
			else
			{
				if ((getFloatPar(q2) - getFloatPar(q)) > CompareEpsilon())
					ResultRegister = -1;
				else
					ResultRegister = 0;
			}
			ProgramCounter = pc + 1;
			break;

		case CubCompF:
			if (!goodPar(q, 'f'))
				break;
			fval = *((float *)(&p->val));
			if (fval < getFloatPar(q))
			{
				if ((getFloatPar(q) - fval) > CompareEpsilon())
					ResultRegister = 1;
				else
					ResultRegister = 0;
			}
			else
			{
				if ((fval - getFloatPar(q)) > CompareEpsilon())
					ResultRegister = -1;
				else
					ResultRegister = 0;
			}
			ProgramCounter = pc + 1;
			break;

		case CubWaitFGT:
			if (goodPar(q, 'f') && (getFloatPar(q) > *((float *)&p->val)))
				ProgramCounter = pc + 1;
			break;

		case CubWaitFLT:
			if (goodPar(q, 'f') && (getFloatPar(q) < *((float *)&p->val)))
				ProgramCounter = pc + 1;
			break;

		case CubWaitUGT:
			if (goodPar(q, 'u') && (getUnsignedPar(q) > *((Uint32 *)&p->val)))
				ProgramCounter = pc + 1;
			break;

		case CubWaitUNE:
			if (goodPar(q, 'u') && (getUnsignedPar(q) != *((Uint32 *)&p->val)))
				ProgramCounter = pc + 1;
			break;

		case CubSetWaitTimer:
			if (!pCnetTsLo)
			{ 
				doException(CubExcptnBadPtr);
			}
			else
			{
				BaseTimerHigh = *pCnetTsHi;
				BaseTimerLow = *pCnetTsLo;
				ProgramCounter = pc + 1;
			}
			break;

		case CubWaitForTimeout:
			if (!pCnetTsLo)
			{ 
				doException(CubExcptnBadPtr);
			}
			else
			{
				baseTime = (((u64_t) BaseTimerHigh()) << 32) + (u64_t) BaseTimerLow();
				cnetTime = (((u64_t) *pCnetTsHi) << 32) + (u64_t)*pCnetTsLo;
	 			CurrentTime = (unsigned)(cnetTime-baseTime);
				if( CurrentTime() > WaitTime())
					ProgramCounter = pc + 1;
			}
			break;

		case CubZeroChan:
			//not working in sim
#ifndef PCTEST
			if ((s = CtrlChkSignal(p->par)) && (s->pExtCh))
			{
				chandef *c = (chandef *)(s->pExtCh);
				if (c && c->refzero_zero)
				{
					if (c->refzero_zero(c, TRUE, TRUE, FALSE) != NO_ERROR)
					{
						doException(CubExcptnZeroRange);
						break;
					}
				}
			}
			else
			{
				doException(CubExcptnIllVal);
				break;
			}
#endif
			ProgramCounter = pc + 1;
			break;

		case CubRTControl:
			ctrl_rtctrl_control(p->par);
			ProgramCounter = pc + 1;
			break;

		case CubRTPause:
			ctrl_rtctrl_pause(p->par);
			ProgramCounter = pc + 1;
			break;

		case CubRTResume:
			ctrl_rtctrl_resume();
			ProgramCounter = pc + 1;
			break;

		case CubDelay:
			Delay = p->par;
			ProgramCounter = pc + 1;
			break;

		case CubClrInt:
			InterruptStatus &= ~(*((Uint32 *)&(p->val)));
			ProgramCounter = pc + 1;
			break;

		case CubWaitInt:
			WIMask = *((Uint32 *)&(p->val));
			if (WIState)
			{
				ResultRegister = ConditionStatus;
				ProgramCounter = pc + 1;
				WIMask = 0;
				WIState = 0;
			}
			break;

		case CubSelectGenerator:
			pGen = (Generator *)(p->par);
			TableIndex = 0;
			ProgramCounter = pc + 1;
			break;

		case CubLoadWavetable:
			pGen->setWavetableEntry(TableIndex++, *((float *)&(p->val)));

			/* After we've written the last table entry, call the newWaveTable function to
			   update various settings and copy the wavetable into DSP B.
			*/

			if (TableIndex == GenWTSz) pGen->newWaveTable();
			ProgramCounter = pc + 1;
			break;

		case CubBeginGroup:
			GroupOn = True;
			ProgramCounter = pc + 1;
			break;

		case CubEndGroup:
			GroupOn = False;
			ProgramCounter = pc + 1;
			break;

		case CubLedOn:
#ifndef PCTEST
			LOG_LED(0, 1);
#endif
			ProgramCounter = pc + 1;
			break;

		case CubLedOff:
#ifndef PCTEST
			LOG_LED(0, 0);
#endif
			ProgramCounter = pc + 1;
			break;

		case CubReturnInt:
			if((sp<1)||(Stack.stackFrame[sp-1].Type()!=CubStackTypeInt))
				doException(CubExcptnNest);
			else
			{
//				Uint32 i = ((UnsignedPar *)&Stack.stackFrame[sp-1].Tag)->fastRdRaw();
//				Uint32 mask = ((UnsignedPar *)&IntTable.Table[i].Mask)->fastRdRaw();

				Delay = Stack.stackFrame[sp-1].Delay();
				ProgramCounter=Stack.stackFrame[sp-1].Start();
				Stack.stackFrame[sp-1].Delay=0;
				Stack.stackFrame[sp-1].Reqd=0;
				Stack.stackFrame[sp-1].Count=0;
				Stack.stackFrame[sp-1].Start=0;
				Stack.stackFrame[sp-1].Tag=0;
				Stack.stackFrame[sp-1].Type=CubStackTypeNone;
				StackPointer=sp-1;

				InterruptMask |= *((Uint32 *)&(p->val));
			}
			break;

		case CubClrIntMask:
			InterruptMask &= ~(*((Uint32 *)&(p->val)));
			ProgramCounter = pc + 1;
			break;

		case CubSetIntMask:
			InterruptMask |= *((Uint32 *)&(p->val));
			ProgramCounter = pc + 1;
			break;

		case CubRepeatBreak:
			if((sp<1)||(Stack.stackFrame[sp-1].Type()!=CubStackTypeLoop))
				doException(CubExcptnNest);
			else
			{
				while(1)
				{
					pc++;
					if (pc >= CubPrgrmSz)
					{
						doException(CubExcptnNest);
						break;
					}
					else
					{
						p = pProgram + pc;
						if (p->opCode == CubRepeatEnd)
						{
							ProgramCounter = pc + 1;
							Stack.stackFrame[sp-1].Delay=0;
							Stack.stackFrame[sp-1].Reqd=0;
							Stack.stackFrame[sp-1].Count=0;
							Stack.stackFrame[sp-1].Start=0;
							Stack.stackFrame[sp-1].Tag=0;
							Stack.stackFrame[sp-1].Type=CubStackTypeNone;
							StackPointer=sp-1;
							break;
						}
					}
				}
			}
			break;

		case CubAssign:
			if(goodPar(q))
			{
				q->Load(&p->val);
				ProgramCounter=pc+1;
			}
			break;

		case CubPlusEqual:
			if (goodPar(q))
			{
				int type = q->getType();

				if (type == 'f')
				{
					float v = getFloatPar(q);
					v += *((float *)(&p->val));
  				    (*((FloatPar *)q))(v, 0, false);
				}
				else if (type == 'u')
				{
					Uint32 v = getUnsignedPar(q);
					v += *((Uint32 *)(&p->val));
  				    (*((UnsignedPar *)q))(v, 0);
				}
				else
				{
					int v = getIntegerPar(q);
					v += *((Sint32 *)(&p->val));
  				    (*((IntegerPar *)q))(v, 0);
				}

				ProgramCounter = pc + 1;
			}
			break;

		case CubMpyEqual:
			if (goodPar(q))
			{
				int type = q->getType();

				if (type == 'f')
				{
					float v = getFloatPar(q);
					v *= *((float *)(&p->val));
  				    (*((FloatPar *)q))(v, 0, false);
				}
				else if (type == 'u')
				{
					Uint32 v = getUnsignedPar(q);
					v *= *((Uint32 *)(&p->val));
  				    (*((UnsignedPar *)q))(v, 0);
				}
				else
				{
					int v = getIntegerPar(q);
					v *= *((Sint32 *)(&p->val));
  				    (*((IntegerPar *)q))(v, 0);
				}

				ProgramCounter = pc + 1;
			}
			break;

		case CubWaitEQ:
			if(goodPar(q))
			{
				Uint32 v = p->val;
				int type = q->getType();

				if (type == 'f')
				{
					float diff = getFloatPar(q) - _itof(v);
					if (fabs(diff) <= CompareEpsilon()) ProgramCounter=pc+1;
				}
				else if (type == 'u')
				{
					if (getUnsignedPar(q) == v) ProgramCounter=pc+1;
				}
				else
				{
					if (getIntegerPar(q) == (Sint32)v) ProgramCounter=pc+1;
				}
			}
			break;

		case CubWaitNE:
			if(goodPar(q))
			{
				Uint32 v = p->val;
				int type = q->getType();

				if (type == 'f')
				{
					float diff = getFloatPar(q) - _itof(v);
					if (fabs(diff) > CompareEpsilon()) ProgramCounter=pc+1;
				}
				else if (type == 'u')
				{
					if (getUnsignedPar(q) != v) ProgramCounter=pc+1;
				}
				else
				{
					if (getIntegerPar(q) != (Sint32)v) ProgramCounter=pc+1;
				}
			}
			break;

		case CubWaitGT:
			if(goodPar(q))
			{
				Uint32 v = p->val;
				int type = q->getType();

				if (type == 'f')
				{
					float diff = getFloatPar(q) - _itof(v);
					if (diff > CompareEpsilon()) ProgramCounter=pc+1;
				}
				else if (type == 'u')
				{
					if (getUnsignedPar(q) > v) ProgramCounter=pc+1;
				}
				else
				{
					if (getIntegerPar(q) > (Sint32)v) ProgramCounter=pc+1;
				}
			}
			break;

		case CubWaitLT:
			if(goodPar(q))
			{
				Uint32 v = p->val;
				int type = q->getType();

				if (type == 'f')
				{
					float diff = getFloatPar(q) - _itof(v);
					if (diff < -CompareEpsilon()) ProgramCounter=pc+1;
				}
				else if (type == 'u')
				{
					if (getUnsignedPar(q) < v) ProgramCounter=pc+1;
				}
				else
				{
					if (getIntegerPar(q) < (Sint32)v) ProgramCounter=pc+1;
				}
			}
			break;

		case CubComp:
			if (goodPar(q))
			{
				generic v;
				int type = q->getType();

				v.u = p->val;

				if (type == 'f')
				{
					float diff = getFloatPar(q) - v.f;
					float epsilon = CompareEpsilon();
					if (diff >= 0.0F)
					{
						ResultRegister = (diff > epsilon) ? 1 : 0;
					}
					else
					{
						ResultRegister = (diff < -epsilon) ? -1 : 0;
					}
				}
				else if (type == 'u')
				{
					unsigned int vq = getUnsignedPar(q);
					if (vq > v.u)
					{
						ResultRegister = 1;
					}
					else if (vq < v.u)
					{
						ResultRegister = -1;
					}
					else
					{
						ResultRegister = 0;
					}
				}
				else
				{
					unsigned int vq = getIntegerPar(q);
					if (vq > v.i)
					{
						ResultRegister = 1;
					}
					else if (vq < v.i)
					{
						ResultRegister = -1;
					}
					else
					{
						ResultRegister = 0;
					}
				}
			}
			ProgramCounter = pc + 1;
			break;

		case CubCompPar:
			q2 = (Parameter *)p->val;
			if (goodPar(q) && goodPar(q2))
			{
				int type = q->getType();

				if (type != q2->getType())
				{
					doException(CubExcptnTypeMismatch);
					break;
				}

				if (type == 'f')
				{
					float diff = getFloatPar(q) - getFloatPar(q2);
					float epsilon = CompareEpsilon();
					if (diff >= epsilon)
					{
						ResultRegister = 1;
					}
					else if (diff < -epsilon)
					{
						ResultRegister = -1;
					}
					else
					{
						ResultRegister = 0;
					}
				}
				else if (type == 'u')
				{
					unsigned int vq = getUnsignedPar(q);
					unsigned int vq2 = getUnsignedPar(q2);
					if (vq > vq2)
					{
						ResultRegister = 1;
					}
					else if (vq < vq2)
					{
						ResultRegister = -1;
					}
					else
					{
						ResultRegister = 0;
					}
				}
				else
				{
					int vq = getIntegerPar(q);
					int vq2 = getIntegerPar(q2);
					if (vq > vq2)
					{
						ResultRegister = 1;
					}
					else if (vq < vq2)
					{
						ResultRegister = -1;
					}
					else
					{
						ResultRegister = 0;
					}
				}
			}
			ProgramCounter = pc + 1;
			break;

		case CubSaveMem:
			if (goodPar(q))
			{
				int type = q->getType();
				int i = p->val;

				if(i >= CubNMemRegs)
				{
					doException(CubExcptnIllVal);
					break;
				}

				/* Write value into appropriate register array depending on type */

				if (type == 'f')
				{
					float v = getFloatPar(q);
					MemoryRegister.member[i]->Load(&v);
				}
				else
				{
					int v = getIntegerPar(q);
					IntegerRegister.member[i]->Load(&v);
				}
			}
			ProgramCounter=pc+1;
			break;

		case CubRecallMem:
			if (goodPar(q))
			{
				int type = q->getType();
				int i = p->val;

				if(i >= CubNMemRegs)
				{
					doException(CubExcptnIllVal);
					break;
				}

				/* Read value from appropriate register array depending on type */

				if (type == 'f')
				{
					float v = (*MemoryRegister.member[i])();
  				    (*((FloatPar *)q))(v, 0, false);
				}
				else if (type == 'u')
				{
					Uint32 v = IntegerRegister.member[i]->fastRdRaw();
  				    (*((UnsignedPar *)q))(v, 0);
				}
				else
				{
					int v = IntegerRegister.member[i]->fastRdRaw();
  				    (*((IntegerPar *)q))(v, 0);
				}
			}
			ProgramCounter=pc+1;
			break;

		case CubAddMem:
			if (goodPar(q))
			{
				int i = p->val;

				if (i >= CubNMemRegs)
				{
					doException(CubExcptnIllVal);
					break;
				}

				if (q->getType() == 'f')
				{
					(*MemoryRegister.member[i]) = (*MemoryRegister.member[i])() + getFloatPar(q);
				}
				else
				{
					(*IntegerRegister.member[i]) = (*IntegerRegister.member[i])() + getIntegerPar(q);
				}
			}
			ProgramCounter = pc + 1;
			break;

		case CubCondBegin:
			if(goodPar(q))
			{
				Uint32 eq;

				if (q->getType() == 'f')
				{
					float diff = fabs(getFloatPar(q) - *((float *)(&p->val)));
					eq = (diff <= CompareEpsilon());
				}
				else
				{
					eq = (getIntegerPar(q) == p->val);
				}

				if (eq)
					ProgramCounter=pc+1;
				else
				{
					i=1;
					while(i&&((++pc)<(CubPrgrmSz-1)))
					{
						switch(pProgram[pc].opCode)
						{
						case CubCondBegin:
							i++;
							break;
						case CubCondEnd:
							i--;
							break;
						}
					}
					if (pc>=(CubPrgrmSz-1))
						doException(CubExcptnNest);
					else
						ProgramCounter=pc+1;
				}
			}
			break;

		default:
			doException(CubExcptnIllOp);
		}

	} while(GroupOn);

}

void CubicEngine::doException(Sint32 e)
{
	Exception=e;
	RunMode=0;
	GroupOn=False;
	Delay=0;
}

bool CubicEngine::goodPar(Parameter *p, char t)
{
	char parType;

	if(!p->isGood())
	{
		doException(CubExcptnIllPar);
		return false;
	}
	parType=p->getType();
	if(parType=='e')
		parType='i';
	if(parType!=t)
	{
		doException(CubExcptnIllPar);
		return false;
	}
	return true;
}

bool CubicEngine::goodPar(Parameter *p)
{
	if(!p->isGood())
	{
		doException(CubExcptnIllPar);
		return false;
	}
	return true;
}

void CubicEngine::setIntegerPar(Parameter *p, Sint32 v)
{
	if(p->getType()=='i')
		(*((IntegerPar *)p))(v, 0);
	else
		(*((EnumPar *)p))(v, 0);
}

Sint32 CubicEngine::getIntegerPar(Parameter *p)
{
	if(p->getType()=='i')
		return (*((IntegerPar *)p))();
	else
		return (*((EnumPar *)p))();
}

Uint32 CubicEngine::getUnsignedPar(Parameter *p)
{
	if(p->getType()=='u')
		return (*((UnsignedPar *)p))();
     else 
		return 0;
}

float CubicEngine::getFloatPar(Parameter *p)
{
	if(p->getType()=='f')
		return (*((FloatPar *)p))();
     else 
		return 0;
}

Result CubicEngine::special(int fn, void *in, int incnt, void *out, int outcnt)
{
	Uint32 bs, len;

	switch(fn)
	{
	case ParSp_Set_PrgrmSP:
		if(incnt<sizeof(Uint32))
			return ParBufferError;
		bs=((Uint32 *)in)[0];
		if(bs>CubPrgrmSz)
			return ParOverRange;
		seekPointer=bs;
		break;
	case ParSp_Set_Prgrm:
		if(RunMode())
			return ParNotSettable;
		if(incnt<sizeof(CubInstruction))
			return ParBufferError;
		bs=seekPointer;
		len=(Uint32)(incnt/sizeof(CubInstruction));
		if((bs+len)>CubPrgrmSz)
			len=CubPrgrmSz-bs;
		if(len)
			memcpy(pProgram+bs, in, len*sizeof(CubInstruction));
		seekPointer=bs+len;
		break;
	default:
		return Parameter::special(fn, in, incnt, out, outcnt);
	}
		return ParOkay;
}

void CubicEngine::updateInstruction()
{
	register CubInstruction *p;
	Uint32 pc;

	pc=ProgramCounter();
	if(pc>=CubPrgrmSz)
		return;
	p=pProgram+pc;
	Instruction.Address=pc;
	Instruction.Opcode=p->opCode;
	Instruction.OperandPar=p->par;
	Instruction.OperandVal=p->val;
}

void CubicEngine::parSetEvent(Parameter *sce)
{
	int i;

	if((sce==&Reset)&&(RunMode()==0))
	{
		ProgramCounter=0;
		StackPointer=0;
		Exception=0;
		PauseTag=0;
		Delay = 0;
		GroupOn = False;
		InterruptStatus = 0;
		InterruptMask = 0;
		updateInstruction();
		for(i=0; i<CubStackSz; i++)
			Stack.stackFrame[i].Type=0;
		for(i=0; i<CubNMemRegs; i++)
		{
			(*MemoryRegister.member[i])=0.0f;
			(*IntegerRegister.member[i])=0;
		}
	}
	if((sce==&Step)&&(RunMode()==0))
		doInstruction();
}

void CubicEngine::parAlteredEvent(Parameter *sce)
{
	if((sce==&RunMode)&&(RunMode()==1))
	{
		Exception = 0;
		ConditionStatus = 0;
		InterruptStatus = 0;
		InterruptMask = 0;
		WIMask = 0;
		WIState = 0;
		GroupOn = False;
	}
}

#endif
