// ErrorPath.h - Error Path Class header

#include <stdlib.h>

#define	EPFadeInc		(1.0f/(2.0f*SysFs))		// Two second command fade
#define	EPNameSz		32						// Size of name string
#define	EPDeltaPHPFreq	100.0f					// Delta-P high pass filter
#define	EPILErrLPFreq	500.0f					// Inner loop error low pass filter

typedef struct {
	SmpSigInputChannel *pFbCh;
	Sint32 req;
	Sint32 sel;
	Sint32 ilmodeflg;
	Sint32 setupflg;
} SmpEPMode;

typedef struct {
	Sint32 req;
	Sint32 sel;
	Sint32 safe;
	Sint32 state;
	float fade;
} SmpEPCmd;

typedef struct {
	float den[3];
	float state[2];
} SmpOscillator;

class SmpErrorPath {
public:
#ifdef CONTROLCUBE
	void iter()
	{
		register int i;
#ifdef CONTROLCUBE
		register SmpSigInputChannel *q;
#ifndef EXTERNALSIMULATION
		float simFb;
#endif
		float w, y, z;
#endif
		Uint32 tsrin, tsrout;
		register SmpEPCmd *u;
		register SmpEPMode *r, *s;
		register float ecfb;
		register float x;
		float span;
		int rampSp;

		if(!enable)
		{
			servoen=0;
			flowlimon=0;
			feedback=0.0f;
			OLoutput=0.0f;
			error=0.0f;
			output=extCmd=0.0f;
			cmd=(pIntCmd?*pIntCmd:0.0f);
			cNetCmd=pCNet?pCNet[cNetCmdSlot]:0.0f;
			STM=0.0f;
			return;
		}

		// TSR I/O and span
		tsrin=pTsrIn?(*pTsrIn):0;
		tsrout=0;
		servoen=tsrin&TSRServoEn;
		span=pSpan?*pSpan:0.0f;

		// Command source change - manual
		for(i=EPNCmd, u=cmdSce; i--; u++)
			if(u->req)
			{
				if((!u->state)&&(u->safe))
				{
					if(currentCmd)
					{
						currentCmd->state=0;
						currentCmd->sel=0;
					}
					currentCmd=u;
					currentCmd->state=1;
				}
				u->req=0;
				break;
			}

		ecfb=pCompFb?(*pCompFb):0.0f;

		// Mode change - manual
		s=NULL;
		for(i=EPNMode, r=modeArray; i--; r++)
			if(r->req)
			{
				if((r->pFbCh)&&!(servoen&&r->ilmodeflg))
					s=r;
				r->req=0;
				break;
			}

		// Mode change - TSR
		rampSp=0;
		if(startMode&&(modeArray[startMode-1].pFbCh)&&(tsrin&TSRForceSetup))
			s=modeArray+startMode-1;
		else
		{
			if(changeOnStop&&targetMode&&(tsrin&TSRModeChStop))
			{
				if(modeArray[targetMode-1].pFbCh)
				{
					s=modeArray+(targetMode-1);
					rampSp=relaxSp;
				}
			}
		}

		// Mode change - hand controller
		if(startMode&&(modeArray[startMode-1].pFbCh)&&pHCDelta&&((*pHCDelta)&SigHCSetupBit))
			s=modeArray+(startMode-1);

		// Default to start up mode
		if(startMode&&!(currentMode&&currentMode->pFbCh&&(servoen||ILTune)))
		{
			s=modeArray+(startMode-1);
			if(!s->pFbCh)
				s=modeArray;
		}

		// Perform mode change if required
		if(s)
		{
			if(currentMode)
				currentMode->sel=0;
			currentMode=s;
			currentMode->sel=1;
		}

		// Switch command source to Set point
		if(s||(dmhSP&&pHCDelta&&(!(tsrin&TSRStatPend))&&((*pHCDelta)&SigHCDMBit)))
		//if( (switch required) OR (hand controller wants it?) ){
		{
			if(currentCmd)
			{
				currentCmd->state=0;
				currentCmd->sel=0;
			}
			currentCmd=cmdSce+0;
			currentCmd->state=1;
			for(i=EPNCmd, u=cmdSce; i--; u++)
				u->fade=0.0f;
			currentCmd->fade=1.0f;
			currentCmd->sel=1;
		}

		// Perform command fade
		if(augment)
		{
			cmdSce[0].sel=1;
			cmdSce[0].fade=1.0f;
			i=EPNCmd-1;
			u=cmdSce+1;
		}
		else
		{
			i=EPNCmd;
			u=cmdSce;
		}
		while(i--)
		{
			if(u->state)
			{
				u->fade+=cmdXferRate;
				if(u->fade>=1.0f)
				{
					u->fade=1.0f;
					u->sel=1;
				}
			}
			else
			{
				u->fade-=cmdXferRate;
				if(u->fade<0.0f)
					u->fade=0.0f;
			}
			u++;
		}

		// Get composite command
		cNetCmd=pCNet?pCNet[cNetCmdSlot]:0.0f;
		extCmd=(pExtCmdCh&&cmdSce[2].safe)?(pExtCmdCh->output):0.0f;
		x =cmdSce[3].fade*span*cNetCmd;
		x+=cmdSce[2].fade*(pExtCmdCh?(pExtCmdCh->setPnt+span*extCmd):0.0f);
		x+=cmdSce[1].fade*(pIntCmd?*pIntCmd:0.0f);

		// Outer loop iteration / set point track
#ifdef CONTROLCUBE
		y=z=w=0.0f;
		for(i=0, r=modeArray; i<EPNMode; i++, r++)
			
			/*if(q=r->pFbCh) //let FB know if it is being used.
			{
				if(r->sel)
				{
					SmpSigInputChannel *pTemp = (r->pFbCh);
					((*pTemp).fbEn) = 1;
				}
				else
				{
					SmpSigInputChannel *pTemp = (r->pFbCh);
					((*pTemp).fbEn) = 0;
				}
			}*/
			if(q=r->pFbCh)
			{
				if(r->sel) {
					q -> fbEn=1;}
				else {
					q ->fbEn=0;}
#ifdef EXTERNALSIMULATION
				if(r->sel)
				{
					if(r->ilmodeflg?ILTune:servoen)
//						y=q->loopIter(x, cmdSce[0].fade, &cmd, &w, &z, ecfb, rampSp, &STM, pSetPointInc?*pSetPointInc:0.0f, r->ilmodeflg, pCascadeCh);
						y=q->loopIter(x, cmdSce[0].fade, &cmd, &w, &z, ecfb, rampSp, &STM, 0.0f, r->ilmodeflg, pCascadeCh);
					else
					{
						STM=0.0f;
						if(r->ilmodeflg)
							cmd=0.0f;
						else
							cmd=q->spTrackIter(OLoutput, ecfb, pCascadeCh);
					}
				}
				else
					if(!r->ilmodeflg)
						q->spTrackIter(OLoutput, ecfb, pCascadeCh);
#else
				simFb=((simOutput>0.0f)?modeGainPos[i]:modeGainNeg[i])*simOutput;
				if(r->sel)
				{
					if(r->ilmodeflg?ILTune:servoen)
//						y=q->loopIter(x, cmdSce[0].fade, &cmd, &w, &z, ecfb, simEnable, simFb, rampSp, &STM, pSetPointInc?*pSetPointInc:0.0f, r->ilmodeflg, pCascadeCh);
						y=q->loopIter(x, cmdSce[0].fade, &cmd, &w, &z, ecfb, simEnable, simFb, rampSp, &STM, 0.0f, r->ilmodeflg, pCascadeCh);
					else
					{
						STM=0.0f;
						if(r->ilmodeflg)
							cmd=0.0f;
						else
							cmd=q->spTrackIter(OLoutput, ecfb, simEnable, simFb, pCascadeCh);
					}
				}
				else
					if(!r->ilmodeflg)
						q->spTrackIter(OLoutput, ecfb, simEnable, simFb, pCascadeCh);
#endif
			}
		feedback=w;
		OLoutput=y;
		error=z;
#endif

		// Simulation
		if(simEnable)
		{
			double noise;
			double total;
#ifdef PCTEST
			simInt+=(double)(aGain*aGainFac*output);
#else
			simInt+=(double)(aGain*aGainFac*output);
#endif
			Clamp(simInt, -1.0f, 1.0f);
			noise = (((double)rand() / RAND_MAX) - 0.5) / 100.0;
			noise = noise * noiseRange;
			total = simInt + noise;
			Clamp(noise, -1.0F, 1.0F);
			simOutput=DeltaFilter(simNum, simDen, simState, 2, (float)total);
		}
		else
		{
			simInt=simState[0]=simState[1]=0.0;
			simOutput=0.0f;
		}

		// Flow limit
		flowlimon=pTsrIn?(tsrin&TSRFlowLim):1;
		x=OLoutput;
		if(flowlimon)
			Clamp(x, -flowlimlevel, flowlimlevel);

		// Unequal area compensation
		if(x>0.0f)
		{
			if(!UACDirn)
				x*=UACGain;
		}
		else
			if(UACDirn)
				x*=UACGain;

		// Inner loop
		if(ilen&&pILCh&&!ILTune)
		{
			if(tsrin&TSRPressureOff) {
				x = 0.0; }
			else {
				if(pDeltaPFb)
					x-=deltaPGain*Filter(deltaPDerFilterNum, deltaPDerFilterDen, &deltaPDerFilterState, 1, *pDeltaPFb);
				x=pILCh->ilIter(x);
			}
		}

		Clamp(x, -1.0f, 1.0f);
		output=x;

		// Stop generator if not commanding
		if((currentCmd!=(cmdSce+1))&&(tsrin&TSRStatPend))
			tsrout|=(TSRStopDyn+TSRStopWR);

		// Setup mode flagging
		if(currentMode&&currentMode->setupflg)
			tsrout|=TSRORedSetup;

		if(pTsrOut)
			(*pTsrOut)|=tsrout;

		modeNo=currentMode?(currentMode-modeArray):0;
	}
#endif
#ifdef AICUBE
	void iter()
	{
		#if 0
		servoen=0;
		flowlimon=0;
		feedback=0.0f;
		OLoutput=0.0f;
		error=0.0f;
		output=extCmd=0.0f;
		STM=0.0f;
		#endif
		cmd=(pIntCmd?*pIntCmd:0.0f);
		cNetCmd=pCNet?pCNet[cNetCmdSlot]:0.0f;
		//#endif
	}
#endif
// Inputs:
// Outputs:
	float output; //
	#ifndef AICUBE
		float extCmd;
	#endif
	float cNetCmd;
	Sint32 modeNo;
// Supervisory comms:
	#ifndef AICUBE
		float *pCompFb; //
		SmpSigInputChannel *pILCh, *pCascadeCh; //
	#endif
	float *pIntCmd; 
	float *pSpan;
	float *pSetPointInc;
	#ifndef AICUBE
		float *pDeltaPFb; //
	#endif
	SmpSigInputChannel *pExtCmdCh;
	float *pCNet;
	SmpEPCmd cmdSce[EPNCmd];
	#ifndef AICUBE
		Sint32 augment;
		Sint32 changeOnStop;
		Sint32 targetMode;
		Sint32 relaxSp;
		Sint32 startMode;		
	#endif
	SmpEPMode modeArray[EPNMode];
	float cmd, feedback, OLoutput;
	float error;
	#ifndef AICUBE
		Sint32 simEnable;
		float aGain;
		float aGainFac;
		float modeGainPos[EPNMode];
		float modeGainNeg[EPNMode];
		float simNum[3], simDen[3];
		float simOutput;
		float noiseRange;
	#endif
	Uint32 *pTsrIn;
	Uint32 *pTsrOut;
	Sint32 servoen, cascen; //
	#ifndef AICUBE
		Sint32 flowlimon;
		float flowlimlevel;
		Sint32 UACDirn;
		float  UACGain;
		Sint32 ilen;
		float deltaPDerFilterNum[2];
		float deltaPDerFilterDen[2];
		float deltaPGain;
		float STM;
	#endif
	Uint32 cNetCmdSlot;
	//#ifndef AICUBE
		Sint32 enable;//
		Sint32 quasi; //
		Sint32 ILTune;//
	//#endif
	#ifndef AICUBE
		Sint32 dmhSP;
		float cmdXferRate;
	#endif
	Uint32 *pHCDelta;
	
private:
	#ifndef AICUBE
		SmpEPMode *currentMode; //
		SmpEPCmd  *currentCmd; //
		double simState[2], simInt; //
		float deltaPDerFilterState; //
	#endif
};

#ifdef SUPGEN

class EPSimulationParametersClass: public Parameter {
public:
	FloatPar      ActuatorGain;
	FloatPar      NaturalFrequency;
	FloatPar      DampingRatio;
	FloatArrayPar ModeArrayPos;
	FloatArrayPar ModeArrayNeg;
	FloatPar      NoiseLevel;
	Float         modeGainNorm[EPNMode];
};

class EPSimulationClass: public Parameter {
public:
	IntegerPar                  Enable;
	EPSimulationParametersClass SimulationParameters;
};

class EPFlowLimitClass: public Parameter {
public:
	IntegerPar FlowLimited;
	FloatPar   Level;
};

class EPUACClass: public Parameter {
public:
	EnumPar  Direction;
	FloatPar Gain;
};

class EPInnerLoopClass: public Parameter {
public:
	EnumPar  Enable;
	FloatPar DeltaPGain;
};

class EPModeChangeClass: public Parameter {
public:
	EnumPar    ChangeOnStop;
	IntegerPar TargetMode;
	EnumPar    RelaxSetPoint;
	EnumPar    StartModeAutoset;
	IntegerPar StartMode;
};

class EPCommandControlClass: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpEPCmd &smp);
	void checkCompatible(SignalRange *p);
	IntegerPar Selected;
	IntegerPar RangeSafe;
	IntegerPar Request;
	SignalRange *pCmdRange;
private:
	virtual void parPrimeSignal();
	SmpEPCmd *smpbs;
	Integer comp;
};

class EPCommandSourceClass: public Parameter {
public:
	EPCommandControlClass SetPoint;
	EPCommandControlClass FuncGen;
	EPCommandControlClass External;
	EPCommandControlClass CNet;
};

class EPModeControlClass: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpEPMode &smp);
	TextPar    Name;
	IntegerPar Selected;
	IntegerPar Request;
	char numberBuf[4];
	void *pSimExtCh;
	SmpSigInputChannel *smpPModeFbCh;
	SigInputChannel *pModeFbCh;
	Integer    ilModeFlag;
private:
	virtual void parConfigureSignal();
	SmpEPMode *smpbs;
};

class EPModeClass: public Parameter {
public:
	EPModeControlClass ModeArray[EPNMode];
};

class ErrorPath: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpErrorPath &smp);
	TextPar              Name;
	EnumPar				 Enable;
	EnumPar				 QuasiStatic;
	FloatPar             Error;
	FloatPar             ValveDeflection;
	EPFlowLimitClass	 FlowLimit;
	IntegerPar			 ILTuning;
	IntegerPar			 ServoEnable;
	IntegerPar			 CascadeEnable;
	EPSimulationClass    Simulation;
	EnumPar              TestBiasMode;
	EnumPar              CmdTransferMode;
	EnumPar              DMHAction;
	UnsignedPar          CNetCmdTimeSlot;			
	EPUACClass           UAC;
	EPInnerLoopClass     InnerLoop;
	EPCommandSourceClass CommandSource;
	EPModeChangeClass	 ModeChange;
	EPModeClass          Mode;
	float *pIntCmd;
	float *pSpan;
	float *pSetPointInc;
	float *pCompFb;
	SmpSigInputChannel *pILCh;
	SmpSigInputChannel *pCascadeCh;
	float *pDeltaPFb;
	SmpSigInputChannel *pExtCmdCh;
	float *pCNet;
	typedef float SmpErrorPath::*monitorOffsetType;
	typedef char *monitorStrType;
	static const monitorOffsetType MonitorOffset[];
	static const monitorStrType MonitorStr[];
	Uint32 *pHCDelta;
private:
	virtual void parAlteredEvent(Parameter *sce);
	virtual	void parPrimeSignal();
	virtual	void parConfigureSignal();
	virtual	void parPollSignal();
	void newSimFilter();
	void checkRangeSafety();
	void checkSimMode();
	void updateExtSimPars();
	void updateSimILEn();
	void newCmdTransferRate();
	void newStartModeAutoset();
	SmpErrorPath *smpbs;
	Uint32 *pTsrIn, *pTsrOut;
	Integer *pSimulationEnable;
	Integer *pInnerLoopTuneEnable;
};

#endif
