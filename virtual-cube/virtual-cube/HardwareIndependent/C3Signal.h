// Signal.h - Signal Class header

enum {SigFiltOrd=1,			// (Common) filter order
	  SigNameSz=32,			// Size of name string
	  SigSernoSz=20,		// Size of serial number string
	  SigConSz=20,  		// Size of connector string
	  SigSymSz=8,			// Size of symbol string
	  SigPTFiltOrd=8,		// Filter order for peak/trough
	  SigMaxChans=16,		// Maximum sixteen servo channels
	  SigNotConfigured=0,	// Permissions
	  SigMonitorOnly,
	  SigOuterLoopFeedback,
	  SigInnerLoopFeedback,
	  SigCompensationFeedback,
	  SigExternalCommand,
	  SigDeltaPFeedback,
	  SigAdaptationFeedback,
	  SigCascadeFeedback,
	  SigDisabled=0,		// Able values
	  SigEnabled,
	  SigOuterLoop=0,		// Ext. comp. mode values
	  SigInnerLoop,
	  SigNormal=0,			// Phasing values
	  SigInverted,
	  SigLastNone=0,		// Slack follower last turn 
	  SigLastPeak,
	  SigLastTrough
};

enum {
	SetupPartNone=0,
	SetupPartContr,
	SetupPartMon,
	SetupPartImpl
};

#define	SigGBLPFreq			5.0f			// Gain boost low pass filter
#define	SigGBHPFreq			100.0f			// Gain boost high pass for rate estimation
#define	SigLPFreq			2000.0f			// Low pass for limits
#define	SigPersist			1.0e-3f			// Persistence - 1 msec
#define	SigSPRelax			(1.0f/SysFs)	// Set point ramp rate - relax after mode change (1 sec for full range)
#define	SigSetupRamp		(5.0f/SysFs)	// Set point ramp rate - setup mode load removal
#define	SigIInvFreq			1.0f			// Integrator inverse lower frequency limit
#define	SigMinPG			0.16f			// Term ranges for self tune
#define	SigMaxPG			16.0f
#define	SigMaxIG			(64.0f/SysFs)
#define	SigMaxDG			8.0f
#define	SigMaxSTGain		0.001f			// Self tune gain at maximum setting
#define	SigMinSGPC			1.0f			// Term minima (percent)
#define	SigNormSGPC			3.0f
#define	SigMinPGPC			1.0f
#define	SigNormPGPC			10.0f
#define	SigMinECPGPC     	0.001f
#define	SigPAUpdate			9.2f			// Cyclic delta to filter coeff factor = -ln(one cycle decay)
//#define	SigRampUpdate		0.1f			// Percentage transition between ramp adaptations
//#define	SigRampSettle		0.05f			// Percentage transition for new ramp adjustment to settle
#define SigRampSettleFactor 0.5f			// Ratio of settle time to ramp update time
#define	SigRAFactor			92.1f			// Ramp adjustment speed - delta to filter (92.1 adjusts to 1% in 5% of transition)
#define	SigSPTFCoeff		0.05f			// Set point tracking filter coefficient
#define	SigIntDisInc		(0.25/SysFs)	// Integrate disable increment towards zero (25%/sec)
#define	SigHCTimeOut		0.2				// Seconds after last HC adjustment when set point is set to feedback (in case of run-on)
//#define	SigHCSpeedCoeff	0.01f			// Hand controller speed filter for log law // Fudge
#define	SigNsMeterHPFreq	10.0f			// Noise meter high pass frequency
#define	SigNsMeterLPFreq	0.4f			// Noise meter low pass frequency
//// IDH
//#define CONTROLCUBE 1
//// IDH

// Common parameter record
typedef struct {
	float filterNum[SigFiltOrd+1];
	float filterDen[SigFiltOrd+1];
	float gbHPFilterNum[2];
	float gbHPFilterDen[2];
	float gbLPFilterNum[2];
	float gbLPFilterDen[2];
	Sint32 persistence;
	float gbFac;
	GenGenBus gb;
	float *(pop[SigNCh]);
	Uint32 *pTsrIn;
	float iInvDecay;
	float *pCNet;
	Uint32 *pHCDelta;
} SmpSigCommon;

// Signal threshold limit record
typedef struct {
	float  threshold;
	Sint32 perCnt;
	SmpLimit limit;
} SmpSigThrLimit;

// Signal peak/trough limit record
typedef struct {
	float  threshold;
	Sint32 underPerCnt;
	Sint32 overPerCnt;
	SmpLimit underLimit;
	SmpLimit overLimit;
	Uint32 vioCnt;
} SmpSigPTLimit;

// Error limit record
typedef struct {
	float  threshold;
	Sint32 perCnt;
	SmpLimit limit;
} SmpSigErrLimit;

// Sample processing channel
class SmpSigInputChannel {
public:
	void channelIter()
	{
		register float x;
		float z;
#if defined(CONTROLCUBE) || defined(AICUBE)
		float dummyf;
		register float y;
		register int pkdet, trdet;
#ifdef CONTROLCUBE
		float rampTAdj, rampOAdj;
#endif
		Uint32 i;
#endif

		// Do nothing if not enabled
		if((!enable)||(!pCommon))
		{
			output=reading=0.0f;
			return;
		}

		// Get new input
#ifndef EXTERNALSIMULATION
		if(simTakeOver)
			simTakeOver=false;
		else
#endif
		{
			if(cNetSce)
				z=pCommon->pCNet?(pCommon->pCNet)[cNetSlot]:0.0f;
			else
				z=pInput?*pInput:0.0f;
			output=isnan(z)?0.0f:z;
//			output=0.2f*(float)sin(roobarb);	// This code purely for testing peak adaptation
//			roobarb+=0.0015f;
//			if(roobarb>6.0f)
//				roobarb-=6.0f;
		}

		// Get new filtered signal
#ifdef SIGNALCUBE
		x=reading=output;
#else
		x=reading=Filter(pCommon->filterNum, pCommon->filterDen, filterState, SigFiltOrd, output);
#endif

#if defined(CONTROLCUBE) || defined(AICUBE)
		// Do slack following and peak/trough detection
		pkdet=0;
		trdet=0;
		// x = filtered input
		// slack = peak adaptation hysterysis
		// slackFollower = x +/- slack (PW - 28/07/16)
		//if (pPTAPort){
			//pPTAPort -> trough = 3.1415f;
		//}
		if(pPTAPort && (amplAdapt || fbEn)){
			pPTAPort -> feedback = x;
			if(amplAdapt){ // change to seperate flags for amplude adaptation
				pPTAPort -> peakCtrlCnt += 1;
				pPTAPort -> peakDes = peakDes;
				pPTAPort -> troughDes = troughDes;
			}
		}
		/*if(pPTAPort && selFlag && ){ // if this signal is selected and TSRflag for stop is set...
			//..then send FB to generator.
		}*/
		if( useGlobalDetection && pGb)
		{
			// Use the generator theta (sawtooth) and dynamic fade signals to enable and disable peak adaptation
			if( pGb->xcode == GenCycling && pGb->fade == 1.0)
			{
				switch( peakState)
				{
					case 0:
						// Initialisation phase 
						oldTheta = pGb->theta;
						peakState = 1;
						completedCycles = 0;
						activeCycles = 0;
						break;

					case 1: 
						// Waiting for first cycle to start
						if(pGb->theta < oldTheta)
						{
							currPeak = x;
							currTrough = x;
							peakState = 2; 
						}
						oldTheta = pGb->theta;
						break;
					case 2:
						// Now acquiring samples and measuring the peak and troughs
						if(pGb->theta < oldTheta)
						{
							if( ++completedCycles < delayPACycles)
							{
								activeCycles = 0;
								troughMeas = currTrough;
								peakMeas = currPeak;
							}
							else
							{
								activeCycles = 1;
								ptFilter(troughMeas, currTrough);
								ptFilter( peakMeas, currPeak);
								if (pPTAPort && amplAdapt)
								{
									if(amplAdapt)
									{
										pPTAPort->trough = troughMeas;
										pPTAPort->peak 	 = peakMeas;
									}
								}
								cycleComplete = 1;
								pkdet=1;
								trdet=1;
							}
							
							currPeak = x;
							currTrough = x;
							//peakState = 2; 
						}
						else
						{
							if( x > currPeak)
							{
								currPeak = x;
								// Measure phase at the peak
								peakTheta=pGb?pGb->theta:0.0f;
								if(pHarmonic&&*pHarmonic)
									peakTheta=modff((*pHarmonic)*peakTheta, &dummyf);
							}
							else if( x < currTrough )
							{
								currTrough = x;
							}
							cycleComplete = 0;
						}  
						oldTheta = pGb->theta;
						break;
				}
			}
			else
			{
				peakState = 0;
				activeCycles = 0;
			}
			// Ensure that the delta values are zeroed

			amplDelta = medianDelta = phaseDelta = 0.0f;

			// Do peak adaptation?

			if (cycleComplete && activeCycles)
			{
				adapt();
				if (countMode == 2)
					countCycles++;
			}
		}
		else
		{
			if(x>(slackFollower+slack))
				{
				if(lastTurn==SigLastPeak)
				{
					ptFilter(troughMeas, slackFollower-slack);
					if (pPTAPort && amplAdapt){
						if(amplAdapt){
							pPTAPort -> trough = troughMeas;
						}
						//pPTAPort -> rateAdaptEn = medianAdapt;
					}
					y=phaseWrap(1.5f-2.0f*troughTheta);
					phaseMeas=phaseWrap(phaseMeas+(1.0f-ptaFilter)*phaseWrap(y-phaseMeas));
					trdet=1; // trough detected.
				}
				slackFollower=x-slack;
				peakTheta=pGb?pGb->theta:0.0f;
				if(pHarmonic&&*pHarmonic)
					peakTheta=modff((*pHarmonic)*peakTheta, &dummyf);
				lastTurn=SigLastTrough;
				}
				pkdet=0;
				if(x<(slackFollower-slack))
				{
					if(lastTurn==SigLastTrough)
					{
						ptFilter(peakMeas, slackFollower+slack);
						if (pPTAPort && amplAdapt){
							if(amplAdapt){
								pPTAPort -> peak = peakMeas;
								//periodMeas = /* new timestamp */ - /* previous timestamp */;
								/*	
									set pCnetTsLo from ControlCube.cpp like function generator does.
									use those pointers to get timestamp
									calculate periodMeas;
								*/
							}
							//pPTAPort -> rateAdaptEn = medianAdapt;
						}
						y=phaseWrap(0.5f-2.0f*peakTheta);
						phaseMeas=phaseWrap(phaseMeas+(1.0f-ptaFilter)*phaseWrap(y-phaseMeas));
						pkdet=1;
					}
					slackFollower=x+slack;
					troughTheta=pGb?pGb->theta:0.0f;
					if(pHarmonic&&*pHarmonic)
						troughTheta=modff((*pHarmonic)*troughTheta, &dummyf);
					lastTurn=SigLastPeak;
				}
				

			// Do peak adaptation

			z=norespDel+norespPer;
			if(pGb&&((pGb->xcode==GenCycling)||(pGb->xcode==GenExtCmd))&&(pGb->fade==1.0f))
			{
				if(pkdet)
					peaksSeen++;
				if(trdet)
					troughsSeen++;
				activeCycles=((peaksSeen>0)&&(troughsSeen>0))?1:0;
				if(pkdet||trdet)
					norespCount=0.0f;
				else if(norespCount<z)
					norespCount+=pGb->delta;
				i=creepEn&&(pGb->xcode==GenCycling)?1:0;
				norespTimeout=(i&&(norespCount>=z))?1:0;
				norespActive=(i&&(norespCount>=norespDel)&&!norespTimeout)?1:0;
			}
			else
			{
				norespCount=0.0f;
				norespTimeout=0;
				norespActive=0;
				activeCycles=0;
				peaksSeen=0;
				troughsSeen=0;
				amplAch=0;
				meanAch=0;
			}
			amplDelta=medianDelta=phaseDelta=0.0f;
			i=((activeCycles&&meanAch&&amplAch)||countWhen)?1:0;
			if(trdet)
			{
				if(activeCycles)
					adapt();
				if(i&&(countMode==2))
					countCycles++;
			}
			if(pkdet)
			{
				if(activeCycles)
					adapt();
				if(i&&(countMode==1))
					countCycles++;
			}
			if(norespActive)
			{
				peakMeas=troughMeas=x;
				if(amplAdapt&&(meanAch||!meanFirst))
					amplDelta=0.5f*pGb->delta*amplCR*(peakDes-troughDes);
				if(medianAdapt)
				{
					switch(medianMode)
					{
					case 1:
						z=peakDes;
						break;
					case 2:
						z=troughDes;
						break;
					default:
						z=0.5f*(peakDes+troughDes);
						break;
					}
					z=creepBand*(z-x);
					Clamp(z, -1.0f, 1.0f);
					medianDelta=meanCreepFac*meanCR*z;
				}
			}
		}	


		countComplete.iter();
		countComplete.update((countRequired>0L)&&(countCycles>=countRequired), 0.0f);
		norespLimit.iter();
		norespLimit.update(norespTimeout, 0.0f);
#endif

		// Service threshold limits
		upperLimitIter(x, upperOuter);
		upperLimitIter(x, upperInner);
		lowerLimitIter(x, lowerInner);
		lowerLimitIter(x, lowerOuter);

#ifdef CONTROLCUBE
		// Following error limits (iteration only - threshold/state determined in ErrorPath.cpp)
		errorOuter.limit.iter();
		errorInner.limit.iter();

		// Peak limits
		ptLimitIter(peakMeas, peakDes, (pkdet&&activeCycles)?1:0, peakOuter);
		ptLimitIter(peakMeas, peakDes, (pkdet&&activeCycles)?1:0, peakInner);

		// Trough limits
		ptLimitIter(troughMeas, troughDes, (trdet&&activeCycles)?1:0, troughOuter);
		ptLimitIter(troughMeas, troughDes, (trdet&&activeCycles)?1:0, troughInner);

		// Violation limit
		i=0;
		if(vioMode&1)
			i+=peakInner.vioCnt;
		if(vioMode&2)
			i+=troughInner.vioCnt;
		vioLimit.iter();
		vioLimit.update(((vioMode>0)&&(i>=vioMax)), 0.0f);

		// Ramp adaptation and limits
		pkdet=0;
		rampOAdj=rampTAdj=0.0f;
		if(pGb&&(pGb->xcode==GenTransitioning))
		{
			if(pPTAPort){
				rampStart = pPTAPort->start;
				rampEnd = pPTAPort->end;
			}
			z=pGb->fade;
			z=((1.0f-z)*rampStart)+(z*rampEnd);
			rampDes=z;
			rampMeas=x;
			z-=x;
			errorLimitIter(z, rampInner);
			errorLimitIter(z, rampOuter);
			if(rampEnable&&rampEnteredFlag)
			{
				y=pGb->fade-rampEnterFade;
				if(y>=(rampUpdateFraction * SigRampSettleFactor))
				{
					rampx+=pGb->fade;
					rampy+=x;
					rampxx+=pGb->fade*pGb->fade;
					rampxy+=x*pGb->fade;
					rampn+=1.0f;
					pkdet=1;
				}
				else
					rampx=rampy=rampxx=rampxy=rampn=0.0f;
				if(y>=rampUpdateFraction)
				{
					y=1.0f/rampn;
					rampx*=y;
					rampy*=y;
					rampxx*=y;
					rampxy*=y;
					y=rampxx-rampx*rampx;
					if(y>0.0f)
					{
						y=1.0f/y;
						rampOAdj=rampStart-y*(rampy*rampxx-rampx*rampxy);
						rampTAdj=rampEnd-y*(rampy*(rampxx-rampx)+(1-rampx)*rampxy);
					}
					rampOAdj*=rampAdaptGain;
					rampTAdj*=rampAdaptGain;
					rampEnterFade=pGb->fade;
				}
			}
			else
			{
				rampEnteredFlag=1;
				rampEnterFade=pGb->fade;
			}
		}
		else
		{
			rampEnteredFlag=0;
			errorLimitIter(0.0f, rampInner);
			errorLimitIter(0.0f, rampOuter);
		}
		rampActive=pkdet;
#endif

#if defined(CONTROLCUBE) || defined(AICUBE)
		// Update PTA port
		if(pPTAPort)
		{
			if(pGb&&(pGb->xcode==GenCycling)&&(pGb->fade>0.0f))
				y=SigPAUpdate*pGb->delta;
			else
				y=1.0f;
			amplFilt+=y*(amplDelta-amplFilt);
			if(amplAdapt)
				pPTAPort->amplitudeAdjust=amplFilt;
			medianFilt+=y*(medianDelta-medianFilt);
			if(medianAdapt)
				pPTAPort->medianAdjust=medianFilt;
			phaseFilt+=y*(phaseDelta-phaseFilt);
			if(phaseAdapt)
				pPTAPort->phaseAdjust=phaseFilt;
#ifdef CONTROLCUBE
			if (rampEnable)
			{
				if(pGb&&(pGb->xcode==GenTransitioning))
				{
					prevRampEnable = 1;
					y=SigRAFactor*pGb->delta;
				}
				else
					y=1.0f;
				rampOFilt+=y*(rampOAdj-rampOFilt);
				pPTAPort->originAdjust=rampOFilt;
				rampTFilt+=y*(rampTAdj-rampTFilt);
				pPTAPort->targetAdjust=rampTFilt;
			}
			if ((prevRampEnable)&&!(rampEnable)){ //if ramp adaption was enabled...
				pPTAPort->originAdjust=0.0f;
				pPTAPort->targetAdjust=0.0f;
				prevRampEnable = 0;
			}
#endif
			if(countMode)
				pPTAPort->cyclesComplete=countCycles;
		}
#endif
		if(unanimity&&(x<lowerInner.threshold)&&(lowerInner.limit.pTsrOut))
			(*(lowerInner.limit.pTsrOut))|=TSRUnanimity;

		// Noise meter
		x=DeltaFilter(nsMeterHPFilterNum, nsMeterHPFilterDen, &nsMeterHPFilterState, 1, x);
		msNoise=DeltaFilter(nsMeterLPFilterNum, nsMeterLPFilterDen, &nsMeterLPFilterState, 1, x*x);
	}
#ifdef CONTROLCUBE
#ifdef EXTERNALSIMULATION
	float loopIter(float cmd, float spFade, float *pCmd, float *pFb, float *pError, float ecfb, Sint32 rampSp, float *pSTM, float spinc, Sint32 intAlways,
		SmpSigInputChannel *pCasCh);
	float spTrackIter(float olop, float ecfb, SmpSigInputChannel *pCasCh);
#else
	float loopIter(float cmd, float spFade, float *pCmd, float *pFb, float *pError, float ecfb, Sint32 sim, float simFb, Sint32 rampSp, float *pSTM, float spinc, Sint32 intAlways,
		SmpSigInputChannel *pCasCh);
	float spTrackIter(float olop, float ecfb, Sint32 sim, float simFb, SmpSigInputChannel *pCasCh);
#endif
	float ilIter(float cmd);
	float selfTune(float cmd, float fb);
#endif
	SmpSigCommon *pCommon;
	Sint32 enable;
	float *pInput;
	float output;
#ifndef EXTERNALSIMULATION
	bool  simTakeOver;
#endif
//	float roobarb;	// For testing peak adaptation
	float reading;
	Sint32 fbEn;
	float setPnt;
	float filterState[SigFiltOrd];
	float slackFollower;
	float slack;
	//float slackUSD;
	//float slackCubus;
	Sint32 slackFlag;
	float peakTheta;
	float troughTheta;
	Sint32 amplAdapt;
	Sint32 phaseAdapt;
	Sint32 medianAdapt, medianMode;
	Sint32 lastTurn;
	Sint32 casen;
	#ifndef AICUBE
		Sint32 ecEn;
		float ecPropGain;
		float ecDerGain;
		Sint32 ecMode;
		Sint32 ecPhasing;
		float servoGain;
		float propGain;
		float intGain;
		Sint32 intEn;
		Sint32 intOn;
		float derGain;
		float gainBoost;
		float errFilterNum[2];
		float errFilterDen[2];
		float ecDerFilterNum[2];
		float ecDerFilterDen[2];
		float derFilterNum[2];
		float derFilterDen[2];
		float errFilterState;
		float ecDerFilterState;
		float derFilterState;
		float gbHPFilterState;
		float gbLPFilterState;
		Sint32   errfiltEnable;
		double intCap;
		float intReading;
	#endif
	float peakDes;
	float troughDes;
	float phaseDes;
	float peakMeas;
	float troughMeas;
	float phaseMeas;
	//float periodMeas;
	float amplDelta;
	float medianDelta;
	float phaseDelta;
	float amplFilt;
	float medianFilt;
	float phaseFilt;
	float ptaFilter;
	float ptaGain;
	#ifndef AICUBE
		Sint32 stEnable;
		float stErrThrsh;
		Sint32 stAct;
		float stGain;
		Sint32 stPrd;
		Sint32 stCnt;
		float derInvNum[2];
		float derInvState;
		float dInvFb;
		float iInvFb;
		float stModelNum[3];
		float stModelDen[3];
		float stModelState1[2];
		float stModelState2[2];
	#endif
	float rampStart;
	float rampEnd;
	float rampDes;
	float rampMeas;
	Sint32 rampEnable;
	Sint32 prevRampEnable;
	Sint32 rampActive;
	Sint32 rampEnteredFlag;
	float rampEnterFade;
	float rampAdaptGain;
	float rampUpdateFraction;
	float rampx, rampxx, rampy, rampxy, rampn;
#ifdef CONTROLCUBE
	float rampOFilt, rampTFilt;
#endif
	GenPTAPort *pPTAPort;
	SmpSigThrLimit upperOuter;
	SmpSigThrLimit upperInner;
	SmpSigThrLimit lowerInner;
	SmpSigThrLimit lowerOuter;
	SmpSigPTLimit peakOuter;
	SmpSigPTLimit peakInner;
	SmpSigPTLimit troughOuter;
	SmpSigPTLimit troughInner;
	#ifndef AICUBE
		SmpSigErrLimit errorOuter;
		SmpSigErrLimit errorInner;
	#endif
	SmpSigErrLimit rampOuter;
	SmpSigErrLimit rampInner;
	Sint32 realTxFlag;
	GenGenBus *pGb;
	float *pSetupModeLoadFb;
	float llGain;
	float llMax;
	Sint32 cNetSce;
	Uint32 cNetSlot;
	float trackingError;
	float cmdPosClamp, cmdNegClamp;
	float relaxTar;
	float relaxRate;
	Sint32 HCEn;
	float HCSpeed, HCMeanSpeed, HCLogLaw, SigHCSpeedCoeff; // Fudge
	Sint32 unanimity;
	Sint32 ownChan;
	Sint32 countMode, countWhen;
	Uint32 countCycles;
	Uint32 countRequired;
	SmpLimit countComplete;
	float norespDel, norespPer, norespCount;
	Sint32 norespActive;
	Sint32 norespTimeout;
	Sint32 creepEn, amplAch, meanAch, meanFirst;
	float meanCR, amplCR, meanCreepFac, creepBand;
	SmpLimit norespLimit;
	Sint32 vioMode;
	Uint32 vioMax;
	SmpLimit vioLimit;
	float nsMeterHPFilterNum[2], nsMeterHPFilterDen[2];
	float nsMeterLPFilterNum[2], nsMeterLPFilterDen[2];
	double nsMeterHPFilterState, nsMeterLPFilterState;
	float msNoise;
	Sint32 activeCycles;
	float *pHarmonic;
	double casIntCap;
	float casDerFilterState;
	Sint32 useGlobalDetection;
	Sint32 peakState;
	float oldTheta;
 	Sint32 delayPACycles ;
	Uint32 completedCycles;
	float currPeak;
	float currTrough;
	Sint32 cycleComplete ;
	float meanTheta;
private:
#ifdef INLINE_LIMIT_FUNCS
	void upperLimitIter(float v, SmpSigThrLimit &p)
	{
		p.limit.iter();
		if(v>p.threshold)
		{
			p.perCnt++;
			if(p.perCnt>pCommon->persistence)
				p.perCnt=pCommon->persistence;
		}
		else
			p.perCnt=0;
		p.limit.update((Uint32)(p.perCnt==pCommon->persistence), v-p.threshold);
	}
	void lowerLimitIter(float v, SmpSigThrLimit &p)
	{
		p.limit.iter();
		if(v<p.threshold)
		{
			p.perCnt++;
			if(p.perCnt>pCommon->persistence)
				p.perCnt=pCommon->persistence;
		}
		else
			p.perCnt=0;
		p.limit.update((Uint32)(p.perCnt==pCommon->persistence), v-p.threshold);
	}
	void ptLimitIter(float v, float target, int det, SmpSigPTLimit &p)
	{
		bool ptActive, underState, overState;

		p.underLimit.iter();
		p.overLimit.iter();
		if(activeCycles)
		{
//			  ptActive=meanAch&&amplAch;
			
			/* If global detection is enabled, then ignore mean and amplitude achieved flags */

			ptActive = useGlobalDetection || (meanAch&&amplAch);

			if(det)
			{
				underState=ptActive&&(v<(target-p.threshold));
				overState=ptActive&&(v>(target+p.threshold));
				p.underLimit.update((Uint32)underState, v-(target-p.threshold));
				p.overLimit.update ((Uint32)overState, v-(target+p.threshold));
				if(underState||overState)
					p.vioCnt++;
			}
		}
		else
		{
			// Clear States if not generating
			p.underLimit.update(0, 0.0f);
			p.overLimit.update(0, 0.0f);
		}
	}
	void errorLimitIter(float v, SmpSigErrLimit &p)
	{
		// Note iter() call moved to transducer channel processing
		if(fabs(v)>p.threshold)
		{
			p.perCnt++;
			if(p.perCnt>pCommon->persistence)
			p.perCnt=pCommon->persistence;
		}
		else
			p.perCnt=0;
		p.limit.update((Uint32)(p.perCnt==pCommon->persistence), (v>=0.0f)?v-p.threshold:v+p.threshold);
	}
#else
	void upperLimitIter(float v, SmpSigThrLimit &p);
	void lowerLimitIter(float v, SmpSigThrLimit &p);
	void ptLimitIter(float v, float target, int det, SmpSigPTLimit &p);
	void errorLimitIter(float v, SmpSigErrLimit &p);
#endif
	void ptFilter(float &state, float val)
	{
		state=ptaFilter*state+(1.0f-ptaFilter)*val;
	}
	void adapt()
	{
		register float x;

		if(amplAdapt)
			amplDelta=0.5f*ptaGain*(peakDes-troughDes-peakMeas+troughMeas);
		if(medianAdapt)
			switch(medianMode)
			{
			case 1:
				medianDelta=ptaGain*(peakDes-peakMeas);
				break;
			case 2:
				medianDelta=ptaGain*(troughDes-troughMeas);
				break;
			default:
				medianDelta=0.5f*ptaGain*(peakDes+troughDes-peakMeas-troughMeas);
				break;
			}
		if(phaseAdapt)
		{
			x=phaseWrap(phaseDes-phaseMeas);
			phaseDelta=ptaGain*x;
		}
	}
	float phaseWrap(float x)
	{
		while(x<=-1.0f)
			x+=2.0f;
		while(x>1.0f)
			x-=2.0f;
		return x;
	}
	Uint32 lastHCDelta, HCAdjustTimeOut;
	Uint32 lastSpFreeze;
	Sint32 HCExcess;
	Uint32 peaksSeen, troughsSeen;
};

class SmpSignal {
public:
	void iterBegin()
	{
		if(nChans)
			//memset(ptaPort, 0, nChans*sizeof(GenPTAPort));
			for (int i=0;i>=nChans;i++){
				memset((ptaPort+i*sizeof(GenPTAPort)),0,28);
			}
		if(pGb)
		{
			common.gb.xcode=pGb->xcode;
			common.gb.fade=pGb->fade;
			common.gb.theta=pGb->theta;
			common.gb.delta=pGb->delta;
		}
		else
		{
			common.gb.xcode=GenAtBaseLine;
			common.gb.fade=0.0f;
			common.gb.theta=0.0f;
			common.gb.delta=0.0f;
		}
	}
	void iterChans(int bs, int cnt)
	{
		SmpSigInputChannel *p;
		int i;

		p=channel+bs;
		for(i=cnt; i--; p++)
			p->channelIter();
	}
	// Supervisory comms:
	SmpSigInputChannel channel[SigNCh];
	SmpSigCommon common;
	Sint32 nChans;
	GenPTAPort ptaPort[SigMaxChans];
	GenGenBus *pGb;
};

#ifdef SUPGEN

class SigSourceClass: public Parameter {
public:
	EnumPar     Source;
	UnsignedPar CNetSlot;
};

class SigThresholdLimitClass: public Parameter {
public:
	FloatPar  Threshold;
	Limit     SICLimit;
};

class SigErrorLimitClass: public Parameter {
public:
	FloatPar  Threshold;
	Limit     SICLimit;
};

class SigPeakTroughLimitClass: public Parameter {
public:
	FloatPar    Threshold;
	Limit       UnderLimit;
	Limit       OverLimit;
	UnsignedPar Counter;
};

class SigPeakViolationLimitClass: public Parameter {
public:
	EnumPar     Mode;
	UnsignedPar	Maximum;
	Limit       Excess;
};

class SigExternalCompClass: public Parameter {
public:
	EnumPar   Enable;
	EnumPar   Mode;
	EnumPar   Phasing;
	FloatPar  ProportionalGain;
	FloatPar  DerivativeGain;
	FloatPar  DerivativePlateau;
};

class SigSelfTuneClass: public Parameter {
public:
	EnumPar    Enable;
	FloatPar   ErrorThreshold;
	FloatPar   AdaptationPeriod;
	IntegerPar Active;
	FloatPar   ModelBandwidth;
 	FloatPar   ModelDamping;
	FloatPar   AdaptationGain;
};

class SigTermsClass: public Parameter {
public:
	EnumPar			 ErrorFilterEnable;
	FloatPar		 ErrorFilter;
	FloatPar		 ServoGain;
	FloatPar		 ProportionalGain;
	FloatPar		 IntegralGain;
	FloatPar         IntegratorReading;
	EnumPar			 IntegratorEnable;
	IntegerPar		 IntegratorOn;
	FloatPar		 DerivativeGain;
	FloatPar		 GainBoost;
	FloatPar		 DerivativePlateau;
	IntegerPar       LLAutoSet;
	SigSelfTuneClass SelfTune;
};

class SigThresholdLimitsClass: public Parameter {
public:
	SigThresholdLimitClass UpperOuter;
	SigThresholdLimitClass UpperInner;
	SigThresholdLimitClass LowerInner;
	SigThresholdLimitClass LowerOuter;
};

class SigPeakParamsClass: public Parameter {
public:
	FloatPar Amplitude;
	FloatPar Mean;
	FloatPar Peak;
	FloatPar Trough;
	FloatPar Phase;
};

/*class SigPeakParamsClassD: public Parameter {
public:
	FloatPar Amplitude;
	FloatPar Mean;
	FloatPar Peak;
	FloatPar Trough;
	FloatPar Phase;
};

class SigPeakParamsClassM: public Parameter {
public:
	FloatPar Amplitude;
	FloatPar Mean;
	FloatPar Peak;
	FloatPar Trough;
	FloatPar Phase;
	//FloatPar Period;
};*/

class SigPeakTermsClass: public Parameter {
public:
	FloatPar   Filter;
	FloatPar   Gain;
	FloatPar   Hysteresis;
	EnumPar    GlobalDetection;
	IntegerPar DelayCycles;
	//FloatPar   HysteresisUSD;
	//EnumPar    SlackFlag;
	IntegerPar AmplitudeTooLow;
};

class SigPeakEnablesClass: public Parameter {
public:
	EnumPar Amplitude;
	EnumPar Phase;
	EnumPar Mean;
	EnumPar MeanMode;
};

class SigPeakAchievedClass: public Parameter {
public:
	IntegerPar Amplitude;
	IntegerPar Mean;
	IntegerPar ActiveCycles;
	FloatPar   Tolerance;
};

class SigPeakCounterClass: public Parameter {
public:
	EnumPar     Mode;
	EnumPar     When;
	UnsignedPar Cycles;
	UnsignedPar Required;
	Limit       CompleteLimit;
};

class SigPeakNorespClass: public Parameter {
public:
	FloatPar   Delay;
	FloatPar   Period;
	EnumPar    CreepEn;
	EnumPar    Order;
	FloatPar   MeanCreep;
	FloatPar   MeanBand;
	FloatPar   AmplCreep;
	IntegerPar Active;
	Limit      NorespLimit;
};

class SigPeakAdaptationClass: public Parameter {
public:
	SigPeakParamsClass   Desired;
	SigPeakTermsClass    Terms;
	SigPeakParamsClass   Measured;
	SigPeakEnablesClass  Enables;
	SigPeakAchievedClass Achieved;
	SigPeakCounterClass  Counter;
	SigPeakNorespClass   Noresp;
};

class SigRampAdaptationClass: public Parameter {
public:
	FloatPar   Start;
	FloatPar   End;
	FloatPar   Desired;
	FloatPar   Measured;
	EnumPar    Enable;
	IntegerPar Active;
	FloatPar   Gain;
	FloatPar   UpdateFraction;
};

class SigPeakTroughLimitsClass: public Parameter {
public:
	SigPeakTroughLimitClass    PeakInner;
	SigPeakTroughLimitClass    PeakOuter;
	SigPeakTroughLimitClass    TroughInner;
	SigPeakTroughLimitClass    TroughOuter;
	SigPeakViolationLimitClass Violations;
};

class SigErrorLimitsClass: public Parameter {
public:
	SigErrorLimitClass Outer;
	SigErrorLimitClass Inner;
};

class SigSetupParametersClass: public Parameter {
public:
	FloatPar MaxLoad;
	FloatPar ControlGain;
};

class SigCommandClampClass: public Parameter {
public:
	FloatPar Upper;
	FloatPar Lower;
};

class SigHCDeltaClass: public Parameter {
public:
	EnumPar  Enable;
	FloatPar Speed;
	FloatPar MeanSpeed;
	FloatPar LogLaw;
	FloatPar Coeff;
};

class SigSPSettingsClass: public Parameter {
public:
	FloatPar        SPAdjRate;
	FloatPar        RelaxTarget;
	FloatPar		RelaxRate;
	SigHCDeltaClass HCDelta;
	FloatPar		LargeIncrement;
	FloatPar		SmallIncrement;
};

class SigInputChannel: public Parameter {
public:
	void operator()(Parameter *par, int nCh, int ind, SmpSigInputChannel &smp);
	TextPar                  Name;
	TextPar					 SerialNumber;
	TextPar					 Connector;
	TextPar                  Symbol;
	SigSourceClass           Source;
	EnumPar                  FBSelected;
	SignalRange				 Range;
	FloatPar                 Reading;
	FloatPar                 Noise;
	EnumPar                  Enable;
	EnumPar                  Permission;
	IntegerPar				 OwningChannel;
	EnumPar                  SetupParticipation;
	EnumPar					 CascadeEnable;
	FloatPar                 SetPoint;
	SigExternalCompClass     ExternalCompensation;
	SigTermsClass            Terms;
	SigThresholdLimitsClass  ThresholdLimits;
	SigPeakAdaptationClass   PeakAdaptation;
	SigRampAdaptationClass   RampAdaptation;
	SigPeakTroughLimitsClass PeakTroughLimits;
	SigErrorLimitsClass      ErrorLimits;
	SigErrorLimitsClass      RampLimits;
	SigSetupParametersClass  SetupMode;
	SigCommandClampClass	 CommandClamp;
	SigSPSettingsClass       SPSettings;
	EnumPar                  Unanimity;
	float *pInput;
	void *pExtCh;
	Sint32 realTxFlag;
	SigInputChannel *pLoadCh, *pDispCh, *pSetupCh;
	void setSetupModeLoadPointer(float *p);
	void setCmdClamp(float neg, float pos);
	virtual void parRangeChangedEvent(Parameter *sce);
	virtual void parSetEvent(Parameter *sce);
	void doLLAutoSet();
	int chandefIndex;
	bool isControlFeedback();
	float *pHarmonic;
private:
	virtual void parAlteredEvent(Parameter *sce);
	virtual void parChangedEvent(Parameter *sce);
	virtual void parPrimeSignal();
	virtual void parPollSignal();
	virtual void parConfigureSignal();
	void newRange();
	void newErrorFilterFrequency();
	void newECDerivativePlateauFrequency();
	void newDerivativePlateauFrequency();
	void newDesiredPeak();
	void newDesiredTrough();
	void newDesiredPhase();
	void newPTAFilter();
	void newSelfTuneModel();
	void newSelfTunePeriod();
	void newTermSettings();
	void informSGNofRangeChange();
	void updateMeasuredAmplMn();
	void updateDesiredAmplMn();
	void updateDesiredPkTr();
	void checkForLowPTAmplitude()
	{
		PeakAdaptation.Terms.AmplitudeTooLow=(PeakAdaptation.Desired.Amplitude()<(0.55f*PeakAdaptation.Terms.Hysteresis()))?1:0;
	}
	void checkPTAchievement();
	void newPermission();
	void newMeanBand();
	SmpSigInputChannel *smpbs;
	char myName[4];
};

class SignalArray: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpSignal &smp, int nCh);
	SigInputChannel Channel[SigNCh];
	int FindSignal(Integer Perm, Integer start, Integer owner);
	float **pop;
	GenGenBus *pGb;
	int FindSignalByName(char *s, int n);
	int FindSignalBySymbol(char *s, int n);
	float *pCNet;
	Uint32 *pHCDelta;
private:
	virtual	void parPrimeSignal();
	virtual void parConfigureSignal();
	void initGainBoostFilter();
	SmpSignal *smpbs;
	double num[2], den[2];
	Uint32 *pTsrIn;
	Sint32 nChans;
};

#endif
