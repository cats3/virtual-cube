// Parameter.h - Parameter Class header

#ifdef SUPGEN

#define	ParGoodMagic	0xb1e55edb		// Blessed be!

enum {ParOkay=0,			// Result codes
	  ParNotSettable,
	  ParOverRange,
	  ParUnderRange,
	  ParNan,
	  ParNotFound,
	  ParBufferError,
	  ParSp_WT0=0,			// Special function codes
	  ParSp_WT1,
	  ParSp_WT2,
	  ParSp_WT3,
	  ParSp_Set_PrgrmSP,
	  ParSp_Set_Prgrm,
	  ParSp_Get_Prgrm,
	  ParSp_Set_SNV_SP=65536,
	  ParSp_Set_SNV,
	  ParSp_Get_SNV,
	  ParSp_Set_DNV_SP,
	  ParSp_Set_DNV,
	  ParSp_Get_DNV,
	  ParSp_Tell,
	  ParSp_Suspend,
	  ParSp_Resume,
	  ParUnitsBufSz=16,
	  ParDescentSz=32
};

class CubicEngine;

class Parameter
{
public:
	static Parameter *FindPath(Parameter *r, char *p);
	void InstancePath(Parameter *p, char *s, int c);
	static char *GetResultStr(Result r);
	static void broadcastRange(Parameter *p, Float fs, Integer sc, char *units);
	void operator()(Parameter *par, char *n);
	Parameter *operator()();
	char *getName(){return nameStr;}
	char *getFlags(){return flagStr;}
	char *getUnits(){return unitsBuf;}
	Parameter *getNext(){return listNext;}
	Parameter *getSubList(){return subList;}
	Parameter *getParent(){return parent;}
	bool isGood(){return magic==ParGoodMagic;}
	void adoptMe(Parameter *p);
	void setFlags(char *f);
	void setUnits(char *u)
	{
		if(u)
		{
			strncpy(unitsBuf, u, ParUnitsBufSz);
			unitsBuf[ParUnitsBufSz-1]=0;
		}
		else
			unitsBuf[0]=0;
	}
	Result getUnits(void *buf, int sz)
	{
		strncpy((char *)buf, unitsBuf, sz);
		return ParOkay;
	}
#if defined(SIGNALCUBE) || defined(AICUBE)

	/* Signal Cube does not have battery-backed RAM, so remove any
	   'i' flags that may be present.
	   AI Cube doesn't perform regular updates to 'i' parameters
	   during a test, so they can be treated as SNV. So remove
	   any 'i' flags that may be present.
	*/

	Uint32 flag(char c){return flags&~(1<<('i'&31))&(1<<(c&31));}
#else
	Uint32 flag(char c){return flags&(1<<(c&31));}
#endif
	void abortAdjustment();
	virtual char getType();
	virtual Result getMin(void *buf, int sz, unsigned int msk);		// NB In all cases, sz is assumed to
	virtual Result getMax(void *buf, int sz);						//  be four minimum. sz is only tested
	virtual Result setValue(void *buf, int sz, unsigned int msk);	//  when it is expected to be five or
	virtual Result getValue(void *buf, int sz);						//  greater.
	virtual Result getLive(void *buf, int sz);						//
	virtual Result getAddr(void *buf, int sz);
	virtual Result setInstant(void *buf, int sz, unsigned int msk);
	virtual char *operator[](int n);
	virtual Result special(int fn, void *in, int incnt, void *out, int outcnt);
	virtual void *getBsSz(Uint32 &sz);
	virtual Uint32 sizeSNV(Uint32 s);
	virtual Uint32 sizeDNV(Uint32 s);
	virtual void *formatSNV(void *p);
	virtual void *formatDNV(void *p);
	virtual Uint32 getViolation();
	virtual void parSNVValidateUREvent(Parameter *sce);
	virtual void parSNVValidateOREvent(Parameter *sce);
	virtual void parSNVValidateNANEvent(Parameter *sce);
	virtual void parDNVValidateUREvent(Parameter *sce);
	virtual void parDNVValidateOREvent(Parameter *sce);
	virtual void parDNVValidateNANEvent(Parameter *sce);
	virtual void parSetROEvent(Parameter *sce);
	virtual void parSetUREvent(Parameter *sce);
	virtual void parSetOREvent(Parameter *sce);
	virtual void parSetNanEvent(Parameter *sce);
	virtual void parSetLockEvent(Parameter *sce);
	virtual void parSNVModifiedEvent(Parameter *sce);
	virtual void parDNVModifiedEvent(Parameter *sce);
	virtual void parManagedSetEvent(Parameter *sce);
	virtual void parSetEvent(Parameter *sce);
	virtual void parAlteredEvent(Parameter *sce);
	virtual void parChangedEvent(Parameter *sce);
	virtual void parAdjFailEvent(Parameter *sce);
	virtual void parRangeChangedEvent(Parameter *sce);
	virtual void parConfigChangedEvent(Parameter *sce);
	virtual void parIdentifierChangedEvent(Parameter *sce);
	virtual void parCompileEvent(Parameter *sce);
	virtual void parDACChangeEvent(Parameter *sce);
	virtual void parClampChangedEvent(Parameter *sce);
	virtual void parValidateSignal();
	virtual void parRevalidateSignal();
	virtual void parPrimeSignal();
	virtual void parPollSignal();
	virtual void parCorruptSignal();
	virtual void parNormaliseSignal();
	virtual void parConfigureSignal();
	virtual void parRecompileSignal();
	virtual Result Load(void *p);
	virtual Uint32 fastRdRaw();

#ifdef FASTPOLL
	virtual void fastPoll();
	Parameter *fastChangePollNext;
#endif
	Result setEvent(int eHandle, int pri);
	void clearEventFlags(int flgs);
	char *nameStr;
	static void eventPoll(int msk);
#ifdef PCTEST
	static void eventQDump();
#endif
	static bool blockCopy(Parameter *p, Parameter *q);
protected:
	Result operator()(char *v);
	void scanFlags();
	void descendSignal(void (Parameter::*fn)())
	{
		Parameter *p=subList;
		while(p)
		{
			(p->*fn)();
			p=p->getNext();
		}
	}
	void valUREvent()
	{
		if(flag('d'))
			parDNVValidateUREvent(this);
		else
			parSNVValidateUREvent(this);
	}
	void valOREvent()
	{
		if(flag('d'))
			parDNVValidateOREvent(this);
		else
			parSNVValidateOREvent(this);
	}
	void valNANEvent()
	{
		if(flag('d'))
			parDNVValidateNANEvent(this);
		else
			parSNVValidateNANEvent(this);
	}
	void notifyHosts(){notifyHosts(0xffffffff);}
	void notifyHosts(Uint32 msk);
	Uint32 magic;
	char *flagStr;
	Uint32 flags;
	Integer *lockPtr;
	char unitsBuf[ParUnitsBufSz];
	Parameter *parent;
	Parameter *listNext;
	Parameter *subList;
	Parameter *tail;
	Parameter *value;
	Adjustment *adjMech;
	Uint32 hostFlags;
	int eventMask, eventWait;
	Parameter *eventQPrev, *eventQNext;
	static Parameter *eventQHead, *eventQTail;
	bool eventQFlag;
#ifdef FASTPOLL
	bool fastChange;
#endif
	Uint32 sem;
#if defined(CONTROLCUBE) || defined(AICUBE)
	CubicEngine *pCubicEngine;
#endif
	char *snvBs, *dnvBs;
	Uint32 snvSz, dnvSz, SNVSeekPointer, DNVSeekPointer;
	bool snvLastModified, pollSuspended;
};

class IntegerPar: public Parameter
{
public:
	void operator()(Parameter *par, char *n, char *f, Sint32 *smp,
		Integer mn=DfltMinInt, Integer mx=DfltMaxInt, char *u=NULL, Integer nrm=0);
	Result operator()(Integer v, unsigned int msk);
	Integer operator()();
	void operator=(Integer v);
	virtual char getType();
	virtual Result getMin(void *buf, int sz, unsigned int msk);
	virtual Result getMax(void *buf, int sz);
	virtual Result setValue(void *buf, int sz, unsigned int msk);
	virtual Result getValue(void *buf, int sz);
	virtual Result getAddr(void *buf, int sz);
	virtual void *getBsSz(Uint32 &sz);
	virtual Uint32 sizeSNV(Uint32 s);
	virtual Uint32 sizeDNV(Uint32 s);
	virtual void *formatSNV(void *p);
	virtual void *formatDNV(void *p);
	virtual Uint32 getViolation();
	virtual Integer fastGet();
	virtual Uint32 fastRdRaw();
	virtual void fastWrRaw(Uint32 v);
	virtual Result Load(void *p);
protected:
	virtual void parValidateSignal();
	virtual void parPrimeSignal();
	virtual void parNormaliseSignal();
	virtual void parCorruptSignal();
	virtual void parPollSignal();
#ifdef FASTPOLL
	virtual void fastPoll();
#endif
	bool setSupBuf(Integer v);
	Integer localValue;
	Integer *supBuf;
	Sint32 *smpBuf;
	Integer min;
	Integer max;
	Integer norm;
	Integer violation;
};

class UnsignedPar: public Parameter
{
public:
	void operator()(Parameter *par, char *n, char *f, Uint32 *smp,
		Unsigned mn=DfltMinUns, Unsigned mx=DfltMaxUns, char *u=NULL, Unsigned nrm=0);
	Unsigned operator()();
	Result operator()(Unsigned v, unsigned int msk);
	void operator=(Unsigned v);
	virtual char getType();
	virtual Result getMin(void *buf, int sz, unsigned int msk);
	virtual Result getMax(void *buf, int sz);
	virtual Result setValue(void *buf, int sz, unsigned int msk);
	virtual Result getValue(void *buf, int sz);
	virtual Result getAddr(void *buf, int sz);
	virtual void *getBsSz(Uint32 &sz);
	virtual Uint32 sizeSNV(Uint32 s);
	virtual Uint32 sizeDNV(Uint32 s);
	virtual void *formatSNV(void *p);
	virtual void *formatDNV(void *p);
	virtual Uint32 getViolation();
	virtual Unsigned fastGet();
	virtual Uint32 fastRdRaw();
	virtual void fastWrRaw(Uint32 v);
	virtual Result Load(void *p);
protected:
	virtual void parValidateSignal();
	virtual void parPrimeSignal();
	virtual void parNormaliseSignal();
	virtual void parCorruptSignal();
	virtual void parPollSignal();
#ifdef FASTPOLL
	virtual void fastPoll();
#endif
	bool setSupBuf(Unsigned v);
	Unsigned localValue;
	Unsigned *supBuf;
	Uint32 *smpBuf;
	Unsigned min;
	Unsigned max;
	Unsigned norm;
	Unsigned violation;
};

class FloatPar: public Parameter
{
public:
	void operator()(Parameter *par, char *n, char *f, float *smp,
		Float mn=DfltMinFloat, Float mx=DfltMaxFloat, char *u=NULL, Float nrm=0.0f);
	Result operator()(Float v, unsigned int msk, bool usr);
	Float operator()();
	void operator=(Float v);
	void setMin(Float mn);
	void setMax(Float mx);
	void setMap(Float m){map=m;}
	void setRate(Float r){myRate=r;}
	Float getAssignedValue(){return assignedValue;}
	virtual char getType();
	virtual Result getMin(void *buf, int sz, unsigned int msk);
	virtual Result getMax(void *buf, int sz);
	virtual Result setValue(void *buf, int sz, unsigned int msk);
	virtual Result getValue(void *buf, int sz);
	virtual Result getLive(void *buf, int sz);
	virtual Result getAddr(void *buf, int sz);
	virtual Result setInstant(void *buf, int sz, unsigned int msk);
	virtual void *getBsSz(Uint32 &sz);
	virtual Uint32 sizeSNV(Uint32 s);
	virtual Uint32 sizeDNV(Uint32 s);
	virtual void *formatSNV(void *p);
	virtual void *formatDNV(void *p);
	virtual Uint32 getViolation();
	virtual void parRevalidateSignal();
	virtual void parValidateSignal();
	virtual void parPrimeSignal();
	virtual void parNormaliseSignal();
	virtual void parCorruptSignal();
	virtual void parPollSignal();
	virtual void parAdjFailEvent(Parameter *sce);
#ifdef FASTPOLL
	virtual void fastPoll();
#endif
	virtual Float fastGet();
	virtual Uint32 fastRdRaw();
	virtual void fastWrRaw(Uint32 v);
	virtual Result Load(void *p);
protected:
	bool setSupBuf(Float v);
	void setSmpBuf(Float v);
	Float getSmpBuf(); 
	Float localValue, assignedValue;
	Float *supBuf;
	float *smpBuf;
	Float min;
	Float max;
	Float norm;
	Float violation;
	AdjRequester adjuster;
	Float map, myRate;
};

class TextPar: public Parameter
{
public:
	void operator()(Parameter *par, char *n, char *f,
		int sz=16, char *nrm="");
	char *operator()(){return supBuf;}
	void operator=(char *v);
	virtual char getType();
	virtual Result getMin(void *buf, int sz, unsigned int msk);
	virtual Result getMax(void *buf, int sz);
	virtual Result setValue(void *buf, int sz, unsigned int msk);
	virtual Result getValue(void *buf, int sz);
	virtual void *getBsSz(Uint32 &sz);
	virtual Uint32 sizeSNV(Uint32 s);
	virtual Uint32 sizeDNV(Uint32 s);
	virtual void *formatSNV(void *p);
	virtual void *formatDNV(void *p);
protected:
	virtual void parValidateSignal();
	virtual void parNormaliseSignal();
	virtual void parCorruptSignal();
	Result operator()(char *v, unsigned int msk);
	char *supBuf;
	char *norm;
	int bufSz;
};

class EnumPar: public Parameter
{
public:
	void operator()(Parameter *par, char *n, char *f, Sint32 *smp, char **s,
		Integer nrm=0);
	Result operator()(Integer v, unsigned int msk);
	Integer operator()();
	void operator=(Integer v);
	virtual char getType();
	virtual Result getMin(void *buf, int sz, unsigned int msk);
	virtual Result getMax(void *buf, int sz);
	virtual Result setValue(void *buf, int sz, unsigned int msk);
	virtual Result getValue(void *buf, int sz);
	virtual Result getAddr(void *buf, int sz);
	virtual char *operator[](int n);
	virtual void *getBsSz(Uint32 &sz);
	virtual Uint32 sizeSNV(Uint32 s);
	virtual Uint32 sizeDNV(Uint32 s);
	virtual void *formatSNV(void *p);
	virtual void *formatDNV(void *p);
	virtual Uint32 getViolation();
	virtual Integer fastGet();
	virtual Uint32 fastRdRaw();
	virtual void fastWrRaw(Uint32 v);
	virtual Result Load(void *p);
protected:
	virtual void parValidateSignal();
	virtual void parPrimeSignal();
	virtual void parNormaliseSignal();
	virtual void parCorruptSignal();
	virtual void parPollSignal();
#ifdef FASTPOLL
	virtual void fastPoll();
#endif
	bool setSupBuf(Integer v);
	Integer localValue;
	Integer *supBuf;
	Sint32 *smpBuf;
	Integer min;
	Integer max;
	Integer norm;
	char **strings;
	Integer violation;
};

#define	MaxFltArraySize		100
class FloatArrayPar: public Parameter
{
public:
	void operator()(Parameter *par, char *n, char *f, float *smp, int sz,
		bool ver=false, Float mn=DfltMinFloat, Float mx=DfltMaxFloat, char *units=NULL, Float *nrm=NULL);
	FloatPar *(member[MaxFltArraySize]);
protected:
	virtual void parSetEvent(Parameter *sce);
	virtual void parAlteredEvent(Parameter *sce);
	virtual void parChangedEvent(Parameter *sce);
	virtual void parRangeChangedEvent(Parameter *sce);
	virtual void parConfigChangedEvent(Parameter *sce);
	Float *supBuf;
	float *smpBuf;
	int arraySz;
	char nameBuf[MaxFltArraySize][8];
	char augFlagBuf[16];
};

#define	MaxIntArraySize		100
class IntegerArrayPar: public Parameter
{
public:
	void operator()(Parameter *par, char *n, char *f, Sint32 *smp, int sz,
		bool ver=false, Integer mn=DfltMinInt, Integer mx=DfltMaxInt, char *units=NULL, Integer *nrm=NULL);
	IntegerPar *(member[MaxIntArraySize]);
protected:
	virtual void parSetEvent(Parameter *sce);
	virtual void parAlteredEvent(Parameter *sce);
	virtual void parChangedEvent(Parameter *sce);
	virtual void parRangeChangedEvent(Parameter *sce);
	virtual void parConfigChangedEvent(Parameter *sce);
	Integer *supBuf;
	Sint32 *smpBuf;
	int arraySz;
	char nameBuf[MaxIntArraySize][8];
	char augFlagBuf[16];
};

#endif
