// Parameter.cpp - Parameter Class module

#include "C3Std.h"

#ifdef PCTEST
#include "Windows.h"
#endif

// Types
//
//	c	Compound (new form or group box)
//	e	Enumeration (based on 'i' type)
//	f	Single precision floating point
//	i	Signed 32 bit integer
//	t	Text
//	u	Unsigned 32 bit integer

// flags
//
//	a	Long execution time (applies to 'i' type when 'b' flag present)
//	b	Button (applies to 'i' type)
//	c	Recommend combobox presentation (applies to 'e' type)
//	d	Dynamically changing in sample processing *
//	e	The 'd' flag is copied here when saving parameters to file
//	f	Inhibit adjustment when TSR SpFreezePos/Neg flags are set
//	g	Recommend group box presentation (applies to 'c' type)
//	h	Recommend horizontal layout (applies to 'c' type)
//	i	Use DNV space for storing NV parameter data
//	j
//	k	Variable amplitude i.e. minimum==0 (applies to 'f' types)
//	l	Recommend LED presentation (applies to 'i' type)
//	m	Variable range (applies to 'f' types)
//	n	Non-volatile
//	o	Range minimum/maximum/units origin
//	p	Lockable parameter
//	q	Recommend progress bar presentation (applies to 'f' type)
//	r	Read only
//	s	Smoothly adjusted
//	t	Ten percent overrange permitted (e.g. limit threshold - applies to 'f' type)
//	u	Recommend vertical presentation (applies to 'f' type)
//	v	Normalised sample processing (applies to 'f' type)
//	w	Assignments managed by parent (applies to 'f' type)
//	x	Recommend hex presentation (applies to 'u' type)
//	y	Cacheable parameter
//	z	Cache invalidation required if this parameter is set
//
//  *   The d flag is intended to indicate changes originating in sample processing
//		 and not changes resulting from supervisory assignment. The 'y' flag (or the
//       lack of a 'y' flag) indicates this property to an external computer

#ifdef SUPGEN

#ifdef KINETUSB
#ifndef PCTEST
#define	sprintf		SYS_sprintf
extern "C" {
void SYS_sprintf(char *buf, const char *fmt, ...);
}
#endif
#endif

extern "C" {
extern int event_handle_to_bitmask(int handle);
extern int eventGen(Parameter *caller, int eMask, void *pVal, int szVal);
extern int disable_int(void);
extern void restore_int(int state);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Parameter stuff - static

Parameter *Parameter::FindPath(Parameter *r, char *p)
{
	static char txtbuf[256];
	char *s, *q;
	Parameter *t;

	if(strlen(p)>255)
		return NULL;
	if(!r->isGood())
		return NULL;
	strncpy(txtbuf, p, 255);
	txtbuf[255]=0;
	s=txtbuf;
	t=r;
	while(q=strtok(s, ".[]\t\n\r "))
	{
		if(t->getType()!='c')
			return NULL;
		if(t->setValue(q, strlen(q)+1, 0)!=ParOkay)
			return NULL;
		t->getValue(&t, sizeof(t));
		if(!t)
			return NULL;
		s=NULL;
	}
	return t;
}

void Parameter::InstancePath(Parameter *p, char *buf, int c)
{
	Parameter *(wayPoint[64]);
	register Parameter *r, **q;
	register char *s, *t;
	int i, j, k;

	if(!p || !p->isGood())
	{
		strncpy(buf, "Bad parameter handle", c);
		return;
	}
	q=wayPoint;
	r=p;
	i=0;
	while(r&&(i<64))
	{
		*q++=r;
		r=r->parent;
		i++;
	}
	s=buf;
	if(i==64)
	{
		strcpy(s, "...");
		s+=3;
		j=c-7;
	}
	else
		j=c-4;
	if(i>0)
	{
		i--;
		q--;
	}
	while(i)
	{
		t=(*--q)->getName();
		k=strlen(t);
		if(isdigit(t[0]))
		{
			if((k+2)>j)
				break;
			*s++='[';
			strcpy(s, t);
			s+=k;
			*s++=']';
			j-=(k+2);
		}
		else
		{
			if((k+1)>j)
				break;
			if(s==buf)
				*s++='/';
			else
				*s++='.';
			strcpy(s, t);
			s+=k;
			j-=(k+1);
		}
		i--;
	}
	if(i>0)
		strcpy(s, "...");
	else
		*s=0;
}

static const char *(ParResultStr[])={	"Okay",
								"Not settable",
								"Over range",
								"Under range",
								"Not found"
};

char *Parameter::GetResultStr(Result r)
{
	if(r>=(sizeof(ParResultStr)/sizeof(char *)))
		return "Unknown result code";
	return (char *)ParResultStr[r];
}

void Parameter::broadcastRange(Parameter *par, Float fullScale, Integer unipolar, char *units)
{
	Parameter *p;
	FloatPar *q;

	if(!par)
		return;
	p=par->getSubList();
	while(p)
	{
		switch(p->getType())
		{
		case 'c':
			broadcastRange(p, fullScale, unipolar, units);
			break;
		case 'f':
			if(p->flag('k'))
			{
				q=(FloatPar *)p;
				q->setMax(fullScale);
				p->setUnits(units);
				q->parRevalidateSignal();
			}
			if(p->flag('m'))
			{
				q=(FloatPar *)p;
				q->setMin(unipolar?0.0f:-fullScale);
				q->setMax(fullScale);
				p->setUnits(units);
				q->parRevalidateSignal();
			}
			break;
		}
		p=p->getNext();
	}
}

Parameter *Parameter::eventQHead=NULL;
Parameter *Parameter::eventQTail=NULL;

void Parameter::eventPoll(int msk)
{
	Parameter *p;
	Uint32 i;
	void *q;

	p=eventQHead;
	while(p)
	{
		if(msk&p->eventWait)
		{
			q=p->getBsSz(i);
			if(!(p->eventWait=eventGen(p, p->eventWait, q, (int)i)))
			{
				if(p->eventQPrev)
					p->eventQPrev->eventQNext=p->eventQNext;
				else
					eventQHead=p->eventQNext;
				if(p->eventQNext)
					p->eventQNext->eventQPrev=p->eventQPrev;
				else
					eventQTail=p->eventQPrev;
				p->eventQFlag=false;
			}
		}
		p=p->eventQNext;
	}
}

#ifdef PCTEST
void Parameter::eventQDump()
{
	Parameter *p;
	int i;
	char buf[256];

	sprintf(buf, "0x%08x\n", (Uint32)eventQHead);
	p=eventQHead;
	i=11;
	while(p&&(i<187))
	{
		sprintf(buf+i, "0x%08x 0x%08x\n", (Uint32)(p->eventQPrev), (Uint32)(p->eventQNext));
		p=p->eventQNext;
		i+=22;
	}
	sprintf(buf+i, "0x%08x\n", (Uint32)eventQTail);
	MessageBox(NULL, buf, "Event queue", MB_OK);
}
#endif

bool Parameter::blockCopy(Parameter *p, Parameter *q)
{
	char t;

	if(!((p->isGood())&&(q->isGood())))
		return false;
	if((t=p->getType())!=(q->getType()))
		return false;
	switch(t)
	{
	case 'c':
		p=p->getSubList();
		q=q->getSubList();
		while(p&&q)
		{
			if(!blockCopy(p, q))
				return false;
			p=p->getNext();
			q=q->getNext();
		}
		break;
	case 'i':
		(*((IntegerPar *)p))=(*((IntegerPar *)q))();
		break;
	case 'u':
		(*((UnsignedPar *)p))=(*((UnsignedPar *)q))();
		break;
	case 'f':
		(*((FloatPar *)p))=(*((FloatPar *)q))();
		break;
	case 't':
		(*((TextPar *)p))=(*((TextPar *)q))();
		break;
	case 'e':
		(*((EnumPar *)p))=(*((EnumPar *)q))();
		break;
	default:
		return false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Parameter stuff - instance

void Parameter::scanFlags()
{
	char *p, ch;

	p=flagStr;
	flags=0;
	while(ch=*p++)
		flags|=(1<<(ch&31));
	listNext=subList=tail=value=NULL;
	eventMask=eventWait=0;
	eventQFlag=false;
#ifdef FASTPOLL
	fastChange=false;
#endif
	magic=ParGoodMagic;
	if(parent)
		parent->adoptMe(this);
	adjMech=Adjustment::IContext;
	lockPtr=LimitSys::IContext.supCntxt?&(LimitSys::IContext.supCntxt->ParLock):NULL;
	sem=0;
#if defined(CONTROLCUBE) || defined(AICUBE)
	pCubicEngine=CubicEngine::IContext;
#endif
	snvBs=dnvBs=NULL;
	SNVSeekPointer=DNVSeekPointer=0;
	pollSuspended=false;
}

void Parameter::operator()(Parameter *par, char *n)
{
	parent=par;
	nameStr=n;
	flagStr="";
	unitsBuf[0]=0;
	scanFlags();
}

char Parameter::getType(){return 'c';}
Result Parameter::setValue(void *buf, int sz, unsigned int msk){return operator()((char *)buf);}
Result Parameter::setInstant(void *buf, int sz, unsigned int msk){return ParOkay;}
Result Parameter::getValue(void *buf, int sz){*((Parameter **)buf)=value; return ParOkay;}
Result Parameter::getLive(void *buf, int sz){*((Parameter **)buf)=value; return ParOkay;}
Parameter *Parameter::operator()(){return value;}
void Parameter::abortAdjustment(){if(adjMech)adjMech->abort();}
Result Parameter::getAddr(void *buf, int sz){*((Unsigned *)buf)=0;return ParOkay;}
Uint32 Parameter::getViolation(){return 0;}

void *Parameter::getBsSz(Uint32 &sz)
{
	if(snvLastModified)
	{
		sz=snvSz;
		return snvBs;
	}
	else
	{
		sz=dnvSz;
		return dnvBs;
	}
}

Result Parameter::special(int fn, void *in, int incnt, void *out, int outcnt)
{
	Uint32 bs, len;

	switch(fn)
	{
	case ParSp_Set_SNV_SP:
		if(incnt<sizeof(Uint32))
			return ParBufferError;
		bs=*((Uint32 *)in);
		if(bs>snvSz)
			return ParOverRange;
		SNVSeekPointer=bs;
		break;
	case ParSp_Set_SNV:
		if(incnt<1)
			return ParBufferError;
		bs=SNVSeekPointer;
		len=(Uint32)incnt;
		if((bs+len)>snvSz)
			len=snvSz-bs;
		if(len)
			memcpy(snvBs+bs, in, len);
		SNVSeekPointer=bs+len;
		break;
	case ParSp_Get_SNV:
		if(outcnt<1)
			return ParBufferError;
		bs=SNVSeekPointer;
		len=(Uint32)outcnt;
		if((bs+len)>snvSz)
			len=snvSz-bs;
		if(len)
			memcpy(out, snvBs+bs, len);
		SNVSeekPointer=bs+len;
		break;
	case ParSp_Set_DNV_SP:
		if(incnt<sizeof(Uint32))
			return ParBufferError;
		bs=*((Uint32 *)in);
		if(bs>dnvSz)
			return ParOverRange;
		DNVSeekPointer=bs;
		break;
	case ParSp_Set_DNV:
		if(incnt<1)
			return ParBufferError;
		bs=DNVSeekPointer;
		len=(Uint32)incnt;
		if((bs+len)>dnvSz)
			len=dnvSz-bs;
		if(len)
			memcpy(dnvBs+bs, in, len);
		DNVSeekPointer=bs+len;
		break;
	case ParSp_Get_DNV:
		if(outcnt<1)
			return ParBufferError;
		bs=DNVSeekPointer;
		len=(Uint32)outcnt;
		if((bs+len)>dnvSz)
			len=dnvSz-bs;
		if(len)
			memcpy(out, dnvBs+bs, len);
		DNVSeekPointer=bs+len;
		break;
	case ParSp_Tell:
		if(outcnt<16)
			return ParBufferError;
		((Uint32 *)out)[0]=SNVSeekPointer;
		((Uint32 *)out)[1]=DNVSeekPointer;
		((Uint32 *)out)[2]=snvSz;
		((Uint32 *)out)[3]=dnvSz;
		break;
	case ParSp_Suspend:
		pollSuspended=true;
		break;
	case ParSp_Resume:
		parPrimeSignal();
		pollSuspended=false;
		snvLastModified=true;
		parSNVModifiedEvent(this);
		snvLastModified=false;
		parDNVModifiedEvent(this);
		break;
	default:
		return ParNotFound;
	}
	return ParOkay;
}

Result Parameter::getMin(void *buf, int sz, unsigned int msk)
{
	*((Uint32 *)buf)=hostFlags&msk;
	hostFlags&=~msk;
	return ParOkay;
}

Result Parameter::getMax(void *buf, int sz)
{
	Parameter *p=subList;
	Integer n=0;
	while(p)
	{
		p=p->getNext();
		n++;
	}
	*((Integer *)buf)=n;
	return ParOkay;
}

void Parameter::notifyHosts(Uint32 msk)
{
	Parameter *p;
	Uint32 i;
#ifndef FASTPOLL
	void *q;
#endif
	int j;
	int istate;

	p=this;
	do
	{
		p=p->parent;
		if(p)
			i=p->flag('g');
	}
	while(p&&i);
	if(p)
		p->hostFlags|=msk;
	j=eventMask&(int)0x0000ffff;
	if(j)
	{
		eventWait|=j;
#ifndef PCTEST
		istate = disable_int();
#endif
		if(!eventQFlag)
			if(eventQHead)
			{
				eventQPrev=eventQTail;
				eventQPrev->eventQNext=this;
				eventQTail=this;
				eventQNext=NULL;
			}
			else
			{
				eventQHead=eventQTail=this;
				eventQPrev=eventQNext=NULL;
			}
		eventQFlag=true;
#ifndef PCTEST
		restore_int(istate);
#endif
	}
#ifndef FASTPOLL
	j=eventMask&HIPRI;
	if(j)
	{
		q=getBsSz(i);
		j=eventGen(this, j, q, (int)i);
		if(j)
		{
			eventWait|=j;
#ifndef PCTEST
			istate = disable_int();
#endif
			if(!eventQFlag)
				if(eventQHead)
				{
					eventQPrev=NULL;
					eventQNext=eventQHead;
					eventQHead=this;
					eventQNext->eventQPrev=this;
				}
				else
				{
					eventQHead=eventQTail=this;
					eventQPrev=eventQNext=NULL;
				}
			eventQFlag=true;
#ifndef PCTEST
			restore_int(istate);
#endif
		}
	}
#endif
}

Result Parameter::operator()(char *v)
{
	Parameter *p=subList;
	while(p&&strcmp(v, p->getName()))
		p=p->getNext();
	value=p;
	return p?ParOkay:ParNotFound;
}

char *Parameter::operator[](int n)
{
	Parameter *p=subList;
	while(p&&n)
	{
		p=p->getNext();
		n--;
	}
	return p?p->getName():NULL;
}

void Parameter::adoptMe(Parameter *me)
{
	if(subList)
	{
		tail->listNext=me;
		tail=me;
	}
	else
		subList=tail=me;
	me->listNext=NULL;
}

#undef DEBUG_PARLIST

#ifdef DEBUG_PARLIST

/* Prototype serial output function used below */

extern "C" {
void serial_write(char *fmt, ...);
}

#define SHOWPAR serial_write("%08X %s\n", qq, q->getName());
//#define SHOWPAR serial_write("%08X\n", qq);

#endif

Uint32 Parameter::sizeSNV(Uint32 s)
{
	Parameter *q=subList;
#ifdef DEBUG_PARLIST
	Parameter *prev = NULL;

	serial_write("sizeSNV\n");
#endif

	snvSz=s;
	while(q)
	{
#ifdef DEBUG_PARLIST
		Uint32 qq = (Uint32)q;
		SHOWPAR;
		if ((qq < 0x80000000UL) || (qq > 0x81FFFFFFUL) || !(q->isGood())) while(prev);
#endif
		s=q->sizeSNV(s);
#ifdef DEBUG_PARLIST
		prev = q;
#endif
		q=q->getNext();
	}
	snvSz=s-snvSz;
	return s;
}

Uint32 Parameter::sizeDNV(Uint32 s)
{
	Parameter *q=subList;
#ifdef DEBUG_PARLIST
	Parameter *prev = NULL;

	serial_write("sizeDNV\n");
#endif

	dnvSz=s;
	while(q)
	{
#ifdef DEBUG_PARLIST
		Uint32 qq = (Uint32)q;
		SHOWPAR;
		if ((qq < 0x80000000UL) || (qq > 0x81FFFFFFUL) || !(q->isGood())) while(prev);
#endif
		s=q->sizeDNV(s);
#ifdef DEBUG_PARLIST
		prev = q;
#endif
		q=q->getNext();
	}
	dnvSz=s-dnvSz;
	return s;
}

void *Parameter::formatSNV(void *p)
{
	Parameter *q=subList;
#ifdef DEBUG_PARLIST
	Parameter *prev = NULL;

	serial_write("formatSNV\n");
#endif

	snvBs=(char *)p;
	while(q)
	{
#ifdef DEBUG_PARLIST
		Uint32 qq = (Uint32)q;
		SHOWPAR;
		if ((qq < 0x80000000UL) || (qq > 0x81FFFFFFUL) || !(q->isGood())) while(prev);
#endif
		p=q->formatSNV(p);
#ifdef DEBUG_PARLIST
		prev = q;
#endif
		q=q->getNext();
	}
	return p;
}

void *Parameter::formatDNV(void *p)
{
	Parameter *q=subList;
#ifdef DEBUG_PARLIST
	Parameter *prev = NULL;

	serial_write("formatDNV\n");
#endif

	dnvBs=(char *)p;
	while(q)
	{
#ifdef DEBUG_PARLIST
		Uint32 qq = (Uint32)q;
		SHOWPAR;
		if ((qq < 0x80000000UL) || (qq > 0x81FFFFFFUL) || !(q->isGood())) while(prev);
#endif
		p=q->formatDNV(p);
#ifdef DEBUG_PARLIST
		prev = q;
#endif
		q=q->getNext();
	}
	return p;
}

void Parameter::setFlags(char *f)
{
	char *p, ch;

	flagStr=p=f;
	flags=0;
	while(ch=*p++)
		flags|=(1<<(ch&31));
}

Result Parameter::setEvent(int eHandle, int pri)
{
	int msk;

	msk=event_handle_to_bitmask(eHandle);
	switch(pri)
	{
	case 0:
		msk|=(msk<<16);
		eventMask&=~msk;
		break;
	case 1:
		eventMask|=msk;
		break;
	case 2:
		eventMask|=(msk<<16);
		break;
	}
	return ParOkay;
}

void Parameter::clearEventFlags(int flgs)
{
	Parameter *p;

	eventMask&=~flgs;
	eventWait&=~flgs;
	if(eventQFlag&&!eventWait)
	{
		if(eventQPrev)
			eventQPrev->eventQNext=eventQNext;
		else
			eventQHead=eventQNext;
		if(eventQNext)
			eventQNext->eventQPrev=eventQPrev;
		else
			eventQTail=eventQPrev;
		eventQFlag=false;
	}
	p=subList;
	while(p)
	{
		(p->clearEventFlags)(flgs);
		p=p->getNext();
	}
}

void Parameter::parPollSignal()
{
#if defined(CONTROLCUBE) || defined(AICUBE)
	if(pCubicEngine)
		pCubicEngine->doPoll();
#endif
	if(!pollSuspended)
		descendSignal(&Parameter::parPollSignal);
}

Uint32 Parameter::fastRdRaw()
{
	return 0;
}

void Parameter::parSNVValidateUREvent(Parameter *sce){if(parent)parent->parSNVValidateUREvent(sce);}
void Parameter::parSNVValidateOREvent(Parameter *sce){if(parent)parent->parSNVValidateOREvent(sce);}
void Parameter::parSNVValidateNANEvent(Parameter *sce){if(parent)parent->parSNVValidateNANEvent(sce);}
void Parameter::parDNVValidateUREvent(Parameter *sce){if(parent)parent->parDNVValidateUREvent(sce);}
void Parameter::parDNVValidateOREvent(Parameter *sce){if(parent)parent->parDNVValidateOREvent(sce);}
void Parameter::parDNVValidateNANEvent(Parameter *sce){if(parent)parent->parDNVValidateNANEvent(sce);}
void Parameter::parSetROEvent(Parameter *sce){if(parent)parent->parSetROEvent(sce);}
void Parameter::parSetUREvent(Parameter *sce){if(parent)parent->parSetUREvent(sce);}
void Parameter::parSetOREvent(Parameter *sce){if(parent)parent->parSetOREvent(sce);}
void Parameter::parSetNanEvent(Parameter *sce){if(parent)parent->parSetNanEvent(sce);}
void Parameter::parSetLockEvent(Parameter *sce){if(parent)parent->parSetLockEvent(sce);}
void Parameter::parSNVModifiedEvent(Parameter *sce){if(parent)parent->parSNVModifiedEvent(sce);}
void Parameter::parDNVModifiedEvent(Parameter *sce){if(parent)parent->parDNVModifiedEvent(sce);}
void Parameter::parManagedSetEvent(Parameter *sce){if(parent)parent->parManagedSetEvent(sce);}
void Parameter::parSetEvent(Parameter *sce){if(parent)parent->parSetEvent(sce);}
void Parameter::parAlteredEvent(Parameter *sce){if(parent)parent->parAlteredEvent(sce);}
void Parameter::parChangedEvent(Parameter *sce){if(parent)parent->parChangedEvent(sce);}
void Parameter::parAdjFailEvent(Parameter *sce){if(parent)parent->parAdjFailEvent(sce);}
void Parameter::parRangeChangedEvent(Parameter *sce){if(parent)parent->parRangeChangedEvent(sce);}
void Parameter::parConfigChangedEvent(Parameter *sce){if(parent)parent->parConfigChangedEvent(sce);}
void Parameter::parIdentifierChangedEvent(Parameter *sce){if(parent)parent->parIdentifierChangedEvent(sce);}
void Parameter::parCompileEvent(Parameter *sce){if(parent)parent->parCompileEvent(sce);}
void Parameter::parDACChangeEvent(Parameter *sce){if(parent)parent->parDACChangeEvent(sce);}
void Parameter::parClampChangedEvent(Parameter *sce){if(parent)parent->parClampChangedEvent(sce);}
void Parameter::parValidateSignal(){descendSignal(&Parameter::parValidateSignal);}
void Parameter::parRevalidateSignal(){descendSignal(&Parameter::parRevalidateSignal);}
void Parameter::parPrimeSignal(){descendSignal(&Parameter::parPrimeSignal);}
void Parameter::parCorruptSignal(){descendSignal(&Parameter::parCorruptSignal);}
void Parameter::parNormaliseSignal(){descendSignal(&Parameter::parNormaliseSignal);}
void Parameter::parConfigureSignal(){descendSignal(&Parameter::parConfigureSignal);}
void Parameter::parRecompileSignal(){descendSignal(&Parameter::parRecompileSignal);}
Result Parameter::Load(void *p){return(ParOkay);}
#ifdef FASTPOLL
void Parameter::fastPoll(){}
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Integer Parameter stuff

void IntegerPar::operator()(Parameter *par, char *n, char *f, Sint32 *smp,
	Integer mn, Integer mx, char *u, Integer nrm)
{
	parent=par;
	nameStr=n;
	flagStr=f;
	supBuf=&localValue;
	localValue=nrm;
	smpBuf=smp;
	min=mn;
	max=mx;
	norm=nrm;
#ifdef PCTEST
	if(norm>mx)
		Trace("Normalise value above maximum");
	if(norm<mn)
		Trace("Normalise value below minimum");
#endif
	setUnits(u);
	scanFlags();
}

char IntegerPar::getType(){return 'i';}
Result IntegerPar::getMin(void *buf, int sz, unsigned int msk){*((Integer *)buf)=min; return ParOkay;}
Result IntegerPar::getMax(void *buf, int sz){*((Integer *)buf)=max; return ParOkay;}
void *IntegerPar::getBsSz(Uint32 &sz){sz=sizeof(Integer); return (void *)supBuf;}
Result IntegerPar::getValue(void *buf, int sz){*((Integer *)buf)=operator()();return ParOkay;}
Result IntegerPar::getAddr(void *buf, int sz){*((Unsigned *)buf)=(Unsigned)smpBuf;return ParOkay;}
Uint32 IntegerPar::getViolation(){return *((Uint32 *)(&violation));}

Result IntegerPar::setValue(void *buf, int sz, unsigned int msk)
{
	Result rv;

	acquireSem(&sem, 1);
	rv=operator()(*((Integer *)buf), msk);
	releaseSem(&sem, 1);
	return rv;
}

void IntegerPar::operator=(Integer v)
{
	if(v<min)
		v=min;
	if(v>max)
		v=max;
	if(smpBuf&&(flag('d')||flag('b')))
		Poke(*smpBuf, v);
	else if(setSupBuf(v))
	{
		if(smpBuf)
			Poke(*smpBuf, v);
		notifyHosts();
	}
}

bool IntegerPar::setSupBuf(Integer v)
{
	if(*supBuf==v)
		return False;
	*supBuf=v;
	if(flag('n'))
		if(flag('d')||flag('i'))
			parDNVModifiedEvent(this);
		else
			parSNVModifiedEvent(this);
	return True;
}

Result IntegerPar::operator()(Integer v, unsigned int msk)
{
	bool altrd;

	violation=v;
	if(flag('r'))
	{
		parSetROEvent(this);
		return ParNotSettable;
	}
	if(flag('p')&&lockPtr&&*lockPtr)
	{
#ifdef QUIETVALIDATION
		return ParOkay;
#else
		parSetLockEvent(this);
		return ParNotSettable;
#endif
	}
	if(v<min)
	{
		parSetUREvent(this);
#ifdef QUIETVALIDATION
		v=min;
#else
		return ParUnderRange;
#endif
	}
	if(v>max)
	{
		parSetOREvent(this);
#ifdef QUIETVALIDATION
		v=max;
#else
		return ParOverRange;
#endif
	}
	if(smpBuf&&(flag('d')||flag('b')))
		Poke(*smpBuf, v);
	else
	{
		altrd=setSupBuf(v);
		if(altrd&&smpBuf)
			Poke(*smpBuf, v);
		if(altrd)
		{
			parAlteredEvent(this);
			notifyHosts(~msk);
		}
	}
	parSetEvent(this);
	return ParOkay;
}

#ifdef FASTPOLL
Integer IntegerPar::operator()()
{
	if(fastChange||(smpBuf&&flag('d')&&(!(eventMask&HIPRI))&&setSupBuf(Peek(*smpBuf))))
	{
		fastChange=false;
		parChangedEvent(this);
		notifyHosts();
	}
	return *supBuf;
}
#else
Integer IntegerPar::operator()()
{
	if(smpBuf&&flag('d')&&setSupBuf(Peek(*smpBuf)))
	{
		parChangedEvent(this);
		notifyHosts();
	}
	return *supBuf;
}
#endif

Integer IntegerPar::fastGet()
{
	if(smpBuf&&flag('d')&&setSupBuf(Peek(*smpBuf)))
	{
		notifyHosts();
	}
	return *supBuf;
}

Uint32 IntegerPar::fastRdRaw()
{
	if(smpBuf&&flag('d'))
	{
		return((Uint32)Peek(*smpBuf));
	}
	return (Uint32)*supBuf;
}

void IntegerPar::fastWrRaw(Uint32 v)
{
	*supBuf=(Sint32)v;
}

Result IntegerPar::Load(void *p)
{
	bool altrd;
	Sint32 v = *((Sint32 *)p);

	violation=v;
	if(flag('r'))
	{
		parSetROEvent(this);
		return ParNotSettable;
	}
	if(flag('p')&&lockPtr&&*lockPtr)
	{
#ifdef QUIETVALIDATION
		return ParOkay;
#else
		parSetLockEvent(this);
		return ParNotSettable;
#endif
	}
	if(v<min)
	{
		parSetUREvent(this);
#ifdef QUIETVALIDATION
		v=min;
#else
		return ParUnderRange;
#endif
	}
	if(v>max)
	{
		parSetOREvent(this);
#ifdef QUIETVALIDATION
		v=max;
#else
		return ParOverRange;
#endif
	}
	if(smpBuf&&(flag('d')||flag('b')))
		Poke(*smpBuf, v);
	else
	{
		altrd=setSupBuf(v);
		if(altrd&&smpBuf)
			Poke(*smpBuf, v);
		if(altrd)
		{
			parAlteredEvent(this);
//			notifyHosts(~msk);
		}
	}
	parSetEvent(this);
	return ParOkay;
}

void IntegerPar::parValidateSignal()
{
	if(*supBuf<min)
	{
		violation=*supBuf;
		*supBuf=min;
		valUREvent();
	}
	if(*supBuf>max)
	{
		violation=*supBuf;
		*supBuf=max;
		valOREvent();
	}
}

void IntegerPar::parPrimeSignal()
{
	if(smpBuf)
		Poke(*smpBuf, *supBuf);
}

void IntegerPar::parNormaliseSignal()
{
	if(!(flag('r')||flag('b')))
		operator()(norm, 0);
}

void IntegerPar::parCorruptSignal()
{
	if(flag('n'))
		*supBuf=norm;
}

#ifdef FASTPOLL
void IntegerPar::parPollSignal()
{
	acquireSem(&sem, 2);
	if(fastChange||(smpBuf&&flag('d')&&(!(eventMask&&HIPRI))&&setSupBuf(Peek(*smpBuf))))
	{
		fastChange=false;
		parChangedEvent(this);
		notifyHosts();
	}
	releaseSem(&sem, 2);
}

void IntegerPar::fastPoll()
{
	Uint32 i;
	void *q;
	Sint32 oldv;

	oldv=*supBuf;
	if(smpBuf&&flag('d')&&(eventMask&HIPRI)&&setSupBuf(Peek(*smpBuf)))
	{
		fastChange=true;
		q=getBsSz(i);
		if(eventGen(this, eventMask&HIPRI, q, (int)i))
			*supBuf=oldv;
	}
}
#else
void IntegerPar::parPollSignal()
{
	acquireSem(&sem, 3);
	if(smpBuf&&flag('d')&&setSupBuf(Peek(*smpBuf)))
	{
		parChangedEvent(this);
		notifyHosts();
	}
	releaseSem(&sem, 3);
}
#endif

Uint32 IntegerPar::sizeSNV(Uint32 s)
{
	if(flag('n')&&!(flag('d')||flag('i')))
		return s+sizeof(Integer);
	else
		return s;
}

Uint32 IntegerPar::sizeDNV(Uint32 s)
{
	if(flag('n')&&(flag('d')||flag('i')))
		return s+sizeof(Integer);
	else
		return s;
}

void *IntegerPar::formatSNV(void *p)
{
	if(flag('n')&&!(flag('d')||flag('i')))
	{
		supBuf=(Integer *)p;
		return (void *)(((Integer *)p)+1);
	}
	else
		return p;
}

void *IntegerPar::formatDNV(void *p)
{
	if(flag('n')&&(flag('d')||flag('i')))
	{
		supBuf=(Integer *)p;
		return (void *)(((Integer *)p)+1);
	}
	else
		return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Unsigned Parameter stuff

void UnsignedPar::operator()(Parameter *par, char *n, char *f, Uint32 *smp,
		Unsigned mn, Unsigned mx, char *u, Unsigned nrm)
{
	parent=par;
	nameStr=n;
	flagStr=f;
	supBuf=&localValue;
	localValue=nrm;
	smpBuf=smp;
	min=mn;
	max=mx;
	norm=nrm;
#ifdef PCTEST
	if(norm>mx)
		Trace("Normalise value above maximum");
	if(norm<mn)
		Trace("Normalise value below minimum");
#endif
	setUnits(u);
	scanFlags();
}

char UnsignedPar::getType(){return 'u';}
Result UnsignedPar::getMin(void *buf, int sz, unsigned int msk){*((Unsigned *)buf)=min; return ParOkay;}
Result UnsignedPar::getMax(void *buf, int sz){*((Unsigned *)buf)=max; return ParOkay;}
void *UnsignedPar::getBsSz(Uint32 &sz){sz=sizeof(Unsigned); return (void *)supBuf;}
Result UnsignedPar::getValue(void *buf, int sz){*((Unsigned *)buf)=operator()();return ParOkay;}
Result UnsignedPar::getAddr(void *buf, int sz){*((Unsigned *)buf)=(Unsigned)smpBuf;return ParOkay;}
Uint32 UnsignedPar::getViolation(){return violation;}

Result UnsignedPar::setValue(void *buf, int sz, unsigned int msk)
{
	Result rv;
	
	acquireSem(&sem, 4);
	rv=operator()(*((Unsigned *)buf), msk);
	releaseSem(&sem, 4);
	return rv;
}

void UnsignedPar::operator=(Unsigned v)
{
	if(v<min)
		v=min;
	if(v>max)
		v=max;
	if(smpBuf&&flag('d'))
		Poke(*smpBuf, v);
	else if(setSupBuf(v))
	{
		if(smpBuf)
			Poke(*smpBuf, v);
		notifyHosts();
	}
}

bool UnsignedPar::setSupBuf(Unsigned v)
{
	if(*supBuf==v)
		return False;
	*supBuf=v;
	if(flag('n'))
		if(flag('d')||flag('i'))
			parDNVModifiedEvent(this);
		else
			parSNVModifiedEvent(this);
	return True;
}

Result UnsignedPar::operator()(Unsigned v, unsigned int msk)
{
	bool altrd;

	violation=v;
	if(flag('r'))
	{
		parSetROEvent(this);
		return ParNotSettable;
	}
	if(flag('p')&&lockPtr&&*lockPtr)
	{
#ifdef QUIETVALIDATION
		return ParOkay;
#else
		parSetLockEvent(this);
		return ParNotSettable;
#endif
	}
	if(v<min)
	{
		parSetUREvent(this);
#ifdef QUIETVALIDATION
		v=min;
#else
		return ParUnderRange;
#endif
	}
	if(v>max)
	{
		parSetOREvent(this);
#ifdef QUIETVALIDATION
		v=max;
#else
		return ParOverRange;
#endif
	}
	if(smpBuf&&flag('d'))
		Poke(*smpBuf, v);
	else
	{
		altrd=setSupBuf(v);
		if(altrd&&smpBuf)
			Poke(*smpBuf, v);
		if(altrd)
		{
			parAlteredEvent(this);
			notifyHosts(~msk);
		}
	}
	parSetEvent(this);
	return ParOkay;
}

#ifdef FASTPOLL
Unsigned UnsignedPar::operator()()
{
	if(fastChange||(smpBuf&&flag('d')&&(!(eventMask&HIPRI))&&setSupBuf(Peek(*smpBuf))))
	{
		fastChange=false;
		parChangedEvent(this);
		notifyHosts();
	}
	return *supBuf;
}
#else
Unsigned UnsignedPar::operator()()
{
	if(smpBuf&&flag('d')&&setSupBuf(Peek(*smpBuf)))
	{
		parChangedEvent(this);
		notifyHosts();
	}
	return *supBuf;
}
#endif

Unsigned UnsignedPar::fastGet()
{
	if(smpBuf&&flag('d')&&setSupBuf(Peek(*smpBuf)))
	{
		notifyHosts();
	}
	return *supBuf;
}

Uint32 UnsignedPar::fastRdRaw()
{
	if(smpBuf&&flag('d'))
	{
		return((Uint32)Peek(*smpBuf));
	}
	return (Uint32)*supBuf;
}

void UnsignedPar::fastWrRaw(Uint32 v)
{
	*supBuf=(Uint32)v;
}

Result UnsignedPar::Load(void *p)
{
	bool altrd;
	Uint32 v = *((Uint32 *)p);

	violation=v;
	if(flag('r'))
	{
		parSetROEvent(this);
		return ParNotSettable;
	}
	if(flag('p')&&lockPtr&&*lockPtr)
	{
#ifdef QUIETVALIDATION
		return ParOkay;
#else
		parSetLockEvent(this);
		return ParNotSettable;
#endif
	}
	if(v<min)
	{
		parSetUREvent(this);
#ifdef QUIETVALIDATION
		v=min;
#else
		return ParUnderRange;
#endif
	}
	if(v>max)
	{
		parSetOREvent(this);
#ifdef QUIETVALIDATION
		v=max;
#else
		return ParOverRange;
#endif
	}
	if(smpBuf&&flag('d'))
		Poke(*smpBuf, v);
	else
	{
		altrd=setSupBuf(v);
		if(altrd&&smpBuf)
			Poke(*smpBuf, v);
		if(altrd)
		{
			parAlteredEvent(this);
//			notifyHosts(~msk);
		}
	}
	parSetEvent(this);
	return ParOkay;
}

void UnsignedPar::parValidateSignal()
{
	if(*supBuf<min)
	{
		violation=*supBuf;
		*supBuf=min;
		valUREvent();
	}
	if(*supBuf>max)
	{
		violation=*supBuf;
		*supBuf=max;
		valOREvent();
	}
}

void UnsignedPar::parPrimeSignal()
{
	if(smpBuf)
		Poke(*smpBuf, *supBuf);
}

void UnsignedPar::parNormaliseSignal()
{
	if(!(flag('r')||flag('x')))
		operator()(norm, 0);
}

void UnsignedPar::parCorruptSignal()
{
	if(flag('n'))
		*supBuf=norm;
}

#ifdef FASTPOLL
void UnsignedPar::parPollSignal()
{
	acquireSem(&sem, 5);
	if(fastChange||(smpBuf&&flag('d')&&(!(eventMask&HIPRI))&&setSupBuf(Peek(*smpBuf))))
	{
		fastChange=false;
		parChangedEvent(this);
		notifyHosts();
	}
	releaseSem(&sem, 5);
}

void UnsignedPar::fastPoll()
{
	Uint32 i;
	void *q;
	Uint32 oldv;

	oldv=*supBuf;
	if(smpBuf&&flag('d')&&(eventMask&HIPRI)&&setSupBuf(Peek(*smpBuf)))
	{
		fastChange=true;
		q=getBsSz(i);
		if(eventGen(this, eventMask&HIPRI, q, (int)i))
			*supBuf=oldv;
	}
}
#else
void UnsignedPar::parPollSignal()
{
	acquireSem(&sem, 6);
	if(smpBuf&&flag('d')&&setSupBuf(Peek(*smpBuf)))
	{
		parChangedEvent(this);
		notifyHosts();
	}
	releaseSem(&sem, 6);
}
#endif

Uint32 UnsignedPar::sizeSNV(Uint32 s)
{
	if(flag('n')&&!(flag('d')||flag('i')))
		return s+sizeof(Unsigned);
	else
		return s;
}

Uint32 UnsignedPar::sizeDNV(Uint32 s)
{
	if(flag('n')&&(flag('d')||flag('i')))
		return s+sizeof(Unsigned);
	else
		return s;
}

void *UnsignedPar::formatSNV(void *p)
{
	if(flag('n')&&!(flag('d')||flag('i')))
	{
		supBuf=(Unsigned *)p;
		return (void *)(((Unsigned *)p)+1);
	}
	else
		return p;
}

void *UnsignedPar::formatDNV(void *p)
{
	if(flag('n')&&(flag('d')||flag('i')))
	{
		supBuf=(Unsigned *)p;
		return (void *)(((Unsigned *)p)+1);
	}
	else
		return p;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Float Parameter stuff

void FloatPar::operator()(Parameter *par, char *n, char *f, float *smp,
		Float mn, Float mx, char *u, Float nrm)
{
	parent=par;
	nameStr=n;
	flagStr=f;
	supBuf=&localValue;
	localValue=nrm;
	smpBuf=smp;
	min=mn;
	max=mx;
	norm=nrm;
#ifdef PCTEST
	if(norm>mx)
		Trace("Normalise value above maximum");
	if(norm<mn)
		Trace("Normalise value below minimum");
#endif
	setUnits(u);
	scanFlags();
	adjuster.init(this, smp);
	map=1.0f;
	myRate=0.0f;
}

char FloatPar::getType(){return 'f';}
Result FloatPar::getMin(void *buf, int sz, unsigned int msk){*((Float *)buf)=min; return ParOkay;}
Result FloatPar::getMax(void *buf, int sz){*((Float *)buf)=max; return ParOkay;}
void FloatPar::setMin(Float mn){min=mn;}
void FloatPar::setMax(Float mx){max=mx;}
void *FloatPar::getBsSz(Uint32 &sz){sz=sizeof(Float); return (void *)supBuf;}
Result FloatPar::getValue(void *buf, int sz){*((Float *)buf)=operator()();return ParOkay;}
Result FloatPar::getAddr(void *buf, int sz){*((Unsigned *)buf)=(Unsigned)smpBuf;return ParOkay;}
Uint32 FloatPar::getViolation(){return *((Uint32 *)(&violation));}

Result FloatPar::setValue(void *buf, int sz, unsigned int msk)
{
	Result rv;
	
	acquireSem(&sem, 7);
	rv=operator()(*((Float *)buf), msk, true);
	releaseSem(&sem, 7);
	return rv;
}

void FloatPar::operator=(Float v)
{
	float normFac, rate, tmin, tmax;

	if(isnan(v))
		return;
	tmin=flag('t')?1.1f*min:min;
	tmax=flag('t')?1.1f*max:max;
	if(v<tmin)
		v=tmin;
	if(v>tmax)
		v=tmax;
	normFac=flag('v')?(1.0f/max):map;
	rate=flag('v')?0.1f:0.1f*map*max;
	if(smpBuf&&flag('d'))
	{
		if(flag('s'))
			adjuster.request(flag('f'), normFac*v, rate, 10.0f*rate, myRate);
		else
			setSmpBuf(v);
	}
	else if(setSupBuf(v))
	{
		if(smpBuf)
			if(flag('s'))
				adjuster.request(flag('f'), normFac*v, rate, 10.0f*rate, myRate);
			else
				setSmpBuf(v);
		notifyHosts();
	}
}

bool FloatPar::setSupBuf(Float v)
{
	if(*supBuf==v)
		return False;
	*supBuf=v;
	if(flag('n'))
		if(flag('d')||flag('i'))
			parDNVModifiedEvent(this);
		else
			parSNVModifiedEvent(this);
	return True;
}

void FloatPar::setSmpBuf(Float v)
{
	v*=(flag('v')?1.0f/max:map);
	Poke(*smpBuf, v);
}

Float FloatPar::getSmpBuf()
{
	Float rv, ul, ll;

	rv=Peek(*smpBuf);
	rv*=(flag('v')?max:1.0f/map);
	ll=min-C3RoundToll*max;
	ul=(1.0f+C3RoundToll)*max;
	if((rv<min)&&(rv>ll))
		rv=min;
	if((rv>max)&&(rv<ul))
		rv=max;
	return rv;
}

Result FloatPar::operator()(Float v, unsigned int msk, bool usr)
{
	bool altrd;
	float normFac, rate, tmin, tmax;

	violation=v;
	if(flag('r'))
	{
		parSetROEvent(this);
		return ParNotSettable;
	}
	if(flag('p')&&lockPtr&&*lockPtr)
	{
#ifdef QUIETVALIDATION
		return ParOkay;
#else
		parSetLockEvent(this);
		return ParNotSettable;
#endif
	}
	if(isnan(v))
	{
		parSetNanEvent(this);
		return ParNan;
	}
	tmin=flag('t')?1.1f*min:min;
	tmax=flag('t')?1.1f*max:max;
	if(v<tmin)
	{
		parSetUREvent(this);
#ifdef QUIETVALIDATION
		v=tmin;
#else
		return ParUnderRange;
#endif
	}
	if(v>tmax)
	{
		parSetOREvent(this);
#ifdef QUIETVALIDATION
		v=tmax;
#else
		return ParOverRange;
#endif
	}
	if(usr&&flag('w'))
	{
		assignedValue=v;
		parManagedSetEvent(this);
		return ParOkay;
	}
	normFac=flag('v')?(1.0f/max):map;
	rate=flag('v')?0.1f:0.1f*map*max;
	if(smpBuf&&flag('d'))
	{
		if(flag('s'))
			adjuster.request(flag('f'), normFac*v, rate, 10.0f*rate, myRate);
		else
			setSmpBuf(v);
	}
	else
	{
		altrd=setSupBuf(v);
		if(altrd&&smpBuf)
			if(flag('s'))
				adjuster.request(flag('f'), normFac*v, rate, 10.0f*rate, myRate);
			else
				setSmpBuf(v);
		if(altrd)
		{
			parAlteredEvent(this);
			notifyHosts(~msk);
		}
	}
	parSetEvent(this);
	return ParOkay;
}

Result FloatPar::setInstant(void *buf, int sz, unsigned int msk)
{
	bool altrd;
	float v, normFac, tmin, tmax;

	v=*((Float *)buf);
	violation=v;
	if(flag('r'))
	{
		parSetROEvent(this);
		return ParNotSettable;
	}
	if(flag('p')&&lockPtr&&*lockPtr)
	{
#ifdef QUIETVALIDATION
		return ParOkay;
#else
		parSetLockEvent(this);
		return ParNotSettable;
#endif
	}
	if(isnan(v))
	{
		parSetNanEvent(this);
		return ParNan;
	}
	tmin=flag('t')?1.1f*min:min;
	tmax=flag('t')?1.1f*max:max;
	if(v<tmin)
	{
		parSetUREvent(this);
#ifdef QUIETVALIDATION
		v=tmin;
#else
		return ParUnderRange;
#endif
	}
	if(v>tmax)
	{
		parSetOREvent(this);
#ifdef QUIETVALIDATION
		v=tmax;
#else
		return ParOverRange;
#endif
	}
	acquireSem(&sem, 8);
	if(flag('w'))
	{
		assignedValue=v;
		parManagedSetEvent(this);
		releaseSem(&sem, 8);
		return ParOkay;
	}
	normFac=flag('v')?(1.0f/max):map;
	if(smpBuf&&flag('d'))
	{
		if(flag('s'))
			adjuster.request(flag('f'), normFac*v, 0.0f, 0.0f, 0.0f);
		else
			setSmpBuf(v);
	}
	else
	{
		altrd=setSupBuf(v);
		if(altrd&&smpBuf)
			if(flag('s'))
				adjuster.request(flag('f'), normFac*v, 0.0f, 0.0f, 0.0f);
			else
				setSmpBuf(v);
		if(altrd)
		{
			parAlteredEvent(this);
			notifyHosts(~msk);
		}
	}
	parSetEvent(this);
	releaseSem(&sem, 8);
	return ParOkay;
}

#ifdef FASTPOLL
Float FloatPar::operator()()
{
	Float rv;

	if(fastChange||(smpBuf&&flag('d')&&(!(eventMask&HIPRI))&&setSupBuf(getSmpBuf())))
	{
		fastChange=false;
		parChangedEvent(this);
		notifyHosts();
	}
	if(adjuster.busy())
		rv=(flag('v')?max:1.0f/map)*adjuster.getTarget();
	else
		rv=*supBuf;
	return rv;
}
#else
Float FloatPar::operator()()
{
	Float rv;

	if(smpBuf&&flag('d')&&setSupBuf(getSmpBuf()))
	{
		parChangedEvent(this);
		notifyHosts();
	}
	if(adjuster.busy())
		rv=(flag('v')?max:1.0f/map)*adjuster.getTarget();
	else
		rv=*supBuf;
	return rv;
}
#endif

Float FloatPar::fastGet()
{
	Float rv;

	if(smpBuf&&flag('d')&&setSupBuf(getSmpBuf()))
	{
		notifyHosts();
	}
	if(adjuster.busy())
		rv=(flag('v')?max:1.0f/map)*adjuster.getTarget();
	else
		rv=*supBuf;
	return rv;
}

#ifdef PCTEST
int _ftoi(float aNumber)
{
	return (int)aNumber;
}
int _itof(int aNumber)
{
	return (float)aNumber;
}
#endif
Uint32 FloatPar::fastRdRaw()
{
	if(smpBuf&&flag('d'))
	{
		return((Uint32)_ftoi(Peek(*smpBuf)));
	}
	return (Uint32)_ftoi(*supBuf);
}

void FloatPar::fastWrRaw(Uint32 v)
{
	*supBuf=(float)_itof(v);
}

Result FloatPar::Load(void *p)
{
	bool altrd;
	float normFac, rate, tmin, tmax;
	float v = *((float *)p);

	violation=v;
	if(flag('r'))
	{
		parSetROEvent(this);
		return ParNotSettable;
	}
	if(flag('p')&&lockPtr&&*lockPtr)
	{
#ifdef QUIETVALIDATION
		return ParOkay;
#else
		parSetLockEvent(this);
		return ParNotSettable;
#endif
	}
	if(isnan(v))
	{
		parSetNanEvent(this);
		return ParNan;
	}
	tmin=flag('t')?1.1f*min:min;
	tmax=flag('t')?1.1f*max:max;
	if(v<tmin)
	{
		parSetUREvent(this);
#ifdef QUIETVALIDATION
		v=tmin;
#else
		return ParUnderRange;
#endif
	}
	if(v>tmax)
	{
		parSetOREvent(this);
#ifdef QUIETVALIDATION
		v=tmax;
#else
		return ParOverRange;
#endif
	}
#if 0
	if(usr&&flag('w'))
	{
		assignedValue=v;
		parManagedSetEvent(this);
		return ParOkay;
	}
#endif
	normFac=flag('v')?(1.0f/max):map;
	rate=flag('v')?0.1f:0.1f*map*max;
	if(smpBuf&&flag('d'))
	{
		if(flag('s'))
			adjuster.request(flag('f'), normFac*v, rate, 10.0f*rate, myRate);
		else
			setSmpBuf(v);
	}
	else
	{
		altrd=setSupBuf(v);
		if(altrd&&smpBuf)
			if(flag('s'))
				adjuster.request(flag('f'), normFac*v, rate, 10.0f*rate, myRate);
			else
				setSmpBuf(v);
		if(altrd)
		{
			parAlteredEvent(this);
//			notifyHosts(~msk);
		}
	}
	parSetEvent(this);
	return ParOkay;
}

void FloatPar::parAdjFailEvent(Parameter *sce)
{
	setSupBuf((flag('v')?max:1.0f/map)*adjuster.getfinalv());
	notifyHosts();
}

void FloatPar::parValidateSignal()
{
	float tmin, tmax;

	if(isnan(*supBuf))
	{
		violation=*supBuf;
		*supBuf=0.0f;
		valNANEvent();
	}
	tmin=flag('t')?min-1.1f*max:min;
	tmax=flag('t')?1.1f*max:max;
	if(*supBuf<tmin)
	{
		violation=*supBuf;
		*supBuf=tmin;
		valUREvent();
	}
	if(*supBuf>tmax)
	{
		violation=*supBuf;
		*supBuf=tmax;
		valOREvent();
	}
}

void FloatPar::parRevalidateSignal()
{
	bool smpset;
	float tmin, tmax;

	tmin=flag('t')?1.1f*min:min;
	tmax=flag('t')?1.1f*max:max;
	smpset=false;
	if(*supBuf<tmin)
	{
		*supBuf=tmin;
		if(smpBuf)
			setSmpBuf(tmin);
		if(flag('n'))
			if(flag('d'))
				parDNVModifiedEvent(this);
			else
				parSNVModifiedEvent(this);
		smpset=true;
	}
	if(*supBuf>tmax)
	{
		*supBuf=tmax;
		if(smpBuf)
			setSmpBuf(tmax);
		if(flag('n'))
			if(flag('d'))
				parDNVModifiedEvent(this);
			else
				parSNVModifiedEvent(this);
		smpset=true;
	}
	if(!smpset&&smpBuf&&flag('v'))
			setSmpBuf(*supBuf);
	notifyHosts();
}

void FloatPar::parPrimeSignal()
{
	if(smpBuf)
		setSmpBuf(*supBuf);
}

void FloatPar::parNormaliseSignal()
{
	if(!flag('r'))
		operator()(norm, 0, false);
}

void FloatPar::parCorruptSignal()
{
	if(flag('n'))
		*supBuf=norm;
}

#ifdef FASTPOLL
void FloatPar::parPollSignal()
{
	acquireSem(&sem, 9);
	if(fastChange||(smpBuf&&flag('d')&&(!(eventMask&HIPRI))&&setSupBuf(getSmpBuf())))
	{
		fastChange=false;
		parChangedEvent(this);
		notifyHosts();
	}
	releaseSem(&sem, 9);
}

void FloatPar::fastPoll()
{
	Uint32 i;
	void *q;
	float oldv;

	oldv=*supBuf;
	if(smpBuf&&flag('d')&&(eventMask&HIPRI)&&setSupBuf(getSmpBuf()))
	{
		fastChange=true;
		q=getBsSz(i);
		if(eventGen(this, eventMask&HIPRI, q, (int)i))
			*supBuf=oldv;
	}
}
#else
void FloatPar::parPollSignal()
{
	acquireSem(&sem, 10);
	if(smpBuf&&flag('d')&&setSupBuf(getSmpBuf()))
	{
		parChangedEvent(this);
		notifyHosts();
	}
	releaseSem(&sem, 10);
}
#endif

Result FloatPar::getLive(void *buf, int sz)
{
	if(smpBuf&&adjuster.busy())
		*((Float *)buf)=getSmpBuf();
	else
		*((Float *)buf)=*supBuf;
	 return ParOkay;
}

Uint32 FloatPar::sizeSNV(Uint32 s)
{
	if(flag('n')&&!(flag('d')||flag('i')))
		return s+sizeof(Float);
	else
		return s;
}

Uint32 FloatPar::sizeDNV(Uint32 s)
{
	if(flag('n')&&(flag('d')||flag('i')))
		return s+sizeof(Float);
	else
		return s;
}

void *FloatPar::formatSNV(void *p)
{
	if(flag('n')&&!(flag('d')||flag('i')))
	{
		supBuf=(Float *)p;
		return (void *)(((Float *)p)+1);
	}
	else
		return p;
}

void *FloatPar::formatDNV(void *p)
{
	if(flag('n')&&(flag('d')||flag('i')))
	{
		supBuf=(Float *)p;
		return (void *)(((Float *)p)+1);
	}
	else
		return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Text Parameter stuff

void TextPar::operator()(Parameter *par, char *n, char *f, int sz, char *nrm)
{
	parent=par;
	nameStr=n;
	flagStr=f;
	bufSz=(sz<1)?1:sz;
	supBuf=new char[bufSz];
	strncpy(supBuf, nrm, bufSz);
	supBuf[bufSz-1]=0;
	norm=nrm;
	unitsBuf[0]=0;
	scanFlags();
}

char TextPar::getType(){return 't';}
Result TextPar::getMin(void *buf, int sz, unsigned int msk){*((int *)buf)=0; return ParOkay;}
Result TextPar::getMax(void *buf, int sz){*((int *)buf)=bufSz; return ParOkay;}
Result TextPar::setValue(void *buf, int sz, unsigned int msk){return operator()((char *)buf, msk);}
void *TextPar::getBsSz(Uint32 &sz){sz=bufSz; return (void *)supBuf;}

void TextPar::operator=(char *v)
{
	if(strncmp(supBuf, v, bufSz-1))
	{
		strncpy(supBuf, v, bufSz-1);
		supBuf[bufSz-1]=0;
		if(flag('n'))
			if(flag('d')||flag('i'))
				parDNVModifiedEvent(this);
			else
				parSNVModifiedEvent(this);
		notifyHosts();
	}
}

Result TextPar::getValue(void *buf, int sz)
{
#ifndef QUIETVALIDATION
	if(sz<bufSz)
		return ParBufferError;
#endif
	strncpy((char *)buf, supBuf, sz-1);
	((char *)buf)[sz-1]=0;
	return ParOkay;
}

Result TextPar::operator()(char *v, unsigned int msk)
{
	if(flag('r'))
	{
		parSetROEvent(this);
		return ParNotSettable;
	}
	if(flag('p')&&lockPtr&&*lockPtr)
	{
#ifdef QUIETVALIDATION
		return ParOkay;
#else
		parSetLockEvent(this);
		return ParNotSettable;
#endif
	}
	if(strncmp(supBuf, v, bufSz-1))
	{
		strncpy(supBuf, v, bufSz-1);
		supBuf[bufSz-1]=0;
		if(flag('n'))
			if(flag('d')||flag('i'))
				parDNVModifiedEvent(this);
			else
				parSNVModifiedEvent(this);
		parAlteredEvent(this);
		notifyHosts(~msk);
	}
	parSetEvent(this);
	return ParOkay;
}

void TextPar::parValidateSignal()
{
	supBuf[bufSz-1]=0;
}

void TextPar::parNormaliseSignal()
{
	if(!flag('r'))
		operator()((char *)(norm?norm:""), 0);
}

void TextPar::parCorruptSignal()
{
	if(flag('n'))
	{
		strncpy(supBuf, norm?norm:"", bufSz-1);
		supBuf[bufSz-1]=0;
	}
}

Uint32 TextPar::sizeSNV(Uint32 s)
{
	if(flag('n')&&!(flag('d')||flag('i')))
		return s+bufSz;
	else
		return s;
}

Uint32 TextPar::sizeDNV(Uint32 s)
{
	if(flag('n')&&(flag('d')||flag('i')))
		return s+bufSz;
	else
		return s;
}

void *TextPar::formatSNV(void *p)
{
	if(flag('n')&&!(flag('d')||flag('i')))
	{
		supBuf=(char *)p;
		return (void *)(((char *)p)+bufSz);
	}
	else
		return p;
}

void *TextPar::formatDNV(void *p)
{
	if(flag('n')&&(flag('d')||flag('i')))
	{
		supBuf=(char *)p;
		return (void *)(((char *)p)+bufSz);
	}
	else
		return p;
	return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Enum Parameter stuff

void EnumPar::operator()(Parameter *par, char *n, char *f, Sint32 *smp, char **str,
	Integer nrm)
{
	char **p;
	Integer i;

	parent=par;
	nameStr=n;
	flagStr=f;
	supBuf=&localValue;
	localValue=nrm;
	smpBuf=smp;
	min=0;
	p=str;
	i=0;
	if(p)
		while(*p)
		{
			i++;
			p++;
		}
	max=i-1;
	norm=nrm;
#ifdef PCTEST
	if(norm>max)
		Trace("Normalise value above maximum");
	if(norm<0)
		Trace("Normalise value below minimum");
#endif
	strings=str;
	unitsBuf[0]=0;
	scanFlags();
}

char EnumPar::getType(){return 'e';}
Result EnumPar::getValue(void *buf, int sz){*((Integer *)buf)=operator()(); return ParOkay;}
Result EnumPar::getMin(void *buf, int sz, unsigned int msk){*((Integer *)buf)=min; return ParOkay;}
Result EnumPar::getMax(void *buf, int sz){*((Integer *)buf)=max; return ParOkay;}
Result EnumPar::getAddr(void *buf, int sz){*((Unsigned *)buf)=(Unsigned)smpBuf;return ParOkay;}
void *EnumPar::getBsSz(Uint32 &sz){sz=sizeof(Integer); return (void *)supBuf;}
Uint32 EnumPar::getViolation(){return *((Uint32 *)(&violation));}

Result EnumPar::setValue(void *buf, int sz, unsigned int msk)
{
	Result rv;

	acquireSem(&sem, 11);
	rv=operator()(*((Integer *)buf), msk);
	releaseSem(&sem, 11);
	return rv;
}

void EnumPar::operator=(Integer v)
{
	if(v<min)
		v=min;
	if(v>max)
		v=max;
	if(smpBuf&&flag('d'))
		Poke(*smpBuf, v);
	else if(setSupBuf(v))
	{
		if(smpBuf)
			Poke(*smpBuf, v);
		notifyHosts();
	}
}

bool EnumPar::setSupBuf(Integer v)
{
	if(*supBuf==v)
		return False;
	*supBuf=v;
	if(flag('n'))
		if(flag('d')||flag('i'))
			parDNVModifiedEvent(this);
		else
			parSNVModifiedEvent(this);
	return True;
}

Result EnumPar::operator()(Integer v, unsigned int msk)
{
	bool altrd;

	violation=v;
	if(flag('r'))
	{
		parSetROEvent(this);
		return ParNotSettable;
	}
	if(flag('p')&&lockPtr&&*lockPtr)
	{
#ifdef QUIETVALIDATION
		return ParOkay;
#else
		parSetLockEvent(this);
		return ParNotSettable;
#endif
	}
	if(v<min)
	{
		parSetUREvent(this);
#ifdef QUIETVALIDATION
		v=min;
#else
		return ParUnderRange;
#endif
	}
	if(v>max)
	{
		parSetOREvent(this);
#ifdef QUIETVALIDATION
		v=max;
#else
		return ParOverRange;
#endif
	}
	if(smpBuf&&flag('d'))
		Poke(*smpBuf, v);
	else
	{
		altrd=setSupBuf(v);
		if(altrd&&smpBuf)
			Poke(*smpBuf, v);
		if(altrd)
		{
			parAlteredEvent(this);
			notifyHosts(~msk);
		}
	}
	parSetEvent(this);
	return ParOkay;
}

#ifdef FASTPOLL
Integer EnumPar::operator()()
{
	if(fastChange||(smpBuf&&flag('d')&&(!(eventMask&HIPRI))&&setSupBuf(Peek(*smpBuf))))
	{
		fastChange=false;
		parChangedEvent(this);
		notifyHosts();
	}
	return *supBuf;
}
#else
Integer EnumPar::operator()()
{
	if(smpBuf&&flag('d')&&setSupBuf(Peek(*smpBuf)))
	{
		parChangedEvent(this);
		notifyHosts();
	}
	return *supBuf;
}
#endif

Integer EnumPar::fastGet()
{
	if(smpBuf&&flag('d')&&setSupBuf(Peek(*smpBuf)))
	{
		notifyHosts();
	}
	return *supBuf;
}

Uint32 EnumPar::fastRdRaw()
{
	if(smpBuf&&flag('d'))
	{
		return((Uint32)Peek(*smpBuf));
	}
	return (Uint32)*supBuf;
}

void EnumPar::fastWrRaw(Uint32 v)
{
	*supBuf=v;
}

Result EnumPar::Load(void *p)
{
	bool altrd;
	Uint32 v = *((Uint32 *)p);

	violation=v;
	if(flag('r'))
	{
		parSetROEvent(this);
		return ParNotSettable;
	}
	if(flag('p')&&lockPtr&&*lockPtr)
	{
#ifdef QUIETVALIDATION
		return ParOkay;
#else
		parSetLockEvent(this);
		return ParNotSettable;
#endif
	}
	if(v<min)
	{
		parSetUREvent(this);
#ifdef QUIETVALIDATION
		v=min;
#else
		return ParUnderRange;
#endif
	}
	if(v>max)
	{
		parSetOREvent(this);
#ifdef QUIETVALIDATION
		v=max;
#else
		return ParOverRange;
#endif
	}
	if(smpBuf&&flag('d'))
		Poke(*smpBuf, v);
	else
	{
		altrd=setSupBuf(v);
		if(altrd&&smpBuf)
			Poke(*smpBuf, v);
		if(altrd)
		{
			parAlteredEvent(this);
//			notifyHosts(~msk);
		}
	}
	parSetEvent(this);
	return ParOkay;
}

char *EnumPar::operator[](int n)
{
	char **p, *q;

	if(!(p=strings) || !*p)
		return NULL;
	while((q=*p++)&&n)
		n--;
	return q;
}

void EnumPar::parValidateSignal()
{
	if(*supBuf<min)
	{
		violation=*supBuf;
		*supBuf=min;
		valUREvent();
	}
	if(*supBuf>max)
	{
		violation=*supBuf;
		*supBuf=max;
		valOREvent();
	}
}

void EnumPar::parPrimeSignal()
{
	if(smpBuf)
		Poke(*smpBuf, *supBuf);
}

void EnumPar::parNormaliseSignal()
{
	if(!flag('r'))
		operator()(norm, 0);
}

void EnumPar::parCorruptSignal()
{
	if(!flag('r'))
		*supBuf=norm;
}

#ifdef FASTPOLL
void EnumPar::parPollSignal()
{
	acquireSem(&sem, 12);
	if(fastChange||(smpBuf&&flag('d')&&(!(eventMask&HIPRI))&&setSupBuf(Peek(*smpBuf))))
	{
		fastChange=false;
		parChangedEvent(this);
		notifyHosts();
	}
	releaseSem(&sem, 12);
}

void EnumPar::fastPoll()
{
	Uint32 i;
	void *q;
	Sint32 oldv;

	oldv=*supBuf;
	if(smpBuf&&flag('d')&&(eventMask&HIPRI)&&setSupBuf(Peek(*smpBuf)))
	{
		fastChange=true;
		q=getBsSz(i);
		if(eventGen(this, eventMask&HIPRI, q, (int)i))
			*supBuf=oldv;
	}
}
#else
void EnumPar::parPollSignal()
{
	acquireSem(&sem, 13);
	if(smpBuf&&flag('d')&&setSupBuf(Peek(*smpBuf)))
	{
		parChangedEvent(this);
		notifyHosts();
	}
	releaseSem(&sem, 13);
}
#endif

Uint32 EnumPar::sizeSNV(Uint32 s)
{
	if(flag('n')&&!(flag('d')||flag('i')))
		return s+sizeof(Integer);
	else
		return s;
}

Uint32 EnumPar::sizeDNV(Uint32 s)
{
	if(flag('n')&&(flag('d')||flag('i')))
		return s+sizeof(Integer);
	else
		return s;
}

void *EnumPar::formatSNV(void *p)
{
	if(flag('n')&&!(flag('d')||flag('i')))
	{
		supBuf=(Integer *)p;
		return (void *)(((Integer *)p)+1);
	}
	else
		return p;
}

void *EnumPar::formatDNV(void *p)
{
	if(flag('n')&&(flag('d')||flag('i')))
	{
		supBuf=(Integer *)p;
		return (void *)(((Integer *)p)+1);
	}
	else
		return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Float Array Parameter stuff

void FloatArrayPar::operator()(Parameter *par, char *n, char *f, float *smp, int sz,
		bool ver, Float mn, Float mx, char *u, Float *nrm)
{
	int i;
	FloatPar *p;

	if(sz<0)
		sz=0;
	if(sz>MaxFltArraySize)
		sz=MaxFltArraySize;
	smpBuf=smp;
	arraySz=sz;
	Parameter::operator()(par, n);
	if(ver)
	{
		this->setFlags("h");
		strcpy(augFlagBuf, "u");
		strncat(augFlagBuf, f, 14);
	}
	else
		strncpy(augFlagBuf, f, 15);
	for(i=0; i<sz; i++)
	{
		p=new FloatPar();
		member[i]=p;
		sprintf(nameBuf[i], "%d", i+1);
		(*p)(this, nameBuf[i], augFlagBuf, smp?smp+i:0, mn, mx, u, nrm?(nrm[i]):0.0f);
	}
}

void FloatArrayPar::parSetEvent(Parameter *sce){Parameter::parSetEvent(this);}
void FloatArrayPar::parAlteredEvent(Parameter *sce){Parameter::parAlteredEvent(this);}
void FloatArrayPar::parChangedEvent(Parameter *sce){Parameter::parChangedEvent(this);}
void FloatArrayPar::parRangeChangedEvent(Parameter *sce){Parameter::parRangeChangedEvent(this);}
void FloatArrayPar::parConfigChangedEvent(Parameter *sce){Parameter::parConfigChangedEvent(this);}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Integer Array Parameter stuff

void IntegerArrayPar::operator()(Parameter *par, char *n, char *f, Sint32 *smp, int sz,
		bool ver, Integer mn, Integer mx, char *u, Integer *nrm)
{
	int i;
	IntegerPar *p;

	if(sz<0)
		sz=0;
	if(sz>MaxIntArraySize)
		sz=MaxIntArraySize;
	smpBuf=smp;
	arraySz=sz;
	Parameter::operator()(par, n);
	if(ver)
	{
		this->setFlags("h");
		strcpy(augFlagBuf, "u");
		strncat(augFlagBuf, f, 14);
	}
	else
		strncpy(augFlagBuf, f, 15);
	for(i=0; i<sz; i++)
	{
		p=new IntegerPar();
		member[i]=p;
		sprintf(nameBuf[i], "%d", i+1);
		(*p)(this, nameBuf[i], augFlagBuf, smp?smp+i:0, mn, mx, u, nrm?(nrm[i]):0);
	}
}

void IntegerArrayPar::parSetEvent(Parameter *sce){Parameter::parSetEvent(this);}
void IntegerArrayPar::parAlteredEvent(Parameter *sce){Parameter::parAlteredEvent(this);}
void IntegerArrayPar::parChangedEvent(Parameter *sce){Parameter::parChangedEvent(this);}
void IntegerArrayPar::parRangeChangedEvent(Parameter *sce){Parameter::parRangeChangedEvent(this);}
void IntegerArrayPar::parConfigChangedEvent(Parameter *sce){Parameter::parConfigChangedEvent(this);}

#endif
