// Expression.cpp - Expression Class module

#include "C3Std.h"
#ifndef PCTEST

extern "C" {
#include "../include/channel.h"
#include "../include/shared.h"
}

#endif
#ifdef SUPGEN

// NB. SmpExpression::iter does not have access to this table.
// If you add a new function you must add it to SmpExpression::iter too
//  otherwise its use will result in a run time error.
//
// Note that 'if' operator is not included as this is dealt with as a
//  special case.
//
const ExpFuncRec Expression::fnTbl[]={
//	  id			code			type	nargs	ctFunc			val					nstates
	{"tx",			ExpTxFunc,		ExpFunc,	1,	NULL,			0.0f,				0},
	{"cnet",		ExpCnetFunc,	ExpFunc,	1,	NULL,			0.0f,				0},
	{"pi",			0,				ExpK,		0,	NULL,			(float)Pi,			0},
	{"log",			ExpLogFunc,		ExpFunc,	1,	&ctLogFunc,		0.0f,				0},
	{"ln",			ExpLnFunc,		ExpFunc,	1,	&ctLnFunc,		0.0f,				0},
	{"exp",			ExpExpFunc,		ExpFunc,	1,	&ctExpFunc,		0.0f,				0},
	{"pow",			ExpPowFunc,		ExpFunc,	2,	&ctPowFunc,		0.0f,				0},
	{"sin",			ExpSinFunc,		ExpFunc,	1,	&ctSinFunc,		0.0f,				0},
	{"cos",			ExpCosFunc,		ExpFunc,	1,	&ctCosFunc,		0.0f,				0},
	{"tan",			ExpTanFunc,		ExpFunc,	1,	&ctTanFunc,		0.0f,				0},
	{"asin",		ExpAsinFunc,	ExpFunc,	1,	&ctAsinFunc,	0.0f,				0},
	{"acos",		ExpAcosFunc,	ExpFunc,	1,	&ctAcosFunc,	0.0f,				0},
	{"atan",		ExpAtanFunc,	ExpFunc,	2,	&ctAtanFunc,	0.0f,				0},
	{"limit",		ExpLimitFunc,	ExpFunc,	3,	&ctLimitFunc,	0.0f,				0},
	{"squ",			ExpSquFunc,		ExpFunc,	1,	&ctSquFunc,		0.0f,				0},
	{"root",		ExpRootFunc,	ExpFunc,	1,	&ctRootFunc,	0.0f,				0},
	{"diff",		ExpDiffFunc,	ExpFunc,	1,	NULL,			0.0f,				1},
	{"max",			ExpMaxFunc,		ExpFunc,	2,	NULL,			0.0f,				1},
	{"min",			ExpMinFunc,		ExpFunc,	2,	NULL,			0.0f,				1},
	{"user",		ExpUserFunc,	ExpFunc,	1,	NULL,			0.0f,				0},
	{"dband",		ExpDBandFunc,	ExpFunc,	2,	&ctDBandFunc,	0.0f,				0},
	{"proof",		ExpProofFunc,	ExpFunc,	3,	NULL,			0.0f,				5},
	{"derivative",	ExpDerFunc,		ExpFunc,	1,	NULL,			0.0f,				1},
	{"integral",	ExpIntFunc,		ExpFunc,	2,	NULL,			0.0f,				1},
	{"monitor",		ExpMonitorFunc,	ExpFunc,	2,	NULL,			0.0f,				0},
	{"abs",         ExpAbsFunc,     ExpFunc,    1,  &ctAbsFunc,     0.0f,               0},
	{"|",			ExpOrFunc,		ExpOp,		2,	&ctOrFunc,		0.0f,				0},
	{"&",			ExpAndFunc,		ExpOp,		2,	&ctAndFunc,		0.0f,				0},
	{"==",			ExpEquFunc,		ExpOp,		2,	&ctEquFunc,		0.0f,				0},
	{"!=",			ExpNequFunc,	ExpOp,		2,	&ctNequFunc,	0.0f,				0},
	{"<=",			ExpLteFunc,		ExpOp,		2,	&ctLteFunc,		0.0f,				0},
	{"<",			ExpLtFunc,		ExpOp,		2,	&ctLtFunc,		0.0f,				0},
	{">=",			ExpGteFunc,		ExpOp,		2,	&ctGteFunc,		0.0f,				0},
	{">",			ExpGtFunc,		ExpOp,		2,	&ctGtFunc,		0.0f,				0},
	{"+",			ExpAddFunc,		ExpOp,		2,	&ctAddFunc,		0.0f,				0},
	{"-",			ExpSubFunc,		ExpOp,		2,	&ctSubFunc,		0.0f,				0},
	{"*",			ExpMulFunc,		ExpOp,		2,	&ctMulFunc,		0.0f,				0},
	{"/",			ExpDivFunc,		ExpOp,		2,	&ctDivFunc,		0.0f,				0},
	{"-",			ExpNegFunc,		ExpOp,		1,	&ctNegFunc,		0.0f,				0},
	{"!",			ExpNotFunc,		ExpOp,		1,	&ctNotFunc,		0.0f,				0},
	{NULL,		0,				ExpNone,	0,	NULL,			0.0f,				0}
};

void Expression::ctNegFunc(float *p)  {p[0]=-p[0];}
void Expression::ctNotFunc(float *p)  {p[0]=(p[0]==0.0f)?1.0f:0.0f;}
void Expression::ctLogFunc(float *p)  {p[0]=log10f(p[0]);}
void Expression::ctLnFunc(float *p)   {p[0]=logf(p[0]);}
void Expression::ctExpFunc(float *p)  {p[0]=expf(p[0]);}
void Expression::ctSinFunc(float *p)  {p[0]=sinf(p[0]);}
void Expression::ctCosFunc(float *p)  {p[0]=cosf(p[0]);}
void Expression::ctTanFunc(float *p)  {p[0]=tanf(p[0]);}
void Expression::ctAsinFunc(float *p) {p[0]=asinf(p[0]);}
void Expression::ctAcosFunc(float *p) {p[0]=acosf(p[0]);}
void Expression::ctOrFunc(float *p)   {p[0]=((p[0]!=0.0f)||(p[1]!=0.0f))?1.0f:0.0f;}
void Expression::ctAndFunc(float *p)  {p[0]=((p[0]!=0.0f)&&(p[1]!=0.0f))?1.0f:0.0f;}
void Expression::ctEquFunc(float *p)  {p[0]=(p[0]==p[1])?1.0f:0.0f;}
void Expression::ctNequFunc(float *p) {p[0]=(p[0]!=p[1])?1.0f:0.0f;}
void Expression::ctLteFunc(float *p)  {p[0]=(p[0]<=p[1])?1.0f:0.0f;}
void Expression::ctLtFunc(float *p)   {p[0]=(p[0]<p[1])?1.0f:0.0f;}
void Expression::ctGteFunc(float *p)  {p[0]=(p[0]>=p[1])?1.0f:0.0f;}
void Expression::ctGtFunc(float *p)   {p[0]=(p[0]>p[1])?1.0f:0.0f;}
void Expression::ctAddFunc(float *p)  {p[0]+=p[1];}
void Expression::ctSubFunc(float *p)  {p[0]-=p[1];}
void Expression::ctMulFunc(float *p)  {p[0]*=p[1];}
void Expression::ctDivFunc(float *p)  {p[0]/=p[1];}
void Expression::ctPowFunc(float *p)  {p[0]=powf(p[0], p[1]);}
void Expression::ctAtanFunc(float *p) {p[0]=atan2f(p[0], p[1]);}
void Expression::ctIfFunc(float *p)   {p[0]=(p[0]!=0.0f)?p[1]:p[2];}
void Expression::ctLimitFunc(float *p){float v;	v=p[0];if(v<p[1])v=p[1];if(v>p[2])v=p[2];p[0]=v;}
void Expression::ctSquFunc(float *p)  {p[0]=p[0]*p[0];}
void Expression::ctRootFunc(float *p) {p[0]=sqrtf(p[0]);}
void Expression::ctDBandFunc(float *p)
{
	float x;

	x=p[0];
	if(x>0.0f)
	{
		x-=p[1];
		if(x<0.0f)
			x=0.0f;
	}
	else
	{
		x+=p[1];
		if(x>0.0f)
			x=0.0f;
	}
	p[0]=x;
}
void Expression::ctAbsFunc(float *p)  {p[0]=p[0]<0.0f?-p[0]:p[0];}

void Expression::operator()(Parameter *par, char *n, SmpExpression &smp)
{
	static const char *(StatusStr[])={"Expression_okay", "Syntax_error", "Too_complicated",
								"Unknown_identifier", "Argument_count_error", "Run_time_error", NULL};
	static const char *(PriorityStr[])={"Realtime", "Background", NULL};

	smpbs=&smp;
	Parameter::operator()(par, n);
	Priority     (this, "Priority",       "np", Smp(priority), (char **)PriorityStr);
	Formula      (this, "Formula",        "inp", ExpTextSize, "0.0");
	Status       (this, "Status",         "r",  NULL, (char **)StatusStr);
	ErrorPosition(this, "Error_position", "r",  NULL, 0, ExpTextSize);
	Compile      (this, "Compile",        "bp", NULL, 0, 1);

	// Use zero function for now
	instr[0]=ExpPushFunc;
	instr[1]=ExpEndFunc;
	instrcnt=2;
	konst[0]=0.0f;
	konstcnt=1;
	pop=&smpbs->output;
	pGenbus=NULL;
	pMonVec=NULL;
}

void Expression::parPrimeSignal()
{
	double num[2], den[2], g;

	FilterDesign(num, den, 1, FDClassicOrder1, FDHighPass, ExpDerivPlatFreq, 0.0, (double)SysFs, 0.0, False);
	g=(double)SysFs*(den[0]+den[1])/num[0];
	num[0]*=g;
	num[1]*=g;
//	Trace("num= %f %f, den= %f %f", num[0], num[1], den[0], den[1]);
	PokeCoeffs(smpbs->derFilterNum, num, 1);
	PokeCoeffs(smpbs->derFilterDen, den, 1);
	Poke(smpbs->pGenbus, pGenbus);
	Poke(smpbs->pMonVec, pMonVec);
	recompileNow();
}

void Expression::parPollSignal()
{
	Parameter::parPollSignal();
	if(Peek(smpbs->error))
		Status=ExpRunTimeError;
}

void Expression::parRecompileSignal()
{
	recompileNow();
}

void Expression::parSetEvent(Parameter *sce)
{
	if(sce!=&Compile)
		return;
	recompileNow();
}

void Expression::parAlteredEvent(Parameter *sce)
{
	if(sce!=&Formula)
		return;
	recompileNow();
}

void Expression::recompileNow()
{
	Integer sts;

	sts=compile(Formula());
	Status=sts;
	if(!sts)
		optimise();
	ErrorPosition=(pstr-strbs);
#ifdef PCTEST
	decompile();
#endif
	parCompileEvent(this);
}

Result Expression::compile(char *pexpr)
{
	Result r;
	int i;

	// Set up pointers and counts for parsing
	pstr=strbs=pexpr;
	pinstr=instr;
	instrcnt=0;
	pkonst=konst;
	konstcnt=0;
	stackcnt=0;
	statecnt=0;

	// Parse expression
	r=expression();

	// Must be end of expression text
	if(!r && *pstr)
		r=ExpSyntaxError;

	// Space for 'end' instruction?
	if(!r)
		r=addInstr(ExpEndFunc, 0, 0);

	Poke(smpbs->enable, 0);
	if(!r)
	{
		// Download instructions and constants
		for(i=0; i<instrcnt; i++)
			Poke(smpbs->instr[i], instr[i]);
		for(i=0; i<konstcnt; i++)
			Poke(smpbs->konst[i], konst[i]);
		// Clear state variables
		for(i=0; i<ExpMaxState; i++)
			Poke(smpbs->stateVec[i], 0.0f);
		Poke(smpbs->enable, 1);
	}
	else
	{
		// Error - set zero function
		instr[0]=ExpPushFunc;
		instr[1]=ExpEndFunc;
		instrcnt=2;
		konst[0]=0.0f;
		konstcnt=1;
	}

	return r;
}

Result Expression::expression()
{
	Result r;

	// Get first <orexpr>
	if(r=orexpr())
		return r;

	// Look for '?'
	if(!nextIs("?"))
		return ExpOkay;

	// Get True option 
	if(r=orexpr())
		return r;

	// Must be ':' next
	if(!nextIs(":"))
		return ExpSyntaxError;

	// Get False option
	if(r=orexpr())
		return r;

	// Add 'if' instruction 
	if(r=addInstr(ExpIfFunc, -2, 0))
		return r;

	return ExpOkay;
}

Result Expression::orexpr()
{
	Result r;
	bool isor;

	// Get first <andexpr>
	if(r=andexpr())
		return r;
	do
	{
		if(isor=nextIs("|"))
		{
			// Get second <andexpr>
			if(r=andexpr())
				return r;

			// Add 'or' instruction 
			if(r=addInstr(ExpOrFunc, -1, 0))
				return r;
		}
	}
	while(isor);

	return ExpOkay;
}

Result Expression::andexpr()
{
	Result r;
	bool isand;

	// Get first <equexpr>
	if(r=equexpr())
		return r;
	do
	{
		if(isand=nextIs("&"))
		{
			// Get second <equexpr>
			if(r=equexpr())
				return r;

			// Add 'and' instruction 
			if(r=addInstr(ExpAndFunc, -1, 0))
				return r;
		}
	}
	while(isand);

	return ExpOkay;
}

Result Expression::equexpr()
{
	Result r;
	bool isequ, isnequ;

	// Get first <relexpr>
	if(r=relexpr())
		return r;
	do
	{
		isequ=nextIs("=");
		isnequ=nextIs("!=");
		if(isequ||isnequ)
		{
			// Get second <relexpr>
			if(r=relexpr())
				return r;

			// Add 'equ' or 'nequ' instruction
			if(isequ)
			{
				if(r=addInstr(ExpEquFunc, -1, 0))
					return r;
			}
			else
			{
				if(r=addInstr(ExpNequFunc, -1, 0))
					return r;
			}
		}
	}
	while(isequ||isnequ);

	return ExpOkay;
}

Result Expression::relexpr()
{
	Result r;
	bool islt, islte, isgt, isgte;

	// Get first <addexpr>
	if(r=addexpr())
		return r;
	do
	{
		islte=nextIs("<=");
		islt=nextIs("<");
		isgte=nextIs(">=");
		isgt=nextIs(">");
		if(islte||islt||isgte||isgt)
		{
			// Get second <addexpr>
			if(r=addexpr())
				return r;

			// Add 'lte', 'lt', 'gte' or 'gt' instruction
			if(islte)
			{
				if(r=addInstr(ExpLteFunc, -1, 0))
					return r;
			}
			else if(islt)
			{
				if(r=addInstr(ExpLtFunc, -1, 0))
					return r;
			}
			else if(isgte)
			{
				if(r=addInstr(ExpGteFunc, -1, 0))
					return r;
			}
			else if(isgt)
			{
				if(r=addInstr(ExpGtFunc, -1, 0))
					return r;
			}
		}
	}
	while(islte||islt||isgte||isgt);

	return ExpOkay;
}

Result Expression::addexpr()
{
	Result r;
	bool isadd, issub;

	// Get first <mulexpr>
	if(r=mulexpr())
		return r;
	do
	{
		isadd=nextIs("+");
		issub=nextIs("-");
		if(isadd||issub)
		{
			// Get second <mulexpr>
			if(r=mulexpr())
				return r;

			// Add 'add' or 'sub' instruction
			if(isadd)
			{
				if(r=addInstr(ExpAddFunc, -1, 0))
					return r;
			}
			else
			{
				if(r=addInstr(ExpSubFunc, -1, 0))
					return r;
			}
		}
	}
	while(isadd||issub);

	return ExpOkay;
}

Result Expression::mulexpr()
{
	Result r;
	bool ismul, isdiv;

	// Get first <term>
	if(r=term())
		return r;
	do
	{
		ismul=nextIs("*");
		isdiv=nextIs("/");
		if(ismul||isdiv)
		{
			// Get second <term>
			if(r=term())
				return r;

			// Add 'mul' or 'div' instruction
			if(ismul)
			{
				if(r=addInstr(ExpMulFunc, -1, 0))
					return r;
			}
			else
			{
				if(r=addInstr(ExpDivFunc, -1, 0))
					return r;
			}
		}
	}
	while(ismul||isdiv);

	return ExpOkay;
}

Result Expression::term()
{
	Result r;

	// Unary '+'?
	if(nextIs("+"))
		return item();

	// Unary '-'?
	if(nextIs("-"))
	{
		if(r=item())
			return r;

		if(r=addInstr(ExpNegFunc, 0, 0))
			return r;

		return ExpOkay;
	}

	// Unary '!'?
	if(nextIs("!"))
	{
		if(r=item())
			return r;

		if(r=addInstr(ExpNotFunc, 0, 0))
			return r;

		return ExpOkay;
	}

	// Just plain old item then
	return item();
}

Result Expression::item()
{
	Result r;
	float val;
	int n, m;
	ExpFuncRec *p;

	// Parentheses?
	if(nextIs("("))
	{
		if(r=expression())
			return r;
		if(nextIs(")"))
			return ExpOkay;
		else
			return ExpSyntaxError;
	}
	skipws();

	// Number?
	if(isdigit(*pstr))
	{
		// Use sscanf to convert number
		if(!sscanf(pstr, "%f", &val))
			return ExpSyntaxError;

		// sscanf doesn't tell us where the number ends.
		// We must work that out for ourselves.
		while(isdigit(*pstr))
			pstr++;
		if(*pstr=='.')
		{
			pstr++;
			while(isdigit(*pstr))
				pstr++;
		}
		if((*pstr=='e')||(*pstr=='E'))
		{
			pstr++;
			if(*pstr=='+')
				pstr++;
			else if(*pstr=='-')
				pstr++;
			while(isdigit(*pstr))
				pstr++;
		}

		// Add new constant
		if(r=addKonst(val))
			return r;

		return ExpOkay;
	}

	// Must be an identifier then
	if(!(isalpha(*pstr)||(*pstr=='_')))
		return ExpSyntaxError;

	// Determine length of identifier
	n=0;
	while(isalnum(pstr[n])||(pstr[n]=='_'))
		n++;

	// Try to find it in function table
	p=(ExpFuncRec *)fnTbl;
	while((p->id) && ((strlen(p->id)!=(unsigned)n) || strncmp(p->id, pstr, n)))
		p++;
	if(p->id)
	{
		pstr+=n;

		// Count arguments
		n=0;
		if(nextIs("(") && !nextIs(")"))
		{
			if(r=arglist(n))
				return r;
			if(!nextIs(")"))
				return ExpSyntaxError;
		}
		if(n!=p->nargs)
			return ExpArgumentCountError;

		// Constant?
		if(p->type==ExpK)
			return addKonst(p->val); // Yes - commute to constant
		else
			return addInstr(p->code, 1-p->nargs, p->nstates);
	}

#if (defined(CONTROLCUBE) || defined(SIGNALCUBE) || defined(AICUBE))
	// Transducer name?
	if(pSignals&&(m=pSignals->FindSignalBySymbol(pstr, n)))
	{
		pstr+=n;
		if(r=addKonst((float)m))
			return r;
		return addInstr(ExpTxFunc, 0, 0);
	}
#endif

#ifdef CONTROLCUBE
	// Monitor point?
	m=0;
	while(ErrorPath::MonitorStr[m])
	{
		if(!strncmp(pstr,ErrorPath::MonitorStr[m],n)&&(ErrorPath::MonitorStr[m][n]==NULL))
		{
			pstr+=n;
			return addKonst((float)m); // Yes - commute to constant
		}
		m++;
	}
#endif

	return ExpIdentifierUnknown;
}

Result Expression::arglist(int &n)
{
	Result r;

	n=0;
	do
	{
		// Parse argument expression
		if(r=expression())
			return r;
		n++;
	}
	while(nextIs(","));

	return ExpOkay;
}

void Expression::skipws()
{
	while(*pstr && *pstr<=' ')
		pstr++;
}

bool Expression::nextIs(char *s)
{
	size_t n;

	skipws();
	n=strlen(s);
	if(strncmp(s, pstr, n)==0)
	{
		pstr+=n;
		return True;
	}
	else
		return False;
}

Result Expression::addInstr(Sint32 i, int usage, int nstates)
{
	// Space for instruction?
	if(instrcnt>=ExpMaxInstr)
		return ExpTooComplicated;

	// Space for states?
	statecnt+=nstates;
	if(statecnt>ExpMaxState)
		return ExpTooComplicated;

	// Add instruction
	*pinstr++=i;
	instrcnt++;

	// Check stack usage
	stackcnt+=usage;
	if(stackcnt>=ExpStackSz)
		return ExpTooComplicated;

	return ExpOkay;
}

Result Expression::addKonst(float k)
{
	Result r;

	// Space for constant?
	if(konstcnt>=ExpMaxKonst)
		return ExpTooComplicated;

	// Add constant
	*pkonst++=k;
	konstcnt++;

	// Add 'push' instruction
	if(r=addInstr(ExpPushFunc, 1, 0))
		return r;

	return ExpOkay;
}

// This optimiser cruches out unnecessary operations on
//  constants, e.g. sequences like push-push-add
//
// Note that we don't optimise the 'if' operator this way.
//
void Expression::optimise()
{
	Sint32 i, *p;
	int pushcnt, j;
	ExpFuncRec *q;
	float *r, k;

	pinstr=instr;
	pkonst=konst;
	pushcnt=0;
	while(i=*pinstr++)
	{
		if(i==ExpPushFunc)
		{
			pushcnt++;
			pkonst++;
		}
		else
		{
			// Try to find function code in function table
			q=(ExpFuncRec *)fnTbl;
			while((q->id) && (i!=q->code))
				q++;

			// No reason we shouldn't find it but, well...
			if(!q->id)
			{
				pushcnt=0;
				continue;
			}

			// Can't optimise function that has no compile time function
			if(!q->ctFunc)
			{
				pushcnt=0;
				continue;
			}

			// Can't optimise if any non-constant arguments
			if(pushcnt<q->nargs)
			{
				pushcnt=0;
				continue;
			}

			// Delete function code and all but one push
			p=pinstr;
			j=q->nargs;
			do
			{
				i=p[0];
				p[-j]=i;
				p++;
			}
			while(i);
			pinstr-=j;
			instrcnt-=j;

			// Replace constants with function result
			(*(q->ctFunc))(pkonst-q->nargs);
			if((j=q->nargs-1)==0)
				continue;
			r=pkonst;
			while(r<(konst+konstcnt))
			{
				k=r[0];
				r[-j]=k;
				r++;
			}
			pkonst-=j;
			konstcnt-=j;
			pushcnt-=j;
		}
	}
}

#ifdef PCTEST

void Expression::decompile()
{
	Sint32 i;
	ExpFuncRec *p;
	FILE *fp;

	// Set up the string stack
	for(i=0; i<=ExpStackSz; i++)
		decomStack[i]=decomStackEl[i];

	// Execute compiled code using strings
	pinstr=instr;
	pkonst=konst;
	decomStackPtr=decomStack;
	while(i=*pinstr++)
	{
		if(i==ExpPushFunc)
		{
			sprintf(*decomStackPtr++, "%g", *pkonst++);
			continue;
		}

		// Special case - 'if' operator
		if(i==ExpIfFunc)
		{
			decomIfOp();
			continue;
		}

		// Try to find function code in function table
		p=(ExpFuncRec *)fnTbl;
		while((p->id) && (i!=p->code))
			p++;
		if(!p->id)
			continue;

		// Deal with operator or function
		if(p->type==ExpOp)
			decomOp(p->id, p->nargs);
		else
			decomFunc(p->id, p->nargs);
	}
	if(fp=fopen("Exp.txt", "w"))
	{
		fwrite(decomStackPtr[-1], strlen(decomStackPtr[-1]), 1, fp);
		fwrite("\n", 1, 1, fp);
		fclose(fp);
	}
}

void Expression::decomOp(char *s, int n)
{
	char *p;

	switch(n)
	{
	case 1:
		sprintf(*decomStackPtr, "%s(%s)", s, decomStackPtr[-1]);
		break;
	case 2:
		sprintf(*decomStackPtr, "(%s)%s(%s)", decomStackPtr[-2], s, decomStackPtr[-1]);
		break;
	}
	decomStackPtr-=(n-1);
	p=decomStackPtr[-1];
	decomStackPtr[-1]=decomStackPtr[n-1];
	decomStackPtr[n-1]=p;
}

void Expression::decomIfOp()
{
	char *p;

	sprintf(*decomStackPtr, "(%s)?(%s):(%s)", decomStackPtr[-3], decomStackPtr[-2], decomStackPtr[-1]);
	decomStackPtr-=2;
	p=decomStackPtr[-1];
	decomStackPtr[-1]=decomStackPtr[2];
	decomStackPtr[2]=p;
}

void Expression::decomFunc(char *s, int n)
{
	char *p;
	int i;

	p=*decomStackPtr;
	p+=sprintf(p, "%s(", s);
	i=n;
	if(i)
	{
		p+=sprintf(p, "%s", decomStackPtr[-i]);
		i--;
	}
	while(i)
	{
		p+=sprintf(p, ", %s", decomStackPtr[-i]);
		i--;
	}
	sprintf(p, ")");
	decomStackPtr-=(n-1);
	p=decomStackPtr[-1];
	decomStackPtr[-1]=decomStackPtr[n-1];
	decomStackPtr[n-1]=p;
}

#endif

void ExpUserInput::parPrimeSignal()
{
	Parameter::parPrimeSignal();		// Important - prime parameters first!
	newRange();
}

void ExpUserInput::parRangeChangedEvent(Parameter *sce)
{
	if(sce==&Range)
		newRange();
}

void ExpUserInput::newRange()
{
	Float fs;
	Integer um;

	fs=Range.FullScale();
	um=Range.UnipolarMode();
	broadcastRange(this, fs, um, Range.Units());
}

void ExpressionArray::operator()(Parameter *par, char *n, SmpExpressionArray &smp)
{
	Expression *p;
	ExpUserInput *q;
	int i;

	smpbs=&smp;
	Parameter::operator()(par, n);
	Members(this, "Formulae");
	for(i=0, p=Members.Member; i<ExpNExp; i++, p++)
	{
		sprintf(vtn[i], "%d", i+1);
		(*p)(&Members, vtn[i], smpbs->channel[i]);
	}
	UserInputs(this, "User_Inputs");
	for(i=0, q=UserInputs.Member; i<ExpNUser; i++, q++)
	{
		sprintf(q->name, "%d", i+1);
		(*q)(&UserInputs, q->name);
		q->Range  (q, "Range", "ny");
		q->Setting(q, "Setting", "mny", smpbs->userInput+i, MinFloat, MaxFloat);
	}
	pGenbus=NULL;
	for(i=0; i<ExpMaxServoCh; i++)
		pServoChannel[i]=NULL;
}

void ExpressionArray::parPrimeSignal()
{
	int i, j, k;
	SmpErrorPath *p;

	for(i=0; i<ExpNExp; i++)
	{
		Members.Member[i].pSignals=pSignals;
		Members.Member[i].pGenbus=pGenbus;
		Members.Member[i].pMonVec=smpbs->monitorVec;
		Poke(smpbs->channel[i].ppInput, ppInput);
		Poke(smpbs->channel[i].pUser, smpbs->userInput);
	}
#if defined(CONTROLCUBE) || defined(AICUBE)
	for(i=0; i<ExpMaxServoCh; i++)
	{
		for(j=0; j<ExpMaxMon; j++)
		{
			p=pServoChannel[i];
			k=0;
			while(ErrorPath::MonitorStr[k])
				k++;
			if((!p)||((j+1)>=k))
				Poke(smpbs->monitorVec[ExpMaxMon*i+j], NULL);
			else
				Poke(smpbs->monitorVec[ExpMaxMon*i+j], &(p->*(ErrorPath::MonitorOffset[j+1])));
		}
#ifndef PCTEST
		Poke(smpbs->monitorVec[ExpMaxMon*i+6], (float *)get_channel_base(chanptr(512+i)));	/* SN */
#endif
	}
#endif
	Parameter::parPrimeSignal();
}

#endif

#ifdef SMPGEN

void SmpExpression::iter(Sint32 pri)
{
	register Sint32 *pinstr, i, j;
	register float *pstack, *pkonst, *pfloat;
	register double *pstate;
	Sint32 errflag;
	float x;
	double e, k;

	if(!enable)
	{
		output=0.0f;
		return;
	}
	if(pri!=priority)
		return;
	pinstr=instr;
	pkonst=konst;
	pstack=evalStack;
	pstate=stateVec;
	errflag=False;
	while(!errflag && (i=*pinstr++))
		switch(i)
		{
		case ExpIfFunc:
			pstack-=2;
			pstack[-1]=(pstack[-1]!=0.0f)?pstack[0]:pstack[1];
			break;
		case ExpOrFunc:
			pstack-=1;
			pstack[-1]=((pstack[-1]!=0.0f)||(pstack[0]!=0.0f))?1.0f:0.0f;
			break;
		case ExpAndFunc:
			pstack-=1;
			pstack[-1]=((pstack[-1]!=0.0f)&&(pstack[0]!=0.0f))?1.0f:0.0f;
			break;
		case ExpEquFunc:
			pstack-=1;
			pstack[-1]=(pstack[-1]==pstack[0])?1.0f:0.0f;
			break;
		case ExpNequFunc:
			pstack-=1;
			pstack[-1]=(pstack[-1]!=pstack[0])?1.0f:0.0f;
			break;
		case ExpLteFunc:
			pstack-=1;
			pstack[-1]=(pstack[-1]<=pstack[0])?1.0f:0.0f;
			break;
		case ExpLtFunc:
			pstack-=1;
			pstack[-1]=(pstack[-1]<pstack[0])?1.0f:0.0f;
			break;
		case ExpGteFunc:
			pstack-=1;
			pstack[-1]=(pstack[-1]>=pstack[0])?1.0f:0.0f;
			break;
		case ExpGtFunc:
			pstack-=1;
			pstack[-1]=(pstack[-1]>pstack[0])?1.0f:0.0f;
			break;
		case ExpAddFunc:
			pstack-=1;
			pstack[-1]+=pstack[0];
			break;
		case ExpSubFunc:
			pstack-=1;
			pstack[-1]-=pstack[0];
			break;
		case ExpMulFunc:
			pstack-=1;
			pstack[-1]*=pstack[0];
			break;
		case ExpDivFunc:
			pstack-=1;
			pstack[-1]/=pstack[0];
			break;
		case ExpNegFunc:
			pstack[-1]=-pstack[-1];
			break;
		case ExpNotFunc:
			pstack[-1]=(pstack[-1]==0.0f)?1.0f:0.0f;
			break;
		case ExpPushFunc:
			*pstack++=*pkonst++;
			break;
		case ExpLogFunc:
			pstack[-1]=log10f(pstack[-1]);
			break;
		case ExpLnFunc:
			pstack[-1]=logf(pstack[-1]);
			break;
		case ExpExpFunc:
			pstack[-1]=expf(pstack[-1]);
			break;
		case ExpPowFunc:
			pstack-=1;
			pstack[-1]=powf(pstack[-1], pstack[0]);
			break;
		case ExpSinFunc:
			pstack[-1]=sinf(pstack[-1]);
			break;
		case ExpCosFunc:
			pstack[-1]=cosf(pstack[-1]);
			break;
		case ExpTanFunc:
			pstack[-1]=tanf(pstack[-1]);
			break;
		case ExpAsinFunc:
			pstack[-1]=asinf(pstack[-1]);
			break;
		case ExpAcosFunc:
			pstack[-1]=acosf(pstack[-1]);
			break;
		case ExpAtanFunc:
			pstack-=1;
			pstack[-1]=atan2f(pstack[-1], pstack[0]);
			break;
		case ExpLimitFunc:
			pstack-=2;
			if(pstack[-1]<pstack[0])
				pstack[-1]=pstack[0];
			if(pstack[-1]>pstack[1])
				pstack[-1]=pstack[1];
			break;
		case ExpTxFunc:
			i=(Sint32)pstack[-1]-1;
			if((i<0)||(i>=SigNCh)||!ppInput)
				pstack[-1]=0.0f;
			else
				pstack[-1]=*(ppInput[i]);
			break;
		case ExpCnetFunc:
			i=(Sint32)pstack[-1]; // get argument value (in this case Cnet slot)
			if((i<0)||(i>255))
				pstack[-1]=0.0f;
			else
				pstack[-1]=((float*) 0xB0002000)[i]; //Cnet base address (Simons idea, don't blame me)
			break;
		case ExpSquFunc:
			pstack[-1]=pstack[-1]*pstack[-1];
			break;
		case ExpRootFunc:
			pstack[-1]=sqrtf(pstack[-1]);
			break;
		case ExpDiffFunc:
			pfloat=(float *)pstate;
			x=pstack[-1]-*pfloat;
			*pfloat=pstack[-1];
			pstack[-1]=x;
			pstate++;
			break;
		case ExpUserFunc:
			i=(Sint32)pstack[-1]-1;
			if((i<0)||(i>=ExpNUser)||!pUser)
				pstack[-1]=0.0f;
			else
				pstack[-1]=pUser[i];
			break;
		case ExpMinFunc:
			pstack-=1;
			pfloat=(float *)pstate;
			if(pstack[0]>0.5f)
				*pfloat=pstack[-1];
			else
			{
				if(pstack[-1]<*pfloat)
					*pfloat=pstack[-1];
				pstack[-1]=*pfloat;
			}
			pstate++;
			break;
		case ExpMaxFunc:
			pstack-=1;
			pfloat=(float *)pstate;
			if(pstack[0]>0.5f)
				*pfloat=pstack[-1];
			else
			{
				if(pstack[-1]>*pfloat)
					*pfloat=pstack[-1];
				pstack[-1]=*pfloat;
			}
			pstate++;
			break;
		case ExpDBandFunc:
			pstack-=1;
			x=pstack[-1];
			if(x>0.0f)
			{
				x-=pstack[0];
				if(x<0.0f)
					x=0.0f;
			}
			else
			{
				x+=pstack[0];
				if(x>0.0f)
					x=0.0f;
			}
			pstack[-1]=x;
			break;
		case ExpProofFunc:
			// Args: x y threshold
			// States (doubles): Sxx Syy Sxy MaxS/N BestK
			pstack-=2;
			if(pGenbus&&((((GenGenBus *)pGenbus)->xcode & GenXcodeMask)==GenTransitioning))
			{
				pstate[0]+=(double)(pstack[-1]*pstack[-1]);
				pstate[1]+=(double)(pstack[0]*pstack[0]);
				pstate[2]+=(double)(pstack[-1]*pstack[0]);
				if((fabs(pstack[0])>pstack[1])&&(pstate[1]>0.0))
				{
					k=pstate[2]/pstate[1];
					e=pstate[0]-2*k*pstate[2]+k*k*pstate[1];
					if(e>0.0)
					{
						e=pstate[0]/e;
						if(e>pstate[3])
						{
							pstate[3]=e;
							pstate[4]=k;
						}
					}
					pstack[-1]-=((float)pstate[4])*pstack[0];
				}
				else
					pstack[-1]=0.0f;
			}
			else
			{
				pstate[0]=0.0;
				pstate[1]=0.0;
				pstate[2]=0.0;
				pstate[3]=0.0;
				pstate[4]=0.0;
				pstack[-1]=0.0f;
			}
			pstate+=5;
			break;
		case ExpDerFunc:
			pstack[-1]=1e-4*Filter(derFilterNum, derFilterDen, (float *)pstate, 1, pstack[-1]);
			pstate++;
			break;
		case ExpIntFunc:
			pstack-=1;
			pfloat=(float *)pstate;
			if(pstack[0]>0.5f)
				*pfloat=0.0f;
			else
				*pfloat+=ExpIntFac*pstack[-1];
			Clamp(*pfloat, -1.0f, 1.0f);
			pstack[-1]=*pfloat;
			pstate++;
			break;
		case ExpMonitorFunc:
			pstack-=1;
			i=((Sint32)pstack[-1])-1;
			j=((Sint32)pstack[0])-1;
			if((i<0)||(i>=ExpMaxServoCh)||(j<0)||(j>=ExpMaxMon)||(!pMonVec)||(!(pfloat=pMonVec[ExpMaxMon*i+j])))
				pstack[-1]=0.0f;
			else
				pstack[-1]=*pfloat;
			break;
		case ExpAbsFunc:
			x=pstack[-1];
			pstack[-1]=x<0.0f?-x:x;
			break;
		default:
			errflag=True;
		}
	output=errflag?0.0f:pstack[-1];	// Zero output if error
	error=errflag;
	Clamp(output, -1.1f, 1.1f);		// Allow 10% over-range
}

#endif
