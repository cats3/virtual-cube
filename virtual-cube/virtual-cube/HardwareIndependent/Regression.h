// Regression.h - Regression Class header

typedef struct {
	Uint32 nSamples;
	double sigmaX;
	double sigmaY;
	double sigmaXX;
	double sigmaXY;
	double sigmaYY;
} Moments;

#ifdef REGROTEST
typedef struct {
	double x,y;
} TestSample;
TestSample testData[];
#endif

#define	RegDecayFactor	(1e-7f)

class SmpRegression {
public:
	void iter()
	{
		double xval, yval, dec;
		float vio;
		Uint32 en;

		en = (autoctrl) ? (pGenBusIn && ((pGenBusIn->xcode & GenXcodeMask) == GenTransitioning)) : enable;

		if(en&&pTx)
		{
#ifdef REGROTEST
			if(dataIndex<1000)
			{
				xval=testData[dataIndex].x;
				yval=testData[dataIndex].y;
				live.nSamples++;
				live.sigmaX+=xval;
				live.sigmaY+=yval;
				live.sigmaXX+=(xval*xval);
				live.sigmaXY+=(xval*yval);
				live.sigmaYY+=(yval*yval);
				dataIndex++;
			}
#else
			xval=(double)pTx[xTx-1].output;
			yval=(double)pTx[yTx-1].output;
			live.nSamples++;
			live.sigmaX+=xval;
			live.sigmaY+=yval;
			live.sigmaXX+=(xval*xval);
			live.sigmaXY+=(xval*yval);
			live.sigmaYY+=(yval*yval);
#endif
		dec=1.0-(double)decay;
		live.sigmaX*=dec;
		live.sigmaY*=dec;
		live.sigmaXX*=dec;
		live.sigmaXY*=dec;
		live.sigmaYY*=dec;
		}
		if(snap)
		{
			snapShot.nSamples=live.nSamples;
			snapShot.sigmaX  =live.sigmaX;
			snapShot.sigmaY  =live.sigmaY;
			snapShot.sigmaXX =live.sigmaXX;
			snapShot.sigmaXY =live.sigmaXY;
			snapShot.sigmaYY =live.sigmaYY;
			snap=0;
		}
		limit.iter();
		vio=(float)fabs(mlimitVal-mlimitRef);
		limit.update((vio>=mlimitThr)?1:0, vio);
	}
	Sint32 enable, snap;
	Sint32 autoctrl;
	Uint32 xTx, yTx;
	float decay;
	Moments live, snapShot;
	SmpSigInputChannel *pTx;
	GenGenBus *pGenBusIn;
#ifdef REGROTEST
	Sint32 dataIndex;
#endif
	float mlimitThr, mlimitRef, mlimitVal;
	SmpLimit limit;
};

#ifdef SUPGEN

class RegMLimit: public Parameter {
public:
	FloatPar   Threshold;
	FloatPar   Reference;
	IntegerPar SetReference;
	Limit      MEstLimit;
};

class Regression: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpRegression &smp);
	TextPar     Model;
	EnumPar     Enable;
	EnumPar		AutoCtrl;
	IntegerPar  Reset;
	UnsignedPar XTx;
	UnsignedPar YTx;
	FloatPar    Decay;
	UnsignedPar NSamples;
	FloatPar    SigmaX;
	FloatPar    SigmaY;
	FloatPar    SigmaXX;
	FloatPar    SigmaXY;
	FloatPar    SigmaYY;
	FloatPar	MEstimate;
	FloatPar	CEstimate;
	FloatPar    RMSError;
	FloatPar    RMSY;
	FloatPar    RMSYminusc;
	RegMLimit   MLimit;
	SmpSigInputChannel *pTx;
	GenGenBus	*pGenBusIn;
private:
	virtual void parPrimeSignal();
	virtual void parPollSignal();
	virtual void parSetEvent(Parameter *sce);
	void reset();
	SmpRegression *smpbs;
};

#endif
