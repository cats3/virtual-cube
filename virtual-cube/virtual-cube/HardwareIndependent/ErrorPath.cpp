// ErrorPath.cpp - Error Path Class module

#include "C3Std.h"

#ifdef SUPGEN

#ifdef EXTERNALSIMULATION
void SetExtSimEn(void *p, Sint32 val);
void SetExtSimPars(void *p, float *simOp, float pgain, float ngain);
#endif

void ErrorPath::operator()(Parameter *par, char *n, SmpErrorPath &smp)
{
	static const char *(AbleStr[])        ={"Disabled", "Enabled", NULL};
	static const char *(TestBiasModeStr[])={"Replace_set_point", "Augment_set_point", NULL};
	static const char *(CmdTransferModeStr[])={"Cross_fade", "Instant", NULL};
	static const char *(DMHActionStr[])   = {"None", "Set_point_cmd", NULL};
	static const char *(UACDirnStr[])     ={"Positive", "Negative", NULL};
	EPModeControlClass *p;
	Parameter *r;
	int i;

	smpbs=&smp;
	Parameter::operator()(par, n);
	Name           (this, "Name",              "ny",   EPNameSz,     "None");
	//#ifndef AICUBE
		Enable         (this, "Enable",            "npyz", Smp(enable),  (char **)AbleStr);
		QuasiStatic    (this, "Quasi_Static",      "npyz", Smp(quasi),  (char **)AbleStr); 
		ILTuning       (this, "Inner_loop_tuning", "rdl",  Smp(ILTune),  0, 1, "yellow");
		ServoEnable    (this, "Servo_enabled",     "rdl",  Smp(servoen), 0, 1, "yellow");
		CascadeEnable  (this, "Cascade_enabled",   "rdl",  Smp(cascen),  0, 1, "yellow");
		Error          (this, "Error",             "rdv",  Smp(error),   -100.0f, 100.0f, "%");
		ValveDeflection(this, "Valve_deflection",  "rdv",  Smp(output),  -100.0f, 100.0f, "%");
	/*#else
		Enable         (this, "Enable",            "npyz", NULL,  (char **)AbleStr);
		QuasiStatic    (this, "Quasi_Static",      "npyz", NULL,  (char **)AbleStr); 
		ILTuning       (this, "Inner_loop_tuning", "rdl",  NULL,  0, 1, "yellow");
		ServoEnable    (this, "Servo_enabled",     "rdl",  NULL, 0, 1, "yellow");
		CascadeEnable  (this, "Cascade_enabled",   "rdl",  NULL,  0, 1, "yellow");
		Error          (this, "Error",             "rdv",  NULL,   -100.0f, 100.0f, "%");
		ValveDeflection(this, "Valve_deflection",  "rdv",  NULL,  -100.0f, 100.0f, "%");
	#endif*/
	
	FlowLimit(this, "Flow_Limit");
	FlowLimit.setFlags("g");
	#ifndef AICUBE
		FlowLimit.FlowLimited(&FlowLimit, "Flow_limited", "rdl", Smp(flowlimon), 0, 1, "yellow");
		FlowLimit.Level      (&FlowLimit, "Level",        "nvy", Smp(flowlimlevel), 5.0f, 100.0f, "%", 12.5f);
	#else
		FlowLimit.FlowLimited(&FlowLimit, "Flow_limited", "rdl", NULL, 0, 1, "yellow");
		FlowLimit.Level      (&FlowLimit, "Level",        "nvy", NULL, 5.0f, 100.0f, "%", 12.5f);
	#endif

	UAC(this, "Unequal_Area_Comp");
	#ifndef AICUBE
		UAC.Direction(&UAC, "Direction", "ny",  Smp(UACDirn), (char **)UACDirnStr);
		UAC.Gain     (&UAC, "Gain",      "nvy", Smp(UACGain), 0.01f, 100.0f, "%", 100.0f);
	#else
		UAC.Direction(&UAC, "Direction", "ny",  NULL, (char **)UACDirnStr);
		UAC.Gain     (&UAC, "Gain",      "nvy", NULL, 0.01f, 100.0f, "%", 100.0f);
	#endif

	InnerLoop(this, "Inner_Loop");
	InnerLoop.setFlags("g");
	#ifndef AICUBE
		InnerLoop.Enable    (&InnerLoop, "Enable",      "yz",  Smp(ilen), (char **)AbleStr);
		InnerLoop.DeltaPGain(&InnerLoop, "DeltaP_gain", "sny", Smp(deltaPGain), 0.0f, 100.0f, "%");
	#else
		InnerLoop.Enable    (&InnerLoop, "Enable",      "yz",  NULL, (char **)AbleStr);
		InnerLoop.DeltaPGain(&InnerLoop, "DeltaP_gain", "sny", NULL, 0.0f, 100.0f, "%");
	#endif
	InnerLoop.DeltaPGain.setMap(0.01f);

	ModeChange(this, "Mode_Change");
	#ifndef AICUBE
		ModeChange.ChangeOnStop    (&ModeChange, "Change_on_stop",     "ny", Smp(changeOnStop), (char **)AbleStr);
		ModeChange.TargetMode      (&ModeChange, "Target_mode",        "ny", Smp(targetMode),   1, EPNMode, "", 1);
		ModeChange.RelaxSetPoint   (&ModeChange, "Relax_set_point",    "ny", Smp(relaxSp),      (char **)AbleStr);
		ModeChange.StartModeAutoset(&ModeChange, "Start_mode_autoset", "ny", NULL,              (char **)AbleStr, 1);
		ModeChange.StartMode       (&ModeChange, "Start_up_mode",      "n",  Smp(startMode),    1, EPNMode, "", 1);
	#else
		ModeChange.ChangeOnStop    (&ModeChange, "Change_on_stop",     "ny", NULL, (char **)AbleStr);
		ModeChange.TargetMode      (&ModeChange, "Target_mode",        "ny", NULL,   1, EPNMode, "", 1);
		ModeChange.RelaxSetPoint   (&ModeChange, "Relax_set_point",    "ny", NULL, (char **)AbleStr);
		ModeChange.StartModeAutoset(&ModeChange, "Start_mode_autoset", "ny", NULL, (char **)AbleStr, 1);
		ModeChange.StartMode       (&ModeChange, "Start_up_mode",      "n",  NULL,   1, EPNMode, "", 1);
	#endif

	for(i=0; i<EPNMode; i++)
		Simulation.SimulationParameters.modeGainNorm[i]=1.0f;
	Simulation(this, "Simulation");
	Simulation.setFlags("g");
	r=(Parameter *)&Simulation;
	Simulation.Enable(r, "Enabled", "rdl", NULL, 0, 1, "yellow");

	Simulation.SimulationParameters(r, "Simulation_Parameters");
	r=(Parameter *)&Simulation.SimulationParameters;
	#ifndef AICUBE
		Simulation.SimulationParameters.ActuatorGain    (r, "Actuator_gain",          "sny", Smp(aGain),        0.01f, 100.0f,  "(0.01..100)",     10.0f);
		Simulation.SimulationParameters.NaturalFrequency(r, "Natural_frequency",      "ny",  NULL,              0.01f, 1000.0f, "(0.01..1000) Hz", 100.0f);  
		Simulation.SimulationParameters.DampingRatio    (r, "Damping_ratio",          "ny",  NULL,              0.01f, 1.0f,    "(0.01..1)",       0.7f);
		Simulation.SimulationParameters.ModeArrayPos    (r, "FB_Positive_Deflection", "sny", *Smp(modeGainPos), EPNMode, false, 0.0f, 100.0f, "(0..100)", Simulation.SimulationParameters.modeGainNorm);
		Simulation.SimulationParameters.ModeArrayNeg    (r, "FB_Negative_Deflection", "sny", *Smp(modeGainNeg), EPNMode, false, 0.0f, 100.0f, "(0..100)", Simulation.SimulationParameters.modeGainNorm);
		Simulation.SimulationParameters.NoiseLevel      (r, "Noise_level",            "sny", Smp(noiseRange),   0.0f,  100.0f,  "(0.0..100)",      0.0f);
	#else
		Simulation.SimulationParameters.ActuatorGain    (r, "Actuator_gain",          "sny", NULL,              0.01f, 100.0f,  "(0.01..100)",     10.0f);
		Simulation.SimulationParameters.NaturalFrequency(r, "Natural_frequency",      "ny",  NULL,              0.01f, 1000.0f, "(0.01..1000) Hz", 100.0f);  
		Simulation.SimulationParameters.DampingRatio    (r, "Damping_ratio",          "ny",  NULL,              0.01f, 1.0f,    "(0.01..1)",       0.7f);
		Simulation.SimulationParameters.ModeArrayPos    (r, "FB_Positive_Deflection", "sny", NULL, EPNMode, false, 0.0f, 100.0f, "(0..100)", Simulation.SimulationParameters.modeGainNorm);
		Simulation.SimulationParameters.ModeArrayNeg    (r, "FB_Negative_Deflection", "sny", NULL, EPNMode, false, 0.0f, 100.0f, "(0..100)", Simulation.SimulationParameters.modeGainNorm);
		Simulation.SimulationParameters.NoiseLevel      (r, "Noise_level",            "sny", NULL,              0.0f,  100.0f,  "(0.0..100)",      0.0f);
	#endif
	Simulation.SimulationParameters.ModeArrayPos.setFlags("g");
	Simulation.SimulationParameters.ModeArrayNeg.setFlags("g");

	#ifndef AICUBE
		TestBiasMode   (this, "Test_bias_mode",         "ny", Smp(augment),     (char **)TestBiasModeStr);
		CmdTransferMode(this, "Cmd_transfer_mode",      "ny", NULL,             (char **)CmdTransferModeStr);
		DMHAction      (this, "Deadman_action",         "ny", Smp(dmhSP),       (char **)DMHActionStr);
	#else
		TestBiasMode   (this, "Test_bias_mode",         "ny", NULL, (char **)TestBiasModeStr);
		CmdTransferMode(this, "Cmd_transfer_mode",      "ny", NULL, (char **)CmdTransferModeStr);
		DMHAction      (this, "Deadman_action",         "ny", NULL, (char **)DMHActionStr);
	#endif
	CNetCmdTimeSlot(this, "CNet_cmd_slot",          "ny", Smp(cNetCmdSlot), 0, C3NTS-1, "", 0);

	CommandSource(this, "Command_Source");
	CommandSource.setFlags("g");
	//#ifndef AICUBE //cmdSce needs to be there for some reason.
		CommandSource.SetPoint    (&CommandSource, "Set_point",          *Smp(cmdSce[0]));
		CommandSource.FuncGen     (&CommandSource, "Function_Generator", *Smp(cmdSce[1]));
		CommandSource.External    (&CommandSource, "External",           *Smp(cmdSce[2]));
		CommandSource.CNet        (&CommandSource, "CNet",               *Smp(cmdSce[3]));
	/*#else
		CommandSource.SetPoint    (&CommandSource, "Set_point",          NULL);
		CommandSource.FuncGen     (&CommandSource, "Function_Generator", NULL);
		CommandSource.External    (&CommandSource, "External",           NULL);
		CommandSource.CNet        (&CommandSource, "CNet",               NULL);
	#endif*/

	Mode(this, "Mode");
	Mode.setFlags("g");
	#ifndef AICUBE
	for(i=0, p=Mode.ModeArray; i<EPNMode; i++, p++)
	{
		sprintf(p->numberBuf, "%d", i+1);
		(*p)(&Mode, p->numberBuf, *Smp(modeArray[i]));
		p->smpPModeFbCh=NULL;
		p->pModeFbCh=NULL;
		p->pSimExtCh=NULL;
	}
	#endif

	pIntCmd=NULL;
	pExtCmdCh=NULL;
	pCNet=NULL;
	pCompFb=NULL;
	pILCh=NULL;
	pCascadeCh=NULL;
	pTsrIn=LimitSys::IContext.smpCntxt?&((LimitSys::IContext.smpCntxt)->tsrInBufLo):NULL;
	pTsrOut=LimitSys::IContext.smpCntxt?((LimitSys::IContext.smpCntxt)->tsrOutBufLo):NULL;
	pSimulationEnable=LimitSys::IContext.supCntxt?&((LimitSys::IContext.supCntxt)->SimEn):NULL;
	pInnerLoopTuneEnable=LimitSys::IContext.supCntxt?&((LimitSys::IContext.supCntxt)->ILEn):NULL;
	pHCDelta=NULL;
}

#ifndef AICUBE
	const ErrorPath::monitorOffsetType ErrorPath::MonitorOffset[]
	={NULL, &SmpErrorPath::cmd, &SmpErrorPath::feedback, &SmpErrorPath::error, &SmpErrorPath::OLoutput, &SmpErrorPath::output, &SmpErrorPath::STM, NULL}; /* SN */
#else
	const ErrorPath::monitorOffsetType ErrorPath::MonitorOffset[]
	={NULL, &SmpErrorPath::cmd, NULL, NULL, &SmpErrorPath::OLoutput, NULL, NULL, NULL}; // SN
#endif
const ErrorPath::monitorStrType ErrorPath::MonitorStr[]={"None", "Command", "Feedback", "Error", "Outer_loop_output", "Valve_drive", "Self_tune_model", "Valve_drive_op", NULL}; /* SN */

void ErrorPath::parAlteredEvent(Parameter *sce)
{
	if(sce==&Simulation.SimulationParameters.NaturalFrequency)
		newSimFilter();
	if(sce==&Simulation.SimulationParameters.DampingRatio)
		newSimFilter();
	if(sce==&Simulation.SimulationParameters.ModeArrayPos)
		updateExtSimPars();
	if(sce==&Simulation.SimulationParameters.ModeArrayNeg)
		updateExtSimPars();
	if(sce==&Enable)
		parConfigChangedEvent(this);
	if(sce==&Name)
		parConfigChangedEvent(this);
	if(sce==&InnerLoop.Enable)
		parConfigChangedEvent(this);
	if(sce==&CmdTransferMode)
		newCmdTransferRate();
	if(sce==&ModeChange.StartModeAutoset)
		newStartModeAutoset();
	Parameter::parAlteredEvent(sce);
}

void ErrorPath::parPrimeSignal()
{
	double num[2], den[2];

	Parameter::parPrimeSignal();
	newSimFilter();
	#ifndef AICUBE
		Poke(smpbs->aGainFac, 1.0f/SysFs);
	#endif
	Poke(smpbs->pIntCmd, pIntCmd);
	Poke(smpbs->pSpan, pSpan);
	Poke(smpbs->pSetPointInc, pSetPointInc);
	Poke(smpbs->pTsrIn, pTsrIn);
	Poke(smpbs->pTsrOut, pTsrOut);
	FilterDesign(num, den, 1, FDClassicOrder1, FDHighPass, (double)EPDeltaPHPFreq, 0.0, (double)SysFs, 0.0, False);
	#ifndef AICUBE
		PokeCoeffs(smpbs->deltaPDerFilterNum, num, 1);
		PokeCoeffs(smpbs->deltaPDerFilterDen, den, 1);
	#endif
	newCmdTransferRate();
	Poke(smpbs->pHCDelta, pHCDelta);
}

void ErrorPath::parConfigureSignal()
{
	int i;
	EPModeControlClass *p;

	#ifndef AICUBE
	Poke(smpbs->pCompFb, pCompFb);
	smpbs->pILCh = pILCh;
	smpbs->pCascadeCh = pCascadeCh;
	//Poke(*((Unsigned *)(&smpbs->pILCh)), (Unsigned)pILCh);
	//Poke(*((Unsigned *)(&smpbs->pCascadeCh)), (Unsigned)pCascadeCh);
	Poke(smpbs->pDeltaPFb, pDeltaPFb);
	#endif
	smpbs->pExtCmdCh = pExtCmdCh;
	//Poke(*((Unsigned *)(&smpbs->pExtCmdCh)), (Unsigned)pExtCmdCh);
	Poke(smpbs->pCNet, pCNet);
	for(i=0, p=Mode.ModeArray; i<EPNMode; i++, p++)
	{
		smpbs->modeArray[i].pFbCh = p->smpPModeFbCh;
		//Poke(*((Unsigned *)(&smpbs->modeArray[i].pFbCh)), (Unsigned)p->smpPModeFbCh);
		#ifndef AICUBE
		Poke(smpbs->modeArray[i].ilmodeflg, p->ilModeFlag);
		Poke(smpbs->modeArray[i].setupflg, (p->pModeFbCh&&p->pModeFbCh->SetupParticipation()==SetupPartImpl)?1:0);
		if(ModeChange.StartModeAutoset()&&p->pModeFbCh&&p->pModeFbCh->SetupParticipation()==SetupPartImpl)
			ModeChange.StartMode=i+1;
		#endif
	}
	updateExtSimPars();
	checkRangeSafety();
	updateSimILEn();
	//#endif
}

void ErrorPath::parPollSignal()
{
	checkRangeSafety();
	updateSimILEn();
	Parameter::parPollSignal();
}

void ErrorPath::newStartModeAutoset()
{
	int i;
	EPModeControlClass *p;

	if(!ModeChange.StartModeAutoset())
		return;
	for(i=0, p=Mode.ModeArray; i<EPNMode; i++, p++)
		if(p->pModeFbCh&&p->pModeFbCh->SetupParticipation()==SetupPartImpl)
			ModeChange.StartMode=i+1;
}

void ErrorPath::updateSimILEn()
{
#ifndef AICUBE
	int en;
#ifdef EXTERNALSIMULATION
	int i;
	EPModeControlClass *p;
#endif

	en=pSimulationEnable?*pSimulationEnable:0;
	if(en!=Simulation.Enable())
	{
		Simulation.Enable=en;
		Poke(smpbs->simEnable, en);
#ifdef EXTERNALSIMULATION
	for(i=0, p=Mode.ModeArray; i<EPNMode; i++, p++)
		if(p->pSimExtCh)
			SetExtSimEn(p->pSimExtCh, en);
#endif
	}
	en=pInnerLoopTuneEnable?*pInnerLoopTuneEnable:0;
	if(en!=ILTuning())
	{
		ILTuning=en;
		Poke(smpbs->ILTune, en);
	}
#endif
}

void ErrorPath::newCmdTransferRate()
{
	#ifndef AICUBE
		Poke(smpbs->cmdXferRate, CmdTransferMode()?1.0f:EPFadeInc);
	#endif
}

void ErrorPath::checkRangeSafety()
{
	#ifndef AICUBE
	EPModeControlClass *p;
	char namebuf[SigNameSz+1];
	char *q;
	int i;

	for(i=0, p=Mode.ModeArray; i<EPNMode; i++, p++)
	{
		if(p->pModeFbCh)
		{
			if(p->Selected())
			{
				CommandSource.SetPoint.pCmdRange=&p->pModeFbCh->Range;
				CommandSource.CNet.pCmdRange=&p->pModeFbCh->Range;
				CommandSource.SetPoint.checkCompatible(&p->pModeFbCh->Range);
				CommandSource.External.checkCompatible(&p->pModeFbCh->Range);
				CommandSource.FuncGen.checkCompatible(&p->pModeFbCh->Range);
				CommandSource.CNet.checkCompatible(&p->pModeFbCh->Range);
			}
			strncpy(namebuf, p->pModeFbCh->Range.SRParameter[p->pModeFbCh->Range.SRParameter()], SigNameSz);
			q=namebuf;
			while(*q)
			{
				if(*q=='_')
					*q=' ';
				q++;
			}
			if(p->pModeFbCh->SetupParticipation()==SetupPartImpl)
			{
				namebuf[SigNameSz]=0;
				if(strlen(namebuf)<=(SigNameSz-11))
					strcat(namebuf, " (startup)");
			}
			p->Name=namebuf;
		}
		else
			p->Name="None";
	}
	#endif
}

void ErrorPath::updateExtSimPars()
{
#ifdef EXTERNALSIMULATION
#ifndef AICUBE
	int i;
	EPModeControlClass *p;

	for(i=0, p=Mode.ModeArray; i<EPNMode; i++, p++)
		if(p->pSimExtCh)
			SetExtSimPars(p->pSimExtCh, &smpbs->simOutput,
				(*Simulation.SimulationParameters.ModeArrayPos.member[i])(),
				(*Simulation.SimulationParameters.ModeArrayNeg.member[i])());
#endif
#endif
}

void ErrorPath::newSimFilter()
{
	#ifndef AICUBE
	double num[3], den[3], fc, damp;

	fc=(double)Simulation.SimulationParameters.NaturalFrequency();
	damp=(double)Simulation.SimulationParameters.DampingRatio();
	FilterDesign(num, den, 2, FDClassicOrder2, FDLowPass, fc, damp, (double)SysFs, 0.0, true);
	PokeCoeffs(smpbs->simNum, num, 2);
	PokeCoeffs(smpbs->simDen, den, 2);
	#endif
}

void EPModeControlClass::operator()(Parameter *par, char *n, SmpEPMode &smp)
{
	smpbs=&smp;
	Parameter::operator()(par, n);
	this->setFlags("g");
	Name    (this, "Name",     "r",  SigNameSz, "None");
	Selected(this, "Selected", "dl", Smp(sel), 0, 1, "red");
	Request (this, "Request",  "b",  Smp(req), 0, 1);
	smpPModeFbCh=NULL;
	pModeFbCh=NULL;
}

void EPModeControlClass::parConfigureSignal()
{
	Parameter::parConfigureSignal();
	smpbs->pFbCh = smpPModeFbCh;
	//Poke(*((Unsigned *)Smp(pFbCh)), (Unsigned)(smpPModeFbCh));
}

void EPCommandControlClass::operator()(Parameter *par, char *n, SmpEPCmd &smp)
{
	smpbs=&smp;
	Parameter::operator()(par, n);
	this->setFlags("g");
	Selected  (this, "Selected",   "rdl", Smp(sel), 0, 1, "red");
	RangeSafe (this, "Range_safe", "rl",  NULL,     0, 1, "green");
	Request   (this, "Request",    "b",   Smp(req), 0, 1);
	pCmdRange=NULL;
	comp=0;
}

void EPCommandControlClass::parPrimeSignal()
{
	Parameter::parPrimeSignal();
	Poke(smpbs->safe, comp);
}

void EPCommandControlClass::checkCompatible(SignalRange *p)
{
	if((!p)||(!pCmdRange))
		comp=0;
	else
		comp=(	  (p->FullScale()==pCmdRange->FullScale())
				&&(p->UnipolarMode()==pCmdRange->UnipolarMode())
				&&(p->SRParameter()==pCmdRange->SRParameter())
				&&(!strcmp(p->Units(), pCmdRange->Units()))		)?1:0;
	RangeSafe=comp;
	Poke(smpbs->safe, comp);
}

#endif
