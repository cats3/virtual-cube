// C3Std.h - C3 Standard header - MODIFIED FOR CUBE PROFILE TESTING

//#ifdef __cplusplus
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <math.h>
#include <float.h>

#define TEMPLATE

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Legal Switches:
//
// One of these    : CONTROLCUBE SIGNALCUBE KINETUSB AICUBE
//
// And optionally  : PCTEST
//
// If CONTROLCUBE or AICUBE switch is defined but PCTEST is not:
//
// One of these    : _DSPA _DSPB


///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Control Cube build
//
#ifdef CONTROLCUBE

#ifdef PCTEST					// PC simulation

#define	SysFs			4096.0f
#define SUPGEN
#define SMPGEN
#define QUIETVALIDATION
#define	CNET_DATASLOT_TIMELO	253
#define	CNET_DATASLOT_TIMEHI	254
//#define INLINEPEEKPOKE
//#define INLINEFILTER
//#define INLINE_LIMIT_FUNCS
//#define FASTPOLL
extern void Trace(char *fmt, ...);
typedef signed char		Sint8;
typedef unsigned char	Uint8;
typedef signed short	Sint16;
typedef unsigned short	Uint16;
typedef signed int		Sint32;		// SJE CHANGED
typedef unsigned int	Uint32;		// SJE CHANGED
typedef unsigned int    u32_t;
typedef unsigned long long u64_t;
struct eventlog_entry {
	Uint32 type;
	Uint32 timestamp_lo;
	Uint32 timestamp_hi;
	Uint32 parameter1;
	Uint32 parameter2;
};
#ifdef INLINEPEEKPOKE
inline Sint32 Peek(Sint32 &a){return a;}
inline Uint32 Peek(Uint32 &a){return a;}
inline float Peek(float &a){return a;}
inline double Peek(double &a){return a;}
inline float *Peek(float* &a){return a;}
inline void Poke(Sint32 &a, Sint32 d){a=d;}
inline void Poke(Sint32* &a, Sint32 *d){a=d;}
inline void Poke(Uint32 &a, Uint32 d){a=d;}
inline void Poke(Uint32* &a, Uint32 *d){a=d;}
inline void Poke(float &a, float d){a=d;}
inline void Poke(float* &a, float *d){a=d;}
inline void Poke(double &a, double d){a=d;}
inline void Poke(float** &a, float **d){a=d;}
#else
Sint32 Peek(Sint32 &a);
Uint32 Peek(Uint32 &a);
float Peek(float &a);
double Peek(double &a);
float *Peek(float* &a);
void Poke(Sint32 &a, Sint32 d);
void Poke(Sint32* &a, Sint32 *d);
void Poke(Uint32 &a, Uint32 d);
void Poke(Uint32* &a, Uint32 *d);
void Poke(float &a, float d);
void Poke(float* &a, float *d);
void Poke(double &a, double d);
void Poke(float** &a, float **d);
void event_queue_add(eventlog_entry e);
bool event_queue_remove(eventlog_entry*& eptr);
Sint32* cnet_get_slotaddr(Uint32 slot, Uint32 proc);


#endif // INLINEPEEKPOKE

#ifdef TEMPLATE
#define	C3AppNChans		9
#define	SigNCh			32
#define	ExpNExp			6
#define MonNCh			9
#else
#define	C3AppNChans		4
#define	SigNCh			24
#define	ExpNExp			6
#define MonNCh			6
#endif // TEMPLATE
#define	CubPrgrmSz		30000	// Increased from 10000

#else // !PCTEST

/* Any changes to these values must have corresponding changes to the values in 
   the channel.h and chanproc.asm files.
*/

#define	SysFs			4096.0f
#define	C3AppNChans		4
#define	SigNCh			24
#define	ExpNExp			6
#define MonNCh			8
#define	CubPrgrmSz		30000	// Increased from 10000

#ifdef __cplusplus

#define EXTERNALSIMULATION

#ifdef _DSPA					// Hardware supervisory processor (C6711)
#define SUPGEN
typedef signed char		Sint8;
typedef unsigned char	Uint8;
typedef signed short	Sint16;
typedef unsigned short	Uint16;
typedef signed int		Sint32;
typedef unsigned int	Uint32;
typedef unsigned int    u32_t;
typedef unsigned long long u64_t;
#define QUIETVALIDATION
//#define INLINEPEEKPOKE
//#define FASTPOLL
#ifdef INLINEPEEKPOKE
extern "C" {
	Uint32 hpi_read4(Uint32 a);
	void hpi_write4(Uint32 a, Uint32 d);
}
inline Sint32 Peek(Sint32 &a){return (Sint32)hpi_read4((Uint32)&a);}
inline Uint32 Peek(Uint32 &a){return hpi_read4((Uint32)&a);}
inline float Peek(float &a){return _itof((Sint32)hpi_read4((Uint32)&a));}
inline float *Peek(float* &a){return (float *)hpi_read4((Uint32)&a);}
inline void Poke(Sint32 &a, Sint32 d){hpi_write4((Uint32)&a, (Uint32)d);}
inline void Poke(Sint32* &a, Sint32 *d){hpi_write4((Uint32)&a, (Uint32)d);}
inline void Poke(Uint32 &a, Uint32 d){hpi_write4((Uint32)&a, d);}
inline void Poke(Uint32* &a, Uint32 *d){hpi_write4((Uint32)&a, (Uint32)d);}
inline void Poke(float &a, float d){hpi_write4((Uint32)&a, (Uint32)_ftoi(d));}
inline void Poke(float* &a, float *d){hpi_write4((Uint32)&a, (Uint32)d);}
inline void Poke(float** &a, float **d){hpi_write4((Uint32)&a, (Uint32)d);}
#else // !INLINEPEEKPOKE
Sint32 Peek(Sint32 &a);
Uint32 Peek(Uint32 &a);
float Peek(float &a);
float *Peek(float* &a);
void Poke(Sint32 &a, Sint32 d);
void Poke(Sint32* &a, Sint32 *d);
void Poke(Uint32 &a, Uint32 d);
void Poke(Uint32* &a, Uint32 *d);
void Poke(float &a, float d);
void Poke(float* &a, float *d);
void Poke(double &a, double d);
void Poke(float** &a, float **d);
#endif // INLINEPEEKPOKE
double Peek(double &a);
void Poke(double &a, double d);
#endif // _DSPA

#ifdef _DSPB					// Hardware sample processor (C6711)
#define SMPGEN
typedef signed char		Sint8;
typedef unsigned char	Uint8;
typedef signed short	Sint16;
typedef unsigned short	Uint16;
typedef signed int		Sint32;
typedef unsigned int	Uint32;
typedef unsigned int    u32_t;
typedef unsigned long long u64_t;
//#define INLINEFILTER
#endif // _DSPB

#endif // __cplusplus
#endif // PCTEST
#endif // CONTROLCUBE

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// AI Cube build
//
#ifdef AICUBE

#ifdef PCTEST					// PC simulation
#define	SysFs			4100.0f
#define SUPGEN
#define SMPGEN
#define QUIETVALIDATION
//#define INLINEPEEKPOKE
//#define INLINEFILTER
//#define INLINE_LIMIT_FUNCS
//#define FASTPOLL
extern void Trace(char *fmt, ...);
typedef signed char		Sint8;
typedef unsigned char	Uint8;
typedef signed short	Sint16;
typedef unsigned short	Uint16;
typedef signed long		Sint32;
typedef unsigned long	Uint32;
typedef unsigned int    u32_t;
typedef unsigned long long u64_t;
#ifdef INLINEPEEKPOKE
inline Sint32 Peek(Sint32 &a){return a;}
inline Uint32 Peek(Uint32 &a){return a;}
inline float Peek(float &a){return a;}
inline double Peek(double &a){return a;}
inline float *Peek(float* &a){return a;}
inline void Poke(Sint32 &a, Sint32 d){a=d;}
inline void Poke(Sint32* &a, Sint32 *d){a=d;}
inline void Poke(Uint32 &a, Uint32 d){a=d;}
inline void Poke(Uint32* &a, Uint32 *d){a=d;}
inline void Poke(float &a, float d){a=d;}
inline void Poke(float* &a, float *d){a=d;}
inline void Poke(double &a, double d){a=d;}
inline void Poke(float** &a, float **d){a=d;}
#else
Sint32 Peek(Sint32 &a);
Uint32 Peek(Uint32 &a);
float Peek(float &a);
double Peek(double &a);
float *Peek(float* &a);
void Poke(Sint32 &a, Sint32 d);
void Poke(Sint32* &a, Sint32 *d);
void Poke(Uint32 &a, Uint32 d);
void Poke(Uint32* &a, Uint32 *d);
void Poke(float &a, float d);
void Poke(float* &a, float *d);
void Poke(double &a, double d);
void Poke(float** &a, float **d);
#endif

#define	C3AppNChans		2
#define	SigNCh			16
#define	ExpNExp			8
#define	MonNCh			4
#define	CubPrgrmSz		30000	// Increased from 10000
#else

/* Any changes to these values must have corresponding changes to the values in 
   the channel.h and chanproc.asm files.
*/

#define	SysFs			4096.0f
#define	C3AppNChans		6
#define	SigNCh			24
#define	ExpNExp			6
#define MonNCh			9
#define	CubPrgrmSz		30000	// Increased from 10000

#ifdef __cplusplus

#define EXTERNALSIMULATION

#ifdef _DSPA					// Hardware supervisory processor (C6711)
#define SUPGEN
typedef signed char		Sint8;
typedef unsigned char	Uint8;
typedef signed short	Sint16;
typedef unsigned short	Uint16;
typedef signed int		Sint32;
typedef unsigned int	Uint32;
typedef unsigned int    u32_t;
typedef unsigned long long u64_t;
#define QUIETVALIDATION
//#define INLINEPEEKPOKE
//#define FASTPOLL
#ifdef INLINEPEEKPOKE
extern "C" {
	Uint32 hpi_read4(Uint32 a);
	void hpi_write4(Uint32 a, Uint32 d);
}
inline Sint32 Peek(Sint32 &a){return (Sint32)hpi_read4((Uint32)&a);}
inline Uint32 Peek(Uint32 &a){return hpi_read4((Uint32)&a);}
inline float Peek(float &a){return _itof((Sint32)hpi_read4((Uint32)&a));}
inline float *Peek(float* &a){return (float *)hpi_read4((Uint32)&a);}
inline void Poke(Sint32 &a, Sint32 d){hpi_write4((Uint32)&a, (Uint32)d);}
inline void Poke(Sint32* &a, Sint32 *d){hpi_write4((Uint32)&a, (Uint32)d);}
inline void Poke(Uint32 &a, Uint32 d){hpi_write4((Uint32)&a, d);}
inline void Poke(Uint32* &a, Uint32 *d){hpi_write4((Uint32)&a, (Uint32)d);}
inline void Poke(float &a, float d){hpi_write4((Uint32)&a, (Uint32)_ftoi(d));}
inline void Poke(float* &a, float *d){hpi_write4((Uint32)&a, (Uint32)d);}
inline void Poke(float** &a, float **d){hpi_write4((Uint32)&a, (Uint32)d);}
#else
Sint32 Peek(Sint32 &a);
Uint32 Peek(Uint32 &a);
float Peek(float &a);
float *Peek(float* &a);
void Poke(Sint32 &a, Sint32 d);
void Poke(Sint32* &a, Sint32 *d);
void Poke(Uint32 &a, Uint32 d);
void Poke(Uint32* &a, Uint32 *d);
void Poke(float &a, float d);
void Poke(float* &a, float *d);
void Poke(double &a, double d);
void Poke(float** &a, float **d);
#endif
double Peek(double &a);
void Poke(double &a, double d);
#endif

#ifdef _DSPB					// Hardware sample processor (C6711)
#define SMPGEN
typedef signed char		Sint8;
typedef unsigned char	Uint8;
typedef signed short	Sint16;
typedef unsigned short	Uint16;
typedef signed int		Sint32;
typedef unsigned int	Uint32;
typedef unsigned int    u32_t;
typedef unsigned long long u64_t;
//#define INLINEFILTER
#endif

#endif
#endif
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Signal Cube build
//
#ifdef SIGNALCUBE

#define SUPGEN
#define SMPGEN

#ifdef PCTEST					// PC simulation
#define	SysFs			4100.0f
#define QUIETVALIDATION
//#define INLINEPEEKPOKE
//#define INLINEFILTER
//#define INLINE_LIMIT_FUNCS
extern void Trace(char *fmt, ...);
typedef signed char		Sint8;
typedef unsigned char	Uint8;
typedef signed short	Sint16;
typedef unsigned short	Uint16;
typedef signed long		Sint32;
typedef unsigned long	Uint32;
typedef unsigned int    u32_t;
typedef unsigned long long u64_t;
#ifdef INLINEPEEKPOKE
inline Sint32 Peek(Sint32 &a){return a;}
inline Uint32 Peek(Uint32 &a){return a;}
inline float Peek(float &a){return a;}
inline double Peek(double &a){return a;}
inline float *Peek(float* &a){return a;}
inline void Poke(Sint32 &a, Sint32 d){a=d;}
inline void Poke(Sint32* &a, Sint32 *d){a=d;}
inline void Poke(Uint32 &a, Uint32 d){a=d;}
inline void Poke(Uint32* &a, Uint32 *d){a=d;}
inline void Poke(float &a, float d){a=d;}
inline void Poke(float* &a, float *d){a=d;}
inline void Poke(double &a, double d){a=d;}
inline void Poke(float** &a, float **d){a=d;}
#else
Sint32 Peek(Sint32 &a);
Uint32 Peek(Uint32 &a);
float Peek(float &a);
float *Peek(float* &a);
void Poke(Sint32 &a, Sint32 d);
void Poke(Sint32* &a, Sint32 *d);
void Poke(Uint32 &a, Uint32 d);
void Poke(Uint32* &a, Uint32 *d);
void Poke(float &a, float d);
void Poke(float* &a, float *d);
void Poke(float** &a, float **d);
#endif
double Peek(double &a);
void Poke(double &a, double d);

#define	C3AppNChans		2
#define	SigNCh			1
#define	ExpNExp			1
#define	MonNCh			1
#else

/* Any changes to these values must have corresponding changes to the values in 
   the channel.h and chanproc.asm files.
*/

#define	SysFs			4096.0f
#define	C3AppNChans		1
#define	SigNCh			32
#define	ExpNExp			4
#define	MonNCh			1

#ifdef __cplusplus

#define EXTERNALSIMULATION

typedef signed char		Sint8;	// C6711 processor
typedef unsigned char	Uint8;
typedef signed short	Sint16;
typedef unsigned short	Uint16;
typedef signed int		Sint32;
typedef unsigned int	Uint32;
typedef unsigned int    u32_t;
typedef unsigned long long u64_t;
#define QUIETVALIDATION
//#define INLINEPEEKPOKE
#ifdef INLINEPEEKPOKE
inline Sint32 Peek(Sint32 &a){return a;}
inline Uint32 Peek(Uint32 &a){return a;}
inline float Peek(float &a){return a;}
inline double Peek(double &a){return a;}
inline float *Peek(float* &a){return a;}
inline void Poke(Sint32 &a, Sint32 d){a=d;}
inline void Poke(Sint32* &a, Sint32 *d){a=d;}
inline void Poke(Uint32 &a, Uint32 d){a=d;}
inline void Poke(Uint32* &a, Uint32 *d){a=d;}
inline void Poke(float &a, float d){a=d;}
inline void Poke(float* &a, float *d){a=d;}
inline void Poke(double &a, double d){a=d;}
inline void Poke(float** &a, float **d){a=d;}
#else
Sint32 Peek(Sint32 &a);
Uint32 Peek(Uint32 &a);
float Peek(float &a);
double Peek(double &a);
float *Peek(float* &a);
void Poke(Sint32 &a, Sint32 d);
void Poke(Sint32* &a, Sint32 *d);
void Poke(Uint32 &a, Uint32 d);
void Poke(Uint32* &a, Uint32 *d);
void Poke(float &a, float d);
void Poke(float* &a, float *d);
void Poke(double &a, double d);
void Poke(float** &a, float **d);
#endif
//#define INLINEFILTER
#endif
#endif
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// KiNet/USB build
//
#ifdef KINETUSB

#ifdef PCTEST					// PC simulation
#define	SysFs	4100.0f
#define SUPGEN
#define SMPGEN
extern void Trace(char *fmt, ...);
#define QUIETVALIDATION
//#define INLINEPEEKPOKE
//#define INLINEFILTER
//#define INLINE_LIMIT_FUNCS
typedef signed char		Sint8;
typedef unsigned char	Uint8;
typedef signed short	Sint16;
typedef unsigned short	Uint16;
typedef signed long		Sint32;
typedef unsigned long	Uint32;
typedef unsigned long long u64_t;
#ifdef INLINEPEEKPOKE
inline Sint32 Peek(Sint32 &a){return a;}
inline Uint32 Peek(Uint32 &a){return a;}
inline float Peek(float &a){return a;}
inline double Peek(double &a){return a;}
inline float *Peek(float* &a){return a;}
inline void Poke(Sint32 &a, Sint32 d){a=d;}
inline void Poke(Sint32* &a, Sint32 *d){a=d;}
inline void Poke(Uint32 &a, Uint32 d){a=d;}
inline void Poke(Uint32* &a, Uint32 *d){a=d;}
inline void Poke(float &a, float d){a=d;}
inline void Poke(float* &a, float *d){a=d;}
inline void Poke(double &a, double d){a=d;}
inline void Poke(float** &a, float **d){a=d;}
#else
Sint32 Peek(Sint32 &a);
Uint32 Peek(Uint32 &a);
float Peek(float &a);
double Peek(double &a);
float *Peek(float* &a);
void Poke(Sint32 &a, Sint32 d);
void Poke(Sint32* &a, Sint32 *d);
void Poke(Uint32 &a, Uint32 d);
void Poke(Uint32* &a, Uint32 *d);
void Poke(float &a, float d);
void Poke(float* &a, float *d);
void Poke(double &a, double d);
void Poke(float** &a, float **d);
#endif
#define	C3AppNChans				4
#define	SigNCh					1
#define	ExpNExp					1
#define	MonNCh					1
#else

#define	C3AppNChans				4
#define	SigNCh					1
#define	ExpNExp					1
#define	MonNCh					1

#ifdef __cplusplus

#define	SysFs		2048.0f		// Hardware build
#define SUPGEN
#define SMPGEN
#define QUIETVALIDATION
//#define INLINEPEEKPOKE
//#define INLINEFILTER
typedef signed char		Sint8;
typedef unsigned char	Uint8;
typedef signed short	Sint16;
typedef unsigned short	Uint16;
typedef signed int		Sint32;
typedef unsigned int	Uint32;
typedef unsigned long long u64_t;
#ifdef INLINEPEEKPOKE
inline Sint32 Peek(Sint32 &a){return a;}
inline Uint32 Peek(Uint32 &a){return a;}
inline float Peek(float &a){return a;}
inline double Peek(double &a){return a;}
inline float *Peek(float* &a){return a;}
inline void Poke(Sint32 &a, Sint32 d){a=d;}
inline void Poke(Sint32* &a, Sint32 *d){a=d;}
inline void Poke(Uint32 &a, Uint32 d){a=d;}
inline void Poke(Uint32* &a, Uint32 *d){a=d;}
inline void Poke(float &a, float d){a=d;}
inline void Poke(float* &a, float *d){a=d;}
inline void Poke(double &a, doouble d){a=d;}
inline void Poke(float** &a, float **d){a=d;}
#else
Sint32 Peek(Sint32 &a);
Uint32 Peek(Uint32 &a);
float Peek(float &a);
double Peek(double &a);
float *Peek(float* &a);
void Poke(Sint32 &a, Sint32 d);
void Poke(Sint32* &a, Sint32 *d);
void Poke(Uint32 &a, Uint32 d);
void Poke(Uint32* &a, Uint32 *d);
void Poke(float &a, float d);
void Poke(float* &a, float *d);
void Poke(double &a, douoble d);
void Poke(float** &a, float **d);
#endif

#endif
#endif
#endif

#ifdef __cplusplus
#define	MinInt			(-2147483647-1)
#define	MaxInt			2147483647
#define	MinUns			0
#define	MaxUns			4294967295
#define	MinFloat		(-FLT_MAX)
#define	MaxFloat		FLT_MAX
#define	DfltMinInt		(-2147483647-1)
#define	DfltMaxInt		2147483647
#define	DfltMinUns		0
#define	DfltMaxUns		4294967295
#define DfltMinFloat	(-1.0f)
#define DfltMaxFloat	1.0f
#define	C3RoundToll		0.001f
#define	Pi				3.1415926535897932384626433832795
#define	Smp(a)			&smpbs->a
#ifdef isnan
	#undef isnan
#endif
#define isnan(x)		(((*((Uint32 *)(&(x))))&0x7f800000)==0x7f800000)

#define	Clamp(a, b, c)	{if((a)<(b))(a)=(b);if((a)>(c))(a)=(c);}

typedef float Float;
typedef Sint32 Integer;
typedef Uint32 Unsigned;
typedef Uint16 Result;
typedef Uint16 Choice;
enum {False=0, True};

#define	C3LLFactor		0.1f
#define	C3NTS			256
#define	EPNCmd			4
#define	EPNMode			8

void acquireSem(Uint32 *pSem, Uint32 val);
void releaseSem(Uint32 *pSem, Uint32 val);

#define	DigNDigin	10
#define	DigNDigout	10
#define PROC_DSPA 0
#define PROC_DSPB 1

#include "AdjustRequest.h"
#include "Parameter.h"
#include "Limit.h"
#include "Adjustment.h"
//#include "Limit.h"
#include "Command.h"
#include "Miscellaneous.h"
#include "Generator.h"
#include "C3Signal.h"
#include "ErrorPath.h"
#include "Expression.h"
#include "MonitorOP.h"
#include "Events.h"
#include "CubicEngine.h" 
#include "DigitalIO.h"
#include "Analyser.h"
#include "Regression.h"

//#include "json.h"

#endif
//#endif // if __cplusplus
