// DigitalIO.h - DigitalIO Class header

enum {	DigNameSz=32,		// Size of name string
		DigConSz=20  		// Size of connector string
};

class SmpDiginChannel {
public:
	void iter()
	{
		diginLimit.iter();
		diginLimit.update(dinstate, 0.0f);
	}
	SmpLimit diginLimit;
	Uint32 dinstate;
};

class SmpDiginArray {
public:
	void iter()
	{
		SmpDiginChannel *p;
		int i;

		p=channel;
		for(i=DigNDigin; i--; p++)
			p->iter();
	}
	SmpDiginChannel channel[DigNDigin];
};

class SmpDigoutChannel {
public:
	void iter()
	{
	}
};

class SmpDigoutArray {
public:
	void iter()
	{
		SmpDigoutChannel *p;
		int i;

		p=channel;
		for(i=DigNDigout; i--; p++)
			p->iter();
	}
	SmpDigoutChannel channel[DigNDigout];
};

#ifdef SUPGEN

class DiginChannel: public Parameter {
public:
	void operator()(Parameter *par, int ind, SmpDiginChannel &smp);
	TextPar		Name;
	TextPar		Connector;
	EnumPar		Enable;
	IntegerPar	Index;
	EnumPar		Polarity;
	Limit		DiginLimit;
	int			chandefindex;
private:
	virtual void parPrimeSignal();
	virtual void parPollSignal();
	virtual void parAlteredEvent(Parameter *sce);
	SmpDiginChannel *smpbs;
	char myName[4];
};

class DiginArray: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpDiginArray &smp);
	DiginChannel Channel[DigNDigin];
private:
	SmpDiginArray *smpbs;
};

class DigoutChannel: public Parameter {
public:
	void operator()(Parameter *par, int ind, SmpDigoutChannel &smp);
	TextPar		Name;
	TextPar		Connector;
	EnumPar		Enable;
	IntegerPar	Index;
	EnumPar		Polarity;
	EnumPar		State;
	int			chandefindex;
private:
	virtual void parPrimeSignal();
	virtual void parAlteredEvent(Parameter *sce);
	SmpDigoutChannel *smpbs;
	char myName[4];
};

class DigoutArray: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpDigoutArray &smp);
	DigoutChannel Channel[DigNDigout];
private:
	SmpDigoutArray *smpbs;
};

#endif
