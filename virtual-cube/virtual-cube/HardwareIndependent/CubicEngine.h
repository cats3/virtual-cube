// CubicEngine.h - Cubic Engine Class header

#ifdef SUPGEN

typedef struct {
	Uint32 opCode;
	Uint32 par;
	Uint32 val;
} CubInstruction;

enum {	CubHalt=0,				// Cubic script opcode values
		CubWaitI,				// 1
		CubAssignI,				// 2
		CubAssignU,				// 3
		CubAssignF,				// 4
		CubAssignFInstant,		// 5
		CubRepeatBegin,			// 6
		CubRepeatEnd,			// 7
		CubCondIBegin,			// 8
		CubCondEnd,				// 9
		CubPause,				// 10
		CubAssignMemF,			// 11
		CubSaveMemF,			// 12
		CubRecallMemF,			// 13
		CubCallSub,				// 14
		CubReturnSub,			// 15
		CubParCopy,				// 16
		CubRecallMemFInstant,	// 17
		CubWriteHS,				// 18
		CubWaitHS0,				// 19
		CubWaitHS1,				// 20
		CubWaitU,				// 21
	    CubWaitINE,				// 22
		CubCondUBegin,			// 23
		CubGotoLine,			// 24
		CubAddMemF,				// 25
		CubFPlusEqual,			// 26
		CubGotoParLine,			// 27
		CubCompParF,			// 28
		CubCompF,				// 29
		CubWaitFGT,				// 30
		CubWaitFLT,				// 31
		CubWaitUGT,				// 32
		CubWaitUNE,				// 33
		CubDelay,				// 34
		CubSetWaitTimer,		// 35
		CubWaitForTimeout,		// 36
		CubZeroChan,			// 37
		CubRTControl,			// 38
		CubRTPause,				// 39
		CubRTResume,			// 40
		CubClrInt,				// 41
		CubWaitInt,				// 42
		CubSelectGenerator,		// 43
		CubLoadWavetable,		// 44
		CubBeginGroup,			// 45
		CubEndGroup,			// 46
		CubLedOn,				// 47
		CubLedOff,				// 48
		CubReturnInt,			// 49
		CubClrIntMask,			// 50
		CubSetIntMask,			// 51
		CubRepeatBreak,			// 52
		CubAssign,				// 53
		CubPlusEqual,			// 54
		CubWaitEQ,				// 55
		CubWaitNE,				// 56
		CubWaitGT,				// 57
		CubWaitLT,				// 58
		CubCompPar,				// 59
		CubComp,				// 60
		CubSaveMem,				// 61
		CubRecallMem,			// 62
		CubAddMem,				// 63
		CubCondBegin,			// 64
		CubMpyEqual,			// 65

		CubExcptnNone=0,		// Exceptions
		CubExcptnIllOp,
		CubExcptnIllPar,
		CubExcptnIllVal,
		CubExcptnStackOF,
		CubExcptnNest,
		CubExcptnPause,
		CubExcptnHalt,
		CubExcptnBadPtr,
		CubExcptnZeroRange,
		CubExcptnTypeMismatch,

        CubStackSz=8,
		CubStackTypeNone=0,
		CubStackTypeLoop,
		CubStackTypeCall,
		CubStackTypeInt,

		CubNMemRegs=32,

		CubConditionTableSz=8,
		CubIntTableSz=8,

		CubConditionTypeNone=0,		// 0
		CubConditionTypeChange,		// 1
		CubConditionTypeEQ,			// 2
		CubConditionTypeNE,			// 3
		CubConditionTypeLT,			// 4
		CubConditionTypeGT			// 5
};

typedef union ParVal {
	Uint32	u;
	Sint32	i;
	float	f;
} ParVal;

class CubStackFrameClass: public Parameter {
public:
	EnumPar     Type;
	UnsignedPar Start;
	UnsignedPar Reqd;
	UnsignedPar Count;
	UnsignedPar	Delay;
	UnsignedPar Tag;
};

class CubStackClass: public Parameter {
public:
	CubStackFrameClass stackFrame[CubStackSz];
	char name[CubStackSz][4];
};

class CubConditionTableEntryClass: public Parameter {
public:
	EnumPar 	Type;	/* Condition type				 		*/
	UnsignedPar	State;	/* Current state				 		*/
	UnsignedPar Par;	/* Pointer to parameter being monitored	*/
	UnsignedPar Val;	/* Value for comparison types			*/
	Uint32		Prev;	/* Previous value int/uint/float 		*/
};

class CubConditionTableClass: public Parameter {
public:
	CubConditionTableEntryClass Table[CubConditionTableSz];
	char name[CubConditionTableSz][4];
};

class CubIntTableEntryClass: public Parameter {
public:
	UnsignedPar	Mask;		/* Interrupt vector mask	*/
	UnsignedPar Handler;	/* Address of handler code	*/
};

class CubIntTableClass: public Parameter {
public:
	CubIntTableEntryClass Table[CubIntTableSz];
	char name[CubIntTableSz][4];
};

class CubInstructionClass: public Parameter {
public:
	UnsignedPar Address;
	UnsignedPar	Opcode;
	UnsignedPar	OperandPar;
	UnsignedPar OperandVal;
};

class CubicEngine: public Parameter {
public:
	void operator()(Parameter *par, char *n, CubInstruction *pPrgrm, SmpLimitSys &smp);
	EnumPar             RunMode;
	UnsignedPar         ProgramCounter;
	UnsignedPar         ProgramCap;
	UnsignedPar         PauseTag;
	UnsignedPar         StackPointer;
	IntegerPar			ResultRegister;
	FloatPar			CompareEpsilon;
	UnsignedPar			BaseTimerLow;
	UnsignedPar			BaseTimerHigh;
	UnsignedPar			WaitTime;
 	UnsignedPar         CurrentTime;
	EnumPar             Exception;
	IntegerPar          Reset;
	IntegerPar          Step;
	CubStackClass       Stack;
	FloatArrayPar       MemoryRegister;
	IntegerArrayPar     IntegerRegister;
	CubInstructionClass Instruction;
	UnsignedPar         FlagsWord;
	static              CubicEngine *IContext;
	Uint32				Delay;
	Uint32				ConditionStatus;
	Uint32				InterruptStatus;
	Uint32				InterruptMask;
	Uint32				WIMask;					/* Mask for WaitInt instruction 	*/
	Uint32				WIState;				/* State of WaitInt mask evaluation */
	CubConditionTableClass	ConditionTable;
	CubIntTableClass	IntTable;
	Uint32				GroupOn;
	EnumPar				ResetLoopState;
	void                doPoll();
	void                doIter();
	void				doInterrupt();
private:
	virtual Result special(int fn, void *in, int incnt, void *out, int outcnt);
	virtual void parPollSignal();
	virtual void parPrimeSignal();
	virtual void parSetEvent(Parameter *sce);
	virtual void parAlteredEvent(Parameter *sce);
	void doInstruction();
	void doException(Sint32 e);
	bool goodPar(Parameter *p, char t);
	bool goodPar(Parameter *p);
	void setIntegerPar(Parameter *p, Sint32 v);
	void updateInstruction();
	Sint32 getIntegerPar(Parameter *p);
	Uint32 getUnsignedPar(Parameter *p);
	float getFloatPar(Parameter *p);
	CubInstruction *pProgram;
	Uint32 seekPointer;
	SmpLimitSys *smpbsLimitSys;
	Uint32 *pCnetTsLo;
	Uint32 *pCnetTsHi;
	Generator *pGen;
	Uint32 TableIndex;
};

#endif
