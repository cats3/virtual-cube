// Generator.cpp - Function Generator Class module

#include "C3Std.h"
//#include "Events.h"

#ifdef SUPGEN

#ifdef FUDGE
Uint32 *statePeekaboo={NULL};
float  *transElapsedPeekaboo={NULL};
#endif

#ifdef KINETUSB
#ifdef PCTEST
Sint16 kinet[128];
Sint16 *kinet_get_slotaddr(Uint32 slot)
{
	return kinet+slot;
}
#else
extern "C" {
	Sint16 *kinet_get_slotaddr(Uint32 slot);
}
#endif
#endif
/*
// TSR usage (TSR0)
#define	TSRDump			0x00000001
#define	TSRStartInh		0x00000002
#define	TSRWarning		0x00000004
#define	TSRFreeze		0x00000008
#define	TSRStopDyn		0x00000010
#define	TSRStopStat		0x00000020
#define	TSRFuncPend		0x00000040
#define	TSRDynPend		0x00000080
#define	TSRStatPend		0x00000100
#define	TSRStopWR		0x00004000
#define	TSRUnloadWR		0x00008000
#define	TSRModeChStop	0x00400000
*/
static const Uint32 actionTable[]={
	0,																// Nothing
	TSRStartInh,													// Start inhibit
	TSRWarning,														// Warning
	TSRFreeze,														// Hold
	TSRStopDyn,														// Pause
	TSRStopDyn+TSRStopWR+TSRModeChStop,								// Stop
	TSRStopDyn+TSRStopWR+TSRModeChStop+TSRUnloadWR,					// Stop and unload
	TSRStopDyn+TSRStopStat+TSRModeChStop+TSRDump					// Unload immediately
};

static const Uint32 isSpFreezeTable[]={
	False,															// Nothing
	False,															// Start inhibit
	False,															// Warning
	True,															// Hold
	True,															// Pause
	True,															// Stop
	True,															// Stop and unload
	True															// Unload immediately
};

void Generator::operator()(Parameter *par, char *n, SmpGenerator &smp)
{
	static const char *(ChStatusStr[])		={"Stopped", "Started", "Generating", "Function_complete", NULL};
	static const char *(IntegrationStr[])	={"Single_ch", "Multi_ch_slave", "Multi_ch_master", NULL};
	static const char *(ElementTypeStr[])	={"None", "Dwell", "Transition", "Cyclic", "Sweep", "External", NULL};
	static const char *(RunStatusStr[])		={"Stopped", "Start_static", "Pause_static", "Start_dynamic", "Running",
												"Pause_dynamic", "Hold_dynamic", "Stop_static", "Stop_dynamic",
												"Function_complete", "Function_complete_ack", NULL};
	static const char *(GenStatusStr[])		={"Complete", "In_progress", NULL};
	static const char *(TransProfileStr[])	={"Linear", "Haversine", "Custom_linear", "Custom_spline", "Square", NULL};
	static const char *(WaveShapeStr[])		={"Sine", "Square", "Triangle", "Sawtooth", "Trapezoid","Custom", NULL};
	static const char *(AbleStr[])			={"Disabled", "Enabled", NULL};
	static const char *(ExtSourceStr[])		={"External", "CNet", NULL};
	static const char *(GenPAModeStr[])		={"Cycles", "Seconds", NULL};
	static const char *(NextElementStr[])	={"None", "Transition", "Dwell", "Cyclic", "MasterOnly", NULL};
	static const char *(LinLogStr[])		={"Linear", "Logarithmic", NULL};
	static const char *(ActionStr[])		={"Nothing", "Start_inhibit", "Warning", "Hold", "Pause",
												"Stop", "Stop_then_unload", "Unload_immediately", NULL};
	Parameter *p; 
	int i;

	smpbs=&smp;
	Parameter::operator()   (par, n);
	ChannelStatus           (this, "Channel_status", "rd",   Smp(chStatus),    (char **)ChStatusStr);
	Integration             (this, "Integration",    "nyz",  Smp(integration), (char **)IntegrationStr);
	TSRResponse             (this, "TSR_response",   "iny",  Smp(tsrResp),     (char **)AbleStr, 1);

	TSRIn                   (this, "TSR_in",         "rd",   Smp(tsrIn),       0, MaxUns);

	p=(Parameter *)&ElementTypes;
	(*p)(this, "Element_Types");
	(*p).setFlags("g");

	p=(Parameter *)&ElementTypes.Transition;
	(*p)                    (&ElementTypes, "Transition");
	ElementTypes.Transition.Time         (p, "Time",           "idn", Smp(transTime),     MINTIME,  MAXTIME,  "sec", MINTIME); //"dn", Smp(transTime),     MINTIME,  MAXTIME,  "sec", MINTIME);
	ElementTypes.Transition.Rate         (p, "Rate",           "in",  Smp(transRate),     0.0f,     MaxFloat, "units/sec");
	ElementTypes.Transition.ConstantRate (p, "Constant_rate",  "iny", Smp(transRateEn),              (char **)AbleStr);
	ElementTypes.Transition.Elapsed      (p, "Elapsed",        "dn", Smp(transElapsed),  0.0f,     MAXTIME,  "sec", MINTIME);
	ElementTypes.Transition.Timeout      (p, "Timeout",        "dn", Smp(transTimeout),  MINTIME,  524000.0f,  "sec", 2.0f);
	ElementTypes.Transition.TpCount      (p, "Turning_point_count",	"dn", Smp(countTP),  0,   2147483647,  "counts", 0); //Smp(countTP)
	ElementTypes.Transition.Profile      (p, "Profile",        "iny", Smp(profileTP),              (char **)TransProfileStr);
	ElementTypes.Transition.CustomProfile(p, "Custom_profile", "iny", NULL, GenProfSz,   true, 0.0f,     1.0f,     "(0..1)", (float *)profileNorm);
	ElementTypes.Transition.Continuous   (p, "Continuous_mode");
	ElementTypes.Transition.Continuous.Enable         (&ElementTypes.Transition.Continuous, "Enable",        "iny", Smp(crun),    (char **)AbleStr);      
	ElementTypes.Transition.Continuous.EndPointSlotNo (&ElementTypes.Transition.Continuous, "Endpoint_slot", "iny", Smp(endslot), 0, C3NTS-1, "", 0);
	ElementTypes.Transition.Continuous.TransTimeSlotNo(&ElementTypes.Transition.Continuous, "Duration_slot", "iny", NULL,         0, C3NTS-1, "", 0);
	ElementTypes.Transition.New          (p, "New",            "b",  NULL,              0,        1);
	ElementTypes.Transition.ForceComplete(p, "Force_complete", "b",  NULL,              0,        1);
	ElementTypes.Transition.Status       (p, "Status",         "r",  NULL,              (char **)GenStatusStr);
	ElementTypes.Transition.Progress     (p, "Progress",       "rq", NULL,              0.0f,     100.0f,   "%");
	ElementTypes.Transition.RateAdaptation   (p, "Rate_adaptation"); //rate adaptation will look at the turning point error and adjust the rate accordingly. Amplitude adaptation must be enabled on the transducer that you want the error to come from.
	ElementTypes.Transition.RateAdaptation.Enable		(&ElementTypes.Transition.RateAdaptation, "Enable",			"iny",	Smp(rateAdaptEn),	(char **)AbleStr); //enable rate adaptation
	ElementTypes.Transition.RateAdaptation.ReduceError	(&ElementTypes.Transition.RateAdaptation, "Reduce_error",	"dn",	Smp(reduceError),	0.0f,  MaxFloat,  "units", 0.0f);
	ElementTypes.Transition.RateAdaptation.ReduceRate	(&ElementTypes.Transition.RateAdaptation, "Reduce_rate",	"dn",	Smp(reduceRate),	0.0f,  MaxFloat,  "%", 0.0f);
	ElementTypes.Transition.RateAdaptation.IncreaseError(&ElementTypes.Transition.RateAdaptation, "Increase_error",	"dn",	Smp(increaseError),	0.0f,  MaxFloat,  "units", 0.0f);
	ElementTypes.Transition.RateAdaptation.IncreaseRate	(&ElementTypes.Transition.RateAdaptation, "Increase_rate",	"dn",	Smp(increaseRate),	0.0f,  MaxFloat,  "%", 0.0f);
	ElementTypes.Transition.RateAdaptation.CurrentRateGain	(&ElementTypes.Transition.RateAdaptation, "Current_Gain",	"dn",	Smp(rateFac),	0.0f,  MaxFloat,  "%", 0.0f);
	ElementTypes.Transition.DemandHold   	(p, "Demand_Hold"); // This will hold the output untill the FB catches up
	ElementTypes.Transition.DemandHold.EnableDemandHold		(&ElementTypes.Transition.DemandHold, "Enable",  "iny", Smp(tpTimeoutEn),(char **)AbleStr);
	ElementTypes.Transition.DemandHold.DwellErrorTP 		(&ElementTypes.Transition.DemandHold, "Dwell_Error",	"dn",	Smp(dwellErrorTP),	0.0f,  MaxFloat,  "units", 0.0f);
	ElementTypes.Transition.DemandHold.DwellDebounceTimeTP	(&ElementTypes.Transition.DemandHold, "Dwell_Debounce",	"dn",	Smp(dwellDebounceTimeTP),	0.0f,  MAXTIME,  "s", 0.0f);
	ElementTypes.Transition.DemandHold.DwellTimeout1TP		(&ElementTypes.Transition.DemandHold, "Dwell_Timeout1",	"dn",	Smp(dwellTimeout1TP),	0.0f,  MAXTIME,  "s", 0.0f);
	ElementTypes.Transition.DemandHold.DwellTimeout2TP		(&ElementTypes.Transition.DemandHold, "Dwell_Timeout2",	"dn",	Smp(dwellTimeout2TP),	0.0f,  MAXTIME,  "s", 0.0f);
	ElementTypes.Transition.DemandHold.DwellGainTP			(&ElementTypes.Transition.DemandHold, "Dwell_Gain",		"dn",	Smp(dwellGainTP),	0.0f,  MaxFloat,  "%", 0.0f);
	ElementTypes.Transition.DemandHold.DwellAction			(&ElementTypes.Transition.DemandHold, "Action",			"incy", NULL,	(char **)ActionStr);
	ElementTypes.Transition.ErrorLimit   	(p, "Error_Limit"); // This will cause an action if the TP error exceeds a setpoint.
	ElementTypes.Transition.ErrorLimit.Enable1	(&ElementTypes.Transition.ErrorLimit, "Enable1",	"iny", 	Smp(error1En),(char **)AbleStr);
	ElementTypes.Transition.ErrorLimit.Action1	(&ElementTypes.Transition.ErrorLimit, "Action1",	"incy", NULL,	(char **)ActionStr);
	ElementTypes.Transition.ErrorLimit.Error1 	(&ElementTypes.Transition.ErrorLimit, "Error1",		"dn",	Smp(errorThreshold1TP),	0.0f,  MaxFloat,  "units", 0.0f);
	ElementTypes.Transition.ErrorLimit.Trig1	(&ElementTypes.Transition.ErrorLimit, "Trig1",		"d", 	Smp(trig1),(char **)AbleStr);
	ElementTypes.Transition.ErrorLimit.Enable2	(&ElementTypes.Transition.ErrorLimit, "Enable2",	"iny", 	Smp(error2En),(char **)AbleStr);
	ElementTypes.Transition.ErrorLimit.Action2	(&ElementTypes.Transition.ErrorLimit, "Action2",	"incy", NULL,	(char **)ActionStr);
	ElementTypes.Transition.ErrorLimit.Error2 	(&ElementTypes.Transition.ErrorLimit, "Error2",		"dn",	Smp(errorThreshold2TP),	0.0f,  MaxFloat,  "units", 0.0f);
	ElementTypes.Transition.ErrorLimit.Trig2	(&ElementTypes.Transition.ErrorLimit, "Trig2",		"d", 	Smp(trig2),(char **)AbleStr); // worked with flag "i" before

	p=(Parameter *)&ElementTypes.Dwell;
	(*p)                    (&ElementTypes, "Dwell");
	ElementTypes.Dwell.Time              (p, "Time",           "dn", Smp(dwellTime),    MINTIME,  MAXTIME, "sec", MINTIME);
	ElementTypes.Dwell.Elapsed           (p, "Elapsed",        "dn", Smp(dwellElapsed), 0.0f,     MAXTIME, "sec", MINTIME);
	ElementTypes.Dwell.New               (p, "New",            "b",  NULL,              0,        1);
	ElementTypes.Dwell.ForceComplete     (p, "Force_complete", "b",  NULL,              0,        1);
	ElementTypes.Dwell.Status            (p, "Status",         "r",  NULL,              (char **)GenStatusStr);
	ElementTypes.Dwell.Progress          (p, "Progress",       "rq", NULL,              0.0f,     100.0f,  "%");

	p=(Parameter *)&ElementTypes.Cyclic;
	(*p)                    (&ElementTypes, "Cyclic");
	ElementTypes.Cyclic.Frequency        (p, "Frequency",       "dny", Smp(phaseInc),   MINFREQ,  MAXFREQ,     "Hz", 1.0f);
	ElementTypes.Cyclic.Frequency.setMap(1.0f/SysFs);
	ElementTypes.Cyclic.CyclesRequired   (p, "Cycles_required", "dn",  Smp(cyclsRqrd),  0 ,       4000000000L, "cycles");
	ElementTypes.Cyclic.CyclesComplete   (p, "Cycles_complete", "dn",  Smp(cyclsCmplt), 0,        4000000000L, "cycles");
	ElementTypes.Cyclic.Phase            (p, "Phase",           "dnv", Smp(phase),      0.0f,     360.0f,      "degrees");
	ElementTypes.Cyclic.DirectStart      (p, "Direct_start",    "iny",  Smp(cycDirStt),  (char **)AbleStr);
	ElementTypes.Cyclic.Harmonic         (p, "Harmonic",        "iny",  NULL,            1,        5,           "",   1);
	ElementTypes.Cyclic.WaveShape        (p, "Wave_shape",      "iny",  Smp(waveShape),  (char **)WaveShapeStr);
	ElementTypes.Cyclic.AntiAlias        (p, "Anti_alias",      "iny",  Smp(antiAlias),  (char **)AbleStr);
	ElementTypes.Cyclic.TrapezoidRatio   (p, "Trapezoid_ratio", "iny",  Smp(trapRat),    0.0f,     MaxFloat,    "",  0.7f);
	ElementTypes.Cyclic.New              (p, "New",             "b",   NULL,            0,        1);
	ElementTypes.Cyclic.ForceComplete    (p, "Force_complete",  "b",   NULL,            0,        1);
	ElementTypes.Cyclic.Status           (p, "Status",          "r",   NULL,            (char **)GenStatusStr);
	ElementTypes.Cyclic.Progress         (p, "Progress",        "rq",  NULL,            0.0f,     100.0f,      "%");

	p=(Parameter *)&ElementTypes.Sweep;
	(*p)                    (&ElementTypes, "Sweep");
	ElementTypes.Sweep.Time              (p, "Time",            "iny",  Smp(sweepTime),    MINTIME,  MAXTIME, "sec", MINTIME);
	ElementTypes.Sweep.Elapsed           (p, "Elapsed",         "dn",  Smp(sweepElapsed), 0.0f,     MAXTIME, "sec", MINTIME);
	ElementTypes.Sweep.StartFrequency    (p, "Start_frequency", "iny",  NULL,              MINFREQ,  MAXFREQ, "Hz",  1.0f);
	ElementTypes.Sweep.EndFrequency      (p, "End_frequency",   "iny",  NULL,              MINFREQ,  MAXFREQ, "Hz",  1.0f);
	ElementTypes.Sweep.LogMode           (p, "Log_mode",        "iny",  Smp(sweepLogMode), (char **)LinLogStr);
	ElementTypes.Sweep.New               (p, "New",             "b",   NULL,              0,        1);
	ElementTypes.Sweep.ForceComplete     (p, "Force_complete",  "b",   NULL,              0,        1);
	ElementTypes.Sweep.Status            (p, "Status",          "r",   NULL,              (char **)GenStatusStr);
	ElementTypes.Sweep.Progress          (p, "Progress",        "rq",  NULL,              0.0f,     100.0f,  "%");
	ElementTypes.Sweep.Frequency         (p, "Frequency",       "r",   NULL,              MINFREQ,  MAXFREQ, "Hz", MINFREQ);

	p=(Parameter *)&ElementTypes.External;
	(*p)                    (&ElementTypes, "External");
	ElementTypes.External.Time           (p, "Time",             "iny", Smp(extTime),    MINTIME,  MAXTIME, "sec", MINTIME);
	ElementTypes.External.Elapsed        (p, "Elapsed",          "dn", Smp(extElapsed), 0.0f,     MAXTIME, "sec", MINTIME);
	ElementTypes.External.RunIndef       (p, "Run_indefinitely", "iny", Smp(extIndef),   (char **)AbleStr);
	ElementTypes.External.Source         (p, "Source",           "iny", Smp(extSource),  (char **)ExtSourceStr);
	ElementTypes.External.New            (p, "New",              "b",  NULL,            0,        1);
	ElementTypes.External.ForceComplete  (p, "Force_complete",   "b",  NULL,            0,        1);
	ElementTypes.External.Status         (p, "Status",           "r",  NULL,            (char **)GenStatusStr);
	ElementTypes.External.Progress       (p, "Progress",         "rq", NULL,            0.0f,     100.0f,  "%");

	MasterBuffer(this, "Buffer");
	p=&MasterBuffer;
	MasterBuffer.ElementType    (p, "Element_type",     "dn", Smp(nextType),      (char **)NextElementStr);
	MasterBuffer.TransitionTime (p, "Transition_time",  "in",  Smp(nextTransTime), MINTIME, MAXTIME, "sec", MINTIME);
	MasterBuffer.DwellTime      (p, "Dwell_time",       "in",  Smp(nextDwellTime), MINTIME, MAXTIME, "sec", MINTIME);
	MasterBuffer.CyclicFrequency(p, "Cyclic_frequency", "in",  NULL,               MINFREQ, MAXFREQ, "Hz",  1.0f);
	MasterBuffer.CyclicCycles   (p, "Cyclic_cycles",    "in",  Smp(nextCycles),    0,       4000000000L,    "cycles");

	Levels(this, "Levels");
	Levels.Range(&Levels, "Range", "ni");
  //  Levels.Broadcast   (&Levels, "Broadcast",  "b",  Smp(broadcast),       0,    1);   

	p=&Levels.Buffer;
	(*p)(&Levels, "Buffer");
	Levels.Buffer.ElementType(p, "Element_type", "dn",   Smp(slaveNextType), (char **)NextElementStr);
	Levels.Buffer.TransTarget(p, "Target",       "imnvy", Smp(bufTarget), MinFloat, MaxFloat);
	Levels.Buffer.Amplitude  (p, "Amplitude",    "iknvy", Smp(bufAmpl),   0.0f,     MaxFloat);
	Levels.Buffer.PhaseOffset(p, "Phase",        "invy",  Smp(bufPhase),  -180.0f,  180.0f, "degrees");

	p=&Levels.Static;
	(*p)(&Levels, "Static");
	(*p).setFlags("g");
	Levels.Static.BaseLine(p, "Baseline", "dmvw",  Smp(baseLine), MinFloat, MaxFloat);
	Levels.Static.Bias    (p, "Bias",     "sdnmv", Smp(bias),     MinFloat, MaxFloat);
	Levels.Static.Origin  (p, "Origin",   "ndmv",  Smp(origin),   MinFloat, MaxFloat);
	Levels.Static.Target  (p, "Target",   "ndmv",  Smp(target),   MinFloat, MaxFloat);

	Levels.StatBiasTrack  (&Levels, "Static_bias_track", "iny", Smp(statBiasTrack), (char **)AbleStr);
	Levels.BaselineTrack  (&Levels, "Baseline_track",    "iny", Smp(baselineTrack), (char **)AbleStr);

	p=&Levels.Dynamic;
	(*p)(&Levels, "Dynamic");
	(*p).setFlags("g");
	Levels.Dynamic.Amplitude  (p, "Amplitude", "sdnkv", Smp(amplitude),   0.0f,     MaxFloat);
	Levels.Dynamic.Bias       (p, "Bias",      "sdnmv", Smp(cycBias),     MinFloat, MaxFloat);
	//Levels.Dynamic.Peak       (p, "Peak",      "dnm",    Smp(peakTemp),             MinFloat, MaxFloat);
	//Levels.Dynamic.Trough     (p, "Trough",    "dnm",    Smp(troughTemp),             MinFloat, MaxFloat);
	Levels.Dynamic.Peak       (p, "Peak",      "dnm",    NULL,             MinFloat, MaxFloat);
	Levels.Dynamic.Trough     (p, "Trough",    "dnm",    NULL,             MinFloat, MaxFloat);
	Levels.Dynamic.PhaseOffset(p, "Phase",     "sdnv",  Smp(phaseOffset), -180.0f,  180.0f, "degrees");

	Levels.DynBiasTrack (&Levels, "Dynamic_bias_track", "iny",   Smp(dynBiasTrack), (char **)AbleStr);
	Levels.Output       (&Levels, "Output",             "rmdv", Smp(output),       MinFloat, MaxFloat);

	Levels.Modes(&Levels, "Modes");
	for(i=0; i<EPNMode; i++)
	{
		sprintf(Levels.Modes.numBuf[i], "%d", i+1);
		Levels.Modes.Members[i](&Levels.Modes, Levels.Modes.numBuf[i]);
		Levels.Modes.Members[i].BaseLine(Levels.Modes.Members+i, "BaseLine", "sdnvy", smpbs->modes+i, -100.0f, 100.0f, "%");
	}
    //	TestControl.Start        (p, "Start",            "b",  Smp(start),       0,    1);

	p=(Parameter *)&StartStop;
	(*p)                    (this, "Start_Stop");
	StartStop.StartRampTime (p, "Start_ramp_time",  "iny", NULL, 0.0f, 1000.0f, "sec",    2.0f);
	StartStop.StopRampTime  (p, "Stop_ramp_time",   "iny", NULL, 0.0f, 1000.0f, "sec",    2.0f);
	StartStop.FadeInCycles  (p, "Fade_in_cycles",   "iny", NULL, 1,    1000,    "cycles", 2);
	StartStop.FadeOutCycles (p, "Fade_out_cycles",  "iny", NULL, 1,    1000,    "cycles", 2);
	StartStop.StopAtTestEnd (p, "Stop_at_test_end", "iny", Smp(stopAtEnd), (char **)AbleStr);
	StartStop.FadeInTime    (p, "Fade_in_time",     "iny", NULL, 0.0f, 1000.0f, "sec",    2.0f);
	StartStop.FadeOutTime   (p, "Fade_out_time",    "iny", NULL, 0.0f, 1000.0f, "sec",    2.0f);

	PerAcq(this, "Periodic_Acquisition");
	PerAcq.Enable(&PerAcq, "Enable", "iny", Smp(PAEn), (char **)AbleStr);
	p=(Parameter *)&PerAcq.Delay;
	(*p)(&PerAcq, "Delay");
	p->setFlags("g");
	PerAcq.Delay.Setting      (p, "Setting",       "ny", Smp(PADelay),     0, 4000000000L);
	PerAcq.Delay.Specification(p, "Specification", "ny", Smp(PADelayMode), (char **)GenPAModeStr);
	PerAcq.Delay.Remaining    (p, "Remaining",     "dn", Smp(PADelayRem),  0, 4000000000L);
	p=(Parameter *)&PerAcq.Repeat;
	(*p)(&PerAcq, "Repeat");
	p->setFlags("g");
	PerAcq.Repeat.Setting      (p, "Setting",        "ny", Smp(PARepeat),        0, 4000000000L);
	PerAcq.Repeat.Specification(p, "Specification",  "ny", Smp(PARepeatMode),    (char **)GenPAModeStr);
	PerAcq.Repeat.LogLaw       (p, "Log_law",        "ny", Smp(PALogLaw),        1.0f, 10.0f,    "", 1.0f);
	PerAcq.Repeat.CurrentFactor(p, "Current_factor", "dn", Smp(PACurrentFactor), 1.0f, MaxFloat, "", 1.0f);
	PerAcq.Repeat.Remaining    (p, "Next",           "dn", Smp(PARepeatNext),    0, 4000000000L);
	p=(Parameter *)&PerAcq.Duration;
	(*p)(&PerAcq, "Duration");
	p->setFlags("g");
	PerAcq.Duration.Setting      (p, "Setting",       "ny", Smp(PADuration),     0, 4000000000L);
	PerAcq.Duration.Specification(p, "Specification", "ny", Smp(PADurationMode), (char **)GenPAModeStr);
	PerAcq.Duration.Remaining    (p, "End",           "dn", Smp(PADurationEnd),  0, 4000000000L);
	PerAcq.Active(&PerAcq, "Active", "rdl", Smp(PAAct), 0, 1, "yellow");

#ifdef KINETUSB
	p=(Parameter *)&KiNetSlot;
	(*p)(this, "KiNet_Output");
	(*p).setFlags("g");
	KiNetSlot.Enable(p, "Enable",       "ny", NULL,  (char **)AbleStr);
	KiNetSlot.SlotNo(p, "Slot_number",  "ny",  NULL,  0, 127);
#ifdef PCTEST
	KiNetSlot.Slot37(p, "Time_slot_37", "rdv", Smp(kinetSlot37), -100.0f, 100.0f, "%");
#endif
#endif

	p=(Parameter *)&CNetMon;
	(*p)(this, "CNet_Monitors");
	CNetMon.CycleCount(p, "Cycle_Count");
	CNetMon.CycleCount.setFlags("g");
	CNetMon.Elapsed(p, "Elapse_Time");
	CNetMon.Elapsed.setFlags("g");
	p=(Parameter *)&CNetMon.CycleCount;
	CNetMon.CycleCount.Enable(p, "Enable",    "ny", Smp(cnetCCEnable),     (char **)AbleStr);
	CNetMon.CycleCount.Slot  (p, "CNet_slot", "ny", Smp(cnetCCSlot),       0, C3NTS-1, "", 0);
	p=(Parameter *)&CNetMon.Elapsed;
	CNetMon.Elapsed.Enable   (p, "Enable",    "ny", Smp(cnetElapseEnable), (char **)AbleStr);
	CNetMon.Elapsed.Slot     (p, "CNet_slot", "ny", Smp(cnetElapseSlot),   0, C3NTS-1, "", 0);

	p=(Parameter *)&AFC;
	(*p)(this, "AFC");
	AFC.Enable    (p, "Enable",      "ny", Smp(afcEn),     (char **)AbleStr);
	AFC.Mode      (p, "Mode",        "ny", Smp(afcMode),   (char **)LinLogStr);
	AFC.LinearRate(p, "Linear_rate", "ny", Smp(afcLinInc), MINFREQ, 0.1f*MAXFREQ, "Hz/sec", 0.1f);
	AFC.LinearRate.setMap(1.0f/(SysFs*SysFs));
	AFC.LogRate   (p, "Log_rate",    "ny", Smp(afcLogFac), 0.1f,    100.0f,       "%/sec",  0.1f);
	AFC.LogRate.setMap(0.01f/SysFs);

	p=(Parameter *)&SpanSet;
	(*p)(this, "Global_controls");
	(*p).setFlags("g");
	SpanSet.GlobalSpan    (p, "Global_span",      "snvy", Smp(globalSpan),         0.0f, 100.0f, "%", 100.0f);
	SpanSet.LocalSpan     (p, "Local_span",       "sdnv", Smp(localSpan),          0.0f, 100.0f, "%", 100.0f);
	SpanSet.GlobalSetPoint(p, "Global_set_point", "sdv" , Smp(globalSetPoint),  -100.0f, 100.0f, "%", 0.0f);
	SpanSet.Reset         (p, "Re_zero",          "b",    Smp(gspreset), 0, 1);

	p=(Parameter *)&TestControl;
	(*p)(this, "Master_Control");
	(*p).setFlags("g");
	TestControl.Start        (p, "Start",            "b",  Smp(start),       0,    1);
	TestControl.Stop         (p, "Stop",             "b",  Smp(stop),        0,    1);
	TestControl.Pause        (p, "Pause",            "b",  Smp(pause),       0,    1);
	TestControl.Hold         (p, "Hold",             "b",  Smp(hold),        0,    1);
	TestControl.NudgeUp      (p, "Nudge_up",         "b",  Smp(nudgeUp),     0,    1);
	TestControl.NudgeDown    (p, "Nudge_down",       "b",  Smp(nudgeDown),   0,    1);
	TestControl.ackComplete  (p, "Ack_complete",     "b",  Smp(ackComplete), 0,    1);
	TestControl.Status       (p, "Status",           "rd", Smp(runStatus),   (char **)RunStatusStr);
	TestControl.StatusPos	 (p, "Status_pos",       "u",  NULL,			 0,    8);

	p=(Parameter *)&TestControl.CurrentElement;
	(*p)(&TestControl, "Current_Element");
	(*p).setFlags("g");
	TestControl.CurrentElement.Type    (p, "Type",     "rd", Smp(elementType), (char **)ElementTypeStr);
	TestControl.CurrentElement.Progress(p, "Progress", "rq", NULL,             0.0f, 100.0f, "%");
	TestControl.CurrentElement.Elapsed (p, "Elapsed",  "dn", Smp(elapsed),     0,    4000000000L, "seconds");
	TestControl.CurrentElement.Ticks   (p, "Ticks",    "dn", Smp(elapseCount), 0,    4000000000L, "samples");

	pTsrIn=LimitSys::IContext.smpCntxt?&((LimitSys::IContext.smpCntxt)->tsrInBufLo):NULL;
	pTsrOut=LimitSys::IContext.smpCntxt?((LimitSys::IContext.smpCntxt)->tsrOutBufLo):NULL;
	pGenBusIn=NULL;
	pGenBusOut=NULL;
	pPTAPort=NULL;
	pExtCmd=NULL;
	pCNetCmd=NULL;
	pCNet=NULL;
	pModeNo=NULL;

#ifdef FUDGE
	if(!statePeekaboo)
	{
		statePeekaboo=&(smpbs->state);
		transElapsedPeekaboo=&(smpbs->transElapsed);
	}
#endif
} 

Result Generator::special(int fn, void *in, int incnt, void *out, int outcnt)
{
	int i, j;
	float *p, x;

	if((fn<ParSp_WT0)||(fn>ParSp_WT3))
		return Parameter::special(fn, in, incnt, out, outcnt);
	if(incnt<256)
		return ParBufferError;
	p=(float *)in;

	i=((fn-ParSp_WT0-1)&3)*64;
    x=Peek(smpbs->wvTbl[i+63][0]);
	Poke(smpbs->wvTbl[i+63][1], p[0]-x);

	i=(fn-ParSp_WT0)*64;
	for(j=0; j<63; j++)
	{
		Poke(smpbs->wvTbl[i+j][0], p[j]);
		Poke(smpbs->wvTbl[i+j][1], p[j+1]-p[j]);
	}

	j=((fn-ParSp_WT0+1)&3)*64;
    x=Peek(smpbs->wvTbl[j][0]);
	Poke(smpbs->wvTbl[i+63][0], p[63]);
	Poke(smpbs->wvTbl[i+63][1], x-p[63]);

	return ParOkay;
}

void Generator::parPrimeSignal()
{
	Parameter::parPrimeSignal();		// Important - prime parameters first!
	newStartRampTime();
	newStopRampTime();
	newFadeInCycles();
	newFadeOutCycles();
	if(ElementTypes.Transition.ConstantRate())
		newTransRate();
	else
		newTransTime();
	initStdProfiles();
	newTransProfile();
	newCustomProfile();
	newNextFrequency();
	newWaveTable();
	newHarmonic();
	newSweepTime();
	newStartFrequency();
	newEndFrequency();
	newRange();
	Poke(smpbs->pTsrIn, pTsrIn);
	Poke(smpbs->pTsrOut, pTsrOut);
	smpbs->pGenBusOut = pGenBusOut;
	smpbs->pGenBusIn = pGenBusIn;
	smpbs->pPTAPort = pPTAPort;
	//Poke(*((Uint32 *)&smpbs->pGenBusIn), (Uint32)pGenBusIn);
	//Poke(*((Uint32 *)&smpbs->pGenBusOut), (Uint32)pGenBusOut);
	//Poke(*((Uint32 *)&smpbs->pPTAPort), (Uint32)pPTAPort);
	Poke(smpbs->pCnetTsLo, pCnetTsLo);
	Poke(smpbs->pCnetTsHi, pCnetTsHi);
	Poke(smpbs->pExtCmd, pExtCmd);
	Poke(smpbs->pCNetCmd, pCNetCmd);
	Poke(smpbs->pCNet, pCNet);
	Poke(smpbs->pModeNo, pModeNo);
	Poke(smpbs->pFeedback, pFeedback);
	Poke(smpbs->ackComplete, 0);
	newFCyclsCmplt();
#ifdef KINETUSB
	newKiNetSlot();
#endif
	newFadeInTime();
	newFadeOutTime();
	newDwellAction();
	newTPerror1Action();
	newTPerror2Action();
	//ElementTypes.Transition.ErrorLimit.Trig1() = 0;
	//ElementTypes.Transition.ErrorLimit.Trig2() = 0;
	initTPtrig1();
	initTPtrig2();

	/* Test code for 'special' * /
	float buf[256];
	int i;
	for(i=0; i<256; i++)
		buf[i]=sinf((8.0f*((float)Pi)/128.0f)*((float)i));
	special(0, buf,     256, NULL, 0);
	special(1, buf+64,  256, NULL, 0);
	special(2, buf+128, 256, NULL, 0);
	special(3, buf+192, 256, NULL, 0);
	*/
}

void Generator::parConfigureSignal()
{
	Parameter::parConfigureSignal();
	smpbs->pGenBusIn = pGenBusIn;
	//Poke(*((Uint32 *)&smpbs->pGenBusIn), (Uint32)pGenBusIn);
	Poke(smpbs->pExtCmd, pExtCmd);
}

void Generator::parPollSignal()
{
	Parameter::parPollSignal();
	newDwellProgress();
	newTransProgress();
	newCyclicProgress();
	newSweepProgress();
	newExternalProgress();
	newSweepFrequency();
	newProgress();
	newDwellStatus();
	newTransStatus();
	newCyclicStatus();
	newSweepStatus();
	newExternalStatus();
	//
	newTPtrig1(&ElementTypes.Transition.ErrorLimit.Error1);
	newTPtrig2(&ElementTypes.Transition.ErrorLimit.Error2);
	newTPdwellTrig(&ElementTypes.Transition.DemandHold);

//	(*((int *)0x80))++;
}

void Generator::parManagedSetEvent(Parameter *sce)
{
	int i;
	Float v;

	if(sce==&Levels.Static.BaseLine)
	{
		Levels.Static.BaseLine.getMax(&v, 4);
		v=100.0f*Levels.Static.BaseLine.getAssignedValue()/v;
//		Trace("Set baseline to %f", v);
		for(i=0; i<EPNMode; i++)
			Levels.Modes.Members[i].BaseLine=v;
	}
}

void Generator::parSetEvent(Parameter *sce)
{
	if(sce==&ElementTypes.Dwell.New)
		newDwell();
	if(sce==&ElementTypes.Dwell.ForceComplete)
		forceDwell();
	if(sce==&ElementTypes.Transition.New)
		newTrans();
	if(sce==&ElementTypes.Transition.ForceComplete)
		forceTrans();
	if(sce==&ElementTypes.Cyclic.New)
		newCyclic();
	if(sce==&ElementTypes.Cyclic.ForceComplete)
		forceCyclic();
	if(sce==&ElementTypes.Sweep.New)
		newSweep();
	if(sce==&ElementTypes.Sweep.ForceComplete)
		forceSweep();
	if(sce==&ElementTypes.External.New)
		newExternal();
	if(sce==&ElementTypes.External.ForceComplete)
		forceExternal();
	if(sce==&ElementTypes.Cyclic.CyclesComplete)
		newFCyclsCmplt();
	if(sce==&ElementTypes.Cyclic.Phase)
		newFCyclsCmplt();
	if(sce==&TestControl.CurrentElement.Elapsed)
		Poke(smpbs->elapseCount, 0);
}

void Generator::parAlteredEvent(Parameter *sce)
{
	if(sce==&Integration)
		parConfigChangedEvent(this);
	if(sce==&ElementTypes.Transition.CustomProfile)
		newCustomProfile();
	if(sce==&StartStop.StartRampTime)
		newStartRampTime();
	if(sce==&StartStop.StopRampTime)
		newStopRampTime();
	if(sce==&StartStop.FadeInCycles)
		newFadeInCycles();
	if(sce==&StartStop.FadeOutCycles)
		newFadeOutCycles();
	if(sce==&Levels.Dynamic.Amplitude)
		newAmplBias();
	if(sce==&Levels.Dynamic.Bias)
		newAmplBias();
	if(sce==&Levels.Dynamic.Peak){
//		Poke(smpbs->test1, 1.0f);
		newPeak();}
	if(sce==&Levels.Dynamic.Trough){
//		Poke(smpbs->test2, 1.0f);
		newTrough();}
	if(sce==&ElementTypes.Transition.Continuous.Enable)
		Poke(smpbs->rateFac, 0);
	if(sce==&ElementTypes.Transition.RateAdaptation.Enable)
	{
		//DSPB now works out all this...
//		Poke(smpbs->newTransCalc, 1);
		if(ElementTypes.Transition.RateAdaptation.Enable())
		{
			newTransRate();
		}
		else
		{
			newTransTime();
		}
	}
	if(sce==&ElementTypes.Transition.Time)
		newTransTime();//Poke(smpbs->newTransCalc, 1);//
	if(sce==&ElementTypes.Transition.Rate)
		newTransRate();//Poke(smpbs->newTransCalc, 1);//
	if(sce==&ElementTypes.Transition.Profile)
		newTransProfile();
	if(sce==&ElementTypes.Cyclic.Frequency)
		if(ElementTypes.Cyclic.AntiAlias()==GenEnabled)
			newWaveTable();
	if(sce==&MasterBuffer.CyclicFrequency)
		newNextFrequency();
	if(sce==&ElementTypes.Cyclic.WaveShape)
		newWaveTable();
	if(sce==&ElementTypes.Cyclic.Harmonic)
		newHarmonic();
	if(sce==&ElementTypes.Cyclic.TrapezoidRatio)
	{
		if(ElementTypes.Cyclic.WaveShape()==GenTrapezoid)
			newWaveTable();
	}
	if(sce==&ElementTypes.Cyclic.AntiAlias)
		newWaveTable();
	if(sce==&ElementTypes.Sweep.Time)
		newSweepTime();
	if(sce==&ElementTypes.Sweep.StartFrequency)
		newStartFrequency();
	if(sce==&ElementTypes.Sweep.EndFrequency)
		newEndFrequency();
#ifdef KINETUSB
	if((sce==&KiNetSlot.SlotNo)||(sce==&KiNetSlot.Enable))
		newKiNetSlot();
#endif
	if(sce==&StartStop.FadeInTime)
		newFadeInTime();
	if(sce==&StartStop.FadeOutTime)
		newFadeOutTime();
	if(sce==&ElementTypes.Transition.DemandHold.DwellAction)
		newDwellAction();
	if(sce==&ElementTypes.Transition.ErrorLimit.Action1)
		newTPerror1Action();
	if(sce==&ElementTypes.Transition.ErrorLimit.Action2)
		newTPerror2Action();
	if(sce==&ElementTypes.Transition.ErrorLimit.Error1)
		newTPerror1Action();
	if(sce==&ElementTypes.Transition.ErrorLimit.Error2){
		newTPerror2Action();
		//Syslog(Uint32 type, Uint32 tslo, Uint32 tshi, Parameter *p, Uint32 vio); //for testing.
		/*if(pCnetTsLo)
			cnetTsLo=*pCnetTsLo;
		if(pCnetTsHi)
			cnetTsHi=*pCnetTsHi;*/
		//Syslog(1, 5, 6, sce, 1234); //doesn't work :(
		}
	if(sce==&ElementTypes.Transition.ErrorLimit.Trig1)
		newTPtrig1(&ElementTypes.Transition.ErrorLimit.Error1);
	if(sce==&ElementTypes.Transition.ErrorLimit.Trig2)
		newTPtrig2(&ElementTypes.Transition.ErrorLimit.Error2);
}

#ifdef KINETUSB
void Generator::newKiNetSlot()
{
	Poke(smpbs->pKiNetSlot, KiNetSlot.Enable()?(Uint32 *)kinet_get_slotaddr((Uint32)KiNetSlot.SlotNo()):NULL);
}
#endif

void Generator::parRangeChangedEvent(Parameter *sce)
{
	if(sce==&Levels.Range)
		newRange();
}

void Generator::parChangedEvent(Parameter *sce)
{
	if(sce==&Levels.Dynamic.Amplitude)
		newAmplBias();
	if(sce==&Levels.Dynamic.Bias)
		newAmplBias();
}



const Generator::profileNormType Generator::profileNorm={
	0.03125f, 0.0625f, 0.09375f, 0.125f, 0.15625f, 0.1875f,	0.21875f, 0.25f,
	0.28125f, 0.3125f, 0.34375f, 0.375f, 0.40625f, 0.4375f, 0.46875f, 0.5f,
	0.53125f, 0.5625f, 0.59375f, 0.625f, 0.65625f, 0.6875f, 0.71875f, 0.75f,
	0.78125f, 0.8125f, 0.84375f, 0.875f, 0.90625f, 0.9375f, 0.96875f, 1.0f
};

void Generator::initLogFreqTbl()
{
	int i;
	float x, y, z;

	x=frequencyToInc(ElementTypes.Sweep.StartFrequency());
	y=frequencyToInc(ElementTypes.Sweep.EndFrequency())/x;
	for(i=0; i<=GenLogFreqTableSz; i++)
		Poke(smpbs->logFreqTbl[i], z=x*powf(y,(float)i/(float)GenLogFreqTableSz));
	Poke(smpbs->logFreqTbl[GenLogFreqTableSz+1], z);
}

void Generator::newRange()
{
	Float fs;
	Integer um;

	fs=Levels.Range.FullScale();
	um=Levels.Range.UnipolarMode();
	broadcastRange(&Levels, fs, um, Levels.Range.Units());
	Poke(smpbs->fullScale, fs);
}

void Generator::newDwellAction() //action for Demand hold
{
	Uint32 action = ElementTypes.Transition.DemandHold.DwellAction();
	Uint32 flags = actionTable[ElementTypes.Transition.DemandHold.DwellAction()];

	//if (isSpFreezeTable[action]) flags |= LimitSys::GetDirectionFreezeFlags(Details.Direction());
	Poke(smpbs->dwellActionTP, flags);
}

void Generator::newTPerror1Action() //action for TPerror
{
	Uint32 action = ElementTypes.Transition.ErrorLimit.Action1();
	Uint32 flags = actionTable[ElementTypes.Transition.ErrorLimit.Action1()];

	//if (isSpFreezeTable[action]) flags |= LimitSys::GetDirectionFreezeFlags(Details.Direction());
	Poke(smpbs->errorAction1TP, flags);
}

void Generator::newTPerror2Action() //action for TPerror
{
	Uint32 action = ElementTypes.Transition.ErrorLimit.Action2();
	Uint32 flags = actionTable[ElementTypes.Transition.ErrorLimit.Action2()];

	//if (isSpFreezeTable[action]) flags |= LimitSys::GetDirectionFreezeFlags(Details.Direction());
	Poke(smpbs->errorAction2TP, flags);
}

void Generator::newTPdwellTrig(Parameter *sce) //TPerror triggered
{
	//if(ElementTypes.Transition.ErrorLimit.Trig1()==1){ //with "i"
	if(Peek(smpbs->dwellTrig)==1){
		Uint32 tslo = Peek(smpbs->cnetTsLoDwell);
		Uint32 tshi	= Peek(smpbs->cnetTsHiDwell);
		//Uint32 vio	= Peek(smpbs->errorTP);
		Syslog(1, tslo, tshi, sce,0);
		Poke(smpbs->dwellTrig, 0);
		//ElementTypes.Transition.ErrorLimit.Trig1=0; //with "i"
	}
}

void Generator::newTPtrig1(Parameter *sce) //TPerror triggered
{
	//if(ElementTypes.Transition.ErrorLimit.Trig1()==1){ //with "i"
	if(Peek(smpbs->trig1)==1){
		Uint32 tslo = Peek(smpbs->cnetTsLo1);
		Uint32 tshi	= Peek(smpbs->cnetTsHi1);
		Uint32 vio	= 0;
		Syslog(1, tslo, tshi, sce,vio);
		Poke(smpbs->trig1, 0);
		//ElementTypes.Transition.ErrorLimit.Trig1=0; //with "i"
	}
}

void Generator::newTPtrig2(Parameter *sce) //TPerror triggered
{
	if(Peek(smpbs->trig2)==1){
		Uint32 tslo = Peek(smpbs->cnetTsLo2);
		Uint32 tshi	= Peek(smpbs->cnetTsHi2);
		Uint32 vio	= 0;
		Syslog(1, tslo, tshi, sce,vio);
		Poke(smpbs->trig2, 0);
		//ElementTypes.Transition.ErrorLimit.Trig1=0; //with "i"
	}
}

void Generator::initTPtrig1() //TPerror trigger init
{
	ElementTypes.Transition.ErrorLimit.Trig1 = 0;
	//Poke(smpbs->trig1, 0);
}

void Generator::initTPtrig2() //TPerror trigger init
{
	ElementTypes.Transition.ErrorLimit.Trig2 = 0;
	//Poke(smpbs->trig2, 0);
}

void Generator::newStartRampTime(){Poke(smpbs->startRampInc, timeToInc(StartStop.StartRampTime()));}

void Generator::newStopRampTime(){Poke(smpbs->stopRampInc, timeToInc(StartStop.StopRampTime()));}

void Generator::newFadeInTime(){Poke(smpbs->fadeInInc, timeToInc(StartStop.FadeInTime()));}

void Generator::newFadeOutTime(){Poke(smpbs->fadeOutInc, timeToInc(StartStop.FadeOutTime()));}

void Generator::newFadeInCycles()
{
	if(StartStop.FadeInCycles()==0)
		Poke(smpbs->CFIFac, GenMaxFade);
	else
		Poke(smpbs->CFIFac, 1.0f/(float)StartStop.FadeInCycles());
}

void Generator::newFadeOutCycles()
{
	if(StartStop.FadeOutCycles()==0)
		Poke(smpbs->CFOFac, GenMaxFade);
	else
		Poke(smpbs->CFOFac, 1.0f/(float)StartStop.FadeOutCycles());
}

void Generator::newProgress()
{
	switch(TestControl.CurrentElement.Type())
	{
	case GenDwell:
		TestControl.CurrentElement.Progress=ElementTypes.Dwell.Progress();
		break;
	case GenTransition:
		TestControl.CurrentElement.Progress=ElementTypes.Transition.Progress();
		break;
	case GenCyclic:
		TestControl.CurrentElement.Progress=ElementTypes.Cyclic.Progress();
		break;
	case GenSweep:
		TestControl.CurrentElement.Progress=ElementTypes.Sweep.Progress();
		break;
	case GenExternal:
		TestControl.CurrentElement.Progress=ElementTypes.External.Progress();
		break;
	default:
		TestControl.CurrentElement.Progress=0.0f;
	}
}

void Generator::newAmplBias()
{
	Float bias, x, y;

	bias=Levels.Dynamic.Bias.fastGet();
	x=bias+(Levels.Range.UnipolarMode.fastGet()?0.0f:Levels.Range.FullScale.fastGet());
	y=Levels.Range.FullScale.fastGet()-bias;
	if(x<y)
		y=x;
	x=Levels.Dynamic.Amplitude.fastGet();
	if(x>y)
	{
		x=y;
		Levels.Dynamic.Amplitude=x;
	}
	Levels.Dynamic.Peak=bias+x;
	Levels.Dynamic.Trough=bias-x;
}

void Generator::newPeak()
{
	if(Levels.Dynamic.Peak()<Levels.Dynamic.Trough())
		Levels.Dynamic.Trough=Levels.Dynamic.Peak();
	newPeakTrough();
//	Poke(smpbs->test3, 1.0f);
}

void Generator::newTrough()
{
	if(Levels.Dynamic.Trough()>Levels.Dynamic.Peak())
		Levels.Dynamic.Peak=Levels.Dynamic.Trough();
	newPeakTrough();
//	Poke(smpbs->test4, 1.0f);
}

void Generator::newPeakTrough()
{
	Float ampl;

	Levels.Dynamic.Bias=(0.5f*(Levels.Dynamic.Peak()+Levels.Dynamic.Trough()));
	ampl=0.5f*(Levels.Dynamic.Peak()-Levels.Dynamic.Trough());
	if(ampl<0.0f)
		ampl=-ampl;
	Levels.Dynamic.Amplitude=ampl;
//	Poke(smpbs->test6, ampl);
//	Poke(smpbs->test7, Levels.Dynamic.Peak());
//	Poke(smpbs->test8, Levels.Dynamic.Trough());
//	Poke(smpbs->test5, 1.0f); //for testing.
}

void Generator::newDwellStatus()
{
	ElementTypes.Dwell.Status=((ElementTypes.Dwell.Elapsed()<ElementTypes.Dwell.Time())?GenInProgress:GenComplete);
}

void Generator::newDwellProgress()
{
	float x;

	if(ElementTypes.Dwell.Time()==0.0f)
		x=0.0f;
	else
		x=100.0f*ElementTypes.Dwell.Elapsed()/ElementTypes.Dwell.Time();
	if(x>100.0f)
		x=100.0f;
	ElementTypes.Dwell.Progress=x;
}

void Generator::newTrans()
{
	Levels.Static.Origin=Levels.Static.Bias();
	ElementTypes.Transition.Elapsed=0.0f;
//	Poke(smpbs->newTransCalc, 1);
	if(ElementTypes.Transition.ConstantRate())
		newTransRate();
	else
		newTransTime();
}

void Generator::forceTrans()
{
	ElementTypes.Transition.Elapsed=(1.0f/Peek(smpbs->transFac));
	ElementTypes.Transition.Time=(1.0f/Peek(smpbs->transFac));
}

void Generator::newTransTime()
{
	#ifndef SIGNALCUBE //the signal cube doesn't like peeking into dfade.
	//if(!Peek(smpbs->transRateEn)){ //if rate adaptation is not enabled...
	/*if( (!ElementTypes.Transition.RateAdaptation.Enable()) && !(ElementTypes.Transition.ConstantRate()) ){
	//if(0){
		float x;
		float dfadeTmp = Peek(smpbs->dfade);
		float oldTime = 1.0f/Peek(smpbs->transFac);
		float newTime = ElementTypes.Transition.Time();

		x=Levels.Static.Target()-Levels.Static.Origin();
		if(x<0.0f)
			x=-x;
		if(newTime==0.0f)
		{
			ElementTypes.Transition.Rate=0.0f;
			Poke(smpbs->transFac, timeToFac(0.000000000001f));
		}
		else
		{
			ElementTypes.Transition.Rate=(x/newTime);
			Poke(smpbs->transFac, timeToFac(newTime));
		}
		dfadeTmp *= (newTime/oldTime); //work out where we are now (this will hopfully stop glitches when changing rate half way through a transition).
		Poke(smpbs->dfade, dfadeTmp);
	}*/
	if( (!ElementTypes.Transition.RateAdaptation.Enable()) && !(ElementTypes.Transition.ConstantRate()) ){
		float x;
		float dfadeTmp;
		float oldTime;
		float newTime = ElementTypes.Transition.Time(); // The new time.
		x=Levels.Static.Target()-Levels.Static.Origin(); //distance.
		if(x<0.0f)
			x=-x;
		//important stuff
		oldTime = 1.0f/Peek(smpbs->transFac); // The previous time.
		dfadeTmp = Peek(smpbs->dfade); // How far through we are.
		if(newTime==0.0f)
		{
			newTime = 0.000000000001f;
		}
		dfadeTmp *= (newTime/oldTime); //work out where we are now.
		Poke(smpbs->transFac, timeToFac(newTime));
		Poke(smpbs->dfade, dfadeTmp);
		//end important stuff
		ElementTypes.Transition.Rate=(x/newTime);
	}
	#endif
}

void Generator::newTransRate()
{
	#ifndef SIGNALCUBE
	if( (!ElementTypes.Transition.RateAdaptation.Enable()) && (ElementTypes.Transition.ConstantRate()) ){ //if rate adaptation is not enabled...
		float x;
		//float transFacTmp = Peek(smpbs->transFac);
		float dfadeTmp;
		float oldTime = Peek(smpbs->transTime);
		float newRate = ElementTypes.Transition.Rate();
		if(newRate==0.0f)
		{
			newRate = 0.000000001f;
		}
		//important stuff
		x=Levels.Static.Target()-Levels.Static.Origin(); //distance.
		if(x<0.0f)
			x=-x;
		x/=newRate;//time
		if(x>MAXTIME)
			x=MAXTIME;
		if(x<=0){
			x=0.000000001f;
		}
		dfadeTmp = Peek(smpbs->dfade);
		dfadeTmp *= (x/oldTime); //work out where we are now.
		Poke(smpbs->transFac, timeToFac(x)); //send rate over
		Poke(smpbs->dfade, dfadeTmp);
		//end important stuff
		ElementTypes.Transition.Time=x;
		//set delta,(fade?)
	}
	else
	{

	}
	#endif
}

void Generator::newTransStatus()
{
	ElementTypes.Transition.Status=((ElementTypes.Transition.Elapsed()<ElementTypes.Transition.Time())?GenInProgress:GenComplete);
}

void Generator::newTransProgress()
{
	float x;

	if(ElementTypes.Transition.Time()==0.0f)
		x=0.0f;
	else
		x=100.0f*ElementTypes.Transition.Elapsed()/ElementTypes.Transition.Time();
	if(x>100.0f)
		x=100.0f;
	ElementTypes.Transition.Progress=x;
}

inline void PokePoly(float to[4], float from[4])
{
	Poke(to[0], from[0]);
	Poke(to[1], from[1]);
	Poke(to[2], from[2]);
	Poke(to[3], from[3]);
}

void Generator::initStdProfiles()
{
	int i;
	float x, y, y0, y1, d0, d1, buf[4];

	// Linear
	x=1.0f/((float)GenProfSz);
	buf[0]=0.0f;
	buf[1]=x;
	buf[2]=0.0f;
	buf[3]=0.0f;
	for(i=0; i<GenProfSz; i++)
	{
		PokePoly(smpbs->linear[i], buf);
		buf[0]+=x;
	}
	buf[0]=1.0f;
	buf[1]=0.0f;
	buf[2]=0.0f;
	buf[3]=0.0f;
	PokePoly(smpbs->linear[GenProfSz], buf);
	/*
	x = 1/32
	\  	0	1	2	3	4	...	32
	 \________________________________
	0|	0	x	2x	3x	4x	...	1
	1|	x	x	x	x	x	... x
	2|	0	0	0	0	0	...	0
	3|	0	0	0	0	0	...	0
	*/

	// Haversine
	y1=d1=0.0f;
	x=(float)Pi/(float)GenProfSz; // =Pi/32~=0.09817
	y=0.5f*x;
	for(i=0; i<GenProfSz; i++)
	{
		y0=y1;
		d0=d1;
		d1=x*(float)(i+1);
		y1=0.5f*(1.0f-cosf(d1));
		d1=y*sinf(d1);
		SplinePoly(buf, y0, y1, d0, d1);
		PokePoly(smpbs->haversine[i], buf);
	}
	buf[0]=1.0f;
	buf[1]=0.0f;
	buf[2]=0.0f;
	buf[3]=0.0f;
	PokePoly(smpbs->haversine[GenProfSz], buf);
	/*

	void SplinePoly(float p[4], float y0, float y1, float d0, float d1)
	{
		p[0]=y0;
		p[1]=d0;
		p[2]=3.0f*(y1-y0)-2.0f*d0-d1;
		p[3]=2.0f*(y0-y1)+d0+d1;
	}

	x = pi/32
	\  	0	1	2	3	4	...	32
	 \________________________________
	0|
	1|
	2|
	3|
	*/
}

void Generator::newTransProfile()
{
	ProfileElement *p;

	switch(ElementTypes.Transition.Profile())
	{
	case GenHaversine:
		p=smpbs->haversine;
		//Poke(smpbs->profileTP, (int)0);
		break;
	case GenCustomLinear:
		p=smpbs->customLinear;
		//Poke(smpbs->profileTP, (int)0);
		break;
	case GenCustomSpline:
		p=smpbs->customSpline;
		//Poke(smpbs->profileTP, (int)0);
		break;
	case Gensquare:
		//Poke(smpbs->profileTP, (int)1);
		break;
	default:
		p=smpbs->linear;
		//Poke(smpbs->profileTP, (int)0);
	}
	Poke(*(float **)Smp(profile), (float *)p);
}

void Generator::newCustomProfile()
{
	int i;
	float y0, y1, d0, d1, buf[4];

	// Custom linear
	buf[0]=0.0f;
	buf[1]=(*ElementTypes.Transition.CustomProfile.member[0])();
	buf[2]=0.0f;
	buf[3]=0.0f;
	PokePoly(smpbs->customLinear[0], buf);
	for(i=1; i<GenProfSz; i++)
	{
		buf[0]=(*ElementTypes.Transition.CustomProfile.member[i-1])();
		buf[1]=(*ElementTypes.Transition.CustomProfile.member[i])()
				-(*ElementTypes.Transition.CustomProfile.member[i-1])();
		PokePoly(smpbs->customLinear[i], buf);
	}
	buf[0]=1.0f;
	buf[1]=0.0f;
	buf[2]=0.0f;
	buf[3]=0.0f;
	PokePoly(smpbs->customLinear[GenProfSz], buf);

	// Custom spline
	y0=0.0f;
	y1=(*ElementTypes.Transition.CustomProfile.member[0])();
	d0=0.0f;
	d1=0.5f*(*ElementTypes.Transition.CustomProfile.member[1])();
	SplinePoly(buf, y0, y1, d0, d1);
	PokePoly(smpbs->customSpline[0], buf);
	for(i=1; i<(GenProfSz-1); i++)
	{
		y0=y1;
		y1=(*ElementTypes.Transition.CustomProfile.member[i])();
		d0=d1;
		d1=0.5f*((*ElementTypes.Transition.CustomProfile.member[i+1])()
				-(*ElementTypes.Transition.CustomProfile.member[i-1])());
		SplinePoly(buf, y0, y1, d0, d1);
		PokePoly(smpbs->customSpline[i], buf);
	}
	y0=y1;
	y1=(*ElementTypes.Transition.CustomProfile.member[GenProfSz-1])();
	d0=d1;
	d1=0.0f;
	SplinePoly(buf, y0, y1, d0, d1);
	PokePoly(smpbs->customSpline[GenProfSz-1], buf);
	buf[0]=1.0f;
	buf[1]=0.0f;
	buf[2]=0.0f;
	buf[3]=0.0f;
	PokePoly(smpbs->customSpline[GenProfSz], buf);
}

void Generator::newNextFrequency(){Poke(smpbs->nextPhaseInc, frequencyToInc(MasterBuffer.CyclicFrequency()));}

void Generator::newFCyclsCmplt()
{
	Poke(smpbs->fCyclsCmplt, (float)ElementTypes.Cyclic.CyclesComplete()+phaseToCycle(ElementTypes.Cyclic.Phase()));
}

void Generator::newCyclicStatus()
{
	ElementTypes.Cyclic.Status=((ElementTypes.Cyclic.CyclesComplete()<ElementTypes.Cyclic.CyclesRequired())?GenInProgress:GenComplete);
}

void Generator::newCyclic()
{
	ElementTypes.Cyclic.CyclesComplete=0L;
	ElementTypes.Cyclic.Phase=0.0f;
	Poke(smpbs->fCyclsCmplt, 0.0f);
}

void Generator::forceCyclic()
{
	ElementTypes.Cyclic.CyclesComplete=ElementTypes.Cyclic.CyclesRequired();
	ElementTypes.Cyclic.Phase=0.0f;
	Poke(smpbs->fCyclsCmplt, (float)ElementTypes.Cyclic.CyclesRequired());
}


void Generator::newCyclicProgress()
{
	float x;

	x=(float)ElementTypes.Cyclic.CyclesRequired();
	if(x<1.0f)
		x=0.0f;
	else
	{
		x=100.0f*Peek(smpbs->fCyclsCmplt)/x;
		if(x>100.0f)
			x=100.0f;
	}
	ElementTypes.Cyclic.Progress=x;
}

void Generator::newHarmonic(){Poke(smpbs->harmonic, (float)(ElementTypes.Cyclic.Harmonic()));}

void Generator::newWaveTable()
{
	int i;
	Uint16 maxharm;

	if(ElementTypes.Cyclic.AntiAlias()==GenEnabled)
		maxharm=(Uint16)floor(0.5f*SysFs/(ElementTypes.Cyclic.Frequency()));
	else
		maxharm=(Uint16)(GenWTSz/2);
	StdWaveSyn(wvTbl, GenWTSz, (Choice)(SWSine+ElementTypes.Cyclic.WaveShape()), maxharm, False, 0,
		((float)Pi)/(2.0f*(1.0f+ElementTypes.Cyclic.TrapezoidRatio())));
	for(i=0; i<(GenWTSz-1); i++)
	{
		Poke(smpbs->wvTbl[i][0], wvTbl[i]);
		Poke(smpbs->wvTbl[i][1], wvTbl[i+1]-wvTbl[i]);
	}
	Poke(smpbs->wvTbl[GenWTSz-1][0], wvTbl[GenWTSz-1]);
	Poke(smpbs->wvTbl[GenWTSz-1][1], wvTbl[0]-wvTbl[GenWTSz-1]);
}

void Generator::newSweepTime(){Poke(smpbs->sweepFac, timeToFac(ElementTypes.Sweep.Time()));}

void Generator::newSweepStatus()
{
	ElementTypes.Sweep.Status=((ElementTypes.Sweep.Elapsed()<ElementTypes.Sweep.Time())?GenInProgress:GenComplete);
}

void Generator::newSweepProgress()
{
	float x;

	if(ElementTypes.Sweep.Time()==0.0f)
		x=0.0f;
	else
		x=100.0f*ElementTypes.Sweep.Elapsed()/ElementTypes.Sweep.Time();
	if(x>100.0f)
		x=100.0f;
	ElementTypes.Sweep.Progress=x;
}

void Generator::newSweepFrequency()
{
	if(ElementTypes.Sweep.LogMode())
		ElementTypes.Sweep.Frequency=ElementTypes.Sweep.StartFrequency()*powf(ElementTypes.Sweep.EndFrequency()/ElementTypes.Sweep.StartFrequency(), 0.01f*ElementTypes.Sweep.Progress());
	else
		ElementTypes.Sweep.Frequency=ElementTypes.Sweep.StartFrequency()+0.01f*ElementTypes.Sweep.Progress()*(ElementTypes.Sweep.EndFrequency()-ElementTypes.Sweep.StartFrequency());
}

void Generator::newStartFrequency()
{
	Poke(smpbs->startPhaseInc, frequencyToInc(ElementTypes.Sweep.StartFrequency()));
	initLogFreqTbl();
}

void Generator::newEndFrequency()
{
	Poke(smpbs->endPhaseInc, frequencyToInc(ElementTypes.Sweep.EndFrequency()));
	initLogFreqTbl();
}

void Generator::newExternalStatus()
{
	ElementTypes.External.Status=((ElementTypes.External.Elapsed()<ElementTypes.External.Time())?GenInProgress:GenComplete);
}

void Generator::newExternalProgress()
{
	float x;

	if(ElementTypes.External.Time()==0.0f)
		x=0.0f;
	else
		x=100.0f*ElementTypes.External.Elapsed()/ElementTypes.External.Time();
	if(x>100.0f)
		x=100.0f;
	ElementTypes.External.Progress=x;
}

float Generator::frequencyToInc(float f)
{
	float inc;

	inc=f/SysFs;
	if(inc<0.0f)
		inc=0.0f;
	if(inc>1.0f)
		inc=1.0f;
	return inc;
}

float Generator::timeToInc(float t)
{
	float inc;
	
	inc=t*SysFs;
	if(inc<=0.0f)
		inc=1.0f;
	else
	{
		inc=1.0f/inc;
		if(inc>1.0f)
			inc=1.0f;
	}
	return inc;
}

float Generator::phaseToCycle(float theta)
{
	float cyc;

	cyc=2.7777777777e-3f*theta;
	if(cyc<-1.0f)
		cyc=-1.0f;
	if(cyc>1.0f)
		cyc=1.0f;
	return cyc;
}

void Generator::setWavetableEntry(int i, float val)
{
	if ((i >= 0) && (i < GenWTSz))
	{
		wvTbl[i] = val;
	}
}

#endif

#ifdef SMPGEN

const SmpGenerator::assertTableType SmpGenerator::assertTable={
	0,									// GenStopped
	TSRStatPend,						// GenStartStatic
	TSRStatPend,						// GenPauseStatic
	TSRStatPend+TSRDynPend+TSRFuncPend,	// GenStartDynamic
	TSRStatPend+TSRDynPend+TSRFuncPend,	// GenRunDynamic
	TSRStatPend+TSRFuncPend,			// GenPauseDynamic
	TSRStatPend+TSRDynPend+TSRFuncPend,	// GenHoldDynamic
	TSRStatPend,						// GenStopStatic
	TSRStatPend+TSRDynPend+TSRFuncPend,	// GenStopDynamic
	TSRStatPend,						// GenFunctionComplete
	TSRStatPend							// GenFunctionCompleteAck
};

#endif
