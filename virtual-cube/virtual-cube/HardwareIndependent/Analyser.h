// Analyser.h - Analyser Class header

#define	AnalMaxCh		10
#define	AnalFunSz		10
#define	AnalDelBufSz	(AnalMaxCh*AnalMaxCh+AnalFunSz)
typedef char AnalChName[4];

extern float DSPF_sp_dotprod(const float *x, const float *y, const int n);
//extern float testData[1000][4];

class SmpAnalyser {
public:
	void iter()
	{
		register Sint32 indexA, indexB, i, j;
		Sint32 current, loading, overlap;
		float smp;

		if(!(pCNet&&asyncEn&&latchedEn))
		{
			latchedEn=syncEn;
			return;
		}
		current=bufPhase;
		loading=(current+1)%3;
		overlap=(loading+1)%3;
		indexA=count/nCh;
		indexB=count%nCh;
		for(i=0; i<nCh; i++)
		{
			j=count+AnalFunSz-1;
			smp=(float)(pCNet[cNetNo[i]]);
			delBuf[i][loading][j]=smp;
			if((j-=cycle)>=0)
				delBuf[i][overlap][j]=smp;

		}
/*
		for(i=0; i<nCh; i++)
		{
			j=count+AnalFunSz-1;
			if(dataIndex<1000)
				smp=testData[dataIndex][i];
			else
				smp=0.0f;
			delBuf[i][loading][j]=smp;
			if((j-=cycle)>=0)
				delBuf[i][overlap][j]=smp;

		}
		if(dataIndex<1000)
			dataIndex++;
*/
		if(indexA<nCh)
			for(i=0; i<=ord; i++)
				corFun[indexA][indexB][i]+=(double)DSPF_sp_dotprod(delBuf[indexA][current], (delBuf[indexB][current])+i, cycle);
		if((++count)>=cycle)
		{
			latchedEn=syncEn;
			count=0;
			bufPhase=(bufPhase+1)%3;
		}
	}
//	void saveCor();
	Sint32 count;
	Sint32 cycle;
	Sint32 asyncEn;
	Sint32 syncEn;
	Sint32 latchedEn;
	Sint32 nCh;
	Sint32 ord;
	Sint32 bufPhase;
	Uint32 cNetNo[AnalMaxCh];
	double corFun[AnalMaxCh][AnalMaxCh][AnalFunSz];
	float delBuf[AnalMaxCh][3][AnalDelBufSz];
	Uint32 *pCNet;
	Uint32 dataIndex;
};

#ifdef SUPGEN

class AnalChanArray: public Parameter {
public:
	UnsignedPar member[AnalMaxCh];
	AnalChName name[AnalMaxCh];
};

class Analyser: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpAnalyser &smp);
	IntegerPar Channels;
	IntegerPar Order;
	EnumPar Enable;
	IntegerPar Reset;
	IntegerPar Active;
//	UnsignedPar DataIndex;
//	IntegerPar Save;
	AnalChanArray CNetAddress;
	void reset();
	Uint32 *pCNet;
private:
	virtual void parSetEvent(Parameter *sce);
	virtual void parPrimeSignal();
	virtual void parAlteredEvent(Parameter *sce);
	virtual Result special(int fn, void *in, int incnt, void *out, int outcnt);
	SmpAnalyser *smpbs;
};

#endif
