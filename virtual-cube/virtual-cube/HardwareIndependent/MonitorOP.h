// Monitor.h - Monitor Class header

enum {MonNameSz=32,			// Size of name string
	  MonMaxServoCh=16,		// Maximum servo channels
	  MonMaxPoint=32		// Maximum number of points to monitor
};

// Sample processing channel record
class SmpMonChannel {
public:
	void iter()
	{
		output=((pInput?*pInput:0.0f)+offset)*scale;
		Clamp(output, -1.1f, 1.1f);
		if(cNetEn&&pCNet)
			pCNet[cNetSlot]=output;
	}
	float *pInput;
	float offset;
	float scale;
	float output;
	float *pCNet;
	Sint32 cNetEn;
	Sint32 cNetSlot;
};

class SmpMonitor {
public:
	void iter()
	{
		register SmpMonChannel *p;
		register int i;

		for(i=MonNCh, p=channel; i--; p++)
			p->iter();
	}
	SmpMonChannel channel[MonNCh];
};

#ifdef SUPGEN

class MonDACClass: public Parameter {
public:
	EnumPar    Enable;
	IntegerPar DACNumber;
};

class MonCNetClass: public Parameter {
public:
	EnumPar    Enable;
	IntegerPar TimeSlot;
};

class MonSettingsClass: public Parameter {
public:
	SignalRange  Range;
	MonDACClass  DAC;
	MonCNetClass CNet;
	FloatPar     Offset;
	FloatPar     Scale;
};

class MonChannel: public Parameter {
public:
	void operator()(Parameter *par, int nCh, int nTx, int ind, SmpMonChannel &smp, SmpSigInputChannel *pTx);
	TextPar          Name;
	EnumPar          Point;
	IntegerPar       Index;
	IntegerPar       Channel;
	FloatPar         Reading;
	MonSettingsClass Settings;
	float            *pCNet;
private:
	virtual void parAlteredEvent(Parameter *sce);
	virtual void parRangeChangedEvent(Parameter *sce);
	virtual void parPrimeSignal();
	void newMonitorPoint();
	void newRange();
	SmpMonChannel *smpbs;
	char myName[4];
	char *(pointArray[MonMaxPoint+1]);
	int endEPPoints, nTxs;
	SmpSigInputChannel *pTxs;
	SmpErrorPath **ppServoChannel;
};

class MonitorArray: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpMonitor &smp, int nCh, int nTx, SmpSigInputChannel *pTx);
	MonChannel Channel[MonNCh];
	static SmpErrorPath **IContext;
	SmpErrorPath *(pServoChannel[MonMaxServoCh]);
private:
	SmpMonitor *smpbs;
};

#endif
