// Generator.h - Function Generator Class header

//*((float *)0x70) = (float to send);
#ifdef GEN_SECOND_CALL

#define	GenProfSz	32					// Minimum two!
#define	GenWTSz		256
#define	GenNudgeSz	0.01f				// 1% sweep nudges
#define	GenTimeInc	(1.0/(double)SysFs)
#define	GenMaxFade	1.0e10f				// 0.1msec even at 1e-6Hz

#define	On(a)		if(a?(a=0,1):0)		// Test and clear primitive

typedef float ProfileElement[4];

#ifdef KINETUSB
#ifdef PCTEST
extern Sint16 kinet[128];
#endif
#endif

#ifndef PCTEST
	extern int* tp_q_head; //this is for TP queue
	extern int* tp_q_tail; // "
	extern int event_queue[];

extern "C" {
	int tp_add_to_queue(u32_t CnetTsLo, u32_t CnetTsHi, u32_t tpCount, u32_t tpError);
	uint32_t _ftoi(float f);
	float _itof(uint32_t i);
}
#endif

// Peak/trough adaptation port
typedef struct {
	// note: amplitudeAdjust to peakCtrlCnt will get reset every sample period.
	//			peak, trough and so on will not.
	// This happens in signal.h ( function memset() )
	float amplitudeAdjust;
	float medianAdjust;
	float phaseAdjust;
	float originAdjust;
	float targetAdjust;
	Uint32 cyclesComplete;
	Uint32 peakCtrlCnt; //this is used to let the generator know that there is some peak control happening.
	float peakDes;	//from signal.h
	float troughDes;
	float peak;
	float trough;
	//int rateAdaptEn;
	float start;
	float end;
	float feedback;
} GenPTAPort;

typedef struct {
	unsigned long int TPcount;
	float error;
} TPerr;

enum {GenSingleChannel=0,	// GenMode values
	  GenMultiChannel,
	  GenNotStarted=0,		// ChStatus values
	  GenStarted,
	  GenGenerating,
	  GenAtFunctionComplete,
	  GenNone=0,			// ElementType values
	  GenDwell,
	  GenTransition,
	  GenCyclic,
	  GenSweep,
	  GenExternal,
	  GenStopped=0,			// RunStatus values
	  GenStartStatic,
	  GenPauseStatic,
	  GenStartDynamic,
	  GenRunDynamic,
	  GenPauseDynamic,
	  GenHoldDynamic,
	  GenStopStatic,
	  GenStopDynamic,
	  GenFunctionComplete,
	  GenFunctionCompleteAck,
	  GenComplete=0,		// GenStatus values
	  GenInProgress,
	  GenTime=0,			// TransMode values
	  GenRate,
	  GenLinear=0,			// TransProfile values
	  GenHaversine,
	  GenCustomLinear,
	  GenCustomSpline,
	  Gensquare,
	  GenAmplBias=0,		// CyclicMode values
	  GenPeakTrough,
	  GenSine=0,			// WaveShape values
	  GenSquare,
	  GenTriangle,
	  GenSawtooth,
	  GenTrapezoid,
	  GenCustom,
	  GenDisabled=0,		// Able values
	  GenEnabled,
	  GenAtBaseLine=0,		// GenBus instructions
	  GenStaticFade,
	  GenAtBias,
	  GenDwelling,
	  GenTransitioning,
	  GenCycling,
	  GenExtCmd,
	  GenDirectCyclicStart,
	  GenLoadTransBuf,
	  GenLoadCyclicBuf,
	  GenLoadDwellBuf,
      GenAtBiasFC,
	  GenAtBiasBaselineTrack,
	  GenDirectCyclicStop,
	  GenTransContStart,
	  GenSync     =0x00010000,	// Generator sync flag
	  GenHoldState=0x00020000,	// Generator hold state
	  GenXcodeMask=0x0001FFFF,	// Mask to extract xcode and GenSync fields from GenGenBus.xcode
	  GenIntSC=0,			// Integration values
	  GenIntMCSlave,
	  GenIntMCMaster,
	  GenLogFreqTableSz=32	// Log frequency table for sweep
};

#define	MINTIME		0.0f
#define	MAXTIME		1.0e9f
#define	MINFREQ		1.0e-6f
#define	MAXFREQ		400.0f

class SmpGenerator {
public:
	void iter(int chanNo)
	{
		static int old_start, old_stop, old_runStatus, old_xcode, old_state;
		Uint32 xcode, acqpls, tmp;
		float fade, theta, y;
#if 0
		double tmpd;	// temporary double for asin/acos functions
#endif
		float x, z, *p;
		Sint32 provChStatus;

		// TSR responses
		tsrIn=TSRGenSync;
		if(tsrResp&&pTsrIn)
		{
			//TSRStopDyn+TSRStopWR+TSRModeChStop
			tsrIn=*pTsrIn;
			stop|=((tsrIn&TSRStopStat)||((tsrIn&TSRStopWR)&&!(tsrIn&TSRDynPend)));
			pause|=(tsrIn&TSRStopDyn);
			hold|=(tsrIn&TSRFreeze);
#if 0		/* The following code is only suitable for use on single channel systems because
				it uses the local feedback to calculate the required value for dfade after
				a stop/pause/hold operation.

				Removed to prevent problems when run on omulti-channel systems.
			*/
			if(stop||pause||hold){
			}
			if (hold && pFeedback && !holdState) {
				if( ((state == 1)||(state==3)) && ((bias-baseLine)!=0.0f) ){ //start/stop ramp
					dfade = ((*pFeedback)-baseLine)/(bias-baseLine); //work out new dfade.
					if (dfade>1.0f){// limit dfade
						dfade = 1.0f;
					}
					else if (dfade < 0.0f){
						dfade = 0.0f;
					}
					genBusOut.fade=dfade;// update genBusOut.fade
				}
				else if((state == 7) && ((target - origin)!=0.0f)){ // Turning point
					/*  Possible profileTP options
					GenLinear
					GenHaversine
					GenCustomLinear
					GenCustomSpline
					Gensquare
					*/
					if(profileTP==GenHaversine){
						// -- make sure acos() isn't given something outside of the range [-1,+1]
						// -- defined behaviour of the TI math library is to *silently* ignore out of range
						// -- values to acos() and other functions by internally clamping them.  Make the
						// -- simulator do the same.  See SPRU187L p.9-23.
						tmpd = (((((*pFeedback) - origin) / (target - origin))) - 0.5) * -2.0f;
#ifdef PCTEST
						Clamp(tmpd, -1.0f, 1.0f);
#endif
						dfade = (acos(tmpd)/(float)Pi)/transFac;
					}
					else if(profileTP==GenLinear){
						dfade = (((*pFeedback)-origin)/(target - origin)/transFac);
					}
					//if square or custom, then do nothing.
					
					if(dfade>(1.0f/transFac)) {
						dfade=1.0f/transFac;}
					else if(dfade<0.0f) {
						dfade=0.0f;}
					transElapsed=(float)dfade;
					genBusOut.fade=transFac*transElapsed;
				}
				else if (state == 14){ //cyclic running
					/*	Possible waveShape options
					GenSine
					GenSquare
					GenTriangle
					GenSawtooth
					GenTrapezoid
					*/
					//If peak control is on, then reset amplitudes and carry on.
					if((pPTAPort -> peakCtrlCnt) > 0){
						amplitude = ((pPTAPort -> peakDes) - (pPTAPort -> troughDes))/2.0f;
						cycBias = ((pPTAPort -> peakDes) + (pPTAPort -> troughDes))/2.0f;
					}
					if(antiAlias || (waveShape==GenSine)){ // if sine wave
						//use asin to work out phase
						// -- make sure asin() isn't given something outside of the range [-1,+1]
						// -- defined behaviour of the TI math library is to *silently* ignore out of range
						// -- values to asin() and other functions by internally clamping them.  Make the
						// -- simulator do the same.  See SPRU187L p.9-23.
						tmpd = ((*pFeedback) - cycBias) / (amplitude);
#ifdef PCTEST
						Clamp(tmpd, -1.0f, 1.0f);
#endif
						x = asin(tmpd)/(2.0f*(float)Pi);
						if(x>=0.0f){ //if section 1
							if((0.5-x)<dtheta) x=(0.5-x);
						}
						else {
							x=0.5-x;
							if((1.5-x)<dtheta) x=(1.5-x); // == 0.5 + (1-x)
							else if (dtheta < 0.5f) {
								x=(1.5-x); //set as far as possible
								cyclsCmplt-=1L;} // decrement cycle count
						}
						dtheta = x;
						genBusOut.theta=phase=(float)dtheta;
					}
					else if(waveShape == GenTriangle){
						x = 0.25f*(((*pFeedback) - cycBias)/(amplitude));
						if(x>=0.0f){ //if section 1
							if((0.5-x)<dtheta) x=(0.5-x);
						}
						else {
							x=0.5-x;
							if((1.5-x)<dtheta) x=(1.5-x); // == 0.5 + (1-x)
							else if (dtheta < 0.5f) {
								x=(1.5-x); //set as far as possible
								cyclsCmplt-=1L;} // decrement cycle count
						}
						dtheta = x;
						genBusOut.theta=phase=(float)dtheta;
					}
					else if(waveShape == GenTrapezoid){
						x = 0.25f*(((*pFeedback) - cycBias)/(amplitude))/(trapRat+1.0f);
						if(x>=0.0f){ //if section 1
							if((0.5-x)<dtheta) x=(0.5-x);
						}
						else {
							x=0.5-x;
							if((1.5-x)<dtheta) x=(1.5-x); // == 0.5 + (1-x)
							else if (dtheta < 0.5f) {
								x=(1.5-x); //set as far as possible
								cyclsCmplt-=1L;} // decrement cycle count
						}
						dtheta = x;
						genBusOut.theta=phase=(float)dtheta;
					}
					else if(waveShape == GenSawtooth){
						x=0.5f*(((*pFeedback) - cycBias)/(amplitude));
						if(x<0.0f){
							x+=1.0f;
							if(dtheta < 0.5f) cyclsCmplt-=1L;
						}
						dtheta = x;
						genBusOut.theta=phase=(float)dtheta;
					}
					
				}
			}
#endif
			if ((tsrIn & TSRStopWR) && (runStatus == GenPauseDynamic))
			{
				//stop=1;
			}
			if(TPDataTaken&&!(tsrIn&TSRTPDAV))
				TPDataTaken=0;
		}

		// Baseline selection
		xcode=pModeNo?*pModeNo:0;
		if(xcode>=EPNMode)
			xcode=0;
		baseLine=modes[xcode];

		
		/* Check for a request to move from GenFunctionComplete to FunctionCompleteAck 
		   state. If we're not currently in the GenFunctionComplete state, then
		   ignore the request, but don't clear the flag.
		*/
		if (chanNo == 0)
		{
			if (old_runStatus != runStatus)
			{
				printf("runStatus=%d\n", runStatus);
				old_runStatus = runStatus;
			}
			if (old_start != start)
			{
				printf("start=%d\n", start);
				old_start = start;
			}
			if (old_stop != stop)
			{
				printf("stop=%d\n", stop);
				old_stop = stop;
			}
			if (old_state != state)
			{
				printf("move from state %d > %d\n", old_state, state);
				old_state = state;
			}
		}


		if (ackComplete && (runStatus == GenFunctionComplete))
		{
			ackComplete = 0;
			enterFunctionCompleteAck();
		}

		switch(state)
		{
		// Starting and stopping stuff
		case 0: // Stopped
			On(start)
			{
				ackComplete = 0;
				genBusOut.xcode=GenStaticFade;
				genBusOut.fade=0.0f;
				dfade=0.0;
				runStatus=GenStartStatic;
				stop=pause=hold=0;
				state=1;
			}
			break;
		case 1: // Start ramp
			On(stop)
			{
				runStatus=GenStopStatic;
				state=3;
				break;
			}
			On(pause)
			{
				runStatus=GenPauseStatic;
				start=0;
				state=2;
				break;
			}
			On(hold)
			{
				runStatus=GenPauseStatic;
				start=0;
				state=2;
				break;
			}
			dfade+=(double)startRampInc;
			if(dfade>=1.0)
			{
				dfade=1.0;
				  enterTrans()
				||enterDwell()
				||enterCyclic()
				||enterSweep()
				||enterExternal()
				||enterFunctionComplete();
			}
			genBusOut.fade=(float)dfade;
			break;
		case 2: // Pause on start ramp
			On(stop)
			{
				runStatus=GenStopStatic;
				state=3;
				break;
			}
			On(start)
			{
				ackComplete = 0;
				runStatus=GenStartStatic;
				pause=hold=0;
				state=1;
			}
			break;
		case 3: // Stop ramp
			dfade-=(double)stopRampInc;
			if(dfade<=0.0)
			{
				dfade=0.0;
				genBusOut.xcode=GenAtBaseLine;
				runStatus=GenStopped;
				start=0;		// SJE - why do we do this?  This cancels a pending start....
				state=0;
			}
			genBusOut.fade=(float)dfade;
			break;
		case 32:
			genBusOut.fade=1.0f;
			dfade=1.0;
			genBusOut.xcode=GenStaticFade;
			state=3;
			break;

		// Function complete stuff
		case 4:
			On(stop)
			{
				enterStop();
				break;
			}
			if(nextType)
			{
				if(integration)
				{
					genBusOut.xcode=GenSync | nextType;
					state=30;
				}
				else if(nextType==slaveNextType  || slaveNextType == 4)
					switch(nextType)
					{
					case 1:
						transTime=nextTransTime;
						transFac=(transTime>0.0f)?1.0f/transTime:0.0f;
						//*****trying to explain*****
						// delta is the percentage increase (in dec) per sample
						// fade is the percentage through (in dec)
						//***************************
						transElapsed=0.0f;
						if (slaveNextType != 4)
							genBusOut.xcode = GenLoadTransBuf;
						else
							slaveNextType = 0;
						state=31;
						break;
					case 2:
						dwellTime=nextDwellTime;
						dwellElapsed=0.0f;
						if (slaveNextType != 4)
							genBusOut.xcode = GenLoadDwellBuf;
						else
							slaveNextType = 0;
						state = 31;
						break;
					case 3:
						phaseInc=nextPhaseInc;
						cyclsRqrd=nextCycles;
						cyclsCmplt=0L;
						phase=0.0f;
						fCyclsCmplt=0.0f;
						if (slaveNextType != 4)
							genBusOut.xcode = GenLoadCyclicBuf;
						else
							slaveNextType = 0;
						state = 31;
						break;
					}
				break;
			}
			if(stopAtEnd&&!(tsrIn&TSRFuncPend))
			{
				enterStop();
				break;
			}
			On(start)
			{
				ackComplete = 0;
				  enterTrans()
				||enterDwell()
				||enterCyclic()
				||enterSweep()
				||enterExternal();
			}
			break;
		case 23:
			enterFunctionComplete();
			break;
		case 30:
			On(stop)
			{
				enterStop();
				break;
			}
			if(!(tsrIn&TSRGenSync))
			{
				switch(nextType)
				{
				case 1:
					transTime=nextTransTime;
					transFac=(transTime>0.0f)?1.0f/transTime:0.0f;
					transElapsed=0.0f;
					if (slaveNextType != 4)
						genBusOut.xcode = GenLoadTransBuf;
					else
						slaveNextType = 0;
					state = 31;
					break;
				case 2:
					dwellTime=nextDwellTime;
					dwellElapsed=0.0f;
					if (slaveNextType != 4)
						genBusOut.xcode = GenLoadDwellBuf;
					else
						slaveNextType = 0;
					state = 31;
					break;
				case 3:
					phaseInc=nextPhaseInc;
					cyclsRqrd=nextCycles;
					cyclsCmplt=0L;
					phase=0.0f;
					fCyclsCmplt=0.0f;
					if (slaveNextType != 4)
						genBusOut.xcode = GenLoadCyclicBuf;
					else
						slaveNextType = 0;
					state = 31;
					break;
				}
			}
			break;
		case 31:
			On(stop)
			{
				enterStop();
				break;
			}
			switch(nextType)
			{
			case 1:
				if(enterTrans()||!crun){ // If enterTrans does something or if in continuous mode..
					nextType=0;			 // .. then set nextType to 0. If not then try again. 18/07/16
					}
				break;
			case 2:
				enterDwell();
				nextType=0;
				break;
			case 3:
				enterCyclic();
				nextType=0;
				break;
			}
			break;

		// Dwell stuff
		case 5: // Running
			On(stop)
			{
				enterStop();
				break;
			}
			On(pause)
			{
				genBusOut.xcode=GenAtBias;
				runStatus=GenPauseDynamic;
				start=0;
				state=6;
				break;
			}
			On(hold)
			{
				genBusOut.xcode=GenAtBias;
				runStatus=GenPauseDynamic;
				start=0;
				state=6;
				break;
			}
			dfade+=GenTimeInc;
			dwellElapsed=(float)dfade;
			if(dwellElapsed>=dwellTime)
			{
				dwellElapsed=dwellTime;
				  enterCyclic()
				||enterSweep()
				||enterExternal()
				||enterFunctionComplete();
			}
			break;
		case 6: // Pause on dwell
			On(stop)
			{
				enterStop();
				break;
			}
			On(start)
			{
				ackComplete = 0;
				  enterDwell()
				||enterCyclic()
				||enterSweep()
				||enterExternal()
				||enterFunctionComplete();
			}
			if(dwellElapsed>=dwellTime)
				  enterCyclicPaused()
				||enterSweepPaused()
				||enterExternalPaused()
				||enterFunctionComplete();
			break;

		// Transition stuff
		case 7: // Running
			On(stop)
			{
				enterStop();
				break;
			}
			On(pause)
			{
				genBusOut.xcode=GenAtBias;
				runStatus=GenPauseDynamic;
				start=0;
				state=8;
				break;
			}
			On(hold)
			{
				//genBusOut.xcode=GenAtBias;
				runStatus=GenPauseDynamic;
				start=0;
				state=8;
				break;
			}
			
			dfade+=GenTimeInc;			

#if 1
			if(newTransCalc && !rateAdaptEn){//recalculate transition time/rate
				if(transRateEn){//rate
					if(transRate==0.0f)
					{
						transRate = 0.000000001f;
					}
					transTemp=target-origin; //distance.
					if(transTemp<0.0f)
						transTemp=-transTemp;
					transTemp/=(transRate/fullScale); //work out new time and scale.
					if(transTemp>MAXTIME)
						transTemp=MAXTIME;
					if(transTemp<=0){
						transTemp=0.000000001f;
					}
					dfade *= (transTemp*transFac); //work out where we are now.
					transFac = 1.0f/transTemp;
					transTime = transTemp;

				}
				else{//time
					transTemp=target-origin; //distance.
					if(transTemp<0.0f)
						transTemp=-transTemp;
					if(transTime==0.0f)
					{
						transTime = 0.000000000001f;
					}
					dfade *= (transTime*transFac); //work out where we are now.
					transFac = 1.0f/transTime;
					transRate=(fullScale*transTemp/transTime);
				}
				newTransCalc = 0;
			}
			genBusOut.delta=transFac*(float)GenTimeInc;
#endif
			transElapsed=(float)dfade;
#if 1
			//if((transElapsed>=transTime)&&(dwellTimeout1TP||dwellTimeout2TP)) //if we should wait untill feedback catches up..
			if(tpTimeoutEn&&(transitionHasCompleted()&&(dwellTimeout1TP||dwellTimeout2TP))) //if we should wait untill feedback catches up..
			{
				//transElapsed=transTime;
				transElapsed=(1.0f/transFac);
				dwellTimeTP += (float)GenTimeInc;

				//this works out the error
				transTemp = ((pPTAPort -> feedback) - (pPTAPort -> end)) * fullScale;
				if(transTemp < 0.0f){
					transTemp *= -1.0f;
				}
				//if( ((pPTAPort -> feedback) < ((pPTAPort -> end)+(dwellErrorTP)) ) && ( (pPTAPort -> feedback) > ((pPTAPort -> end)-(dwellErrorTP)) ) ) {//if error is small...
				if(transTemp < dwellErrorTP){
					dwellDebounceTimerTP += (float)GenTimeInc;// ...then inc debounce timer
					if(dwellDebounceTimerTP >= dwellDebounceTimeTP){//if debounce timer has run out...
						state=9;//...then we have succeded and should finish.
						dwellDebounceTimerTP = 0.0f;
					}
				}
				else { //if the error is large...
					dwellDebounceTimerTP = 0.0f;//..then reset debounce timer
				}

				if(dwellTimeTP>=dwellTimeout1TP){ //if passed timeout 1...
					if(dwellTimeTP>=(dwellTimeout2TP+dwellTimeout1TP)){ //if passed timeout 2 + timeout 1...
						//...then give up and do action
						dwellTrig = 1;

						if(pCnetTsLo)
							cnetTsLoDwell=*pCnetTsLo;
						else
							cnetTsLoDwell=1;
						if(pCnetTsHi)
							cnetTsHiDwell=*pCnetTsHi;
						if((pTsrOut)){
							(*pTsrOut) |= dwellActionTP;
						}
						if(dwellActionTP == 0){//if the action is nothing, then carry on...
							state=9;
						}
					}
					else{ //if passed timeout 1 but not giving up yet...
						//...then adjust output.
						target = target * (1.0f+(dwellGainTP/100.0f)*((pPTAPort -> end)-(pPTAPort -> feedback)));
					}
				}
			}
#endif
			//else if(transElapsed>=transTime){ //if finished and are not waiting for feedback...
			if(transitionHasCompleted()){ //if finished and are not waiting for feedback...
			//else if(transElapsed>=(1.0f/transFac)){ //if finished and are not waiting for feedback...
				//transElapsed=transTime;
				transElapsed=(1.0f/transFac);
				state=9;//then move on
			}
#if 1
			else{ //if not finished.
				dwellTimeTP = 0.0f;
			}
#endif
			/*if(transElapsed>=transTime){ //if finished...
				state=9;//then move on
			}*/
			genBusOut.fade=transFac*transElapsed;
			break;
		case 8: // Pause on transition
			genBusOut.xcode=GenAtBias;
			On(stop)
			{
				enterStop();
				break;
			}
			On(start)
			{
				ackComplete = 0;
				if(!transitionHasCompleted()) //if not finished..
				{
					genBusOut.xcode=GenTransitioning;
					state=7;
					pause=hold=0;
					runStatus=GenRunDynamic;
				}
				else //if finished..
				{
					  enterTrans()
					||enterDwell()
					||enterCyclic()
					||enterSweep()
					||enterExternal()
					||enterFunctionComplete();
				}
			}
			if(transitionHasCompleted()) //if finished..
			{
				printf("paused transition & complete\n");
				  enterDwellPaused()
				||enterCyclicPaused()
				||enterSweepPaused()
				||enterExternalPaused()
				||enterFunctionComplete();
			}
			break;
		case 9: // Follows single cycle with genBusOut.fade==1.0f before completion
			On(stop)
			{
				enterStop();
				break;
			}
			if(!(crun&&enterTrans()))
				  enterDwell()
				||enterCyclic()
				||enterSweep()
				||enterExternal()
				||enterFunctionComplete();
			break;
		case 34:
			transElapsed=0.0f;
			genBusOut.xcode=GenTransitioning;
			genBusOut.fade=0.0f;
			dfade=0.0;
			TPDataTaken=1;
			state=7;
			break;

		// Cyclic stuff
		case 10: // Cyclic fade in
			advanceCyclicPhase();
			On(stop)
			{
				runStatus=GenStopDynamic;
				state=11;
				break;
			}
			On(start)
			{
				ackComplete = 0;
				runStatus=GenStartDynamic;
				holdState=False;
			}
			On(hold)
			{
				runStatus=GenHoldDynamic;
				holdState=True;
			}
			On(pause)
			{
				runStatus=GenStopDynamic;
				state=12;
				break;
			}
			if(!holdState)
			{
				dfade+=(double)(CFIFac*phaseInc);
				if(dfade>=1.0)
				{
					dfade=1.0;
					genBusOut.fade=1.0f;
					runStatus=GenRunDynamic;
					state=14;
				}
				genBusOut.fade=(float)dfade;
			}
			break;
		case 11: // Cyclic fade out - stop
			advanceCyclicPhase();
			dfade-=(double)(CFOFac*phaseInc);
			if(dfade<=0.0)
			{
				dfade=0.0;
				enterStop();
			}
			genBusOut.fade=(float)dfade;
			break;
		case 12: // Cyclic fade out - pause
			advanceCyclicPhase();
			On(stop)
			{
				runStatus=GenStopDynamic;
				state=11;
				break;
			}
			On(start)
			{
				ackComplete = 0;
				runStatus=GenStartDynamic;
				holdState=False;
				pause=hold=0;
				state=10;
				break;
			}
//			On(hold)
//			{
//				runStatus=GenHeld;
//				holdState=True;
//			}
			dfade-=(double)(CFOFac*phaseInc);
			if(dfade<=0.0)
			{
				dfade=0.0;
				genBusOut.xcode=GenAtBias;
				runStatus=GenPauseDynamic;
				state=13;
			}
			genBusOut.fade=(float)dfade;
			break;
		case 13: // Paused
			On(stop)
			{
				enterStop();
				break;
			}
			On(start)
			{
				ackComplete = 0;
				  enterCyclic()
				||enterSweep()
				||enterExternal()
				||enterFunctionComplete();
			}
			//if(cyclsCmplt>=cyclsRqrd)
			//	enterSweepPaused()
			//	||enterExternalPaused()
			//	||enterFunctionComplete();
			break;
		case 14: // Running
			if(!holdState)
			{
				dtheta+=(double)phaseInc;
				if(dtheta>=1.0)
				{
					dtheta-=1.0;
					cyclsCmplt+=1L;
					if(cyclsCmplt>=cyclsRqrd)
					{
						cyclsCmplt=cyclsRqrd;			// Might overstep if ForceComplete used
						if(cycDirStt)
						{
							dtheta=0.0;
							genBusOut.xcode=GenDirectCyclicStop;
							state=33;
						}
						else
						{
							runStatus=GenStopDynamic;
							state=15;
						}
					}
				}										// NB. phase might be left ==1.0f here
				genBusOut.theta=phase=(float)dtheta;	// This is acceptable and indicates that
														//  the cycle is all but complete.
				if(	afcEn								// AFC
					&&(tsrResp?((tsrIn&TSRAFCCheck)==0):!intAFCCheck)
					&&(phaseInc<(MAXFREQ/SysFs)))
				{
					if(afcMode)
						phaseInc*=(1.0f+afcLogFac);
					else
						phaseInc+=afcLinInc;
					if (phaseInc>(MAXFREQ/SysFs))
						phaseInc=(MAXFREQ/SysFs);
				}
			}
			fCyclsCmplt=(float)cyclsCmplt+phase;
			On(stop)									// When restarting, a phase of 1.0f ensures
			{											//  that the cycles counter is advanced
				runStatus=GenStopDynamic;				//  immediately, which is correct.
				state=11;
				break;
			}
			On(start)
			{
				ackComplete = 0;
				runStatus=GenRunDynamic;
				holdState=False;
				break;
			}
			On(hold)
			{
				runStatus=GenHoldDynamic;
				holdState=True;
				break;
			}
			On(pause)
			{
				runStatus=GenStopDynamic;
				state=12;
				break;
			}
			break;
		case 15: // Cyclic fade out - cycles complete
			advanceCyclicPhase();
			On(stop)
			{
				runStatus=GenStopDynamic;
				state=11;
				break;
			}
			dfade-=(double)(CFOFac*phaseInc);
			if(dfade<=0.0)
			{
				dfade=0.0;
				On(pause)
				{
					runStatus = GenPauseDynamic;
					state = 13;
					break;
				}
				  enterSweep()
				||enterExternal()
				||enterFunctionComplete();
			}
			genBusOut.fade=(float)dfade;
			break;
		case 33: // Cyclic direct stop special cycle
			  enterSweep()
			||enterExternal()
			||enterFunctionComplete();
			break;

		// Sweep stuff
		case 16: // Sweep fade in
			advanceSweepPhase();
			On(stop)
			{
				runStatus=GenStopDynamic;
				state=17;
				break;
			}
			On(pause)
			{
				runStatus=GenStopDynamic;
				start=0;
				state=18;
				break;
			}
			dfade+=(double)(CFIFac*sweepPhaseInc);
			if(dfade>=1.0)
			{
				dfade=1.0;
				genBusOut.fade=1.0f;
				runStatus=GenRunDynamic;
				dfade=(double)sweepElapsed;
				start=hold=nudgeUp=nudgeDown=0;
				state=20;
			}
			genBusOut.fade=(float)dfade;
			break;
		case 17: // Sweep fade out - stop
			advanceSweepPhase();
			dfade-=(double)(CFOFac*sweepPhaseInc);
			if(dfade<=0.0)
			{
				dfade=0.0;
				enterStop();
			}
			genBusOut.fade=(float)dfade;
			break;
		case 18: // Sweep fade out - pause
			advanceSweepPhase();
			On(stop)
			{
				runStatus=GenStopDynamic;
				state=17;
				break;
			}
			On(start)
			{
				ackComplete = 0;
				runStatus=GenStartDynamic;
				pause=0;
				state=16;
				break;
			}
			dfade-=(double)(CFOFac*sweepPhaseInc);
			if(dfade<=0.0)
			{
				dfade=0.0;
				genBusOut.xcode=GenAtBias;
				runStatus=GenPauseDynamic;
				state=19;
			}
			genBusOut.fade=(float)dfade;
			break;
		case 19: // Paused
			On(stop)
			{
				enterStop();
				break;
			}
			On(start)
			{
				ackComplete = 0;
				  enterSweep()
				||enterExternal()
				||enterFunctionComplete();
			}
			if(sweepElapsed>=sweepTime)
				enterExternalPaused()
				||enterFunctionComplete();
			break;
		case 20: // Running NB. dfade no longer cyclic fade!
			advanceSweepPhase();
			On(stop)
			{
				dfade=1.0;
				runStatus=GenStopDynamic;
				state=17;
				break;
			}
			On(start)
			{
				ackComplete = 0;
				runStatus=GenRunDynamic;
				holdState=False;
			}
			On(hold)
			{
				runStatus=GenHoldDynamic;
				holdState=True;
			}
			On(pause)
			{
				dfade=1.0;
				runStatus=GenStopDynamic;
				state=18;
				break;
			}
			On(nudgeUp)
				if(holdState)
				{
					sweepElapsed+=GenNudgeSz*sweepTime;
					if(sweepElapsed>sweepTime)
						sweepElapsed=sweepTime;
				}
			On(nudgeDown)
				if(holdState)
				{
					sweepElapsed-=GenNudgeSz*sweepTime;
					if(sweepElapsed<0.0f)
						sweepElapsed=0.0f;
				}
			if(holdState)
				dfade=(double)sweepElapsed;
			else
			{
				dfade+=GenTimeInc;
				sweepElapsed=(float)dfade;
				if(sweepElapsed>=sweepTime)
				{
					sweepElapsed=sweepTime;
					dfade=1.0;
					runStatus=GenStopDynamic;
					state=21;
				}
			}
			break;
		case 21: // Sweep fade out - sweep complete
			dtheta+=(double)endPhaseInc;
			if(dtheta>=1.0)
				dtheta-=1.0;
			genBusOut.theta=(float)dtheta;
			dfade-=(double)(CFOFac*endPhaseInc);
			if(dfade<=0.0)
			{
				dfade=0.0;
				enterExternal()
				||enterFunctionComplete();
			}
			genBusOut.fade=(float)dfade;
			break;

		// Cyclic direct start
		case 22:
			genBusOut.xcode=GenCycling;
			state=14;
			break;

		// External stuff
		case 24: // External fade in
			On(stop)
			{
				runStatus=GenStopDynamic;
				state=25;
				break;
			}
			On(pause)
			{
				runStatus=GenStopDynamic;
				state=26;
				break;
			}
			On(hold)
			{
				runStatus=GenStopDynamic;
				state=26;
				break;
			}
			dfade+=(double)fadeInInc;
			if(dfade>=1.0)
			{
				dfade=(double)extElapsed;
				genBusOut.fade=1.0f;
				runStatus=GenRunDynamic;
				state=28;
			}
			else
				genBusOut.fade=(float)dfade;
			break;
		case 25: // External fade out - stop
			dfade-=(double)fadeOutInc;
			if(dfade<=0.0)
			{
				dfade=0.0;
				enterStop();
			}
			genBusOut.fade=(float)dfade;
			break;
		case 26: // External fade out - pause
			On(stop)
			{
				runStatus=GenStopDynamic;
				state=25;
				break;
			}
			On(start)
			{
				ackComplete = 0;
				runStatus=GenStartDynamic;
				pause=hold=0;
				state=24;
				break;
			}
			dfade-=(double)fadeOutInc;
			if(dfade<=0.0)
			{
				dfade=0.0;
				genBusOut.xcode=GenAtBias;
				runStatus=GenPauseDynamic;
				state=27;
			}
			genBusOut.fade=(float)dfade;
			break;
		case 27: // Paused
			On(stop)
			{
				enterStop();
				break;
			}
			On(start)
			{
				ackComplete = 0;
				enterExternal()
				||enterFunctionComplete();
			}
			if(extElapsed>=extTime)
				enterFunctionComplete();
			break;
		case 28: // Running
			On(stop)
			{
				runStatus=GenStopDynamic;
				dfade=1.0;
				state=25;
				break;
			}
			On(pause)
			{
				runStatus=GenStopDynamic;
				start=0;
				dfade=1.0;
				state=26;
				break;
			}
			On(hold)
			{
				runStatus=GenStopDynamic;
				start=0;
				dfade=1.0;
				state=26;
				break;
			}
			dfade+=GenTimeInc;
			extElapsed=(float)dfade;
			if(extElapsed>=extTime)
			{
				extElapsed=extTime;
				if(extIndef==0)
				{
					dfade=1.0;
					state=29;
				}
			}
			break;
		case 29: // External fade out - time complete
			On(stop)
			{
				runStatus=GenStopDynamic;
				state=25;
				break;
			}
			dfade-=(double)fadeOutInc;
			if(dfade<=0.0)
			{
				dfade=0.0;
				enterFunctionComplete();
			}
			genBusOut.fade=(float)dfade;
			break;
		}

		// Elapse timer
		if(runStatus==GenRunDynamic)
			if(elapseCount==((Uint32)SysFs-1))
			{
				elapseCount=0;
				elapsed++;
			}
			else
				elapseCount++;

		// Periodic acquisition

		acqpls=0;

		/* In the code below, xcode is used simply as a local temporary variable that is
		   not related to the normal use of xcode to indicate the generator status.
		*/

		xcode=(elementType==GenExternal)?(pPTAPort?pPTAPort->cyclesComplete:0L):cyclsCmplt;
		tmp=PADelayMode?elapsed:xcode;
		if(runStatus==GenRunDynamic)
		{
			if(PADelay>tmp)
				PADelayRem=PADelay-tmp;
			else
			{
				PADelayRem=0;
				tmp=PARepeatMode?elapsed:xcode;
				if(tmp>=PARepeatNext)
				{
					if(PARepeatNext==0)
						PARepeatNext=tmp;
					PARepeatNext+=(Uint32)(PACurrentFactor*(float)PARepeat);
					PACurrentFactor*=PALogLaw;
					PADurationEnd=PADuration+(PADurationMode?elapsed:xcode);
					acqpls=PAEn;
				}
			}
		}
		else
			if(tmp==0)
			{
				PARepeatNext=0;
				PACurrentFactor=1.0f;
				PADurationEnd=0;
			}
		tmp=PADurationMode?elapsed:xcode;
		PAAct=PAEn&&(tmp<PADurationEnd);

		// CNet monitors
		if(pCNet)
#ifdef PCTEST
		{
			if(cnetCCEnable)
				pCNet[cnetCCSlot]|=(elementType==GenExternal)?(pPTAPort?pPTAPort->cyclesComplete:0L):cyclsCmplt;
			if(cnetElapseEnable)
			{
				pCNet[cnetElapseSlot]|=elapsed;
				pCNet[cnetElapseSlot+1]|=elapseCount;
			}
		}
#else
		{
			if(cnetCCEnable)
				//pCNet[cnetCCSlot] = (elementType == GenExternal) ? (pPTAPort ? pPTAPort->cyclesComplete : 0L) : ((elementType == GenTransition) ? countTP : cyclsCmplt);
				pCNet[cnetCCSlot+256]=(elementType==GenExternal)?(pPTAPort?pPTAPort->cyclesComplete:0L):((elementType==GenTransition)?countTP:cyclsCmplt);
			if(cnetElapseEnable)
			{
				//pCNet[cnetElapseSlot]=elapsed;
				//pCNet[cnetElapseSlot+1]=elapseCount;
				pCNet[cnetElapseSlot+256] = elapsed;
				pCNet[cnetElapseSlot + 1 + 256] = elapseCount;
			}
		}
#endif

		/* Insert GenHoldState flag into genBusOut xcode word */

		genBusOut.xcode = (genBusOut.xcode & ~GenHoldState) | (holdState ? GenHoldState : 0);

		// Genbus output
		if((genBusOut.xcode & GenXcodeMask)==GenCycling)
			genBusOut.delta=phaseInc;
		if((genBusOut.xcode & GenXcodeMask)==GenTransitioning)
			genBusOut.delta=transFac*(float)GenTimeInc;
		genBusOut.span=globalSpan;
		if(gspreset)
		{
			globalSetPoint=lastGlobalSetPoint=0.0f;
			gspreset=0;
		}
		genBusOut.setPointInc=globalSetPoint-lastGlobalSetPoint;
		lastGlobalSetPoint=globalSetPoint;
		if((integration==GenIntMCMaster)&&pGenBusOut)
		{
			pGenBusOut->xcode      =genBusOut.xcode;
			pGenBusOut->fade       =genBusOut.fade;
			pGenBusOut->delta      =genBusOut.delta;
			pGenBusOut->theta      =genBusOut.theta;
			pGenBusOut->span       =genBusOut.span;
			pGenBusOut->setPointInc=genBusOut.setPointInc;
		}

		// Slave part
		if(integration)
		{
			if(pGenBusIn)
			{
				xcode=pGenBusIn->xcode;
				fade=pGenBusIn->fade;
				theta=pGenBusIn->theta;
				span=pGenBusIn->span;
				setPointInc=pGenBusIn->setPointInc;
			}
			else
			{
				xcode=GenAtBaseLine;
				fade=theta=0.0f;
				span=0.0f;
				setPointInc=0.0f;
			}
		}
		else
		{
			xcode=genBusOut.xcode;
			fade=genBusOut.fade;
			theta=genBusOut.theta;
			span=genBusOut.span;
			setPointInc=genBusOut.setPointInc;
		}

		/* If this Cube has no master generator, then output a safe state on the
		   CNet GenBusOut slots to avoid corrupting the state broadcast by the master.
		*/

		if(pGenBusOut && !CubeHasMaster)
		{
			pGenBusOut->xcode      =GenAtBaseLine;
			pGenBusOut->fade       =0.0F;
			pGenBusOut->delta      =0.0F;
			pGenBusOut->theta      =0.0F;
			pGenBusOut->span       =0.0F;
			pGenBusOut->setPointInc=0.0F;
		}


		// Peak/ramp adaptation
		if(pPTAPort && ((xcode & GenHoldState) == 0))
			switch(xcode & GenXcodeMask)
			{
			case GenCycling:
				if(fade<1.0f)
					break;
				if(pPTAPort->amplitudeAdjust!=0.0f)
				{
					amplitude+=pPTAPort->amplitudeAdjust;
					Clamp(amplitude,0.0f,1.0f);
				}
				if(pPTAPort->medianAdjust!=0.0f)
				{
					cycBias+=pPTAPort->medianAdjust;
					Clamp(cycBias,-1.0f,1.0f);
				}
				if(pPTAPort->phaseAdjust!=0.0f)
				{
					phaseOffset+=pPTAPort->phaseAdjust;
					if(phaseOffset>1.0f)
						phaseOffset-=2.0f;
					if(phaseOffset<-1.0f)
						phaseOffset+=2.0f;
				}
				break;
			case GenTransitioning:
				if(pPTAPort->originAdjust!=0.0f)
				{
					origin+=pPTAPort->originAdjust;
					Clamp(origin,-1.0f,1.0f);
				}
				if(pPTAPort->targetAdjust!=0.0f)
				{
					target+=pPTAPort->targetAdjust;
					Clamp(target,-1.0f,1.0f);
				}
				break;
			case GenExtCmd:
				if(fade<1.0f)
					break;
				if(pPTAPort->amplitudeAdjust!=0.0f)
				{
					localSpan+=pPTAPort->amplitudeAdjust;
					Clamp(localSpan,0.0f,1.0f);
				}
				if(pPTAPort->medianAdjust!=0.0f)
				{
					bias+=pPTAPort->medianAdjust;
					Clamp(bias,-1.0f,1.0f);
				}
				break;
			default:
				break;
			}
		span*=localSpan;

		provChStatus=GenGenerating;
		switch(xcode & GenXcodeMask)
		{
		case GenAtBaseLine:
			output=baseLine;
			provChStatus=GenNotStarted;
			break;
		case GenStaticFade:
			x=fade;
			output=baseLine+x*(bias-baseLine);
			provChStatus=GenStarted;
			break;
		case GenAtBias:
			output=bias;
			break;
		case GenDwelling:
			output=bias;
			if(dynBiasTrack)
				cycBias=bias;
			break;
		case GenTransitioning:
			x=modff(fade*(float)GenProfSz, &y);
			p=profile[(int)y];
			z=p[0]+x*(p[1]+x*(p[2]+x*p[3]));
			if(profileTP==Gensquare){
				output=bias=target;
			}
			else {
				output=bias=origin+z*(target-origin);
			}
			if(dynBiasTrack)
				cycBias=bias;
			//if(hold)genBusOut.xcode=GenAtBias;
			break;
		case GenCycling:
			x=wavePoint(theta);
			x+=cycBias;
			output=x*fade+(1.0f-fade)*bias;
			if(statBiasTrack&&(fade==1.0f))
				bias=cycBias;
			break;
		case GenDirectCyclicStart:
			cycBias=bias-wavePoint(theta);
			output=bias;
			break;
		case GenLoadTransBuf:
			//setup error calc
			prevTP = 0x7FFFFFFF;
			rateFac = 1.0f;
			//countTP = 0;
			//----------------
			target=bufTarget;
			slaveNextType=0;
			origin=bias;
			output=bias;
			provChStatus=GenAtFunctionComplete;
			break;
		case GenLoadDwellBuf:
			slaveNextType=0;
			output=bias;
			provChStatus=GenAtFunctionComplete;
			break;
		case GenLoadCyclicBuf:
			amplitude=bufAmpl;
			phaseOffset=bufPhase;
			slaveNextType=0;
			output=bias;
			provChStatus=GenAtFunctionComplete;
			break;
		case GenExtCmd:
			p=(theta==0.0f)?pExtCmd:pCNetCmd;
			output=bias+fade*span*(p?*p:0.0f);
			break;
		case GenAtBiasFC:
			output=bias;
			provChStatus=GenAtFunctionComplete;
			break;
		case GenAtBiasBaselineTrack:
			output=bias;
			tmp=pModeNo?*pModeNo:0;
			if(tmp>=EPNMode)
				tmp=0;
			modes[tmp]=bias;
			baseLine=bias;
			break;
		case GenDirectCyclicStop:
			x=wavePoint(theta);
			x+=cycBias;
			bias=output=x;
			break;
		case GenTransContStart: // happens at the start of a TP
			// send TP error
			countTP++;//increment countTP
			if((prevTP == 0x7FFFFFFF)||(rateFac == 0.0f)) { //if no prev TP...
				rateFac = 1.0f;}
			if(pPTAPort && crun && !(prevTP == 0x7FFFFFFF)){
				//prevTP = 0x7FFFFFFF; //0x7FFFFFFF is NaN
				//calc error
				if((pPTAPort -> peak == 0x7FFFFFFF) && !(pPTAPort -> trough == 0x7FFFFFFF)){
					//if only trough is detected
					errorTP = prevTP - (pPTAPort -> trough);
					/*
					^
					|_          __          __ '(pPTAPort -> peak) - prevTP;' Gives negative error in this case
					|_\      __/_ \      __/_ \
					| \\    / |  \ \    / |  \ \     CMD
					|  \|__/ /    \_|__/ /    \_|_FB /
					|    \__/        \__/        \__/ 'prevTP - (pPTAPort -> trough);' Gives negative error in this case
					|_________________________________>
					*/
					errorTP = (errorTP * fullScale);// scale error
				}
				else if(!(pPTAPort -> peak == 0x7FFFFFFF) && (pPTAPort -> trough == 0x7FFFFFFF)){
					//if only peak is detected
					errorTP = (pPTAPort -> peak) - prevTP;
					errorTP = (errorTP * fullScale);// scale error
				}
				else {
					errorTP = 0x7FFFFFFF;
				}

				if(errorTP < 0){
					transTemp = -errorTP;
				}
				else {
					transTemp = errorTP;
				}
				if(error1En && (errorTP != 0x7FFFFFFF) && (transTemp > errorThreshold1TP) && (pTsrOut)){
					//do action 1
					(*pTsrOut) |= errorAction1TP;
					if(pCnetTsLo)
						cnetTsLo1=*pCnetTsLo;
					else
						cnetTsLo1=1;
					if(pCnetTsHi)
						cnetTsHi1=*pCnetTsHi;
					trig1 = 1;
				}
				if(error2En && (errorTP != 0x7FFFFFFF) && (transTemp > errorThreshold2TP) && (pTsrOut)){
					//do action 2
					(*pTsrOut) |= errorAction2TP;
					if(pCnetTsLo)
						cnetTsLo2=*pCnetTsLo;
					else
						cnetTsLo2=1;
					if(pCnetTsHi)
						cnetTsHi2=*pCnetTsHi;
					trig2 = 1;
					//Syslog(1, cnetTsLo, cnetTsHi, sce, 1234);
					//Syslog(Uint32 type, Uint32 tslo, Uint32 tshi, Parameter *p, Uint32 vio); //for testing.
				}
				pPTAPort -> peak = 0x7FFFFFFF;
				pPTAPort -> trough = 0x7FFFFFFF;
				//prevTP = bias;
				prevTP = (pPTAPort -> end); //This needs to be the actual target, not the adjusted one from TAV and TAC.
//				*((int *)0x7C) = (*pTsrOut);
				
				//tp_add_to_queue(countTP,errorTP); //add turning point error to TP queue.
				tp_add_to_queue((u32_t)*pCnetTsLo, (u32_t)*pCnetTsHi, (u32_t)countTP, (u32_t)_ftoi(errorTP));

				//---- calculate change in rate ----
				if((rateAdaptEn) && !(errorTP == 0x7FFFFFFF)){ //is adaption is enabled.
					transTemp = errorTP;
					if (transTemp < 0.0f){ //invert if negative
						transTemp *= -1.0f;
					}
					if (transTemp > reduceError){ //if the error is more than reduceError...
						rateFac = rateFac*(1.0f-(reduceRate/100.0f)); //reduce rate by reduceRate%
						if(rateFac > 100.0f) rateFac = 100.0f; //limits rateFac from going too quick
					}
					else if (transTemp < increaseError){ //if error is smaller than increaseError...
						rateFac = rateFac/(1.0f-(increaseRate/100.0f)); //increase rate by increaseRate%
						if(rateFac > 100.0f) rateFac = 100.0f; //limits rateFac from going too quick
					}
				}
				if (!(rateAdaptEn)){
					rateFac = 1.0f;
				}
				//----------------------------------
			}
			else{
				errorTP = 0x7FFFFFFF;
			}
			if(pCNet) //if there is something in CNet...
				target=*((float *)(pCNet+endslot));
//				target=-bias; // Just alternate for now
			else
				target=0.0f;
			
			//work out time if rate is enabled.
			if(transRateEn){
				//*******************************************************************************
				transTemp = (target-bias);

				if(transTemp<0.0f){ //make positive
					transTemp=-transTemp;}
				if(transTemp == 0.0f){ //stop divide by 0 errors
				transTemp = 0.000000000001;}
				if(transRate == 0.0f){ //stop divide by 0 errors
				//if(0){
					//transTemp=MINTIME;
					if(transTemp>0.0f){
						transTemp=MAXTIME;}
					else{
						transTemp=MINTIME;}
				}
				else{
					transTemp/=(transRate*rateFac);} // Convert distance to time
					transTemp*=fullScale;//scale to get correct units
				if (transTemp<MINTIME){ //clip time to limits
					transTemp = MINTIME;}
				if (transTemp>MAXTIME){
					transTemp = MAXTIME;}
				transTime = transTemp;
			}
			//added by paul as a test
			//transTime=nextTransTime;
			transFac=(transTime>0.0f)?1.0f/transTime:0.0f;
			genBusOut.delta=transFac*(float)GenTimeInc;
			origin=bias;
			output=bias;
			pPTAPort -> start = origin;
			pPTAPort -> end = target;
			//targetTP = target;
			break;
		default: // Unknown - hold still
			output=bias;
			break;
		}
		if(xcode & GenSync) 	/* Is sync active? */
			provChStatus=GenAtFunctionComplete;
		chStatus=provChStatus;
		// TSR asserts
		intAFCCheck=((float)fabs(cycBias)+amplitude)>0.9f;
		if(pTsrOut&&integration)
		{
			tsrOut=assertTable[runStatus]|TSRGenSync;
			if(PAAct)
				tsrOut|=TSRAcqEn;
			if(acqpls)
				tsrOut|=TSRAcqPulse;
			if((xcode & GenSync)&&((xcode & 0x0000FFFF)==((Uint32)slaveNextType) || slaveNextType == 4))
				tsrOut&=~TSRGenSync;
			if(intAFCCheck)
				tsrOut|=TSRAFCCheck;
			if(TPDataTaken)
				tsrOut|=TSRTPACK;
			(*pTsrOut)|=tsrOut;
		}

#ifdef KINETUSB
		if(pKiNetSlot)
		{
			x=output;
			Clamp(x, -1.0f, 1.0f);
			*((Sint16 *)pKiNetSlot)=(Sint16)(20000.0f*x);
#ifdef PCTEST
			kinetSlot37=5e-5f*(float)kinet[37];
#endif
		}
#endif
		if(runStatus!=prevRunStatus){ //if we want to send a new generator status...
			prevRunStatus = runStatus;

			/* We use NaN values of the form 0x7FC00000 - 0x7FC0000F to indicate status
			   change events, where the lower 4 bits represent the generator index.

			   The NaN value 0x7FFFFFFF is already used to indicate the situation where
			   no TP error has been calculated because either peak or trough value was
			   not available.
			*/

			tp_add_to_queue((u32_t)*pCnetTsLo, (u32_t)*pCnetTsHi, (u32_t)runStatus, (u32_t)(0x7FC00000 | chanNo));
		}
	}
	float wavePoint(float theta)
	{
		register float x, *p;
		float y;

		x=(1.0f+theta+0.5f*phaseOffset)*harmonic;
		x=modff(x, &y);
		if(antiAlias||(waveShape==GenSine)||(waveShape==GenCustom))
		{
			x=modff(x*(float)GenWTSz, &y);
			p=wvTbl[(int)y];
			x=p[0]+x*p[1];
		}
		else
		{
			switch(waveShape)
			{
			case GenSquare:
				x=(x<0.5f)?1.0f:-1.0f;
				break;
			case GenTriangle:
				if(x<0.25f)
					x=4.0f*x;
				else if (x<0.75f)
					x=2.0f-4.0f*x;
				else
					x=4.0f*x-4.0f;
				break;
			case GenSawtooth:
				x=(x<0.5f)?2.0f*x:2.0f*x-2.0f;
				break;
			case GenTrapezoid:
				if(x<0.25f)
					x=4.0f*x;
				else if (x<0.75f)
					x=2.0f-4.0f*x;
				else
					x=4.0f*x-4.0f;
				x*=(trapRat+1.0f);
				Clamp(x, -1.0f, 1.0f);
				break;
			default:
				x=0.0f;
			}
		}
		x*=amplitude;
		x*=span;
		return x;
	}
// Inputs:
	GenGenBus *pGenBusIn;
	GenGenBus *pGenBusOut;
	Sint32 *pModeNo;
	float  *pFeedback;
	Uint32 *pCnetTsLo;
	Uint32 *pCnetTsHi;
	Uint32 cnetTsLo1;
	Uint32 cnetTsHi1;
	Uint32 cnetTsLo2;
	Uint32 cnetTsHi2;
	Uint32 cnetTsLoDwell;
	Uint32 cnetTsHiDwell;
// Outputs:
	GenGenBus genBusOut;
	float output;
// Supervisory comms:
	Sint32 chStatus;
	float startRampInc;
	float stopRampInc;
	float CFIFac;
	float CFOFac;
    Sint32 broadcast;
	Sint32 start;
	Sint32 stop;
	Sint32 pause;
	Sint32 hold;
	Sint32 nudgeUp;
	Sint32 nudgeDown;
	Sint32 ackComplete;
	Sint32 elementType;
	Sint32 runStatus;
	Sint32 prevRunStatus;
	Sint32 stopAtEnd;
	float baseLine;
	float bias;
	float origin;
	float target;
	float amplitude;
	float cycBias;
	//float peakTemp;
	//float troughTemp;
	float dwellTime;
	float dwellElapsed;
	float prevTP;
	//float targetTP; // A saved version of the target in TP tests. This is used to TAV and TAC can do whatever they want.
	//unsigned long int countTP;
	Sint32 countTP;
	float errorTP;
	float transTime;
	float transRate;
	float rateFac; //this is used to adjust the rate is the error is too large
	Sint32 tpTimeoutEn;
	float dwellErrorTP;
	float dwellTimeTP;
	float dwellDebounceTimeTP;
	float dwellDebounceTimerTP;
	float dwellTimeout1TP;
	float dwellTimeout2TP;
	float dwellGainTP;
	Sint32 profileTP;
	Uint32 dwellActionTP;
	Sint32 dwellTrig;
	Sint32 error1En;
	float errorThreshold1TP;
	Uint32 errorAction1TP;
	Sint32 trig1;
	Sint32 error2En;
	float errorThreshold2TP;
	Uint32 errorAction2TP;
	Sint32 trig2;
	Sint32 transRateEn;
	float transTimeout;
	float transTimeoutOld;
	float transTemp;
	float transElapsed;
	float transFac;
	Sint32 rateAdaptEn;
	float reduceError;
	float reduceRate;
	float increaseError;
	float increaseRate;
	double dfade;
	float fullScale;
	ProfileElement *profile;
	ProfileElement linear[GenProfSz+1];			// NB. All tables are terminated so as
	ProfileElement haversine[GenProfSz+1];		//  to tolerate an index of GenProfSz
	ProfileElement customLinear[GenProfSz+1];
	ProfileElement customSpline[GenProfSz+1];
	float phaseInc;
	Uint32 cyclsRqrd;
	Uint32 cyclsCmplt;
	float phase;
	float fCyclsCmplt;
	Sint32 cycDirStt;
	float phaseOffset;
	float harmonic;
	Sint32 antiAlias;
	Sint32 waveShape;
	float trapRat;
	float wvTbl[GenWTSz][2];
	float sweepTime;
	float sweepElapsed;
	float sweepFac;
	float startPhaseInc;
	float endPhaseInc;
	Uint32 *pTsrIn;
	Uint32 *pTsrOut;
	//Uint32 *pCnetTsLo;
	//Uint32 *pCnetTsHi;
	GenPTAPort *pPTAPort;
	//PTerr *pPTerr;
	Sint32 integration;
	float globalSpan;
	float localSpan;
	float globalSetPoint;
	float lastGlobalSetPoint;
	float span;
	float setPointInc;
	Sint32 gspreset;
	float bufTarget, bufAmpl, bufPhase;
	Sint32 slaveNextType;
	Uint32 CubeHasMaster;
#ifdef KINETUSB
	Uint32 *pKiNetSlot;
#ifdef PCTEST
	float kinetSlot37;
#endif
#endif
	float extTime;
	float extElapsed;
	Sint32 extIndef;
	float fadeInInc;
	float fadeOutInc;
	Sint32 extSource;
	float *pExtCmd;
	float *pCNetCmd;
	Sint32 PAEn;
	Uint32 PADelay;
	Sint32 PADelayMode;
	Uint32 PADelayRem;
	Uint32 PARepeat;
	Sint32 PARepeatMode;
	Uint32 PARepeatNext;
	Uint32 PADuration;
	Sint32 PADurationMode;
	float  PALogLaw;
	float  PACurrentFactor;
	Uint32 PADurationEnd;
	Sint32 PAAct;
	Uint32 elapsed;
	Uint32 elapseCount;
	Sint32 cnetCCEnable;
	Uint32 cnetCCSlot;
	Sint32 cnetElapseEnable;
	Uint32 cnetElapseSlot;
	Uint32 *pCNet;
	Sint32 nextType;
	float  nextTransTime;
	float  nextDwellTime;
	float  nextPhaseInc;
	Uint32 nextCycles;
	Sint32 tsrResp;
	Sint32 dynBiasTrack;
	Sint32 statBiasTrack;
	Sint32 baselineTrack;
	Uint32 state;
	Sint32 transCount; // added for a transition timeout.
	Sint32 transCountMax; // "
	//Uint32 action;
	float modes[EPNMode];
	Sint32 sweepLogMode;
	float logFreqTbl[GenLogFreqTableSz+2];
	float sweepPhaseInc;
	Sint32 afcEn, afcMode;
	float afcLinInc, afcLogFac;
	bool intAFCCheck;
	Sint32 crun;
	Uint32 endslot;
	Uint32 TPDataTaken;
	Uint32 tsrIn, tsrOut;
	Sint32 newTransCalc; //flag to recalculate transition time/rate.
	// ***********************************************************************************
//	float test1;
//	float test2;
//	float test3;
//	float test4;
//	float test5;
//	float test6;
//	float test7;
//	float test8;
	// ***********************************************************************************
private:
	void enterStop()
	{
		runStatus=GenStopStatic;
		elementType=GenNone;
		if(baselineTrack)
		{
			genBusOut.xcode=GenAtBiasBaselineTrack;
			state=32;
		}
		else
		{
			genBusOut.fade=1.0f;
			dfade=1.0;
			genBusOut.xcode=GenStaticFade;
			state=3;
		}
	}
	bool enterFunctionComplete()
	{
		genBusOut.xcode=GenAtBiasFC;
		runStatus=GenFunctionComplete;
		elementType=GenNone;
		start=0;
		state=4;
		elapsed=0;
		elapseCount=0;
		return True;
	}
	bool enterFunctionCompleteAck()
	{
		runStatus=GenFunctionCompleteAck;
		return True;
	}
	bool enterDwell()
	{
		if(dwellElapsed<dwellTime)
		{
			genBusOut.xcode=GenDwelling;
			dfade=(double)dwellElapsed;
			runStatus=GenRunDynamic;
			elementType=GenDwell;
			pause=hold=0;
			state=5;
			return True;
		}
		return False;
	}
	bool enterTrans()
	{
		// Work out transCountMax from transTimeout if different.
		if(transTimeoutOld != transTimeout)
		{
			transTimeoutOld = transTimeout;
			transCount = (Sint32)transTimeout * 4096;
		}
		//if(transElapsed<transTime) //If not finished..
		if(!transitionHasCompleted()) //If not finished..
		{
			genBusOut.xcode=GenTransitioning;
			genBusOut.fade=transFac*transElapsed;
			dfade=(double)transElapsed;
			runStatus=GenRunDynamic;
			elementType=GenTransition;
			pause=hold=0;
			state=7;
			return True;
		}
		if(crun)
		{
			if((tsrIn&TSRTPDAV)&&!TPDataTaken) //If data available and not taken..
			{
				genBusOut.xcode=GenTransContStart;
				runStatus=GenRunDynamic;
				elementType=GenTransition;
				pause=hold=0;
				state=34;
				transCount=(Sint32)transTimeout * 4096; //..reset count to max
				//return True;
			}
			transCount--;
			if(transCount <=0 ){
				return False;
			} //decrement count. If count <=0 then stop test (return False).
			return True;
		}
		return False;
	}
	bool enterCyclic()
	{
		if(cyclsCmplt>=cyclsRqrd)
			return False;
		elementType=GenCyclic;
		start=pause=hold=0;
		holdState=False;
		genBusOut.theta=phase;
		dtheta=(double)phase;
		if(cycDirStt&&(phase==0.0f))
		{
			genBusOut.xcode=GenDirectCyclicStart;
			genBusOut.fade=1.0f;
			dfade=1.0;
			runStatus=GenRunDynamic;
			state=22;
		}
		else
		{
			genBusOut.xcode=GenCycling;
			genBusOut.fade=0.0f;
			dfade=0.0;
			runStatus=GenStartDynamic;
			state=10;
		}
		return True;
	}
	bool enterSweep()
	{
		if(sweepElapsed<sweepTime)
		{
			genBusOut.xcode=GenCycling;
			genBusOut.fade=genBusOut.theta=0.0f;
			dfade=0.0;
			dtheta=0.0;
			runStatus=GenStartDynamic;
			elementType=GenSweep;
			pause=0;
			holdState=False;
			state=16;
			return True;
		}
		return False;
	}
	bool enterExternal()
	{
		if(extElapsed<extTime)
		{
			genBusOut.xcode=GenExtCmd;
			genBusOut.fade=0.0f;
			genBusOut.theta=(float)extSource;
			dfade=0.0;
			runStatus=GenStartDynamic;
			elementType=GenExternal;
			start=pause=hold=0;
			state=24;
			return True;
		}
		return False;
	}
	bool enterDwellPaused()
	{
		if(dwellElapsed<dwellTime)
		{
			genBusOut.xcode=GenAtBias;
			runStatus=GenPauseDynamic;
			elementType=GenDwell;
			start=0;
			state=6;
			return True;
		}
		return False;
	}
	bool enterTransPaused()
	{
		//if(transElapsed<transTime)
		if(!transitionHasCompleted())
		{
			genBusOut.xcode=GenAtBias;
			runStatus=GenPauseDynamic;
			elementType=GenTransition;
			start=0;
			state=8;
			return True;
		}
		return False;
	}
	bool enterCyclicPaused()
	{
		if(cyclsCmplt<cyclsRqrd)
		{
			genBusOut.xcode=GenAtBias;
			runStatus=GenPauseDynamic;
			elementType=GenCyclic;
			start=0;
			state=13;
			return True;
		}
		return False;
	}
	bool enterSweepPaused()
	{
		if(sweepElapsed<sweepTime)
		{
			genBusOut.xcode=GenAtBias;
			runStatus=GenPauseDynamic;
			elementType=GenSweep;
			start=0;
			state=19;
			return True;
		}
		return False;
	}
	bool enterExternalPaused()
	{
		if(extElapsed<extTime)
		{
			genBusOut.xcode=GenAtBias;
			runStatus=GenPauseDynamic;
			elementType=GenExternal;
			start=0;
			state=27;
			return True;
		}
		return False;
	}
	void advanceCyclicPhase()
	{
		if(!holdState)
		{
			dtheta+=(double)phaseInc;
			if(dtheta>=1.0)
				dtheta-=1.0;
			genBusOut.theta=phase=(float)dtheta;
		}
	}
	void advanceSweepPhase()
	{
		register float x, z, *p;
		register double y;

		x=sweepFac*sweepElapsed;
		y=(double)(sweepPhaseInc=0.001f);
		if(sweepLogMode)
		{
			x=modff(x*(float)GenLogFreqTableSz, &z);
			p=logFreqTbl+(int)z;
			y=(double)(sweepPhaseInc=(1.0f-x)*p[0]+x*p[1]);
		}
		else
			y=(double)(sweepPhaseInc=startPhaseInc+x*(endPhaseInc-startPhaseInc));
		dtheta+=y;
		if(dtheta>=1.0)
			dtheta-=1.0;
		genBusOut.theta=(float)dtheta;
	}

	bool inline transitionHasCompleted()
	{
		return (transElapsed + 0.0001f >= (1.0f / transFac));
	}

#ifdef PCTEST
	int tp_add_to_queue(unsigned long tpCount, float tpError, unsigned a, unsigned b) {
		// Data HAS to be a multiple of 256.
		// (ie. 16,32..)

		//read current head in local variable
		//move on by for (including wrap arround)
		//compare against tail,
		//if not gone passed tail then move data to head position and move head
		int size = sizeof(unsigned long) + sizeof(float);

		int *tp_q_head = (int *)0xF8;
		int *tp_q_tail = (int *)0xFC;
		int *tp_queue = (int *)0x100;

		int* head = tp_q_head;

		head = head + size;

		//check if the head will overtake the tail before the wrap..
		//then check again after the wrap.
		//If this is the case then there is no room in the queue.
		if ((tp_q_head<tp_q_tail) && (head >= tp_q_tail)) {
			// then full
			return 1;
		}
		if ((int)head & 0x200) {
			head = (int*)((int)tp_queue | ((int)head & 0x1FF)); //wrap around..
																//..then check for overtaking
			if (head >= tp_q_tail) {
				// then full
				return 1;
			}
		}
		// seems to not be full :)..
		//..so add data to the queue
		//*tp_q_head = data;
		head = tp_q_head;
		//for (;j<size;j++){
		//	*((int *)((((int)head+j) & 0x1FF)|0x100)) = data[j];
		//}

		*((int *)((((int)head) & 0x1FF) | 0x100)) = (tpCount & 0xFFFF0000) >> 16; //tpCount(high)
		head++;
		//wrap around if nessasery
		if ((head) >= (int*)0x200) {
			//then go loop back
			head = (int*)0x100;
		}
		*((int *)((((int)head) & 0x1FF) | 0x100)) = (tpCount & 0x0000FFFF); //tpCount(low)
		head++;
		if ((head) >= (int*)0x200) {
			//then go loop back
			head = (int*)0x100;
		}
		*((float *)((((int)head) & 0x1FF) | 0x100)) = tpError;
		head++;
		if ((head) >= (int*)0x200) {
			//then go loop back
			head = (int*)0x100;
		}

		tp_q_head = (int *)((((int)tp_q_head + size) & 0x1FF) | 0x100); // update tp_q_head;

		return 0;
	}

	int _ftoi(float aNumber)
	{
		return (int)aNumber;
	}
#endif

	Uint32 holdState;
	double dtheta;
//	double dfade;
//	float sweepPhaseInc; Fudge
	typedef Uint32 assertTableType[];
	static const assertTableType assertTable;
};

#ifdef SUPGEN


class GenStartStopClass: public Parameter {
public:
	FloatPar      StartRampTime;
	FloatPar      StopRampTime;
	IntegerPar    FadeInCycles;
	IntegerPar    FadeOutCycles;
	EnumPar       StopAtTestEnd;
	FloatPar      FadeInTime;
	FloatPar      FadeOutTime;
};

class GenKiNetSlotClass: public Parameter {
public:
	EnumPar       Enable;
	IntegerPar    SlotNo;
#ifdef PCTEST
	FloatPar      Slot37;
#endif
};

class GenElementTypeStatusClass: public Parameter {
public:
	EnumPar     Type;
	FloatPar    Progress;
	UnsignedPar Elapsed;
	UnsignedPar Ticks;
};

class GenTestControlClass: public Parameter {
public:
	IntegerPar  Start;
	IntegerPar  Stop;
	IntegerPar  Pause;
	IntegerPar  Hold;
	IntegerPar  NudgeUp;
	IntegerPar  NudgeDown;
	IntegerPar  ackComplete;
	EnumPar     Status;
	UnsignedPar StatusPos;
	GenElementTypeStatusClass CurrentElement;
};

class GenLevelsStaticClass: public Parameter {
public:
	FloatPar    BaseLine;
	FloatPar    Bias;
	FloatPar    Origin;
	FloatPar    Target;
};

class GenLevelsDynamicClass: public Parameter {
public:
	FloatPar Amplitude;
	FloatPar Peak;
	FloatPar Trough;
	FloatPar Bias;
	FloatPar PhaseOffset;
};

class GenSlaveBufferClass: public Parameter {
public:
	EnumPar  ElementType;
	FloatPar TransTarget;
	FloatPar Amplitude;
	FloatPar PhaseOffset;
};

class GenModeClass: public Parameter {
public:
	FloatPar BaseLine;
};

class GenModesClass: public Parameter {
public:
	GenModeClass Members[EPNMode];
	char numBuf[EPNMode][4];
};

class GenLevelsClass: public Parameter {
public:
	SignalRange           Range;
	GenSlaveBufferClass   Buffer;
	GenModesClass	        Modes;
	GenLevelsStaticClass  Static;
	EnumPar               StatBiasTrack;
	EnumPar               BaselineTrack;
	GenLevelsDynamicClass Dynamic;
	EnumPar               DynBiasTrack;
	FloatPar              Output;
  IntegerPar            Broadcast;
};

class GenDwellClass: public Parameter {
public:
	FloatPar   Time;
	FloatPar   Elapsed;
	IntegerPar New;
	IntegerPar ForceComplete;
	EnumPar    Status;
	FloatPar   Progress;
};

class GenTransContClass: public Parameter {
public:
	EnumPar     Enable;
	UnsignedPar EndPointSlotNo;
	UnsignedPar TransTimeSlotNo;
};

class GenTransRateAdapt: public Parameter {
public:
	EnumPar  Enable;
	FloatPar ReduceError;
	FloatPar ReduceRate;
	FloatPar IncreaseError;
	FloatPar IncreaseRate;
	FloatPar CurrentRateGain;
};

class GenDemandHold: public Parameter {
public:
	EnumPar  EnableDemandHold;
	FloatPar DwellErrorTP;
	FloatPar DwellDebounceTimeTP;
	FloatPar DwellTimeout1TP;
	FloatPar DwellTimeout2TP;
	FloatPar DwellGainTP;
	EnumPar  DwellAction;
};

class GenErrorLimit: public Parameter {
public:
	EnumPar  Enable1;
	EnumPar  Action1;
	FloatPar Error1;
	EnumPar  Trig1;
	EnumPar  Enable2;
	EnumPar  Action2;
	FloatPar Error2;
	EnumPar  Trig2;
};


class GenTransitionClass: public Parameter {
public:
	FloatPar          Time;
	FloatPar          Rate;
	EnumPar           ConstantRate;
	FloatPar          Elapsed;
	FloatPar          Timeout; //if no new turning points are recived within this time, the test will stop.
	IntegerPar        TpCount;
	EnumPar           Profile;
	FloatArrayPar     CustomProfile;
	GenTransContClass Continuous;
	GenTransRateAdapt RateAdaptation;
	GenDemandHold     DemandHold;
	GenErrorLimit     ErrorLimit;
	IntegerPar        New;
	IntegerPar        ForceComplete;
	EnumPar           Status;
	FloatPar          Progress;
};

class GenPerAcqSpecClass: public Parameter {
public:
	UnsignedPar Setting;
	EnumPar     Specification;
	FloatPar    LogLaw;
	FloatPar    CurrentFactor;
	UnsignedPar Remaining;
};

class GenPerAcqClass: public Parameter {
public:
	EnumPar            Enable;
	GenPerAcqSpecClass Delay;
	GenPerAcqSpecClass Repeat;
	GenPerAcqSpecClass Duration;
	IntegerPar         Active;
};

class GenCyclicClass: public Parameter {
public:
	FloatPar       Frequency;
	UnsignedPar    CyclesRequired;
	UnsignedPar    CyclesComplete;
	FloatPar       Phase;
	EnumPar        DirectStart;
	IntegerPar     Harmonic;
	EnumPar        WaveShape;
	EnumPar        AntiAlias;
	FloatPar       TrapezoidRatio;
	IntegerPar     New;
	IntegerPar     ForceComplete;
	EnumPar        Status;
	FloatPar       Progress;
};

class GenSweepClass: public Parameter {
public:
	FloatPar      Time;
	FloatPar      Elapsed;
	FloatPar      StartFrequency;
	FloatPar      EndFrequency;
	EnumPar       LogMode;
	IntegerPar    New;
	IntegerPar    ForceComplete;
	EnumPar       Status;
	FloatPar      Progress;
	FloatPar      Frequency;
};

class GenExternalClass: public Parameter {
public:
	FloatPar   Time;
	FloatPar   Elapsed;
	EnumPar    RunIndef;
	EnumPar    Source;
	IntegerPar New;
	IntegerPar ForceComplete;
	EnumPar    Status;
	FloatPar   Progress;
};

class GenElementTypes: public Parameter {
public:
	GenTransitionClass  Transition;
	GenDwellClass       Dwell;
	GenCyclicClass      Cyclic;
	GenSweepClass       Sweep;
	GenExternalClass    External;
};

class GenMasterBufferClass: public Parameter {
public:
	EnumPar     ElementType;
	FloatPar    TransitionTime;
	FloatPar    DwellTime;
	FloatPar    CyclicFrequency;
	UnsignedPar CyclicCycles;
};

class GenSpanSetClass: public Parameter {
public:
	FloatPar   GlobalSpan;
	FloatPar   LocalSpan;
	FloatPar   GlobalSetPoint;
	IntegerPar Reset;
};

class GenCNetMonitorItemClass: public Parameter {
public:
	EnumPar     Enable;
	UnsignedPar Slot;
};

class GenCNetMonitorClass: public Parameter {
public:
	GenCNetMonitorItemClass CycleCount;
	GenCNetMonitorItemClass Elapsed;
};

class GenAFCClass: public Parameter {
public:
	EnumPar  Enable;
	EnumPar  Mode;
	FloatPar LinearRate;
	FloatPar LogRate;
};

class Generator: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpGenerator &smp);
	void setWavetableEntry(int i, float val);
	void newWaveTable();
	EnumPar              GenMode;
	EnumPar              ChannelStatus;
	EnumPar              Integration;
	EnumPar              TSRResponse;
	UnsignedPar          TSRIn;
	GenLevelsClass       Levels;
	GenStartStopClass    StartStop;
	GenPerAcqClass       PerAcq;
	GenCNetMonitorClass  CNetMon;
	GenAFCClass          AFC;
#ifdef	KINETUSB
	GenKiNetSlotClass    KiNetSlot;
#endif
	GenSpanSetClass      SpanSet;
	GenTestControlClass  TestControl;
	GenElementTypes		 ElementTypes;
	GenMasterBufferClass MasterBuffer;
	GenGenBus			 *pGenBusIn;
	GenGenBus			 *pGenBusOut;
	GenPTAPort			 *pPTAPort;
	//PTerr				 *pPTerr;
	float                *pExtCmd;
	float                *pCNetCmd;
	Uint32               *pCNet;
	Uint32				 *pCnetTsLo;
	Uint32				 *pCnetTsHi;
	Sint32               *pModeNo;
	float				 *pFeedback;
//	void newTransRate();
private:
	virtual void parManagedSetEvent(Parameter *sce);
	virtual void parSetEvent(Parameter *sce);
	virtual void parAlteredEvent(Parameter *sce);
	virtual void parRangeChangedEvent(Parameter *sce);
	virtual void parChangedEvent(Parameter *sce);
	virtual void parPrimeSignal();
	virtual void parConfigureSignal();
	virtual void parPollSignal();
	virtual Result special(int fn, void *in, int incnt, void *out, int outcnt);
	void newRange();
	void newStartRampTime();
	void newStopRampTime();
	void newFadeInCycles();
	void newFadeOutCycles();
	void newProgress();
	void newAmplBias();
	void newPeak();
	void newTrough();
	void newPeakTrough();
	void newDwellAction(); //action for Demand hold
	void newTPdwellTrig(Parameter *sce);
	void newTPerror1Action();
	void newTPerror2Action();
	void newTPtrig1(Parameter *sce);
	void initTPtrig1();
	void newTPtrig2(Parameter *sce);
	void initTPtrig2();
	void newDwell(){ElementTypes.Dwell.Elapsed=0.0f;}
	void forceDwell(){ElementTypes.Dwell.Elapsed=ElementTypes.Dwell.Time();}
	void newDwellStatus();
	void newDwellProgress();
	void newTransTime();
	void newTransRate();
	void initStdProfiles();
	void newTransProfile();
	void newCustomProfile();
	void newTrans();
	void forceTrans();//{ElementTypes.Transition.Elapsed=(1.0f/Peek(smpbs->transFac));ElementTypes.Transition.Time=(1.0f/Peek(smpbs->transFac));}//{ElementTypes.Transition.Elapsed=ElementTypes.Transition.Time();}
	void newTransStatus();
	void newTransProgress();
	void newNextFrequency();
	void newHarmonic();
	void newCyclic();
	void forceCyclic();
	void newCyclicStatus();
	void newCyclicProgress();
	void newSweepTime();
	void newStartFrequency();
	void newEndFrequency();
	void newSweep(){ElementTypes.Sweep.Elapsed=0.0f;}
	void forceSweep(){ElementTypes.Sweep.Elapsed=ElementTypes.Sweep.Time();}
	void newSweepStatus();
	void newSweepProgress();
	void newSweepFrequency();
	void newExternal(){ElementTypes.External.Elapsed=0.0f;}
	void forceExternal(){ElementTypes.External.Elapsed=ElementTypes.External.Time();}
	void newExternalProgress();
	void newExternalStatus();
	void newFCyclsCmplt();
#ifdef KINETUSB
	void newKiNetSlot();
#endif
	float frequencyToInc(float f);
	float timeToInc(float t);
	float timeToFac(float t){return (t>0.0f)?1.0f/t:0.0f;}
	float phaseToCycle(float theta);
	void newFadeInTime();
	void newFadeOutTime();
	void initLogFreqTbl();
	SmpGenerator *smpbs;
	float wvTbl[GenWTSz];
	typedef float profileNormType[GenProfSz];
	static const profileNormType profileNorm;
	Uint32 *pTsrIn;
	Uint32 *pTsrOut;
};

#endif

#else

typedef struct {			// Generator bus
	Uint32 xcode;
	float fade;
	float delta;
	float theta;
	float span;
	float setPointInc;
} GenGenBus;

#define GEN_SECOND_CALL
#endif
