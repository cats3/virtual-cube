// Monitor.cpp - Monitor Class module

#include "C3Std.h"

#ifdef SUPGEN

void MonChannel::operator()(Parameter *par, int nCh, int nTx, int ind, SmpMonChannel &smp, SmpSigInputChannel *pTx)
{
	static const char *(AbleStr[])={"Disabled", "Enabled", NULL};
	int i;

	smpbs=&smp;
	pTxs=pTx;
	sprintf(myName, "%d", ind+1);
	i=0;
	while((i<MonMaxPoint)&&ErrorPath::MonitorStr[i])
	{
		pointArray[i]=ErrorPath::MonitorStr[i];
		i++;
	}
	endEPPoints=i;
	if(i<MonMaxPoint)
	{
		pointArray[i]="Transducer_indexed";
		i++;
	}
	pointArray[i]=NULL;
	nTxs=nTx;
	Parameter::operator()(par, myName);
	Name     (this, "Name",      "npy",  MonNameSz,   "None");
	Channel  (this, "Channel",   "ny",   NULL,        1, nCh, "", 1);
	Point    (this, "Point",     "ncy",  NULL,        pointArray);
	Index    (this, "Index",     "ny",   NULL,        1, nTx, "", 1);
	Reading  (this, "Reading",   "rmdv", Smp(output), MinFloat, MaxFloat);
	Settings(this, "Settings");
	Settings.Range	(&Settings, "Range", "ny");
	Settings.Offset (&Settings, "Offset", "ny",   Smp(offset), MinFloat, MaxFloat, "", 0.0F);
	Settings.Scale	(&Settings, "Scale",  "ny",   Smp(scale),  -1000.0F, 1000.0F, "", 1.0F);
	Settings.DAC(&Settings, "DAC");
	Settings.DAC.setFlags("g");
	Settings.DAC.Enable   (&Settings.DAC, "Enable",     "ny", NULL, (char **)AbleStr);
	Settings.DAC.DACNumber(&Settings.DAC, "DAC_number", "ny", NULL, 1, 16, "", 1);
	Settings.CNet(&Settings, "CNet");
	Settings.CNet.setFlags("g");
	Settings.CNet.Enable  (&Settings.CNet, "Enable",    "ny", Smp(cNetEn),   (char **)AbleStr);
	Settings.CNet.TimeSlot(&Settings.CNet, "Time_slot", "ny", Smp(cNetSlot), 0, 399, "", 0);
	pCNet=NULL;
	ppServoChannel=MonitorArray::IContext;
}

void MonChannel::parPrimeSignal()
{
	Parameter::parPrimeSignal();
	newMonitorPoint();
	newRange();
	Poke(smpbs->pCNet, pCNet);
}

void MonChannel::parAlteredEvent(Parameter *sce)
{
	if(sce==&Point)
		newMonitorPoint();
	if(sce==&Index)
		newMonitorPoint();
	if(sce==&Channel)
		newMonitorPoint();
	if(sce==&Settings.DAC.Enable)
		parDACChangeEvent(this);
	if(sce==&Settings.DAC.DACNumber)
		parDACChangeEvent(this);
	Parameter::parAlteredEvent(sce);
}

void MonChannel::parRangeChangedEvent(Parameter *sce)
{
	if(sce==&Settings.Range)
		newRange();
}

void MonChannel::newRange()
{
	Float fs;
	Integer um;

	fs=Settings.Range.FullScale();
	um=Settings.Range.UnipolarMode();
	broadcastRange(this, fs, um, Settings.Range.Units());
}

void MonChannel::newMonitorPoint()
{
	SmpErrorPath *p;

	if(!(p=ppServoChannel[Channel()-1]))
		return;
	if(Point()>=endEPPoints)
	{
		if(Index()>nTxs)
			Poke(smpbs->pInput, NULL);
		else
			Poke(smpbs->pInput, &(pTxs[Index()-1].output));
		return;
	}
	if(Point()==0)
		Poke(smpbs->pInput, NULL);
	else
		Poke(smpbs->pInput, &(p->*(ErrorPath::MonitorOffset[Point()])));
}

void MonitorArray::operator()(Parameter *par, char *n, SmpMonitor &smp, int nCh, int nTx, SmpSigInputChannel *pTx)
{
	MonChannel *p;
	int i;

	smpbs=&smp;
	Parameter::operator()(par, n);
	IContext=pServoChannel;
	for(i=0, p=Channel; i<MonNCh; i++, p++)
	{
		(*p)(this, nCh, nTx, i, smpbs->channel[i], pTx);
		p->setFlags("g");
	}
	for(i=0; i<MonMaxServoCh; i++)
		pServoChannel[i]=NULL;
}

SmpErrorPath **MonitorArray::IContext;
#endif
