// Message.h - Message module header file

#ifdef SUPGEN

#define MSG_GETHANDLE 0x200

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
	char path[256];
} GetHandleMsg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} GetHandleRply;

#define MSG_GETPARTYPE 0x201

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} GetParTypeMsg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
	Uint32 type;
} GetParTypeRply;

#define MSG_GETPARFLAGS 0x202

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} GetParFlagsMsg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
	char   flags[16];
} GetParFlagsRply;

#define MSG_GETPARMIN4 0x203

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} GetParMin4Msg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
	Uint32 min;
} GetParMin4Rply;

#define MSG_GETPARMAX4 0x204

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} GetParMax4Msg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
	Uint32 max;
} GetParMax4Rply;

#define MSG_SETPARVALUE4 0x205

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
	Uint32 value;
} SetParValue4Msg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
} SetParValue4Rply;

#define MSG_GETPARVALUE4 0x206

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} GetParValue4Msg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
	Uint32 value;
} GetParValue4Rply;

#define MSG_SETPARVALUE256 0x207

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
	Uint8  value[256];
} SetParValue256Msg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
} SetParValue256Rply;

#define MSG_GETPARVALUE256 0x208

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} GetParValue256Msg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
	Uint8 value[256];
} GetParValue256Rply;

#define MSG_PARSPECIAL256 0x209

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
	Uint32 fn;
	Uint8  arg[256];
} ParSpecial256Msg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
	Uint8 arg[256];
} ParSpecial256Rply;

#define MSG_GETPARMEMBER256 0x20A

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
	Uint32 index;
} GetParMember256Msg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
	char member[256];
} GetParMember256Rply;

#define MSG_GETPARLIVE4 0x20D

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} GetParLive4Msg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
	Uint32 value;
} GetParLive4Rply;

#define MSG_GETPARUNITS256 0x20E

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} GetParUnits256Msg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
	Uint8 units[256];
} GetParUnits256Rply;

#define MSG_ABORTADJUSTMENT 0x20F

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} AbortAdjustmentMsg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
} AbortAdjustmentRply;

#define MSG_GETPARADDR4 0x210

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} GetParAddr4Msg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
	Uint32 addr;
} GetParAddr4Rply;

#define MSG_SETEVENT 0x211

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
	Uint32 eHandle;
	Uint32 pri;
} SetEventMsg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
} SetEventRply;

#define MSG_SETPARINSTANT 0x212

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
	Uint32 value;
} SetParInstantMsg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
} SetParInstantRply;

#define MSG_NORMALISE 0x213

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 handle;
} NormaliseMsg;

typedef struct {
	Uint16 command;
	Uint16 pad;
	Uint32 result;
} NormaliseRply;

typedef int MsgFunc(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn);

#ifndef PCTEST
extern "C"
{
MsgFunc MsgGetHandle, MsgGetParType, MsgGetParFlags;
MsgFunc MsgGetParMin4, MsgGetParMax4, MsgSetParValue4, MsgGetParValue4, MsgGetParLive4;
MsgFunc MsgSetParValue256, MsgGetParValue256;
MsgFunc MsgParSpecial256, MsgGetParMember256;
MsgFunc MsgGetParUnits256, MsgAbortAdjustment, MsgGetParAddr4, MsgSetEvent, MsgSetInstant, MsgNormalise;
}
#endif

#endif
