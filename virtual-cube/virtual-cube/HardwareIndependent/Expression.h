// Expression.h - Expression Class header

enum {ExpOkay=0,			// Results
	  ExpSyntaxError,
	  ExpTooComplicated,
	  ExpIdentifierUnknown,
	  ExpArgumentCountError,
	  ExpRunTimeError,

	  ExpMaxInstr=32,		// Up to thirty-two instructions
	  ExpMaxKonst=16,		// Up to sixteen constants
	  ExpStackSz=16,		// Size of evaluation stack
	  ExpTextSize=80,		// Size of text string inc. null
	  ExpMaxState=8,		// Maximum state variables (doubles!)
	  ExpNUser=8,			// User inputs
	  ExpMaxServoCh=16,		// Maximum servo channels			// These are for the array of
 	  ExpMaxMon=16,			// Maximum number of monitor points	//  pointers used by monitor()

	  ExpEndFunc=0,			// Function codes
	  ExpIfFunc,
	  ExpOrFunc,
	  ExpAndFunc,
	  ExpEquFunc,
	  ExpNequFunc,
	  ExpLteFunc,
	  ExpLtFunc,
	  ExpGteFunc,
	  ExpGtFunc,
	  ExpAddFunc,
	  ExpSubFunc,
	  ExpMulFunc,
	  ExpDivFunc,
	  ExpNegFunc,
	  ExpNotFunc,
	  ExpPushFunc,
	  ExpLogFunc,
	  ExpLnFunc,
	  ExpExpFunc,
	  ExpPowFunc,
	  ExpSinFunc,
	  ExpCosFunc,
	  ExpTanFunc,
	  ExpAsinFunc,
	  ExpAcosFunc,
	  ExpAtanFunc,
	  ExpLimitFunc,
	  ExpTxFunc,
	  ExpSquFunc,
	  ExpRootFunc,
	  ExpDiffFunc,
	  ExpUserFunc,
	  ExpMinFunc,
	  ExpMaxFunc,
	  ExpDBandFunc,
	  ExpProofFunc,
	  ExpDerFunc,
	  ExpIntFunc,
	  ExpMonitorFunc,
	  ExpAbsFunc,
	  ExpCnetFunc
};

#define	ExpDerivPlatFreq		500.0			// Plateau frequency for derivative		SN
#define	ExpIntFac				(1.0f/SysFs)	// Scaling factor for integral

class SmpExpression {
public:
	void iter(Sint32 pri);
// Inputs:
	float **ppInput;
	float *pUser;
	Uint32 *pGenbus;
	float **pMonVec;
// Outputs:
	float output;
// Supervisory comms:
	Sint32 enable;
	Sint32 error;
	Sint32 priority;
	float derFilterNum[2];
	float derFilterDen[2];
	Sint32 instr[ExpMaxInstr];
	float konst[ExpMaxKonst];
	double stateVec[ExpMaxState];
private:
	float evalStack[ExpStackSz];
};

class SmpExpressionArray {
public:
	void iter(Sint32 pri)
	{
		SmpExpression *p;
		int i;

		for(i=ExpNExp, p=channel; i--; p++)
			p->iter(pri);
	}
// Supervisory comms:
	SmpExpression channel[ExpNExp];
	float userInput[ExpNUser];
	float *(monitorVec[ExpMaxServoCh*ExpMaxMon]);
};

#ifdef SUPGEN

enum ExpFuncType{
	ExpNone=0,
	ExpFunc,
	ExpOp,
	ExpK
};

typedef struct {
	char *id;					// Identifier string
	Sint32 code;				// Function code
	ExpFuncType type;			// Function/operator type
	int nargs;					// Argument count
	void (*ctFunc)(float *p);	// Compile time function
	float val;					// (Parameterless function) value
	int nstates;				// State variable count
} ExpFuncRec;

class Expression: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpExpression &smp);
	EnumPar     Priority;
	TextPar     Formula;
	EnumPar     Status;
	UnsignedPar ErrorPosition;
	IntegerPar  Compile;
	float *pop;
	SignalArray *pSignals;
	virtual void parSetEvent(Parameter *sce);
	virtual void parAlteredEvent(Parameter *sce);
	virtual void parPrimeSignal();
	virtual void parPollSignal();
	virtual void parRecompileSignal();
	Uint32 *pGenbus;
	float **pMonVec;
private:
	void recompileNow();
	Result compile(char *pexpr);
	void optimise();
#ifdef PCTEST
	void decompile();
#endif
	Result expression();
	Result orexpr();
	Result andexpr();
	Result equexpr();
	Result relexpr();
	Result addexpr();
	Result mulexpr();
	Result term();
	Result item();
	Result arglist(int &n);
	void skipws();
	bool nextIs(char *s);
	Result addInstr(Sint32 i, int usage, int states);
	Result addKonst(float k);
	static void ctNegFunc(float *p);
	static void ctNotFunc(float *p);
	static void ctLogFunc(float *p);
	static void ctLnFunc(float *p);
	static void ctExpFunc(float *p);
	static void ctSinFunc(float *p);
	static void ctCosFunc(float *p);
	static void ctTanFunc(float *p);
	static void ctAsinFunc(float *p);
	static void ctAcosFunc(float *p);
	static void ctOrFunc(float *p);
	static void ctAndFunc(float *p);
	static void ctEquFunc(float *p);
	static void ctNequFunc(float *p);
	static void ctLteFunc(float *p);
	static void ctLtFunc(float *p);
	static void ctGteFunc(float *p);
	static void ctGtFunc(float *p);
	static void ctAddFunc(float *p);
	static void ctSubFunc(float *p);
	static void ctMulFunc(float *p);
	static void ctDivFunc(float *p);
	static void ctPowFunc(float *p);
	static void ctAtanFunc(float *p);
	static void ctIfFunc(float *p);
	static void ctLimitFunc(float *p);
	static void ctSquFunc(float *p);
	static void ctRootFunc(float *p);
	static void ctDBandFunc(float *p);
	static void ctAbsFunc(float *p);
	static const ExpFuncRec fnTbl[];
	SmpExpression *smpbs;
	char *strbs;
	char *pstr;
	Sint32 *pinstr;
	int instrcnt;
	float *pkonst;
	int konstcnt;
	int stackcnt;
	int statecnt;
	Sint32 instr[ExpMaxInstr];
	float konst[ExpMaxKonst];
#ifdef PCTEST
	void decomOp(char *s, int n);
	void decomIfOp();
	void decomFunc(char *s, int n);
	char *(decomStack[ExpStackSz+1]);
	char decomStackEl[ExpStackSz+1][1000];
	char **decomStackPtr;
#endif
};

class ExpressionArrayMembers: public Parameter {
public:
	Expression Member[ExpNExp];
};

class ExpUserInput: public Parameter {
public:
	SignalRange Range;
	FloatPar Setting;
	char name[4];
private:
	virtual void parPrimeSignal();
	virtual void parRangeChangedEvent(Parameter *sce);
	void newRange();
};

class ExpUserInputMembers: public Parameter {
public:
	ExpUserInput Member[ExpNUser];
};

class ExpressionArray: public Parameter {
public:
	void operator()(Parameter *par, char *n, SmpExpressionArray &smp);
	ExpressionArrayMembers Members;
	ExpUserInputMembers UserInputs;
	float **ppInput;
	SignalArray *pSignals;
	Uint32 *pGenbus;
	SmpErrorPath *(pServoChannel[ExpMaxServoCh]);
private:
	virtual void parPrimeSignal();
	char vtn[ExpNExp][8];
	SmpExpressionArray *smpbs;
};

#endif
