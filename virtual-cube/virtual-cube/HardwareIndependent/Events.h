// Events definition header
#include <stdint.h>

#ifdef SUPGEN

typedef uint32_t Uint32;

void Syslog(Uint32 type, Uint32 tslo, Uint32 tshi, Parameter *p, Uint32 par);

#define	LogTypeNone					0
#define	LogTypeLimitTrip			1
#define	LogTypeValUR				2
#define	LogTypeValOR				3
#define	LogTypeValNan				4
#define	LogTypeSetRO				5
#define	LogTypeSetUR				6
#define	LogTypeSetOR				7
#define	LogTypeSetNan				8
#define	LogTypeSetLock				9
#define	LogTypePowerdown			10
#define	LogTypeHydraulicStateChange	11

/* parameter 1 = New state
					(depends on hydraulic type, see hydraulic.c for details)
*/

#define	LogTypeNoExtCompTx			12
#define	LogTypeNoModeTx				13
#define	LogTypeNoILTx				14
#define	LogTypeNotConf				15
#define	LogTypeNoSpareVTx			16
#define	LogTypeBadFormula			17
#define	LogTypeCNetStateChange		18

/* parameter 1 = CNet position of device indicating state change
   parameter 2 = New CNet state
					0 = Slave
					1 = Master
					2 = Standalone
*/

#define	LogTypeCNetWdogFail			19

/* parameter 1 = CNet position of device indicating failure */

#define	LogTypeHydUnloadReq			20
#define	LogTypeHydMasterTransition	21

/* Master hydraulic state machine state change

   parameter 1 = New state
					1  = Transition to OFF state from NO-ESTOP state
					2  = Transition to OFF state by timeout from PILOT request
					3  = Transition to OFF state from PILOT state
					4  = Transition to OFF state from waiting for LP state
					5  = Transition to OFF state by timeout from LP request
					6  = Transition to OFF state from LP state
					7  = Transition to OFF state from waiting for HP state
					8  = Transition to PILOT state by request from waiting for HP state
					9  = Transition to LP state by request from waiting for HP state
					10 = Transition to OFF state by timeout during HP request
					11 = Transition to OFF state from HP state
					12 = Transition to OFF state by request from HP state
					13 = Transition to LP state by request from HP state

*/

#define	LogTypeHydSlaveIpOff		22

/* Slave hydraulic state machine state change

   parameter 1 = New state
					4  = Transition to OFF state from PILOT state
					6  = Transition to OFF state from waiting for LP state
					7  = Transition to OFF state from LP state
					9  = Transition to OFF state from waiting for HP state
					10 = Transition to OFF state from HP state
					11 = Transition to OFF state from waiting for LP state from HP state

*/

#define	LogTypeTxdrFaultStateChange	23

/* parameter 1 = Channel identifier (full version bits 8 - 15 indicate type)
   parameter 2 = State flags
					  Bit 0	IP	Input fault
					  Bit 1	SNS	Excitation sense fault
					  Bit 2	EXC	Drive/excitation fault
*/

#define	LogTypeMsgHandlerEvent		24

/* Subtypes for LogTypeMsgHandlerEvent

   parameter 1 = Subtype
					1 = Standard packet header read
					2 =	Standard message code read
					3 =	Standard packet read
					4 =	Standard packet read (CNet destination)
					5 =	Real-time packet header read
					6 =	Real-time message code read
					7 =	Real-time packet read
					8 =	Real-time packet read (CNet destination)

*/

#define LogTypeMsgHandlerClose		25

/* Subtypes for LogTypeMsgHandlerClose

   parameter 1 = Operation
   					1 =	Standard message handler closed
					2 =	Real-time message handler closed

*/

#define LogTypeNetworkConfig		26

/* parameter 1 = IP address
   parameter 2 = Subnet mask
*/

#define LogTypeDHCPEvent			27

/* Subtypes for LogTypeDHCPEvent

   parameter 1 = Event
					1  = DHCP discover request
					2  = DHCP OFFER response
					3  = DHCP discover response timeout
					4  = DHCP ACK response
					5  = DHCP NACK response
					6  = DHCP response timeout
					7  = DHCP request retry
					8  = DHCP renewal request
					9  = DHCP renewal ACK response
					10 = DHCP renewal NACK response
					11 = DHCP renewal retry				
					12 = DHCP rebind request
					13 = DHCP rebind ACK response
					14 = DHCP rebind NACK response
					15 = DHCP rebind timeout
					16 = DHCP lease expiry
					17 = LINKLOCAL ARP probe
					18 = LINKLOCAL ARP conflict 1
					19 = LINKLOCAL ARP conflict 2
					20 = LINKLOCAL claim
					21 = LINKLOCAL defend
					22 = LINKLOCAL attack
					23 = LINKLOCAL relinquish
					24 = MANUAL configuration

*/

#define LogTypeGuardStatusChange	28

/* parameter 1 = New state
					0 = Guard input not supported
					1 = Guard open
					2 = Guard closed
					3 = Guard fault condition detected
*/

#define LogTypeCommTimeout			29

#define LogTypeMsgHandlerOpen		30

/* Subtypes for LogTypeMsgHandlerOpen

   parameter 1 = Operation
   					1 =	Standard message handler opened
					2 =	Real-time message handler opened

*/

#define LogTypeCommBadClose			31

/* Communications socket closed without previous warning message

   parameter 1 = Subtype
					1 = Standard packet header read
					2 =	Standard message code read
					3 =	Standard packet read
					4 =	Standard packet read (CNet destination)
					5 =	Real-time packet header read
					6 =	Real-time message code read
					7 =	Real-time packet read
					8 =	Real-time packet read (CNet destination)

   parameter 2 = Information
   					Bits 31-16 Error number
 				 	Bits 15-0  Socket function returned length

*/

#define LogTypeCommForceClose		32

#define	HIPRI	0xffff0000

#endif

// TSR usage (TSR0)
#define	TSRDump			0x00000001
#define	TSRStartInh		0x00000002
#define	TSRWarning		0x00000004
#define	TSRFreeze		0x00000008
#define	TSRStopDyn		0x00000010
#define	TSRStopStat		0x00000020
#define	TSRFuncPend		0x00000040
#define	TSRDynPend		0x00000080
#define	TSRStatPend		0x00000100
#define	TSRSmoothEn		0x00000200
#define	TSRIntEn		0x00000400
#define	TSRFlowLim		0x00000800
#define	TSRServoEn		0x00001000
#define	TSRLimitEn		0x00002000
#define	TSRStopWR		0x00004000
#define	TSRUnloadWR		0x00008000
#define TSRParLock		0x00010000
#define	TSRAcqEn		0x00020000
#define	TSRAcqPulse		0x00040000
#define	TSRUnanimity	0x00080000
#define	TSRORedSetup	0x00100000
#define	TSRForceSetup	0x00200000
#define	TSRModeChStop	0x00400000
#define	TSRGenSync		0x00800000
#define	TSRInch			0x01000000
#define	TSRAFCCheck		0x02000000
#define	TSRTPDAV		0x04000000
#define	TSRTPACK		0x08000000
#define TSRSpFreezePos	0x10000000
#define TSRSpFreezeNeg	0x20000000
#define TSRPressureOff	0x40000000
#define STARTTSR0		0
#define NTSR0			31

// TSR usage (TSR2)
#define TSRMReady		0x10000000
#define TSRMAck			0x20000000
#define TSRSNotReady	0x40000000
#define TSRSNotAck		0x80000000
#define STARTTSR2		28
#define	NTSR2			4

