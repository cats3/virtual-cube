// ControlCube.cpp - ControlCube application module

#include "C3Std.h"
#include "ControlCube.h"
#include <pthread.h>

#ifdef SUPGEN

#ifdef PCTEST

#define	NDEMOCUBES	1
#define	CNET_DATASLOT_TSR0		233
#define	CNET_DATASLOT_TSR1		235
#define	CNET_DATASLOT_TIMELO	253
#define	CNET_DATASLOT_TIMEHI	254


#include "Windows.h"

static C3App SupSystem[NDEMOCUBES];
Parameter* PSupSystem;
Uint32* CNetInBase, * CNetOutBase, cubeNumber;
Uint32 getCubeNo() { return cubeNumber; }

extern "C" {
	int event_handle_to_bitmask(int handle) { return handle; }
	int eventGen(Parameter* p, int eMask, void* pVal, int szVal)
	{
		//	Trace("Event: ivalue=%d, uval=%u, fval=%f", *((Sint32 *)pVal), *((Uint32 *)pVal), *((float *)pVal));
		return 0;
	}
	void informRangeChange(int chandefIndex, float range, char* units)
	{
		//	Trace("Range change: chandef=%d, range=%f, units=%s", chandefIndex, range, units);
	}
	void set_digital_output(Sint32 index, Uint32 state) {}
	Uint32 get_digital_input(Sint32 index) { return 0; }

}



#else  /* end of PCTEST */

extern "C" {
#include "../include/shared.h"
#include "../include/cnet.h"
#include "../include/eventlog.h"
#include "../include/hydraulic.h"
#include "../include/channel.h"
//#include "..\tcp_ip\support.h"
#include "../include/hardware.h"
#include "../include/eeprom.h"
#include "../include/msgserver.h"
	Uint32 hpi_read4(uint32_t* a);
	void hpi_write4(uint32_t* a, Uint32 d);
	extern int physinchan;
	extern chandef digiochaninfo[];
	extern int totaldigiochan;
#define DIGFLAGSOUT		2
	extern int event_handle_to_bitmask(int handle);
	extern unsigned char* get_channel_base(chandef* def);
	extern void chan_modify_flags(chandef* def, u32_t clr, u32_t set);
	extern void chan_set_setpoint_ptr(chandef* def, float* ptr);
	extern void chan_set_simptr(chandef* def, float* ptr, float pscale, float nscale);
	extern void chan_set_virtualptr(chandef* p, float* q);
	extern Uint32 hyd_get_status(void);
	extern void hyd_unload(void);
	extern Sint32* cnet_get_slotaddr(Uint32 slot, Uint32 proc);
	extern int event_handle_to_bitmask(int handle);
	typedef struct event_report {
		Uint8  src;
		Uint8  flags;
		Uint16 length;
		Uint32 type;
		Uint32 data;
	} event_report;
	extern int event_send_report(int bitmask, event_report* report, int size);
	int eventGen(Parameter* p, int eMask, void* pVal, int szVal)
	{
		static event_report r;

		r.length = 12;
		r.type = (Uint32)p;
		r.data = *((Uint32*)pVal);
		return event_send_report(eMask, &r, 12);
	}
	extern void cnet_get_info(Uint32* ringsize, Uint32* position);
	//void informRangeChange(int chandefIndex, float range, char *units){}
	void set_digital_output(Sint32 index, Uint32 state) { (*((digiochaninfo + index)->write_io_output))(digiochaninfo + index, state); }
	Uint32 get_digital_input(Sint32 index)
	{
		Uint32 st;

		(*((digiochaninfo + index)->read_io_input))(digiochaninfo + index, &st);
		return st;
	}
}

//Sint32* cnet_get_slotaddr(Uint32 slot, Uint32 proc)
//{
//	return (Sint32) * &slot;
//}
static C3App SupSystem;
static Command Conport;
Parameter* PSupSystem = &SupSystem;
Uint32 getCubeNo()
{
	Uint32 rsz, psn;
	cnet_get_info(&rsz, &psn);
	return psn;
}
SigInputChannel* CtrlChkSignal(int c)
{

	if ((c >= 0) && (c < SigNCh))
	{
		return(&SupSystem.Signals.Transducers.Channel[c]);
	}
	return(NULL);
}

#endif /* end of SUPGEN */

#ifndef PCTEST
extern "C" {
#endif
#ifdef PCTEST
#include <deque>
#include <unordered_map>
#endif
	typedef void CBFunc(void* bs, Uint32 sz);
	extern CBFunc CtrlSnvCB, CtrlDnvCB;
	Uint32 CtrlGetSmpSz() { return sizeof(SmpC3App); }
	Uint32 CtrlGetSnvSz() { return((C3App*)PSupSystem)->sizeSNV(0); }
	Uint32 CtrlGetDnvSz() { return ((C3App*)PSupSystem)->sizeDNV(0); }
	void CtrlSetSnvBs(void* p) { ((C3App*)PSupSystem)->setSnvBs(p); }
	void CtrlSetDnvBs(void* p) { ((C3App*)PSupSystem)->setDnvBs(p); }
	void CtrlTSRPoll() { ((C3App*)PSupSystem)->LimitSystem.TSRPoll(); }
	void CubicEngineIter() { CubicEngine* pCubicEngine = CubicEngine::IContext; pCubicEngine->doIter(); }

	Uint32 CtrlGetGenStatus(void)
	{
		Uint32 genstat = 0;
		int n;

		for (n = 0; n < C3AppNChans; n++) {
			GenTestControlClass* p = &((C3App*)PSupSystem)->ServoChannel.Member[n].FunctionGenerator.TestControl;
			Uint32 pos = p->StatusPos();
			if (pos) {
				genstat |= p->Status() << ((pos - 1) * 4);
			}
		}
		return(genstat);
	}

	Uint32 CtrlGetCubicExecutiveFlags() { return((C3App*)PSupSystem)->CubicExecutive.FlagsWord(); }

	void CtrlInit(void* smpbs)
	{
		((C3App*)PSupSystem)->setSmpBs(smpbs);
		((C3App*)PSupSystem)->init();
#ifndef PCTEST
		Conport.init(&SupSystem, 0, "\r\n", True);
#endif
	}
	bool getFullScaleUnits(int chandefIndex, float* pFullScale, char** pUnitsStr)
	{
		int i;
		SigInputChannel* p;

		for (i = 0; i < SigNCh; i++)
		{
			p = (((C3App*)PSupSystem)->Signals.Transducers.Channel) + i;
			if ((p->realTxFlag) && (p->chandefIndex == chandefIndex))
			{
				*pFullScale = p->Range.FullScale();
				*pUnitsStr = p->Range.Units();
				return true;
			}
		}
		return false;
	}
	void CtrlUpdateThreshold(int chan, float threshold)
	{
		int i;
		SigInputChannel* q;

		for (i = 0; i < SigNCh; i++)
		{
			q = ((C3App*)PSupSystem)->Signals.Transducers.Channel + i;
			if (q->chandefIndex == chan)
				q->PeakAdaptation.Terms.Hysteresis = 100.0f * threshold;
		}
	}
	void CtrlSetSimMode(bool flag)
	{
		((C3App*)PSupSystem)->LimitSystem.SimulationMode = flag ? 1 : 0;
		((C3App*)PSupSystem)->LimitSystem.parAlteredEvent(&(((C3App*)PSupSystem)->LimitSystem.SimulationMode));
	}
	void CtrlWdogOut(Uint32 out)
	{
		((C3App*)PSupSystem)->LimitSystem.WdogOut(out);
	}
	void CtrlTSRDAVOut(Uint32 out)
	{
		((C3App*)PSupSystem)->LimitSystem.TSRDAVOut(out);
	}
	Uint32 CtrlGetActionInfo(Uint32 action)
	{
		return ((C3App*)PSupSystem)->LimitSystem.GetActionFlags(action);
	}
	void CtrlClampChangedEvent(void)
	{
		((C3App*)PSupSystem)->parClampChangedEvent(NULL);
	}

#ifdef PCTEST
	void CtrlPoll()
	{
		((C3App*)PSupSystem)->parPollSignal();
#ifdef FASTPOLL
		((C3App*)PSupSystem)->doFastPoll();
#endif
	}
#else
#ifdef FASTPOLL
	void CtrlFastPoll() { ((C3App*)PSupSystem)->doFastPoll(); }
#endif
	void CtrlPoll() { ((C3App*)PSupSystem)->parPollSignal(); }
#endif
	void CtrlValidate() { ((C3App*)PSupSystem)->parValidateSignal(); }
	void CtrlNormalise() { ((C3App*)PSupSystem)->parCorruptSignal(); }
	void CtrlConfigure() { ((C3App*)PSupSystem)->parConfigureSignal(); }
	void CtrlPrime() { ((C3App*)PSupSystem)->parPrimeSignal(); }
	void CtrlClearEventMaskBits(int msk) { ((C3App*)PSupSystem)->clearEventFlags(msk); }
	void CtrlEventPoll(int msk) { ((C3App*)PSupSystem)->eventPoll(msk); }
#ifndef PCTEST
	void CtrlCmd(char* s, int n) { Conport.cmd(s, n); }
	char* CtrlRspStr() { return Conport.rspStr(); }
	int  CtrlRspBuf(char* s, int n) { return Conport.rsp(s, n); }
#endif
	int CtrlGetSafeToStart() { return ((C3App*)PSupSystem)->SafeToStart(); }
	int CtrlIsSetup() { return ((C3App*)PSupSystem)->IsSetup(); }
	void CtrlForceSetup() { ((C3App*)PSupSystem)->ForceSetup(); }
	void ModifyCommandClamp(int channel, float min, float max);
	extern "C" {

		void ctrl_rtctrl_control(Uint32 flags) {
#ifndef PCTEST 
			rtctrl_control(rtmsg_list->stream, flags);
#endif

		}
		void ctrl_rtctrl_pause(Uint32 flags) {
#ifndef PCTEST
			rtctrl_pause(rtmsg_list->stream, flags);
#endif

		}

		void ctrl_rtctrl_resume(void) {
#ifndef PCTEST
			rtctrl_resume(rtmsg_list->stream);
#endif

		}
#ifndef PCTEST
	}
#endif
	}


#ifdef PCTEST
void ModifyCommandClamp(int channel, float min, float max)
{
	((C3App*)PSupSystem)->Signals.Transducers.Channel[channel].setCmdClamp(min, max);
}
#else
void ModifyCommandClamp(int channel, float min, float max)
{
	int i;
	chandef* p;
	SigInputChannel* q;

	p = chanptr(channel);
	for (i = SigNCh, q = ((C3App*)PSupSystem)->Signals.Transducers.Channel; i--; q++)
		if (q->pExtCh == p)
			q->setCmdClamp(min, max);
}
#endif
Sint16 LogLineCount;

#ifdef PCTEST
void acquireSem(Uint32* pSem, Uint32 val)
{
	*pSem = val;
}
void releaseSem(Uint32* pSem, Uint32 val)
{
	if (*pSem != val)
		Syslog(0, 0, 0, NULL, val);
	*pSem = 0;
}
Sint32* cnet_get_slotaddr(Uint32 slot, Uint32 proc) {
	switch (proc) {
	case PROC_DSPA:
		return (Sint32*)(CNetInBase + slot);
		break;
	case PROC_DSPB:
		return (Sint32*)(CNetOutBase + slot);
		break;
	}

}
#else
extern "C" {
#include "../include/asmcode.h"
}
static pthread_mutex_t mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
void acquireSem(Uint32* pSem, Uint32 val)
{
	pthread_mutex_lock(&mutex);
}
void releaseSem(Uint32* pSem, Uint32 val)
{
	pthread_mutex_unlock(&mutex);
}
#endif

void C3App::init()
{
	static const char* (ForceReturnStr[]) = { "Unforced", "ParOkay", "ParNotSettable", "ParOverRange", "ParUnderRange", "ParNan", "ParNotFound", "ParBufferError", NULL };
	int i;

	Adjustment::IContext = &AdjustmentFeeder;
	LimitSys::IContext.smpCntxt = Smp(LimitSystem);
	LimitSys::IContext.supCntxt = &LimitSystem;
	CubicEngine::IContext = &CubicExecutive;
	Parameter::operator()(NULL, "Root");
	CubeNumber(this, "Cube_number", "dr", NULL, 0, 32);
	Signals(this, "Signals");
	Signals.setFlags("g");
	Signals.Transducers(&Signals, "Transducers", smpbs->Transducers, C3AppNChans);
	Signals.VirtualTransducers(&Signals, "Virtual_Transducers", smpbs->VirtualTransducers);
	Signals.Monitors(&Signals, "Monitors", smpbs->Monitors, C3AppNChans, SigNCh, smpbs->Transducers.channel);
	Signals.DigitalInputs(&Signals, "Digital_Inputs", smpbs->DigitalInputs);
	Signals.DigitalOutputs(&Signals, "Digital_Outputs", smpbs->DigitalOutputs);
	Normalisation(this, "Normalisation");
	Normalisation.setFlags("g");
	Normalisation.Inhibited(&Normalisation, "Inhibited", "rl", NULL, 0, 1, "red");
	Normalisation.Start(&Normalisation, "Start", "bp", NULL, 0, 1);
	Normalisation.Progress(&Normalisation, "Progress", "rq", NULL, 0.0f, 100.0f, "%");
	Normalisation.LeafCount(&Normalisation, "Parameter_count", "r", NULL, 0, MaxInt);
	Normalisation.SNVSize(&Normalisation, "SNV_size", "r", NULL, 0, MaxInt, "bytes");
	Normalisation.DNVSize(&Normalisation, "DNV_size", "r", NULL, 0, MaxInt, "bytes");
	Normalisation.Validate(&Normalisation, "Validate", "bp", NULL, 0, 1);
	Normalisation.NVAll(&Normalisation, "NV_all_update", "bp", NULL, 0, 1);
	Test(this, "Test");
	Test.setFlags("g");
	Test.Tx1RangeHandle(&Test, "Tx1_range_handle", "r", NULL);
	Test.Tx2RangeHandle(&Test, "Tx2_range_handle", "r", NULL);
	Test.stickyHandle(&Test, "Sticky_handle", "ux", NULL);
	Test.ForceReturnError(&Test, "Force_return_error", "c", NULL, (char**)ForceReturnStr);
	ServoChannel(this, "Servo_Channel");
	ServoChannel.setFlags("g");
	for (i = 0; i < C3AppNChans; i++)
	{
		sprintf(chName[i], "%d", i + 1);
		(ServoChannel.Member[i])((Parameter*)&ServoChannel, chName[i]);
		(ServoChannel.Member[i]).setFlags("g");
		ServoChannel.Member[i].Controller((Parameter*)(&ServoChannel.Member[i]), "Controller", smpbs->Channel[i].Controller);
		ServoChannel.Member[i].FunctionGenerator((Parameter*)(&ServoChannel.Member[i]), "Function_Generator",
			smpbs->Channel[i].FunctionGenerator);
	}
	LimitSystem(this, "TSR", smpbs->LimitSystem);
	LimitSystem.setFlags("g");
	LimitSystem.pConfigOkay = &combinedFlags;
	CubicExecutive(this, "Cubic_Engine", cubicProgram, smpbs->LimitSystem);
	CubicExecutive.setFlags("g");
	Unanimity(this, "Unanimity");
	Unanimity.setFlags("g");
	Unanimity.Period(&Unanimity, "Period", "ny", NULL, 0.0f, 100.0f, "seconds", 0.01f);
	Unanimity.ULimit(&Unanimity, "Limit", smpbs->Unanimity);
	Unanimity.ULimit.setFlags("g");
	AdjustmentFeeder(this, "Adjustment", smpbs->AdjustmentMechanism);
	AdjustmentFeeder.setFlags("g");
	Log(this, "Event_Log");
	Log.setFlags("g");
	Log.LogLines(&Log, "Log_Line");
	Log.LogLines.setFlags("g");
	for (i = 0; i < C3MaxLogLines; i++)
	{
		sprintf(Log.LogLines.logName[i], "%d", i + 1);
		Log.LogLines.Line[i](&Log.LogLines, Log.LogLines.logName[i], "r", 1024);
	}
	LogLineCount = 0;
	Log.More(&Log, "More", "rl", NULL, 0, 1, "red");
	Log.Reset(&Log, "Reset", "b", NULL, 0, 1);
	Log.Reappraise(&Log, "Reappraise", "b", NULL, 0, 1);
	Log.Translation(&Log, "Handle_Translation");
	Log.Translation.Handle(&Log.Translation, "Handle", "x", NULL, 0, MaxUns);
	Log.Translation.Path(&Log.Translation, "Path", "r", C3TransPathSz);
	LinearAnalyser(this, "Analyser", smpbs->LinearAnalyser);
	Regressor(this, "Regression", smpbs->Regressor);
	countLeafParameters();
	goodConfig = goodExpressions = combinedFlags = 0;
	configMeta = false;
}

void C3App::parPrimeSignal()
{
	int i, j, k, l, m;
	char sceBuf[SigConSz];
	SigInputChannel* q;
#ifndef PCTEST
	chandef* p;
	Sint32* tsrSlot;
	Sint32* tsrOutSlot;
	float* s;
	Uint32 dioflags;
	Uint8 panelBuf[32];
	int svFlag = 0;
	int svCalc = 0;
#endif
	char contemp[SigConSz], sertemp[SigSernoSz];

	Poke(smpbs->unanPer, (Uint32)(((float)SysFs) * Unanimity.Period()));
	Poke(smpbs->pTSRin, &smpbs->LimitSystem.tsrInBufLo);
	for (i = 0; i < C3AppNChans; i++)
		if (i < SigMaxChans)
			ServoChannel.Member[i].FunctionGenerator.pPTAPort = &smpbs->Transducers.ptaPort[i];
		else
			ServoChannel.Member[i].FunctionGenerator.pPTAPort = NULL;

#ifdef PCTEST
	for (i = 0; i < C3AppNChans; i++)
	{
		ServoChannel.Member[i].FunctionGenerator.pGenBusIn = (GenGenBus*)(CNetInBase + CNET_DATASLOT_TSR1);
		ServoChannel.Member[i].FunctionGenerator.pGenBusOut = (GenGenBus*)(CNetOutBase + CNET_DATASLOT_TSR1);
		ServoChannel.Member[i].FunctionGenerator.pCnetTsLo = CNetInBase + CNET_DATASLOT_TIMELO;
		ServoChannel.Member[i].FunctionGenerator.pCnetTsHi = CNetInBase + CNET_DATASLOT_TIMEHI;
	}
	Signals.Transducers.pGb = (GenGenBus*)(CNetInBase + CNET_DATASLOT_TSR1);
	LimitSystem.pTSRIn = CNetInBase + CNET_DATASLOT_TSR0;
	LimitSystem.pTSROut = CNetOutBase + CNET_DATASLOT_TSR0;
	LimitSystem.pCnetTsLo = CNetInBase + CNET_DATASLOT_TIMELO;
	LimitSystem.pCnetTsHi = CNetInBase + CNET_DATASLOT_TIMEHI;
	LimitSystem.pCnetA = CNetInBase;
	LimitSystem.pCnetB = CNetInBase;
	LimitSystem.pGenbus = CNetInBase + CNET_DATASLOT_TSR1;
	Signals.VirtualTransducers.pGenbus = CNetInBase + CNET_DATASLOT_TSR1;
	for (i = 0; i < MonNCh; i++)
		Signals.Monitors.Channel[i].pCNet = (float*)CNetOutBase;
	for (i = 0; i < C3AppNChans; i++)
	{
		ServoChannel.Member[i].Controller.pCNet = (float*)CNetInBase;
		ServoChannel.Member[i].FunctionGenerator.pCNet = (Uint32*)CNetOutBase;
	}
	Signals.Transducers.pCNet = (float*)CNetInBase;
	LinearAnalyser.pCNet = CNetInBase;
#else
	// Wire transducer inputs
	for (i = 0, q = Signals.Transducers.Channel; i < SigNCh; i++, q++)
		if (p = chanptr(i))
		{
			q->pExtCh = (void*)p;
			q->pInput = (float*)get_channel_base(p);

			if (i < (SigNCh - ExpNExp - C3AppNChans))
			{
				sprintf(sceBuf, "(chandef[%d])", i);
				q->realTxFlag = (i < physinchan) ? 1 : 0;
				q->chandefIndex = i;
			}
			else if (i < (SigNCh - C3AppNChans))
			{
				chan_set_virtualptr(p, &smpbs->VirtualTransducers.channel[i - SigNCh + ExpNExp + C3AppNChans].output);
				sprintf(sceBuf, "(Virtual Tx %d)", i - SigNCh + ExpNExp + C3AppNChans + 1);
			}
			else
			{
				chan_set_virtualptr(p, &smpbs->Channel[i - SigNCh + C3AppNChans].Controller.cmd);
				sprintf(sceBuf, "(Command %d)", i - SigNCh + C3AppNChans + 1);
			}
			q->Connector = sceBuf;
			chan_set_setpoint_ptr(p, &smpbs->Transducers.channel[i].setPnt);
		}
		else
		{
			q->pExtCh = NULL;
			q->pInput = NULL;
			q->Connector = "None";
			q->chandefIndex = -1;
		}
	while (p = chanptr(i))
	{
		chan_set_setpoint_ptr(p, NULL);
		i++;
	}
	virtualBs = SigNCh - ExpNExp - C3AppNChans;
	Poke(smpbs->ACnt, virtualBs);
	Poke(smpbs->BCnt, SigNCh - virtualBs);

	// Configure monitor DACs
	parDACChangeEvent(this);

	// Read from EEPROM to see how to set up SVdrives.
	if (dsp_1w_readpage(PAGE_PANELCFG, panelBuf, 31, CHK_CHKSUM) == NO_ERROR) {
		//then no error so get information
		if ((panelBuf[28] == panelBuf[29]) && (panelBuf[28] == panelBuf[30])) {
			// If all agree, then carry on.
			svFlag = panelBuf[28];
		}
	}
#ifdef CONTROLCUBE
	// Wire servovalve outputs
	for (i = 0; i < C3AppNChans; i++) //C3AppNChans needs to be 4 or less (don't change this please!).
								//IF you did want to change this, or use this function on the AI cube because you want to annoy me,
								//you could use more bits out of panelBuf[28 to 30] and use the same method.
		if (p = chanptr(512 + i))
		{
			/*
			panelBuf[28] is split into 4 lots of 2 bits. Such that (panelBuf[28] & 0x03) corresponds to output 0. The bits contain an offset to apply.
			For example, (panelBuf[28] & 0xC0) corresponds to output 3. If the output need to be changed to PID output 2 then panelBuf should be B11xxxxxx.

			panelBuf[28] = BWW XX YY ZZ. WW is the offset to apply to output 3, XX is the offset to apply to output 2, YY to output 1, ZZ to output 0.

			If panelBuf[28] = B00 00 00 00, then the physical output will look at the same number PID output. (this is to keep the functionality the same as it has been).
		physical output		PID output
			output0------>SVdrive0
			output1------>SVdrive1
			output2------>SVdrive2
			output3------>SVdrive3

			If panelBuf[28] = B00 01 10 11, then the physical output will be linked to the PID output as shown below (all 4 physical outputs connected to PID output 3).
		physical output		PID output
			output0-\     SVdrive0
			output1--\    SVdrive1
			output2---\   SVdrive2
			output3------>SVdrive3

			If panelBuf[28] = B10 11 11 00, then the physical output will be linked to the PID output as shown below.
			physical output 0 and 1 connect to PID output 0. physical output 2 and 3 connect to PID output 1.
		physical output		PID output
			output0------>SVdrive0
			output1-/  /->SVdrive1
			output2---/   SVdrive2
			output3--/    SVdrive3
			*/
			svCalc = i + (svFlag & 0x03);
			if (svCalc >= C3AppNChans) svCalc = svCalc - C3AppNChans;
			p->sourceptr = (int*)Smp(Channel[svCalc].Controller.output); //This is where one can change where the SV outputs to.
			svFlag = svFlag >> 2;
			update_shared_channel_parameter(p, offsetof(chandef, sourceptr), sizeof(float*));
		}
#else //this else is just to make sure that nothing weird happens with the AICUBE (we are assuming 4xSV max in the above code)
	for (i = 0; i < C3AppNChans; i++)
		if (p = chanptr(512 + i))
		{
			p->sourceptr = (int*)Smp(Channel[i].Controller.output); //This is where one can change where the SV outputs to.
			update_shared_channel_parameter(p, offsetof(chandef, sourceptr), sizeof(float));
		}
#endif
	// TSR bus
	tsrSlot = cnet_get_slotaddr(CNET_DATASLOT_TSR0, PROC_DSPB);
	LimitSystem.pTSRIn = (Uint32*)tsrSlot;
	tsrSlot = cnet_get_slotaddr_out(CNET_DATASLOT_TSR0, PROC_DSPB);
	LimitSystem.pTSROut = (Uint32*)tsrSlot;
	tsrSlot = cnet_get_slotaddr(CNET_DATASLOT_TSR1, PROC_DSPB);
	LimitSystem.pGenbus = (Uint32*)tsrSlot;
	Signals.VirtualTransducers.pGenbus = (Uint32*)tsrSlot;

	// Function generator GenBus
	tsrSlot = cnet_get_slotaddr(CNET_DATASLOT_TSR1, PROC_DSPB);
	tsrOutSlot = cnet_get_slotaddr_out(CNET_DATASLOT_TSR1, PROC_DSPB);
	for (i = 0; i < C3AppNChans; i++)
	{
		ServoChannel.Member[i].FunctionGenerator.pGenBusIn = (GenGenBus*)tsrSlot;
		ServoChannel.Member[i].FunctionGenerator.pGenBusOut = (GenGenBus*)tsrOutSlot;
	}
	Signals.Transducers.pGb = (GenGenBus*)tsrSlot;

	// CNet pointer
	tsrSlot = cnet_get_slotaddr(0, PROC_DSPB);
	for (i = 0; i < C3AppNChans; i++)
		ServoChannel.Member[i].FunctionGenerator.pCNet = (Uint32*)tsrSlot;
	LinearAnalyser.pCNet = (Uint32*)tsrSlot;

	// Global time
	tsrSlot = cnet_get_slotaddr(CNET_DATASLOT_TIMELO, PROC_DSPB);
	LimitSystem.pCnetTsLo = (Uint32*)tsrSlot;
	for (i = 0; i < C3AppNChans; i++)
	{
		ServoChannel.Member[i].FunctionGenerator.pCnetTsLo = (Uint32*)tsrSlot;
	}
	tsrSlot = cnet_get_slotaddr(CNET_DATASLOT_TIMEHI, PROC_DSPB);
	LimitSystem.pCnetTsHi = (Uint32*)tsrSlot;
	for (i = 0; i < C3AppNChans; i++)
	{
		ServoChannel.Member[i].FunctionGenerator.pCnetTsHi = (Uint32*)tsrSlot;
	}
	tsrSlot = cnet_get_slotaddr(0, PROC_DSPA);
	LimitSystem.pCnetA = (Uint32*)tsrSlot;
	tsrSlot = cnet_get_slotaddr(0, PROC_DSPB);
	LimitSystem.pCnetB = (Uint32*)tsrSlot;

	// CNet external feedbacks and command
	s = (float*)cnet_get_slotaddr(0, PROC_DSPB);
	for (i = 0; i < C3AppNChans; i++)
		ServoChannel.Member[i].Controller.pCNet = s;
	Signals.Transducers.pCNet = s;

	// Digital IO
	p = digiochaninfo;
	i = 0;
	j = 0;
	for (k = 0; k < totaldigiochan; k++)
	{
		sprintf(sceBuf, "(digiochaninfo[%d])", k);
		(*((p + k)->read_io_info))(p + k, &dioflags, NULL, NULL, NULL);
		if (dioflags & DIGFLAGSOUT)
		{
			if (j < DigNDigout)
			{
				Signals.DigitalOutputs.Channel[j].Connector = sceBuf;
				Signals.DigitalOutputs.Channel[j].chandefindex = k;
				j++;
			}
		}
		else
		{
			if (i < DigNDigin)
			{
				Signals.DigitalInputs.Channel[i].Connector = sceBuf;
				Signals.DigitalInputs.Channel[i].chandefindex = k;
				i++;
			}
		}
	}

#endif // PCTEST

	// Link transducers -> virtual transducers
	Signals.VirtualTransducers.ppInput = Signals.Transducers.pop;

	// Let function generator know which mode we're in
	for (i = 0; i < C3AppNChans; i++) {
		ServoChannel.Member[i].FunctionGenerator.pModeNo = &(smpbs->Channel[i].Controller.modeNo);
		ServoChannel.Member[i].FunctionGenerator.pFeedback = &(smpbs->Channel[i].Controller.feedback);
	}

#ifdef PCTEST
	// Internally link virtual transducers -> transducers NB. SigNCh MUST be bigger than ExpNExp!!!
	virtualBs = SigNCh - ExpNExp;
	for (i = 0; i < ExpNExp; i++)
	{
		sprintf(sceBuf, "(Virtual Tx %d)", i + 1);
		Signals.Transducers.Channel[i + SigNCh - ExpNExp].Connector = sceBuf;
		Signals.Transducers.Channel[i + SigNCh - ExpNExp].pInput = Signals.VirtualTransducers.Members.Member[i].pop;
		Signals.Transducers.Channel[i + SigNCh - ExpNExp].pExtCh = NULL;
	}
	for (i = 0, q = Signals.Transducers.Channel; i < (SigNCh - ExpNExp); i++, q++)
		q->realTxFlag = 1;
#endif

	// Internal command links
	for (i = 0; i < C3AppNChans; i++)
	{
		ServoChannel.Member[i].Controller.pIntCmd = &(smpbs->Channel[i].FunctionGenerator.output);
		ServoChannel.Member[i].Controller.pSpan = &(smpbs->Channel[i].FunctionGenerator.span);
		ServoChannel.Member[i].Controller.pSetPointInc = &(smpbs->Channel[i].FunctionGenerator.setPointInc);
		ServoChannel.Member[i].Controller.CommandSource.FuncGen.pCmdRange
			= &ServoChannel.Member[i].FunctionGenerator.Levels.Range;
		if (i < MonMaxServoCh)
			Signals.Monitors.pServoChannel[i] = &smpbs->Channel[i].Controller;
		if (i < ExpMaxServoCh)
			Signals.VirtualTransducers.pServoChannel[i] = &smpbs->Channel[i].Controller;
		ServoChannel.Member[i].FunctionGenerator.pCNetCmd = &(smpbs->Channel[i].Controller.cNetCmd);
		ServoChannel.Member[i].FunctionGenerator.pExtCmd = &(smpbs->Channel[i].Controller.extCmd);
	}

	// Check load limited displacement mode situation
	for (i = 0; i < C3AppNChans; i++)
	{
		k = -1;
		l = -1;
		m = -1;
		j = 0;
		while (j >= 0)
		{
			j = Signals.Transducers.FindSignal(SigOuterLoopFeedback, j, i + 1);
			if (j >= 0)
			{
				if (Signals.Transducers.Channel[j].SetupParticipation() == SetupPartContr)
					k = j;
				if (Signals.Transducers.Channel[j].SetupParticipation() == SetupPartMon)
					l = j;
				if (Signals.Transducers.Channel[j].SetupParticipation() == SetupPartImpl)
					m = j;
				j++;
			}
		}
		if ((k >= 0) && (l >= 0) && (m >= 0))
		{
			Signals.Transducers.Channel[m].pLoadCh = Signals.Transducers.Channel + l;
			Signals.Transducers.Channel[m].pDispCh = Signals.Transducers.Channel + k;
			Signals.Transducers.Channel[k].pSetupCh = Signals.Transducers.Channel + m;
			Signals.Transducers.Channel[l].pSetupCh = Signals.Transducers.Channel + m;
			Signals.Transducers.Channel[m].setSetupModeLoadPointer(&smpbs->Transducers.channel[l].output);
		}
	}

	// Let virtual channels know where signals are
	Signals.VirtualTransducers.pSignals = &Signals.Transducers;

	// Let regressor know where the DSPB transducer array and generator GenBus are
	Regressor.pTx = smpbs->Transducers.channel;
	Regressor.pGenBusIn = (GenGenBus*)cnet_get_slotaddr(CNET_DATASLOT_TSR1, PROC_DSPB);

	// Make hand controller deltas available to transducers and controllers
	Signals.Transducers.pHCDelta = &smpbs->LimitSystem.handConDelta;
	for (i = 0; i < C3AppNChans; i++)
		ServoChannel.Member[i].Controller.pHCDelta = &smpbs->LimitSystem.handConDelta;

	Parameter::parPrimeSignal();

	// Legacy setup mode
	for (i = 0; i < SigNCh; i++)
		if (Signals.Transducers.Channel[i].Range.SRParameter() == RangeParSetupLegacy)
		{
			configMeta = true;
			q = Signals.Transducers.Channel + i;
			strncpy(contemp, q->Connector(), SigConSz);
			strncpy(sertemp, q->SerialNumber(), SigSernoSz);
			q->parNormaliseSignal();
			q->Connector = contemp;
			q->SerialNumber = sertemp;
			configMeta = false;
		}

	doConfigure();
	Parameter::parConfigureSignal();
	parClampChangedEvent(NULL);

	CubeNumber = getCubeNo() + 1;
	Normalisation.SNVSize = snvSz;
	Normalisation.DNVSize = dnvSz;

	Test.Tx1RangeHandle = (Uint32)(&Signals.Transducers.Channel[0].Range);
	Test.Tx2RangeHandle = (Uint32)(&Signals.Transducers.Channel[1].Range);
	Test.stickyHandle = 0;
	Test.ForceReturnError = 0;

	// For testing events
//	Signals.Transducers.Channel[0].SetPoint.setEvent(0x0080, 1);
//	Signals.Transducers.Channel[1].SetPoint.setEvent(0x0001, 1);
//	Signals.Transducers.Channel[2].SetPoint.setEvent(0x0080, 1);
}

void C3App::parPollSignal()
{
	Normalisation.Inhibited = LimitSystem.ParLock;
	CubeNumber = getCubeNo() + 1;
	doSomeNormalisation();
	Parameter::parPollSignal();
}

void C3App::parConfigureSignal()
{
	doConfigure();
	Parameter::parConfigureSignal();
}

void C3App::parSNVValidateUREvent(Parameter* sce)
{
	void* p;
	Uint32 s;

	p = sce->getBsSz(s);
	Syslog(LogTypeValUR, 0, 0, sce, sce->getViolation());
	CtrlSnvCB(p, s);
}

void C3App::parSNVValidateOREvent(Parameter* sce)
{
	void* p;
	Uint32 s;

	p = sce->getBsSz(s);
	Syslog(LogTypeValOR, 0, 0, sce, sce->getViolation());
	CtrlSnvCB(p, s);
}

void C3App::parSNVValidateNANEvent(Parameter* sce)
{
	void* p;
	Uint32 s;

	p = sce->getBsSz(s);
	Syslog(LogTypeValNan, 0, 0, sce, sce->getViolation());
	CtrlSnvCB(p, s);
}

void C3App::parDNVValidateUREvent(Parameter* sce)
{
	void* p;
	Uint32 s;

	p = sce->getBsSz(s);
	Syslog(LogTypeValUR, 0, 0, sce, sce->getViolation());
	CtrlDnvCB(p, s);
}

void C3App::parDNVValidateOREvent(Parameter* sce)
{
	void* p;
	Uint32 s;

	p = sce->getBsSz(s);
	Syslog(LogTypeValOR, 0, 0, sce, sce->getViolation());
	CtrlDnvCB(p, s);
}

void C3App::parDNVValidateNANEvent(Parameter* sce)
{
	void* p;
	Uint32 s;

	p = sce->getBsSz(s);
	Syslog(LogTypeValNan, 0, 0, sce, sce->getViolation());
	CtrlDnvCB(p, s);
}

void C3App::parSNVModifiedEvent(Parameter* sce)
{
	void* p;
	Uint32 s;

	p = sce->getBsSz(s);
	CtrlSnvCB(p, s);
}

void C3App::parDNVModifiedEvent(Parameter* sce)
{
	void* p;
	Uint32 s;

	p = sce->getBsSz(s);
	CtrlDnvCB(p, s);
}

void C3App::parSetROEvent(Parameter* sce)
{
	Syslog(LogTypeSetRO, 0, 0, sce, sce->getViolation());
}

void C3App::parSetUREvent(Parameter* sce)
{
	Syslog(LogTypeSetUR, 0, 0, sce, sce->getViolation());
}

void C3App::parSetOREvent(Parameter* sce)
{
	Syslog(LogTypeSetOR, 0, 0, sce, sce->getViolation());
}

void C3App::parSetNanEvent(Parameter* sce)
{
	Syslog(LogTypeSetNan, 0, 0, sce, sce->getViolation());
}

void C3App::parSetLockEvent(Parameter* sce)
{
	Syslog(LogTypeSetLock, 0, 0, sce, sce->getViolation());
}

void C3App::parConfigChangedEvent(Parameter* sce)
{
	parConfigureSignal();
}

void C3App::parSetEvent(Parameter* sce)
{
	char buf[C3TransPathSz];
	static char* nullstr = "";
	int i;

	if ((sce == &Normalisation.Start) && !Normalisation.Inhibited())
		startNormalisation();
	if ((sce == &Normalisation.Validate) && !Normalisation.Inhibited())
		parValidateSignal();
	if (sce == &Normalisation.NVAll)
	{
		CtrlSnvCB(snvBs, snvSz);
		CtrlDnvCB(dnvBs, dnvSz);
	}
	if (sce == &Log.Reset)
	{
		for (i = 0; i < C3MaxLogLines; i++)
			Log.LogLines.Line[i] = nullstr;
		Log.More = 0;
		LogLineCount = 0;
	}
	if (sce == &Log.Reappraise)
	{
		for (i = 0; i < C3MaxLogLines; i++)
			Log.LogLines.Line[i] = nullstr;
		Log.More = 0;
		LogLineCount = 0;
		parConfigChangedEvent(this);
		parCompileEvent(this);
	}
	if (sce == &Log.Translation.Handle)
	{
		Parameter::InstancePath((Parameter*)Log.Translation.Handle(), buf, C3TransPathSz);
		Log.Translation.Path = buf;
	}
}

void C3App::parAlteredEvent(Parameter* sce)
{
	if (sce == &Unanimity.Period)
		Poke(smpbs->unanPer, (Uint32)(((float)SysFs) * Unanimity.Period()));
}

void C3App::countLeafParameters()
{
	Parameter* p, * q;
#ifdef FASTPOLL
	Parameter* r;
#endif
	int i;

	p = this;
#ifdef FASTPOLL
	r = NULL;
#endif
	while (q = p->getSubList())
		p = q;
	i = 1;
#ifdef FASTPOLL
	if (p->flag('d'))
	{
		if (r)
			r->fastChangePollNext = p;
		else
			fastChangePollList = p;
		r = p;
	}
#endif
	do
	{
		while (!(q = p->getNext()) && (p = p->getParent()))
			;
		p = q;
		if (p)
		{
			while (q = p->getSubList())
				p = q;
			i++;
#ifdef FASTPOLL
			if (p->flag('d'))
			{
				if (r)
					r->fastChangePollNext = p;
				else
					fastChangePollList = p;
				r = p;
			}
#endif
		}
	} 	while (p);
#ifdef FASTPOLL
	if (r)
		r->fastChangePollNext = NULL;
	else
		fastChangePollList = NULL;
	amready = ParGoodMagic;
#endif
	nLeaves = i;
	Normalisation.LeafCount = i;
	normalisationPoint = NULL;
	normalisationCount = 0;
}

#ifdef FASTPOLL
void C3App::doFastPoll()
{
	Parameter* p;

	if (amready != ParGoodMagic)
		return;
	p = fastChangePollList;
	while (p)
	{
		p->fastPoll();
		p = p->fastChangePollNext;
	}
}
#endif

void C3App::startNormalisation()
{
	Parameter* p, * q;

	// Find first leaf node
	p = this;
	while (q = p->getSubList())
		p = q;
	normalisationPoint = p;
	normalisationCount = 0;
	Normalisation.Progress = 0.0f;
	configMeta = true;
}

void C3App::doSomeNormalisation()
{
	Parameter* p, * q;
	int i;
	static char* nullstr = "";

	if (!normalisationPoint)
		return;

	i = C3NormalisationBlock;
	while (i && normalisationPoint)
	{
		p = normalisationPoint;
		p->parNormaliseSignal();
		normalisationCount += 1;
		while (!(q = p->getNext()) && (p = p->getParent()))
			;
		p = q;
		if (p)
			while (q = p->getSubList())
				p = q;
		normalisationPoint = p;
		i--;
	}
	Normalisation.Progress = 100.0f * (float)normalisationCount / (float)nLeaves;
	if (!normalisationPoint)
	{
		configMeta = false;
		for (i = 0; i < C3MaxLogLines; i++)
			Log.LogLines.Line[i] = nullstr;
		Log.More = 0;
		LogLineCount = 0;
		doConfigure();
		parCompileEvent(NULL);
	}
}

void C3App::parIdentifierChangedEvent(Parameter* sce)
{
	Signals.VirtualTransducers.parRecompileSignal();
}

void C3App::parCompileEvent(Parameter* sce)
{
	Expression* p;
	int i, j, k;

	if (normalisationPoint)
		return;
	p = Signals.VirtualTransducers.Members.Member;
	j = 0;
	for (i = ExpNExp; i--; p++)
	{
		if (k = p->Status())
			Syslog(LogTypeBadFormula, 0, 0, p, 0);
		j |= k;
	}
	goodExpressions = j ? 0 : 1;
	combinedFlags = goodConfig && goodExpressions;
}

void C3App::parDACChangeEvent(Parameter* sce)
{
#ifndef PCTEST
	int i, j;
	MonChannel* r;
	chandef* p;

	// Wire monitor outputs
	for (i = 0, r = Signals.Monitors.Channel; i < MonNCh; i++, r++)
	{
		if (r->Settings.DAC.Enable())
		{
			j = r->Settings.DAC.DACNumber();
			if (p = chanptr(255 + j))
			{
				p->sourceptr = (int*)Smp(Monitors.channel[i].output);
				update_shared_channel_parameter(p, offsetof(chandef, sourceptr), sizeof(float*));
			}
		}
		r->pCNet = (float*)cnet_get_slotaddr(0, PROC_DSPB);
	}
#endif
}

void C3App::parClampChangedEvent(Parameter* sce)
{
	// sce==NULL means update all setup mode clamps
	int i;
	SigInputChannel* p;

	if (sce)
	{
		for (i = 0, p = Signals.Transducers.Channel; i < SigNCh; i++, p++)
			if (p->pDispCh == sce)
			{
				p->CommandClamp.Upper = (((SigInputChannel*)sce)->CommandClamp.Upper());
				p->CommandClamp.Lower = (((SigInputChannel*)sce)->CommandClamp.Lower());
			}
	}
	else
	{
		for (i = SigNCh, p = Signals.Transducers.Channel; i--; p++)
			if (p->pDispCh)
			{
				p->CommandClamp.Upper = p->pDispCh->CommandClamp.Upper();
				p->CommandClamp.Lower = p->pDispCh->CommandClamp.Lower();
			}
	}
}

void C3App::doConfigure()
{
	int i, j, k, l, m;
	char buf[32];
	SigInputChannel* p;
	GenGenBus* q;
	char contemp[SigConSz], sertemp[SigSernoSz];
	char namebuf[EPNameSz + 12];

	if (configMeta)
		return;

	// Check load limited displacement mode situation
	goodConfig = 1;
	for (i = 0; i < C3AppNChans; i++)
	{
		k = -1;
		l = -1;
		m = -1;
		j = 0;
		while (j >= 0)
		{
			j = Signals.Transducers.FindSignal(SigOuterLoopFeedback, j, i + 1);
			if (j >= 0)
			{
				if (Signals.Transducers.Channel[j].SetupParticipation() == SetupPartContr)
					k = j;
				if (Signals.Transducers.Channel[j].SetupParticipation() == SetupPartMon)
					l = j;
				if (Signals.Transducers.Channel[j].SetupParticipation() == SetupPartImpl)
					m = j;
				j++;
			}
		}
		if (((k == -1) || (l == -1)) && (m >= 0))
		{
			configMeta = true;
			p = Signals.Transducers.Channel + m;
			strncpy(contemp, p->Connector(), SigConSz);
			strncpy(sertemp, p->SerialNumber(), SigSernoSz);
			p->parNormaliseSignal();
#ifndef PCTEST
			chan_normalise_setup((chandef*)p->pExtCh);
#endif
			p->Connector = contemp;
			p->SerialNumber = sertemp;
			p->pLoadCh = NULL;
			p->pDispCh = NULL;
			p->setSetupModeLoadPointer(NULL);
			for (j = 0; j < SigNCh; j++)
				if (Signals.Transducers.Channel[j].OwningChannel() == (i + 1))
					Signals.Transducers.Channel[j].pSetupCh = NULL;
			configMeta = false;
		}
		if ((k >= 0) && (l >= 0) && (m == -1))
		{
			for (m = virtualBs; (m < SigNCh) && (m < (virtualBs + ExpNExp)); m++)
				if (Signals.Transducers.Channel[m].Enable() == 0)
				{
					configMeta = true;
					sprintf(buf, "tx(%d)+%g*tx(%d)", k + 1, C3LLFactor, l + 1);
					Signals.VirtualTransducers.Members.Member[m - virtualBs].Formula = buf;
					Signals.VirtualTransducers.Members.Member[m - virtualBs].parAlteredEvent(&(Signals.VirtualTransducers.Members.Member[m - virtualBs].Formula));
					p = Signals.Transducers.Channel + m;
					strncpy(contemp, p->Connector(), SigConSz);
					strncpy(sertemp, p->SerialNumber(), SigSernoSz);

					p->parNormaliseSignal();
#ifndef PCTEST
					chan_normalise_setup((chandef*)p->pExtCh);
#endif
					sprintf(namebuf, "%s setup mode", ServoChannel.Member[i].Controller.Name());
					p->Name = namebuf;
					p->SerialNumber = "None";
					p->Connector = contemp;
					p->SerialNumber = sertemp;
					p->Range.FullScale = Signals.Transducers.Channel[k].Range.FullScale();
					p->Range.UnipolarMode = Signals.Transducers.Channel[k].Range.UnipolarMode();
					p->Range.SRParameter = Signals.Transducers.Channel[k].Range.SRParameter();
					p->Range.Units = Signals.Transducers.Channel[k].Range.Units();
					p->parRangeChangedEvent(&p->Range);
					p->OwningChannel = (i + 1);
					p->Permission = SigOuterLoopFeedback;
					p->SetupParticipation = SetupPartImpl;
					p->Enable = 1;
					p->pLoadCh = Signals.Transducers.Channel + l;
					p->pDispCh = Signals.Transducers.Channel + k;
					p->setSetupModeLoadPointer(&smpbs->Transducers.channel[l].output);
					Signals.Transducers.Channel[k].pSetupCh = Signals.Transducers.Channel + m;
					Signals.Transducers.Channel[l].pSetupCh = Signals.Transducers.Channel + m;
					configMeta = false;
					p->doLLAutoSet();
					p->CommandClamp.Upper = Signals.Transducers.Channel[k].CommandClamp.Upper();
					p->CommandClamp.Lower = Signals.Transducers.Channel[k].CommandClamp.Lower();
					break;
				}
			if ((m >= SigNCh) || (m >= (virtualBs + ExpNExp)))
			{
				goodConfig = 0;
				Syslog(LogTypeNoSpareVTx, 0, 0, &ServoChannel.Member[i].Controller, 0);
			}
		}
		if ((k >= 0) && (l >= 0) && (m >= 0))
		{
			p = Signals.Transducers.Channel + m;
#ifndef PCTEST
			chan_normalise_setup((chandef*)p->pExtCh);
#endif
			p->pLoadCh = Signals.Transducers.Channel + l;
			p->pDispCh = Signals.Transducers.Channel + k;
			Signals.Transducers.Channel[k].pSetupCh = Signals.Transducers.Channel + m;
			Signals.Transducers.Channel[l].pSetupCh = Signals.Transducers.Channel + m;
			p->setSetupModeLoadPointer(&smpbs->Transducers.channel[l].output);
			p->Range.FullScale = Signals.Transducers.Channel[k].Range.FullScale();
			p->Range.UnipolarMode = Signals.Transducers.Channel[k].Range.UnipolarMode();
			p->Range.SRParameter = Signals.Transducers.Channel[k].Range.SRParameter();
			p->Range.Units = Signals.Transducers.Channel[k].Range.Units();
			p->SetupParticipation = SetupPartImpl;
			sprintf(namebuf, "%s setup mode", ServoChannel.Member[i].Controller.Name());
			p->Name = namebuf;
			configMeta = true;
			p->parRangeChangedEvent(&p->Range);
			configMeta = false;
		}
	}

	// Perform internal wiring
	for (i = 0; i < C3AppNChans; i++)
	{
		j = 0;
		k = 0;
		while ((k < EPNMode) && ((j = Signals.Transducers.FindSignal(SigOuterLoopFeedback, j, i + 1)) >= 0))
		{
			ServoChannel.Member[i].Controller.Mode.ModeArray[k].smpPModeFbCh = (smpbs->Transducers.channel + j);
			ServoChannel.Member[i].Controller.Mode.ModeArray[k].pModeFbCh = &Signals.Transducers.Channel[j];
			ServoChannel.Member[i].Controller.Mode.ModeArray[k].pSimExtCh = Signals.Transducers.Channel[j].pExtCh;
			ServoChannel.Member[i].Controller.Mode.ModeArray[k].ilModeFlag = 0;
			if (Signals.Transducers.Channel[j].ExternalCompensation.Enable() &&
				(Signals.Transducers.FindSignal(SigCompensationFeedback, 0, i + 1) < 0))
			{
				goodConfig = 0;
				Syslog(LogTypeNoExtCompTx, 0, 0, &Signals.Transducers.Channel[j], 0);
			}
			j++;
			k++;
		}
		if ((k == 0) && ServoChannel.Member[i].Controller.Enable())
		{
			goodConfig = 0;
			Syslog(LogTypeNoModeTx, 0, 0, &ServoChannel.Member[i].Controller, 0);
		}
		j = Signals.Transducers.FindSignal(SigInnerLoopFeedback, 0, i + 1);
		if (j >= 0)
		{
			ServoChannel.Member[i].Controller.pILCh = smpbs->Transducers.channel + j;
			ServoChannel.Member[i].Controller.InnerLoop.Enable = 1;
			if (k < EPNMode)
			{
				ServoChannel.Member[i].Controller.Mode.ModeArray[k].smpPModeFbCh = (smpbs->Transducers.channel + j);
				ServoChannel.Member[i].Controller.Mode.ModeArray[k].pModeFbCh = &Signals.Transducers.Channel[j];
				ServoChannel.Member[i].Controller.Mode.ModeArray[k].pSimExtCh = Signals.Transducers.Channel[j].pExtCh;
				ServoChannel.Member[i].Controller.Mode.ModeArray[k].ilModeFlag = 1;
			}
			k++;
		}
		else
		{
			ServoChannel.Member[i].Controller.pILCh = NULL;
			ServoChannel.Member[i].Controller.InnerLoop.Enable = 0;
		}
		j = Signals.Transducers.FindSignal(SigCascadeFeedback, 0, i + 1);
		if (j >= 0)
		{
			ServoChannel.Member[i].Controller.pCascadeCh = smpbs->Transducers.channel + j;
			ServoChannel.Member[i].Controller.CascadeEnable = 1;
		}
		else
		{
			ServoChannel.Member[i].Controller.pCascadeCh = NULL;
			ServoChannel.Member[i].Controller.CascadeEnable = 0;
		}
		while (k < EPNMode)
		{
			ServoChannel.Member[i].Controller.Mode.ModeArray[k].smpPModeFbCh = NULL;
			ServoChannel.Member[i].Controller.Mode.ModeArray[k].pModeFbCh = NULL;
			ServoChannel.Member[i].Controller.Mode.ModeArray[k].pSimExtCh = NULL;
			ServoChannel.Member[i].Controller.Mode.ModeArray[k].ilModeFlag = 0;
			k++;
		}
		j = Signals.Transducers.FindSignal(SigCompensationFeedback, 0, i + 1);
		ServoChannel.Member[i].Controller.pCompFb = (j >= 0) ? &((smpbs->Transducers.channel + j)->output) : NULL;
		j = Signals.Transducers.FindSignal(SigDeltaPFeedback, 0, i + 1);
		ServoChannel.Member[i].Controller.pDeltaPFb = (j >= 0) ? &((smpbs->Transducers.channel + j)->output) : NULL;
		j = Signals.Transducers.FindSignal(SigExternalCommand, 0, i + 1);
		if (j >= 0)
		{
			ServoChannel.Member[i].Controller.pExtCmdCh = smpbs->Transducers.channel + j;
			ServoChannel.Member[i].Controller.CommandSource.External.pCmdRange = &Signals.Transducers.Channel[j].Range;
		}
		else
		{
			ServoChannel.Member[i].Controller.pExtCmdCh = NULL;
			ServoChannel.Member[i].Controller.CommandSource.External.pCmdRange = NULL;
		}
	}
	for (i = 0; i < SigNCh; i++)
	{
		if (Signals.Transducers.Channel[i].Enable() && (Signals.Transducers.Channel[i].Permission() == SigNotConfigured))
		{
			goodConfig = 0;
			Syslog(LogTypeNotConf, 0, 0, &Signals.Transducers.Channel[i], 0);
		}
		Signals.Transducers.Channel[i].pHarmonic
			= &(smpbs->Channel[Signals.Transducers.Channel[i].OwningChannel() - 1].FunctionGenerator.harmonic);
	}

	// Genbus linkage to transducers for peak adaptation etc.
	for (i = 0; i < SigNCh; i++)
	{
		j = Signals.Transducers.Channel[i].OwningChannel() - 1;
		if (ServoChannel.Member[j].FunctionGenerator.Integration() == GenIntSC)
			q = &(smpbs->Channel[j].FunctionGenerator.genBusOut);
		else
			q = &(smpbs->Transducers.common.gb);
//		Poke(*((Uint32*)&smpbs->Transducers.channel[i].pGb), (Uint32)q);
		smpbs->Transducers.channel[i].pGb = q;
		int index = Signals.Transducers.Channel[i].chandefIndex;
		if(index >= 0)
		{
			chan_set_genbusptr(chanptr(index), (void *)q);	/* SN 002 */
		}
	}
	combinedFlags = goodConfig && goodExpressions;
}

int C3App::SafeToStart()
{
	return LimitSystem.StartInhibited ? 0 : 1;
}

int C3App::IsSetup()
{
	return LimitSystem.GetSetupState();
}

void C3App::ForceSetup()
{
	LimitSystem.ForceSetup();
}

int C3App::IsParameterSticky(Uint32 p)
{
	Uint32 v;
	Test.stickyHandle.getValue(&v, sizeof(v));
	return (p == v);
}

int C3App::ForceReturnError(void)
{
	Uint32 v;
	Test.ForceReturnError.getValue(&v, sizeof(v));
	return v;
}

void Syslog(Uint32 type, Uint32 tslo, Uint32 tshi, Parameter* p, Uint32 vio)
{


	char path[256], * pType;
	char logText[1024];
#ifndef PCTEST
	static eventlog_entry e;
#endif
	static char* (logTypeString[]) =
	{
		"Debug",
		"Limit trip",
		"Validated under-range",
		"Validated over-range",
		"Validated NaN",
		"Set while read only",
		"Set under-range",
		"Set over-range",
		"Set NaN",
		"Set while locked",
		"Powerdown",
		"Hydraulic state change",
		"Start inhibit: No ext comp Tx",
		"Start inhibit: No mode Tx",
		"Start inhibit: No inner loop Tx",
		"Start inhibit: Not configured",
		"Start inhibit: No spare virtual Tx",
		"Start inhibit: Bad formula"
	};
	pType = (char*)((type >= (sizeof(logTypeString) / sizeof(char*))) ? "Unknown" : logTypeString[type]);
	((C3App*)PSupSystem)->InstancePath(p, path, 256);
	sprintf(logText, "%s: %s (tsHi=%u tsLo=%u vi=%d vu=%u vf=%g)", pType, path, tshi, tslo, *((Sint32*)&vio), vio, *((float*)&vio));
	if (LogLineCount < C3MaxLogLines)
	{
		((C3App*)PSupSystem)->Log.LogLines.Line[LogLineCount] = logText;
		LogLineCount++;
	}
	else
		((C3App*)PSupSystem)->Log.More = 1;
#ifndef PCTEST
#ifndef FUDGE
	e.type = type;
	e.timestamp_lo = tslo;
	e.timestamp_hi = tshi;
	e.parameter1 = (Uint32)p;
	e.parameter2 = vio;
	eventlog_write(&e);
#endif
#endif
}

#ifdef PCTEST

void Trace(char* fmt, ...)
{
	va_list ap;
	static char buf[80];

	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	MessageBox(NULL, buf, "Trace", MB_OK);
	va_end(ap);
}
std::deque<eventlog_entry> events;

void event_queue_add(eventlog_entry entry) {
	events.push_back(entry);
}

bool event_queue_remove(eventlog_entry*& eptr) {
	if (!events.empty()) {

		eptr = new eventlog_entry(events.front());
		events.pop_front();
		return true;
	}
	else {
		eptr = nullptr;
		return false;

	}



}





#endif

#ifdef EXTERNALSIMULATION

// External simulation function wrappers

void SetExtSimEn(void* p, Sint32 val)
{
#ifdef PCTEST
	//	Trace("SetExtSimEn(p=0x%x, val=%d)", (unsigned int)p, val);
#else
	if (val)
		chan_modify_flags((chandef*)p, 0, 0x0100);
	else
		chan_modify_flags((chandef*)p, 0x0100, 0);
#endif
}

void SetExtSimPars(void* p, float* simOp, float pgain, float ngain)
{
#ifdef PCTEST
	//	Trace("SetExtSimPars(p=0x%x, simOp=0x%x, pgain=%g, ngain=%g)", (unsigned int)p, (unsigned int)simOp, pgain, ngain);
#else
	chan_set_simptr((chandef*)p, simOp, pgain, ngain);
#endif
}

#endif

// Static Peek/Poke functions
//
#ifndef INLINEPEEKPOKE
static bool badSmpAddress(void* p)
{
	bool res = ((C3App*)PSupSystem)->badSmp(p);
	return res;
}
#if defined(PCTEST) || defined(DEMOCUBE)
Sint32 Peek(Sint32& a) { if (badSmpAddress(&a))return 0; return a; }
Uint32 Peek(Uint32& a) { if (badSmpAddress(&a))return 0; return a; }
float Peek(float& a) { if (badSmpAddress(&a))return 0.0f; return a; }
double Peek(double& a) { if (badSmpAddress(&a))return 0.0; return a; }
float* Peek(float*& a) { if (badSmpAddress(&a))return NULL; return a; }
void Poke(Sint32& a, Sint32 d) { if (badSmpAddress(&a))return; a = d; }
void Poke(Sint32*& a, Sint32* d) { if (badSmpAddress(&a))return; a = d; }
void Poke(Uint32& a, Uint32 d) { if (badSmpAddress(&a))return; a = d; }
void Poke(Uint32*& a, Uint32* d) { if (badSmpAddress(&a))return; a = d; }
void Poke(float& a, float d) { if (badSmpAddress(&a))return; a = d; }
void Poke(float*& a, float* d) { if (badSmpAddress(&a))return; a = d; }
void Poke(double& a, double d) { if (badSmpAddress(&a))return; a = d; }
void Poke(float**& a, float** d) { if (badSmpAddress(&a))return; a = d; }
#if defined(DEMOCUBE)
bool SPTrack() { return hyd_get_status() & 0x80000000; }
#else
bool SPTrack() { return true; }
#endif
bool Dumped() { return (bool)((hyd_get_status() & HYD_STATUS_MASK) < 3); } // FUDGE Was previously 4 - probable cause of 'low pressure problem'
bool hyd_off() { return (bool)((hyd_get_status() & HYD_STATUS_MASK) < 2); }
void Dump() { hyd_unload(); }
#else
#ifdef SIGCOND
Sint32 Peek(Sint32& a) { return 0; }
Uint32 Peek(Uint32& a) { return 0; }
float Peek(float& a) { return 0.0f; }
double Peek(double& a) { return 0.0; }
float* Peek(float*& a) { return NULL; }
void Poke(Sint32& a, Sint32 d) {}
void Poke(Sint32*& a, Sint32* d) {}
void Poke(Uint32& a, Uint32 d) {}
void Poke(Uint32*& a, Uint32* d) {}
void Poke(float& a, float d) {}
void Poke(float*& a, float* d) {}
void Poke(double& a, float d) {}
void Poke(float**& a, float** d) {}
#else
Sint32 Peek(Sint32& a) { if (badSmpAddress(&a))return 0; return (Sint32)hpi_read4((Uint32*)&a); }
Uint32 Peek(Uint32& a) { if (badSmpAddress(&a))return 0; return hpi_read4((Uint32*)&a); }
float Peek(float& a) { if (badSmpAddress(&a))return 0.0f; return _itof((Sint32)hpi_read4((Uint32*)&a)); }
double Peek(double& a)
{
	double rv;

	if (badSmpAddress(&a))
		return 0.0;
	((Uint32*)&rv)[0] = hpi_read4((Uint32*)(((Uint32*)&a) + 0));
	((Uint32*)&rv)[1] = hpi_read4((Uint32*)(((Uint32*)&a) + 1));
	return rv;
}
float* Peek(float*& a) { if (badSmpAddress(&a))return NULL; return (float*)hpi_read4((Uint32*)&a); }
void Poke(Sint32& a, Sint32 d) { if (badSmpAddress(&a))return; hpi_write4((Uint32*)&a, (Uint32)d); }
void Poke(Sint32*& a, Sint32* d) { if (badSmpAddress(&a))return; hpi_write4((Uint32*)&a, (Uint32)d); }
void Poke(Uint32& a, Uint32 d) { if (badSmpAddress(&a))return; hpi_write4((Uint32*)&a, d); }
void Poke(Uint32*& a, Uint32* d) { if (badSmpAddress(&a))return; hpi_write4((Uint32*)&a, (Uint32)d); }
void Poke(float& a, float d) { if (badSmpAddress(&a))return; hpi_write4((Uint32*)&a, (Uint32)_ftoi(d)); }
void Poke(float*& a, float* d) { if (badSmpAddress(&a))return; hpi_write4((Uint32*)&a, (Uint32)d); }
void Poke(double& a, double d)
{
	Uint32* p;

	if (badSmpAddress(&a))
		return;
	p = (Uint32*)&d;
	hpi_write4((Uint32*)&a, *p++);
	hpi_write4((Uint32*)(((Uint32*)&a) + 1), *p++);
}
void Poke(float**& a, float** d) { if (badSmpAddress(&a))return; hpi_write4((Uint32*)&a, (Uint32)d); }
#endif
bool SPTrack() { return hyd_get_status() & 0x80000000; }
bool Dumped() { return (bool)((hyd_get_status() & HYD_STATUS_MASK) < 3); } // FUDGE Was previously 4 - probable cause of 'low pressure problem'
bool hyd_off() { return (bool)((hyd_get_status() & HYD_STATUS_MASK) < 2); }
void Dump() { hyd_unload(); }
#endif
#endif
#endif

#ifdef SMPGEN

#if defined(PCTEST) || defined(DEMOCUBE)
//SmpC3App SmpSystem[NDEMOCUBES];
SmpC3App* PSmpSystem;
extern "C" {
	void* BCtrlGetSmpBs() { return (void*)PSmpSystem; }
	Uint32 BCtrlGetSmpSz() { return sizeof(SmpC3App); }
	void BCtrlInit() { PSmpSystem->init(); }
	void BCtrlIter() { PSmpSystem->iter(); }
	void BCtrlIterA() { PSmpSystem->iterA(); }
	void BCtrlIterB() { PSmpSystem->iterB(); }
	void BCtrlPoll() { PSmpSystem->poll(); }
}
#else

#pragma DATA_SECTION(".onchip2")
SmpC3App SmpSystem;
extern "C" {
	void* BCtrlGetSmpBs() { return (void*)&SmpSystem; }
	Uint32 BCtrlGetSmpSz() { return sizeof(SmpC3App); }
	void BCtrlInit() { SmpSystem.init(); }

#pragma CODE_SECTION(".onchip3")
	void BCtrlIterA() { SmpSystem.iterA(); }
#pragma CODE_SECTION(".onchip3")
	void BCtrlIterB() { SmpSystem.iterB(); }

	void BCtrlPoll() { SmpSystem.poll(); }
}
#endif

void SmpC3App::init()
{
	//memset(this, 0, sizeof(SmpC3App));
}

#endif
extern "C" {
	void setupSharedArea(void* ptr)
	{
		PSmpSystem = (SmpC3App*)ptr;
	}
}
#ifdef PCTEST
void cubeNo(unsigned int n)
{
	if (n < NDEMOCUBES)
	{
		cubeNumber = n;
		PSupSystem = SupSystem + n;
		PSmpSystem = SmpSystem + n;
	}
}

void setCNetBase(Uint32* in, Uint32* out) { CNetInBase = in; CNetOutBase = out; }

#endif
