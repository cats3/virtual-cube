// Adjustment.h - Adjustment Class header

#define	AdjNCh		8

class SmpAdjustment {
public:
	typedef struct {
		float *addr;
		float rns;
		float inc;
		float last;
		Uint32 cntntn;
		Uint32 freeze;
	} Ch;
	void iter()
	{
		register Ch *p;
		register int i;
		Uint32 st, walk;
		float diff;
		bool final;

		st=0;
		walk=1;
		p=ch;
		i=AdjNCh;
		while(i--)
		{
			if(p->addr)
			{
//				if (p->freeze && pTsrIn && (*pTsrIn & (TSRSpFreezePos | TSRSpFreezeNeg))) {
//					p->cntntn=True;
//					p->addr=NULL;
//				}
//				else 
				if(p->last!=*p->addr)
				{
					p->cntntn=True;
					p->addr=NULL;
				}
				else
				{
					Uint32 mask;

					diff=p->rns-(*p->addr);
					final=True;

					mask = (diff > 0) ? TSRSpFreezePos : TSRSpFreezeNeg;

					if (p->freeze && pTsrIn && (*pTsrIn & mask)) 
					{
						p->cntntn=True;
						p->addr=NULL;
					}
					else
					{
						if((diff-p->inc)>0)
						{
							diff=p->inc;
							final=False;
						}
						if((diff+p->inc)<0)
						{
							diff=-p->inc;
							final=False;
						}
						*(p->addr)+=diff;
						p->last=*p->addr;
						if(final)
							p->addr=NULL;
					}
				}
			}
			if(!p->addr)
				st|=walk;
			walk<<=1;
			p++;
		}
		sts=st;
	}
	Uint32 sts;
	Ch ch[AdjNCh];
	Uint32 *pTsrIn;
};

#ifdef SUPGEN

class Parameter;
class AdjRequester;

class Adjustment: public Parameter {
public:
	static Adjustment *IContext;
	void operator()(Parameter *par, char *n, SmpAdjustment &smp);
	void abort(){Abort=1;chkAbrt();}
	void addme(AdjRequester *p);
	void reprogram(AdjRequester *p, Float v);
	void stop(AdjRequester *p);
	IntegerPar  SmoothAdjustment;
#ifdef KINETUSB
	EnumPar     Enable;
#endif
	EnumPar     OneAtATime;
	IntegerPar  Abort;
	UnsignedPar Waiting;
	FloatPar    GlobalRate;
	Integer     smthst;
	virtual void parPollSignal();
private:
	virtual void parSetEvent(Parameter *sce);
#ifdef KINETUSB
	virtual void parAlteredEvent(Parameter *sce);
#endif
	SmpAdjustment *smpbs;
	AdjRequester *head;
	AdjRequester *tail;
	AdjRequester *(inprogress[AdjNCh]);
	Uint32 stsmsk;
	void chkAbrt();
	void chkCmplt(Uint32 sts);
	bool feed(Uint32 sts);
	void doCallback(AdjRequester *p);
	Uint32 *pTsrIn;
};

#endif
