// Miscellaneous.c - C3 Miscellaneous support module

#include "C3Std.h"

#ifdef SUPGEN

#define	MiscMaxOrder	10

static const double BesselProtoOrd1[]=	{1.0, 1.0};
static const double BesselProtoOrd2[]=	{1.0, 1.732050807568877, 1.0};
static const double BesselProtoOrd3[]=	{1.0, 2.432880798229360, 2.466212074330470, 1.0};
static const double BesselProtoOrd4[]=	{1.0, 3.123939936920256, 4.391550328268400,
										3.201085872943679, 1.0};
static const double BesselProtoOrd5[]=	{1.0, 3.810701205349278, 6.776673715676871,
										6.886367652423632, 3.936283427035352, 1.0};
static const double BesselProtoOrd6[]=	{1.0, 4.495195101098734, 9.622275712829550,
										12.358287613066492, 9.920163202897440,
										4.671654850946757, 1.0};
static const double BesselProtoOrd7[]=	{1.0, 5.178347562106810, 12.928797389239172,
										19.925537631410585, 20.267766989109059,
										13.494026799951659, 5.407130298648435, 1.0};
static const double BesselProtoOrd8[]=	{1.0, 5.860640914005954, 16.696512740308592,
										29.899303404383783, 36.506058506668559,
										30.903730013473321, 17.608467383145676,
										6.142672879678742, 1.0};
static const double BesselProtoOrd9[]=	{1.0, 6.542351055301333, 20.925596917281212,
										42.591920334315361, 60.374447238412330,
										61.443017782760698, 44.664643581327020,
										22.263754519539262, 6.878261288583109, 1.0};
static const double BesselProtoOrd10[]=	{1.0, 7.2236471950288, 25.6161659555031,
										58.3163124745262, 93.8253040205482,
										110.9063281435781, 97.1088710551878,
										61.9489028996037, 27.4600443272215,
										7.6138823664935, 1.0};
static const double *(BesselProto[])=	{BesselProtoOrd1, BesselProtoOrd2, BesselProtoOrd3,
										BesselProtoOrd4, BesselProtoOrd5, BesselProtoOrd6,
										BesselProtoOrd7, BesselProtoOrd8, BesselProtoOrd9,
										BesselProtoOrd10};

static const double ButterProtoOrd1[]=	{1.0, 1.0};
static const double ButterProtoOrd2[]=	{1.0, 1.414213562373095, 1.0};
static const double ButterProtoOrd3[]=	{1.0, 2.0, 2.0, 1.0};
static const double ButterProtoOrd4[]=	{1.0, 2.613125929752753, 3.414213562373095,
										2.613125929752753, 1.0};
static const double ButterProtoOrd5[]=	{1.0, 3.236067977499789, 5.236067977499789,
										5.236067977499789, 3.236067977499789, 1.0};
static const double ButterProtoOrd6[]=	{1.0, 3.863703305156273, 7.464101615137754,
										9.141620172685640, 7.464101615137753,
										3.863703305156273, 1.0};
static const double ButterProtoOrd7[]=	{1.0, 4.493959207434934, 10.097834679044610,
										14.591793886479543, 14.591793886479543,
										10.097834679044610, 4.493959207434934, 1.0};
static const double ButterProtoOrd8[]=	{1.0, 5.125830895483012, 13.137071184544089,
										21.846150969207624, 25.688355931461274,
										21.846150969207628, 13.137071184544089,
										5.125830895483013, 1.0};
static const double ButterProtoOrd9[]=	{1.0, 5.758770483143633, 16.581718738763175,
										31.163437477526351, 41.986385733145895,
										41.986385733145895, 31.163437477526347,
										16.581718738763172, 5.758770483143632, 1.0};
static const double ButterProtoOrd10[]=	{1.0, 6.392453221499660, 20.431729094530695,
										42.802061068851948, 64.882396270261765,
										74.233429257077674, 64.882396270261765,
										42.802061068851948, 20.431729094530695,
										6.392453221499659, 1.0};
static const double *(ButterProto[])=	{ButterProtoOrd1, ButterProtoOrd2, ButterProtoOrd3,
										ButterProtoOrd4, ButterProtoOrd5, ButterProtoOrd6,
										ButterProtoOrd7, ButterProtoOrd8, ButterProtoOrd9,
										ButterProtoOrd10};

static const double NumProto[]={0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0};

static void PolyMap(double *dst, double *sce, int ord, double *mapnum, double *mapden, int mapord)
{
	// This function replaces s^n with mapnum^n * mapden^(ord-n)
	int scesz = ord+1;
	int dstsz = ord*mapord +1;
	int i, j, k;
	double contrib[2*MiscMaxOrder+1], temp[2*MiscMaxOrder+1];

	if((ord<1)||(ord>MiscMaxOrder)||(mapord<1)||(mapord>2))
		return;

	for(i=0; i<dstsz; i++)
		dst[i]=0.0;
	for(i=0; i<scesz; i++)
	{
		for(j=0; j<(dstsz-1); j++)
			contrib[j]=0.0;
		contrib[dstsz-1]=1.0;

		// mapden^i
		for(j=i; j--;)
		{
			for(k=0; k<dstsz; k++)
				temp[k]=mapden[2]*contrib[k];
			for(k=0; k<(dstsz-1); k++)
				temp[k]+=mapden[1]*contrib[k+1];
			if(mapord>1)
				for(k=0; k<(dstsz-2); k++)
					temp[k]+=mapden[0]*contrib[k+2];
			for(k=0; k<dstsz; k++)
				contrib[k]=temp[k];
		}

		// mapnum^(ord-i)
		for(j=ord-i; j--;)
		{
			for(k=0; k<dstsz; k++)
				temp[k]=mapnum[2]*contrib[k];
			for(k=0; k<(dstsz-1); k++)
				temp[k]+=mapnum[1]*contrib[k+1];
			if(mapord>1)
				for(k=0; k<(dstsz-2); k++)
					temp[k]+=mapnum[0]*contrib[k+2];
			for(k=0; k<dstsz; k++)
				contrib[k]=temp[k];
		}

		for(j=0; j<dstsz; j++)
			dst[j]+=sce[i]*contrib[j];
	}
}

void FilterDesign(double *numco, double *denco, int ord, Choice type, Choice pass,
							 double fc, double bwdamp, double fs, double fprwrp, bool delta)
{
	double omega, omegasqd;
	double pnum[3], pden[3];
	double snum[2*MiscMaxOrder+1], sden[2*MiscMaxOrder+1];
	double *protonum, *protoden;
	double mapnum[3], mapden[3];
	int mapord, i;

	if((ord<1)||(ord>MiscMaxOrder))
		return;

	switch(type)
	{
	case FDClassicOrder1:
		pnum[0]=0.0;
		pnum[1]=1.0;
		pden[0]=1.0;
		pden[1]=1.0;
		protonum=pnum;
		protoden=pden;
		ord=1;
		break;
	case FDClassicOrder2:
		pnum[0]=0.0;
		pnum[1]=0.0;
		pnum[2]=1.0;
		pden[0]=1.0;
		pden[1]=2.0*bwdamp;
		pden[2]=1.0;
		protonum=pnum;
		protoden=pden;
		ord=2;
		break;
	case FDBessel:
		protonum=(double *)NumProto+MiscMaxOrder-ord;
		protoden=(double *)BesselProto[ord-1];
		break;
	case FDButterworth:
		protonum=(double *)NumProto+MiscMaxOrder-ord;
		protoden=(double *)ButterProto[ord-1];
		break;
	}

	omega=2.0*Pi*fc;
	omegasqd=omega*omega;
	bwdamp*=2.0*Pi;
	switch(pass)
	{
	case FDLowPass:
		// Map s -> s/omega
		mapnum[0]=0.0;
		mapnum[1]=1.0;
		mapnum[2]=0.0;
		mapden[0]=0.0;
		mapden[1]=0.0;
		mapden[2]=omega;
		mapord=1;
		break;
	case FDHighPass:
		// Map s -> omega/s
		mapnum[0]=0.0;
		mapnum[1]=0.0;
		mapnum[2]=omega;
		mapden[0]=0.0;
		mapden[1]=1.0;
		mapden[2]=0.0;
		mapord=1;
		break;
	case FDBandPass:
		// Map s -> (s^2 + omega^2) / fb.s
		mapnum[0]=1.0;
		mapnum[1]=0.0;
		mapnum[2]=omegasqd;
		mapden[0]=0.0;
		mapden[1]=bwdamp;
		mapden[2]=0.0;
		mapord=2;
		break;
	case FDBandStop:
		// Map s -> fb.s / (s^2 + omega^2)
		mapnum[0]=0.0;
		mapnum[1]=bwdamp;
		mapnum[2]=0.0;
		mapden[0]=1.0;
		mapden[1]=0.0;
		mapden[2]=omegasqd;
		mapord=2;
		break;
	}
	PolyMap(snum, protonum, ord, mapnum, mapden, mapord);
	PolyMap(sden, protoden, ord, mapnum, mapden, mapord);
	ord*=mapord;

	if(fprwrp>0.0)
	{
		omega=Pi*fprwrp;
		fs=omega/tan(omega/fs);
	}

	if(delta)
	{
		// s -> (2/T) * d / (d+2)
		mapnum[1]=2.0*fs;
		mapnum[2]=0.0;
		mapden[1]=1.0;
		mapden[2]=2.0;
	}
	else
	{
		// s -> (2/T) * (z-1) / (z+1)
		mapnum[1]=2.0*fs;
		mapnum[2]=-2.0*fs;
		mapden[1]=1.0;
		mapden[2]=1.0;
	}
	PolyMap(numco, snum, ord, mapnum, mapden, 1);
	PolyMap(denco, sden, ord, mapnum, mapden, 1);

	omega=1.0/denco[0];
	for(i=0; i<=ord; i++)
	{
		numco[i]*=omega;
		denco[i]*=omega;
	}
}

static double HMSquare(Uint32 harm, float tor)
{
	double x=(double)harm;
	return 1.0/x;
}
static double HMTriangle(Uint32 harm, float tor)
{
	double x=(double)harm;
//	x=1.0/(x*x);
//	if((harm&3)==3)
//		x=-x;
//	return x;
	return 1.0/(x*x);
}
static double HMSawtooth(Uint32 harm, float tor)
{
	double x=(double)harm;
//	x=1.0/x;
//	if((harm&1)==0)
//		x=-x;
//	return x;
	return 1.0/x;
}
static double HMTrapezoid(Uint32 harm, float tor)
{
	double x=(double)harm;
	return sin(x*(double)tor)/(x*x);
}
#define	FOLDS			128 

void StdWaveSyn(float *tbl, Uint32 tblsz, Choice shape, Uint16 maxharm, bool bndlim, Uint16 sinccomp, float tor)
{
	double sfac, tfac, scomp, cnstmlt, x;
	register float *p;
	register Uint32 i, harm, harmend, harminc;
	double (*harmmltfn)(Uint32 harm, float tor);

	if((tblsz<1)||(tblsz>65536)||(maxharm<1))
		return;

	if((shape==SWTrapezoid)&&(tor<=0.0))
		shape=SWSquare;
	if((shape==SWTrapezoid)&&(tor>=(Pi/2.0)))
		shape=SWTriangle;

	// Determine harmonic range: We will do harmonics 1..hend-1
	//  If table is even size, we could potentially include harmonic tblsz/2,
	//  but we don't because we only synthesise with sin() at present, and
	//  harmonic tblsz/2 always yields zero sine component for even table size.

	harmend=(tblsz+1)/2;
	if(harmend>(Uint32)(maxharm+1))
		harmend=(Uint32)(maxharm+1);

	sfac=Pi/((double)tblsz);
	tfac=2.0*Pi/((double)tblsz);
	scomp=1.0;
	if(shape==SWSine)
	{
		if(sinccomp>0)
		{
			x=sfac/sin(sfac);
			if(sinccomp>1)
				scomp=x*x;
			else
				scomp=x;
		}
		p=tbl;
		for(i=0; i<tblsz; i++)
			*p++=(float)(scomp*sin(tfac*(double)i));
		return;
	}
	switch(shape)
	{
	case SWSquare:
		cnstmlt=4.0/Pi;
		harmmltfn=&HMSquare;
		harminc=2;
		break;
	case SWTriangle:
		cnstmlt=8.0/(Pi*Pi);
		harmmltfn=&HMTriangle;
		harminc=2;
		break;
	case SWSawtooth:
		cnstmlt=2.0/Pi;
		harmmltfn=&HMSawtooth;
		harminc=1;
		break;
	case SWTrapezoid:
		cnstmlt=4.0/(Pi*tor);
		harmmltfn=&HMTrapezoid;
		harminc=2;
		break;
	default:
		return;
	}
	memset(tbl, 0, tblsz*sizeof(float));
	for(harm=1; harm<harmend; harm+=harminc)
	{
		if(sinccomp>0)
		{
			x=sfac*(double)harm;
			x=x/sin(x);
			if(sinccomp>1)
				scomp=x*x;
			else
				scomp=x;
		}
		x=(*harmmltfn)(harm, tor);
		if(!bndlim)
			for(i=1; i<=FOLDS; i++)
			{
				x+=(*harmmltfn)(i*tblsz+harm, tor);
				x-=(*harmmltfn)(i*tblsz-harm, tor);
			}

		// This implements commented out code in HM... functions
		switch(shape)
		{
		case SWTriangle:
			if((harm&3)==3)
				x=-x;
			break;
		case SWSawtooth:
			if((harm&1)==0)
				x=-x;
			break;
		}

		p=tbl;
		for(i=0; i<tblsz; i++)
			(*p++)+=(float)(cnstmlt*x*scomp*sin(tfac*(double)(i*harm)));
	}

//#ifdef PCTEST
//	FILE *fp=fopen("Wavetable.txt", "w");
//	if(fp)
//	{
//		for(i=0; i<tblsz; i++)
//			fprintf(fp, "%f\n", tbl[i]);
//		fclose(fp);
//	}
//#endif
}

void SplinePoly(float p[4], float y0, float y1, float d0, float d1)
{
	p[0]=y0;
	p[1]=d0;
	p[2]=3.0f*(y1-y0)-2.0f*d0-d1;
	p[3]=2.0f*(y0-y1)+d0+d1;
}

void SignalRange::operator()(Parameter *par, char *n, char *f)
{
	static const char *(ScaleStr[])={"Bipolar", "Unipolar", NULL};
	static const char *(ParStr[])  ={"Acceleration", "Angle", "Displacement", "Load",
										"Pressure", "Strain", "Temperature", "Torque", "Setup_legacy",
										"Spool_displacement", "Unassigned", "User_defined", "Stress", "Extension", "Servo_valve_drive",
										"Following_error", "User_defined_1", "User_defined_2", "User_defined_3", "User_defined_4",
										"User_defined_5", "User_defined_6", "User_defined_7", "User_defined_8", "User_defined_9",
										"User_Defined_10", NULL};

	Parameter::operator()(par, n);
	this->setFlags("g");
	strncpy(flagbufa, f, 27);
	strcat(flagbufa, "oz");
	strncpy(flagbufb, f, 27);
	strcat(flagbufb, "ocz");
	FullScale   (this, "Full_scale",    flagbufa, NULL, 0.01f, MaxFloat, "", 100.0f);
	UnipolarMode(this, "Unipolar_mode", flagbufa, NULL, (char **)ScaleStr);
	SRParameter (this, "Parameter",     flagbufb, NULL, (char **)ParStr);
	Units       (this, "Units",         flagbufa, MiscUnitSz, "%");
  Broadcast   (this, "Broadcast",       "b",  NULL,       0,    1); 
}

void SignalRange::parPrimeSignal()
{
	Parameter::parPrimeSignal();
//		parRangeChangedEvent(this);
}

void SignalRange::parAlteredEvent(Parameter *sce)
{
	Parameter::parRangeChangedEvent(this);
	Parameter::parAlteredEvent(sce);
}

void SignalRange::parSetEvent(Parameter *sce)
{
	Parameter::parRangeChangedEvent(this);
	Parameter::parSetEvent(sce);
}
#endif

#ifdef SMPGEN
#ifndef INLINEFILTER

// IIR filter
float Filter(float *num, float *den, float *st, int ord, float ip)
{
    register float op, *r;
    register int i;

    r=st;
    op=r[0]+(*num++)*ip;
	den++;
    i=ord-1;
    while(i--)
    {
        r[0]=r[1]+(*num++)*ip-(*den++)*op;
		r++;
    }
    r[0]=(*num)*ip-(*den)*op;
    return op;
}

// IIR delta filter
float DeltaFilter(float *num, float *den, double *st, int ord, float ip)
{
    register float op;
	register double *r;
    register int i;

    r=st;
    op=(float)r[0]+(*num++)*ip;
	den++;
    i=ord-1;
    while(i--)
    {
    	r[0]+=(r[1]+(double)((*num++)*ip-(*den++)*op));
		r++;
    }
    r[0]+=(double)((*num)*ip-(*den)*op);
    return op;
}

#endif
#endif
