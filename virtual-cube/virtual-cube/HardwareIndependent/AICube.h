// AICube.h - Control cube application header

#define	C3TransPathSz			256
#define	C3NormalisationBlock	100
#ifdef FUDGE
#define	C3MaxLogLines			32
#else
#define	C3MaxLogLines			10
#endif

#ifdef __cplusplus
#ifndef PCTEST
extern "C" {
extern volatile Uint32 tracking_request;
}
#endif

class SmpC3Channel {
public:
	SmpGenerator  FunctionGenerator;
	SmpErrorPath  Controller;
};

class SmpC3App {
public:
	void init();
	void iter()
	{
		int i;

		LimitSystem.iterBegin();
		Transducers.iterBegin();
		Transducers.iterChans(0, SigNCh-ExpNExp);
		VirtualTransducers.iter(0);
		VTBGUFlag=1;
		Transducers.iterChans(SigNCh-ExpNExp, ExpNExp);
		for(i=0; i<C3AppNChans; i++)
		{
			Channel[i].FunctionGenerator.iter(i);
			Channel[i].Controller.iter();
		}
#ifndef PCTEST
		if(tracking_request)
			tracking_request--;
#endif
		AdjustmentMechanism.iter();
		unanimityIter();
		Monitors.iter();
		DigitalInputs.iter();
		DigitalOutputs.iter();
		Regressor.iter();
		LimitSystem.iterEnd();
		LinearAnalyser.iter();
	}
	void iterA()
	{
		LimitSystem.iterBegin();
		Transducers.iterBegin();
		if(ACnt)
			Transducers.iterChans(0, ACnt);
		VirtualTransducers.iter(0);
		VTBGUFlag=1;
	}
	void iterB()
	{
		int i;
		int CubeHasMaster = false;

		for(i=0; i<C3AppNChans; i++)
		{
			CubeHasMaster |= (Channel[i].FunctionGenerator.integration==GenIntMCMaster);
		}

		if(BCnt)
			Transducers.iterChans(ACnt, BCnt);
		for(i=0; i<C3AppNChans; i++)
		{
			Channel[i].FunctionGenerator.CubeHasMaster = CubeHasMaster;
			Channel[i].FunctionGenerator.iter(i);
			Channel[i].Controller.iter();
		}
#ifndef PCTEST
		if(tracking_request)
			tracking_request--;
#endif
		AdjustmentMechanism.iter();
		unanimityIter();
		Monitors.iter();
		DigitalInputs.iter();
		DigitalOutputs.iter();
		Regressor.iter();
		LimitSystem.iterEnd();
		LinearAnalyser.iter();
	}
	void poll()
	{
		if(VTBGUFlag)
		{
			VTBGUFlag=0;
			VirtualTransducers.iter(1);
		}
	}
	void unanimityIter()
	{
		Unanimity.iter();
		if(pTSRin)
		{
			if((*pTSRin)&TSRUnanimity)
				unanCnt=0;
			else
				unanCnt++;
		}
		if(unanCnt>unanPer)
			unanCnt=unanPer;
		Unanimity.update(unanCnt>=unanPer, 0.0f);
	}
	int ACnt, BCnt, VTBGUFlag;
	Uint32 *pTSRin;
	Uint32 unanPer, unanCnt;
	SmpSignal          Transducers;
	SmpExpressionArray VirtualTransducers;
	SmpMonitor         Monitors;
	SmpDiginArray      DigitalInputs;
	SmpDigoutArray     DigitalOutputs;
	SmpAdjustment      AdjustmentMechanism;
	SmpLimitSys        LimitSystem;
	SmpC3Channel       Channel[C3AppNChans];
	SmpLimit           Unanimity;
	SmpAnalyser        LinearAnalyser;
	SmpRegression      Regressor;
};

#ifdef SUPGEN

class C3SignalsClass: public Parameter {
public:
	SignalArray	      Transducers;
	ExpressionArray   VirtualTransducers;
	MonitorArray      Monitors;
	DiginArray        DigitalInputs;
	DigoutArray       DigitalOutputs;
};

class C3ChannelClass: public Parameter {
public:
	Generator   FunctionGenerator;
	ErrorPath   Controller;
};

class C3ServoChClass: public Parameter {
public:
	C3ChannelClass Member[C3AppNChans];
};

class C3LogArrayClass: public Parameter {
public:
	TextPar Line[C3MaxLogLines];
	char logName[C3MaxLogLines][4];
};

class C3TranslationClass: public Parameter {
public:
	UnsignedPar Handle;
	TextPar     Path;
};

class C3LogClass: public Parameter {
public:
	C3LogArrayClass    LogLines;
	IntegerPar         More;
	IntegerPar         Reset;
	IntegerPar         Reappraise;
	C3TranslationClass Translation;
};

class C3NormalisationClass: public Parameter {
public:
	IntegerPar Inhibited;
	IntegerPar Start;
	FloatPar   Progress;
	IntegerPar LeafCount;
	IntegerPar SNVSize;
	IntegerPar DNVSize;
	IntegerPar Validate;
	IntegerPar NVAll;
};

class C3TestClass: public Parameter {
public:
	UnsignedPar TxSupSz;
	UnsignedPar TxSmpSz;
};

class C3UnanimityClass: public Parameter {
public:
	FloatPar   Period;
	Limit      ULimit;
};

class C3App: public Parameter {
public:
	void setSnvBs(void *p)
	{
		formatSNV(p);
		sizeSNV(0);
		snvEnd=(char *)p+snvSz-1;
	}
	void setDnvBs(void *p)
	{
		formatDNV(p);
		sizeDNV(0);
		dnvEnd=(char *)p+dnvSz-1;
	}
	void setSmpBs(Uint32 p)
	{
		smpBs=(char *)p;
		smpSz=sizeof(SmpC3App);
		smpEnd=smpBs+sizeof(SmpC3App)-1;
		smpbs=(SmpC3App *)p;
	}
	bool badSnv(void *p){return (((char *)p)<snvBs)||(((char *)p)>snvEnd);}
	bool badDnv(void *p){return (((char *)p)<dnvBs)||(((char *)p)>dnvEnd);}
	bool badSmp(void *p){return (((char *)p)<smpBs)||(((char *)p)>smpEnd);}
	void init();
	int SafeToStart();
	int IsSetup();
	void ForceSetup();
	virtual void parPrimeSignal();
	virtual void parConfigureSignal();
	virtual void parPollSignal();
	void parClampChangedEvent(Parameter *sce);
	IntegerPar           CubeNumber;
	C3SignalsClass       Signals;
	Adjustment           AdjustmentFeeder;
	LimitSys             LimitSystem;
	CubicEngine			 CubicExecutive;
	C3ServoChClass       ServoChannel;
	C3NormalisationClass Normalisation;
	C3LogClass           Log;
	C3UnanimityClass     Unanimity;
	Analyser             LinearAnalyser;
	Regression           Regressor;
	C3TestClass          Test;
	int goodConfig, goodExpressions, combinedFlags;
#ifdef FASTPOLL
	void doFastPoll();
#endif
private:
	virtual void parAlteredEvent(Parameter *sce);
	virtual void parSNVValidateUREvent(Parameter *sce);
	virtual void parSNVValidateOREvent(Parameter *sce);
	virtual void parSNVValidateNANEvent(Parameter *sce);
	virtual void parDNVValidateUREvent(Parameter *sce);
	virtual void parDNVValidateOREvent(Parameter *sce);
	virtual void parDNVValidateNANEvent(Parameter *sce);
	virtual void parSNVModifiedEvent(Parameter *sce);
	virtual void parDNVModifiedEvent(Parameter *sce);
	virtual void parSetROEvent(Parameter *sce);
	virtual void parSetUREvent(Parameter *sce);
	virtual void parSetOREvent(Parameter *sce);
	virtual void parSetNanEvent(Parameter *sce);
	virtual void parSetLockEvent(Parameter *sce);
	virtual void parConfigChangedEvent(Parameter *sce);
	virtual void parSetEvent(Parameter *sce);
	virtual void parIdentifierChangedEvent(Parameter *sce);
	virtual void parCompileEvent(Parameter *sce);
	virtual void parDACChangeEvent(Parameter *sce);
	void doConfigure();
	void countLeafParameters();
	void startNormalisation();
	void doSomeNormalisation();
	SmpC3App  *smpbs;
	char      chName[C3AppNChans][4];
	char      *snvEnd, *dnvEnd, *smpBs, *smpEnd;
	Uint32	  smpSz;
	bool      configMeta;
	int       virtualBs;
	int       nLeaves;
	Parameter *normalisationPoint;
	int       normalisationCount;
	Parameter *fastChangePollList;
	Uint32    amready;
	CubInstruction cubicProgram[CubPrgrmSz];
};

#endif
#endif
