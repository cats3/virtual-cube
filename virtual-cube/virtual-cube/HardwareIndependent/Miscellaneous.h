// Miscellaneous.h - C3 Miscellaneous support header file


// Filters
#ifdef INLINEFILTER
// IIR filter
inline float Filter(float *num, float *den, float *st, int ord, float ip)
{
    register float op, *r;
    register int i;

    r=st;
    op=r[0]+(*num++)*ip;
	den++;
    i=ord-1;
    while(i--)
	{
        r[0]=r[1]+(*num++)*ip-(*den++)*op;
		r++;
	}
    r[0]=(*num)*ip-(*den)*op;
    return op;
}
// IIR delta filter
inline float DeltaFilter(float *num, float *den, double *st, int ord, float ip)
{
    register float op;
	register double *r;
    register int i;

    r=st;
    op=(float)r[0]+(*num++)*ip;
	den++;
    i=ord-1;
    while(i--)
    {
    	r[0]+=(r[1]+(double)((*num++)*ip-(*den++)*op));
		r++;
	}
    r[0]+=(double)((*num)*ip-(*den)*op);
    return op;
}
#else
float Filter(float *num, float *den, float *st, int ord, float ip);
float DeltaFilter(float *num, float *den, double *st, int ord, float ip);
#endif

#ifdef SUPGEN

inline void PokeCoeffs(float *dst, double *sce, int ord)
{
	int i;

	for(i=0; i<=ord; i++)
		Poke(dst[i], (float)(sce[i]));
}

enum {SWSine=0,				// Waveshapes
	  SWSquare,
	  SWTriangle,
	  SWSawtooth,
	  SWTrapezoid,
	  SWCustom,

	  FDClassicOrder1=0,	// Filter types
	  FDClassicOrder2,
	  FDButterworth,
	  FDBessel,

	  FDLowPass=0,			// Pass types
	  FDHighPass,
	  FDBandPass,
	  FDBandStop
};

void FilterDesign(double *numco, double *denco, int ord, Choice type, Choice pass, double fc,
					double bwdamp, double fs, double fprwrp, bool delta);

void StdWaveSyn(float *tbl, Uint32 tblsz, Choice shape, Uint16 maxharm, bool bndlim, Uint16 sinccomp,
				float tor=0.924);

void SplinePoly(float p[4], float y0, float y1, float d0, float d1);

#define	MiscUnitSz		16
enum {
	RangeParAccel=0,
	RangeParAngle,
	RangeParDisp,
	RangeParLoad,
	RangeParPress,
	RangeParStrain,
	RangeParTemp,
	RangeParTorque,
	RangeParSetupLegacy,
	RangeParSpool,
	RangeParUnass,
	RangeParUser
};

class SignalRange: public Parameter {
public:
	void operator()(Parameter *par, char *n, char *f);
	FloatPar   FullScale;
	EnumPar    UnipolarMode;
	EnumPar    SRParameter;
	TextPar    Units;
  IntegerPar Broadcast;
private:
	virtual void parAlteredEvent(Parameter *sce);
	virtual void parSetEvent(Parameter *sce);
	virtual void parPrimeSignal();
	void checkRange();
	char flagbufa[32], flagbufb[32];
};

#endif

