// AdjustRequest.h - AdjustRequest Class header

#ifdef SUPGEN

class Parameter;
class Adjustment;
enum State {AdjIdle,
			AdjWaiting,
			AdjExecuting,
			AdjAborting,
			AdjCancelled
};
#define	AdjDfltAdjRt	0.2		// 10 seconds to traverse -1..+1
#define	AdjDfltAbrtRt	2.0		// 1 second to traverse -1..+1

class AdjRequester {
	friend class Adjustment;
public:
	void init(Parameter *ownr, float *a);
	void request(Uint32 frz, Float v, float ar=AdjDfltAdjRt, float abrtr=AdjDfltAbrtRt, float rateCtrl=0.0f);
	Float getfinalv(){return finalv;}
	bool busy(){return state!=AdjIdle;}
	float getTarget(){return newv;}
private:
	Adjustment *mechanism;
	float *addr;
	State state;
	float adjrate;
	float abrtrate;
	Float newv;
	Float oldv;
	Float finalv;
	Parameter *owner;
	int chno;
	Uint32 freeze;
	AdjRequester *next;
};

#endif
