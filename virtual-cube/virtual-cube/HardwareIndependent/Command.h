// Command.h - Command Class header

#ifdef SUPGEN

enum {Pathsz=16, Cmdsz=128, Rspsz=1024, Hiwater=800};

class Command
{
public:
	void init(Parameter *root, char eoc, char *eor, bool lp);
	bool cmd(char *txt, int cnt);
	int rsp(char *txt, int cnt);
	char *rspStr();
	void dump(char* fn);	/* SJE EDIT */
	void dumpbranch(Parameter* p, void* vfp, int indent);
private:
	void rspPrintf(char *fmt, ...);
	void rspEol(){rspPrintf("%s", eorsp);}
	void help();
	void displayPath();
	void displayType(Parameter *p, bool showM=False);
	void displayValue(Parameter *p);
	bool assignment(char *t);
	bool developPath(char *t);
	void issuePrompt();
	void processCommand();
	void checkAssignable();
	void list();
	void BSEdit();
	//void dump(char* fn);	/* SJE EDIT */
	//void dumpbranch(Parameter *p, void *vfp, int indent);
	char eocmd;
	char *eorsp;
	bool lngprmpt;
	Parameter *(path[Pathsz]);
	Parameter *current;
	int pathcnt;
	bool assgnbl;
	char cmdbuf[Cmdsz];
	int cmdcnt;
	char rspbuf[Rspsz];
	char *rspptr;
	int rspcnt;
	char buf[80];
};

#endif


