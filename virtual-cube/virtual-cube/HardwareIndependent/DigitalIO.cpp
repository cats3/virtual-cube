// DigitalIO.cpp - DigitalIO Class module

#include "C3Std.h"

#ifdef SUPGEN

extern "C" {
extern void set_digital_output(Sint32 index, Uint32 state);
extern Uint32 get_digital_input(Sint32 index);
}

void DiginChannel::operator()(Parameter *par, int ind, SmpDiginChannel &smp)
{
	static const char *(AbleStr[])={"Disabled", "Enabled", NULL};
	static const char *(PhaseStr[]) ={"Normal", "Inverted", NULL};

	smpbs=&smp;
	sprintf(myName, "%d", ind+1);
	Parameter::operator()(par, myName);
	Name      (this, "Name",     "npy", DigNameSz, "None");
	Connector (this, "Connector", "r",  DigConSz,  "None");
	Enable    (this, "Enable",   "npy", NULL, (char **)AbleStr);
	Index     (this, "Index",    "npy", NULL);
	Polarity  (this, "Polarity", "npy", NULL, (char **)PhaseStr);
	DiginLimit(this, "Limit",     smpbs->diginLimit);
	chandefindex=-1;
}

void DiginChannel::parPrimeSignal()
{
	Uint32 st;

	Parameter::parPrimeSignal();
	st=(chandefindex>=0)?get_digital_input(chandefindex):0;
	Poke(smpbs->dinstate, Polarity()?(!st):st);
}

void DiginChannel::parPollSignal()
{
	Uint32 st;

	Parameter::parPollSignal();
	st=(chandefindex>=0)?get_digital_input(chandefindex):0;
	Poke(smpbs->dinstate, Polarity()?(!st):st);
}

void DiginChannel::parAlteredEvent(Parameter *sce)
{
	Uint32 st;

	if(sce==&Polarity)
	{
		st=(chandefindex>=0)?get_digital_input(chandefindex):0;
		Poke(smpbs->dinstate, Polarity()?(!st):st);
	}
}

void DiginArray::operator()(Parameter *par, char *n, SmpDiginArray &smp)
{
	DiginChannel *p;
	int i;

	smpbs=&smp;
	Parameter::operator()(par, n);
	for(i=0, p=Channel; i<DigNDigin; i++, p++)
		(*p)(this, i, smpbs->channel[i]);
}

void DigoutChannel::operator()(Parameter *par, int ind, SmpDigoutChannel &smp)
{
	static const char *(AbleStr[])={"Disabled", "Enabled", NULL};
	static const char *(PhaseStr[]) ={"Normal", "Inverted", NULL};
	static const char *(AssertStr[]) ={"Deassert", "Assert", NULL};

	smpbs=&smp;
	sprintf(myName, "%d", ind+1);
	Parameter::operator()(par, myName);
	Name     (this, "Name",      "npy", DigNameSz, "None");
	Connector(this, "Connector", "r",   DigConSz,  "None");
	Enable   (this, "Enable",    "npy", NULL, (char **)AbleStr);
	Index    (this, "Index",     "npy", NULL);
	Polarity (this, "Polarity",  "npy", NULL, (char **)PhaseStr);
	State    (this, "State",     "",    NULL, (char **)AssertStr);
	chandefindex=-1;
}

void DigoutChannel::parPrimeSignal()
{
	Uint32 st;

	Parameter::parPrimeSignal();
	if(chandefindex>=0)
	{
		st=(Uint32)State();
		set_digital_output(chandefindex, Polarity()?(!st):st);
	}
}

void DigoutChannel::parAlteredEvent(Parameter *sce)
{
	Uint32 st;

	if((sce==&State)||(sce==&Polarity))
		if(chandefindex>=0)
		{
			st=(Uint32)State();
			set_digital_output(chandefindex, Polarity()?(!st):st);
		}
}

void DigoutArray::operator()(Parameter *par, char *n, SmpDigoutArray &smp)
{
	DigoutChannel *p;
	int i;

	smpbs=&smp;
	Parameter::operator()(par, n);
	for(i=0, p=Channel; i<DigNDigout; i++, p++)
		(*p)(this, i, smpbs->channel[i]);
}

#endif
