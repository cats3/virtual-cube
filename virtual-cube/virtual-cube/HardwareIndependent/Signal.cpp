// Signal.cpp - Signal Class module

#include "C3Std.h"

#ifdef SUPGEN

#ifdef EXTERNALSIMULATION
void SetExtSimEn(void *p, Sint32 val);
#endif

extern "C" {
#ifndef PCTEST
#include "../include/channel.h"
#endif
extern void informRangeChange(int chandefIndex, float range, char *units);
#ifdef PCTEST
void informEnableChange(int chandefIndex, int enable)
{
}
#else
void informEnableChange(int chandefIndex, int enable);
#endif
}

void SigInputChannel::operator()(Parameter *par, int nCh, int ind, SmpSigInputChannel &smp)
{
	Parameter *p;
	SigThresholdLimitClass *q;
	SmpSigThrLimit *s;
	SigPeakTroughLimitClass *t;
	SmpSigPTLimit *u;
	SigErrorLimitClass *v;
	SmpSigErrLimit *w;
	static const char *(AbleStr[])={"Disabled", "Enabled", NULL};
	static const char *(SourceStr[]) ={"Hardware", "CNet", NULL};
	static const char *(ModeStr[])  ={"Outer_loop", "Inner_loop", NULL};
	static const char *(PhaseStr[]) ={"Normal", "Inverted", NULL};
	static const char *(PermStr[])={"Not_configured", "Monitor_only", "Outer_loop_feedback", "Inner_loop_feedback",
									"Compensation_feedback", "External_command", "Delta_P_feedback", "Adaptation_feedback", "Cascade_feedback", NULL};
	static const char *(UnanimityStr[])={"Disabled", "Veto", NULL};
	static const char *(SetupPartStr[])={"None", "Controlled", "Monitored", "Implementation", NULL};
	static const char *(CountStr[])={"None", "Peaks", "Troughs", NULL};
	static const char *(OrderStr[])={"Simultaneous", "Mean_first", NULL};
	static const char *(VioModeStr[])={"Ignore", "Peaks", "Troughs", "Either", NULL};
	static const char *(WhenStr[])={"Valid_cycles", "Always", NULL};
	static const char *(MeanModeStr[])={"Both", "Peaks", "Troughs", NULL};

	smpbs=&smp;
	sprintf(myName, "%d", ind+1);
	Parameter::operator()(par, myName);
	Name         (this, "Name",           "npy", SigNameSz,    "None");
	SerialNumber (this, "Serial_number",  "npy", SigSernoSz);
	Connector    (this, "Connector",      "ny",  SigConSz);
	Symbol       (this, "Symbol",         "ny",  SigSymSz);

	Source         (this, "Source");
	Source.setFlags("g");
	Source.Source  (&Source, "Source",    "ncpy", Smp(cNetSce), (char **)SourceStr);
	Source.CNetSlot(&Source, "CNet_slot", "ny",  Smp(cNetSlot), 0, C3NTS-1, "", 0);

	FBSelected        (this, "FB_Selected",         "dr",     Smp(fbEn),    (char **)AbleStr);
	Range             (this, "Range",               "ny");
	Reading           (this, "Reading",             "mdrv",   Smp(reading), MinFloat, MaxFloat);
	Noise             (this, "Noise_level",         "r",      NULL,         MinFloat, MaxFloat, "% rms");
	Enable            (this, "Enable",              "inz",    Smp(enable),  (char **)AbleStr); //(changed by Paul) "inpz" to "inz". This enables a tx to be enabled while pressure is on. (requested by iain).
	Permission        (this, "Permission",          "ncpyz",  NULL,         (char **)PermStr);
	OwningChannel     (this, "Owning_channel",      "npyz",   Smp(ownChan), 1, nCh, "", 1);
	SetupParticipation(this, "Setup_participation", "ncpyz",  NULL,         (char **)SetupPartStr);
	CascadeEnable     (this, "Cascade_enable",      "npz",    Smp(casen),   (char **)AbleStr);
	SetPoint          (this, "Set_point",           "smdtvf", Smp(setPnt),  MinFloat, MaxFloat);

	p=(Parameter *)&ExternalCompensation;
	(*p)                                  (this, "External_Compensation");
	#ifndef AICUBE
		ExternalCompensation.Enable           (p, "Enable",             "npyz", Smp(ecEn),       (char **)AbleStr);
		ExternalCompensation.Mode             (p, "Mode",               "npy",  Smp(ecMode),     (char **)ModeStr);
		ExternalCompensation.Phasing          (p, "Phasing",            "npy",  Smp(ecPhasing),  (char **)PhaseStr);
		ExternalCompensation.ProportionalGain (p, "Proportional_gain",  "isny", Smp(ecPropGain), SigMinECPGPC, 100.0f,  "%", SigNormPGPC);
		ExternalCompensation.ProportionalGain.setMap(0.16f);
		ExternalCompensation.DerivativeGain   (p, "Derivative_gain",    "isny", Smp(ecDerGain),  0.0f,       100.0f,  "%");
		ExternalCompensation.DerivativeGain.setMap(0.08f);
		ExternalCompensation.DerivativePlateau(p, "Derivative_plateau", "npy",  NULL,            10.0f,      1000.0f, "Hz",      100.0f);
	#else
		ExternalCompensation.Enable           (p, "Enable",             "npyz", NULL,       (char **)AbleStr);
		ExternalCompensation.Mode             (p, "Mode",               "npy",  NULL,     (char **)ModeStr);
		ExternalCompensation.Phasing          (p, "Phasing",            "npy",  NULL,  (char **)PhaseStr);
		ExternalCompensation.ProportionalGain (p, "Proportional_gain",  "isny", NULL, SigMinECPGPC, 100.0f,  "%", SigNormPGPC);
		ExternalCompensation.ProportionalGain.setMap(0.16f);
		ExternalCompensation.DerivativeGain   (p, "Derivative_gain",    "isny", NULL,  0.0f,       100.0f,  "%");
		ExternalCompensation.DerivativeGain.setMap(0.08f);
		ExternalCompensation.DerivativePlateau(p, "Derivative_plateau", "npy",  NULL,            10.0f,      1000.0f, "Hz",      100.0f);
	#endif

	p=(Parameter *)&Terms;
	(*p)                   (this, "Terms");
	
	#ifndef AICUBE
		Terms.ErrorFilterEnable(p, "Error_filter_enable",  "ny", Smp(errfiltEnable),     (char **)AbleStr, 1);
		Terms.ErrorFilter      (p, "Error_filter",        "npy", NULL,           10.0f,      1000.0f, "Hz", 100.0f);
		Terms.ServoGain        (p, "Servo_gain",         "isny", Smp(servoGain), SigMinSGPC, 100.0f,  "%", SigNormSGPC);
		Terms.ServoGain.setMap(0.32f);
		Terms.ProportionalGain (p, "Proportional_gain",  "dsn",  Smp(propGain) , SigMinPGPC, 100.0f,  "%", SigNormPGPC);
		Terms.ProportionalGain.setMap(0.16f);
		Terms.IntegralGain     (p, "Integral_gain",      "dsn",  Smp(intGain),    0.0f,       100.0f,  "%");
		Terms.IntegratorReading(p, "Integrator_reading", "mdrv", Smp(intReading), MinFloat, MaxFloat);
		Terms.IntegralGain.setMap(0.64f/SysFs);
		Terms.IntegratorEnable (p, "Integrator_enable",  "iny",  Smp(intEn),     (char **)AbleStr, 1);
		Terms.IntegratorOn     (p, "Integrator_on",      "rdl",  Smp(intOn),     0, 1, "yellow");
		Terms.DerivativeGain   (p, "Derivative_gain",    "dsn",  Smp(derGain),   0.0f,       100.0f,  "%");
		Terms.DerivativeGain.setMap(0.08f);
		Terms.DerivativePlateau(p, "Derivative_plateau", "inpy", NULL,           10.0f,      1000.0f, "Hz",      100.0f);
		Terms.GainBoost        (p, "Gain_boost",         "isny", Smp(gainBoost), 0.0f,       2.0f,    "(0..2)");
		Terms.LLAutoSet        (p, "LL_auto_set",        "b",    NULL,           0,           1);
	#else
		Terms.ErrorFilterEnable(p, "Error_filter_enable",  "ny", NULL,     (char **)AbleStr, 1);
		Terms.ErrorFilter      (p, "Error_filter",        "npy", NULL,           10.0f,      1000.0f, "Hz", 100.0f);
		Terms.ServoGain        (p, "Servo_gain",         "isny", NULL, SigMinSGPC, 100.0f,  "%", SigNormSGPC);
		Terms.ServoGain.setMap(0.32f);
		Terms.ProportionalGain (p, "Proportional_gain",  "dsn",  NULL , SigMinPGPC, 100.0f,  "%", SigNormPGPC);
		Terms.ProportionalGain.setMap(0.16f);
		Terms.IntegralGain     (p, "Integral_gain",      "dsn",  NULL,    0.0f,       100.0f,  "%");
		Terms.IntegratorReading(p, "Integrator_reading", "mdrv", NULL, MinFloat, MaxFloat);
		Terms.IntegralGain.setMap(0.64f/SysFs);
		Terms.IntegratorEnable (p, "Integrator_enable",  "iny",  NULL,     (char **)AbleStr, 1);
		Terms.IntegratorOn     (p, "Integrator_on",      "rdl",  NULL,     0, 1, "yellow");
		Terms.DerivativeGain   (p, "Derivative_gain",    "dsn",  NULL,   0.0f,       100.0f,  "%");
		Terms.DerivativeGain.setMap(0.08f);
		Terms.DerivativePlateau(p, "Derivative_plateau", "inpy", NULL,           10.0f,      1000.0f, "Hz",      100.0f);
		Terms.GainBoost        (p, "Gain_boost",         "isny", NULL, 0.0f,       2.0f,    "(0..2)");
		Terms.LLAutoSet        (p, "LL_auto_set",        "b",    NULL,           0,           1);
	#endif
	

	p=(Parameter *)&Terms.SelfTune;
	(*p)(&Terms, "Self_Tune");
	#ifndef AICUBE
		Terms.SelfTune.Enable          (p, "Enable",            "y",   Smp(stEnable),   (char **)AbleStr);
		Terms.SelfTune.ErrorThreshold  (p, "Error_threshold",   "nvy", Smp(stErrThrsh), 0.0f,  100.0f, "%",           2.0f);  
		Terms.SelfTune.AdaptationPeriod(p, "Adaptation_period", "ny",  NULL,            0.01f, 5.0f,   "sec",         0.25f);
		Terms.SelfTune.Active          (p, "Active",            "rdl", Smp(stAct),      0, 1,          "yellow");
		Terms.SelfTune.ModelBandwidth  (p, "Model_bandwidth",   "ny",  NULL,            1.0f,  100.0f, "(1..100) Hz", 10.0f);
		Terms.SelfTune.ModelDamping    (p, "Model_damping",     "ny",  NULL,            0.1f,  1.0f,   "(0.1..1)",    0.7f);
		Terms.SelfTune.AdaptationGain  (p, "Adaptation_Gain",   "ny",  Smp(stGain),     0.0f,  1.0f,   "",            0.1f);
	#else
		Terms.SelfTune.Enable          (p, "Enable",            "y",   NULL,   (char **)AbleStr);
		Terms.SelfTune.ErrorThreshold  (p, "Error_threshold",   "nvy", NULL, 0.0f,  100.0f, "%",           2.0f);  
		Terms.SelfTune.AdaptationPeriod(p, "Adaptation_period", "ny",  NULL,            0.01f, 5.0f,   "sec",         0.25f);
		Terms.SelfTune.Active          (p, "Active",            "rdl", NULL,      0, 1,          "yellow");
		Terms.SelfTune.ModelBandwidth  (p, "Model_bandwidth",   "ny",  NULL,            1.0f,  100.0f, "(1..100) Hz", 10.0f);
		Terms.SelfTune.ModelDamping    (p, "Model_damping",     "ny",  NULL,            0.1f,  1.0f,   "(0.1..1)",    0.7f);
		Terms.SelfTune.AdaptationGain  (p, "Adaptation_Gain",   "ny",  NULL,     0.0f,  1.0f,   "",            0.1f);
	#endif

	p=(Parameter *)&ThresholdLimits;
	(*p)(this, "Threshold_Limits");

	q=&ThresholdLimits.UpperOuter;
	s=Smp(upperOuter);
	(*q)(p, "Upper_Outer");
	(*q).setFlags("g");
	q->Threshold(q, "Threshold", "inmtvy", &s->threshold, MinFloat, MaxFloat);
	q->SICLimit (q, "Limit", s->limit, Smp(ownChan));

	q=&ThresholdLimits.UpperInner;
	s=Smp(upperInner);
	(*q)(p, "Upper_Inner");
	(*q).setFlags("g");
	q->Threshold(q, "Threshold", "inmtvy", &s->threshold, MinFloat, MaxFloat);
	q->SICLimit (q, "Limit", s->limit, Smp(ownChan));

	q=&ThresholdLimits.LowerOuter;
	s=Smp(lowerOuter);
	(*q)(p, "Lower_Outer");
	(*q).setFlags("g");
	q->Threshold(q, "Threshold", "inmtvy", &s->threshold, MinFloat, MaxFloat);
	q->SICLimit (q, "Limit", s->limit, Smp(ownChan));

	q=&ThresholdLimits.LowerInner;
	s=Smp(lowerInner);
	(*q)(p, "Lower_Inner");
	(*q).setFlags("g");
	q->Threshold(q, "Threshold", "inmtvy", &s->threshold, MinFloat, MaxFloat);
	q->SICLimit (q, "Limit", s->limit, Smp(ownChan));
	Unanimity   (q, "Unanimity", "inpy",	Smp(unanimity), (char **)UnanimityStr);

	PeakAdaptation(this, "Peak_Adaptation");
	p=(Parameter *)&PeakAdaptation.Desired;
	(*p)(&PeakAdaptation, "Desired");
	(*p).setFlags("g");
	PeakAdaptation.Desired.Amplitude(p, "Amplitude", "k",    NULL,           0.0f,     MaxFloat);
	PeakAdaptation.Desired.Mean     (p, "Mean",      "m",    NULL,           MinFloat, MaxFloat);
	PeakAdaptation.Desired.Peak     (p, "Peak",      "inmvy", Smp(peakDes),   MinFloat, MaxFloat);
	PeakAdaptation.Desired.Trough   (p, "Trough",    "inmvy", Smp(troughDes), MinFloat, MaxFloat);
	PeakAdaptation.Desired.Phase    (p, "Phase",     "invy",  Smp(phaseDes),  -180.0f,  180.0f, "degrees");
	p=(Parameter *)&PeakAdaptation.Measured;
	(*p)(&PeakAdaptation, "Measured");
	(*p).setFlags("g");
	PeakAdaptation.Measured.Amplitude(p, "Amplitude", "k",   NULL,            0.0f,     MaxFloat);
	PeakAdaptation.Measured.Mean     (p, "Mean",      "m",   NULL,            MinFloat, MaxFloat);
	PeakAdaptation.Measured.Peak     (p, "Peak",      "dmv", Smp(peakMeas),   MinFloat, MaxFloat);
	PeakAdaptation.Measured.Trough   (p, "Trough",    "dmv", Smp(troughMeas), MinFloat, MaxFloat);
	PeakAdaptation.Measured.Phase    (p, "Phase",     "dv",  Smp(phaseMeas),  -180.0f,  180.0f, "degrees");
	//PeakAdaptation.Measured.Period   (p, "Period",    "dv",  Smp(periodMeas),  0.0f,    MaxFloat, "s"); //period between peaks.
	p=(Parameter *)&PeakAdaptation.Terms;
	(*p)(&PeakAdaptation, "Terms");
	(*p).setFlags("g");
	PeakAdaptation.Terms.Filter           (p, "Filter",             "ny",  NULL,         0.0f,   100.0f, "%",      0.0f);
	PeakAdaptation.Terms.Gain             (p, "Gain",               "ny",  Smp(ptaGain), 0.00001f,  2.0f,   "(0..2)", 0.2f);
	PeakAdaptation.Terms.Hysteresis       (p, "Hysteresis",         "ny",  Smp(slack),   0.0001f, 100.0f, "%",      2.0f);
	PeakAdaptation.Terms.GlobalDetection  (p, "Global_detection",   "ny",  Smp(useGlobalDetection),         (char **)AbleStr);
	PeakAdaptation.Terms.DelayCycles      (p, "Delay_Cycles",       "ny",  Smp(delayPACycles),  1, 1000000, "Cycles", 2);
	//PeakAdaptation.Terms.HysteresisUSD    (p, "Hysteresis_USD",     "iny",  NULL,  0.001f, 100.0f, "%",      2.0f);
	//PeakAdaptation.Terms.SlackFlag        (p, "Auto_Config",        "iny",  NULL, (char **)AbleStr); //if slackFlag is set to one, then use slack, if not use slackUSD.
	PeakAdaptation.Terms.Hysteresis.setMap(0.005f);
	PeakAdaptation.Terms.AmplitudeTooLow  (p, "Amplitude_too_low",  "rl",  NULL,         0,    1,      "red");
	p=(Parameter *)&PeakAdaptation.Enables;
	(*p)(&PeakAdaptation, "Adaptation_Enable");
	(*p).setFlags("g");
	PeakAdaptation.Enables.Amplitude(p, "Amplitude", "ny",  Smp(amplAdapt),   (char **)AbleStr);
	PeakAdaptation.Enables.Mean     (p, "Mean",      "ny",  Smp(medianAdapt), (char **)AbleStr);
	PeakAdaptation.Enables.Phase    (p, "Phase",     "ny",  Smp(phaseAdapt),  (char **)AbleStr);
	PeakAdaptation.Enables.MeanMode (p, "Mean_mode", "ny",  Smp(medianMode),  (char **)MeanModeStr);
	p=(Parameter *)&PeakAdaptation.Achieved;
	(*p)(&PeakAdaptation, "Adaptation_Achieved");
	(*p).setFlags("g");
	PeakAdaptation.Achieved.Amplitude   (p, "Amplitude",     "rl",  NULL,              0, 1, "green");
	PeakAdaptation.Achieved.Mean        (p, "Mean",          "rl",  NULL,              0, 1, "green");
	PeakAdaptation.Achieved.ActiveCycles(p, "Active_cycles", "drl", Smp(activeCycles), 0, 1, "red");
	PeakAdaptation.Achieved.Tolerance   (p, "Tolerance",     "ny",  NULL, 0.01f, 5.0f, "%", 0.5f);

	p=(Parameter *)&PeakAdaptation.Counter;
	(*p)(&PeakAdaptation, "Counter");
	(*p).setFlags("g");
	PeakAdaptation.Counter.Mode         (p, "Mode",            "ny", Smp(countMode),      (char **)CountStr);
	PeakAdaptation.Counter.When         (p, "When",            "ny", Smp(countWhen),      (char **)WhenStr);
	PeakAdaptation.Counter.Cycles       (p, "Cycles_complete", "dn", Smp(countCycles),    0, 4000000000L, "cycles");
	PeakAdaptation.Counter.Required     (p, "Cycles_required", "dn", Smp(countRequired),  0, 4000000000L, "cycles");
	PeakAdaptation.Counter.CompleteLimit(p, "Limit", smpbs->countComplete);
	p=(Parameter *)&PeakAdaptation.Noresp;
	(*p)(&PeakAdaptation, "No_Response");
	(*p).setFlags("g");
	PeakAdaptation.Noresp.Delay      (p, "Delay",           "iny",  Smp(norespDel),    1.0f, 1000000.0f, "cycles", 2.0f);
	PeakAdaptation.Noresp.Period     (p, "Period",          "iny",  Smp(norespPer),    1.0f, 1000000.0f, "cycles", 10.0f);
	PeakAdaptation.Noresp.CreepEn    (p, "Creep_enable",    "iny",  Smp(creepEn),      (char **)AbleStr);
	PeakAdaptation.Noresp.Order      (p, "Order",           "iny",  Smp(meanFirst),    (char **)OrderStr);
	PeakAdaptation.Noresp.MeanCreep  (p, "Mean_creep_rate", "invy", Smp(meanCR),       0.01f, 100.0f, "%/sec", 5.0f);
	PeakAdaptation.Noresp.MeanBand   (p, "Mean_decel_band", "iny",  NULL,              1.0f, 20.0f, "%", 10.0f);
	PeakAdaptation.Noresp.AmplCreep  (p, "Ampl_creep_rate", "invy", Smp(amplCR),       0.01f, 100.0f, "%/cycle", 5.0f);
	PeakAdaptation.Noresp.Active     (p, "Active",          "rdl", Smp(norespActive), 0,    1,       "red");
	PeakAdaptation.Noresp.NorespLimit(p, "Limit",           smpbs->norespLimit,       Smp(ownChan));

	p=(Parameter *)&RampAdaptation;
	(*p)(this, "Ramp_Adaptation");
	RampAdaptation.Start           (p, "Start",           "inmvy",  Smp(rampStart),          MinFloat, MaxFloat);
	RampAdaptation.End		       (p, "End",             "inmvy",  Smp(rampEnd),            MinFloat, MaxFloat);
	RampAdaptation.Desired	  	   (p, "Desired",         "mdrvy", Smp(rampDes),            MinFloat, MaxFloat);
	RampAdaptation.Measured		   (p, "Measured",        "mdrv",  Smp(rampMeas),           MinFloat, MaxFloat);
	RampAdaptation.Enable          (p, "Enable",          "y",     Smp(rampEnable),         (char **)AbleStr);
	RampAdaptation.Active  		   (p, "Active",          "rdl",   Smp(rampActive),         0, 1, "yellow");
	RampAdaptation.Gain    		   (p, "Control_gain",    "iny",    Smp(rampAdaptGain),      0.0f, 2.0f,   "(0..2)", 1.0f);
	RampAdaptation.UpdateFraction  (p, "Update_fraction", "iny",    Smp(rampUpdateFraction), 0.0f, 1.0f,   "(0..1)", 0.1f);

	p=(Parameter *)&PeakTroughLimits;
	(*p)(this, "Peak_Trough_Limits");

	t=&PeakTroughLimits.PeakInner;
	u=Smp(peakInner);
	(*t)(p, "Peak_Inner");
	(*t).setFlags("g");
	t->Threshold (t, "Threshold", "inktvy", &u->threshold, 0.0f, MaxFloat);
	t->UnderLimit(t, "Under_Window", u->underLimit, Smp(ownChan));
	t->OverLimit (t, "Over_Window",  u->overLimit, Smp(ownChan));
	t->Counter   (t, "Violation_count", "dn", &u->vioCnt, 0, 4000000000L, "cycles");

	t=&PeakTroughLimits.PeakOuter;
	u=Smp(peakOuter);
	(*t)(p, "Peak_Outer");
	(*t).setFlags("g");
	t->Threshold (t, "Threshold", "inktvy", &u->threshold, 0.0f, MaxFloat);
	t->UnderLimit(t, "Under_Window", u->underLimit, Smp(ownChan));
	t->OverLimit (t, "Over_Window",  u->overLimit, Smp(ownChan));

	t=&PeakTroughLimits.TroughInner;
	u=Smp(troughInner);
	(*t)(p, "Trough_Inner");
	(*t).setFlags("g");
	t->Threshold (t, "Threshold", "inktvy", &u->threshold, 0.0f, MaxFloat);
	t->UnderLimit(t, "Under_Window", u->overLimit, Smp(ownChan));
	t->OverLimit (t, "Over_Window",  u->underLimit, Smp(ownChan));
	t->Counter   (t, "Violation_count", "dn", &u->vioCnt, 0, 4000000000L, "cycles");

	t=&PeakTroughLimits.TroughOuter;
	u=Smp(troughOuter);
	(*t)(p, "Trough_Outer");
	(*t).setFlags("g");
	t->Threshold (t, "Threshold", "inktvy", &u->threshold, 0.0f, MaxFloat);
	t->UnderLimit(t, "Under_Window", u->overLimit, Smp(ownChan));
	t->OverLimit (t, "Over_Window",  u->underLimit, Smp(ownChan));

	PeakTroughLimits.Violations(p, "Violations");
	PeakTroughLimits.Violations.setFlags("g");
	PeakTroughLimits.Violations.Mode   (&PeakTroughLimits.Violations, "Mode",    "iny", Smp(vioMode), (char **)VioModeStr);
	PeakTroughLimits.Violations.Maximum(&PeakTroughLimits.Violations, "Maximum", "iny", Smp(vioMax),  0, 4000000000L, "violations");
	PeakTroughLimits.Violations.Excess (&PeakTroughLimits.Violations, "Limit",         smpbs->vioLimit, Smp(ownChan));

	p=(Parameter *)&ErrorLimits;
	(*p)(this, "Following_Error_Limits");
	
	v=&ErrorLimits.Outer;
	(*v)(p, "Outer");
	(*v).setFlags("g");
	#ifndef AICUBE
	w=Smp(errorOuter);
	v->Threshold(v, "Threshold", "inktvy", &w->threshold, 0.0f, MaxFloat);
	v->SICLimit (v, "Limit", w->limit, Smp(ownChan));
	#endif

	v=&ErrorLimits.Inner;
	(*v)(p, "Inner");
	(*v).setFlags("g");
	#ifndef AICUBE
	w=Smp(errorInner);
	v->Threshold(v, "Threshold", "inktvy", &w->threshold, 0.0f, MaxFloat);
	v->SICLimit (v, "Limit", w->limit, Smp(ownChan));
	#endif

	p=(Parameter *)&RampLimits;
	(*p)(this, "Ramp_Error_Limits");

	v=&RampLimits.Outer;
	w=Smp(rampOuter);
	(*v)(p, "Outer");
	(*v).setFlags("g");
	v->Threshold(v, "Threshold", "inktvy", &w->threshold, 0.0f, MaxFloat);
	v->SICLimit (v, "Limit", w->limit, Smp(ownChan));

	v=&RampLimits.Inner;
	w=Smp(rampInner);
	(*v)(p, "Inner");
	(*v).setFlags("g");
	v->Threshold(v, "Threshold", "inktvy", &w->threshold, 0.0f, MaxFloat);
	v->SICLimit (v, "Limit", w->limit, Smp(ownChan));

	SetupMode(this, "Setup_Mode");
	SetupMode.MaxLoad    (&SetupMode, "Maximum_load", "invy", Smp(llMax),  0.0f, 100.0f, "%", 2.0f);
	SetupMode.ControlGain(&SetupMode, "Control_gain", "iny",  Smp(llGain), 0.0f, 100.0f, "%", 5.0f);
	SetupMode.ControlGain.setMap(0.64f/SysFs);

	CommandClamp(this, "Command_Clamp");
	CommandClamp.Upper(&CommandClamp, "Upper", "mtv", Smp(cmdPosClamp),  MinFloat, MaxFloat, "%",  110.0f);
	CommandClamp.Lower(&CommandClamp, "Lower", "mtv", Smp(cmdNegClamp),  MinFloat, MaxFloat, "%", -110.0f);

	SPSettings(this, "Set_Point_Settings");
	SPSettings.SPAdjRate     (&SPSettings, "Adjustment_rate", "iny",   NULL,          0.001f,   100.0f, "%/sec",  20.0f);
	SPSettings.RelaxTarget   (&SPSettings, "Relax_target",    "imnvy", Smp(relaxTar), MinFloat, MaxFloat);
	SPSettings.RelaxRate(&SPSettings, "Relax_rate", "iny", Smp(relaxRate), 0.001f, 100.0f, "%/sec", 20.0f); //, "ny", NULL, 0.001f, 100.0f, "%/sec", 20.0f);
	SPSettings.RelaxRate.setMap(0.01f / SysFs); /* Normalise the map */
	SPSettings.LargeIncrement(&SPSettings, "Large_increment",  "iny", NULL, 0.0f, 100.0f, "%", 5.0f);
	SPSettings.SmallIncrement(&SPSettings, "Small_increment",  "iny", NULL, 0.0f, 100.0f, "%", 1.0f);
	p=(Parameter *)&SPSettings.HCDelta;
	(*p)(&SPSettings, "Hand_Controller");
	(*p).setFlags("g");
	SPSettings.HCDelta.Enable   (p, "Enable",     "iny", Smp(HCEn),        (char **)AbleStr);
	SPSettings.HCDelta.Speed    (p, "Speed",      "iny", Smp(HCSpeed),     0.0f, 100.0f, "%/rev", 5.0f);
	SPSettings.HCDelta.LogLaw   (p, "Log_law",    "iny", Smp(HCLogLaw),    0.0f, 100.0f, "", 0.0f);
	SPSettings.HCDelta.MeanSpeed(p, "Mean_speed", "rd", Smp(HCMeanSpeed), 0.0f, 0.2f);
	SPSettings.HCDelta.Coeff    (p, "Coeff",      "iny", Smp(SigHCSpeedCoeff),    0.002f, 0.02f, "", 0.01f); //Fudge

	pInput=NULL;
	pExtCh=NULL;
	realTxFlag=0;
	pLoadCh=NULL;
	pDispCh=NULL;
	pSetupCh=NULL;
	realTxFlag=0;
	chandefIndex=-1;
	pHarmonic=NULL;
}

void SigInputChannel::setCmdClamp(float neg, float pos)
{
	float mx;

	CommandClamp.Upper.getMax(&mx, sizeof(float));
	CommandClamp.Upper=pos*mx;
	CommandClamp.Lower=neg*mx;
	parClampChangedEvent(this);
}

void SigInputChannel::updateMeasuredAmplMn()
{
	PeakAdaptation.Measured.Mean=0.5f*(PeakAdaptation.Measured.Peak.fastGet()+PeakAdaptation.Measured.Trough.fastGet());
	PeakAdaptation.Measured.Amplitude=0.5f*(float)fabs(PeakAdaptation.Measured.Peak.fastGet()-PeakAdaptation.Measured.Trough.fastGet());
	checkPTAchievement();
}

void SigInputChannel::updateDesiredAmplMn()
{
	float pk, tr;

	pk=PeakAdaptation.Desired.Peak();
	tr=PeakAdaptation.Desired.Trough();
	PeakAdaptation.Desired.Mean=0.5f*(pk+tr);
	PeakAdaptation.Desired.Amplitude=0.5f*(float)fabs(pk-tr);
	checkForLowPTAmplitude();
	checkPTAchievement();
}

void SigInputChannel::updateDesiredPkTr()
{
	float max;

	PeakAdaptation.Desired.Amplitude.getMax(&max, sizeof(float));
	if(((float)fabs(PeakAdaptation.Desired.Mean())+PeakAdaptation.Desired.Amplitude())>max)
		if(PeakAdaptation.Desired.Mean()<0.0f)
			PeakAdaptation.Desired.Amplitude=max+PeakAdaptation.Desired.Mean();
		else
			PeakAdaptation.Desired.Amplitude=max-PeakAdaptation.Desired.Mean();
	PeakAdaptation.Desired.Peak=PeakAdaptation.Desired.Mean()+PeakAdaptation.Desired.Amplitude();
	PeakAdaptation.Desired.Trough=PeakAdaptation.Desired.Mean()-PeakAdaptation.Desired.Amplitude();
	PeakAdaptation.Measured.Peak=PeakAdaptation.Desired.Peak();		// Most recent problem - transient on first start at end of fade in
	PeakAdaptation.Measured.Trough=PeakAdaptation.Desired.Trough();//
	checkForLowPTAmplitude();
	checkPTAchievement();
}

void SigInputChannel::checkPTAchievement()
{
	float max, err, tol;
	Sint32 active;
	bool amplAchieved, meanAchieved;

	tol=0.01f*PeakAdaptation.Achieved.Tolerance.fastGet();
	active=Peek(smpbs->activeCycles);
	max=PeakAdaptation.Desired.Amplitude.fastGet();
	err=(float)fabs(PeakAdaptation.Measured.Amplitude.fastGet()-PeakAdaptation.Desired.Amplitude.fastGet());
	amplAchieved=active&&((err/max)<tol);
	PeakAdaptation.Achieved.Amplitude=amplAchieved;
	if(amplAchieved)
		Poke(smpbs->amplAch, 1);
	//
	active|=Peek(smpbs->norespActive);
	PeakAdaptation.Desired.Mean.getMax(&max, sizeof(float));
	err=(float)fabs(PeakAdaptation.Measured.Mean.fastGet()-PeakAdaptation.Desired.Mean.fastGet());
	meanAchieved=active&&((err/max)<tol);
	PeakAdaptation.Achieved.Mean=meanAchieved;
	if(meanAchieved)
		Poke(smpbs->meanAch, 1);
}

void SigInputChannel::parPrimeSignal()
{
	double num[2], den[2];

	Parameter::parPrimeSignal();
	newRange();
	informSGNofRangeChange();
	newErrorFilterFrequency();
	newECDerivativePlateauFrequency();
	newDerivativePlateauFrequency();
	newRange();
	newPTAFilter();
	newSelfTuneModel();
	newSelfTunePeriod();
	Poke(smpbs->pInput, pInput);
	Poke(smpbs->realTxFlag, realTxFlag);
	SetPoint.setRate(SPSettings.SPAdjRate());
	updateMeasuredAmplMn();
	updateDesiredAmplMn();
	Poke(smpbs->meanCreepFac, 1.0f/SysFs);
	FilterDesign(num, den, 1, FDClassicOrder1, FDHighPass, (double)SigNsMeterHPFreq, 0.0, (double)SysFs, 0.0, True);
	PokeCoeffs(smpbs->nsMeterHPFilterNum, num, 1);
	PokeCoeffs(smpbs->nsMeterHPFilterDen, den, 1);
	FilterDesign(num, den, 1, FDClassicOrder1, FDLowPass, (double)SigNsMeterLPFreq, 0.0, (double)SysFs, 0.0, True);
	PokeCoeffs(smpbs->nsMeterLPFilterNum, num, 1);
	PokeCoeffs(smpbs->nsMeterLPFilterDen, den, 1);
	Poke(smpbs->pHarmonic, pHarmonic);
	newMeanBand();
	if(realTxFlag) informEnableChange(chandefIndex, Enable());

}

void SigInputChannel::parConfigureSignal()
{
	Poke(smpbs->pHarmonic, pHarmonic);
	Parameter::parConfigureSignal();
}

void SigInputChannel::parSetEvent(Parameter *sce)
{
	if(sce==&PeakAdaptation.Desired.Peak)
	{
		newDesiredPeak();
		updateDesiredAmplMn();
	}
	if(sce==&PeakAdaptation.Desired.Trough)
	{
		newDesiredTrough();
		updateDesiredAmplMn();
	}
	if((sce==&PeakAdaptation.Desired.Amplitude)||(sce==&PeakAdaptation.Desired.Mean))
	{
		updateDesiredAmplMn();
		updateDesiredPkTr();
	}
	if(sce==&PeakAdaptation.Desired.Phase)
		newDesiredPhase();
	if((sce==&Terms.LLAutoSet)&&pLoadCh&&pDispCh)
		doLLAutoSet();
	if((sce==&PeakAdaptation.Measured.Amplitude)||(sce==&PeakAdaptation.Measured.Mean))
		checkPTAchievement();
	if(sce==&Enable)
		informEnableChange(chandefIndex, Enable());
}

void SigInputChannel::newMeanBand()
{
	Poke(smpbs->creepBand, 100.0f/PeakAdaptation.Noresp.MeanBand());
}

void SigInputChannel::doLLAutoSet()
{
	float x, y, z, w;

	x=pDispCh->Terms.ServoGain()*pDispCh->Terms.ProportionalGain();
	y=(1.0f/C3LLFactor)*pLoadCh->Terms.ServoGain()*pLoadCh->Terms.ProportionalGain();
	z=pDispCh->Terms.ServoGain()*pDispCh->Terms.IntegralGain();
	w=pDispCh->Terms.ServoGain()*pDispCh->Terms.DerivativeGain();
	if(x<=y)
		y=x;
	else
	{
		z*=y/x;
		w*=y/x;
	}
	if(y<=(SigMinSGPC*SigMinPGPC))
	{
		x=SigMinSGPC;
		y=SigMinPGPC;
	}
	else if(y>=10000.0f)
		x=y=100.0f;
	else
	{
		x=SigNormSGPC;
		y/=SigNormSGPC;
		if(y<SigMinPGPC)
		{
			x*=(y/SigMinPGPC);
			y=SigMinPGPC;
		}
		if(y>100.0f)
		{
			x*=(y/100.0f);
			y=100.0f;
		}
	}
	Terms.ServoGain=x;
	Terms.ProportionalGain=y;
	z/=x;
	Clamp(z, 0.0f, 100.0f);
	Terms.IntegralGain=z;
	Terms.IntegratorEnable=(z==0.0f)?0:pDispCh->Terms.IntegratorEnable();
	w/=x;
	Clamp(w, 0.0f, 100.0f);
	Terms.DerivativeGain=w;
}

/*
void SigInputChannel::doLLAutoSet()
{
	if(pDispCh)
	{
		Terms.ServoGain       =pDispCh->Terms.ServoGain();
		Terms.ProportionalGain=pDispCh->Terms.ProportionalGain();
		Terms.IntegralGain    =pDispCh->Terms.IntegralGain();
		Terms.DerivativeGain  =pDispCh->Terms.DerivativeGain();
		Terms.IntegratorEnable=pDispCh->Terms.IntegratorEnable();
		parConfigChangedEvent(this);
	}
}
*/

void SigInputChannel::parPollSignal()
{
	Parameter::parPollSignal();
	Noise=100.0f*(float)sqrt(Peek(smpbs->msNoise));
	checkPTAchievement();
}

void SigInputChannel::parAlteredEvent(Parameter *sce)
{
	if(sce==&Symbol)
		parIdentifierChangedEvent(this);
	if(sce==&Terms.ErrorFilter)
		newErrorFilterFrequency();
	if(sce==&ExternalCompensation.DerivativePlateau)
		newECDerivativePlateauFrequency();
	if(sce==&Terms.DerivativePlateau)
		newDerivativePlateauFrequency();
	if(sce==&Name)
		parConfigChangedEvent(this);
	if(sce==&Enable)
	{
		parConfigChangedEvent(this);
		informEnableChange(chandefIndex, Enable());
	}
	if(sce==&Permission)
	{
		newPermission();
		parConfigChangedEvent(this);
	}
	if(sce==&SetupParticipation)
	{
#ifndef PCTEST
		if (SetupParticipation()==SetupPartImpl) chan_normalise_setup((chandef *)pExtCh);
#endif
		parConfigChangedEvent(this);
	}
	if(sce==&OwningChannel)
		parConfigChangedEvent(this);
	if(sce==&ExternalCompensation.Enable)
		parConfigChangedEvent(this);
	if(sce==&PeakAdaptation.Terms.Filter)
		newPTAFilter();
	if(sce==&Terms.SelfTune.ModelBandwidth)
		newSelfTuneModel();
	if(sce==&Terms.SelfTune.ModelDamping)
		newSelfTuneModel();
	if(sce==&Terms.SelfTune.AdaptationPeriod)
		newSelfTunePeriod();
	if((sce==&Terms.ServoGain)||(sce==&Terms.ProportionalGain)||(sce==&Terms.DerivativeGain)||(sce==&Terms.IntegralGain)||(sce==&Terms.IntegratorEnable))
		newTermSettings();
	if(sce==&SPSettings.SPAdjRate)
		SetPoint.setRate(SPSettings.SPAdjRate());
	if((sce==&CommandClamp.Upper)||(sce==&CommandClamp.Lower))
		parClampChangedEvent(this);
	if(sce==&PeakAdaptation.Desired.Peak)
	{
		if(PeakAdaptation.Desired.Peak()<PeakAdaptation.Desired.Trough())
			PeakAdaptation.Desired.Trough=PeakAdaptation.Desired.Peak();
		updateDesiredAmplMn();
	}
	if(sce==&PeakAdaptation.Desired.Trough)
	{
		if(PeakAdaptation.Desired.Peak()<PeakAdaptation.Desired.Trough())
			PeakAdaptation.Desired.Peak=PeakAdaptation.Desired.Trough();
		updateDesiredAmplMn();
	}
	if((sce==&PeakAdaptation.Desired.Amplitude)||(sce==&PeakAdaptation.Desired.Mean))
		updateDesiredPkTr();
	if(sce==&PeakAdaptation.Terms.Hysteresis)
		checkForLowPTAmplitude();
	if(sce==&PeakAdaptation.Noresp.MeanBand)
		newMeanBand();
}

bool SigInputChannel::isControlFeedback()
{
	Sint32 perm;

	perm=Permission();
	return (perm==2)||(perm==3);
}

void SigInputChannel::newPermission()
{
#ifdef EXTERNALSIMULATION
	if(pExtCh && !isControlFeedback())
		SetExtSimEn(pExtCh, 0);
#endif
}

void SigInputChannel::parChangedEvent(Parameter *sce)
{
	if((sce==&Terms.ServoGain)||(sce==&Terms.ProportionalGain)||(sce==&Terms.DerivativeGain)||(sce==&Terms.IntegralGain))
		newTermSettings();
	if((sce==&PeakAdaptation.Measured.Peak)||(sce==&PeakAdaptation.Measured.Trough))
		updateMeasuredAmplMn();
}

void SigInputChannel::newTermSettings()
{
//	if(pSetupCh)					// Abandon copying terms to setup mode.
//		pSetupCh->doLLAutoSet();
}

void SigInputChannel::parRangeChangedEvent(Parameter *sce)
{
	newRange();
	informSGNofRangeChange();
	Parameter::parRangeChangedEvent(sce);
	parConfigChangedEvent(this);
}

void SigInputChannel::informSGNofRangeChange()
{
	if(realTxFlag)
		informRangeChange(chandefIndex, Range.FullScale(), Range.Units());
}

void SigInputChannel::newRange()
{
	Float fs;
	Integer um;

	fs=Range.FullScale();
	um=Range.UnipolarMode();
	broadcastRange(this, fs, um, Range.Units());
}

void SigInputChannel::setSetupModeLoadPointer(float *p)
{
	Poke(smpbs->pSetupModeLoadFb, p);
}

void SigInputChannel::newErrorFilterFrequency()
{
	#ifndef AICUBE
	double num[2], den[2];

	FilterDesign(num, den, 1, FDClassicOrder1, FDLowPass, (double)Terms.ErrorFilter(), 0.0, (double)SysFs, 0.0, False);
	PokeCoeffs(smpbs->errFilterNum, num, 1);
	PokeCoeffs(smpbs->errFilterDen, den, 1);
	#endif
}

void SigInputChannel::newECDerivativePlateauFrequency()
{
	#ifndef AICUBE
	double num[2], den[2];

	FilterDesign(num, den, 1, FDClassicOrder1, FDHighPass, (double)ExternalCompensation.DerivativePlateau(), 0.0, (double)SysFs, 0.0, False);
	PokeCoeffs(smpbs->ecDerFilterNum, num, 1);
	PokeCoeffs(smpbs->ecDerFilterDen, den, 1);
	#endif
}

void SigInputChannel::newDerivativePlateauFrequency()
{
	#ifndef AICUBE
	double num[2], den[2];

	FilterDesign(num, den, 1, FDClassicOrder1, FDHighPass, (double)Terms.DerivativePlateau(), 0.0, (double)SysFs, 0.0, False);
	PokeCoeffs(smpbs->derFilterNum, num, 1);
	PokeCoeffs(smpbs->derFilterDen, den, 1);
	num[0]=num[1]-num[0]*den[1];
	num[1]=0.0;
	PokeCoeffs(smpbs->derInvNum, num, 1);
	#endif
}

void SigInputChannel::newDesiredPeak()
{
	if(PeakAdaptation.Desired.Peak()<PeakAdaptation.Desired.Trough())
	{
		PeakAdaptation.Desired.Trough=PeakAdaptation.Desired.Peak();
	}
	PeakAdaptation.Measured.Peak=PeakAdaptation.Desired.Peak();
	PeakAdaptation.Measured.Trough=PeakAdaptation.Desired.Trough();
	PeakAdaptation.Measured.Phase=PeakAdaptation.Desired.Phase();
}

void SigInputChannel::newDesiredTrough()
{
	if(PeakAdaptation.Desired.Trough()>PeakAdaptation.Desired.Peak())
	{
		PeakAdaptation.Desired.Peak=PeakAdaptation.Desired.Trough();
	}
	PeakAdaptation.Measured.Peak=PeakAdaptation.Desired.Peak();
	PeakAdaptation.Measured.Trough=PeakAdaptation.Desired.Trough();
	PeakAdaptation.Measured.Phase=PeakAdaptation.Desired.Phase();
}

void SigInputChannel::newDesiredPhase()
{
	PeakAdaptation.Measured.Peak=PeakAdaptation.Desired.Peak();
	PeakAdaptation.Measured.Trough=PeakAdaptation.Desired.Trough();
	PeakAdaptation.Measured.Phase=PeakAdaptation.Desired.Phase();
}

void SigInputChannel::newPTAFilter()
{
	float x;

	x=0.01f*PeakAdaptation.Terms.Filter();
	if(x>0.99f)
		x=0.99f;
	Poke(*Smp(ptaFilter), x);
}

void SigInputChannel::newSelfTuneModel()
{
	#ifndef AICUBE
	double num[3], den[3], fc, damp;

	fc=(double)Terms.SelfTune.ModelBandwidth();
	damp=(double)Terms.SelfTune.ModelDamping();
	FilterDesign(num, den, 2, FDClassicOrder2, FDLowPass, fc, damp, (double)SysFs, 0.0, false);
	PokeCoeffs(smpbs->stModelNum, num, 2);
	PokeCoeffs(smpbs->stModelDen, den, 2);
	#endif
}

void SigInputChannel::newSelfTunePeriod()
{
	#ifndef AICUBE
	Poke(smpbs->stPrd, (Sint32)(SysFs*Terms.SelfTune.AdaptationPeriod()));
	#endif
}

void SignalArray::operator()(Parameter *par, char *n, SmpSignal &smp, int nCh)
{
	SigInputChannel *p;
	int i;

	smpbs=&smp;
	Parameter::operator()(par, n);
	for(i=0, p=Channel; i<SigNCh; i++, p++)
		(*p)(this, nCh, i, smpbs->channel[i]);
	FilterDesign(num, den, 1, FDClassicOrder1, FDLowPass, SigLPFreq, 0.0, (double)SysFs, 0.0, False);
	pTsrIn=LimitSys::IContext.smpCntxt?&((LimitSys::IContext.smpCntxt)->tsrInBufLo):NULL;
	pop=smpbs->common.pop;
	nChans=(nCh>SigMaxChans)?SigMaxChans:nCh;
	pGb=NULL;
	pCNet=NULL;
	pHCDelta=NULL;
}

void SignalArray::parPrimeSignal()
{
	int i;

	PokeCoeffs(smpbs->common.filterNum, num, 1);
	PokeCoeffs(smpbs->common.filterDen, den, 1);
	Poke(smpbs->common.persistence, (Sint32)(SigPersist*SysFs));
	for(i=0; i<SigNCh; i++)
	{
		Poke(smpbs->common.pop[i], &(smpbs->channel[i].output));
		smpbs->channel[i].pCommon = &smpbs->common;
		//Poke(*(Uint32 **)Smp(channel[i].pCommon), (Uint32 *)Smp(common));
	}
	initGainBoostFilter();
	Poke(smpbs->common.pTsrIn, pTsrIn);
	Poke(smpbs->nChans, nChans);
	Poke(smpbs->common.iInvDecay, 1.0f-2.0f*(float)Pi*SigIInvFreq/SysFs);
	smpbs->pGb = pGb;
	//Poke(*((Uint32 *)Smp(pGb)), (Uint32)pGb);
	Poke(smpbs->common.pCNet, pCNet);
	Parameter::parPrimeSignal();
	Poke(smpbs->common.pHCDelta, pHCDelta);
}

void SignalArray::parConfigureSignal()
{
	int i, j;

	for(i=0; i<SigNCh; i++)
	{
		j=Channel[i].OwningChannel()-1;
		if ((j >= 0) && (j < SigMaxChans))
			smpbs->channel[i].pPTAPort = (smpbs->ptaPort + j);
			//Poke(*(Uint32 **)Smp(channel[i].pPTAPort), (Uint32 *)(smpbs->ptaPort+j));
		else
			smpbs->channel[i].pPTAPort = NULL;
			//Poke(*(Uint32 **)Smp(channel[i].pPTAPort), NULL);

	}
	Parameter::parConfigureSignal();
}

int SignalArray::FindSignal(Integer perm, Integer start, Integer owner)
{
	int i;

	if(start<0)
		return -100;
	for(i=start; i<SigNCh; i++)
		if((Channel[i].Enable()==1)&&(Channel[i].Permission()==perm)&&(Channel[i].OwningChannel()==owner))
			return(i);
	return(-100);
}

int SignalArray::FindSignalByName(char *s, int n)
{
	int i, j;
	char nameBuf[SigNameSz];

	if(n>SigNameSz)
		return 0;
	for(i=0; i<SigNCh; i++)
	{
		strncpy(nameBuf, Channel[i].Name(), SigNameSz);
		for(j=0; j<n; j++)
			if(nameBuf[j]==' ')
				nameBuf[j]='_';
		if((strlen(nameBuf)==(unsigned)n) && !strncmp(s, nameBuf, n))
			return i+1;
	}
	return 0;
}

int SignalArray::FindSignalBySymbol(char *s, int n)
{
	int i, j;
	char nameBuf[SigSymSz];

	if(n>SigSymSz)
		return 0;
	for(i=0; i<SigNCh; i++)
	{
		strncpy(nameBuf, Channel[i].Symbol(), SigSymSz);
		for(j=0; j<n; j++)
			if(nameBuf[j]==' ')
				nameBuf[j]='_';
		if((strlen(nameBuf)==(unsigned)n) && !strncmp(s, nameBuf, n))
			return i+1;
	}
	return 0;
}

void SignalArray::initGainBoostFilter()
{
	double num[2], den[2];

	FilterDesign(num, den, 1, FDClassicOrder1, FDHighPass, SigGBHPFreq, 0.0, (double)SysFs, 0.0, False);
	PokeCoeffs(smpbs->common.gbHPFilterNum, num, 1);
	PokeCoeffs(smpbs->common.gbHPFilterDen, den, 1);

	// This factor is required to ensure rising part of gain boost high pass filter response approximates s not s/w.
	Poke(smpbs->common.gbFac, (float)(2.0*Pi*SigGBHPFreq));

	FilterDesign(num, den, 1, FDClassicOrder1, FDLowPass, SigGBLPFreq, 0.0, (double)SysFs, 0.0, False);
	PokeCoeffs(smpbs->common.gbLPFilterNum, num, 1);
	PokeCoeffs(smpbs->common.gbLPFilterDen, den, 1);
}

#endif

#ifdef SMPGEN

#ifdef CONTROLCUBE
#ifndef PCTEST
extern "C" {
extern volatile Uint32 tracking_request;
}
#endif

float SmpSigInputChannel::loopIter(float cmd, float spFade, float *pCmd, float *pFb,
									float *pError, float ecfb,
#ifndef EXTERNALSIMULATION
	Sint32 sim, float simFb,
#endif
	Sint32 rampSp, float *pSTM, float spinc, Sint32 intAlways, SmpSigInputChannel *pCasCh)
{
	float gb;
	register float w, x, y, z;
#ifdef PCTEST
	static int tracking_request=0;
#endif
	Sint32 i;
	Uint32 newHCDelta;
	Uint32 newSpFreeze;
	register SmpSigInputChannel *q;

	if(!pCommon)
		return 0.0f;

#ifndef EXTERNALSIMULATION
	if(sim&&realTxFlag)
	{
		output=simFb;
		simTakeOver=true;
	}
#endif

	if(pFb)
		*pFb=output;

	// External compensation
	if(ecEn==SigEnabled)
	{
		x=Filter(ecDerFilterNum, ecDerFilterDen, &ecDerFilterState, 1, ecfb);
		x=ecDerGain*x+ecPropGain*ecfb;
		if(ecPhasing)
			x=-x;
		ecfb=x;
	}

	// Gain boost
	x=(pCommon->gbFac)*Filter(pCommon->gbHPFilterNum, pCommon->gbHPFilterDen, &gbHPFilterState, 1, cmd);
	if(x<0.0f)
		x=-x;
	gb=Filter(pCommon->gbLPFilterNum, pCommon->gbLPFilterDen, &gbLPFilterState, 1, x);
	Clamp(gb, 0.0f, 1.0f);
	gb=1.0f+gainBoost*gb;

	// Set point ramping
	if(rampSp)
		if(setPnt>relaxTar)
		{
			setPnt-=relaxRate;
			if(setPnt<relaxTar)
				setPnt=relaxTar;
		}
		else if(setPnt<relaxTar)
		{
			setPnt+= relaxRate;
			if(setPnt>relaxTar)
				setPnt=relaxTar;
		}

	// Set point ramping for setup mode load limit
	if(pSetupModeLoadFb)
	{
		x=*pSetupModeLoadFb;
		if(x>llMax)
		{
			x-=llMax;
			setPnt-=llGain*x;
			if(setPnt<-1.1f)
				setPnt=-1.1f;
		}
		if(x<-llMax)
		{
			x=-llMax-x;
			setPnt+=llGain*x;
			if(setPnt>1.1f)
				setPnt=1.1f;
		}
	}

	// Set point increment from global set point
	setPnt+=spinc;
	Clamp(setPnt, -1.1f, 1.1f);

	// Momentary set point track for transducer zero change
	if(tracking_request)
	{
		setPnt=output+trackingError;
		Clamp(setPnt, -1.1f, 1.1f);
	}

	// Error
	w=spFade*setPnt+cmd;
	if(w<cmdNegClamp)
		w=cmdNegClamp;
	if(w>cmdPosClamp)
		w=cmdPosClamp;
	if(spFade==0.0f)
	{
		setPnt=w;
		Clamp(setPnt, -1.1f, 1.1f);
	}
	if(pCmd)
		*pCmd=w;
	x=w-output;
	if(!tracking_request)
		trackingError=(1.0f-SigSPTFCoeff)*trackingError+SigSPTFCoeff*x;
#ifdef EXTERNALSIMULATION
	if((ecEn==SigEnabled)&&(ecMode==SigOuterLoop))
#else
	if(!sim&&(ecEn==SigEnabled)&&(ecMode==SigOuterLoop))
#endif
		x+=ecfb;
  // Filter enabled?
  if( errfiltEnable)
	  y=Filter(errFilterNum, errFilterDen, &errFilterState, 1, x);
  else
    y=x;
	if(pError)
		*pError=y;
	if(y<0.0f)
		x=-y;
	else
		x=y;

	// Set point adjustment from hand controller
	if(HCEn&&(pCommon->pHCDelta)&&(pCommon->pTsrIn))
	{
		newHCDelta=*pCommon->pHCDelta;
		newSpFreeze=*(pCommon->pTsrIn)&(TSRSpFreezePos|TSRSpFreezeNeg);
		if(!(*(pCommon->pTsrIn)&TSRInch))
			newHCDelta=0;
		if(newHCDelta&SigHCDMBit)
		{
			i=(Sint32)(newHCDelta&0x0000ffff);
			if(newHCDelta&0x00008000)
				i-=65536;
			HCMeanSpeed=(1.0f-SigHCSpeedCoeff)*HCMeanSpeed+SigHCSpeedCoeff*(float)fabs((float)i);
			if(i)
			{

				/* The hand controller is trying to move the setpoint, so check if the 
				   appropriate TSRSpFreeze flag is active. If so, freeze the current
				   operation.
				   
				   When freezing, two different operations are performed: 
				   
				   On the first update after the freeze flags have been set, the code 
				   jumps to the resetsp label, where the setpoint is reset to the current 
				   feedback. This prevents the setpoint from overrunning if the encoder
				   is changed quickly.

				   On subsequent updates, the code jumps to the freeze label, where no
				   change is made to the setpoint. This prevents problems if the control
				   loop is poorly adjusted, where repeatedly setting the setpoint equal
				   to the feedback causes the setpoint to drift.

				   The variable HCExcess is used to measure how many times the encoder
				   has attempted to move past the limit and to prevent opposite motion
				   until it has been moved back that number of times. This prevents an
				   odd situation where it has been observed that continued motion into
				   a limit can actually result in motion away from the limit. The reason
				   is that apparently smooth unidirectional motion can actually consist
				   of bidirectional vibration caused by the "detents" on the encoder.
				   When this vibration occurs, steps into the limit are clipped, but
				   opposite steps are counted leading to a net motion away from the
				   limit even when moving the encoder towards the limit.

				   The HCExcess variable prevents opposite motion from registering until
				   the encoder has been moved back sufficiently.

				   A limit of 128 step times is used to prevent an excessive number of
				   steps being accumulated, which would make normal operation difficult.

				*/

				if(newSpFreeze) {
					if(newSpFreeze&((i > 0) ? TSRSpFreezePos : TSRSpFreezeNeg))
					{
						if (HCExcess < 128) HCExcess++;
						if(newSpFreeze ^ lastSpFreeze)
							goto resetsp;
						else
							goto freeze;
					}
					else {
						if (HCExcess && (--HCExcess > 0)) goto freeze;
					}
				}
				else {
					HCExcess = 0;
				}
				setPnt+=(0.00005f*HCSpeed*(1.0f+HCLogLaw*HCMeanSpeed)*(float)i);
				Clamp(setPnt, -1.1f, 1.1f);
				HCAdjustTimeOut=(Uint32)(SigHCTimeOut*SysFs);
			}
			else
			{
				if(HCAdjustTimeOut)
					if(--HCAdjustTimeOut==0)
					{
						setPnt=output;
						Clamp(setPnt, -1.1f, 1.1f);
					}
			}
		}
		else
		{
			if(lastHCDelta&SigHCDMBit)
			{
resetsp:
				setPnt=output;
				Clamp(setPnt, -1.1f, 1.1f);
			}
freeze:
			HCAdjustTimeOut=0;
			HCMeanSpeed=0.0f;
		}
		lastHCDelta=newHCDelta;
		lastSpFreeze=newSpFreeze;
	}
	else
	{
		lastHCDelta=0;
		HCAdjustTimeOut=0;
		HCMeanSpeed=0.0f;
		lastSpFreeze=0;
	}

	// Error limit
	errorLimitIter(x, errorOuter);
	errorLimitIter(x, errorInner);

	// Terms
	z=y*servoGain;
	intOn=intEn&&(intAlways||((pCommon->pTsrIn)&&(TSRIntEn&*(pCommon->pTsrIn))));
	if(intOn)
	{
		intCap+=(double)(intGain*z);
		Clamp(intCap, -1.0f, 1.0f);
	}
	else
	{
		if(intCap>0.0)
		{
			intCap-=SigIntDisInc;
			if(intCap<0.0)
				intCap=0.0;
		}
		if(intCap<0.0)
		{
			intCap+=SigIntDisInc;
			if(intCap>0.0)
				intCap=0.0;
		}
	}
	intReading=(float)intCap;
	x=Filter(derFilterNum, derFilterDen, &derFilterState, 1, z);
	x=derGain*x+propGain*gb*z+(float)intCap;
	Clamp(x, -1.25f, 1.25f);
#ifdef EXTERNALSIMULATION
	if((ecEn==SigEnabled)&&(ecMode==SigInnerLoop))
#else
	if(!sim&&(ecEn==SigEnabled)&&(ecMode==SigInnerLoop))
#endif
		x+=ecfb;
	y=selfTune(w, output);
	if(pSTM)
		*pSTM=y;

	// Cascade terms
	if(q=pCasCh)
		if(casen)
		{
			z=x*(q->servoGain);
			if(q->intOn=(q->intEn)&&(intAlways||((pCommon->pTsrIn)&&(TSRIntEn&*(pCommon->pTsrIn)))))
			{
				casIntCap+=(double)((q->intGain)*z);
				Clamp(casIntCap, -1.0, 1.0);
			}
			else
			{
				if(casIntCap>0.0)
				{
					casIntCap-=SigIntDisInc;
					if(casIntCap<0.0)
						casIntCap=0.0;
				}
				if(casIntCap<0.0)
				{
					casIntCap+=SigIntDisInc;
					if(casIntCap>0.0)
						casIntCap=0.0;
				}
			}
			x=Filter(q->derFilterNum, q->derFilterDen, &casDerFilterState, 1, z);
			x=(q->derGain)*x+(q->propGain)*z+(float)casIntCap;
			Clamp(x, -1.25f, 1.25f);
		}
		else
			q->intOn=0;

	return x;
}

float SmpSigInputChannel::ilIter(float cmd)
{
	#ifndef AICUBE
	register float x, y;

	x=cmd-output;
    if( errfiltEnable)
      x=Filter(errFilterNum, errFilterDen, &errFilterState, 1, x);
	x*=servoGain;
	if(intOn=intEn)
	{
		intCap+=(double)(intGain*x);
		Clamp(intCap, -1.0f, 1.0f);
	}
	else
		intCap=0.0;
	y=Filter(derFilterNum, derFilterDen, &derFilterState, 1, x);
	x=derGain*y+propGain*x+(float)intCap;
	Clamp(x, -1.25f, 1.25f);
	return x;
	#endif
}

float SmpSigInputChannel::selfTune(float cmd, float fb)
{
	#ifndef AICUBE
	register float w, x, y, z;

	z=cmd-fb;
	y=Filter(stModelNum, stModelDen, stModelState1, 2, z);
	x=intOn?intGain:0.0f;
	y-=(x*iInvFb+derGain*dInvFb);
	y/=(propGain+x+derFilterNum[0]*derGain);
	if(x==0.0f)
		iInvFb*=pCommon->iInvDecay;	
	iInvFb+=y;
	x=dInvFb+derFilterNum[0]*y;
	dInvFb=Filter(derInvNum, derFilterDen, &derInvState, 1, y);
	w=Filter(stModelNum, stModelDen, stModelState2, 2, cmd);
	if(!stEnable)
	{
		stCnt=stAct=0;
		return w;
	}
	if((z>=stErrThrsh)||(z<=-stErrThrsh))
	{
		stCnt=stPrd;
		stAct=1;
	}
	if(stCnt>0)
	{
		z=y*y+x*x;
		if(intOn)
			z+=(iInvFb*iInvFb/(SysFs*SysFs));
		if(z>0.0f)
		{
			z=stGain*SigMaxSTGain*(w-fb)/z;
			y=propGain+z*y;
			Clamp(y, SigMinPG, SigMaxPG);
			propGain=y;
			if(intOn)
			{
				y=intGain+z*iInvFb/(SysFs*SysFs);
				Clamp(y, 0.0f, SigMaxIG);
				intGain=y;
			}
			y=derGain+z*x;
			Clamp(y, 0.0f, SigMaxDG);
			derGain=y;
		}
		if(!--stCnt)
			stAct=0;
	}
	return w;
	#endif
}
#endif

#ifdef CONTROLCUBE
float SmpSigInputChannel::spTrackIter(float olop, float ecfb
#ifndef EXTERNALSIMULATION
	, Sint32 sim, float simFb
#endif
	, SmpSigInputChannel *pCasCh
)
{
	float x, y;
	register SmpSigInputChannel *q;

#ifndef EXTERNALSIMULATION
	if(sim&&realTxFlag)
	{
		output=simFb;
		simTakeOver=true;
	}
#endif

	// External compensation
	if(ecEn==SigEnabled)
	{
		Filter(ecDerFilterNum, ecDerFilterDen, &ecDerFilterState, 1, ecfb);
		y=ecPropGain*ecfb;
		if(ecPhasing)
			y=-y;
	}

	// Cascade terms
	if(q=pCasCh)
		if(casen)
		{
			if(q->intOn=(q->intEn)&&(pCommon->pTsrIn)&&(TSRIntEn&*(pCommon->pTsrIn)))
			{
				casIntCap=(double)((1.0f-SigSPTFCoeff)*(float)casIntCap+SigSPTFCoeff*olop);
				Clamp(casIntCap, -1.0, 1.0);
				olop=0.0f;
			}
			else
			{
				casIntCap=0.0;
				olop/=((q->servoGain)*(q->propGain));
			}
			Filter(q->derFilterNum, q->derFilterDen, &casDerFilterState, 1, (q->servoGain)*olop);
		}
		else
			q->intOn=0;

	// Terms
	intOn=intEn&&(pCommon->pTsrIn)&&(TSRIntEn&*(pCommon->pTsrIn));
#ifdef EXTERNALSIMULATION
	if((ecEn==SigEnabled)&&(ecMode==SigInnerLoop))
#else
	if(!sim&&(ecEn==SigEnabled)&&(ecMode==SigInnerLoop))
#endif
		olop-=y;
	if(intOn)
	{
		intCap=(double)((1.0f-SigSPTFCoeff)*(float)intCap+SigSPTFCoeff*olop);
		Clamp(intCap, -1.0, 1.0);
		x=0.0f;
	}
	else
	{
		intCap=0.0;
		x=olop/(servoGain*propGain);
	}
	Filter(errFilterNum, errFilterDen, &errFilterState, 1, x);
	Filter(derFilterNum, derFilterDen, &derFilterState, 1, servoGain*x);
#ifdef EXTERNALSIMULATION
	if((ecEn==SigEnabled)&&(ecMode==SigOuterLoop))
#else
	if(!sim&&(ecEn==SigEnabled)&&(ecMode==SigOuterLoop))
#endif
		x-=y;
	setPnt=(1.0f-SigSPTFCoeff)*setPnt+SigSPTFCoeff*(output+x);
	Clamp(setPnt, -1.1f, 1.1f);
	Filter(pCommon->gbHPFilterNum, pCommon->gbHPFilterDen, &gbHPFilterState, 1, setPnt);
	gbLPFilterState=0.0f;

	// Error limits
	errorLimitIter(0.0f, errorOuter);
	errorLimitIter(0.0f, errorInner);

	return output;
}
#endif

#ifndef INLINE_LIMIT_FUNCS
void SmpSigInputChannel::upperLimitIter(float v, SmpSigThrLimit &p)
{
	p.limit.iter();
	if(v>p.threshold)
	{
		p.perCnt++;
		if(p.perCnt>pCommon->persistence)
			p.perCnt=pCommon->persistence;
	}
	else
		p.perCnt=0;
	p.limit.update((Uint32)(p.perCnt==pCommon->persistence), v-p.threshold);
}

void SmpSigInputChannel::lowerLimitIter(float v, SmpSigThrLimit &p)
{
	p.limit.iter();
	if(v<p.threshold)
	{
		p.perCnt++;
		if(p.perCnt>pCommon->persistence)
			p.perCnt=pCommon->persistence;
	}
	else
		p.perCnt=0;
	p.limit.update((Uint32)(p.perCnt==pCommon->persistence), v-p.threshold);
}

void SmpSigInputChannel::ptLimitIter(float v, float target, int det, SmpSigPTLimit &p)
{
	bool ptActive, underState, overState;

	p.underLimit.iter();
	p.overLimit.iter();
	if(activeCycles)
	{
//		ptActive=meanAch&&amplAch;

		/* If global detection is enabled, then ignore mean and amplitude achieved flags */

		ptActive = useGlobalDetection || (meanAch&&amplAch);

		if(det)
		{
			underState=ptActive&&(v<(target-p.threshold));
			overState=ptActive&&(v>(target+p.threshold));
			p.underLimit.update((Uint32)underState, v-(target-p.threshold));
			p.overLimit.update ((Uint32)overState, v-(target+p.threshold));
			if(underState||overState)
				p.vioCnt++;
		}
	}
	else
	{
		// Clear States if not generating
		p.underLimit.update(0, 0.0f);
		p.overLimit.update(0, 0.0f);
	}
}


void SmpSigInputChannel::errorLimitIter(float v, SmpSigErrLimit &p)
{
	// Note iter() call moved to transducer channel processing
	if(fabs(v)>p.threshold)
	{
		p.perCnt++;
		if(p.perCnt>pCommon->persistence)
		p.perCnt=pCommon->persistence;
	}
	else
		p.perCnt=0;
	p.limit.update((Uint32)(p.perCnt==pCommon->persistence), (v>=0.0f)?v-p.threshold:v+p.threshold);
}
#endif

#endif
