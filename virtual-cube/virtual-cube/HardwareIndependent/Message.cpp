// Message.cpp - Message module

#include "C3Std.h"
#include "ControlCube.h"
#include "Message.h"

#ifdef SUPGEN

#ifdef FUDGE
extern Uint32 *statePeekaboo;
extern float  *transElapsedPeekaboo;
#endif

#ifndef PCTEST
extern "C" {
#if (defined(CONTROLCUBE) || defined(AICUBE) || defined(SIGNALCUBE))
#include "../include/defines.h"
#include "../include/server.h"
#endif
}
#endif

#ifdef PCTEST
#define NOCONN
#endif

#ifdef KINETUSB					// Remove this if 'connection' definition available
#define NOCONN					//
#endif							//

#if (defined(SIGNALCUBE) || defined(AICUBE))

/* Signal Cube does not have battery-backed RAM, so remove any
   'i' flags that may be present.
   AI Cube doesn't perform regular updates to 'i' parameters
   during a test, so they can be treated as SNV. So remove
   any 'i' flags that may be present.

*/

char *flagcopy(char *s, const char *t, int n)
{
	char c;
	char *p = s;

	while((n > 0) && ((c = *t++) != '\0'))
	{
		if (c != 'i')
		{
			*p++ = c;
			--n;
		}
	}
	while(n > 0)
	{
		*p++ = '\0';
		--n;
	}

	return(s);
}

#else

#define flagcopy(s, t, n) strncpy(s, t, n)

#endif



#define	NO_ERROR	0
extern Parameter *PSupSystem;

int MsgGetHandle(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	GetHandleMsg *p;
	GetHandleRply *q;
	Parameter *r;

	// Get input/output pointers
	p=(GetHandleMsg *)msg;
	q=(GetHandleRply *)rply;
	r=(Parameter *)p->handle;
	
	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Precaution
	p->path[255]=0;

	// Find the path
	r=Parameter::FindPath(r, p->path);

	// Set lengths, etc.
	q->command=p->command;
	q->handle=(Uint32)r;
	*msglen=sizeof(GetHandleMsg);
	*rplylen=sizeof(GetHandleRply);

	// Return success
	return NO_ERROR;
}

int MsgGetParType(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	GetParTypeMsg *p;
	GetParTypeRply *q;
	Parameter *r;

	// Get input/output pointers
	p=(GetParTypeMsg *)msg;
	q=(GetParTypeRply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
	{
		q->type=(Uint32)r->getType();
		q->result=ParOkay;
	}
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(GetParTypeMsg);
	*rplylen=sizeof(GetParTypeRply);

	// Return success
	return NO_ERROR;
}

int MsgGetParFlags(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	GetParFlagsMsg *p;
	GetParFlagsRply *q;
	Parameter *r;

	// Get input/output pointers
	p=(GetParFlagsMsg *)msg;
	q=(GetParFlagsRply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
	{
//		strncpy(q->flags, r->getFlags(), sizeof(q->flags));
		flagcopy(q->flags, r->getFlags(), sizeof(q->flags));
		q->flags[sizeof(q->flags)-1]=0;
		q->result=ParOkay;
	}
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(GetParFlagsMsg);
	*rplylen=sizeof(GetParFlagsRply);

	// Return success
	return NO_ERROR;
}

int MsgGetParMin4(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	GetParMin4Msg *p;
	GetParMin4Rply *q;
	Parameter *r;

	// Get input/output pointers
	p=(GetParMin4Msg *)msg;
	q=(GetParMin4Rply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
#ifdef NOCONN
		q->result=r->getMin(&q->min, sizeof(q->min), 1);
#else
		q->result=r->getMin(&q->min, sizeof(q->min), ((connection *)conn)->komask);
#endif
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(GetParMin4Msg);
	*rplylen=sizeof(GetParMin4Rply);

	// Return success
	return NO_ERROR;
}

int MsgGetParMax4(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	GetParMax4Msg *p;
	GetParMax4Rply *q;
	Parameter *r;

	// Get input/output pointers
	p=(GetParMax4Msg *)msg;
	q=(GetParMax4Rply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
		q->result=r->getMax(&q->max, sizeof(q->max));
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(GetParMax4Msg);
	*rplylen=sizeof(GetParMax4Rply);

	// Return success
	return NO_ERROR;
}

int MsgSetParValue4(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	SetParValue4Msg *p;
	SetParValue4Rply *q;
	Parameter *r;
	Uint32 forcedResult;

	// Get input/output pointers
	p=(SetParValue4Msg *)msg;
	q=(SetParValue4Rply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
	{
		
#ifdef FUDGE
		Syslog(0, statePeekaboo?Peek(*statePeekaboo):0, transElapsedPeekaboo?((Uint32)(1000.0f*Peek(*transElapsedPeekaboo))):0, r, p->value);
#endif
#ifdef NOCONN
		q->result=r->setValue(&p->value, sizeof(p->value), 1);
#else
		if (((C3App*)PSupSystem)->IsParameterSticky(p->handle) == 0)
			q->result = r->setValue(&p->value, sizeof(p->value), ((connection*)conn)->komask);
		forcedResult = ((C3App*)PSupSystem)->ForceReturnError();
		if (forcedResult > 0)
			q->result = (forcedResult - 1);		// nasty frig, having knowledge of the enum!
#endif
	}
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(SetParValue4Msg);
	*rplylen=sizeof(SetParValue4Rply);

	// Return success
	return NO_ERROR;
}

int MsgGetParValue4(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	GetParValue4Msg *p;
	GetParValue4Rply *q;
	Parameter *r;

	// Get input/output pointers
	p=(GetParValue4Msg *)msg;
	q=(GetParValue4Rply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
		q->result=r->getValue(&q->value, sizeof(q->value));
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(GetParValue4Msg);
	*rplylen=sizeof(GetParValue4Rply);

	// Return success
	return NO_ERROR;
}

int MsgSetParValue256(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	SetParValue256Msg *p;
	SetParValue256Rply *q;
	Parameter *r;

	// Get input/output pointers
	p=(SetParValue256Msg *)msg;
	q=(SetParValue256Rply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
#ifdef NOCONN
		q->result=r->setValue(&p->value, sizeof(p->value), 1);
#else
		q->result=r->setValue(&p->value, sizeof(p->value), ((connection *)conn)->komask);
#endif
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(SetParValue256Msg);
	*rplylen=sizeof(SetParValue256Rply);

	// Return success
	return NO_ERROR;
}

int MsgGetParValue256(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	GetParValue256Msg *p;
	GetParValue256Rply *q;
	Parameter *r;

	// Get input/output pointers
	p=(GetParValue256Msg *)msg;
	q=(GetParValue256Rply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
		q->result=r->getValue(q->value, sizeof(q->value));
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(GetParValue256Msg);
	*rplylen=sizeof(GetParValue256Rply);

	// Return success
	return NO_ERROR;
}

int MsgParSpecial256(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	ParSpecial256Msg *p;
	ParSpecial256Rply *q;
	Parameter *r;

	// Get input/output pointers
	p=(ParSpecial256Msg *)msg;
	q=(ParSpecial256Rply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
		q->result=r->special(p->fn, p->arg, sizeof(p->arg), q->arg, sizeof(q->arg));
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(ParSpecial256Msg);
	*rplylen=sizeof(ParSpecial256Rply);

	// Return success
	return NO_ERROR;
}

int MsgGetParMember256(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	GetParMember256Msg *p;
	GetParMember256Rply *q;
	Parameter *r;
	char *s;

	// Get input/output pointers
	p=(GetParMember256Msg *)msg;
	q=(GetParMember256Rply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
	{
		s=r->operator[](p->index);
		if(s)
		{
			strncpy(q->member, s, sizeof(q->member));
			q->member[sizeof(q->member)-1]=0;
		}
		else
			q->member[0]=0;
		q->result=ParOkay;
	}
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(GetParMember256Msg);
	*rplylen=sizeof(GetParMember256Rply);

	// Return success
	return NO_ERROR;
}

int MsgGetParLive4(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	GetParLive4Msg *p;
	GetParLive4Rply *q;
	Parameter *r;

	// Get input/output pointers
	p=(GetParLive4Msg *)msg;
	q=(GetParLive4Rply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
		q->result=r->getLive(&q->value, sizeof(q->value));
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(GetParLive4Msg);
	*rplylen=sizeof(GetParLive4Rply);

	// Return success
	return NO_ERROR;
}

int MsgGetParUnits256(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	GetParUnits256Msg *p;
	GetParUnits256Rply *q;
	Parameter *r;

	// Get input/output pointers
	p=(GetParUnits256Msg *)msg;
	q=(GetParUnits256Rply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
		q->result=r->getUnits(q->units, sizeof(q->units));
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(GetParUnits256Msg);
	*rplylen=sizeof(GetParUnits256Rply);

	// Return success
	return NO_ERROR;
}

int MsgAbortAdjustment(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	AbortAdjustmentMsg *p;
	AbortAdjustmentRply *q;
	Parameter *r;

	// Get input/output pointers
	p=(AbortAdjustmentMsg *)msg;
	q=(AbortAdjustmentRply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
	{
		r->abortAdjustment();
		q->result=ParOkay;
	}
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(AbortAdjustmentMsg);
	*rplylen=sizeof(AbortAdjustmentRply);

	// Return success
	return NO_ERROR;
}

int MsgGetParAddr4(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	GetParAddr4Msg *p;
	GetParAddr4Rply *q;
	Parameter *r;

	// Get input/output pointers
	p=(GetParAddr4Msg *)msg;
	q=(GetParAddr4Rply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
		q->result=r->getAddr(&q->addr, sizeof(q->addr));
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(GetParAddr4Msg);
	*rplylen=sizeof(GetParAddr4Rply);

	// Return success
	return NO_ERROR;
}

int MsgSetEvent(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	SetEventMsg *p;
	SetEventRply *q;
	Parameter *r;

	// Get input/output pointers
	p=(SetEventMsg *)msg;
	q=(SetEventRply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
		q->result=r->setEvent((int)p->eHandle, (int)p->pri);
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(SetEventMsg);
	*rplylen=sizeof(SetEventRply);

	// Return success
	return NO_ERROR;
}

int MsgSetInstant(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	SetParInstantMsg *p;
	SetParInstantRply *q;
	Parameter *r;

	// Get input/output pointers
	p=(SetParInstantMsg *)msg;
	q=(SetParInstantRply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
	{
#ifdef NOCONN
		q->result=r->setInstant(&p->value, sizeof(p->value), 1);
#else
		q->result=r->setInstant(&p->value, sizeof(p->value), ((connection *)conn)->komask);
#endif
	}
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(SetParInstantMsg);
	*rplylen=sizeof(SetParInstantRply);

	// Return success
	return NO_ERROR;
}

int MsgNormalise(Uint8 *msg, Uint8 *rply, Uint32 *msglen, Uint32 *rplylen, void *conn)
{
	NormaliseMsg *p;
	NormaliseRply *q;
	Parameter *r;

	// Get input/output pointers
	p=(NormaliseMsg *)msg;
	q=(NormaliseRply *)rply;
	r=(Parameter *)p->handle;

	// Null handle means root
	if(!r)
		r=PSupSystem;

	// Do our business
	if(r->isGood())
	{
		r->parNormaliseSignal();
		q->result=ParOkay;
	}
	else
		q->result=ParNotFound;

	// Set lengths, etc.
	q->command=p->command;
	*msglen=sizeof(NormaliseMsg);
	*rplylen=sizeof(NormaliseRply);

	// Return success
	return NO_ERROR;
}

#endif
