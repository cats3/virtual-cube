/* json.h */

class JSON {
public:
	void make_json(Parameter* p, FILE* fp);
	void init(Parameter* p);
private:
	Parameter* foo;
	void print_value(Parameter* p, FILE* fp);
	void print_type(Parameter* p, FILE* fp);
	void print_path(Parameter* p, FILE* fp);
	void print_name(Parameter* p, FILE* fp);
	void print_parameter(Parameter* p, FILE* fp);
	void print_group(Parameter* p, FILE* fp);
	void print_indent(FILE* fp);
	int indent;
};
