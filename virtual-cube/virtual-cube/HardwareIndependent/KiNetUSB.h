// KiNetUSB.h - KiNet/USB application header

#define	C3TransPathSz			256
#define	C3NormalisationBlock	100
#define	TSRKiNetSlotBs			128

class SmpC3Channel {
public:
	SmpGenerator  FunctionGenerator;
};

class SmpC3App {
public:
	void init();
	void iter()
	{
		int i;

		LimitSystem.iterBegin();
		for(i=0; i<C3AppNChans; i++)
			Channel[i].FunctionGenerator.iter();
		AdjustmentMechanism.iter();
		LimitSystem.iterEnd();
		TSRKiNetLo=(Uint16)(LimitSystem.tsrOut.state);
		TSRKiNetHi=(Uint16)((LimitSystem.tsrOut.state)>>16);
	}
	SmpAdjustment      AdjustmentMechanism;
	SmpLimitSys        LimitSystem;
	SmpC3Channel       Channel[C3AppNChans];
	Uint16             TSRKiNetLo, TSRKiNetHi;
};

#ifdef SUPGEN

class C3SignalsClass: public Parameter {
public:
	SignalArray	      Transducers;
};

class C3ChannelClass: public Parameter {
public:
	Generator   FunctionGenerator;
};

class C3ServoChClass: public Parameter {
public:
	C3ChannelClass Member[C3AppNChans];
};

class C3App: public Parameter {
public:
	void setSnvBs(void *p)
	{
		snvBs=(char *)p;
		formatSNV(p);
		snvSz=sizeSNV(0);
		snvEnd=(char *)p+snvSz-1;
	}
	void setDnvBs(void *p)
	{
		dnvBs=(char *)p;
		formatDNV(p);
		dnvSz=sizeDNV(0);
		dnvEnd=(char *)p+dnvSz-1;
	}
	void setSmpBs(Uint32 p)
	{
		smpBs=(char *)p;
		smpSz=sizeof(SmpC3App);
		smpEnd=smpBs+sizeof(SmpC3App)-1;
		smpbs=(SmpC3App *)p;
	}
	bool badSnv(void *p){return (((char *)p)<snvBs)||(((char *)p)>snvEnd);}
	bool badDnv(void *p){return (((char *)p)<dnvBs)||(((char *)p)>dnvEnd);}
	bool badSmp(void *p){return (((char *)p)<smpBs)||(((char *)p)>smpEnd);}
	void init();
	virtual void parPrimeSignal();
	virtual void parConfigureSignal();
	Adjustment           AdjustmentFeeder;
	LimitSys             LimitSystem;
	C3ServoChClass       ServoChannel;
private:
	virtual void parSNVValidateUREvent(Parameter *sce);
	virtual void parSNVValidateOREvent(Parameter *sce);
	virtual void parSNVValidateNANEvent(Parameter *sce);
	virtual void parDNVValidateUREvent(Parameter *sce);
	virtual void parDNVValidateOREvent(Parameter *sce);
	virtual void parDNVValidateNANEvent(Parameter *sce);
	virtual void parSNVModifiedEvent(Parameter *sce);
	virtual void parDNVModifiedEvent(Parameter *sce);
	virtual void parSetROEvent(Parameter *sce);
	virtual void parSetUREvent(Parameter *sce);
	virtual void parSetOREvent(Parameter *sce);
	virtual void parSetNanEvent(Parameter *sce);
	virtual void parSetLockEvent(Parameter *sce);
	virtual void parConfigChangedEvent(Parameter *sce);
	void doConfigure();
	SmpC3App  *smpbs;
	char      chName[C3AppNChans][4];
	char      *snvBs, *dnvBs, *snvEnd, *dnvEnd, *smpBs, *smpEnd;
	Uint32	  snvSz, dnvSz, smpSz;
};

#endif
