// Adjustment.cpp - Adjustment Class module

#include "C3Std.h"

#ifdef SUPGEN

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Adjustment - static stuff

Adjustment *Adjustment::IContext;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Adjustment stuff - the hardware feeder component

void Adjustment::operator()(Parameter *par, char *n, SmpAdjustment &smp)
{
	static const char *(EnableStr[])  ={"Disabled", "Enabled", NULL};
	int i;

	smpbs=&smp;
	Parameter::operator()(par, n);
	SmoothAdjustment(this, "Smooth_adjustment", "rl",  NULL, 0, 1, "yellow");
#ifdef KINETUSB
	Enable	        (this, "Enable",            "y",   NULL, (char **)EnableStr);
#endif
	OneAtATime      (this, "One_at_a_time",     "y",   NULL, (char **)EnableStr);
	Abort           (this, "Abort",             "b",   NULL, 0, 1);
	Waiting         (this, "Waiting",           "r",   NULL, 0, MaxUns, "adjustments");
	GlobalRate      (this, "Global_rate",       "y",   NULL, 0.001f, 100.0f, "%/sec",  20.0f);
	head=NULL;
	tail=NULL;
	for(i=0; i<AdjNCh; i++)
		inprogress[i]=NULL;
	stsmsk=0;
	i=AdjNCh;
	while(i--)
		stsmsk=(stsmsk<<1)|1;
	pTsrIn=LimitSys::IContext.smpCntxt?&((LimitSys::IContext.smpCntxt)->tsrInBufLo):NULL;
	Poke(smpbs->pTsrIn, pTsrIn);
	smthst=0;
}

void Adjustment::parPollSignal()
{
	Uint32 sts;

	Parameter::parPollSignal();
	if(pTsrIn)
		smthst=(Peek(*pTsrIn)&TSRSmoothEn)?1:0;
	SmoothAdjustment=smthst;
	if(!(sts=stsmsk&Peek(smpbs->sts)))
		return;
	chkCmplt(sts);
	if(!head)
		return;
	if(OneAtATime())
		sts&=1;
	if(feed(sts))
	{
		// Clear the status if any adjustments issued this visit.
		//
		// There is a hazard that newly programmed channels might not
		//  take down their available status for a whole sample period.
		//  We might get back and think these channels are free!
		//
		// Clearing the status prevents this happening. Sample processing
		//  will restore true availability status on its next iteration.
		//
		Poke(smpbs->sts, 0);
	}
}

void Adjustment::parSetEvent(Parameter *sce)
{
	if(sce==&Abort)
		chkAbrt();
}

#ifdef KINETUSB
void Adjustment::parAlteredEvent(Parameter *sce)
{
	if(sce==&Enable)
		smthst=Enable();
}
#endif

void Adjustment::chkAbrt()
{
	AdjRequester **p, *q;
	int chno;
	float inc;

	if(!Abort())
		return;
	if(q=head)
	{
		while(q)
		{
			doCallback(q);
			q->state=AdjIdle;
			Waiting=Waiting()-1;
			q=q->next;
		}
		head=tail=NULL;
	}
	p=inprogress;
	for(chno=0; chno<AdjNCh; chno++)
	{
		if((q=*p)&&(q->state==AdjExecuting))
		{
			Poke(smpbs->ch[chno].addr, NULL);
			if(q->abrtrate==0.0)
			{
				Poke(*q->addr, q->oldv);
				doCallback(q);
				*p=NULL;
				q->state=AdjIdle;
				Waiting=Waiting()-1;
			}
			else
			{
				Poke(smpbs->ch[chno].rns, q->oldv);
				inc=q->abrtrate/SysFs;
				Poke(smpbs->ch[chno].inc, inc);
				Poke(smpbs->ch[chno].last, Peek(*q->addr));
				Poke(smpbs->ch[chno].addr, q->addr);
				q->state=AdjAborting;
			}
		}
		p++;
	}
	Abort=0;
}

void Adjustment::addme(AdjRequester *p)
{
	p->next=NULL;
	if(head)
	{
		tail->next=p;
		tail=p;
	}
	else
		head=tail=p;
	p->state=AdjWaiting;
	Waiting=Waiting()+1;
}

void Adjustment::reprogram(AdjRequester *p, Float v)
{
	Poke(smpbs->ch[p->chno].addr, NULL);
	Poke(smpbs->ch[p->chno].rns, v);
	Poke(smpbs->ch[p->chno].inc, p->adjrate/SysFs);
	Poke(smpbs->ch[p->chno].addr, p->addr);
}

void Adjustment::stop(AdjRequester *p)
{
	Poke(smpbs->ch[p->chno].addr, NULL);
}

void Adjustment::chkCmplt(Uint32 sts)
{
	AdjRequester **p, *q;
	int i;

	p=inprogress;
	i=AdjNCh;
	while(i--)
	{
		if((q=*p)&&(sts&1))
		{
			if((q->state==AdjAborting)||Peek(smpbs->ch[q->chno].cntntn))
				doCallback(q);
			q->state=AdjIdle;
			*p=NULL;
			Waiting=Waiting()-1;
		}
		p++;
		sts>>=1;
	}
}

bool Adjustment::feed(Uint32 sts)
{
	bool issd;
	AdjRequester *p, **q;
	int chno;

	issd=False;
	chno=0;
	while((p=head)&&sts)
	{
		if(!(head=p->next))
			tail=NULL;
		if((p->state==AdjCancelled)||(p->newv==Peek(*p->addr)))
		{
			p->state=AdjIdle;
			Waiting=Waiting()-1;
			continue;
		}
		if(!smthst || p->adjrate==0.0)
		{
			Poke(*p->addr, p->newv);
			p->state=AdjIdle;
			Waiting=Waiting()-1;
			continue;
		}
		while(!(sts&1))
		{
			sts>>=1;
			chno++;
		}
		q=inprogress+chno;
		p->state=AdjExecuting;
		p->chno=chno;
		*q=p;
		p->oldv=Peek(*p->addr);
		Poke(smpbs->ch[chno].rns, p->newv);
		Poke(smpbs->ch[chno].inc, p->adjrate/SysFs);
		Poke(smpbs->ch[chno].last, p->oldv);
		Poke(smpbs->ch[chno].cntntn, False);
		Poke(smpbs->ch[chno].freeze, p->freeze);
		Poke(smpbs->ch[chno].addr, p->addr);
		issd=True;
		sts&=~1;
	}
	return issd;
}

void Adjustment::doCallback(AdjRequester *p)
{
	p->finalv=Peek(*p->addr);
	if(p->owner)
		p->owner->parAdjFailEvent(NULL);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Requester stuff

void AdjRequester::init(Parameter *ownr, float *a)
{
	mechanism=Adjustment::IContext;
	addr=a;
	owner=ownr;
	state=AdjIdle;
}

void AdjRequester::request(Uint32 frz, float v, float adjr, float abrtr, float rateCtrl)
{
	if(!mechanism)
	{
		Poke(*addr, v);
		return;
	}
	if(rateCtrl==0.0f)
		rateCtrl=mechanism->GlobalRate();
	adjr*=(0.1f*rateCtrl);
	abrtr*=(0.1f*rateCtrl);
	if((!mechanism->smthst)||(adjr==0.0f))
	{
		switch(state)
		{
		case AdjWaiting:
			state=AdjCancelled;
			break;
		case AdjExecuting:
			mechanism->stop(this);
			break;
		case AdjAborting:
			return;
		}
		Poke(*addr, v);
		return;
	}	
	switch(state)
	{
	case AdjIdle:
		if(v==Peek(*addr))
			return;
		newv=v;
		adjrate=adjr;
		abrtrate=abrtr;
		freeze = frz;
		mechanism->addme(this); /* HERE */
		break;
	case AdjWaiting:
		adjrate=adjr;
		abrtrate=abrtr;
		freeze = frz;
		newv=v;
		break;
	case AdjCancelled:
		adjrate=adjr;
		abrtrate=abrtr;
		freeze = frz;
		newv=v;
		state=AdjWaiting;
		break;
	case AdjExecuting:
		adjrate=adjr;
		abrtrate=abrtr;
		freeze = frz;
		mechanism->reprogram(this, v);
		break;
	}
}

#endif
