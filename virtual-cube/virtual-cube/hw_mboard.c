/********************************************************************************
 * MODULE NAME       : hw_mboard.c												*
 * PROJECT			 : Control Cube												*
 * MODULE DETAILS    : Interface routines for functions on mainboard.			*
 ********************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <math.h>
#include <stdio.h>

#include <stdint.h>
#include "porting.h"


 //#include "tcp_ip/support.h"

#include "defines.h"
#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"
#include "hydraulic.h"
#include "asmcode.h"

//#include "HardwareIndependent\Expression.h"
#include "HardwareIndependent/C3Std.h"

/* External functions required from 2gp1sv module */

void hw_2gp1sv_set_balance(chandef* def, float balance);
void hw_2gp1sv_set_raw_excitation(chandef* def, int type, int value, float phase, int offset);
void hw_2gp1sv_enable_excitation(chandef* def, int enable);
void hw_2gp1sv_read_raw_gain(chandef* def, uint32_t* coarse, uint32_t* fine);
void hw_2gp1sv_1w_init(info_2gp1sv* info, uint32_t bus);
int  hw_2gp1sv_tedsid(chandef* def, uint32_t* lo, uint32_t* hi);
void hw_2gp1sv_spi_send(info_2gp1sv* info, uint8_t data);

int hw_4ad2da_1w_reset(info_4ad2da* info);

/* Configuration EEPROM usage:

	Addr 0x00		0x01	Board type
		 0x01		0xAA	Validity code
		 0x02		0x55
		 0x03		0x00
		 0x04		0xFF
		 0x05-0x0C			Serial no. string
		 0x0D-0xFF			Board specific data
								0x0D		Bit  0		Disable SV channel
											Bits 1-7	Reserved
								0x0E-0xFF				Reserved
*/


/* Hardware input/output lines */

#define CALPOL		0			/* Calibration polarity select relay 0=NEG,1=POS	*/
#define CALEN		1			/* Calibration enable relay							*/
#define GAIN400_0	2			/* Input 0 gain x400 select relay					*/
#define GAIN20_0	3			/* Input 0 gain x20 select relay					*/
#define GAIN400_1	4			/* Input 1 gain x400 select relay					*/
#define GAIN20_1	5			/* Input 1 gain x20 select relay					*/

#define RLYMASKCAL	((1 << CALPOL) | (1 << CALEN))


/********************************************************************************
  FUNCTION NAME     : hw_mboard_cal_ctrl
  FUNCTION DETAILS  : Control the calibration relays for the selected
					  transducer channel.

					  On entry:

					  def		Points to the channel definition structure
					  state		Calibration state
									Bit 0		Enabled
									Bit 1		0 = Positive, 1 = Negative
									Bits 2..7	Reserved

********************************************************************************/

void hw_mboard_cal_ctrl(chandef* def, int state)
{
	info_2gp1sv* i;
	uint8_t calrly;
	int istate;

	i = &def->hw.i_2gp1sv;	/* 2GP1SV specific information 	*/

	/* Update the state of the chandef ctrl word and the corresponding word in DSP B memory */

	istate = disable_int();

	def->ctrl &= ~(FLAG_SHUNTCAL | FLAG_NEGSHUNTCAL);		/* Clear FLAG_SHUNTCAL and FLAG_NEGSHUNTCAL 	 */
	def->ctrl |= ((state & 0x01) ? FLAG_SHUNTCAL : 0) |		/* Set FLAG_SHUNTCAL if enabled					 */
		((state & 0x02) ? FLAG_NEGSHUNTCAL : 0);	/* Set FLAG_NEGSHUNTCAL if negative cal selected */

	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(uint32_t));

	restore_int(istate);

	/* If channel is inverted, then shunt calibration polarity must also be inverted */

	if (def->ctrl & FLAG_POLARITY) state ^= 0x02;

	/* Adjust outputs to allow for hardware differences on rev A hardware */

	if (get_hw_version(NULL, NULL) > 1) {

		/* Rev B and later hardware has a correctly wired calibration enable relay */

		calrly = (state & 0x01) ? ((1 << CALEN) | ((state & 0x02) ? 0 : (1 << CALPOL))) : 0;

	}
	else {

		/* Rev A hardware has an inverted calibration enable relay */

		calrly = ((state & 0x01) ? 0 : (1 << CALEN)) | ((state & 0x02) ? 0 : (1 << CALPOL));

	}

	/* Clear existing relay state */

	*(i->relaystate) &= ~RLYMASKCAL;

	/* Update new settings */

	*(i->relaystate) |= calrly;

	/* Write new state to relay control outputs */

	hw_2gp1sv_spi_send(i, *(i->relaystate));

	grp_update_shuntcal(chan_check_state(FLAG_SHUNTCAL)); // chan_check_state will return true if this flag is set anywhere. If it is, then it will enable a global flag to stop hydraulics.
}



/********************************************************************************
  FUNCTION NAME     : hw_mboard_led_ctrl
  FUNCTION DETAILS  : Control LEDs on mainboard.
********************************************************************************/

void hw_mboard_led_ctrl(chandef* def, int state)
{
	//fprintf(stdout, "led:%d|%d\n", def->identifier, state);
	//info_mboard* i;
	//int drive;
	//uint8_t newstate;
	//uint8_t response;

	//i = &def->hw.i_mboard;	/* Mainboard specific information	*/

	//switch (def->type) {
	//case TYPE_GP:
	//	switch (i->input) {
	//	case 0:
	//	case 1:
	//		drive = (i->input) ? TXDR2LED_DRIVE : TXDR1LED_DRIVE;
	//		local_io(state ? drive : 0, state ? 0 : drive);

	//		/* Deliberate fall-through to handle DS2408 driver case also */

	//	case 2:
	//	case 3:
	//		newstate = *(i->ledstate) & ~(i->ledmask);	/* Clear current LED state 	*/
	//		newstate |= (state) ? (i->ledmask) : 0;		/* Update with new state	*/

	//		*(i->ledstate) = newstate;

	//		if (hw_4ad2da_1w_reset((info_4ad2da*)i)) return;	/* Return if nothing present on bus 	*/

	//		/* When sending the state to the DS2408 a zero output turns the LED on.
	//		   Therefore we must send the inverse of the newstate value.
	//		*/

	//		hw_4ad2da_1w_write(i, 0xCC);		/* Send SKIP ROM command				*/
	//		hw_4ad2da_1w_write(i, 0x5A);		/* Send CHANNEL ACCESS WRITE command	*/
	//		hw_4ad2da_1w_write(i, ~newstate);	/* Send data							*/
	//		hw_4ad2da_1w_write(i, newstate);	/* Send inverted data					*/

	//		/* Check for 0xAA byte indicating successful write, then read back PIO
	//		   state to check for errors.
	//		*/

	//		response = hw_4ad2da_1w_read(i);
	//		if (response == 0xAA) {
	//			if (hw_4ad2da_1w_read(i) == ~newstate) {
	//			}
	//		}

	//		hw_4ad2da_1w_reset(i);	/* Reset 1-wire bus */
	//		break;
	//	}
	//	break;
	//case TYPE_ADC:
	//	switch (i->input) {
	//	case 0:
	//	case 1:
	//	case 2:
	//	case 3:
	//		newstate = *(i->ledstate) & ~(i->ledmask);	/* Clear current LED state 	*/
	//		newstate |= (state) ? (i->ledmask) : 0;		/* Update with new state	*/

	//		*(i->ledstate) = newstate;

	//		if (hw_4ad2da_1w_reset(i)) return;	/* Return if nothing present on bus 	*/

	//		/* When sending the state to the DS2408 a zero output turns the LED on.
	//		   Therefore we must send the inverse of the newstate value.
	//		*/

	//		hw_4ad2da_1w_write(i, 0xCC);		/* Send SKIP ROM command				*/
	//		hw_4ad2da_1w_write(i, 0x5A);		/* Send CHANNEL ACCESS WRITE command	*/
	//		hw_4ad2da_1w_write(i, ~newstate);	/* Send data							*/
	//		hw_4ad2da_1w_write(i, newstate);	/* Send inverted data					*/

	//		/* Check for 0xAA byte indicating successful write, then read back PIO
	//		   state to check for errors.
	//		*/

	//		response = hw_4ad2da_1w_read(i);
	//		if (response == 0xAA) {
	//			if (hw_4ad2da_1w_read(i) == ~newstate) {
	//			}
	//		}

	//		hw_4ad2da_1w_reset(i);	/* Reset 1-wire bus */
	//		break;
	//	}
	//	break;
	//case TYPE_DAC:
	//	switch (i->output) {
	//	case 0:
	//	case 1:
	//		newstate = *(i->ledstate) & ~(i->ledmask);	/* Clear current LED state 	*/
	//		newstate |= (state) ? (i->ledmask) : 0;		/* Update with new state	*/

	//		*(i->ledstate) = newstate;

	//		if (hw_4ad2da_1w_reset(i)) return;	/* Return if nothing present on bus 	*/

	//		/* When sending the state to the DS2408 a zero output turns the LED on.
	//		   Therefore we must send the inverse of the newstate value.
	//		*/

	//		hw_4ad2da_1w_write(i, 0xCC);		/* Send SKIP ROM command				*/
	//		hw_4ad2da_1w_write(i, 0x5A);		/* Send CHANNEL ACCESS WRITE command	*/
	//		hw_4ad2da_1w_write(i, ~newstate);	/* Send data							*/
	//		hw_4ad2da_1w_write(i, newstate);	/* Send inverted data					*/

	//		/* Check for 0xAA byte indicating successful write, then read back PIO
	//		   state to check for errors.
	//		*/

	//		response = hw_4ad2da_1w_read(i);
	//		if (response == 0xAA) {
	//			if (hw_4ad2da_1w_read(i) == ~newstate) {
	//			}
	//		}

	//		hw_4ad2da_1w_reset(i);	/* Reset 1-wire bus */
	//		break;
	//	}
	//	break;
	//case TYPE_SV:
	//	local_io(state ? SVLED_DRIVE : 0, state ? 0 : SVLED_DRIVE);
	//	break;
	//}
}



/* Table to convert gaintable index into corresponding relay states. Note that
   the table uses the relays for input channel 0. These must be shifted left
   two places to convert for input channel 1.
*/

static uint8_t gainctrl[] = { 0,
						   1 << GAIN20_0,
						   1 << GAIN400_0,
						   0
};

#define RLYMASK0	((1 << GAIN20_0) | (1 << GAIN400_0))
#define RLYMASK1	((1 << GAIN20_1) | (1 << GAIN400_1))



/********************************************************************************
  FUNCTION NAME     : hw_mboard_set_raw_gain
  FUNCTION DETAILS  : Set the raw gain values for the selected transducer
					  channel.

					  On entry:

					  def		Points to the channel definition structure
					  coarse	Coarse gain setting
									0 = x1
									1 = x20
									2 = x400
					  fine		Fine gain setting (0 to 255)

********************************************************************************/

void hw_mboard_set_raw_gain(chandef* def, uint32_t coarse, uint32_t fine)
{
	info_mboard* i;
	cfg_gp* cfg;
	uint8_t gainrly;

	i = &def->hw.i_mboard;	/* Mainboard specific information	*/
	cfg = &def->cfg.i_gp;	/* GP specific configuration		*/

	cfg->coarsegain = (coarse > 2) ? 0 : coarse;
	cfg->finegain = fine & 0xFF;

	/* Convert coarse gain setting into required relay state */

	gainrly = gainctrl[cfg->coarsegain] << (i->input ? 2 : 0);

	/* Clear current coarse settings for this input */

	*(i->relaystate) &= ~(i->input ? RLYMASK1 : RLYMASK0);

	/* Update new settings for this input */

	*(i->relaystate) |= gainrly;

	/* Write new state to relay control outputs */

	//hw_2gp1sv_spi_send(i, *(i->relaystate));

	///* Update fine hardware gain setting */

	//hw_2gp1sv_i2c_start(i);
	//hw_2gp1sv_i2c_send(i, i->input ? 0x5A : 0x58);
	//hw_2gp1sv_i2c_send(i, 0x00);
	//hw_2gp1sv_i2c_send(i, (255 - cfg->finegain) & 0xFF);
	//hw_2gp1sv_i2c_stop(i);

	/* Update saturation detection thresholds depending on selected
	   fine gain setting.
	*/

	if (fine < 2) {
		def->satpos = 470000;
		def->satneg = -470000;
	}
	else {
		def->satpos = 513800;
		def->satneg = -511700;
	}
	update_shared_channel_parameter(def, offsetof(chandef, satpos), sizeof(int) * 2);

}



/********************************************************************************
  FUNCTION NAME     : hw_mboard_set_raw_current
  FUNCTION DETAILS  : Set the current range the servo-valve drive output.

					  On entry:

					  def		Points to the channel definition structure
					  current	Current range (0 to 255)

********************************************************************************/

void hw_mboard_set_raw_current(chandef* def, int current)
{
	fprintf(stdout, "%s: [%d]\n", __FUNCTION__, current);
	//info_2gp1sv* i;

	//i = &def->hw.i_2gp1sv;	/* 2GP1SV specific information 	*/

	///* Update current drive setting */

	//hw_2gp1sv_i2c_start(i);
	//hw_2gp1sv_i2c_send(i, 0x5E);
	//hw_2gp1sv_i2c_send(i, 0x00);
	//hw_2gp1sv_i2c_send(i, current & 0xFF);
	//hw_2gp1sv_i2c_stop(i);
}



/********************************************************************************
  FUNCTION NAME     : hw_mboard_io_write_output
  FUNCTION DETAILS  : Write the selected digital I/O channel output.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void hw_mboard_io_write_output(chandef* def, uint32_t output)
{
	uint32_t op = (output & DIGIO_INVERT) ^ (def->cfg.i_io.flags & DIGIO_INVERT);
	local_io(((op) ? def->cfg.i_io.mask : 0), ((op) ? 0 : def->cfg.i_io.mask));
}



/********************************************************************************
  FUNCTION NAME     : hw_mboard_io_read_input
  FUNCTION DETAILS  : Read the selected digital I/O channel input.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void hw_mboard_io_read_input(chandef* def, uint32_t* input)
{
	if (input) *input = ((hyd_io_state & def->cfg.i_io.mask) ? 1 : 0) ^
		(def->cfg.i_io.flags & DIGIO_INVERT);
}



/********************************************************************************
  FUNCTION NAME     : hw_mboard_io_read_output
  FUNCTION DETAILS  : Read the selected digital I/O channel output.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void hw_mboard_io_read_output(chandef* def, uint32_t* output)
{
	if (output) *output = (local_io_rdop() & def->cfg.i_io.mask) ? 1 : 0;
}



/*******************************************************************************
 FUNCTION NAME	   : hw_mboard_io_set_cnet_output
 FUNCTION DETAILS  : Sets the CNet mapping for digital input channels.

					 On entry:

					 def	Pointer to channel being mapped.

					 slot	Destination CNet output slot.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void hw_mboard_io_set_cnet_output(chandef* def, int slot)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	slot_status[0].digip_cnetptr = cnet_get_slotaddr(slot, PROC_DSPA);
	slot_status[0].digip_srcptr = &dig_ip_state;
#endif
}






/* Table to convert from channel number in the range 0 to 5 into the bit position
   required to drive the LED. Note that channels 0,1,2,3 correspond to the four
   input channels, whilst channels 4 and 5 correspond to the two output channels.
*/

int mboard_led_conversion[] = { 0,1,3,4,2,5 };



/****************************************************
* Table of handler functions for mainboard channels *
****************************************************/

void* mboard_gp_fntable[] = {
  &read_chan_status,				/* Fn  0. Ptr to readstatus function 				*/
  &chan_gp_set_pointer,				/* Fn  1. Ptr to setpointer function				*/
  &chan_gp_get_pointer,				/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  &save_chan_calibration,			/* Fn  6. Ptr to savecalibration function 			*/
  &restore_chan_calibration,		/* Fn  7. Ptr to restorecalibration function 		*/
  &flush_chan_calibration,			/* Fn  8. Ptr to flushcalibration function 			*/
  &chan_gp_set_excitation,			/* Fn  9. Ptr to setexc function 					*/
  &chan_gp_read_excitation,			/* Fn 10. Ptr to readexc function 					*/
  &chan_gp_set_exccal,				/* Fn 11. Ptr to setexccal function 				*/
  &chan_gp_read_exccal,				/* Fn 12. Ptr to readexccal function 				*/
  &chan_gp_set_gain,				/* Fn 13. Ptr to setgain function 					*/
  &chan_gp_read_gain,				/* Fn 14. Ptr to readgain function 					*/
  &hw_2gp1sv_set_balance,			/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  &hw_mboard_cal_ctrl,				/* Fn 17. Ptr to calctrl function 					*/
  &hw_mboard_led_ctrl,				/* Fn 18. Ptr to ledctrl function 					*/
  &hw_2gp1sv_set_raw_excitation,	/* Fn 19. Ptr to setrawexc function 				*/
  &hw_2gp1sv_enable_excitation,		/* Fn 20. Ptr to enableexc function 				*/
  &hw_mboard_set_raw_gain,			/* Fn 21. Ptr to setrawgain function 				*/
  &hw_2gp1sv_read_raw_gain,			/* Fn 22. Ptr to readrawgain function 				*/
  &chan_read_adc,					/* Fn 23. Ptr to readadc function 					*/
  &set_filter,						/* Fn 24. Ptr to setfilter function 				*/
  &read_filter,						/* Fn 25. Ptr to readfilter function 				*/
  NULL,								/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function	 			*/
  &chan_gp_set_caltable,			/* Fn 28. Ptr to set_caltable function 				*/
  &chan_gp_read_caltable,			/* Fn 29. Ptr to read_caltable function 			*/
  &chan_gp_set_calgain,				/* Fn 30. Ptr to set_calgain function 				*/
  &chan_gp_read_calgain,			/* Fn 31. Ptr to read_calgain function 				*/
  &chan_gp_set_caloffset,			/* Fn 32. Ptr to set_caloffset function 			*/
  &chan_gp_read_caloffset,			/* Fn 33. Ptr to read_caloffset function	 		*/
  &chan_set_cnet_output,			/* Fn 34. Ptr to set_cnet_slot function 			*/
  &chan_gp_set_gaintrim,			/* Fn 35. Ptr to set_gaintrim function 				*/
  &chan_gp_read_gaintrim,			/* Fn 36. Ptr to read_gaintrim function	 			*/
  &set_ip_offsettrim,				/* Fn 37. Ptr to set_offsettrim function 			*/
  &read_ip_offsettrim,				/* Fn 38. Ptr to read_offsettrim function 			*/
  &chan_gp_set_txdrzero,			/* Fn 39. Ptr to set_txdrzero function				*/
  &chan_gp_read_txdrzero,			/* Fn 40. Ptr to read_txdrzero function				*/
  &read_peakinfo,					/* Fn 41. Ptr to readpeakinfo function 				*/
  &reset_peaks,						/* Fn 42. Ptr to resetpeak function 				*/
  &chan_gp_set_flags,			 	/* Fn 43. Ptr to set_flags function 				*/
  &chan_gp_read_flags,				/* Fn 44. Ptr to read_flags function 				*/
  &chan_gp_write_map,				/* Fn 45. Ptr to write_map function 				*/
  &chan_gp_read_map,				/* Fn 46. Ptr to read_map function 					*/
  &chan_gp_set_refzero,				/* Fn 47. Ptr to set_refzero function 				*/
  &read_refzero,					/* Fn 48. Ptr to read_refzero function 				*/
  &undo_refzero,					/* Fn 49. Ptr to undo_refzero function 				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  &hw_2gp1sv_tedsid,				/* Fn 52. Ptr to tedsid function 					*/
  NULL,								/* Fn 53. Ptr to set source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  NULL,								/* Fn 58. Ptr to set info function					*/
  NULL,								/* Fn 59. Ptr to read info function					*/
  NULL,								/* Fn 60. Ptr to write output function				*/
  NULL,								/* Fn 61. Ptr to read input function				*/
  NULL,								/* Fn 62. Ptr to read output function				*/
  &chan_gp_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_gp_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  &chan_gp_txdrzero_zero,			/* Fn 72. Ptr to txdrzero_zero function				*/
  &chan_read_cyclecount,			/* Fn 73. Ptr to readcycle function					*/
  &reset_cycles,					/* Fn 74. Ptr to resetcycles function 				*/
  &chan_gp_set_peak_threshold,		/* Fn 75. Ptr to setpkthreshold function 			*/
  &chan_gp_read_peak_threshold,		/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_gp_write_faultmask,			/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_gp_read_faultstate,			/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  &chan_gp_refzero_zero,			/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};

void* mboard_ad_fntable[] = {
  &read_chan_status,				/* Fn  0. Ptr to readstatus function 				*/
  &chan_ad_set_pointer,				/* Fn  1. Ptr to setpointer function				*/
  &chan_ad_get_pointer,				/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  &save_chan_calibration,			/* Fn  6. Ptr to savecalibration function 			*/
  &restore_chan_calibration,		/* Fn  7. Ptr to restorecalibration function 		*/
  &flush_chan_calibration,			/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  &chan_ad_set_gain,				/* Fn 13. Ptr to setgain function 					*/
  &chan_ad_read_gain,				/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  &hw_mboard_led_ctrl,				/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  &chan_read_adc,					/* Fn 23. Ptr to readadc function 					*/
  &set_filter,						/* Fn 24. Ptr to setfilter function	 				*/
  &read_filter,						/* Fn 25. Ptr to readfilter function 				*/
  NULL,								/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function 			*/
  &chan_ad_set_caltable,			/* Fn 28. Ptr to set_caltable function 				*/
  &chan_ad_read_caltable,			/* Fn 29. Ptr to read_caltable function 			*/
  &chan_ad_set_calgain,				/* Fn 30. Ptr to set_calgain function 				*/
  &chan_ad_read_calgain,			/* Fn 31. Ptr to read_calgain function 				*/
  &chan_ad_set_caloffset,			/* Fn 32. Ptr to set_caloffset function 			*/
  &chan_ad_read_caloffset,			/* Fn 33. Ptr to read_caloffset function 			*/
  &chan_set_cnet_output,			/* Fn 34. Ptr to set_cnet_slot function 			*/
  &chan_ad_set_gaintrim,			/* Fn 35. Ptr to set_gaintrim function 				*/
  &chan_ad_read_gaintrim,			/* Fn 36. Ptr to read_gaintrim function 			*/
  &set_ip_offsettrim,				/* Fn 37. Ptr to set_offsettrim function	 		*/
  &read_ip_offsettrim,				/* Fn 38. Ptr to read_offsettrim function 			*/
  &chan_ad_set_txdrzero,			/* Fn 39. Ptr to set_txdrzero function				*/
  &chan_ad_read_txdrzero,			/* Fn 40. Ptr to read_txdrzero function				*/
  &read_peakinfo,					/* Fn 41. Ptr to readpeakinfo function 				*/
  &reset_peaks,						/* Fn 42. Ptr to resetpeak function 				*/
  &chan_ad_set_flags,				/* Fn 43. Ptr to set_flags function 				*/
  &chan_ad_read_flags,				/* Fn 44. Ptr to read_flags function 				*/
  &chan_ad_write_map,				/* Fn 45. Ptr to write_map function 				*/
  &chan_ad_read_map,				/* Fn 46. Ptr to read_map function 					*/
  &chan_ad_set_refzero,				/* Fn 47. Ptr to set_refzero function 				*/
  &read_refzero,					/* Fn 48. Ptr to read_refzero function 				*/
  &undo_refzero,					/* Fn 49. Ptr to undo_refzero function 				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  NULL,								/* Fn 52. Ptr to tedsid function 					*/
  NULL,								/* Fn 53. Ptr to set source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  NULL,								/* Fn 58. Ptr to set info function					*/
  NULL,								/* Fn 59. Ptr to read info function					*/
  NULL,								/* Fn 60. Ptr to write output function				*/
  NULL,								/* Fn 61. Ptr to read input function				*/
  NULL,								/* Fn 62. Ptr to read output function				*/
  &chan_ad_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_ad_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  &chan_ad_txdrzero_zero,			/* Fn 72. Ptr to txdrzero_zero function				*/
  &chan_read_cyclecount,			/* Fn 73. Ptr to readcycle function					*/
  &reset_cycles,					/* Fn 74. Ptr to resetcycles function 				*/
  &chan_ad_set_peak_threshold,		/* Fn 75. Ptr to setpkthreshold function 			*/
  &chan_ad_read_peak_threshold,		/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_ad_write_faultmask,			/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_ad_read_faultstate,			/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  &chan_ad_refzero_zero,			/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};

void* mboard_da_fntable[] = {
  &read_chan_status,				/* Fn  0. Ptr to readstatus function 				*/
  &chan_da_set_pointer,				/* Fn  1. Ptr to setpointer function				*/
  &chan_da_get_pointer,				/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  &save_chan_calibration,			/* Fn  6. Ptr to savecalibration function 			*/
  &restore_chan_calibration,		/* Fn  7. Ptr to restorecalibration function 		*/
  &flush_chan_calibration,			/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  &chan_da_set_gain,				/* Fn 13. Ptr to setgain function 					*/
  &chan_da_read_gain,				/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  &hw_mboard_led_ctrl,				/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  NULL,								/* Fn 23. Ptr to readadc function 					*/
  &set_filter,						/* Fn 24. Ptr to setfilter function 				*/
  &read_filter,						/* Fn 25. Ptr to readfilter function 				*/
  &chan_write_dac,					/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function	 			*/
  &chan_da_set_caltable,			/* Fn 28. Ptr to set_caltable function 				*/
  &chan_da_read_caltable,			/* Fn 29. Ptr to read_caltable function 			*/
  &chan_da_set_calgain,				/* Fn 30. Ptr to set_calgain function 				*/
  &chan_da_read_calgain,			/* Fn 31. Ptr to read_calgain function 				*/
  &chan_da_set_caloffset,			/* Fn 32. Ptr to set_caloffset function 			*/
  &chan_da_read_caloffset,			/* Fn 33. Ptr to read_caloffset function	 		*/
  &chan_set_cnet_input,				/* Fn 34. Ptr to set_cnet_slot function 			*/
  &chan_da_set_gaintrim,			/* Fn 35. Ptr to set_gaintrim function 				*/
  &chan_da_read_gaintrim,			/* Fn 36. Ptr to read_gaintrim function	 			*/
  &set_op_offsettrim,				/* Fn 37. Ptr to set_offsettrim function 			*/
  &read_op_offsettrim,				/* Fn 38. Ptr to read_offsettrim function 			*/
  NULL,								/* Fn 39. Ptr to set_txdrzero function				*/
  NULL,								/* Fn 40. Ptr to read_txdrzero function				*/
  NULL,								/* Fn 41. Ptr to readpeak function 					*/
  NULL,								/* Fn 42. Ptr to resetpeak function 				*/
  &chan_da_set_flags,				/* Fn 43. Ptr to set_flags function 				*/
  &chan_da_read_flags,				/* Fn 44. Ptr to read_flags function 				*/
  NULL,								/* Fn 45. Ptr to write_map function 				*/
  NULL,								/* Fn 46. Ptr to read_map function 					*/
  NULL,								/* Fn 47. Ptr to set_refzero function 				*/
  NULL,								/* Fn 48. Ptr to read_refzero function 				*/
  NULL,								/* Fn 49. Ptr to undo_refzero function 				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  NULL,								/* Fn 52. Ptr to tedsid function 					*/
  &chan_set_source,					/* Fn 53. Ptr to set_source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  NULL,								/* Fn 58. Ptr to set info function					*/
  NULL,								/* Fn 59. Ptr to read info function					*/
  NULL,								/* Fn 60. Ptr to write output function				*/
  NULL,								/* Fn 61. Ptr to read input function				*/
  NULL,								/* Fn 62. Ptr to read output function				*/
  &chan_da_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_da_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  NULL,								/* Fn 72. Ptr to txdrzero_zero function				*/
  NULL,								/* Fn 73. Ptr to readcycle function					*/
  NULL,								/* Fn 74. Ptr to resetcycles function 				*/
  NULL,								/* Fn 75. Ptr to setpkthreshold function 			*/
  NULL,								/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_da_write_faultmask,			/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_da_read_faultstate,			/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  NULL,								/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};

void* mboard_sv_fntable[] = {
  &read_chan_status,				/* Fn  0. Ptr to readstatus function 				*/
  &chan_sv_set_pointer,				/* Fn  1. Ptr to setpointer function				*/
  &chan_sv_get_pointer,				/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  &save_chan_calibration,			/* Fn  6. Ptr to savecalibration function 			*/
  &restore_chan_calibration,		/* Fn  7. Ptr to restorecalibration function 		*/
  &flush_chan_calibration,			/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  &chan_sv_set_gain,				/* Fn 13. Ptr to setgain function 					*/
  &chan_sv_read_gain,				/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  &hw_mboard_led_ctrl,				/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  NULL,								/* Fn 23. Ptr to readadc function 					*/
  NULL,								/* Fn 24. Ptr to setfilter function 				*/
  NULL,								/* Fn 25. Ptr to readfilter function 				*/
  &chan_write_dac,					/* Fn 26. Ptr to writedac function 					*/
  &hw_mboard_set_raw_current,		/* Fn 27. Ptr to setrawcurrent function 			*/
  &chan_sv_set_caltable,			/* Fn 28. Ptr to set_caltable function 				*/
  &chan_sv_read_caltable,			/* Fn 29. Ptr to read_caltable function 			*/
  &chan_sv_set_calgain,				/* Fn 30. Ptr to set_calgain function 				*/
  &chan_sv_read_calgain,			/* Fn 31. Ptr to read_calgain function 				*/
  &chan_sv_set_caloffset,			/* Fn 32. Ptr to set_caloffset function 			*/
  &chan_sv_read_caloffset,			/* Fn 33. Ptr to read_caloffset function 			*/
  NULL,								/* Fn 34. Ptr to set_cnet_slot function 			*/
  &chan_sv_set_gaintrim,			/* Fn 35. Ptr to set_gaintrim function 				*/
  &chan_sv_read_gaintrim,			/* Fn 36. Ptr to read_gaintrim function	 			*/
  &set_op_offsettrim,				/* Fn 37. Ptr to set_offsettrim function 			*/
  &read_op_offsettrim,				/* Fn 38. Ptr to read_offsettrim function 			*/
  NULL,								/* Fn 39. Ptr to set_txdrzero function				*/
  NULL,								/* Fn 40. Ptr to read_txdrzero function				*/
  NULL,								/* Fn 41. Ptr to readpeak function 					*/
  NULL,								/* Fn 42. Ptr to resetpeak function 				*/
  &chan_sv_set_flags,				/* Fn 43. Ptr to set_flags function 				*/
  &chan_sv_read_flags,				/* Fn 44. Ptr to read_flags function 				*/
  NULL,								/* Fn 45. Ptr to write_map function 				*/
  NULL,								/* Fn 46. Ptr to read_map function 					*/
  NULL,								/* Fn 47. Ptr to set_refzero function 				*/
  NULL,								/* Fn 48. Ptr to read_refzero function 				*/
  NULL,								/* Fn 49. Ptr to undo_refzero function 				*/
  &chan_sv_set_current,				/* Fn 50. Ptr to set_current function				*/
  &chan_sv_read_current,			/* Fn 51. Ptr to read_current function				*/
  NULL,								/* Fn 52. Ptr to tedsid function 					*/
  &chan_set_source,					/* Fn 53. Ptr to set_source function				*/
  &chan_sv_set_dither,				/* Fn 54. Ptr to set dither function				*/
  &chan_sv_read_dither,				/* Fn 55. Ptr to read dither function				*/
  &chan_sv_set_balance,				/* Fn 56. Ptr to set balance function				*/
  &chan_sv_read_balance,			/* Fn 57. Ptr to read balance function				*/
  NULL,								/* Fn 58. Ptr to set info function					*/
  NULL,								/* Fn 59. Ptr to read info function					*/
  NULL,								/* Fn 60. Ptr to write output function				*/
  NULL,								/* Fn 61. Ptr to read input function				*/
  NULL,								/* Fn 62. Ptr to read output function				*/
  &chan_sv_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_sv_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  NULL,								/* Fn 72. Ptr to txdrzero_zero function				*/
  NULL,								/* Fn 73. Ptr to readcycle function					*/
  NULL,								/* Fn 74. Ptr to resetcycles function 				*/
  NULL,								/* Fn 75. Ptr to setpkthreshold function 			*/
  NULL,								/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_sv_write_faultmask,			/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_sv_read_faultstate,			/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  NULL,								/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};

void* mboard_digip_fntable[] = {
  NULL,								/* Fn  0. Ptr to readstatus function 				*/
  NULL,								/* Fn  1. Ptr to setpointer function				*/
  NULL,								/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  NULL,								/* Fn  6. Ptr to savecalibration function 			*/
  NULL,								/* Fn  7. Ptr to restorecalibration function 		*/
  NULL,								/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function	 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  NULL,								/* Fn 13. Ptr to setgain function 					*/
  NULL,								/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  NULL,								/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  NULL,								/* Fn 23. Ptr to readadc function 					*/
  NULL,								/* Fn 24. Ptr to setfilter function	 				*/
  NULL,								/* Fn 25. Ptr to readfilter function 				*/
  NULL,								/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function 			*/
  NULL,								/* Fn 28. Ptr to set_caltable function 				*/
  NULL,								/* Fn 29. Ptr to read_caltable function 			*/
  NULL,								/* Fn 30. Ptr to set_calgain function 				*/
  NULL,								/* Fn 31. Ptr to read_calgain function 				*/
  NULL,								/* Fn 32. Ptr to set_caloffset function 			*/
  NULL,								/* Fn 33. Ptr to read_caloffset function 			*/
  hw_mboard_io_set_cnet_output,		/* Fn 34. Ptr to set_cnet_slot function 			*/
  NULL,								/* Fn 35. Ptr to set_gaintrim function 				*/
  NULL,								/* Fn 36. Ptr to read_gaintrim function 			*/
  NULL,								/* Fn 37. Ptr to set_offsettrim function 			*/
  NULL,								/* Fn 38. Ptr to read_offsettrim function 			*/
  NULL,								/* Fn 39. Ptr to set_txdrzero function				*/
  NULL,								/* Fn 40. Ptr to read_txdrzero function				*/
  NULL,								/* Fn 41. Ptr to readpeak function 					*/
  NULL,								/* Fn 42. Ptr to resetpeak function 				*/
  NULL,								/* Fn 43. Ptr to set_flags function 				*/
  NULL,								/* Fn 44. Ptr to read_flags function 				*/
  NULL,								/* Fn 45. Ptr to write_map function 				*/
  NULL,								/* Fn 46. Ptr to read_map function 					*/
  NULL,								/* Fn 47. Ptr to set_refzero function 				*/
  NULL,								/* Fn 48. Ptr to read_refzero function 				*/
  NULL,								/* Fn 49. Ptr to undo_refzero function 				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  NULL,								/* Fn 52. Ptr to tedsid function 					*/
  NULL,								/* Fn 53. Ptr to set_source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  &chan_io_set_info,				/* Fn 58. Ptr to set info function					*/
  &chan_io_read_info,				/* Fn 59. Ptr to read info function					*/
  &hw_mboard_io_write_output,		/* Fn 60. Ptr to write output function				*/
  &hw_mboard_io_read_input,			/* Fn 61. Ptr to read input function				*/
  &hw_mboard_io_read_output,		/* Fn 62. Ptr to read output function				*/
  &chan_io_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_io_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  NULL,								/* Fn 72. Ptr to txdrzero_zero function				*/
  NULL,								/* Fn 73. Ptr to readcycle function					*/
  NULL,								/* Fn 74. Ptr to resetcycles function 				*/
  NULL,								/* Fn 75. Ptr to setpkthreshold function 			*/
  NULL,								/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_io_write_faultmask,			/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_io_read_faultstate,			/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  NULL,								/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};

void* mboard_digop_fntable[] = {
  NULL,								/* Fn  0. Ptr to readstatus function 				*/
  NULL,								/* Fn  1. Ptr to setpointer function				*/
  NULL,								/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  NULL,								/* Fn  6. Ptr to savecalibration function 			*/
  NULL,								/* Fn  7. Ptr to restorecalibration function 		*/
  NULL,								/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function	 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  NULL,								/* Fn 13. Ptr to setgain function 					*/
  NULL,								/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  NULL,								/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  NULL,								/* Fn 23. Ptr to readadc function 					*/
  NULL,								/* Fn 24. Ptr to setfilter function	 				*/
  NULL,								/* Fn 25. Ptr to readfilter function 				*/
  NULL,								/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function 			*/
  NULL,								/* Fn 28. Ptr to set_caltable function 				*/
  NULL,								/* Fn 29. Ptr to read_caltable function 			*/
  NULL,								/* Fn 30. Ptr to set_calgain function 				*/
  NULL,								/* Fn 31. Ptr to read_calgain function 				*/
  NULL,								/* Fn 32. Ptr to set_caloffset function 			*/
  NULL,								/* Fn 33. Ptr to read_caloffset function 			*/
  NULL,								/* Fn 34. Ptr to set_cnet_slot function 			*/
  NULL,								/* Fn 35. Ptr to set_gaintrim function 				*/
  NULL,								/* Fn 36. Ptr to read_gaintrim function 			*/
  NULL,								/* Fn 37. Ptr to set_offsettrim function 			*/
  NULL,								/* Fn 38. Ptr to read_offsettrim function 			*/
  NULL,								/* Fn 39. Ptr to set_txdrzero function				*/
  NULL,								/* Fn 40. Ptr to read_txdrzero function				*/
  NULL,								/* Fn 41. Ptr to readpeak function 					*/
  NULL,								/* Fn 42. Ptr to resetpeak function 				*/
  NULL,								/* Fn 43. Ptr to set_flags function 				*/
  NULL,								/* Fn 44. Ptr to read_flags function 				*/
  NULL,								/* Fn 45. Ptr to write_map function 				*/
  NULL,								/* Fn 46. Ptr to read_map function 					*/
  NULL,								/* Fn 47. Ptr to set_refzero function 				*/
  NULL,								/* Fn 48. Ptr to read_refzero function 				*/
  NULL,								/* Fn 49. Ptr to undo_refzero function 				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  NULL,								/* Fn 52. Ptr to tedsid function 					*/
  NULL,								/* Fn 53. Ptr to set_source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  &chan_io_set_info,				/* Fn 58. Ptr to set info function					*/
  &chan_io_read_info,				/* Fn 59. Ptr to read info function					*/
  &hw_mboard_io_write_output,		/* Fn 60. Ptr to write output function				*/
  &hw_mboard_io_read_input,			/* Fn 61. Ptr to read input function				*/
  &hw_mboard_io_read_output,		/* Fn 62. Ptr to read output function				*/
  &chan_io_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_io_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  NULL,								/* Fn 72. Ptr to txdrzero_zero function				*/
  NULL,								/* Fn 73. Ptr to readcycle function					*/
  NULL,								/* Fn 74. Ptr to resetcycles function 				*/
  NULL,								/* Fn 75. Ptr to setpkthreshold function 			*/
  NULL,								/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_io_write_faultmask,			/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_io_read_faultstate,			/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  NULL,								/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};

/****************************************************
* Table of handler functions for virtual channels   *
****************************************************/

void* mboard_virtual_fntable[] = {
  &read_chan_status,				/* Fn  0. Ptr to readstatus function 				*/
  &chan_set_pointer,				/* Fn  1. Ptr to setpointer function				*/
  &chan_get_pointer,				/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  NULL,								/* Fn  6. Ptr to savecalibration function 			*/
  NULL,								/* Fn  7. Ptr to restorecalibration function 		*/
  NULL,								/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  NULL,								/* Fn 13. Ptr to setgain function 					*/
  NULL,								/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  NULL,								/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  &chan_read_adc,					/* Fn 23. Ptr to readadc function 					*/
  &set_filter,						/* Fn 24. Ptr to setfilter function 				*/
  &read_filter,						/* Fn 25. Ptr to readfilter function 				*/
  NULL,								/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function	 			*/
  NULL,								/* Fn 28. Ptr to set_caltable function 				*/
  NULL,								/* Fn 29. Ptr to read_caltable function 			*/
  NULL,								/* Fn 30. Ptr to set_calgain function 				*/
  NULL,								/* Fn 31. Ptr to read_calgain function 				*/
  NULL,								/* Fn 32. Ptr to set_caloffset function 			*/
  NULL,								/* Fn 33. Ptr to read_caloffset function	 		*/
  &chan_set_cnet_output,			/* Fn 34. Ptr to set_cnet_slot function 			*/
  &chan_vt_set_gaintrim,			/* Fn 35. Ptr to set_gaintrim function 				*/
  &chan_vt_read_gaintrim,			/* Fn 36. Ptr to read_gaintrim function 			*/
  NULL,								/* Fn 37. Ptr to set_offsettrim function 			*/
  NULL,								/* Fn 38. Ptr to read_offsettrim function 			*/
  NULL,								/* Fn 39. Ptr to set_txdrzero function				*/
  NULL,								/* Fn 40. Ptr to read_txdrzero function				*/
  &read_peakinfo,					/* Fn 41. Ptr to readpeakinfo function 				*/
  &reset_peaks,						/* Fn 42. Ptr to resetpeak function 				*/
  &chan_vt_set_flags,			 	/* Fn 43. Ptr to set_flags function 				*/
  &chan_vt_read_flags,				/* Fn 44. Ptr to read_flags function 				*/
  &chan_vt_write_map,				/* Fn 45. Ptr to write_map function 				*/
  &chan_vt_read_map,				/* Fn 46. Ptr to read_map function 					*/
  &chan_vt_set_refzero,				/* Fn 47. Ptr to set_refzero function 				*/
  &read_refzero,					/* Fn 48. Ptr to read_refzero function 				*/
  &undo_refzero,					/* Fn 49. Ptr to undo_refzero function 				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  NULL,								/* Fn 52. Ptr to tedsid function 					*/
  NULL,								/* Fn 53. Ptr to set source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  NULL,								/* Fn 58. Ptr to set info function					*/
  NULL,								/* Fn 59. Ptr to read info function					*/
  NULL,								/* Fn 60. Ptr to write output function				*/
  NULL,								/* Fn 61. Ptr to read input function				*/
  NULL,								/* Fn 62. Ptr to read output function				*/
  &chan_vt_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_vt_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  NULL,								/* Fn 72. Ptr to txdrzero_zero function				*/
  &chan_read_cyclecount,			/* Fn 73. Ptr to readcycle function					*/
  &reset_cycles,					/* Fn 74. Ptr to resetcycles function 				*/
  &chan_vt_set_peak_threshold,		/* Fn 75. Ptr to setpkthreshold function 			*/
  &chan_vt_read_peak_threshold,		/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_vt_write_faultmask,			/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_vt_read_faultstate,			/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  &chan_vt_refzero_zero,			/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};


/********************************************************************************
  FUNCTION NAME     : hw_mboard_install
  FUNCTION DETAILS  : Install mainboard hardware on the selected channel(s).

					  On entry:

					  slot			Slot number being installed.

					  numinchan		Points to the variable holding the number of
									input channels installed for this slot.

					  numoutchan	Points to the variable holding the number of
									output channels installed for this slot.

					  nummiscchan	Points to the variable holding the number of
									miscellaneous channels installed for this slot.

					  flags			Controls if channel allocation is performed.
									If not, then only initialisation operations
									are performed.
********************************************************************************/

uint32_t hw_mboard_install(int slot, int* numinchan, int* numoutchan, int* nummiscchan,
	int* numdigiochan, hw_mboard* hw, int flags, uint32_t* used_eecal_addr,
	uint32_t* used_eecfg_addr, uint32_t* read_eecfg_addr)
{
	info_mboard* i;
	uint32_t eecal_addr = CHAN_EE_CALSTART;		/* Start of channel EEPROM calibration area	  */
	uint32_t eecfg_addr = CHAN_EEBANK_DATASTART;	/* Start of channel EEPROM configuration area */
	uint32_t eerdcfg_addr = CHAN_EEBANK_DATASTART;	/* Address for reading EEPROM configuration	  */
	int input;
	int output;
	int sv;
	uint8_t* workspace;
	uint32_t status = 0;
	uint32_t chan;
	chandef* def;

	/***************************************************************************
	* Perform initial allocation of channels and setting of default parameters *
	***************************************************************************/

	if (flags & DO_ALLOCATE) {

		/* The workspace is used by all inputs on the mainboard, so determine the
		   address of the workspace area of the first input to be allocated and use
		   that for all input workspaces.
		*/

		workspace = inchaninfo[totalinchan].hw.i_mboard.work;
		memset(workspace, 0, WSIZE_MBOARD);

	}

	/* Allocate the two general-purpose transducers */

	for (input = 0; input < 2; input++) {

		def = &inchaninfo[slot_status[slot].baseinchan + input];

		/* Set defaults for general-purpose transducer channel */

		chan_gp_default(def, (totalinchan + input));

		if (flags & DO_ALLOCATE) {

			i = &def->hw.i_mboard;

			/* Define local hardware/workspace settings */

			i->hw = hw;									/* Pointer to mainboard hardware base	*/
			i->input = input;	 							/* Input channel number on mainboard	*/
			i->relaystate = &workspace[0];						/* Pointer to local workspace		    */
			i->ledmask = 1 << mboard_led_conversion[input];	/* Mask for channel ident LED			*/
			i->ledstate = &workspace[1];						/* Pointer to local workspace			*/

			init_fntable(def, mboard_gp_fntable);	/* Set pointers to control functions */

			SETDEF(slot, TYPE_GP, input, (CHANTYPE_INPUT | (totalinchan + input)));

			/* Allocate space in EEPROM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
			def->eecal_start = eecal_addr; eecal_addr += SAVED_CHANCAL_SIZE; def->eecal_size = SAVED_CHANCAL_SIZE;

			/* Allocate space in DSP B memory for linearisation map */

			if (!(def->mapptr = (float*)alloc_dspb_general(sizeof(def->map)))) diag_error(DIAG_MALLOC);

			/* Define the source address for the channel */

			def->sourceptr = (input) ? &(hw->u.rd.adc2) : &(hw->u.rd.adc1);

			/* Initialise the TEDS interface */

			hw_2gp1sv_1w_init((info_2gp1sv*)i, input);

			/* Initialise the I2C interface */

			//hw_2gp1sv_i2c_stop((info_2gp1sv*)i);
		}

		/* Restore current calibration and configuration data */

		def->status = restore_chan_calibration(def);
		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

	}

	/* Allocate the two high-level inputs */

	for (input = 2; input < 4; input++) {

		def = &inchaninfo[slot_status[slot].baseinchan + input];

		/* Set defaults for high-level ADC input channel */

		chan_ad_default(def, (totalinchan + input));

		if (flags & DO_ALLOCATE) {

			i = &def->hw.i_mboard;

			/* Define local hardware/workspace settings */

			i->hw = hw;									/* Pointer to mainboard hardware base	*/
			i->input = input;								/* Input channel number on mainboard    */
			i->ledmask = 1 << mboard_led_conversion[input];	/* Mask for channel ident LED 		    */
			i->ledstate = &workspace[1];						/* Pointer to local workspace		    */

			init_fntable(def, mboard_ad_fntable);	/* Set pointers to control functions */

			SETDEF(slot, TYPE_ADC, input, (CHANTYPE_INPUT | (totalinchan + input)));

			/* Allocate space in EEPROM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
			def->eecal_start = eecal_addr; eecal_addr += SAVED_CHANCAL_SIZE; def->eecal_size = SAVED_CHANCAL_SIZE;

			/* Allocate space in DSP B memory for linearisation map */

			if (!(def->mapptr = (float*)alloc_dspb_general(sizeof(def->map)))) diag_error(DIAG_MALLOC);

			/* Define the source address for the channel */

			def->sourceptr = (input == 2) ? &(hw->u.rd.adc3) : &(hw->u.rd.adc4);
		}

		/* Restore current calibration and configuration data */

		def->status = restore_chan_calibration(def);
		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

	}

	/* Allocate the two monitor outputs */

	for (output = 0; output < 2; output++) {

		def = &outchaninfo[slot_status[slot].baseoutchan + output];

		/* Set defaults for high-level DAC output channel */

		chan_da_default(def, (totaloutchan + output));

		if (flags & DO_ALLOCATE) {

			//  cfg_da *cfg;

			i = &def->hw.i_mboard;
			//  cfg = &def->cfg.i_da;

				/* Define local hardware/workspace settings */

			i->hw = hw;									/* Pointer to mainboard hardware base	*/
			i->output = output;	 							/* Output channel number on mainboard   */
			i->ledmask = 1 << mboard_led_conversion[output + 4];	/* Mask for channel ident LED 		    */
			i->ledstate = &workspace[1];						/* Pointer to local workspace		    */

			init_fntable(def, mboard_da_fntable);	/* Set pointers to control functions */

			SETDEF(slot, TYPE_DAC, output, (CHANTYPE_OUTPUT | (totaloutchan + output)));

			/* Allocate space in EEPROM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
			def->eecal_start = eecal_addr; eecal_addr += SAVED_CHANCAL_SIZE; def->eecal_size = SAVED_CHANCAL_SIZE;

			/* Define the source and destination addresses for the channel */

		/* def->sourceptr = cnet_get_slotaddr(192 + *totaloutchan, PROC_DSPB); */
			def->sourceptr = NULL;
			def->outputptr = (int*)((output == 0) ? &(hw->u.wr.dac0) : &(hw->u.wr.dac1));
			*def->outputptr = 0;
		}

		/* Restore current calibration and configuration data */

		def->status = restore_chan_calibration(def);
		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

	}

	/* Allocate the SV output */

	if (!HWFN_NOSV(slot)) {

		for (sv = 0; sv < 1; sv++) {

			def = &miscchaninfo[slot_status[slot].basemiscchan + sv];

			/* Set defaults for servo-valve output channel */

			chan_sv_default(def, (totalmiscchan + sv));

			if (flags & DO_ALLOCATE) {

				//      cfg_sv *cfg;

				i = &def->hw.i_mboard;
				//      cfg = &def->cfg.i_sv;

					  /* Define local hardware/workspace settings */

				i->hw = hw;			/* Pointer to mainboard hardware base	*/
				i->output = 2 + sv;	/* Output channel number on mainboard   */

				init_fntable(def, mboard_sv_fntable);	/* Set pointers to control functions */

				SETDEF(slot, TYPE_SV, sv, (CHANTYPE_MISC | (totalmiscchan + sv)));

				/* Allocate space in EEPROM for configuration and calibration data */

				ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
				def->eecal_start = eecal_addr; eecal_addr += SAVED_CHANCAL_SIZE; def->eecal_size = SAVED_CHANCAL_SIZE;

				/* Define the source and destination addresses for the channel */

				def->sourceptr = NULL;
				def->outputptr = (int*)&(hw->u.wr.sv0);
				*def->outputptr = 0;
			}

			/* Restore current calibration and configuration data */

			def->status = restore_chan_calibration(def);
			if (!(flags & DO_FLUSH)) {
				def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
			}
			status |= def->status;
		}
	}

	chan = slot_status[slot].basedigiochan;

	/* Allocate the digital input channels */

	for (input = 0; input < 2; input++) {
		cfg_io* cfg;

		def = &digiochaninfo[chan];
		cfg = &def->cfg.i_io;

		/* Set defaults for digital I/O input channel */

		chan_io_default(def, chan, (DIGIO_INPUT | DIGIO_NORMAL));

		if (flags & DO_ALLOCATE) {

			i = &def->hw.i_mboard;

			/* Define local hardware/workspace settings */

			i->hw = hw;									/* Pointer to mainboard hardware base	*/
			init_fntable(def, mboard_digip_fntable);	/* Set pointers to control functions 	*/
			SETDEF(slot, TYPE_DIGIO, input, (CHANTYPE_DIGIO | chan));

			/* Allocate space in EEPROM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
		}

		/* Restore current calibration and configuration data */

		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

		/* Set the mask for the digital input */

		cfg->mask = GPIN1_SENSE << input;

		/* Force the direction flag to match reality */

		cfg->flags = cfg->flags & ~DIGIO_OUTPUT;

		chan++;
	}

	/* Allocate the digital output channels */

	for (output = 0; output < 2; output++) {
		cfg_io* cfg;

		def = &digiochaninfo[chan];
		cfg = &def->cfg.i_io;

		/* Set defaults for digital I/O output channel */

		chan_io_default(def, chan, (DIGIO_OUTPUT | DIGIO_NORMAL));

		if (flags & DO_ALLOCATE) {

			i = &def->hw.i_mboard;

			/* Define local hardware/workspace settings */

			i->hw = hw;									/* Pointer to mainboard hardware base	*/
			init_fntable(def, mboard_digop_fntable);	/* Set pointers to control functions 	*/
			SETDEF(slot, TYPE_DIGIO, output, (CHANTYPE_DIGIO | chan));

			/* Allocate space in EEPROM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
		}

		/* Restore current calibration and configuration data */

		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

		/* Set the mask for the digital output */

		cfg->mask = GPOUT1_DRIVE << output;

		/* Force the direction flag to match reality */

		cfg->flags = cfg->flags | DIGIO_OUTPUT;

		chan++;
	}

	/***************************************************************************
	* Perform hardware configuration                                           *
	***************************************************************************/

	/* Configure the two general-purpose transducers */

	for (input = 0; input < 2; input++) {

		chandef* def;
		cal_gp* cal;
		cfg_gp* cfg;

		def = &inchaninfo[slot_status[slot].baseinchan + input];
		cal = &def->cal.u.c_gp;
		cfg = &def->cfg.i_gp;

		/* Trap for illegal negative gaintrim value caused by upgrade from version
		   prior to v1.0.0099. Replace by value of 1.0.
		*/

		if (cfg->neg_gaintrim < 0.5) cfg->neg_gaintrim = 1.0;

		/* Restore the user configuration settings */

		/* Update channel flags. Force simulation mode off and disable cyclic event generation. */

		def->set_flags(def, (cfg->flags & ~(FLAG_SIMULATION | FLAG_CYCEVENT)), 0xFFFFFFFF, NO_UPDATECLAMP, NO_MIRROR);

		def->setgain(def, cfg->gain,
			SETGAIN_RETAIN_GAINTRIM | chan_extract_gainflags(cfg->flags),
			NO_MIRROR);

		def->offset = cal->cal_offset;
		def->setexc(def, cfg->flags & FLAG_ACTRANSDUCER, cfg->excitation, cfg->phase, NO_MIRROR);
		def->set_txdrzero(def, cfg->txdrzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);
		def->set_refzero(def, def->refzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);

		/* Set the linearisation map */

		def->write_map(def, (cfg->flags & FLAG_LINEARISE) >> BIT_LINEARISE, def->map, def->filename, FALSE, NO_MIRROR);

		/* Set filter 1 characteristic */

		set_filter(def, FILTER_ALL, 0, def->filter1.type & FILTER_ENABLE, def->filter1.type & FILTER_TYPEMASK,
			def->filter1.order, def->filter1.freq, def->filter1.bandwidth, NO_MIRROR);

		/* Set the fault mask */

		def->writefaultmask(def, cfg->faultmask, NO_MIRROR);

		/* Update the shared channel definition */

		update_shared_channel(def);

	}

	/* Configure the two high-level inputs */

	for (input = 2; input < 4; input++) {

		chandef* def;
		cal_ad* cal;
		cfg_ad* cfg;

		def = &inchaninfo[slot_status[slot].baseinchan + input];
		cal = &def->cal.u.c_ad;
		cfg = &def->cfg.i_ad;

		/* Trap for illegal negative gaintrim value caused by upgrade from version
		   prior to v1.0.0099. Replace by value of 1.0.
		*/

		if (cfg->neg_gaintrim < 0.5) cfg->neg_gaintrim = 1.0;

		/* Restore the user configuration settings */

		def->setgain(def, cfg->gain,
			SETGAIN_RETAIN_GAINTRIM | chan_extract_gainflags(cfg->flags),
			NO_MIRROR);

		def->offset = cal->cal_offset;
		def->set_txdrzero(def, cfg->txdrzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);
		def->set_refzero(def, def->refzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);

		/* Set the linearisation map */

		def->write_map(def, (cfg->flags & FLAG_LINEARISE) >> BIT_LINEARISE, def->map, def->filename, FALSE, NO_MIRROR);

		/* Update channel flags. Force simulation mode off and disable cyclic event generation. */

		def->set_flags(def, (cfg->flags & ~(FLAG_SIMULATION | FLAG_CYCEVENT)), 0xFFFFFFFF, NO_UPDATECLAMP, NO_MIRROR);

		/* Set filter 1 characteristic */

		set_filter(def, FILTER_ALL, 0, def->filter1.type & FILTER_ENABLE, def->filter1.type & FILTER_TYPEMASK,
			def->filter1.order, def->filter1.freq, def->filter1.bandwidth, NO_MIRROR);

		/* Set the fault mask */

		def->writefaultmask(def, cfg->faultmask, NO_MIRROR);

		/* Update the shared channel definition */

		update_shared_channel(def);

	}

	/* Configure the two monitor outputs */

	for (output = 0; output < 2; output++) {

		chandef* def;
		cal_da* cal;
		cfg_da* cfg;

		def = &outchaninfo[slot_status[slot].baseoutchan + output];
		cal = &def->cal.u.c_da;
		cfg = &def->cfg.i_da;

		/* Set gain and offset */

	  //  def->setgain(def, cfg->gain, SETGAIN_RETAIN_GAINTRIM, NO_MIRROR);
		def->set_gaintrim(def, &cfg->gaintrim, NULL, FALSE, NO_MIRROR);
		def->offset = cal->cal_offset;

		/* Set the range */

	  //  def->range = 1.0 / cfg->range;

		/* Update channel flags */

		def->set_flags(def, cfg->flags, 0xFFFFFFFF, NO_UPDATECLAMP, NO_MIRROR);

		/* Initialise DAC output */

		def->writedac(def, 0.0);
		*((int16_t*)(def->outputptr)) = 0;

		/* Update the shared channel definition */

		update_shared_channel(def);

	}

	/* Configure the SV output */

	if (!HWFN_NOSV(slot)) {

		/* SV drive capability not disabled */

		for (sv = 0; sv < 1; sv++) {

			chandef* def;
			cal_sv* cal;
			cfg_sv* cfg;

			def = &miscchaninfo[slot_status[slot].basemiscchan + sv];
			i = &def->hw.i_mboard;
			cal = &def->cal.u.c_sv;
			cfg = &def->cfg.i_sv;

			/* Set offset */

			def->offset = cal->cal_offset;

			/* Update channel flags */

			def->set_flags(def, cfg->flags, 0xFFFFFFFF, NO_UPDATECLAMP, NO_MIRROR);

			/* Set the valve type and current (for current driven valves), configure the dither output and
			   initialise the DAC output. The call to set_current has the SETGAIN_RETAIN_GAINTRIM flag set
			   since this operates in the same way as the set_gain() function for other types.
			*/

			def->set_current(def, cfg->svtype, cfg->current, SETGAIN_RETAIN_GAINTRIM, NO_MIRROR);
			def->set_dither(def, cfg->dither_enable, cfg->dither_ampl, cfg->dither_freq, NO_MIRROR);
			def->set_balance(def, cfg->balance, NO_MIRROR);
			def->writedac(def, 0.0);
			*((int16_t*)(def->outputptr)) = 0;

			/* Set the fault mask */

			def->writefaultmask(def, cfg->faultmask, NO_MIRROR);

			/* Update the shared channel definition */

			update_shared_channel(def);

		}

	}

	/* Configure the slot_status table information for the digital input channels. This is
	   used by the CNet digital input acquisition code when copying the state into the CNet
	   slot memory.
	*/

	slot_status[0].digip_mask = 0x03;
	slot_status[0].digip_invert = 0;
	for (input = 0; input < 2; input++) {
		cfg_io* cfg;

		def = &digiochaninfo[slot_status[0].basedigiochan + input];
		cfg = &def->cfg.i_io;

		if (cfg->flags & DIGIO_INVERT) slot_status[0].digip_invert |= (1 << input);
	}

	/* Update channel counts */

	if (flags & DO_ALLOCATE) {
		totalinchan += 4;
		physinchan += 4;
		totaloutchan += 2;
		if (!HWFN_NOSV(slot)) totalmiscchan += 1;	/* If SV not disabled allocate misc channel */
		totaldigiochan += 4;
		*numinchan = 4;
		*numoutchan = 2;
		if (!HWFN_NOSV(slot)) *nummiscchan = 1;	/* If SV not disabled allocate misc channel */
		*numdigiochan = 4;
	}

	/* Enable the monitor and SV DACs */

	hw->u.wr.dacen |= DAC_EN;
	//LOG_printf(&trace, "Install 27 %d", TIMER_getCount(hTimer1));

	/* Return the amount of calibration and configuration EEPROM space used. This allows later
	   virtual channel allocations to continue their allocations after the physical channels.
	*/

	*used_eecal_addr = eecal_addr;
	*used_eecfg_addr = eecfg_addr;
	*read_eecfg_addr = eerdcfg_addr;

	return(status & (FLAG_BADLIST | FLAG_OLDLIST));
}



#define EXTRA_CHANNELS (ExpNExp + C3AppNChans)	/* Number of virtual + command channels */

/********************************************************************************
  FUNCTION NAME     : hw_mboard_install_virtual
  FUNCTION DETAILS  : Install virtual transducer channels.

					  On entry:

					  slot			Slot number being installed.

					  numinchan		Points to the variable holding the number of
									input channels installed for this slot.

					  flags			Controls if channel allocation is performed.
									If not, then only initialisation operations
									are performed.

					  eecal_addr 	Start of EEPROM calibration area

					  eecfg_addr	Start of EEPROM configuration area

********************************************************************************/

uint32_t hw_mboard_install_virtual(int slot, int* numinchan, uint32_t eecal_addr, uint32_t eecfg_addr, uint32_t eecfg_rdaddr, int flags)
{
	uint32_t eerdcfg_addr = eecfg_rdaddr;	/* Address for reading EEPROM configuration	  */
	int input;
	uint32_t status = 0;
	chandef* def;

	/***************************************************************************
	* Perform initial allocation of channels and setting of default parameters *
	***************************************************************************/

	/* Allocate the virtual transducers */

	for (input = 0; input < EXTRA_CHANNELS; input++) {

		int chan;

		/* Virtual/command channels are now allocated at the end of the
		   channel array, so determine the channel number to be used.
		*/

		chan = MAXINCHAN - EXTRA_CHANNELS + input;

		def = &inchaninfo[chan];

		/* Set defaults for virtual transducer channel */

		chan_vt_default(def, chan);

		if (flags & DO_ALLOCATE) {

			init_fntable(def, mboard_virtual_fntable);	/* Set pointers to control functions */
			SETDEF(slot, TYPE_VIRTUAL, input, (CHANTYPE_INPUT | chan));

			/* Allocate space in EEPROM for configuration data. No calibration data
			   space is allocated because there is no calibration for the virtual transducer. */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
			def->eecal_start = eecal_addr; def->eecal_size = 0;

			/* Allocate space in DSP B memory for linearisation map */

			if (!(def->mapptr = (float*)alloc_dspb_general(sizeof(def->map)))) diag_error(DIAG_MALLOC);

			/* Define the source address for the channel */

			def->sourceptr = NULL;
		}

		/* Restore current configuration data */

		def->status = 0;
		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

	}

	/***************************************************************************
	* Perform hardware configuration                                           *
	***************************************************************************/

	/* Configure the virtual transducers */

	for (input = 0; input < EXTRA_CHANNELS; input++) {

		chandef* def;
		cfg_vt* cfg;
		int chan;

		/* Virtual/command channels are now allocated at the end of the
		   channel array, so determine the channel number to be used.
		*/

		chan = MAXINCHAN - EXTRA_CHANNELS + input;

		def = &inchaninfo[chan];
		cfg = &def->cfg.i_vt;

		/* Trap for illegal negative gaintrim value caused by upgrade from version
		   prior to v1.0.0099. Replace by value of 1.0.
		*/

		if (cfg->neg_gaintrim < 0.5) cfg->neg_gaintrim = 1.0;

		/* Set the linearisation map */

		def->write_map(def, (cfg->flags & FLAG_LINEARISE) >> BIT_LINEARISE, def->map, def->filename, FALSE, NO_MIRROR);

		/* Update channel flags. Force simulation mode off and disable cyclic event generation. */

		def->set_flags(def, (cfg->flags & ~(FLAG_SIMULATION | FLAG_CYCEVENT)), 0xFFFFFFFF, NO_UPDATECLAMP, NO_MIRROR);

		/* Set filter 1 characteristic */

		set_filter(def, FILTER_ALL, 0, def->filter1.type & FILTER_ENABLE, def->filter1.type & FILTER_TYPEMASK,
			def->filter1.order, def->filter1.freq, def->filter1.bandwidth, NO_MIRROR);

		/* Update the shared channel definition */

		update_shared_channel(def);

	}

	/* Update channel counts */

	if (flags & DO_ALLOCATE) {
		totalinchan += EXTRA_CHANNELS;
		*numinchan = EXTRA_CHANNELS;
	}

	return(status & (FLAG_BADLIST | FLAG_OLDLIST | FLAG_BADCFG | FLAG_BADCAL));
}



/********************************************************************************
  FUNCTION NAME     : hw_mboard_save
  FUNCTION DETAILS  : Save the mainboard channel settings to EEPROM.

					  If a bad list or an old list format was detected on any
					  channels, then all of the channels must be updated.

					  On entry:

					  slot		Slot number being saved.

					  status	System configuration status. This indicates if
								a full save operation is required because an
								old or bad format list was detected during
								initialisation.

********************************************************************************/

void hw_mboard_save(int slot, uint32_t status)
{
	int n;
	chandef* def;

	/* Update the input channels */

	for (n = 0; n < 4; n++) {
		def = &inchaninfo[slot_status[slot].baseinchan + n];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

	/* Update the output channels */

	for (n = 0; n < 2; n++) {
		def = &outchaninfo[slot_status[slot].baseoutchan + n];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

	/* Update the SV channel */

	for (n = 0; n < 1; n++) {
		def = &miscchaninfo[slot_status[slot].basemiscchan + n];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

	/* Update the digital I/O channels */

	for (n = 0; n < 4; n++) {
		def = &digiochaninfo[slot_status[slot].basedigiochan + n];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

	/* Update the virtual channels */

	for (n = 0; n < EXTRA_CHANNELS; n++) {
		def = &inchaninfo[MAXINCHAN - EXTRA_CHANNELS + n];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

}
