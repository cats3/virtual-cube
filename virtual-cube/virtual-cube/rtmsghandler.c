/********************************************************************************
  MODULE NAME   	: rtmsghandler
  PROJECT			: Control Cube / Signal Cube
  MODULE DETAILS  	: Handler functions for realtime data streaming messages.
********************************************************************************/

#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <errno.h>

#include <sys/socket.h>

#include "server.h"
#include "porting.h"

#include "defines.h"

#include "hardware.h"
#include "msgserver.h"
#include "msgstruct.h"

#include "cnet.h"
#include "rtbuffer.h"

#include "debug.h"



/********************************************************************************
  FUNCTION NAME   	: fn_define_output_list
  FUNCTION DETAILS  : Handler for define_output_list message.
********************************************************************************/

int fn_define_output_list(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	int n;
	int chan;
	int enable;
	struct msg_define_output_list* m;

	m = (struct msg_define_output_list*)msg;

	//	LOG_printf(&trace, "define_output_list entry");

	/* Calculate message length */

	*msglength = offsetof(struct msg_define_output_list, list) + m->listsize;

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	/* Disable realtime transfers whilst updates take place */

	enable = conn->stream->enable;
	conn->stream->enable = FALSE;

	conn->stream->outlist->size = m->listsize;
	conn->stream->outbuf.pktsize = conn->stream->outlist->size * sizeof(float);

	for (n = 0; n < m->listsize; n++) {
		// LOG_printf(&trace, "entry %d", n);
		chan = m->list[n];
		conn->stream->outlist->list[n].slot = chan;									/* Insert slot number	 */
		conn->stream->outlist->list[n].data = (volatile float*)&CNET_MEM_WRITE[chan];	/* Point to CNet slot 	 */
		conn->stream->outlist->list[n].copy = (float*)&cnet_copy_data[chan];			/* Point to CNet copy 	 */
		conn->stream->outlist->list[n].rate = (float*)&cnet_stop_rate[chan];			/* Point to stop rate 	 */
		conn->stream->outlist->list[n].target = (float*)&cnet_stop_target[chan];		/* Point to stop target	 */
		conn->stream->outlist->list[n].time = &cnet_stop_time[chan];					/* Point to stop time 	 */
		conn->stream->outlist->list[n].tcounter = &cnet_stop_tcounter[chan];				/* Point to stop counter */
	}

	/* Restore realtime transfer state */

	conn->stream->enable = enable;

	// LOG_printf(&trace, "define_output_list exit");

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_output_list
  FUNCTION DETAILS  : Handler for read_output_list message.
********************************************************************************/

int fn_read_output_list(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	//struct msg_read_output_list *m;
	struct msg_return_output_list* r;
	int n;

	//m = (struct msg_read_output_list *)msg;
	r = (struct msg_return_output_list*)reply;

	/* Calculate message length */

	*msglength = offsetof(struct msg_read_output_list, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	*replylength = offsetof(struct msg_return_output_list, list) + conn->stream->outlist->size;

	r->command = MSG_RETURN_OUTPUT_LIST;
	r->listsize = conn->stream->outlist->size;

	for (n = 0; n < r->listsize; n++) {
		r->list[n] = conn->stream->outlist->list[n].slot;	/* Copy slot number	*/
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_define_input_list
  FUNCTION DETAILS  : Handler for define_input_list message.
********************************************************************************/

int fn_define_input_list(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	int n;
	int chan;
	int enable;
	struct msg_define_input_list* m;

	// LOG_printf(&trace, "define_input_list entry");

	m = (struct msg_define_input_list*)msg;

	*msglength = offsetof(struct msg_define_input_list, list) + m->listsize;

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	/* Disable realtime transfers whilst updates take place */

	enable = conn->stream->enable;
	conn->stream->enable = FALSE;

	conn->stream->inlist->size = m->listsize;
	conn->stream->inbuf.pktsize = conn->stream->inlist->size * sizeof(float);

	for (n = 0; n < m->listsize; n++) {
		chan = m->list[n];
		conn->stream->inlist->list[n].slot = chan;									/* Insert slot number	*/
		conn->stream->inlist->list[n].data = (volatile float*)&CNET_MEM_READ[chan];	/* Point to CNet slot 	*/
	}

	/* Restore realtime transfer state */

	conn->stream->enable = enable;

	// LOG_printf(&trace, "define_input_list exit");

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_input_list
  FUNCTION DETAILS  : Handler for read_input_list message.
********************************************************************************/

int fn_read_input_list(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	//struct msg_read_input_list *m;
	struct msg_return_input_list* r;
	int n;

	//m = (struct msg_read_input_list *)msg;
	r = (struct msg_return_input_list*)reply;

	/* Calculate message length */

	*msglength = offsetof(struct msg_read_input_list, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	*replylength = offsetof(struct msg_return_input_list, list) + conn->stream->inlist->size;

	r->command = MSG_RETURN_INPUT_LIST;
	r->listsize = conn->stream->inlist->size;

	for (n = 0; n < r->listsize; n++) {
		r->list[n] = conn->stream->inlist->list[n].slot;	/* Copy slot number	*/
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_total_buffer
  FUNCTION DETAILS  : Handler for read_total_buffer message.
********************************************************************************/

int fn_read_total_buffer(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	//struct msg_read_total_buffer *m;
	struct msg_return_total_buffer* r;

	//m = (struct msg_read_total_buffer *)msg;
	r = (struct msg_return_total_buffer*)reply;

	*msglength = offsetof(struct msg_read_total_buffer, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	*replylength = offsetof(struct msg_return_total_buffer, end);

	r->command = MSG_RETURN_TOTAL_BUFFER;
	r->buf_size = (conn->stream->bufsize / 4);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_output_buffer
  FUNCTION DETAILS  : Handler for read_output_buffer message.
********************************************************************************/

int fn_read_output_buffer(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	//struct msg_read_output_buffer *m;
	struct msg_return_output_buffer* r;
	uint32_t out_size;

	//m = (struct msg_read_output_buffer *)msg;
	r = (struct msg_return_output_buffer*)reply;

	*msglength = offsetof(struct msg_read_output_buffer, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	*replylength = offsetof(struct msg_return_output_buffer, end);

	r->command = MSG_RETURN_OUTPUT_BUFFER;

	if (conn->stream->outbuf.pktsize) {
		out_size = conn->stream->outbuf.size / conn->stream->outbuf.pktsize;
	}
	else {
		out_size = 0;
	}

	r->buf_size = out_size;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_input_buffer
  FUNCTION DETAILS  : Handler for read_input_buffer message.
********************************************************************************/

int fn_read_input_buffer(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	//struct msg_read_input_buffer *m;
	struct msg_return_input_buffer* r;
	uint32_t in_size;

	//m = (struct msg_read_input_buffer *)msg;
	r = (struct msg_return_input_buffer*)reply;

	*msglength = offsetof(struct msg_read_input_buffer, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	*replylength = offsetof(struct msg_return_input_buffer, end);

	r->command = MSG_RETURN_INPUT_BUFFER;

	if (conn->stream->inbuf.pktsize) {
		in_size = conn->stream->inbuf.size / conn->stream->inbuf.pktsize;
	}
	else {
		in_size = 0;
	}

	r->buf_size = in_size;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_define_output_buffer
  FUNCTION DETAILS  : Handler for set_output_buffer message.
********************************************************************************/

int fn_define_output_buffer(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_define_output_buffer* m;
	int err;

	// LOG_printf(&trace, "define_output_buffer entry");

	m = (struct msg_define_output_buffer*)msg;

	*msglength = offsetof(struct msg_define_output_buffer, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	err = set_bufsize(conn->stream, BUFFER_AUTOSIZE, m->buf_size);
	if (err == BUFFER_NOSPACE) {
		return(error_illegal_parameter(reply, replylength));
	}

	// LOG_printf(&trace, "define_output_buffer exit");

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_input_buffer
  FUNCTION DETAILS  : Handler for set_input_buffer message.
********************************************************************************/

int fn_define_input_buffer(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_define_input_buffer* m;
	int err;

	m = (struct msg_define_input_buffer*)msg;

	*msglength = offsetof(struct msg_define_input_buffer, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	err = set_bufsize(conn->stream, m->buf_size, BUFFER_AUTOSIZE);
	if (err == BUFFER_NOSPACE) {
		return(error_illegal_parameter(reply, replylength));
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_buffer_status
  FUNCTION DETAILS  : Handler for read_buffer_status message.
********************************************************************************/

int fn_read_buffer_status(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_return_buffer_status* r;
	uint32_t status;
	uint32_t out_free;
	uint32_t in_used;

	r = (struct msg_return_buffer_status*)reply;

	*msglength = offsetof(struct msg_read_buffer_status, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	*replylength = offsetof(struct msg_return_buffer_status, end);

	r->command = MSG_RETURN_BUFFER_STATUS;

	if (conn->stream->outbuf.pktsize) {
		out_free = conn->stream->outbuf.free / conn->stream->outbuf.pktsize;
	}
	else {
		out_free = 0;
	}

	if (conn->stream->inbuf.pktsize) {
		in_used = conn->stream->inbuf.used / conn->stream->inbuf.pktsize;
	}
	else {
		in_used = 0;
	}

	status = (conn->stream->enable & (RT_INPUT | RT_OUTPUT | RT_RAMP)) << 4;
	if (conn->stream->inbuf.overrun) status |= RTSTATUS_OVERRUN;
	if (conn->stream->outbuf.underrun) status |= RTSTATUS_UNDERRUN;
	if (conn->stream->outbuf.empty) status |= RTSTATUS_EMPTY;

	r->output_free = out_free;
	r->input_used = in_used;
	r->status = status;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_clear_error_flags
  FUNCTION DETAILS  : Handler for clear_error_flags message.
********************************************************************************/

int fn_clear_error_flags(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{

	// LOG_printf(&trace, "clear_error_flags entry");

	*msglength = offsetof(struct msg_clear_error_flags, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	conn->stream->inbuf.overrun = FALSE;
	conn->stream->outbuf.underrun = FALSE;

	// LOG_printf(&trace, "clear_error_flags exit");

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_control_realtime_transfer
  FUNCTION DETAILS  : Handler for control_realtime_transfer message.
********************************************************************************/

int fn_control_realtime_transfer(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_control_realtime_transfer* m;

	m = (struct msg_control_realtime_transfer*)msg;

	*msglength = offsetof(struct msg_control_realtime_transfer, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	rtctrl_control(conn->stream, m->flags);

	/* If we're enabling turning point mode, then set rt_tpstate to STATE_TP_OUTPUT so that
	   the first turning point is output immediately.
	*/

	//if ((conn->stream->enable ^ m->flags) & RT_OUTTSR) {
	//  rt_tpstate = STATE_TP_OUTPUT;
	//}

	//conn->stream->enable = m->flags;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_pause_realtime
  FUNCTION DETAILS  : Handler for pause_realtime message.
********************************************************************************/

int fn_pause_realtime_transfer(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_pause_realtime_transfer* m;

	m = (struct msg_pause_realtime_transfer*)msg;

	*msglength = offsetof(struct msg_pause_realtime_transfer, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	rtctrl_pause(conn->stream, m->flags); /* Copy flags to realtime control variable */

	//rt_control |= (m->flags | GENERATE_PAUSE);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_resume_realtime_transfer
  FUNCTION DETAILS  : Handler for resume_realtime message.
********************************************************************************/

int fn_resume_realtime_transfer(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	*msglength = offsetof(struct msg_resume_realtime_transfer, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	rtctrl_resume(conn->stream); /* Enable realtime transfers */

	//conn->stream->enable = conn->stream->trigger = RT_INPUT | RT_OUTPUT;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_rt_stop_type
  FUNCTION DETAILS  : Handler for set_rt_stop_type message.
********************************************************************************/

int fn_set_rt_stop_type(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_set_rt_stop_type* m;

	// LOG_printf(&trace, "set_rt_stop_type entry");

	m = (struct msg_set_rt_stop_type*)msg;

	*msglength = offsetof(struct msg_set_rt_stop_type, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	cnet_stop_type = m->type;

	// LOG_printf(&trace, "set_rt_stop_type exit");

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_rt_stop_type
  FUNCTION DETAILS  : Handler for read_rt_stop_type message.
********************************************************************************/

int fn_read_rt_stop_type(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_return_rt_stop_type* r;

	r = (struct msg_return_rt_stop_type*)reply;

	*msglength = offsetof(struct msg_read_rt_stop_type, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	*replylength = offsetof(struct msg_return_rt_stop_type, end);

	r->command = MSG_RETURN_RT_STOP_TYPE;

	r->type = cnet_stop_type;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_rt_stop_parameters
  FUNCTION DETAILS  : Handler for set_rt_stop_type message.
********************************************************************************/

int fn_set_rt_stop_parameters(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_set_rt_stop_parameters* m;
	uint32_t slot;

	// LOG_printf(&trace, "set_rt_stop_parameters entry");

	m = (struct msg_set_rt_stop_parameters*)msg;

	*msglength = offsetof(struct msg_set_rt_stop_parameters, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	slot = m->slot;

	cnet_stop_rate[slot] = m->rate;
	cnet_stop_target[slot] = m->target;
	cnet_stop_time[slot] = m->time;

	// LOG_printf(&trace, "set_rt_stop_parameters exit");

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_rt_stop_parameters
  FUNCTION DETAILS  : Handler for read_rt_stop_parameters message.
********************************************************************************/

int fn_read_rt_stop_parameters(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_read_rt_stop_parameters* m;
	struct msg_return_rt_stop_parameters* r;
	uint32_t slot;

	m = (struct msg_read_rt_stop_parameters*)msg;
	r = (struct msg_return_rt_stop_parameters*)reply;

	*msglength = offsetof(struct msg_read_rt_stop_parameters, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	*replylength = offsetof(struct msg_return_rt_stop_parameters, end);

	slot = m->slot;

	r->command = MSG_RETURN_RT_STOP_PARAMETERS;

	r->rate = cnet_stop_rate[slot];
	r->target = cnet_stop_target[slot];
	r->time = cnet_stop_time[slot];

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_realtime_config
  FUNCTION DETAILS  : Handler for set_rt_stop_type message.
********************************************************************************/

int fn_set_realtime_config(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_set_realtime_config* m;

	m = (struct msg_set_realtime_config*)msg;

	*msglength = offsetof(struct msg_set_realtime_config, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	conn->stream->rate = (m->rate) ? (m->rate) : 1;	/* Ensure we never set a rate of zero */


	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_realtime_config
  FUNCTION DETAILS  : Handler for read_realtime_config message.
********************************************************************************/

int fn_read_realtime_config(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_return_realtime_config* r;

	r = (struct msg_return_realtime_config*)reply;

	*msglength = offsetof(struct msg_read_realtime_config, end);

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	*replylength = offsetof(struct msg_return_rt_stop_parameters, end);

	r->command = MSG_RETURN_RT_STOP_PARAMETERS;
	r->rate = conn->stream->rate;

	return(NO_ERROR);
}



/* Determine actual space in reply buffer for message data */

#define REPLYBUFFER_DATASPACE (DEFAULT_REPLYBUFFER - sizeof(commhdr) - offsetof(struct msg_return_realtime_data, data))

/********************************************************************************
  FUNCTION NAME   	: fn_transfer_realtime
  FUNCTION DETAILS  : Handler for transfer_realtime message.

					  See also skt_transfer_realtime_data() for equivalent
					  direct socket access, used to reduce the overhead of
					  copying message data.

********************************************************************************/

int fn_transfer_realtime_data(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn)
{
	struct msg_transfer_realtime_data* m;
	struct msg_return_realtime_data* r;
	uint32_t max_in;				/* Maximum size of input transfer (from computer)	*/
	uint32_t max_out;				/* Maximum size of output transfer (to computer)	*/
	uint32_t in_transfer;			/* No of packets in input transfer (from computer)	*/
	uint32_t out_transfer;			/* No of packets in output transfer (to computer)	*/
	uint8_t  status;				/* Realtime transfer status							*/
	uint32_t enable = RT_INPUT;	/* Local enable flags								*/
	uint32_t in_used;				/* Used space in rt_inbuf in packets				*/
	uint32_t out_free;				/* Free space in output buffer						*/

	m = (struct msg_transfer_realtime_data*)msg;
	r = (struct msg_return_realtime_data*)reply;

	if (!conn->stream) {
		return(error_no_stream(reply, replylength));
	}

	in_transfer = m->num_pkts;

	*msglength = offsetof(struct msg_transfer_realtime_data, data) + (in_transfer * conn->stream->outbuf.pktsize);

	status = (conn->stream->enable & (RT_INPUT | RT_OUTPUT | RT_RAMP)) << 4;
	if (conn->stream->inbuf.overrun) status |= RTSTATUS_OVERRUN;
	if (conn->stream->outbuf.underrun) status |= RTSTATUS_UNDERRUN;
	if (conn->stream->outbuf.empty) status |= RTSTATUS_EMPTY;

	/* Determine the amount of data that the output (command) buffer can take */
	if (conn->stream->outbuf.pktsize) {
		out_free = (conn->stream->outbuf.free / conn->stream->outbuf.pktsize);
	}
	else {
		out_free = 0;
	}
	max_in = out_free;
	if (in_transfer > max_in) in_transfer = max_in;

	/* If input data has been requested, enable realtime output transfers if not set to TSR control */

	if (in_transfer && !(conn->stream->enable & RT_OUTTSR)) enable |= RT_OUTPUT;

	/* Determine how much data is available to return from the input (feedback) buffer */

	max_out = m->max_pkts;
	if (conn->stream->inbuf.pktsize) {
		in_used = (conn->stream->inbuf.used / conn->stream->inbuf.pktsize);
	}
	else {
		in_used = 0;
	}
	out_transfer = in_used;
	if (out_transfer > max_out) out_transfer = max_out;

	/* Make sure that the requested output transfer cannot exceed the maximum message buffer length */

	if ((out_transfer * conn->stream->inbuf.pktsize) >= REPLYBUFFER_DATASPACE) {
		out_transfer = REPLYBUFFER_DATASPACE / conn->stream->inbuf.pktsize;
	}

	/* If in transfer required, transfer the incoming data from the message buffer into the realtime buffer */

	if (in_transfer)
		rt_bufin(&conn->stream->outbuf, (uint8_t*)m->data, (in_transfer * conn->stream->outbuf.pktsize));

	/* If out transfer required, transfer the outgoing data from the realtime buffer into the response buffer */

	if (out_transfer)
		rt_bufout((uint8_t*)r->data, &conn->stream->inbuf, (out_transfer * conn->stream->inbuf.pktsize));

	/* Enable realtime input transfers if not set to TSR control */

	if (!(conn->stream->enable & RT_INTSR)) conn->stream->enable |= enable;

	r->command = MSG_RETURN_REALTIME_DATA;
	*replylength = offsetof(struct msg_return_realtime_data, data) + (out_transfer * conn->stream->inbuf.pktsize);	/* Response data length */

	/* Copy the new buffer sizes to the response message structure */

	r->num_pkts_ack = in_transfer;		/* Number of packets written into output (command) buffer 		      			*/
	r->num_pkts_free = out_free;		/* Amount of free space in output (command) buffer prior to this transfer     	*/
	r->num_pkts_used = in_used;			/* Amount of data available in input (response) buffer prior to this transfer 	*/
	r->num_pkts_input = out_transfer;	/* Number of packets read from input (response) buffer 			      			*/
	r->status = status;
	r->samplectr_lsh = samplectr_lsh;
	r->samplectr_msh = samplectr_msh;

	return(NO_ERROR);
}


extern uint32_t debug_rt_bufin_count;
extern uint32_t debug_rt_rampin_count;
extern uint32_t debug_rt_copy_count;
extern uint32_t debug_rt_notxfr_count;
extern uint32_t debug_rt_underrun_count;
extern uint32_t debug_rt_overrun_count;
extern uint32_t debug_rt_ramp_count;
extern uint32_t debug_rt_txfr_count;
extern uint32_t debug_rt_noinput_count;
extern uint32_t debug_rt_local_count;
extern uint32_t debug_cnet_bufin_count;

/********************************************************************************
  FUNCTION NAME   	: skt_transfer_realtime_data
  FUNCTION DETAILS  : Handler for direct socket realtime transfer.

					  On entry the incoming message has been read upto and
					  including the message code.

********************************************************************************/

int skt_transfer_realtime_data(int skt, uint32_t rxlen, connection* conn)
{
	struct msg_transfer_realtime_data* m;
	struct msg_return_realtime_data* r;
	uint32_t max_in;				/* Maximum size of input transfer (from computer)	*/
	uint32_t max_out;				/* Maximum size of output transfer (to computer)	*/
	uint32_t in_transfer;			/* No of packets in input transfer (from computer)	*/
	uint32_t out_transfer;			/* No of packets in output transfer (to computer)	*/
	uint8_t  status;				/* Realtime transfer status							*/
	uint32_t enable = RT_INPUT;	/* Local enable flags								*/
	int   len;					/* Socket read operation length						*/
	uint32_t in_used;				/* Used space in rt_inbuf in packets				*/

	uint32_t out_free;				/* Free space in output buffer						*/

	uint8_t  msg[256];				/* Local buffer for message parameters				*/
	uint8_t  reply[256];			/* Local buffer for reply parameters				*/

	uint32_t rem;

	commhdr* hdr;
	int   err;
	int   errno;

	m = (struct msg_transfer_realtime_data*)msg;
	hdr = (commhdr*)reply;
	r = (struct msg_return_realtime_data*)(reply + sizeof(commhdr));

	// +2 to take account that the header has been read already
	err = recv(skt, (unsigned char*)msg + 2, offsetof(struct msg_transfer_realtime_data, data) - 2, 0);
	if ((err == 0) || ((err < 0) && (errno == ECONNRESET))) {
		// LOG_printf(&trace, "Error 1 skt_transfer_realtime_data = %d", err);
		return(NO_ERROR);
	}

	rxlen -= err;

	in_transfer = m->num_pkts;

	if (!conn->stream) {
		uint8_t discard;
		/* No realtime stream, so absorb all input data */
		while (rxlen)
		{
			recv(skt, &discard, 1, 0);
			rxlen--;
		}
		return(ERROR_NO_STREAM);
	}

	status = (conn->stream->enable & (RT_INPUT | RT_OUTPUT | RT_RAMP)) << 4;
	if (conn->stream->inbuf.overrun) status |= RTSTATUS_OVERRUN;
	if (conn->stream->outbuf.underrun) status |= RTSTATUS_UNDERRUN;
	if (conn->stream->outbuf.empty) status |= RTSTATUS_EMPTY;

#if 0

	LOG_printf(&trace, "enable = %d", conn->stream->enable);
	LOG_printf(&trace, "pktsize = %d", conn->stream->outbuf.pktsize);
	LOG_printf(&trace, "out free = %d", conn->stream->outbuf.free);
	LOG_printf(&trace, "out used = %d", conn->stream->outbuf.used);
	LOG_printf(&trace, "out underrun = %d", conn->stream->outbuf.underrun);
	LOG_printf(&trace, "out overrun = %d", conn->stream->outbuf.overrun);

	LOG_printf(&trace, "in free = %d", conn->stream->inbuf.free);
	LOG_printf(&trace, "in used = %d", conn->stream->inbuf.used);
	LOG_printf(&trace, "in underrun = %d", conn->stream->inbuf.underrun);
	LOG_printf(&trace, "in overrun = %d", conn->stream->inbuf.overrun);

	LOG_printf(&trace, "cnet count = %d", cnet_interrupt_ctr);
	LOG_printf(&trace, "cnet_bufin_count = %d", debug_cnet_bufin_count);
	LOG_printf(&trace, "rt_txfr_count = %d", debug_rt_txfr_count);
	LOG_printf(&trace, "rt_bufin_count = %d", debug_rt_bufin_count);
	LOG_printf(&trace, "rt_rampin_count = %d", debug_rt_rampin_count);
	LOG_printf(&trace, "rt_copy_count = %d", debug_rt_copy_count);
	LOG_printf(&trace, "rt_notxfr_count = %d", debug_rt_notxfr_count);
	LOG_printf(&trace, "rt_underrun_count = %d", debug_rt_underrun_count);
	LOG_printf(&trace, "rt_overrun_count = %d", debug_rt_overrun_count);
	LOG_printf(&trace, "rt_ramp_count = %d", debug_rt_ramp_count);
	LOG_printf(&trace, "rt_noinput_count = %d", debug_rt_noinput_count);
	LOG_printf(&trace, "rt_local_count = %d", debug_rt_local_count);

#endif

	/* Determine the amount of data that the output (command) buffer can take */

	if (conn->stream->outbuf.pktsize) {
		out_free = (conn->stream->outbuf.free / conn->stream->outbuf.pktsize);
	}
	else {
		out_free = 0;
	}
	max_in = out_free;
	if (in_transfer > max_in) in_transfer = max_in;

	/* If input data has been received, enable realtime output transfers if not set to TSR control
	   and not in paused state.
	*/

	if (in_transfer && !(conn->stream->enable & (RT_OUTTSR | RT_PAUSED))) enable |= RT_OUTPUT;

	/* Determine how much data is available to return */

	max_out = m->max_pkts;
	if (conn->stream->inbuf.pktsize) {
		in_used = (conn->stream->inbuf.used / conn->stream->inbuf.pktsize);
	}
	else {
		in_used = 0;
	}
	out_transfer = in_used;
	if (out_transfer > max_out) out_transfer = max_out;

	/* If input transfer is required, transfer the incoming data from the socket into the realtime buffer */

	if (in_transfer) {
		rt_sktin(skt, &conn->stream->outbuf, (in_transfer * conn->stream->outbuf.pktsize), conn);
	}

	/* Absorb any unused input data */

	if (rxlen > (in_transfer * conn->stream->outbuf.pktsize)) {
		rem = rxlen - (in_transfer * conn->stream->outbuf.pktsize);
		printf("absorb: %d\n", rem);
		uint8_t discard;
		while (rem)
		{
			len = recv(skt, &discard, 1, 0);
			if ((len == 0) || ((len < 0) && (errno == ECONNRESET))) {
				// LOG_printf(&trace, "Error 2 skt_transfer_realtime_data = %d", len);
				return(NO_ERROR);
			}
			rem--;
		}
	}

	/* Build response header */

	hdr->dst = 0;
	hdr->length = offsetof(struct msg_return_realtime_data, data) + (out_transfer * conn->stream->inbuf.pktsize);

	/* Build response message */

	r->command = MSG_RETURN_REALTIME_DATA;
	r->num_pkts_ack = in_transfer;
	r->num_pkts_free = out_free;
	r->num_pkts_used = in_used;
	r->num_pkts_input = out_transfer;
	r->status = status;
	r->samplectr_lsh = samplectr_lsh;
	r->samplectr_msh = samplectr_msh;

	/* Transfer the header and the parameter section of the response data */

	err = send(skt, hdr, sizeof(commhdr) + offsetof(struct msg_return_realtime_data, data), 0);
	if (err < 0) {
		// LOG_printf(&trace, "Error 3 skt_transfer_realtime_data = %d", err);
		return(NO_ERROR);
	}

	/* If out transfer required, transfer the outgoing data from the realtime buffer into the socket */

	if (out_transfer) {
		rt_sktout(skt, &conn->stream->inbuf, (out_transfer * conn->stream->inbuf.pktsize));
	}

	/* Enable realtime input transfers if not set to TSR control */

	if (!(conn->stream->enable & RT_INTSR)) conn->stream->enable |= enable;

	return(NO_ERROR);
}
