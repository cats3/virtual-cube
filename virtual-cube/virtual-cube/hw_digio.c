/********************************************************************************
 * MODULE NAME       : hw_digio.c												*
 * MODULE DETAILS    : Interface routines for Digital I/O card.					*
 *																				*
 ********************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <stdint.h>
#include "porting.h"

#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"


 /*******************************************************************************
  FUNCTION NAME	   : hw_digio_io_set_cnet_output
  FUNCTION DETAILS  : Sets the CNet mapping for digital input channels.

					  On entry:

					  def	Pointer to channel being mapped.

					  slot	Destination CNet output slot.

 ********************************************************************************
  TASK OR INTERRUPT SOURCE :
 ********************************************************************************
  RESOURCE USAGE :
 ********************************************************************************
  Called From :
  Calls:
 *******************************************************************************/

void hw_digio_io_set_cnet_output(chandef* def, int slot)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	slot_info* s = &slot_status[EXTSLOTNUM];

	s->digip_cnetptr = cnet_get_slotaddr(slot, PROC_DSPA);
	s->digip_srcptr = &def->hw.i_digio.hw->u.rd.input;	/* Pointer to input register */
#endif
}

/******************************************************
* Table of handler functions for digital I/O channels *
******************************************************/

void* hwdigio_fntable[] = {
  NULL,								/* Fn  0. Ptr to readstatus function 				*/
  NULL,								/* Fn  1. Ptr to setpointer function				*/
  NULL,								/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  NULL,								/* Fn  6. Ptr to savecalibration function 			*/
  NULL,								/* Fn  7. Ptr to restorecalibration function 		*/
  NULL,								/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function	 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  NULL,								/* Fn 13. Ptr to setgain function 					*/
  NULL,								/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  NULL,								/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  NULL,								/* Fn 23. Ptr to readadc function 					*/
  NULL,								/* Fn 24. Ptr to setfilter function	 				*/
  NULL,								/* Fn 25. Ptr to readfilter function 				*/
  NULL,								/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function 			*/
  NULL,								/* Fn 28. Ptr to set_caltable function 				*/
  NULL,								/* Fn 29. Ptr to read_caltable function 			*/
  NULL,								/* Fn 30. Ptr to set_calgain function 				*/
  NULL,								/* Fn 31. Ptr to read_calgain function 				*/
  NULL,								/* Fn 32. Ptr to set_caloffset function 			*/
  NULL,								/* Fn 33. Ptr to read_caloffset function 			*/
  &hw_digio_io_set_cnet_output,		/* Fn 34. Ptr to set_cnet_slot function 			*/
  NULL,								/* Fn 35. Ptr to set_gaintrim function 				*/
  NULL,								/* Fn 36. Ptr to read_gaintrim function 			*/
  NULL,								/* Fn 37. Ptr to set_offsettrim function 			*/
  NULL,								/* Fn 38. Ptr to read_offsettrim function 			*/
  NULL,								/* Fn 39. Ptr to set_txdrzero function				*/
  NULL,								/* Fn 40. Ptr to read_txdrzero function				*/
  NULL,								/* Fn 41. Ptr to readpeak function 					*/
  NULL,								/* Fn 42. Ptr to resetpeak function 				*/
  NULL,								/* Fn 43. Ptr to set_flags function 				*/
  NULL,								/* Fn 44. Ptr to read_flags function 				*/
  NULL,								/* Fn 45. Ptr to write_map function 				*/
  NULL,								/* Fn 46. Ptr to read_map function 					*/
  NULL,								/* Fn 47. Ptr to set_refzero function 				*/
  NULL,								/* Fn 48. Ptr to read_refzero function 				*/
  NULL,								/* Fn 49. Ptr to undo_refzero function 				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  NULL,								/* Fn 52. Ptr to tedsid function 					*/
  NULL,								/* Fn 53. Ptr to set_source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  &chan_io_set_info,				/* Fn 58. Ptr to set info function					*/
  &chan_io_read_info,				/* Fn 59. Ptr to read info function					*/
  &chan_io_write_output,			/* Fn 60. Ptr to write output function				*/
  &chan_io_read_input,				/* Fn 61. Ptr to read input function				*/
  &chan_io_read_output,				/* Fn 62. Ptr to read output function				*/
  &chan_io_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_io_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  NULL,								/* Fn 72. Ptr to txdrzero_zero function				*/
  NULL,								/* Fn 73. Ptr to readcycle function					*/
  NULL,								/* Fn 74. Ptr to resetcycles function 				*/
  NULL,								/* Fn 75. Ptr to setpkthreshold function 			*/
  NULL,								/* Fn 76. Ptr to readpkthreshold function 			*/
  NULL,								/* Fn 77. Ptr to writefaultmask function 			*/
  NULL,								/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  NULL,								/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};



/********************************************************************************
  FUNCTION NAME     : hw_digio_install
  FUNCTION DETAILS  : Install Digital I/O hardware on the selected channel(s).

					  On entry:

					  slot			Slot number being installed.

					  numinchan		Points to the variable holding the number of
									input channels installed for this slot.

					  numoutchan	Points to the variable holding the number of
									output channels installed for this slot.

					  nummiscchan	Points to the variable holding the number of
									miscellaneous channels installed for this slot.

					  numdigiochan	Points to the variable holding the number of
									digital I/O channels installed for this slot.

					  flags			Controls if channel allocation is performed.
									If not, then only initialisation operations
									are performed.
********************************************************************************/

uint32_t hw_digio_install(int slot, int* numinchan, int* numoutchan, int* nummiscchan,
	int* numdigiochan, hw_digio* hw, int flags)
{
	info_digio* i;
	uint32_t eecfg_addr = CHAN_EEBANK_DATASTART;	/* Start of channel EEPROM configuration area */
	uint32_t eerdcfg_addr = CHAN_EEBANK_DATASTART;	/* Address for reading EEPROM configuration	  */
	uint32_t status = 0;
	chandef* def;
	uint32_t input;
	uint32_t output;
	uint32_t mask;
	uint32_t chan;

	chan = slot_status[slot].basedigiochan;

	/***************************************************************************
	* Perform initial allocation of channels and setting of default parameters *
	***************************************************************************/

	/* Allocate the digital input channels */

	mask = 0x01;
	for (input = 0; input < 8; input++) {
		cfg_io* cfg;

		def = &digiochaninfo[chan];
		cfg = &def->cfg.i_io;

		/* Set defaults for digital I/O input channel */

		def->status = 0;
		chan_io_default(def, chan, (DIGIO_INPUT | DIGIO_NORMAL));
		mask <<= 1;

		if (flags & DO_ALLOCATE) {

			i = &def->hw.i_digio;

			/* Define local hardware/workspace settings */

			i->hw = hw;									/* Pointer to hardware base				*/
			init_fntable(def, hwdigio_fntable);			/* Set pointers to control functions 	*/
			SETDEF(slot, TYPE_DIGIO, 0, (CHANTYPE_DIGIO | chan));

			/* Allocate space in EEPROM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
			def->eecal_start = CHAN_EE_CALSTART; def->eecal_size = 0;
		}

		/* Restore current calibration and configuration data */

		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

		/* Set the mask for the digital input */

		cfg->mask = 1 << input;

		/* Force the direction flag to match reality */

		cfg->flags = cfg->flags & ~DIGIO_OUTPUT;

		chan++;
	}

	/* Allocate the digital output channels */

	mask = 0x01;
	for (output = 0; output < 8; output++) {
		cfg_io* cfg;

		def = &digiochaninfo[chan];
		cfg = &def->cfg.i_io;

		/* Set defaults for digital I/O output channel */

		def->status = 0;
		chan_io_default(def, chan, (DIGIO_OUTPUT | DIGIO_NORMAL));
		mask <<= 1;

		if (flags & DO_ALLOCATE) {

			i = &def->hw.i_digio;

			/* Define local hardware/workspace settings */

			i->hw = hw;									/* Pointer to mainboard hardware base	*/
			init_fntable(def, hwdigio_fntable);			/* Set pointers to control functions 	*/
			SETDEF(slot, TYPE_DIGIO, 0, (CHANTYPE_DIGIO | chan));

			/* Allocate space in EEPROM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
			def->eecal_start = CHAN_EE_CALSTART; def->eecal_size = 0;
		}

		/* Restore current calibration and configuration data */

		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

		/* Set the mask for the digital output */

		cfg->mask = 1 << output;

		/* Force the direction flag to match reality */

		cfg->flags = cfg->flags | DIGIO_OUTPUT;

		chan++;
	}

	/* Clear the output port */

	hw->u.wr.output = 0;

	/* Configure the slot_status table information for the digital input channels. This is
	   used by the CNet digital input acquisition code when copying the state into the CNet
	   slot memory.
	*/

	slot_status[slot].digip_mask = 0xFF;
	slot_status[slot].digip_invert = 0;
	for (input = 0; input < 8; input++) {
		cfg_io* cfg;

		def = &digiochaninfo[slot_status[slot].basedigiochan + input];
		cfg = &def->cfg.i_io;

		if (cfg->flags & DIGIO_INVERT) slot_status[slot].digip_invert |= (1 << input);
	}

	/* Update channel counts */

	if (flags & DO_ALLOCATE) {
		totaldigiochan += 16;
		*numinchan = 0;
		*numoutchan = 0;
		*nummiscchan = 0;
		*numdigiochan = 16;
	}

	return(status);
}



/********************************************************************************
  FUNCTION NAME     : hw_digio_save
  FUNCTION DETAILS  : Save the digital I/O channel settings to EEPROM.

					  If a bad list or an old list format was detected on any
					  channels, then all of the channels must be updated.

					  On entry:

					  slot		Slot number being saved.

					  status	System configuration status. This indicates if
								a full save operation is required because an
								old or bad format list was detected during
								initialisation.

********************************************************************************/

void hw_digio_save(int slot, uint32_t status)
{
	int n;
	chandef* def;

	/* Update the digital I/O channels */

	for (n = 0; n < 16; n++) {
		def = &digiochaninfo[slot_status[slot].basedigiochan + n];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

}
