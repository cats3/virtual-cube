/********************************************************************************
 * MODULE NAME       : configuration.c											*
 * MODULE DETAILS    : System configuration routines							*
 *																				*
 ********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <ctype.h>

#include <stdint.h>
#include "porting.h"

 /* The following two lines are required to allow Events.h to compile */

#define SUPGEN
#define Parameter void

//#include "tcp_ip/support.h"
#include "defines.h"
#include "hardware.h"
#include "channel.h"
#include "shared.h"
//#include "headers.h"
#include "eeprom.h"
//#include "hw_kinet.h"
//#include "eventlog.h"
#include "HardwareIndependent/Events.h"
#include "hydraulic.h"
#include "cnet.h"
//#include "debug.h"
#include "nonvol.h"
#include "controller.h"

/* Define TEST8CHAN to make a test version to simulate having a system
   with three additional 2TX cards, giving a total of 8 standard feedback
   channels.
*/

#undef TEST8CHAN


slot_info slot_status[4];	/* Information for each expansion slot 		*/
uint32_t slot_nv_status = 0;	/* Combined NV status for expansion slots	*/
uint32_t slot_txdrfault = 0;	/* Combined transducer fault status			*/

/* External symbols */

extern const unsigned char fpga_4ad2da[];	/* Bitstream for 4AD2DA FPGA 	*/
extern const unsigned char fpga_2gp1sv[];	/* Bitstream for 2GP1SV FPGA 	*/
extern const unsigned char fpga_kinet[];	/* Bitstream for KINET FPGA  	*/
extern const unsigned char fpga_digio[];	/* Bitstream for DIGIO FPGA  	*/
extern const unsigned char fpga_2dig[];		/* Bitstream for 2DIG FPGA 		*/
extern const unsigned char fpga_testcard[];	/* Bitstream for TESTCARD FPGA  */
extern const unsigned char fpga_canbus[];	/* Bitstream for CANBUS FPGA	*/

/* External functions */

void netprintf(int s, char* fmt, ...);



/********************************************************************************
  FUNCTION NAME     : write_slot_config_data
  FUNCTION DETAILS  : Write configuration data to mainboard or expansion card
					  EEPROM.

					  On entry:

					  slot	Holds the slot number to be written.

					  type	Holds type code of slot.

					  data	Points to an area of 251 bytes holding the
							configuration data to be written.
********************************************************************************/

void write_slot_config_data(int slot, uint8_t type, uint8_t* data)
{
	int n;

	for (n = 5; n < 256; n++) {

		/* Write address to I2C bus */

		dsp_i2c_start();
	busy1:
		if (dsp_i2c_send(0xA0 | (slot << 1))) {
			dsp_i2c_restart();
			goto busy1;
		}
		dsp_i2c_send(0);
		dsp_i2c_send(n);
		dsp_i2c_restart();

		/* Write data from local memory into EEPROM */

		dsp_i2c_send(0xA0 | (slot << 1));
		dsp_i2c_send(*data++);

		/* Terminate I2C bus operation */

		dsp_i2c_stop();

		/* Wait for write operation to complete */

	}

	/* Write address to I2C bus */

	dsp_i2c_start();

busy2:

	if (dsp_i2c_send(0xA0 | (slot << 1))) {
		dsp_i2c_restart();
		goto busy2;
	}
	dsp_i2c_send(0);
	dsp_i2c_send(0);
	dsp_i2c_restart();

	/* Write type code and validity check data to EEPROM */

	dsp_i2c_send(0xA0 | (slot << 1));
	dsp_i2c_send(type);
	dsp_i2c_send(0xAA);
	dsp_i2c_send(0x55);
	dsp_i2c_send(0x00);
	dsp_i2c_send(0xFF);

	/* Terminate I2C bus operation */

	dsp_i2c_stop();

	/* Wait for write operation to complete */

}



/********************************************************************************
  FUNCTION NAME     : write_eeprom
  FUNCTION DETAILS  : Write a block of data to the mainboard system
					  configuration I2C EEPROM.

					  On entry:

					  addr		Start address within EEPROM

					  src		Points to start of data block to be written

					  len		Number of bytes to be written

********************************************************************************/

void write_eeprom(int addr, int len, void* src)
{
	dsp_i2c_eeprom_writeblock(0xA0, addr, (uint8_t*)src, len, 0);
}



/********************************************************************************
  FUNCTION NAME     : read_eeprom
  FUNCTION DETAILS  : Read a block of data from the mainboard system
					  configuration I2C EEPROM.

					  On entry:

					  addr		Start address within EEPROM

					  dst		Points to start of data block to be read

					  len		Number of bytes to be read

********************************************************************************/

void read_eeprom(int addr, int len, void* dst)
{
	dsp_i2c_eeprom_readblock(0xA0, addr, (uint8_t*)dst, len, 0);
}



/* Table of pointers to FPGA bitstreams for various expansion card types */

static uint8_t* const fpgatable[] = { NULL,					/* 0  No card  					*/
								   NULL,					/* 1  Not used (mainboard)		*/
								   NULL,//(uint8_t*)fpga_2gp1sv,		/* 2  1SV2TX card				*/
								   NULL,//(uint8_t*)fpga_4ad2da,		/* 3  2DA4AD card				*/
								   NULL,//(uint8_t*)fpga_digio,		/* 4  Digital I/O card			*/
								   NULL,//(uint8_t*)fpga_kinet,		/* 5  KiNet card				*/
								   NULL,					/* 6  4SG card (Signal Cube)	*/
								   NULL,//(uint8_t*)fpga_2gp1sv,		/* 7  2TX card					*/
								   NULL,					/* 8  4AD card					*/
								   NULL,					/* 9  1DA4AD card				*/
								   NULL,					/* 10 4LVDT card (Signal Cube)	*/
								   NULL,					/* 11 4ACC card (Signal Cube)	*/
								   NULL,					/* 12 5ADC card (Signal Cube)	*/
								   NULL,//(uint8_t*)fpga_testcard,	/* 13 Reserved for test h/w		*/
								   NULL,//(uint8_t*)fpga_2dig,		/* 14 2DIG card					*/
								   NULL,//(uint8_t*)fpga_canbus		/* 15 CAN bus card				*/
};

/********************************************************************************
  FUNCTION NAME     : slot_ident
  FUNCTION DETAILS  : Identify expansion card presence and load configuration
					  data.

					  If the flush flag is set, then use default settings for
					  all configuration values.
********************************************************************************/

void slot_ident(uint32_t flush)
{
	uint8_t data[256];				/* Temporary store for EEPROM data */
	uint8_t* fpgadata[4];			/* Pointers to FPGA bitstream code */
	int slot;
	int n;
	int in;
	int out;
	int misc;
	int digio;
	uint32_t status;				/* Flags indicating configuration status 	  */
	uint32_t mboard_eecal_addr;	/* Used EEPROM calibration address range	  */
	uint32_t mboard_eecfg_addr;	/* Used EEPROM configuration address range	  */
	uint32_t mboard_eecfg_rdaddr;	/* Read EEPROM configuration address range	  */

	hw_bootlog(50, NULL, 0);

	/* Initialise all channel definition structures */

	for (n = 0; n < MAXINCHAN; n++) {
		memset(&inchaninfo[n], 0, sizeof(chandef));
	}
	for (n = 0; n < MAXOUTCHAN; n++) {
		memset(&outchaninfo[n], 0, sizeof(chandef));
	}
	for (n = 0; n < MAXMISCCHAN; n++) {
		memset(&miscchaninfo[n], 0, sizeof(chandef));
	}
	for (n = 0; n < MAXDIGIOCHAN; n++) {
		memset(&digiochaninfo[n], 0, sizeof(chandef));
	}

	hw_bootlog(51, NULL, 0);

	/* Clear slot_status table before initialising */

	memset(slot_status, 0, sizeof(slot_status));

	/* Initialise all slots to CARD_NONE */

	for (slot = 0; slot < 4; slot++) {
		fpgadata[slot] = NULL;								/* No FPGA bitstream 		*/
		slot_status[slot].type = CARD_NONE;					/* No card present 			*/
		strncpy(slot_status[slot].serialno, "                ", 16);
		/* No serial number			*/
//	slot_status[slot].numinchan = 0;					/* No input channels		*/
//	slot_status[slot].numoutchan = 0;					/* No output channels		*/
//	slot_status[slot].nummiscchan = 0;					/* No misc channels			*/
//	slot_status[slot].numdigiochan = 0;					/* No digital I/O channels	*/
//	slot_status[slot].faultmask = 0;					/* Disable all fault masks	*/
//	slot_status[slot].txdrfault = 0;					/* No transducer faults		*/

	/* Clear status data area */

//	memset(slot_status[slot].data, 0, sizeof(slot_status[slot].data));
	}

	hw_bootlog(52, NULL, 0);

	/* Read identification and configuration data from expansion slot cards */

	for (slot = 1; slot < 4; slot++) {

		/* Initialise local config data array */

		data[0] = 0xFF;

		/* Write address to I2C bus */

		dsp_i2c_start();
		if (dsp_i2c_send(0xA0 | (slot << 1))) goto no_card;
		if (dsp_i2c_send(0)) goto no_card;
		if (dsp_i2c_send(0)) goto no_card;
		dsp_i2c_restart();

		/* Read from EEPROM into local memory */

		if (dsp_i2c_send(0xA1 | (slot << 1))) goto no_card;
		for (n = 0; n < 256; n++) {
			data[n] = dsp_i2c_read((n == 255) ? 0 : 1);	/* No ack on final read */
		}

	no_card:

		/* Terminate I2C bus operation */

		dsp_i2c_stop();

		/* Test if valid data in memory and update slot status structure */

		if ((data[1] == 0xAA) && (data[2] == 0x55) && (data[3] == 0x00) && (data[4] == 0xFF)) {

			/* Test for valid card type. If valid, copy card configuration data and
			   setup FPGA bitstream pointer.
			*/

			if ((data[0] > CARD_MAINBOARD) && (data[0] <= CARD_CANBUS)) {

				slot_status[slot].type = data[0];
				memcpy(slot_status[slot].serialno, &data[EEPROM_OFFSET_SERIALNO], sizeof(slot_status[slot].serialno));
				memcpy(slot_status[slot].data, &data[EEPROM_OFFSET_GENERAL], sizeof(slot_status[slot].data));
				fpgadata[slot] = fpgatable[slot_status[slot].type];
			}
		}

	}

	hw_bootlog(53, NULL, 0);

	/* Force slot 0 to be type CARD_MAINBOARD and copy configuration data if valid */

	slot_status[0].type = CARD_MAINBOARD;

	/* Read board serial number from 1-wire memory. Try initially to read the new
	   16 character serial number string. If an error occurs, then assume this is
	   an old-style 8 character serial number instead.
	*/

	if (dsp_1w_readpage(PAGE_BOARDSN, (uint8_t*)slot_status[0].serialno, 16, CHK_CHKSUM))
		dsp_1w_readpage(PAGE_BOARDSN, (uint8_t*)slot_status[0].serialno, 8, CHK_CHKSUM);

	/* Read from EEPROM into local memory */

	dsp_i2c_eeprom_readblock(0xA0, 0, data, 256, 0);
	if ((data[1] == 0xAA) && (data[2] == 0x55) && (data[3] == 0x00) && (data[4] == 0xFF)) {
		memcpy(slot_status[0].data, &data[EEPROM_OFFSET_GENERAL], sizeof(slot_status[0].data));
	}

	hw_bootlog(54, NULL, 0);

	/* Initialise any FPGAs on the expansion cards */

	config_exp_fpga(fpgadata[1], fpgadata[2], fpgadata[3]);
	hw_bootlog(60, NULL, 0);

	/* Initialise channels */

	totalinchan = 0;
	physinchan = 0;
	totaloutchan = 0;
	totalmiscchan = 0;
	totaldigiochan = 0;
	totalvirtualchan = 0;

	status = 0;

#ifndef TEST8CHAN

	for (slot = 0; slot < 4; slot++) {

		slot_status[slot].baseinchan = totalinchan;
		slot_status[slot].baseoutchan = totaloutchan;
		slot_status[slot].basemiscchan = totalmiscchan;
		slot_status[slot].basedigiochan = totaldigiochan;

		in = 0;
		out = 0;
		misc = 0;
		digio = 0;

		/* Install appropriate handler functions */

		switch (slot_status[slot].type) {
		case CARD_MAINBOARD:
			status |= hw_mboard_install(slot, &in, &out, &misc, &digio, (hw_mboard*)slottable[slot],
				(flush | DO_ALLOCATE), &mboard_eecal_addr, &mboard_eecfg_addr,
				&mboard_eecfg_rdaddr);
			break;
		case CARD_2GP1SV:
			status |= hw_2gp1sv_install(slot, &in, &out, &misc, (hw_2gp1sv*)slottable[slot],
				(flush | DO_ALLOCATE));
			break;
		case CARD_4AD2DA:
			status |= hw_4ad2da_install(slot, &in, &out, &misc, (hw_4ad2da*)slottable[slot],
				(flush | DO_ALLOCATE));
			break;
		case CARD_DIGIO:
			status |= hw_digio_install(slot, &in, &out, &misc, &digio, (hw_digio*)slottable[slot],
				(flush | DO_ALLOCATE));
			break;
		case CARD_KINET:
			// TODO: No support for kinet
			//status |= hw_kinet_install(slot, (hw_kinet*)slottable[slot], DO_ALLOCATE);
			break;
		case CARD_2TX:
			status |= hw_2gp1sv_install(slot, &in, &out, &misc, (hw_2gp1sv*)slottable[slot],
				(flush | DO_ALLOCATE | NO_SV));
			break;
		case CARD_4AD:
			status |= hw_4ad2da_install(slot, &in, &out, &misc, (hw_4ad2da*)slottable[slot],
				(flush | DO_ALLOCATE | NO_DA));
			break;
		case CARD_2DIG:
			status |= hw_2dig_install(slot, &in, &out, &misc, (hw_2dig*)slottable[slot],
				(flush | DO_ALLOCATE));
			break;
		case CARD_CANBUS:
			// TODO: no support for canbus
			//status |= hw_can_install(slot, &in, &out, &misc, (hw_can*)slottable[slot],
			//	(flush | DO_ALLOCATE));
			break;
		case CARD_NONE:
		default:
			break;
		}

		hw_bootlog(61 + slot, NULL, 0);

		/* Update channel counts in slot definition */

		slot_status[slot].numinchan = in;
		slot_status[slot].numoutchan = out;
		slot_status[slot].nummiscchan = misc;
		slot_status[slot].numdigiochan = digio;
		slot_status[slot].numchan = in + out + misc + digio;

	}

#else

	/* Special code - just install mainboard hardware */

	for (slot = 0; slot < 1; slot++) {

		slot_status[slot].baseinchan = totalinchan;
		slot_status[slot].baseoutchan = totaloutchan;
		slot_status[slot].basemiscchan = totalmiscchan;
		slot_status[slot].basedigiochan = totaldigiochan;

		in = 0;
		out = 0;
		misc = 0;
		digio = 0;

		/* Install mainboard handler functions */

		status |= hw_mboard_install(slot, &in, &out, &misc, &digio, (hw_mboard*)slottable[slot],
			(flush | DO_ALLOCATE), &mboard_eecal_addr, &mboard_eecfg_addr,
			&mboard_eecfg_rdaddr);

		/* Update channel counts in slot definition */

		slot_status[slot].numinchan = in;
		slot_status[slot].numoutchan = out;
		slot_status[slot].nummiscchan = misc;
		slot_status[slot].numdigiochan = digio;
		slot_status[slot].numchan = in + out + misc + digio;

	}

#if 1

	/* Bodge 3 extra "dummy" 1SV2TX cards into the system */

	for (slot = 1; slot < 4; slot++) {

		slot_status[slot].baseinchan = totalinchan;
		slot_status[slot].baseoutchan = totaloutchan;
		slot_status[slot].basemiscchan = totalmiscchan;
		slot_status[slot].basedigiochan = totaldigiochan;

		in = 0;
		out = 0;
		misc = 0;
		digio = 0;

		/* Install appropriate handler functions */

		status |= hw_2gp1sv_install(slot, &in, &out, &misc, (hw_2gp1sv*)slottable[0],
			(flush | DO_ALLOCATE));

		/* Update channel counts in slot definition */

		slot_status[slot].numinchan = in;
		slot_status[slot].numoutchan = out;
		slot_status[slot].nummiscchan = misc;
		slot_status[slot].numdigiochan = digio;
		slot_status[slot].numchan = in + out + misc + digio;

	}

#endif

#endif

	hw_bootlog(66, NULL, 0);

	/* Allocate fixed virtual channels on the end of the input channel block */

	status |= hw_mboard_install_virtual(0, &totalvirtualchan, mboard_eecal_addr, mboard_eecfg_addr, mboard_eecfg_rdaddr, (flush | DO_ALLOCATE));

	hw_bootlog(67, NULL, 0);

#ifndef TEST8CHAN

	/* Now update any channel configuration data if required */

	for (slot = 0; slot < 4; slot++) {

		/* Update appropriate configuration */

		switch (slot_status[slot].type) {
		case CARD_MAINBOARD:
			hw_mboard_save(slot, status);
			break;
		case CARD_2GP1SV:
			hw_2gp1sv_save(slot, status);
			break;
		case CARD_4AD2DA:
			hw_4ad2da_save(slot, status);
			break;
		case CARD_DIGIO:
			hw_digio_save(slot, status);
			break;
		case CARD_KINET:
			break;
		case CARD_2TX:
			hw_2tx_save(slot, status);
			break;
		case CARD_2DIG:
			hw_2dig_save(slot, status);
			break;
		case CARD_CANBUS:
			// TODO: no support for canbus
			//hw_can_save(slot, status);
			break;
		case CARD_NONE:
		default:
			break;
		}
		hw_bootlog(68 + slot, NULL, 0);
	}

#else

	for (slot = 0; slot < 1; slot++) {

		/* Update appropriate configuration */

		switch (slot_status[slot].type) {
		case CARD_MAINBOARD:
			hw_mboard_save(slot, status);
			break;
		case CARD_2GP1SV:
			hw_2gp1sv_save(slot, status);
			break;
		case CARD_4AD2DA:
			hw_4ad2da_save(slot, status);
			break;
		case CARD_DIGIO:
			hw_digio_save(slot, status);
			break;
		case CARD_KINET:
			break;
		case CARD_2TX:
			hw_2tx_save(slot, status);
			break;
		case CARD_CANBUS:
			hw_can_save(slot, status);
			break;
		case CARD_NONE:
		default:
			break;
		}

	}

#endif

	/* Update channel counters in shared memory */

	hw_bootlog(72, NULL, 0);

	update_shared(offsetof(shared, totalinchan), &totalinchan, sizeof(totalinchan));
	update_shared(offsetof(shared, totaloutchan), &totaloutchan, sizeof(totaloutchan));
	update_shared(offsetof(shared, totalmiscchan), &totalmiscchan, sizeof(totalmiscchan));
	update_shared(offsetof(shared, totalvirtualchan), &totalvirtualchan, sizeof(totalvirtualchan));
	update_shared(offsetof(shared, physinchan), &physinchan, sizeof(physinchan));

	/* Update overall slot NV status */

	slot_nv_status = status;
	hw_bootlog(736, NULL, 0);
}



/********************************************************************************
  FUNCTION NAME     : slot_post_init
  FUNCTION DETAILS  : Perform post initialisation configuration of expansion
					  cards.
********************************************************************************/

void slot_post_init(void)
{
	int slot;
	uint32_t status;				/* Flags indicating configuration status 	  */

	status = 0;

	for (slot = 0; slot < 4; slot++) {

		/* Initialise appropriate handler functions */

		switch (slot_status[slot].type) {
		case CARD_2DIG:
			status |= hw_2dig_post_install(slot, (hw_2dig*)slottable[slot], FALSE);
			break;
		case CARD_CANBUS:
			// TODO: no support for CAN
			//status |= hw_can_post_install(slot, (hw_can*)slottable[slot], FALSE);
			break;
		case CARD_NONE:
		default:
			break;
		}

	}

}



const char* cardname[] = { "Empty",
						   "Mainboard",
						   "2TX Card",
						   "2DA4AD Card",
						   "Digital I/O Card",
						   "KiNet Card",
						   "4SG Card",			/* Signal Cube only */
						   "1SV2TX Card",
						   "4AD Card",
						   "1DA4AD Card",
						   "4LVDT Card",		/* Signal Cube only */
						   "4ACC Card",			/* Signal Cube only */
						   "5ADC Card",			/* Signal Cube only */
						   "Test Hardware",		/* Test hardware (KiNet used as monitor outputs) */
						   "2DIG Card",
						   "CAN bus Card"
};

/********************************************************************************
  FUNCTION NAME     : show_serialno
  FUNCTION DETAILS  : Show card serial number.
********************************************************************************/

void show_serialno(int skt, int slot)
{
	char serialno[17];
	int n;

	memcpy(serialno, slot_status[slot].serialno, 16);
	serialno[16] = '\0';

	for (n = 0; n < 16; n++) {
		if (!isprint(serialno[n])) serialno[n] = ' ';
	}

	netprintf(skt, "%16s", serialno);
}



const char* faultcode[] = { "...","..I",".S.",".SI","E..","E.I","ES.","ESI" };

/********************************************************************************
  FUNCTION NAME     : slotstatus
  FUNCTION DETAILS  : Show current status of all or selected expansion slot.
********************************************************************************/

void slotstatus(char* args[], int numargs, int skt)
{
	int slot;
	const char* name;
	hw_mboard* base;
	uint32_t state;
	uint32_t mask;
	int n;
	int c;

	if (numargs) {

		/* Show selected slot status */

		slot = atoi(args[0]);
		if ((slot < 0) || (slot > 3)) {
			netprintf(skt, "Illegal slot number %s\n\r", args[0]);
			return;
		}

		name = cardname[slot_status[slot].type];
		netprintf(skt, "Slot %d: Ident: %02d Type: %-16s Serial No: ", slot, slot_status[slot].type, name);
		show_serialno(skt, slot);
		netprintf(skt, "\n\r");
		netprintf(skt, "Txdrfault: %08X\n\r", slot_status[slot].txdrfault);
		base = (hw_mboard*)slottable[slot];

		netprintf(skt, "Input channels : %d\n\r", slot_status[slot].numinchan);
		netprintf(skt, "Fault detection available: %s\n\r", (base->u.rd.fault & 0x8000) ? "TRUE" : "FALSE");
		state = (uint32_t)(base->u.rd.fault & 0x7FFF & slot_status[slot].faultmask);
		mask = slot_status[slot].faultmask;
		if (n = slot_status[slot].numinchan) {
			for (c = 0; c < n; c++) {
				netprintf(skt, "  Chan: %2d  Mask: %s Fault: %s\n\r", c, faultcode[mask & 0x07], faultcode[state & 0x07]);
				state = state >> 3;
				mask = mask >> 3;
			}
		}

		netprintf(skt, "Output channels: %d\n\r", slot_status[slot].numoutchan);

		netprintf(skt, "Misc channels  : %d\n\r", slot_status[slot].nummiscchan);
		netprintf(skt, "Fault detection available: %s\n\r", (base->u.rd.fault & 0x8000) ? "TRUE" : "FALSE");
		state = (uint32_t)(base->u.rd.fault & 0x7FFF & slot_status[slot].faultmask);
		state = state >> 12; /* Align first SV channel */
		mask = slot_status[slot].faultmask;
		mask = mask >> 12; /* Align mask */
		if (n = slot_status[slot].nummiscchan) {
			for (c = 0; c < n; c++) {
				netprintf(skt, "  Chan: %2d  Mask: %s Fault: %s\n\r", c, faultcode[mask & 0x07], faultcode[state & 0x07]);
				state = state >> 3;
				mask = mask >> 3;
			}
		}
		netprintf(skt, "Digip mask     : %08X\n\r", slot_status[slot].digip_mask);
		netprintf(skt, "Digip invert   : %08X\n\r", slot_status[slot].digip_invert);

	}
	else {

		for (slot = 0; slot < 4; slot++) {
			name = cardname[slot_status[slot].type];
			netprintf(skt, "Slot %d: Ident: %02d Type: %-16s Serial No: ", slot, slot_status[slot].type, name);
			show_serialno(skt, slot);
			netprintf(skt, "\n\r");
		}

	}
}



/********************************************************************************
  FUNCTION NAME     : slot_safetyscan
  FUNCTION DETAILS  : Scan all occupied slots for any safety fault signals.

					  It is assumed that the transducer fault word is formatted
					  as shown below:

					  +---+-------+-------+-------+-------+-------+
					  | 0 | SV    | Txdr3 | Txdr2 | Txdr1 | Txdr0 |
					  +---+-------+-------+-------+-------+-------+

					  Each source consists of 3 bits as follows:

					  Bit 2	EXC	Drive/excitation fault
					  Bit 1	SNS	Excitation sense fault
					  Bit 0	IP	Input fault

					  Note that not all bits of the source will be present.

					  GP Transducer		EXC SNS IP
					  AD Transducer				IP
					  Servo-valve		EXC
					  Dig Transducer			IP

********************************************************************************/

uint32_t slot_safetyscan(void)
{
	int slot;
	hw_mboard* base;
	uint32_t combined = 0;

	if (!(snvbs->hyd_type & HYD_SIM_MASK)) {

		/* Not in simulation mode, so perform checks on transducer fault conditions */

		for (slot = 0; slot < 4; slot++) {
			if (slot_status[slot].type != CARD_NONE) {
				uint32_t state;
				uint32_t mask = slot_status[slot].faultmask;
				uint32_t fault;
				uint32_t change;
				base = (hw_mboard*)slottable[slot];
				state = (uint32_t)(base->u.rd.fault & 0x7FFF);

				fault = state & mask;
				combined |= (fault ? TRUE : 0);

				change = state ^ slot_status[slot].txdrfault;
				slot_status[slot].txdrfault = state;

				if (change) {

					eventlog_entry log;
					int count;
					int c;

					/* Build partial eventlog entry */

					log.type = LogTypeTxdrFaultStateChange;
					log.timestamp_hi = cnet_time_hi;
					log.timestamp_lo = cnet_time_lo;

					/* Test for input channel faults */

					if (count = slot_status[slot].numinchan) {
						c = CHANTYPE_INPUT | slot_status[slot].baseinchan;
						while (count) {
							if (change & 0x07) {
								if (change & fault & 0x07) {
									ctrl_txdrfault(c);	// Active fault bit asserted so inform control loop of fault and dump hydraulics
								}

								/* Only generate events for enabled faults to avoid flooding the event log
								   if fault state is indeterminate.
								*/

								if (change & mask & 0x07) {
									log.parameter1 = c;
									log.parameter2 = state & 0x07;
									eventlog_write(&log);
								}
							}
							change = change >> 3;
							state = state >> 3;
							fault = fault >> 3;
							mask = mask >> 3;
							count--;
							c++;
						}
					}

					/* The servo-valve channel fault inputs always occupy bit positions
					   12-14 in the fault register. If this slot does not have 4 input channels
					   then we must shift change, state and fault variables to compensate for
					   the "missing" channels.
					*/

					count = slot_status[slot].numinchan;
					if (count < 4) {
						change = change >> (3 * (4 - count));
						state = state >> (3 * (4 - count));
						fault = fault >> (3 * (4 - count));
						mask = mask >> (3 * (4 - count));
					}

					/* Test for servo-valve channel faults */

					if (count = slot_status[slot].nummiscchan) {
						c = CHANTYPE_MISC | slot_status[slot].basemiscchan;
						while (count) {
							if (change & 0x07) {
								if (change & fault & 0x07) {
									ctrl_txdrfault(c);	// Active fault bit asserted so inform control loop of fault and dump hydraulics
								}

								/* Only generate events for enabled faults to avoid flooding the event log
								   if fault state is indeterminate.
								*/

								if (change & mask & 0x07) {
									log.parameter1 = c;
									log.parameter2 = state & 0x07;
									eventlog_write(&log);
								}
							}
							change = change >> 3;
							state = state >> 3;
							fault = fault >> 3;
							mask = mask >> 3;
							count--;
							c++;
						}
					}

				}
			}
		}
	}

	/* Update global fault state */

	slot_txdrfault = combined;

	return(combined);
}

