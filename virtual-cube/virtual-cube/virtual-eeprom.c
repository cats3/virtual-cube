// virtual-eeprom.c

#include "virtual-eeprom.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define EEPROM_WRITE_CHECKSUM		0x01
#define EEPROM_READ_CHECKSUM		0x02

static int valid_address(struct virtual_eeprom* eeprom, int addr)
{
	if (!eeprom)
		return 0;
	return (eeprom->size >= addr);
}

static void write_eeprom_to_file(struct virtual_eeprom* eeprom)
{
	printf("eeprom write: %02x\n", eeprom->addr);
	rewind(eeprom->file);
	fwrite(eeprom->memory, eeprom->size, 1, eeprom->file);
	fflush(eeprom->file);
}

static void initialise_eeprom(struct virtual_eeprom* eeprom, const char* filename)
{
	eeprom->file = fopen(filename, "rb+");
	if (eeprom->file)
	{
		fread(eeprom->memory, eeprom->size, 1, eeprom->file);
	}
	else
	{
		eeprom->file = fopen(filename, "wb+");
		write_eeprom_to_file(eeprom);
	}
}

struct virtual_eeprom* virtual_eeprom_open(const char* filename, size_t size, uint8_t addr)
{
	struct virtual_eeprom* eeprom = malloc(sizeof(struct virtual_eeprom));
	uint8_t* memory = malloc(size);
	if (eeprom && memory)
	{
		memset(memory, 0xff, size);
		eeprom->size = size;
		eeprom->memory = memory;
		eeprom->addr = addr;
		initialise_eeprom(eeprom, filename);
	}
	else
	{
		eeprom = NULL;
	}
	return (eeprom);
}

void virtual_eeprom_close(struct virtual_eeprom* eeprom)
{
	if (eeprom)
	{
		fclose(eeprom->file);
		free(eeprom->memory);
		free(eeprom);
	}
}

void virtual_eeprom_write(struct virtual_eeprom* eeprom, int addr, uint8_t data)
{
	if (eeprom)
	{
		if (valid_address(eeprom, addr))
		{
			eeprom->memory[addr] = data;
			write_eeprom_to_file(eeprom);
		}
	}
}

void virtual_eeprom_write_block(struct virtual_eeprom* eeprom, int addr, uint8_t* data, size_t len, int flags)
{
	uint8_t checksum = 0;
	if (eeprom)
	{
		if (valid_address(eeprom, addr))
		{
			while (len)
			{
				checksum += *data;
				eeprom->memory[addr++] = *data++;
				if (addr >= eeprom->size)
					addr = 0;
				len--;
			}
			if (flags & EEPROM_WRITE_CHECKSUM)
			{
				eeprom->memory[addr] = checksum;
			}
			write_eeprom_to_file(eeprom);
		}
	}
}

void virtual_eeprom_read(struct virtual_eeprom* eeprom, int addr, uint8_t* data)
{
	if (eeprom)
	{
		if (valid_address(eeprom, addr))
			*data = eeprom->memory[addr];
	}
}

void virtual_eeprom_read_block(struct virtual_eeprom* eeprom, int addr, uint8_t* data, size_t len, int flags)
{
	uint8_t checksum = 0;
	uint8_t read;
	size_t tmp_len = len;
	int tmp_addr = addr;

	if (eeprom)
	{
		if (valid_address(eeprom, addr))
		{
			if (flags & EEPROM_READ_CHECKSUM)
			{
				while (tmp_len)
				{
					virtual_eeprom_read(eeprom, tmp_addr++, &read);
					if (tmp_addr >= eeprom->size)
						tmp_addr = 0;
					checksum += read;
					tmp_len--;
				}
				virtual_eeprom_read(eeprom, tmp_addr, &read);
				if (checksum != read)
					return;
			}
			while (len)
			{
				virtual_eeprom_read(eeprom, addr++, data++);
				if (addr >= eeprom->size)
					addr = 0;
				len--;
			}
		}
	}
}
