#include <stdlib.h>
#include <stdio.h>

#include <channel.h>


static void show_header(void)
{
	printf("\"sample\",\"filtered\",\"value\",\"ctrl\",\"status\",\"cycwindow\",\"prev_theta\","
		"\"peak\",\"trough\",\"pk\",\"cyc_peak\",\"cyc_trough\",\"timeout\","
		"\"max\",\"min\",\"prev_timer\"\n");
}

#define FLOATFMT	"%3.5f,"
#define HEXFMT		"0x%08x,"
#define UINTFMT		"%u,"

int main(int argc, char** argv)
{
	FILE* fp = fopen("chanproc.bin", "r");
	struct simple_chandef ch;
	int sample = 0;
	if (fp == NULL)
	{
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	show_header();
	while (!feof(fp))
	{
		size_t r = fread(&ch, sizeof(struct simple_chandef), 1, fp);
		if (r)
		{
			printf("%d,", sample++);
			printf(FLOATFMT FLOATFMT, ch.filtered, ch.value);
			printf(HEXFMT HEXFMT, ch.ctrl, ch.status);
			printf(FLOATFMT FLOATFMT, ch.cycwindow, ch.prev_theta);
			printf(FLOATFMT FLOATFMT FLOATFMT, ch.peak, ch.trough, ch.pk);
			printf(FLOATFMT FLOATFMT, ch.cyc_peak, ch.cyc_trough);
			printf(UINTFMT, ch.timeout);
			printf(FLOATFMT FLOATFMT, ch.max, ch.min);
			printf(UINTFMT, ch.prev_timer);
			printf("\n");
		}
	}
	return EXIT_SUCCESS;
}

