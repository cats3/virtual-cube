// mock_asmcode.c

#include <stdint.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>

int disable_int(void)
{
	return 0;
}

void restore_int(int x)
{
}

uint32_t _ftoi(float f)
{
	return (*(uint32_t*)&f);
}

float _itof(uint32_t i)
{
	return (*(float*)&i);
}

void netprintf(int s, char* fmt, ...)
{
	int len;
	char string[1024];
	va_list args;
	int err;
	unsigned int sent;
	int istate = disable_int();

	va_start(args, fmt);
	len = vsnprintf(string, 1024, fmt, args);
	va_end(args);
	restore_int(istate);
	if (len < 1024) {
		err = send(s, (unsigned char*)string, len, 0);
	}
}


void serial_write(char* fmt, ...)
{
	int len;
	char string[1024];
	va_list args;
	int err;
	unsigned int sent;
	int istate = disable_int();

	va_start(args, fmt);
	len = vfprintf(stdout, fmt, args);
	va_end(args);
	restore_int(istate);
}

uint8_t  atomic_set1(uint8_t* loc, uint8_t set)
{
	*loc = *loc | set;
}

uint8_t  atomic_clr1(uint8_t* loc, uint8_t clr)
{
	*loc = *loc & ~clr;
}

uint32_t atomic_tas(uint32_t* loc, uint32_t val)
{
	uint32_t prev = *loc;
	if (*loc == 0)
		*loc = val;
	return (prev);
}
