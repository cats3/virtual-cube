/********************************************************************************
 * MODULE NAME       : debug.c													*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : General debugging functions.								*
 ********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>

#include <stdint.h>
#include "porting.h"



extern int sysmem;

/* The following two lines are required to allow Events.h to compile */

#define SUPGEN
#define Parameter void

#include "defines.h"
#include "server.h"

#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "eeprom.h"
#include "hw_kinet.h"
#include "debug.h"
#include "nonvol.h"
#include "eventlog.h"
#include "netconfig.h"
#include "hydraulic.h"
#include "HardwareIndependent\Events.h"

void netprintf(int s, char* fmt, ...);
int strnicmp(const char* s1, const char* s2, size_t len);	/* Case insensitive string comparison */
void dsp_spi_send(uint8_t data);
uint16_t local_io(uint16_t set, uint16_t clr);
int dsp_i2c_eeprom_read(uint8_t i2c_addr, int addr, uint8_t* data);
int dsp_i2c_eeprom_write(uint8_t i2c_addr, int addr, uint8_t data);
int dsp_1w_readrom(uint32_t* lo, uint32_t* hi);
int dsp_1w_readeeprom(int addr, int len, uint8_t* data);
int dsp_1w_writeeeprom(int addr, uint8_t data);
void handler_status(connection* list, int* free, int* busy, int* total);
void resetuse(char* args[], int numargs, int skt);

uint8_t debug_log_data[256];
uint32_t debug_log_ptr;

#define DEBUG_MSG 32

typedef struct debug_data {
	char msg[DEBUG_MSG];
	uint32_t val1;
	uint32_t val2;
} debug_data;

#define DEBUG_BUFSIZE 128

debug_data debug_buffer[DEBUG_BUFSIZE];

uint32_t debug_head = 0;
uint32_t debug_tail = 0;

extern uint32_t proc_type;

#if 1

/********************************************************************************
  FUNCTION NAME   	: debug_flush
  FUNCTION DETAILS  : Flush debug information.
********************************************************************************/

void debug_flush(void)
{
	debug_head = 0;
	debug_tail = 0;
}

#if 0

/********************************************************************************
  FUNCTION NAME   	: debug
  FUNCTION DETAILS  : Log debug information.
********************************************************************************/

void debug(char* fmt, ...)
{
	int len;
	va_list args;
	//int istate = disable_int();
	int head;

	head = debug_head + 1;
	if (head >= DEBUG_BUFSIZE) {
		head = 0;
	}

	va_start(args, fmt);
	len = vsprintf(debug_buffer[head].msg, fmt, args);
	va_end(args);
	debug_buffer[head].msg[len++] = '\0'; // Guarantee a zero terminator

	debug_head = head;

	//restore_int(istate);
}

#endif

/********************************************************************************
  FUNCTION NAME   	: debug
  FUNCTION DETAILS  : Log debug information.
********************************************************************************/

void debug(char* msg, uint32_t val1, uint32_t val2)
{
	//int istate = disable_int();
	int head;

	head = debug_head + 1;
	if (head >= DEBUG_BUFSIZE) {
		head = 0;
	}

	strcpy(debug_buffer[head].msg, msg);
	debug_buffer[head].val1 = val1;
	debug_buffer[head].val2 = val2;

	debug_head = head;

	//restore_int(istate);
}

/********************************************************************************
  FUNCTION NAME   	: showdebug
  FUNCTION DETAILS  : Dump debug information.
********************************************************************************/

void showdebug(char* args[], int numargs, int skt)
{
	int head;
	//int istate = disable_int();

	head = debug_head;
	//restore_int(istate);

	netprintf(skt, "Debug dump -------------------\n\r");
	while (debug_tail != head) {
		debug_tail++;
		if (debug_tail >= DEBUG_BUFSIZE) {
			debug_tail = 0;
		}
		netprintf(skt, "%s %08X %08X\n\r", debug_buffer[debug_tail].msg,
			debug_buffer[debug_tail].val1,
			debug_buffer[debug_tail].val2);
	}
	netprintf(skt, "\n\r");
	netprintf(skt, "Debug end  -------------------\n\r");
}



#endif


/********************************************************************************
  FUNCTION NAME   	: ver
  FUNCTION DETAILS  : Display firmware version.
********************************************************************************/

void ver(char* args[], int numargs, int skt)
{
	netprintf(skt, "Version %d.%d.%04d\n\r", (FWVERSION >> 24), ((FWVERSION >> 16) & 0xFF),
		(FWVERSION & 0xFFFF));
}



/********************************************************************************
  FUNCTION NAME   	: info
  FUNCTION DETAILS  : Display hardware information.
********************************************************************************/

void info(char* args[], int numargs, int skt)
{
	uint8_t  hwver;
	uint32_t bootver;
	uint32_t hwflags;

	hwver = get_hw_version(&bootver, &hwflags);

	netprintf(skt, "Hardware version   : %d\n\r", hwver);
	netprintf(skt, "Hardware flags     : %08X\n\r", hwflags);
	netprintf(skt, "Boot loader version: %d\n\r", bootver);
#ifndef LOADER
	netprintf(skt, "Processor type     : C%d\n\r", proc_type);
#endif
}



/********************************************************************************
  FUNCTION NAME   	: setip
  FUNCTION DETAILS  : Set IP address.
********************************************************************************/

void setip(char* args[], int numargs, int skt)
{
	config_network config = { CONFIG_DHCP, 0,0,0,0, 255,255,255,0 };
	char* p;
	int n;
	int err = TRUE;

	switch (numargs) {
	case 1:
		if (strnicmp(args[0], "dhcp", 4) == 0) { config.source = CONFIG_DHCP; err = FALSE; }
		//    else if (strnicmp(args[0], "bootp", 5) == 0) { config.source = CONFIG_BOOTP; err = FALSE; }
		else netprintf(skt, "Invalid option\n\r");
		break;
	case 3:
		if (strnicmp(args[0], "manual", 6) == 0) {
			config.source = CONFIG_MANUAL;
			p = args[1];
			for (n = 4; n != 0; n--) {
				config.ipaddr[4 - n] = strtoul(p, &p, 10);
				if (*p != '.') break;
				p++;
			}
			if (n > 1) {
				netprintf(skt, "Invalid IP address\n\r");
				break;
			}
			p = args[2];
			for (n = 4; n != 0; n--) {
				config.subnetmask[4 - n] = strtoul(p, &p, 10);
				if (*p != '.') break;
				p++;
			}
			if (n > 1) {
				netprintf(skt, "Invalid subnetmask\n\r");
				break;
			}
		}
		err = FALSE;
		break;
	default:
		netprintf(skt, "Invalid option\n\r");
		break;
	}
	if (err == FALSE) {
		if (err = dsp_1w_writepage(PAGE_NETCONFIG, (uint8_t*)&config, sizeof(config), WR_CHKSUM)) {
			switch (err) {
			case NO_1WIRE:
				netprintf(skt, "No 1-wire device detected\n\r");
				break;
			case BAD_WRITE:
				netprintf(skt, "Error during 1-wire write operation\n\r");
				break;
			default:
				netprintf(skt, "Error\n\r");
				break;
			}
		}
	}
}



/********************************************************************************
  FUNCTION NAME   	: showip
  FUNCTION DETAILS  : Show current IP address.
********************************************************************************/

void showip(char* args[], int numargs, int skt)
{
	config_network config;
	uint32_t addr;
	uint32_t netmask;
	uint8_t* p;
	int err;

	netprintf(skt, "Current settings\n\r");
	read_interface(&addr, &netmask);
	p = (uint8_t*)&addr;
	netprintf(skt, "IP address %d.%d.%d.%d\n\r", p[0], p[1], p[2], p[3]);
	p = (uint8_t*)&netmask;
	netprintf(skt, "Subnetmask %d.%d.%d.%d\n\r", p[0], p[1], p[2], p[3]);

	netprintf(skt, "\n\r");
	netprintf(skt, "Current configuration\n\r");

	if (err = dsp_1w_readpage(PAGE_NETCONFIG, (uint8_t*)&config, sizeof(config), CHK_CHKSUM)) {
		switch (err) {
		case NO_1WIRE:
			netprintf(skt, "No 1-wire device detected\n\r");
			break;
		case CRC_ERROR:
			netprintf(skt, "Invalid IP address\n\r");
			break;
		default:
			netprintf(skt, "Error\n\r");
			break;
		}
		return;
	}

	if (config.source > CONFIG_NONE) {
		netprintf(skt, "Invalid IP configuration\n\r");
	}
	else {
		switch (config.source) {
		case CONFIG_MANUAL:
			netprintf(skt, "Manual IP address selection\n\r");
			netprintf(skt, "IP address %d.%d.%d.%d\n\r", config.ipaddr[0], config.ipaddr[1], config.ipaddr[2], config.ipaddr[3]);
			netprintf(skt, "Subnetmask %d.%d.%d.%d\n\r", config.subnetmask[0], config.subnetmask[1], config.subnetmask[2], config.subnetmask[3]);
			break;
		case CONFIG_DHCP:
			netprintf(skt, "IP address selection via DHCP\n\r");
			break;
		case CONFIG_BOOTP:
			netprintf(skt, "IP address selection via BOOTP\n\r");
			break;
		}
	}
}



/********************************************************************************
  FUNCTION NAME   	: setmac
  FUNCTION DETAILS  : Set MAC address.
********************************************************************************/

void setmac(char* args[], int numargs, int skt)
{
	uint8_t addr[6];
	char* p;
	int n;
	int err;

	p = args[0];

	for (n = 6; n != 0; n--) {
		addr[6 - n] = strtoul(p, &p, 16);
		if ((*p != '.') && (*p != ':')) break;
		p++;
	}

	if (n > 1) {
		netprintf(skt, "Invalid MAC address\n\r");
		return;
	}

	if (addr[0] & 0x01) {
		netprintf(skt, "Invalid MAC address (multicast address)\n\r");
		return;
	}

	if (err = dsp_1w_writepage(PAGE_MAC_ADDRESS, addr, 6, WR_CHKSUM)) {
		switch (err) {
		case NO_1WIRE:
			netprintf(skt, "No 1-wire device detected\n\r");
			break;
		case BAD_WRITE:
			netprintf(skt, "Error during 1-wire write operation\n\r");
			break;
		default:
			netprintf(skt, "Error\n\r");
			break;
		}
	}
}



/********************************************************************************
  FUNCTION NAME   	: showmac
  FUNCTION DETAILS  : Show current MAC address.
********************************************************************************/

void showmac(char* args[], int numargs, int skt)
{
	uint8_t addr[6];
	int err;

	if (err = dsp_1w_readpage(PAGE_MAC_ADDRESS, addr, 6, CHK_CHKSUM)) {
		switch (err) {
		case NO_1WIRE:
			netprintf(skt, "No 1-wire device detected\n\r");
			break;
		case CRC_ERROR:
			netprintf(skt, "Invalid MAC address\n\r");
			break;
		default:
			netprintf(skt, "Error\n\r");
			break;
		}
		return;
	}

	netprintf(skt, "Current MAC address %02X:%02X:%02X:%02X:%02X:%02X\n\r", addr[0], addr[1],
		addr[2], addr[3], addr[4], addr[5]);
}



#define NS_INADDRSZ 4

/********************************************************************************
  FUNCTION NAME   	: inet_pton4
  FUNCTION DETAILS  : Convert IP address from presentation form to network form.
********************************************************************************/

static int inet_pton4(const char* src, unsigned char* dst)
{
	static const char digits[] = "0123456789";
	int saw_digit, octets, ch;
	unsigned char tmp[NS_INADDRSZ], * tp;

	saw_digit = 0;
	octets = 0;
	*(tp = tmp) = 0;
	while ((ch = *src++) != '\0') {
		const char* pch;

		if ((pch = strchr(digits, ch)) != NULL) {
			unsigned int new = *tp * 10 + (pch - digits);

			if (new > 255)
				return (0);
			*tp = new;
			if (!saw_digit) {
				if (++octets > 4)
					return (0);
				saw_digit = 1;
			}
		}
		else if (ch == '.' && saw_digit) {
			if (octets == 4)
				return (0);
			*++tp = 0;
			saw_digit = 0;
		}
		else
			return (0);
	}
	if (octets < 4)
		return (0);
	memcpy(dst, tmp, NS_INADDRSZ);
	return (1);
}



/********************************************************************************
  FUNCTION NAME   	: inet_ntop4
  FUNCTION DETAILS  : Convert IP address network form to presentation form.
********************************************************************************/

static const char* inet_ntop4(const unsigned char* src, char* dst, size_t size)
{
	static const char* fmt = "%u.%u.%u.%u";
	char tmp[sizeof "255.255.255.255"];
	int len;

	len = snprintf(tmp, sizeof tmp, fmt, src[0], src[1], src[2], src[3]);
	if (len >= size) {
		//	errno = ENOSPC;
		return (NULL);
	}
	memcpy(dst, tmp, len + 1);

	return (dst);
}



#define SA struct sockaddr

/********************************************************************************
  FUNCTION NAME   	: get_rtaddrs
  FUNCTION DETAILS  : Build array of pointers to socket address structures
					  in routing message.
********************************************************************************/

void get_rtaddrs(int addrs, SA* sa, SA** rti_info)
{
	int i;

	for (i = 0; i < RTAX_MAX; i++) {
		if (addrs & (1 << i)) {
			rti_info[i] = sa;
			sa = (struct sockaddr*)(((uint8_t*)sa) + sa->sa_len);
		}
		else
			rti_info[i] = NULL;
	}
}



/********************************************************************************
  FUNCTION NAME   	: sock_masktop
  FUNCTION DETAILS  : Convert a mask value to its presentation format.
********************************************************************************/

char* sock_masktop(SA* sa, int salen)
{
	static char str[64];
	unsigned char* ptr = (unsigned char*)&sa->sa_data[2];

	memset(str, ' ', sizeof(str));

	if (sa->sa_len == 0)
		return("0.0.0.0        ");
	else if (sa->sa_len == 5)
		snprintf(str, sizeof(str), "%d.0.0.0", *ptr);
	else if (sa->sa_len == 6)
		snprintf(str, sizeof(str), "%d.%d.0.0", *ptr, *(ptr + 1));
	else if (sa->sa_len == 7)
		snprintf(str, sizeof(str), "%d.%d.%d.0", *ptr, *(ptr + 1), *(ptr + 2));
	else if (sa->sa_len == 8)
		snprintf(str, sizeof(str), "%d.%d.%d.%d", *ptr, *(ptr + 1), *(ptr + 2), *(ptr + 3));
	else
		snprintf(str, sizeof(str), "[Len:%d,Fam:%d]", sa->sa_len, sa->sa_family);
	str[15] = '\0';

	return(str);
}



/********************************************************************************
  FUNCTION NAME   	: sock_ntop_host
  FUNCTION DETAILS  : Convert a socket address to its presentation format.
********************************************************************************/

char* sock_ntop_host(const struct sockaddr* sa, int salen)
{
	static char str[128];

	memset(str, ' ', sizeof(str));

	switch (sa->sa_family) {
	case AF_INET: {
		struct sockaddr_in* sin = (struct sockaddr_in*)sa;

		inet_ntop4((const unsigned char*)&sin->sin_addr, str, sizeof(str));
		break;
	}
	case AF_LINK: {
		struct sockaddr_dl* sin = (struct sockaddr_dl*)sa;

		snprintf(str, sizeof(str), "link#%d", sin->sdl_index);
		break;
	}
	default:
		snprintf(str, sizeof(str), "[AF:%d,Len:%d]", sa->sa_family, salen);
		break;
	}
	str[15] = '\0';
	return str;
}





/********************************************************************************
  FUNCTION NAME   	: show_route
  FUNCTION DETAILS  : Show routing table entry.
********************************************************************************/

void show_route(int skt, struct sockaddr** rti_info)
{
	struct sockaddr* sa;

	if ((sa = rti_info[RTAX_DST]) != NULL) {
		netprintf(skt, "dest: %-15s ", sock_ntop_host(sa, sa->sa_len));
	}

	if ((sa = rti_info[RTAX_GATEWAY]) != NULL) {
		netprintf(skt, "gateway: %-15s ", sock_ntop_host(sa, sa->sa_len));
	}

	if ((sa = rti_info[RTAX_NETMASK]) != NULL) {
		netprintf(skt, "netmask: %-15s ", sock_masktop(sa, sa->sa_len));
	}

	if ((sa = rti_info[RTAX_GENMASK]) != NULL) {
		netprintf(skt, "genmask: %-15s ", sock_masktop(sa, sa->sa_len));
	}

	netprintf(skt, "\n\r");
}



#define BUFLEN (sizeof(struct rt_msghdr) + 512)
#define SEQ 9999

/********************************************************************************
  FUNCTION NAME   	: route
  FUNCTION DETAILS  : Show routing table.
********************************************************************************/

void route(char* args[], int numargs, int skt)
{
	if (args[0][0] == 'a') {

		/* Dump routing table */

		int mib[6];
		size_t needed;
		char* buf, * next, * lim;
		struct rt_msghdr* rtm;
		struct sockaddr* sa, * rti_info[RTAX_MAX];

		mib[0] = CTL_NET;
		mib[1] = AF_ROUTE; 	/* entire routing table or a subset of it */
		mib[2] = 0; 		 	/* protocol number - always 0 */
		mib[3] = AF_INET;  	/* address family - 0 for all addres families */
		mib[4] = NET_RT_DUMP;
		mib[5] = 0;

		/* Determine the buffer side needed to get the full routing table */
		if (sysctl(mib, 6, NULL, &needed, NULL, 0, NULL) < 0) {
			netprintf(skt, "route - error sysctl\n\r");
			return;
		}

		if (!(buf = malloc(needed))) {
			netprintf(skt, "route - malloc\n\r");
			return;
		}

		/* Read the routing table into buf */
		if (sysctl(mib, 6, buf, &needed, NULL, 0, NULL) < 0) {
			netprintf(skt, "route - error sysctl\n\r");
			return;
		}

		lim = buf + needed;

		for (next = buf; next < lim; next += rtm->rtm_msglen) {
			rtm = (struct rt_msghdr*)next;
			sa = (struct sockaddr*)(rtm + 1);

			get_rtaddrs(rtm->rtm_addrs, sa, rti_info);

			show_route(skt, rti_info);
		}
	}
	else {

		/* Dump a defined route entry */

		int s;
		char* buf;
		struct rt_msghdr* rtm;
		struct sockaddr* sa, * rti_info[RTAX_MAX];
		struct sockaddr_in* sin;
		int pid = 1;

		s = socket(AF_ROUTE, SOCK_RAW, 0, NULL);

		buf = calloc(1, BUFLEN);

		rtm = (struct rt_msghdr*)buf;

		rtm->rtm_msglen = sizeof(struct rt_msghdr) + sizeof(struct sockaddr_in);
		rtm->rtm_version = RTM_VERSION;
		rtm->rtm_type = RTM_GET;
		rtm->rtm_addrs = RTA_DST;
		rtm->rtm_pid = pid;
		rtm->rtm_seq = SEQ;

		sin = (struct sockaddr_in*)(rtm + 1);
		sin->sin_len = sizeof(struct sockaddr_in);
		sin->sin_family = AF_INET;
		inet_pton4(args[0], (unsigned char*)&sin->sin_addr);

		socketwrite(s, rtm, rtm->rtm_msglen, NULL);

		do {
			socketread(s, rtm, BUFLEN, NULL);
		} while (rtm->rtm_type != RTM_GET || rtm->rtm_seq != SEQ || rtm->rtm_pid != pid);

		rtm = (struct rt_msghdr*)buf;
		sa = (struct sockaddr*)(rtm + 1);
		get_rtaddrs(rtm->rtm_addrs, sa, rti_info);

		show_route(skt, rti_info);

		socketclose(s, NULL);
	}
}



/********************************************************************************
  FUNCTION NAME   	: setser
  FUNCTION DETAILS  : Set serial number.
********************************************************************************/

void setser(char* args[], int numargs, int skt)
{
	uint8_t serno[8];
	char* p;
	int n;
	int err;

	memset(serno, 0, sizeof(serno));

	p = args[0];

	for (n = 8; n != 0; n--) {
		serno[8 - n] = *p;
		if (*p == '\0') break;
		p++;
	}

	if (err = dsp_1w_writepage(PAGE_CTRLSN, serno, sizeof(serno), WR_CHKSUM)) {
		switch (err) {
		case NO_1WIRE:
			netprintf(skt, "No 1-wire device detected\n\r");
			break;
		case BAD_WRITE:
			netprintf(skt, "Error during 1-wire write operation\n\r");
			break;
		default:
			netprintf(skt, "Error\n\r");
			break;
		}
	}
}



/********************************************************************************
  FUNCTION NAME   	: showser
  FUNCTION DETAILS  : Show controller serial number.
********************************************************************************/

void showser(char* args[], int numargs, int skt)
{
	uint8_t serno[9];
	int err;

	if (err = dsp_1w_readpage(PAGE_CTRLSN, serno, 8, CHK_CHKSUM)) {
		switch (err) {
		case NO_1WIRE:
			netprintf(skt, "No 1-wire device detected\n\r");
			break;
		case CRC_ERROR:
			netprintf(skt, "Invalid serial number\n\r");
			break;
		default:
			netprintf(skt, "Error\n\r");
			break;
		}
		return;
	}

	serno[8] = '\0';
	netprintf(skt, "Serial No. %s\n\r", serno);
}



/********************************************************************************
  FUNCTION NAME   	: eewr
  FUNCTION DETAILS  : Write a byte to the mainboard or expansion bus EEPROM.
********************************************************************************/

void eewr(char* args[], int numargs, int skt)
{
	int device;
	int addr;
	int i2c_addr;
	uint8_t data;
	int err;

	device = (int)strtoul(args[0], NULL, 0);	/* Get device */
	addr = (int)strtoul(args[1], NULL, 0);		/* Get address within EEPROM */
	data = (int)strtoul(args[2], NULL, 0);		/* Get data to write */

	i2c_addr = device;
#if (defined(CONTROLCUBE) || defined(AICUBE))
	i2c_addr = (device == 7) ? 7 : ((device > 3) ? 0 : device);
#endif
	i2c_addr = 0xA0 + (i2c_addr << 1);

	err = dsp_i2c_eeprom_write(i2c_addr, addr, data);
	if (err) {
		netprintf(skt, "Error writing device %d, address %04X\n\r", device, addr);
	}
}



#define BYTESPERLINE 8

/********************************************************************************
  FUNCTION NAME   	: eerd
  FUNCTION DETAILS  : Read a number of bytes from the mainboard or expansion
					  bus EEPROM.
********************************************************************************/

void eerd(char* args[], int numargs, int skt)
{
	int device;
	int addr;
	int i2c_addr;
	uint8_t data;
	char charlist[16];
	int len = 1;
	int count = 0;
	int n;
	int err;

	device = (int)strtoul(args[0], NULL, 0);	/* Get device */
	addr = (int)strtoul(args[1], NULL, 0);		/* Get address within EEPROM */
	if (numargs == 3) {
		len = (int)strtoul(args[2], NULL, 0);
	}

	i2c_addr = device;
#if (defined(CONTROLCUBE) || defined(AICUBE))
	i2c_addr = (device == 7) ? 7 : ((device > 3) ? 0 : device);
#endif
	i2c_addr = 0xA0 + (i2c_addr << 1);

	netprintf(skt, "%04X ", addr);

	while (len) {
		err = dsp_i2c_eeprom_read(i2c_addr, addr, &data);
		charlist[count] = (char)data;
		if (err) {
			netprintf(skt, "Error reading device %d, address %04X", device, addr);
			goto end_eerd;
		}
		netprintf(skt, "%02X ", data);
		addr++;
		len--;
		count++;
		if (count == BYTESPERLINE) {
			netprintf(skt, "    ");
			for (n = 0; n < count; n++) {
				netprintf(skt, "%c ", isprint(charlist[n]) ? charlist[n] : '.');
			}
			if (len) {
				netprintf(skt, "\n\r%04X ", addr);
			}
			count = 0;
		}
	}

	if (count) {
		netprintf(skt, "    ");
		for (n = 0; n < count; n++) {
			netprintf(skt, "%c ", isprint(charlist[n]) ? charlist[n] : '.');
		}
	}

end_eerd:

	netprintf(skt, "\n\r");
}



/********************************************************************************
  FUNCTION NAME   	: cnetstat
  FUNCTION DETAILS  : Read status of CNet input/output connections.
********************************************************************************/

void cnetstat(char* args[], int numargs, int skt)
{
	uint32_t in;
	uint32_t out;
	uint32_t lock;
	uint32_t integrity;
	uint32_t fault;
	uint32_t chksum;
	uint32_t wdog;
	uint32_t ts_lo;
	uint32_t ts_hi;
	uint32_t mode;
	uint32_t position;
	uint32_t ringsize;
	uint32_t master;
	uint32_t enable;
	uint32_t id;
	char modestr[32];

	cnet_read_mode(&enable, &master, &id);
	cnet_rdstat(NULL, &in, &out, &lock, &integrity, &fault, &chksum, &wdog, &ts_lo, &ts_hi, &mode, &position, &ringsize);

	switch (mode) {
	case CNET_MODE_MASTER:
		strcpy(modestr, "master");
		break;
	case CNET_MODE_SLAVE:
		strcpy(modestr, "slave");
		break;
	case CNET_MODE_STANDALONE:
		strcpy(modestr, "standalone");
		break;
	}

	netprintf(skt, "CNet\n\r");
	netprintf(skt, "Status          : %s\n\r", (enable) ? "Enabled" : "Disabled");
	netprintf(skt, "ID              : %d\n\r", id);
	netprintf(skt, "In              : %s\n\r", (in) ? "connected" : "not connected");
	netprintf(skt, "Out             : %s\n\r", (out) ? "connected" : "not connected");
	netprintf(skt, "Mode            : %s\n\r", modestr);
	netprintf(skt, "Configuration   : %s\n\r", (cnet_configured) ? "complete" : "busy");
	netprintf(skt, "Position        : %d\n\r", position + 1);
	netprintf(skt, "Ring size       : %d\n\r", ringsize);
	netprintf(skt, "Lock            : %s\n\r", (lock) ? "locked" : "not locked");
	netprintf(skt, "Integrity       : %s\n\r", (integrity) ? "good" : "bad");
	netprintf(skt, "Faults          : %d\n\r", fault);
	netprintf(skt, "Chksum Fail     : %d\n\r", chksum);
	netprintf(skt, "Watchdog State  : %s\n\r", (wdog) ? "good" : "bad");
	netprintf(skt, "Watchdog Errors : %d\n\r", cnet_wdog_count);
	netprintf(skt, "Timestamp       : %08X%08X\n\r", ts_hi, ts_lo);
	netprintf(skt, "Retries         : %d\n\r", cnet_retry_count);
	netprintf(skt, "Collisions      : %d\n\r", cnet_collision_count);
	netprintf(skt, "Timeouts        : %d\n\r", cnet_timeout_count);
	netprintf(skt, "Interrupts      : %d\n\r", cnet_interrupt_ctr);
}



/********************************************************************************
  FUNCTION NAME   	: memread
  FUNCTION DETAILS  : Read 32-bit memory word in DSPA or DSPB.

  Arguments as follows:

  Arg 0 :	Address
  Arg 1 :   Flags (optional):  	"A","B" to select DSP (defaults to DSP A).
								"F" to display in float form
********************************************************************************/

void memread(char* args[], int numargs, int skt)
{
	uint32_t* addr;
	int dsp = 0;
	int format = 0;
	int n;
	uint32_t data;
	float* fptr;

	addr = (uint32_t*)((uint32_t)strtoul(args[0], NULL, 0));	/* Get address to read */
	if (numargs == 2) {
		for (n = 0; n < strlen(args[1]); n++) {
			switch (args[1][n]) {
			case 'A':
			case 'a':
				dsp = 0;
				break;
			case 'B':
			case 'b':
				dsp = 1;
				break;
			case 'F':
			case 'f':
				format = 1;
			}
		}
	}

	if (dsp) {
		data = hpi_read4(addr);
	}
	else {
		data = *addr;
	}

	if (format) {
		fptr = (float*)&data;
		netprintf(skt, "%08X(%c) : %f\n\r", addr, dsp ? 'B' : 'A', *fptr);
	}
	else
		netprintf(skt, "%08X(%c) : %08X\n\r", addr, dsp ? 'B' : 'A', data);
}



/********************************************************************************
  FUNCTION NAME   	: memwrite
  FUNCTION DETAILS  : Write 32-bit memory word in DSPA or DSPB.

  Arguments as follows:

  Arg 0 :	Address
  Arg 1 :   Data
  Arg 2 :   Flags (optional):  	"A","B" to select DSP (defaults to DSP A).
								"F" to enter in float form
********************************************************************************/

void memwrite(char* args[], int numargs, int skt)
{
	uint32_t* addr;
	int dsp = 0;
	int n;
	int format = 0;
	uint32_t data;

	addr = (uint32_t*)((uint32_t)strtoul(args[0], NULL, 0));	/* Get address to write */
	if (numargs == 3) {
		for (n = 0; n < strlen(args[2]); n++) {
			switch (args[2][n]) {
			case 'A':
			case 'a':
				dsp = 0;
				break;
			case 'B':
			case 'b':
				dsp = 1;
				break;
			case 'F':
			case 'f':
				format = 1;
				break;
			}
		}
	}

	if (format) {
		*((float*)&data) = atof(args[1]);	/* Get data in floating form */
	}
	else {
		data = strtoul(args[1], NULL, 0);		/* Get data in integer form */
	}

	if (dsp) {
		hpi_write4(addr, data);
	}
	else {
		*addr = data;
	}
}



/********************************************************************************
  FUNCTION NAME   	: memdump
  FUNCTION DETAILS  : Dump an area of memory from DSPA or DSPB.

  Arguments as follows:

  Arg 0 :	Start address
  Arg 1 :	End address
  Arg 2 :   Flags (optional):  	"A","B" to select DSP (defaults to DSP A).
								"F" to display in float form
********************************************************************************/

void memdump(char* args[], int numargs, int skt)
{
	uint32_t* start;
	uint32_t* end;
	uint32_t* addr;
	int dsp = 0;
	int format = 0;
	int words;
	int first;
	int n;
	uint32_t data;
	float* fptr;

	start = (uint32_t*)((uint32_t)strtoul(args[0], NULL, 0));	/* Get start address */

	end = (uint32_t*)((uint32_t)strtoul(args[1], NULL, 0));		/* Get end address */

	if (numargs == 3) {
		for (n = 0; n < strlen(args[2]); n++) {
			switch (args[2][n]) {
			case 'A':
			case 'a':
				dsp = 0;
				break;
			case 'B':
			case 'b':
				dsp = 1;
				break;
			case 'F':
			case 'f':
				format = 1;
			}
		}
	}

	words = 0;
	first = TRUE;
	for (addr = start; addr <= end; addr++) {
		if ((words & 0x03) == 0) netprintf(skt, "%08X(%c) : ", addr, dsp ? 'B' : 'A');
		if (!first) netprintf(skt, ", ");
		first = FALSE;
		if (dsp) {
			data = hpi_read4(addr);
		}
		else {
			data = *addr;
		}

		if (format) {
			fptr = (float*)&data;
			netprintf(skt, "%12.5g", *fptr);
		}
		else
			netprintf(skt, "%08X", data);

		if ((words & 0x03) == 0x03) {
			netprintf(skt, "\n\r");
			first = TRUE;
		}

		words++;
	}

	if (words & 0x03) netprintf(skt, "\n\r");
}



/********************************************************************************
  FUNCTION NAME   	: memstat
  FUNCTION DETAILS  : Show dynamic memory allocation statistics.
********************************************************************************/

void memstat(char* args[], int numargs, int skt)
{
	MEM_Stat statbuf;

	if (MEM_stat(sysmem, &statbuf)) {
		netprintf(skt, "Segment size: %u\n\r", statbuf.size);
		netprintf(skt, "Used        : %u\n\r", statbuf.used);
		netprintf(skt, "Largest free: %u\n\r", statbuf.length);
		return;
	}
	netprintf(skt, "Error. Unable to read statistics\n\r");
}



/********************************************************************************
  FUNCTION NAME   	: byteread
  FUNCTION DETAILS  : Read 8-bit memory byte in DSPA.

  Arguments as follows:

  Arg 0 :	Address
********************************************************************************/

void byteread(char* args[], int numargs, int skt)
{
	uint8_t* addr;
	uint8_t data;

	addr = (uint8_t*)((uint32_t)strtoul(args[0], NULL, 0));	/* Get address to read */
	data = *addr;

	netprintf(skt, "%08X : %02X\n\r", addr, data);
}



/********************************************************************************
  FUNCTION NAME   	: bytewrite
  FUNCTION DETAILS  : Write 8-bit memory byte in DSPA.

  Arguments as follows:

  Arg 0 :	Address
  Arg 1 :   Data
********************************************************************************/

void bytewrite(char* args[], int numargs, int skt)
{
	uint8_t* addr;
	uint8_t data;

	addr = (uint8_t*)((uint32_t)strtoul(args[0], NULL, 0));	/* Get address to write */
	data = strtoul(args[1], NULL, 0);		/* Get data in integer form */
	*addr = data;
}



/********************************************************************************
  FUNCTION NAME   	: flashwr
  FUNCTION DETAILS  : Write a byte to the flash memory.
********************************************************************************/

void flashwr(char* args[], int numargs, int skt)
{
	int addr;
	uint8_t data;

	addr = (int)strtoul(args[0], NULL, 0);		/* Get address within flash memory */
	data = (int)strtoul(args[1], NULL, 0);		/* Get data to write */

	if (flash_program(addr, data) != PROG_COMPLETE) {
		netprintf(skt, "Error writing flash offset %08X\n\r", addr);
		return;
	}
	netprintf(skt, "Flash write complete\n\r");
}



/********************************************************************************
  FUNCTION NAME   	: flasherase
  FUNCTION DETAILS  : Erase a block within the flash memory.
********************************************************************************/

void flasherase(char* args[], int numargs, int skt)
{
	int block;
	uint32_t status;

	block = (int)strtoul(args[0], NULL, 0);		/* Get block number */

	status = flash_erase_block(block, ERASE_NO_WAIT);
	switch (status) {
	case ERASE_COMPLETE:
		netprintf(skt, "Erase complete block %d\n\r", block);
		break;
	case ERASE_ERROR:
		netprintf(skt, "Erase error block %d\n\r", block);
		break;
	case ERASE_ILLEGAL_BLOCK:
		netprintf(skt, "Illegal block %d\n\r", block);
		break;
	default:
		netprintf(skt, "Unknown error erasing flash block %d\n\r", block);
		break;
	}
}



const char usetable[] = { '.', 'S', 'U', 'X' };

/********************************************************************************
  FUNCTION NAME   	: cnetmap
  FUNCTION DETAILS  : Display CNet timeslot map.

  Arguments as follows:

  Arg 0 :   Optional CNet address. If not present, then defaults to local
			address.
********************************************************************************/

void cnetmap(char* args[], int numargs, int skt)
{
	uint32_t cnet_position;
	uint32_t cnet_ringsize;
	uint32_t mask;
	uint32_t slot;
	uint32_t n;
	char use;

	cnet_get_info(&cnet_ringsize, &cnet_position);

	if (numargs == 1) {
		cnet_position = (uint32_t)strtoul(args[0], NULL, 0);	/* Get CNet address */
	}

	if (cnet_position > cnet_ringsize) {
		netprintf(skt, "Illegal CNet address\n\r");
		return;
	}

	mask = 1 << cnet_position;

	netprintf(skt, "CNet address  %d\n\r", cnet_position);
	netprintf(skt, "CNet ringsize %d\n\r", cnet_ringsize);
	netprintf(skt, "\n\rCNet timeslot map\n\r");

	for (slot = 0; slot < 400; slot += 16) {
		netprintf(skt, "%3d ", slot);
		for (n = 0; n < 16; n++) {
			use = usetable[((cnet_sys_timeslot[slot + n] & mask) ? 0x01 : 0) | ((cnet_usr_timeslot[slot + n] & mask) ? 0x02 : 0)];
			netprintf(skt, "%c ", use);
		}
		netprintf(skt, "\n\r");
	}

}



/* Define CNet transmit control types */

#define CNET_CTRL_NOP			0x0000				/* Do nothing									*/
#define CNET_CTRL_PASS			0x0100				/* Pass through data packet						*/
#define CNET_CTRL_TX			0x0200				/* Transmit standard data packet 				*/
#define CNET_CTRL_ORTX			0x0300				/* Transmit wired-or data packet 				*/
#define CNET_CTRL_COMMCTRLTX	0x0500				/* Transmit comms ctrl packet 					*/
#define CNET_CTRL_COMMCTRLORTX	0x0600				/* Transmit wired-or comms ctrl packet 			*/
#define CNET_CTRL_COMMDATATX	0x0D00				/* Transmit comms data packet 					*/
#define CNET_CTRL_COMMDATAORTX	0x0E00				/* Transmit wired-or comms data packet 			*/
#define CNET_CTRL_INTEGRITY		0x0F00				/* Transmit integrity packet					*/

/********************************************************************************
  FUNCTION NAME   	: cnetctrl
  FUNCTION DETAILS  : Display CNet timeslot control memory.

  Arguments as follows:

  Arg 0 :   Start timeslot number.
  Arg 1 :   End timeslot number
********************************************************************************/

void cnetctrl(char* args[], int numargs, int skt)
{
	uint32_t start = 0;
	uint32_t end = 399;
	uint32_t slot;
	uint32_t use;
	uint32_t dataslot;

	if (numargs == 1) {
		start = (uint32_t)strtoul(args[0], NULL, 0);	/* Get start timeslot number */
	}
	if (numargs == 2) {
		end = (uint32_t)strtoul(args[1], NULL, 0);	/* Get end timeslot number */
	}

	netprintf(skt, "Slot   Type      Dataslot\n\r");
	netprintf(skt, "====   ====      ========\n\r");
	netprintf(skt, "\n\r");

	for (slot = start; slot <= end; slot++) {
		netprintf(skt, "%3d    ", slot);
		use = cnet_mem_ctrl[slot] & 0xFF00;
		dataslot = cnet_mem_ctrl[slot] & 0xFF;
		switch (use) {
		case CNET_CTRL_NOP:
			netprintf(skt, "NOP       ", use);
			break;
		case CNET_CTRL_PASS:
			netprintf(skt, "PASS      ", use);
			break;
		case CNET_CTRL_TX:
			netprintf(skt, "TX        ", use);
			break;
		case CNET_CTRL_ORTX:
			netprintf(skt, "ORTX      ", use);
			break;
		case CNET_CTRL_COMMCTRLTX:
			netprintf(skt, "CCTX      ", use);
			break;
		case CNET_CTRL_COMMCTRLORTX:
			netprintf(skt, "CCORTX    ", use);
			break;
		case CNET_CTRL_COMMDATATX:
			netprintf(skt, "CDTX      ", use);
			break;
		case CNET_CTRL_COMMDATAORTX:
			netprintf(skt, "CDORTX    ", use);
			break;
		case CNET_CTRL_INTEGRITY:
			netprintf(skt, "INTEGRITY ", use);
			break;
		default:
			netprintf(skt, "[%02X]", use);
			break;
		}
		netprintf(skt, "%02X\n\r", dataslot);
	}

}



#ifndef LOADER

/********************************************************************************
  FUNCTION NAME   	: writehyd
  FUNCTION DETAILS  : Write value to hydraulic control output via SPI.
********************************************************************************/

void writehyd(char* args[], int numargs, int skt)
{
#if CONTROLCUBE

	uint8_t data;

	data = (uint8_t)strtoul(args[0], NULL, 0);	/* Get value to write */

	dsp_spi_send(data);

#endif
}



/********************************************************************************
  FUNCTION NAME   	: readio
  FUNCTION DETAILS  : Read current state of hyd/gp I/O.
********************************************************************************/

void readio(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	uint16_t data;
	int n;

	data = local_io(0, 0);

	for (n = 0; n < 16; n++) {
		netprintf(skt, "%c", (data & 0x8000) ? '1' : '0');
		data = data << 1;
	}

	netprintf(skt, "\n\r");

#else

	netprintf(skt, "Not supported\n\r");

#endif
}



/********************************************************************************
  FUNCTION NAME   	: setio
  FUNCTION DETAILS  : Set the requested outputs.
********************************************************************************/

void setio(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	uint16_t data;

	data = atoi(args[0]);

	local_io(1 << data, 0);

#endif
}



/********************************************************************************
  FUNCTION NAME   	: clrio
  FUNCTION DETAILS  : Clear the requested outputs.
********************************************************************************/

void clrio(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	uint16_t data;

	data = atoi(args[0]);

	local_io(0, 1 << data);

#endif
}



/********************************************************************************
  FUNCTION NAME   	: eetest
  FUNCTION DETAILS  : Perform a test of the mainboard EEPROM.
********************************************************************************/

void eetest(char* args[], int numargs, int skt)
{
	uint8_t* mem;
	uint8_t* p;
	int n;
	uint8_t data;

	mem = malloc(0x8000);

	/* Build test data area */

	p = mem;
	for (n = 0; n < 0x4000; n++) {
		*p++ = (n >> 8) & 0xFF;
		*p++ = n & 0xFF;
	}

	/* Write block to EEPROM */

	netprintf(skt, "Writing EEPROM data\n\r");
	dsp_i2c_eeprom_writeblock(0xA0, 0, mem, 0x8000, 0);

	/* Read data to check validity */

	netprintf(skt, "Reading EEPROM data\n\r");
	p = mem;
	for (n = 0; n < 0x8000; n++) {
		if (dsp_i2c_eeprom_read(0xA0, n, &data)) {
			netprintf(skt, "\n\rError reading %04X\n\r", n);
		}
		else {
			if (data != *p) {
				netprintf(skt, "\n\rError at offset %04X - read %02X, expected %02X\n\r", n, data, *p);
				break;
			}
		}
		p++;
		if ((n & 0xFF) == 0) {
			netprintf(skt, ".");
		}
	}

	free(mem);

	netprintf(skt, "\n\rTest completed\n\r");
}



/********************************************************************************
  FUNCTION NAME   	: rdid
  FUNCTION DETAILS  : Read ID from 1-wire device
********************************************************************************/

void rdid(char* args[], int numargs, int skt)
{
	uint32_t datalo;
	uint32_t datahi;
	int err;

	netprintf(skt, "Reading 1-wire ROM data\n\r");

	if ((err = dsp_1w_readrom(&datalo, &datahi)) == NO_1WIRE) {
		netprintf(skt, "No 1-wire device detected\n\r");
		return;
	}

	netprintf(skt, "ROM data ");

	netprintf(skt, "%02X ", (datahi >> 24) & 0xFF);
	netprintf(skt, "%02X ", (datahi >> 16) & 0xFF);
	netprintf(skt, "%02X ", (datahi >> 8) & 0xFF);
	netprintf(skt, "%02X ", datahi & 0xFF);

	netprintf(skt, "%02X ", (datalo >> 24) & 0xFF);
	netprintf(skt, "%02X ", (datalo >> 16) & 0xFF);
	netprintf(skt, "%02X ", (datalo >> 8) & 0xFF);
	netprintf(skt, "%02X ", datalo & 0xFF);

	netprintf(skt, "\n\r");

	if (err == CRC_ERROR) {
		netprintf(skt, "CRC error in ROM data\n\r");
	}
}



/********************************************************************************
  FUNCTION NAME   	: rd1w
  FUNCTION DETAILS  : Read a number of bytes from the mainboard 1-wire EEPROM.
********************************************************************************/

void rd1w(char* args[], int numargs, int skt)
{
	int addr;
	int len = 1;
	int count = 0;
	uint8_t data[512];

	addr = (int)strtoul(args[0], NULL, 0);		/* Get address within EEPROM */
	if (numargs == 2) {
		len = (int)strtoul(args[1], NULL, 0);
	}

	if (dsp_1w_readeeprom(addr, len, data) == NO_1WIRE) {
		netprintf(skt, "No 1-wire device detected\n\r");
		return;
	}

	netprintf(skt, "%04X ", addr);

	while (len) {
		netprintf(skt, "%02X ", data[count]);
		addr++;
		len--;
		count++;
		if ((len) && ((count & 0xF) == 0)) {
			netprintf(skt, "\n\r%04X ", addr);
		}
	}

	netprintf(skt, "\n\r");
}



/********************************************************************************
  FUNCTION NAME   	: wr1w
  FUNCTION DETAILS  : Write a byte to the mainboard 1-wire EEPROM.
********************************************************************************/

void wr1w(char* args[], int numargs, int skt)
{
	int addr;
	uint8_t data;

	addr = (int)strtoul(args[0], NULL, 0);		/* Get address within EEPROM */
	data = (int)strtoul(args[1], NULL, 0);		/* Get data to write */

	if (dsp_1w_writeeeprom(addr, data) == NO_1WIRE) {
		netprintf(skt, "No 1-wire device detected\n\r");
		return;
	}
}



/********************************************************************************
  FUNCTION NAME   	: tedsid
  FUNCTION DETAILS  : Read TEDS ID.

  Arguments as follows:

  Arg 0 :	Channel
********************************************************************************/

void tedsid(char* args[], int numargs, int skt)
{
	int chan;
	chandef* c;
	int err;
	uint32_t hi;
	uint32_t lo;

	chan = strtoul(args[0], NULL, 0);		/* Get channel to read */

	c = chanptr(chan);

	if (c && c->tedsid) {

		netprintf(skt, "Reading TEDS ROM data\n\r");

		if ((err = c->tedsid(c, &lo, &hi)) == NO_1WIRE) {
			netprintf(skt, "No TEDS device detected\n\r");
			return;
		}

		netprintf(skt, "ROM data ");

		netprintf(skt, "%02X ", (hi >> 24) & 0xFF);
		netprintf(skt, "%02X ", (hi >> 16) & 0xFF);
		netprintf(skt, "%02X ", (hi >> 8) & 0xFF);
		netprintf(skt, "%02X ", hi & 0xFF);

		netprintf(skt, "%02X ", (lo >> 24) & 0xFF);
		netprintf(skt, "%02X ", (lo >> 16) & 0xFF);
		netprintf(skt, "%02X ", (lo >> 8) & 0xFF);
		netprintf(skt, "%02X ", lo & 0xFF);

		netprintf(skt, "\n\r");

		if (err == CRC_ERROR) {
			netprintf(skt, "CRC error in ROM data\n\r");
		}

	}
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);
}



/********************************************************************************
  FUNCTION NAME   	: setexc
  FUNCTION DETAILS  : Set channel excitation.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :	Type (0=DC,1=AC)
  Arg 2 :	Raw excitation value
  Arg 3 :	Phase angle (optional)
********************************************************************************/

void setexc(char* args[], int numargs, int skt)
{
	int chan;
	int type;
	int value;
	int i_phase;
	float phase;
	chandef* c;

	chan = strtoul(args[0], NULL, 0);		/* Get channel to configure */
	type = strtoul(args[1], NULL, 0);		/* Get type */
	value = strtoul(args[2], NULL, 0);		/* Get value */
	if (numargs == 4) {
		i_phase = strtoul(args[3], NULL, 0);	/* Get phase */
	}
	else {
		i_phase = 0;
	}
	phase = (float)i_phase;

	c = chanptr(chan);

	if (c && c->setrawexc)
		c->setrawexc(c, type, value, phase, 0);
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);
}



/********************************************************************************
  FUNCTION NAME   	: enableexc
  FUNCTION DETAILS  : Enable/disable channel excitation.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :	Enable/disable
********************************************************************************/

void enableexc(char* args[], int numargs, int skt)
{
	int chan;
	int enable;
	chandef* c;

	chan = strtoul(args[0], NULL, 0);		/* Get channel to configure */
	enable = strtoul(args[1], NULL, 0);		/* Get enable flag */

	c = chanptr(chan);

	if (c && c->enableexc)
		c->enableexc(c, enable);
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);
}



/********************************************************************************
  FUNCTION NAME   	: setrawgain
  FUNCTION DETAILS  : Set channel raw gain.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :	Coarse gain (0,1,2)
  Arg 2 :	Fine gain (0 - 255)
********************************************************************************/

void setrawgain(char* args[], int numargs, int skt)
{
	int chan;
	int coarse;
	int fine;
	chandef* c;

	chan = strtoul(args[0], NULL, 0);		/* Get channel to configure */
	coarse = strtoul(args[1], NULL, 0);		/* Get coarse gain */
	fine = strtoul(args[2], NULL, 0);		/* Get fine gain */

	c = chanptr(chan);

	if (c && c->setrawgain)
		c->setrawgain(c, coarse, fine);
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);

}



/********************************************************************************
  FUNCTION NAME   	: setgain
  FUNCTION DETAILS  : Set channel gain.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :	Gain
********************************************************************************/

void setgain(char* args[], int numargs, int skt)
{
	int chan;
	chandef* c;
	float gain;

	chan = strtoul(args[0], NULL, 0);		/* Get channel to configure */
	gain = atof(args[1]);					/* Get required gain */

	c = chanptr(chan);

	if (c && c->setgain)
		c->setgain(c, gain, 0, MIRROR);
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);

}



/********************************************************************************
  FUNCTION NAME   	: readadc
  FUNCTION DETAILS  : Read channel ADC value.

  Arguments as follows:

  Arg 0 :	Channel
********************************************************************************/

void readadc(char* args[], int numargs, int skt)
{
	int chan;
	float adc;
	int count;
	chandef* c;

	chan = strtoul(args[0], NULL, 0);		/* Get channel to read */
	if (numargs == 2) {
		count = strtoul(args[1], NULL, 0);	/* Get count */
	}
	else {
		count = 1;
	}

	c = chanptr(chan);

	if (c && c->readadc) {
		while (count) {
			adc = c->readadc(c);
			netprintf(skt, "Value %f\n\r", adc);
			count--;
		}
	}
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);

}



/********************************************************************************
  FUNCTION NAME   	: writedac
  FUNCTION DETAILS  : Set channel output.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :	Value
********************************************************************************/

void writedac(char* args[], int numargs, int skt)
{
	int chan;
	float value;
	chandef* c;

	chan = strtoul(args[0], NULL, 0);	/* Get channel to write */
	value = atof(args[1]);				/* Get output value */

	//c = &outchaninfo[chan];
	c = chanptr(chan);

	if (c && c->writedac)
		c->writedac(c, value);
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);

}



/********************************************************************************
  FUNCTION NAME   	: setled
  FUNCTION DETAILS  : Set LED state.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :	LED state
********************************************************************************/

void setled(char* args[], int numargs, int skt)
{
	int chan;
	int value;
	chandef* c;

	chan = strtoul(args[0], NULL, 0);		/* Get channel to set */
	value = strtol(args[1], NULL, 0);		/* Get LED state */

	c = chanptr(chan);

	if (c && c->ledctrl)
		c->ledctrl(c, value);
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);

}



/********************************************************************************
  FUNCTION NAME   	: setcal
  FUNCTION DETAILS  : Set calibration relay state.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :	calibration relay state
			  0 = off
			  1 = positive cal
			  3 = negative cal
********************************************************************************/

void setcal(char* args[], int numargs, int skt)
{
	int chan;
	int value;
	chandef* c;

	chan = strtoul(args[0], NULL, 0);		/* Get channel to set */
	value = strtol(args[1], NULL, 0);		/* Get cal relay state */

	c = chanptr(chan);

	if (c && c->calctrl)
		c->calctrl(c, value);
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);

}



/********************************************************************************
  FUNCTION NAME   	: setsync
  FUNCTION DETAILS  : Set ADC/DAC sync clock output control.

  Arguments as follows:

  Arg 0 :	Enable
  Arg 1 :	Count
********************************************************************************/

void setsync(char* args[], int numargs, int skt)
{
	int enable;
	int count;

	enable = strtoul(args[0], NULL, 0);		/* Get enable */
	count = strtol(args[1], NULL, 0);		/* Get count value */

	cnet_setsync(enable, count);
}



/********************************************************************************
  FUNCTION NAME   	: setcurrent
  FUNCTION DETAILS  : Set current range on SV output.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :	Current range (0 - 255)
********************************************************************************/

void setcurrent(char* args[], int numargs, int skt)
{
	int chan;
	int current;
	chandef* c;

	chan = strtoul(args[0], NULL, 0);		/* Get channel to configure */
	current = strtoul(args[1], NULL, 0);	/* Get current range */

	c = chanptr(chan);

	if (c && c->setrawcurrent)
		c->setrawcurrent(c, current);
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);

}



/********************************************************************************
  FUNCTION NAME   	: setfilter
  FUNCTION DETAILS  : Set channel filter settings.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :   Enable (0=Off,1=On)
  Arg 2 :	Filter type (0=Lowpass,1=Notch,2=Highpass,3=Bandpass)
  Arg 3 :	Filter order (1..4)
  Arg 4 :	Cutoff frequency
  Arg 5 :	Bandwidth
********************************************************************************/

void setfilter(char* args[], int numargs, int skt)
{
	int chan;
	int enable;
	int type;
	int order;
	float freq;
	float bw = 0.0;
	chandef* c;

	chan = strtoul(args[0], NULL, 0);		/* Get channel to configure */
	enable = strtoul(args[1], NULL, 0);		/* Get enable */
	type = strtoul(args[2], NULL, 0);		/* Get type */
	order = strtoul(args[3], NULL, 0);		/* Get order */
	freq = atof(args[4]);					/* Get frequency */

	if (numargs == 6) {
		bw = atof(args[5]);					/* Get bandwidth */
	}

	c = chanptr(chan);

	if (c && c->setfilter)
		c->setfilter(c, FILTER_ALL, 0, enable, type, order, freq, bw, NO_MIRROR);
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);
}



/********************************************************************************
  FUNCTION NAME   	: logwrite
  FUNCTION DETAILS  : Write into event log.
********************************************************************************/

void logwrite(char* args[], int numargs, int skt)
{
	eventlog_entry log;

	if (numargs == 3) {
		log.type = (int)strtol(args[0], NULL, 0);						/* Get type 			 */
		log.parameter1 = (float)strtol(args[1], NULL, 0);				/* Get parameter 1 value */
		log.parameter2 = (float)strtol(args[2], NULL, 0);				/* Get parameter 2 value */
		cnet_read_timestamp(&log.timestamp_lo, &log.timestamp_hi);	/* Get current timestamp */
		if (eventlog_write(&log) == LOG_FULL) {
			netprintf(skt, "Error: event log full\n\r");
		}
	}
}



const char* event_string[] = {
"None",
"LimitTrip",
"ValUR",
"ValOR",
"ValNan",
"SetRO",
"SetUR",
"SetOR",
"SetNan",
"SetLock",
"Powerdown",
"HydraulicStateChange",
"NoExtCompTx",
"NoModeTx",
"NoILTx",
"NotConf",
"NoSpareVTx",
"BadFormula",
"CNetStateChange",
"CNetWdogFail",
"HydUnloadReq",
"HydMasterTransition",
"HydSlaveIpOff",
"TxdrFaultStateChange",
"MsgHandlerEvent",
"MsgHandlerClose",
"NetworkConfig",
"DHCPEvent",
"GuardStatusChange",
"CommTimeout",
"MsgHandlerOpen",
"CommBadClose",
};

/********************************************************************************
  FUNCTION NAME   	: showevent
  FUNCTION DETAILS  : Show the event log.
********************************************************************************/

void showevent(char* args[], int numargs, int skt)
{
	eventlog_entry entry;
	uint32_t n = 0;

	while (eventlog_read(&entry)) {
		n++;
		netprintf(skt, "%3d ", n);
		netprintf(skt, "%08X ", entry.timestamp_hi);
		netprintf(skt, "%08X ", entry.timestamp_lo);
		netprintf(skt, "(%10.4f) ", ((double)entry.timestamp_lo) / 4096.0);
		netprintf(skt, "%02X ", entry.type);
		netprintf(skt, "(%-20s) ", (entry.type <= LogTypeCommBadClose) ? event_string[entry.type] : "");
		netprintf(skt, "%08X ", entry.parameter1);
		netprintf(skt, "%08X\n\r", entry.parameter2);
	}
	if (n == 0) {
		netprintf(skt, "Log empty\n\r");
	}
}



/********************************************************************************
  FUNCTION NAME   	: showlist
  FUNCTION DETAILS  : Show CNet output list.
********************************************************************************/

void showlist(char* args[], int numargs, int skt)
{
	int list[256];
	int size;
	int n;

	cnet_read_list(&size, list);

	netprintf(skt, "CNet output list : ");
	for (n = 0; n < size; n++) {
		netprintf(skt, "%d ", list[n]);
	}

	netprintf(skt, "\n\r");
}



/********************************************************************************
  FUNCTION NAME   	: baseaddr
  FUNCTION DETAILS  : Display channel base address.

  Arguments as follows:

  Arg 0 :	Channel number
********************************************************************************/

void baseaddr(char* args[], int numargs, int skt)
{
	int chan;
	int basea;
	int baseb;

	chan = strtoul(args[0], NULL, 0);		/* Get channel 				*/
	basea = (int)chanptr(chan);				/* Get DSP A base address	*/
	if (!basea) {
		netprintf(skt, "Illegal channel\n\r");
	}
	else {
		baseb = (int)get_channel_base_dspb(basea);	/* Get DSP B base address	*/
		netprintf(skt, "DSPA: %08X  DSPB: %08X\n\r", basea, baseb);
	}
}



/********************************************************************************
  FUNCTION NAME   	: showchan
  FUNCTION DETAILS  : Display channel definition.

  Arguments as follows:

  Arg 0 :	Channel number
********************************************************************************/

void showchan(char* args[], int numargs, int skt)
{
	int chan;
	int basea;
	int baseb;
	int addr;
	int dsp = 0;
	int n;
	simple_chandef def;

	chan = ((uint32_t)strtoul(args[0], NULL, 0));	/* Get channel */

	if (numargs == 2) {
		for (n = 0; n < strlen(args[1]); n++) {
			switch (args[1][n]) {
			case 'A':
			case 'a':
				dsp = 0;
				break;
			case 'B':
			case 'b':
				dsp = 1;
				break;
			}
		}
	}

	basea = (int)chanptr(chan);				/* Get DSP A base address	*/
	if (!basea) {
		netprintf(skt, "Illegal channel\n\r");
	}
	else {
		baseb = (int)get_channel_base_dspb(basea);	/* Get DSP B base address	*/
		if (dsp) {
#if (defined(CONTROLCUBE) || defined(AICUBE))
			hpi_blockread((uint8_t*)&def, (uint8_t*)baseb, sizeof(simple_chandef));
			addr = baseb;
#endif
#ifdef SIGNALCUBE
			memcpy((void*)&def, (void*)baseb, sizeof(simple_chandef));
			addr = baseb;
#endif
		}
		else {
			memcpy((void*)&def, (void*)basea, sizeof(simple_chandef));
			addr = basea;
		}
		netprintf(skt, "base address    : %08X\n\r", addr);
		netprintf(skt, "filtered        : %.5g\n\r", def.filtered);
		netprintf(skt, "value           : %.5g\n\r", def.value);
		netprintf(skt, "type            : %08X\n\r", def.type);
		netprintf(skt, "ctrl            : %08X\n\r", def.ctrl);
		netprintf(skt, "status          : %08X\n\r", def.status);
		netprintf(skt, "chanid          : %08X\n\r", def.chanid);
		netprintf(skt, "slot            : %08X\n\r", def.slot);
		netprintf(skt, "sourceptr       : %08X\n\r", def.sourceptr);
		netprintf(skt, "outputptr       : %08X\n\r", def.outputptr);
		netprintf(skt, "offset          : %08X\n\r", def.offset);
		netprintf(skt, "pos gaintrim    : %08X (%.8g)\n\r", def.pos_gaintrim, ((float)def.pos_gaintrim / GAINTRIM_SCALE));
		netprintf(skt, "neg gaintrim    : %08X (%.8g)\n\r", def.neg_gaintrim, ((float)def.neg_gaintrim / GAINTRIM_SCALE));
		netprintf(skt, "txdrzero        : %08X\n\r", def.txdrzero);
		netprintf(skt, "refzero         : %.5g\n\r", def.refzero);
		netprintf(skt, "cycwindow       : %.5g\n\r", def.cycwindow);
		netprintf(skt, "bitscale        : %08X\n\r", def.bitscale);
		netprintf(skt, "spptr           : %08X\n\r", def.spptr);
		netprintf(skt, "simptr          : %08X\n\r", def.simptr);
		netprintf(skt, "simscalep       : %.5g\n\r", def.simscalep);
		netprintf(skt, "simscalen       : %.5g\n\r", def.simscalen);
		netprintf(skt, "satpos          : %08X\n\r", def.satpos);
		netprintf(skt, "satneg          : %08X\n\r", def.satneg);
		netprintf(skt, "filter type     : %08X\n\r", def.filter1.type);
		netprintf(skt, "filter order    : %d\n\r", def.filter1.order);
		netprintf(skt, "filter freq     : %.5g\n\r", def.filter1.freq);
		netprintf(skt, "filter bandwidth: %.5g\n\r", def.filter1.bandwidth);
		for (n = 0; n < 10; n++)
			netprintf(skt, "filter coeff[%d]: %.5g\n\r", n, def.filter1.coeff[n]);
		for (n = 0; n < 4; n++)
			netprintf(skt, "filter delta[%d]: %.5g\n\r", n, def.filter1.delta[n]);
		netprintf(skt, "maximum         : %.5g\n\r", def.maximum);
		netprintf(skt, "minimum         : %.5g\n\r", def.minimum);
		netprintf(skt, "peak            : %.5g\n\r", def.peak);
		netprintf(skt, "trough          : %.5g\n\r", def.trough);
		netprintf(skt, "pk              : %.5g\n\r", def.pk);
		netprintf(skt, "cyc_peak        : %.5g\n\r", def.cyc_peak);
		netprintf(skt, "cyc_trough      : %.5g\n\r", def.cyc_trough);
		netprintf(skt, "cyc_counter     : %08X\n\r", def.cyc_counter);
		netprintf(skt, "timeout         : %08X\n\r", def.timeout);
		netprintf(skt, "zerofilt        : %.5g\n\r", def.zerofilt);
		netprintf(skt, "zerofiltop      : %.5g\n\r", def.zerofiltop);
		netprintf(skt, "mapptr          : %08X\n\r", def.mapptr);

		netprintf(skt, "upper cmd clamp : %.8g\n\r", chanptr(chan)->upper_cmd_clamp);
		netprintf(skt, "lower cmd clamp : %.8g\n\r", chanptr(chan)->lower_cmd_clamp);

		netprintf(skt, "identifier      : %08X\n\r", chanptr(chan)->identifier);

#if 0
		if (!dsp) {
			netprintf(skt, "eecal_start     : %08X\n\r", def.eecal_start);
			netprintf(skt, "eecal_size      : %08X\n\r", def.eecal_size);
			netprintf(skt, "eecfg_start     : %08X\n\r", def.eecfg_start);
			netprintf(skt, "eeformat        : %08X\n\r", def.eeformat);
			netprintf(skt, "eesavelist      : %08X\n\r", def.eesavelist);
			netprintf(skt, "eesavetable     : %08X\n\r", def.eesavetable);
		}
#endif
	}
}



/********************************************************************************
  FUNCTION NAME   	: cnetaddr
  FUNCTION DETAILS  : Display CNet slot address.

  Arguments as follows:

  Arg 0 :	Slot number
********************************************************************************/

void cnetaddr(char* args[], int numargs, int skt)
{
	int slot;
	uint32_t addr;

	slot = strtoul(args[0], NULL, 0);			/* Get slot 		*/
	if ((slot < 0) || (slot > 255))
		netprintf(skt, "Illegal channel\n\r");
	else {
		addr = (uint32_t)cnet_get_slotaddr(slot, PROC_DSPB);	/* Get CNet address	*/
		netprintf(skt, "Slot address: %08X\n\r", addr);
	}
}



extern uint32_t proc_usage;
extern uint32_t max_proc_usage;
extern uint32_t timer_usage;
extern uint32_t max_timer_usage;
extern uint32_t background_counter;

#define PROCMAX	120.0		/* 100% CNet interrupt usage  */
#define TIMERMAX 2500.0		/* 100% timer interrupt usage */

/* Read HPI location and convert to a percentage processor time */

#define TIMEPERCENT(x) (((float)hpi_read4(x)) / PROCMAX)

/********************************************************************************
  FUNCTION NAME   	: procuse
  FUNCTION DETAILS  : Display processor usage.

  Arguments as follows:

  None
********************************************************************************/

void procuse(char* args[], int numargs, int skt)
{
	uint32_t use, peak;
	float usea;
	float peaka;

#if (defined(CONTROLCUBE) || defined(AICUBE))

	float use_timer;
	float peak_timer;

#endif

	use = proc_usage;
	peak = max_proc_usage;

	/* Convert DSPA CNet interrupt usage figures to percentage values */

	usea = (float)use / PROCMAX;
	peaka = (float)peak / PROCMAX;

	/* Convert timer interrupt usage figures to percentage values */

#if (defined(CONTROLCUBE) || defined(AICUBE))

	use_timer = (float)timer_usage / TIMERMAX;
	peak_timer = (float)max_timer_usage / TIMERMAX;

#endif

	netprintf(skt, "Usage DSPA CNet              Current:%5.1f Peak:%5.1f\n\r", usea, peaka);
#if (defined(CONTROLCUBE) || defined(AICUBE))
	netprintf(skt, "Usage DSPA Timer             Current:%5.1f Peak:%5.1f\n\r", use_timer, peak_timer);
	netprintf(skt, "      DSPB CNet total        Current:%5.1f Peak:%5.1f\n\r", TIMEPERCENT(0x24), TIMEPERCENT(0x28));
	netprintf(skt, "      DSPB CNet inchan phys  Current:%5.1f Peak:%5.1f\n\r", TIMEPERCENT(0x3C), TIMEPERCENT(0x40));
	netprintf(skt, "      DSPB CNet BCtrlIterA   Current:%5.1f Peak:%5.1f\n\r", TIMEPERCENT(0x44), TIMEPERCENT(0x48));
	netprintf(skt, "      DSPB CNet inchan virt  Current:%5.1f Peak:%5.1f\n\r", TIMEPERCENT(0x4C), TIMEPERCENT(0x50));
	netprintf(skt, "      DSPB CNet BCtrlIterB   Current:%5.1f Peak:%5.1f\n\r", TIMEPERCENT(0x54), TIMEPERCENT(0x58));
	netprintf(skt, "      DSPB CNet out/miscchan Current:%5.1f Peak:%5.1f\n\r", TIMEPERCENT(0x5C), TIMEPERCENT(0x60));
#endif
	netprintf(skt, "Background counter:%d\n\r", background_counter);

	if ((numargs == 1) && ((*args[0] == 'R') || (*args[0] == 'r'))) resetuse(NULL, 0, 0);
}



/********************************************************************************
  FUNCTION NAME   	: resetuse
  FUNCTION DETAILS  : Reset processor usage.

  Arguments as follows:

  None
********************************************************************************/

void resetuse(char* args[], int numargs, int skt)
{
	hpi_write4(0x28, 0);
	hpi_write4(0x40, 0);
	hpi_write4(0x48, 0);
	hpi_write4(0x50, 0);
	hpi_write4(0x58, 0);
	hpi_write4(0x60, 0);
	max_proc_usage = 0;
	max_timer_usage = 0;
}



/********************************************************************************
  FUNCTION NAME   	: getprocuse
  FUNCTION DETAILS  : Return processor usage statistics.

  Arguments as follows:

  None
********************************************************************************/

void getprocuse(float* res, int reset)
{
	uint32_t use, peak;
	float usea, peaka;

#if (defined(CONTROLCUBE) || defined(AICUBE))

	float use_timer, peak_timer;
	int p;

#endif

	use = proc_usage;
	peak = max_proc_usage;

	/* Convert DSPA CNet interrupt usage figures to percentage values */

	usea = (float)use / PROCMAX;
	peaka = (float)peak / PROCMAX;

	/* Convert timer interrupt usage figures to percentage values */

#if (defined(CONTROLCUBE) || defined(AICUBE))

	use_timer = (float)timer_usage / TIMERMAX;
	peak_timer = (float)max_timer_usage / TIMERMAX;

#endif

	memset(res, 0, 16 * sizeof(float));

	*res++ = usea;
	*res++ = peaka;

#if (defined(CONTROLCUBE) || defined(AICUBE))

	* res++ = use_timer;
	*res++ = peak_timer;

	*res++ = TIMEPERCENT(0x24);
	*res++ = TIMEPERCENT(0x28);

	for (p = 0x3C; p <= 0x60; p += 4) {
		*res++ = TIMEPERCENT(p);
	}

#endif

	if (reset) resetuse(NULL, 0, 0);
}



/********************************************************************************
  FUNCTION NAME   	: setgaintrim
  FUNCTION DETAILS  : Set channel gain trim value.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :	Gain trim
********************************************************************************/

void setgaintrim(char* args[], int numargs, int skt)
{
	int chan;
	chandef* c;
	float pos_gain;
	float neg_gain;

	chan = strtoul(args[0], NULL, 0);		/* Get channel to configure 		*/
	pos_gain = atof(args[1]);				/* Get required positive gain trim 	*/
	neg_gain = atof(args[2]);				/* Get required negative gain trim 	*/

	c = chanptr(chan);

	if (c && c->set_gaintrim) {
		c->set_gaintrim(c, &pos_gain, NULL, FALSE, MIRROR);
		c->set_gaintrim(c, NULL, &neg_gain, FALSE, NO_MIRROR);
	}
	else
		netprintf(skt, "Channel %d does not support this function\n\r", chan);

}



/********************************************************************************
  FUNCTION NAME   	: kmode
  FUNCTION DETAILS  : Configure KiNet operating mode.

  Arguments as follows:

  Arg 0 :   Flags:	"E" enable or "D" disable KiNet operation
					"M" master or "S" slave
********************************************************************************/

void kmode(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	int n;
	uint32_t enable = 0;
	uint32_t master = 0;
	uint32_t mode;

	if (kinetptr) {

		if (numargs == 1) {
			for (n = 0; n < strlen(args[0]); n++) {
				switch (args[0][n]) {
				case 'E':
				case 'e':
					enable = KINET_ENABLE;
					break;
				case 'D':
				case 'd':
					enable = 0;
					break;
				case 'M':
				case 'm':
					master = KINET_MASTER;
					break;
				case 'S':
				case 's':
					master = 0;
					break;
				default:
					break;
				}
			}
			mode = master | enable;
			kinetptr->setmode(mode);
		}
		else {
			mode = kinetptr->getmode();
			netprintf(skt, "KiNet mode %s %s\n\r", (mode & KINET_ENABLE) ? "Enabled" : "Disabled",
				(mode & KINET_MASTER) ? "Master" : "Slave");
		}
	}
	else {
		netprintf(skt, "No KiNet hardware\n\r");
	}

#else

	netprintf(skt, "Not supported\n\r");

#endif
}



/********************************************************************************
  FUNCTION NAME   	: kdir
  FUNCTION DETAILS  : Set/read KiNet slot direction.

  Arguments as follows:

  Arg 0 :	Slot number
  Arg 1 :	Direction			"I" or "i" select input slot
								"O" or "o" select output slot
********************************************************************************/

void kdir(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	int slot;
	int dirn;

	if (kinetptr) {
		slot = (int)strtoul(args[0], NULL, 0);	/* Get slot number */
		if (numargs == 2) {
			switch (args[1][0]) {
			case 'O':
			case 'o':
				dirn = KINET_OUTPUT;
				break;
			case 'I':
			case 'i':
				dirn = KINET_INPUT;
				break;
			}
			kinetptr->setdirn(slot, dirn);
		}
		else {
			dirn = kinetptr->getdirn(slot);
			netprintf(skt, "KiNet slot %d direction %s\n\r", slot, (dirn == KINET_INPUT) ? "input" : "output");
		}
	}
	else {
		netprintf(skt, "No KiNet hardware\n\r");
	}

#else

	netprintf(skt, "Not supported\n\r");

#endif
}



/********************************************************************************
  FUNCTION NAME   	: kdata
  FUNCTION DETAILS  : Set/read KiNet slot data.

  Arguments as follows:

  Arg 0 :	Slot number
  Arg 1 :	Data (optional)
********************************************************************************/

void kdata(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	int slot;
	s16_t data;

	if (kinetptr) {
		slot = (int)strtoul(args[0], NULL, 0);		/* Get slot number */
		if (numargs == 2) {
			data = (int)strtol(args[1], NULL, 0);	/* Get data value */
			kinetptr->setdata(slot, data);
		}
		else {
			data = kinetptr->getdata(slot);
			netprintf(skt, "KiNet slot %d data %04X\n\r", slot, (data & 0xFFFF));
		}
	}
	else {
		netprintf(skt, "No KiNet hardware\n\r");
	}

#else

	netprintf(skt, "Not supported\n\r");

#endif
}



void runtimer_read(uint32_t* timer_lo, uint32_t* timer_hi);

/********************************************************************************
  FUNCTION NAME   	: sysstate
  FUNCTION DETAILS  : Read system state.
********************************************************************************/

void sysstate(char* args[], int numargs, int skt)
{
	uint8_t state;
#if (defined(CONTROLCUBE) || defined(AICUBE))
	uint32_t estop;
	uint32_t slave;
	uint32_t lo;
	uint32_t hi;
#endif

	state = get_sys_state();
#if (defined(CONTROLCUBE) || defined(AICUBE))
	get_estop_state(&estop, &slave, NULL);
#endif

	netprintf(skt, "System state:\n\r");
#if (defined(CONTROLCUBE) || defined(AICUBE))
	netprintf(skt, "  Fan:         %s\n\r", (state & IP_FANSENSE) ? "Stopped" : "Running");
#endif
	netprintf(skt, "  TMS320C6713: %s\n\r", (state & IP_SLAVE) ? "No" : "Yes");
	netprintf(skt, "  Initialise:  %s\n\r", (state & IP_INITIALISE) ? "Yes" : "No");
#if (defined(CONTROLCUBE) || defined(AICUBE))
	netprintf(skt, "  EStop I/P:   %s\n\r", (estop) ? "On" : "Off");
	netprintf(skt, "  EStop Relay: %s\n\r", (slave) ? "On" : "Off");

	runtimer_read(&lo, &hi);
	netprintf(skt, "  Runtime:     %08X %08X\n\r", hi, lo);
#endif
}



/********************************************************************************
  FUNCTION NAME   	: genarp
  FUNCTION DETAILS  : Generate ARP packet for test purposes.
********************************************************************************/

void send_arp(void);

void genarp(char* args[], int numargs, int skt)
{
	send_arp();
}



extern int tx_state;	/* Current transmit handler state		*/
extern int rx_state;	/* Current receive handler state		*/

/********************************************************************************
  FUNCTION NAME   	: async
  FUNCTION DETAILS  : Display status of async comms.
********************************************************************************/

void async(char* args[], int numargs, int skt)
{
	netprintf(skt, "Async rx = %d tx = %d\n\r", rx_state, tx_state);
}



/********************************************************************************
  FUNCTION NAME   	: logdump
  FUNCTION DETAILS  : Dump the debug log area.
********************************************************************************/

void logdump(char* args[], int numargs, int skt)
{
	int m;
	int n;

	netprintf(skt, "Log pointer %d\n\r", debug_log_ptr);

	for (m = 0; m < sizeof(debug_log_data); m += 16) {
		netprintf(skt, "%04d  ", m);
		for (n = 0; n < 16; n++) {
			if ((m + n) < sizeof(debug_log_data)) netprintf(skt, "%03d ", debug_log_data[m + n]);
		}
		netprintf(skt, "\n\r");
	}
}



/********************************************************************************
  FUNCTION NAME   	: test1
  FUNCTION DETAILS  : Set/read DNV test value.

  Arguments as follows:

  Arg 0 :	Slot number
  Arg 1 :	Data (optional)
********************************************************************************/

void test1(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	int data;

	if (numargs == 1) {
		data = (int)strtol(args[0], NULL, 0);	/* Get data value */
		dnvbs->dummy = data;
		CtrlDnvCB(&dnvbs->dummy, sizeof(int));
	}
	else {
		netprintf(skt, "Value = %d\n\r", dnvbs->dummy);
	}

#endif  
}



/********************************************************************************
  FUNCTION NAME   	: normalise
  FUNCTION DETAILS  : Normalise DSP B parameters.
********************************************************************************/

void normalise(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	CtrlNormalise();
	CtrlPrime();
	netprintf(skt, "Normalised & primed\n\r");

#endif
}



/********************************************************************************
  FUNCTION NAME   	: pointer
  FUNCTION DETAILS  : Set/read channel data pointer.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :	Data pointer (optional)
********************************************************************************/

void pointer(char* args[], int numargs, int skt)
{
	int chan;
	chandef* c;
	uint32_t ptr;

	chan = strtoul(args[0], NULL, 0); /* Get channel to configure */
	c = chanptr(chan);
	if (!c) goto pointer_notsupported;

	if (numargs == 2) {
		ptr = strtoul(args[1], NULL, 0); /* Get pointer */
		if (c->setpointer)
			c->setpointer(c, (int*)ptr);
		else
			goto pointer_notsupported;
	}
	else {
		if (c->getpointer) {
			ptr = (int)c->getpointer(c);
			netprintf(skt, "Channel %d pointer %08X (%u)\n\r", chan, ptr, ptr);
		}
		else
			goto pointer_notsupported;
	}
	return;

pointer_notsupported:
	netprintf(skt, "Channel %d does not support this function\n\r", chan);
}



/********************************************************************************
  FUNCTION NAME   	: hydstat
  FUNCTION DETAILS  : Display hydraulic status.
********************************************************************************/

void hydstat(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	uint32_t state;
	uint32_t ack;
	uint32_t status;
	uint32_t hc_status;

	hyd_read_state(&state, &ack);
	status = hyd_get_status();
	netprintf(skt, "Hydraulic state %08X ack %08X status %08X\n\r\n\r", state, ack, status);

	hc_status = hpi_read4(HC_STATUS);
	netprintf(skt, "HC status:   ");
	switch (hc_status) {
	case HC_UNKNOWN:
		netprintf(skt, "Unknown");
		break;
	case HC_IDENTIFYING:
		netprintf(skt, "Identifying");
		break;
	case HC_NOTPRESENT1:
		netprintf(skt, "Not Present (1)");
		break;
	case HC_PRESENT1:
		netprintf(skt, "Present (1)");
		break;
	case HC_NOTPRESENT2:
		netprintf(skt, "Not Present (2)");
		break;
	case HC_PRESENT2:
		netprintf(skt, "Present (2)");
		break;
	case HC_PRESENT3:
		netprintf(skt, "Present (3)");
		break;
	default:
		netprintf(skt, "Illegal (%08X)", hc_status);
		break;
	}
	netprintf(skt, "\n\r");

	state = hyd_get_raw_io_state();
	netprintf(skt, "EM1L Sense:  %s\n\r", (state & EM1L_SENSE) ? "High" : "Low");
	netprintf(skt, "EM1H Sense:  %s\n\r", (state & EM1H_SENSE) ? "High" : "Low");
	netprintf(skt, "EM2L Sense:  %s\n\r", (state & EM2L_SENSE) ? "High" : "Low");
	netprintf(skt, "EM2H Sense:  %s\n\r", (state & EM2H_SENSE) ? "High" : "Low");
	netprintf(skt, "EM Sense:    %s\n\r", (state & EM_SENSE) ? "High" : "Low");
	netprintf(skt, "ES Slave:    %s\n\r", (state & ES_RELAY_SLAVE) ? "High" : "Low");

	hyd_show_debug(skt);

#else

	netprintf(skt, "Not supported\n\r");

#endif
}

/********************************************************************************
  FUNCTION NAME   	: hydsettype
  FUNCTION DETAILS  : Display hydraulic status.
********************************************************************************/

void hydsettype(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	int data;

	if (numargs == 1) {
		//netprintf(skt, "Hello World!\n\r");
		data = (int)strtol(args[0], NULL, 0);	/* Get data value */
		hyd_set_type(skt, data);
	}
	else {
		hyd_set_type(skt, NULL);
	}
#else

	netprintf(skt, "Not supported\n\r");

#endif
}

/********************************************************************************
  FUNCTION NAME   	: unload
  FUNCTION DETAILS  : Unload hydraulics.
********************************************************************************/

void unload(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	hyd_unload();
#endif
}



/********************************************************************************
  FUNCTION NAME   	: nvspace
  FUNCTION DETAILS  : Display information about NV area.
********************************************************************************/

void nvstat(char* args[], int numargs, int skt)
{
	int chan;
	chandef* c;
	int snv_padding = offsetof(struct snv, user_data) - offsetof(struct snv, padding);

	netprintf(skt, "System storage\n\r");
	netprintf(skt, "--------------\n\r");
	netprintf(skt, "System SNV (used)      : %d bytes\n\r", sizeof(snv) - snv_padding);
	netprintf(skt, "System SNV (allocated) : %d bytes\n\r", SYS_SNV_SIZE);

#if (defined(CONTROLCUBE) || defined(AICUBE))
	netprintf(skt, "Parameter SNV          : %d bytes\n\r", CtrlGetSnvSz());
	netprintf(skt, "Parameter DNV          : %d bytes\n\r", CtrlGetDnvSz());
#endif

	netprintf(skt, "Channel storage\n\r");
	netprintf(skt, "---------------\n\r");
	for (chan = 0; chan < (CHANTYPE_DIGIO | 0xFF); chan++) {
		if (c = chanptr(chan)) {
			netprintf(skt, "Channel %4d : cfg %5d bytes, cal %5d bytes\n\r", chan, get_list_size(c->eesavelist),
				c->eecal_size);
		}
	}
}


void ModifyCommandClamp(int channel, float min, float max);

/********************************************************************************
  FUNCTION NAME   	: cmdclamp
  FUNCTION DETAILS  : Set command clamping values.

  Arguments as follows:

  Arg 0 :	Channel
  Arg 1 :	Min value
  Arg 2 :	Max value
********************************************************************************/

void cmdclamp(char* args[], int numargs, int skt)
{
	int chan;
	float min;
	float max;
	chandef* c;

	chan = strtoul(args[0], NULL, 0);	/* Get channel to write */
	min = atof(args[1]);				/* Get minimum command value */
	max = atof(args[2]);				/* Get maximum command value */

	c = chanptr(chan);

	if (c)
		ModifyCommandClamp(c->chanid & 0xFF, min, max);
	else
		netprintf(skt, "Invalid channel\n\r");

}



/********************************************************************************
  FUNCTION NAME   	: showencoder
  FUNCTION DETAILS  : Show raw encoder count.
********************************************************************************/

void showencoder(char* args[], int numargs, int skt)
{
	uint16_t ctr = (*((uint32_t*)0xB0003038) >> 16) & 0xFFFF;

	netprintf(skt, "Count %04X CNet %08X\n\r", ctr, cnet_mem_data[CNET_DATASLOT_ENCODER]);
}



extern near uint32_t debug_rt_bufin_count;
extern near uint32_t debug_rt_rampin_count;
extern near uint32_t debug_rt_copy_count;
extern near uint32_t debug_rt_notxfr_count;
extern near uint32_t debug_rt_underrun_count;
extern near uint32_t debug_rt_overrun_count;
extern near uint32_t debug_rt_ramp_count;
extern near uint32_t debug_rt_noinput_count;
extern near uint32_t debug_rt_local_count;

/********************************************************************************
  FUNCTION NAME   	: showrtstat
  FUNCTION DETAILS  : Show realtime transfer status.
********************************************************************************/

void showrtstat(char* args[], int numargs, int skt)
{
	netprintf(skt, "Realtime status\n\r");
	netprintf(skt, "Overrun count  : %d\n\r", debug_rt_overrun_count);
	netprintf(skt, "Underrun count : %d\n\r", debug_rt_underrun_count);
	netprintf(skt, "Bufin count    : %d\n\r", debug_rt_bufin_count);
	netprintf(skt, "Rampin count   : %d\n\r", debug_rt_rampin_count);
	netprintf(skt, "Copy count     : %d\n\r", debug_rt_copy_count);
	netprintf(skt, "Notxfr count   : %d\n\r", debug_rt_notxfr_count);
	netprintf(skt, "Ramp count     : %d\n\r", debug_rt_ramp_count);
	netprintf(skt, "Noinput count  : %d\n\r", debug_rt_noinput_count);
	netprintf(skt, "Local count    : %d\n\r", debug_rt_local_count);

	/* Check for reset option */

	if (numargs == 1) {
		if ((*args[0] == 'R') || (*args[0] == 'r')) {

			debug_rt_bufin_count = 0;
			debug_rt_rampin_count = 0;
			debug_rt_copy_count = 0;
			debug_rt_notxfr_count = 0;
			debug_rt_underrun_count = 0;
			debug_rt_overrun_count = 0;
			debug_rt_ramp_count = 0;
			debug_rt_noinput_count = 0;
			debug_rt_local_count = 0;
		}
	}

}



/********************************************************************************
  FUNCTION NAME   	: showfault
  FUNCTION DETAILS  : Show transducer/sv fault state.
********************************************************************************/

void showfault(char* args[], int numargs, int skt)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	int slot;
	hw_mboard* base;

	for (slot = 0; slot < 4; slot++) {
		if (slot_status[slot].type != CARD_NONE) {
			base = (hw_mboard*)slottable[slot];
			netprintf(skt, "Slot %d State %08X Mask %08X Txdrfault %08X\n\r", slot, (uint32_t)(base->u.rd.fault), slot_status[slot].faultmask, slot_status[slot].txdrfault);
		}
	}

#endif
}



/********************************************************************************
  FUNCTION NAME   	: cneterrstat
  FUNCTION DETAILS  : Show CNet error state registers.
********************************************************************************/

void cneterrstat(char* args[], int numargs, int skt)
{
	uint8_t regs[8];
	uint32_t chksum;
	int r;

	cnet_rderr(regs, &chksum);
	netprintf(skt, "CNet error state: Regs ");
	for (r = 0; r < 8; r++) {
		netprintf(skt, "%02X ", regs[r]);
	}
	netprintf(skt, "Calc chksum %02X\n\r", chksum);
}

/********************************************************************************
  FUNCTION NAME   	: cneterr
  FUNCTION DETAILS  : Generate CNet error.

  Arguments as follows:

  Arg 0 :	Slot
  Arg 1 :	Error type s=sof|c=chksum
********************************************************************************/

void cneterr(char* args[], int numargs, int skt)
{
	uint32_t slot;
	uint32_t flags = 0;
	uint32_t data = 0;

	slot = strtoul(args[0], NULL, 0);	/* Get error slot number */
	if (numargs > 1) {
		if (args[1][0] == 's') {
			flags = 0x02; /* Generate missing SOF error */
		}
		else if (args[1][0] == 'c') {
			flags = 0x01; /* Generate checksum error */
		}
		if (numargs == 3) {
			data = strtoul(args[2], NULL, 0);	/* Get modified data */
		}
	}

	netprintf(skt, "CNet error slot %d flags %02X data %08X\n\r", slot, flags, data);

	cnet_generr(slot, flags, data);
}

/********************************************************************************
  FUNCTION NAME   	: cnetlog
  FUNCTION DETAILS  : Log CNet packet.

  Arguments as follows:

  Arg 0 :	Timeslot number
********************************************************************************/

void cnetlog(char* args[], int numargs, int skt)
{
	uint32_t slot;
	uint8_t data[8];
	uint8_t chksum[8];
	int n;

	slot = strtoul(args[0], NULL, 0);	/* Get timeslot number */
	cnet_logpkt(slot, data, chksum);

	netprintf(skt, "CNet slot %d data ", slot);
	for (n = 0; n < 8; n++) {
		netprintf(skt, "%02X ", data[n]);
	}
	netprintf(skt, "chksum ");
	//netprintf(skt, "%02X (inverted = %02X)", chksum[0], (~chksum[0]) & 0xFF);
#if 1
	for (n = 0; n < 8; n++) {
		netprintf(skt, "%02X ", chksum[n]);
	}
#endif
	netprintf(skt, "\n\r");
}

/********************************************************************************
  FUNCTION NAME   	: cnetdebug
  FUNCTION DETAILS  : Debug CNet behaviour.

  Arguments as follows:

  Arg 0 :	Dataslot number
  Arg 1 :	Lower value to trigger log
  Arg 2 :	Upper value to trigger log
********************************************************************************/

void cnetdebug(char* args[], int numargs, int skt)
{
	uint32_t timeslot;
	uint32_t dataslot;
	float min;
	float max;
	uint32_t recorded;
	uint32_t val;
	uint8_t data_a[8];
	uint8_t data_b[8];
	int n;

	timeslot = strtoul(args[0], NULL, 0);	/* Get timeslot number  */
	dataslot = strtoul(args[1], NULL, 0);	/* Get dataslot number  */
	min = atof(args[2]);					/* Get minimum trigger	*/
	max = atof(args[3]);					/* Get maximum trigger	*/

	netprintf(skt, "CNet timeslot %d dataslot %d min %f max %f\n\r", timeslot, dataslot, min, max);

	cnet_debug(timeslot, dataslot, min, max, data_a, data_b, &recorded);

	netprintf(skt, "CNet timeslot %d bank a packet data ", timeslot);
	for (n = 0; n < 8; n++) {
		netprintf(skt, "%02X ", data_a[n]);
	}
	val = ((uint32_t)(data_a[3]) << 24) |
		((uint32_t)(data_a[4]) << 16) |
		((uint32_t)(data_a[5]) << 8) |
		((uint32_t)(data_a[6]) << 0);

	netprintf(skt, "val %f\n\r", _itof(val));

	netprintf(skt, "CNet timeslot %d bank b packet data ", timeslot);
	for (n = 0; n < 8; n++) {
		netprintf(skt, "%02X ", data_b[n]);
	}
	val = ((uint32_t)(data_b[3]) << 24) |
		((uint32_t)(data_b[4]) << 16) |
		((uint32_t)(data_b[5]) << 8) |
		((uint32_t)(data_b[6]) << 0);

	netprintf(skt, "val %f\n\r", _itof(val));

	netprintf(skt, "recorded %08X (float = %f)\n\r", recorded, _itof(recorded));
}

#endif /* LOADER */
