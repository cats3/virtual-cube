/********************************************************************************
  MODULE NAME     : server
  PROJECT		  : Control Cube / Signal Cube
  MODULE DETAILS  : Main server code

					This code is responsible for detecting connection requests
					and handing them over to an appropriate handler function if
					one is available.

					A fixed number of handlers are available for each type of
					connection, if the connection limit for a particular type is
					reached, then new connections of that type will fail.

********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <mqueue.h>

#include "defines.h"
#include "controller.h"
#include "server.h"

//#include <std.h>
//#include <mbx.h>
//#include <log.h>
//#include <mem.h>

connection* msgserver_list;



/********************************************************************************
  FUNCTION NAME   	: start_handler
  FUNCTION DETAILS  : Start a handler for the specified connection type.

					  On entry:

					  list 		Points to the start of a list of connection
								handlers of the correct type. This code scans
								through the list of handlers until it finds a
								free one. It then posts a message into that
								handlers mailbox, which unblocks the process and
								starts the handler running. If no free handler is
								found, then an error is returned.

					  socket	Holds the socket identifier on which the new
								connection has been made.

********************************************************************************/

int start_handler(connection* list, int socket)
{
	conn_msg msg;
	connection* c;

	c = list;

	while (c) {
		if (c->state == CONN_FREE) {
			msg.socket = socket;
			c->timeout = DEFAULT_CONN_TIMEOUT;		/* Default connection timeout   	*/
			c->action = DEFAULT_CONN_ACTION;		/* Default timeout action			*/
			c->timeoutctr = DEFAULT_CONN_TIMEOUT;	/* Initialise timeout counter		*/
			c->state = CONN_BUSY;
#if (defined(CONTROLCUBE) || defined(AICUBE))
			CtrlWdogOut(0);							/* Clear any current timeout action */
#endif
			mq_send(c->mbx, (const char*)&msg, sizeof(conn_msg), 0);
			return(PASS);
		}
		c = c->next;
	};

	fprintf(stderr, "No free handler\n");

	return(FAIL);
}



/********************************************************************************
  FUNCTION NAME   	: available_handler
  FUNCTION DETAILS  : Returns a count of the available handlers for the
					  specified connection type.

					  On entry:

					  list 		Points to the start of a list of connection
								handlers of the correct type.

********************************************************************************/

int available_handler(connection* list)
{
	int count = 0;
	connection* c;

	c = list;

	while (c) {
		if (c->state == CONN_FREE) {
			count++;
		}
		c = c->next;
	};

	return(count);
}



/********************************************************************************
  FUNCTION NAME   	: handler_status
  FUNCTION DETAILS  : Returns handler status information for the specified
					  connection type.

					  On entry:

					  list 		Points to the start of a list of connection
								handlers of the correct type.

********************************************************************************/

void handler_status(connection* list, int* free, int* busy, int* total)
{
	connection* c;
	*free = 0;
	*busy = 0;
	*total = 0;

	c = list;

	while (c) {
		if (c->state == CONN_FREE)
			(*free)++;
		else
			(*busy)++;

		(*total)++;
		c = c->next;
	};
}
