/********************************************************************************
 * MODULE NAME       : msgserver.c												*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Server for communications messages.						*
 ********************************************************************************/

#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>		// POSIX message queue
#include <pthread.h>
#include <semaphore.h>

#include <netinet/in.h>
#include <netinet/tcp.h>

#include <errno.h>

#include <stdint.h>
#include "porting.h"

 /* The following two lines are required to allow Events.h to compile */

#define SUPGEN
#define Parameter void

#include "server.h"

#include "asmcode.h"
#include "defines.h"
#include "hardware.h"
#include "msgserver.h"
#include "msgstruct.h"
#include "cnet.h"
#include "debug.h"
#include "system.h"
#include "eventrep.h"
#include "eventlog.h"
#include "HardwareIndependent/Events.h"

connection* msg_list = NULL;	/* List of standard message connections		*/
connection* rtmsg_list = NULL;	/* List of realtime message connections		*/
connection* semaphore = NULL;	/* Semaphore for controlled access			*/

void* msg_task(void* conn);
void* rtmsg_task(void* conn);
void* cnet_msg_task(void* semaphore);

int  start_handler(connection* list, int socket);
int  available_handler(connection* list);
void init_cnet_msg_task(void);

/* Dedicated handler for direct realtime transfer */
int skt_transfer_realtime_data(int skt, uint32_t rxlen, connection* conn);

extern int sysmem;

#if !defined(DEFFILEMODE)
#define DEFFILEMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)
#endif

#define NUM_MSG				8 //8		/* Define number of standard msg servers 	*/
#define NUM_RT				1			/* Define number of realtime msg servers	*/

#define RT_TIMESLOTS		4			/* Number of timeslots allocated to 
										   realtime message handler.				*/

#define STD_STREAM_ALLOC	0x180000	/* Streaming buffer allocation for standard sockets */
#define RT_STREAM_ALLOC		0x600000	/* Streaming buffer allocation for realtime socket	*/

#if 0

										   /* Loader has no communications timeout */

#ifdef LOADER
#define DEFAULT_CONN_TIMEOUT	0xFFFFFFFFU	/* Loader default no connection timeout 	*/
#define DEFAULT_CONN_ACTION		0			/* Loader default no connection action	 	*/
#else
#define DEFAULT_CONN_TIMEOUT	30*100		/* Default connection timeout = 300 seconds	*/
											/* No timeout = 0xFFFFFFFFU					*/
#define DEFAULT_CONN_ACTION		0			/* Default no connection action	 			*/
#endif								

#endif

/* Semaphore used to control execution of CNet message handler task. */

static pthread_t cnet_msg_thread;

static int data_true = 1;

/* External table of message handlers */

extern int(* const messagelist[])(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, connection* conn);

#ifdef LOADER
#ifdef SIGNALCUBE

/********************************************************************************
  FUNCTION NAME   	: eventlog_log
  FUNCTION DETAILS  : Dummy event log function for Signal Cube loader use.
********************************************************************************/
uint32_t eventlog_log(uint32_t type, uint32_t p1, uint32_t p2)
{
	return(FALSE);
}

#endif
#endif


/********************************************************************************
  FUNCTION NAME   	: comm_close
  FUNCTION DETAILS  : Force comms channel to close.
********************************************************************************/
int comm_close(uint32_t id)
{
	if (id < (NUM_MSG + NUM_RT)) {
		connection* c = (id >= NUM_MSG) ? &rtmsg_list[id - NUM_MSG] : &msg_list[id];
		c->close = TRUE;
		return(NO_ERROR);
	}
	return(CONN_BADID);
}

/********************************************************************************
  FUNCTION NAME   	: chk_comm_state
  FUNCTION DETAILS  : Check comm state based on returned length and error code.
********************************************************************************/
int chk_comm_state(int len, int errno_val, connection* c, int event)
{
	if ((len == 0) || ((len < 0) && (errno_val == ECONNRESET)) || c->close) {
		if (c->close) {
			eventlog_log(LogTypeCommForceClose, event, 0);
		}
		else if ((errno_val == CONN_TIMEOUT) || (errno_val == CONN_BADCLOSE)) {
#if (defined(CONTROLCUBE) || defined(AICUBE))
			/* Take comms timeout action if programmed */
			// TODO: revisit - add in CtrlWdogOut
//			if (c->action) CtrlWdogOut(CtrlGetActionInfo(c->action));
#endif
			eventlog_log((errno_val == CONN_TIMEOUT) ? LogTypeCommTimeout : LogTypeCommBadClose, event, (errno_val << 16) | (len & 0xFFFF));
		}
		else {
			eventlog_log(LogTypeMsgHandlerEvent, event, (errno_val << 16) | (len & 0xFFFF));
		}
		return(TRUE);
	}
	return(FALSE);
}

/********************************************************************************
  FUNCTION NAME   	: msg_server
  FUNCTION DETAILS  : Create a list of handlers for the msg servers, then
					  wait for connections to be accepted on the listening
					  sockets. When connections are established initiate the
					  operation of the appropriate handler code.

					  On entry:

					  None

********************************************************************************/

void* msg_server(void* arg)
{
	struct sockaddr_in msg_addr;
	int msg;		/* Socket used for msg server connection  */
	int msgsock;	/* Socket used for msg handler connection */
	connection* c;
	
	mqd_t m;		/* POSIX message queue */	//MBX_Handle m;
	pthread_t t;	/* POSIX thread */			//TSK_Handle t;
	struct mq_attr attr;

	int n;
	static const int tx_buffer_size = 32768;
	static const int rx_buffer_size = 32768;

	/* Initialise the msg connection lists */

	//msg_list = MEM_alloc(sysmem, sizeof(connection) * NUM_MSG, 4);
	msg_list = malloc(sizeof(connection) * NUM_MSG);
	if (!msg_list) diag_error(DIAG_MALLOC);

	/* Create the TCP/IP message handler tasks */

	c = msg_list;

	for (n = 0; n < NUM_MSG; n++) {

		//m = MBX_create(sizeof(conn_msg), 1, NULL);
		char messageq_name[16];
		char thread_name[16];
		sprintf(messageq_name, "/mqsrv%d", n);
		sprintf(thread_name, "msg_srv_%d", n);

		attr.mq_msgsize = sizeof(conn_msg);
		attr.mq_maxmsg = 8;


		m = mq_open((const char*)messageq_name, O_RDWR | O_CREAT, 0666, &attr);	/* create new POSIX message queue with unique name */
		if (m == (mqd_t)-1)
		{
			fprintf(stderr, "cannot create message queue\n");
			diag_error(DIAG_MSGSERVER);
		}

		c->id = n;
		c->tsk = 0;
		c->mbx = m;
		c->state = CONN_FREE;						/* Initial state is FREE 			*/
		c->closing = FALSE;						/* Connection not closing			*/
		c->close = FALSE;							/* Clear close flag					*/
		c->timeout = DEFAULT_CONN_TIMEOUT;		/* Default connection timeout  		*/
		c->action = DEFAULT_CONN_ACTION;			/* Default timeout action			*/
		c->timeoutctr = DEFAULT_CONN_TIMEOUT;		/* Initialise timeout counter		*/
#ifndef LOADER
		c->stream = rt_alloc(STD_STREAM_ALLOC); 	/* Allocate RT stream  				*/
		rt_initialise(c->stream, 0);				/* Initialise RT stream				*/
#else
		c->stream = NULL;
#endif
		c->komask = 1 << n;
		c->next = (n < (NUM_MSG - 1)) ? (c + 1) : NULL;

		/* Create the actual message handler task */

		// t = TSK_create((Fxn)msg_task, NULL, c);
		// t = task_create((Fxn)msg_task, NULL, c, 1);
		if (0 != pthread_create(&t, NULL, &msg_task, c))
		{
			fprintf(stderr, "cannot create msg_task thread\n");
			diag_error(DIAG_MSGSERVER);
		}
		pthread_setname_np(t, thread_name);

		c->tsk = t;
		c++;
	}

	/* Initialise the CNet message handler */

	init_cnet_msg_task();

	/* Setup the listening socket for message server requests */

	msg = socket(AF_INET, SOCK_STREAM, 0);
//	msg = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
	if (msg < 0) {
		fprintf(stderr, "Error opening socket for msg server\n");
		diag_error(DIAG_MSGSERVER);
	}

	/*ioctl(msg, FIONBIO, &data_true, NULL);*/
	setsockopt(msg, SOL_SOCKET, SO_REUSEPORT, &data_true, sizeof(data_true));
	setsockopt(msg, IPPROTO_TCP, TCP_NODELAY, &data_true, sizeof(data_true));

	setsockopt(msg, SOL_SOCKET, SO_SNDBUF, (const char*)&tx_buffer_size, sizeof(tx_buffer_size));
	setsockopt(msg, SOL_SOCKET, SO_RCVBUF, (const char*)&rx_buffer_size, sizeof(rx_buffer_size));

	msg_addr.sin_family = AF_INET;
	msg_addr.sin_addr.s_addr = INADDR_ANY;
	msg_addr.sin_port = htons(MSGPORT);

	if (bind(msg, (struct sockaddr*)&msg_addr, sizeof(msg_addr)) < 0)
	{
		fprintf(stderr, "Error binding msg socket = %d\n\r", errno);
		close(msg);
		diag_error(DIAG_MSGSERVER);
	}

	/* Begin accepting connections on message port */

	listen(msg, 5);
	// LOG_printf(&trace, "Msg server running");
	do {
		if (available_handler(msg_list)) {

			/* A spare handler is available, so accept connections on this port */

			msgsock = accept(msg, NULL, NULL);
			printf("accept: %d\n", msgsock);
			if (msgsock < 0) {
				// LOG_printf(&trace, "Msg accept error %d", errno);
				if (errno != ECONNRESET && errno != EAGAIN && errno != EWOULDBLOCK) {
					close(msg);
					return(NULL);
				}
			}
			else {
				if (start_handler(msg_list, msgsock)) {
					// LOG_printf(&trace, "Msg task not found");
					close(msgsock);
				}
				else {
					// LOG_printf(&trace, "Msg connection accepted");
				}
			}
		}
		else
		{
			sleep(1);
		}
	} while (1);
}

#ifndef LOADER
/********************************************************************************
  FUNCTION NAME   	: rtmsg_server
  FUNCTION DETAILS  : Create a handler for the realtime msg server, then
					  wait for connections to be accepted on the listening
					  socket. When connections are established initiate the
					  operation of the appropriate handler code.

					  On entry:

					  None

********************************************************************************/

void* rtmsg_server(void* arg)
{
	struct sockaddr_in msg_addr;
	int msg;		/* Socket used for msg connection */
	int msgsock;
	connection* c;
	mqd_t m;		/* POSIX message queue */	//MBX_Handle m;
	pthread_t t;	/* POSIX thread */			//TSK_Handle t;
	int n;
	struct mq_attr attr;

	static const int tx_buffer_size = 65536 * 5;
	static const int rx_buffer_size = 65536 * 5;

	/* Initialise the msg connection list */

	//rtmsg_list = MEM_alloc(sysmem, sizeof(connection) * NUM_RT, 4);
	rtmsg_list = malloc(sizeof(connection) * NUM_RT);
	if (!rtmsg_list) diag_error(DIAG_MALLOC);

	/* Create the TCP/IP message handler task */

	c = rtmsg_list;

	for (n = NUM_MSG; n < (NUM_MSG + NUM_RT); n++) {

		char messageq_name[16];
		char thread_name[16];
		sprintf(messageq_name, "/mqsrv%d", n);
		sprintf(thread_name, "rtmsg_srv_%d", n-NUM_MSG);

		attr.mq_msgsize = sizeof(conn_msg);
		attr.mq_maxmsg = 8;

		m = mq_open((const char*)messageq_name, O_RDWR | O_CREAT, 0666, &attr);	/* create new POSIX message queue with unique name */
		if (m == (mqd_t)-1)
		{
			fprintf(stderr, "cannot create message queue\n");
			diag_error(DIAG_MSGSERVER);
		}

		// m = MBX_create(sizeof(conn_msg), 1, NULL);

		c->id = n;
		c->tsk = 0;
		c->mbx = m;
		c->state = CONN_FREE;						/* Initial state is FREE 			*/
		c->closing = FALSE;						/* Connection not closing			*/
		c->close = FALSE;							/* Clear close flag					*/
		c->timeout = DEFAULT_CONN_TIMEOUT;		/* Default connection timeout   	*/
		c->action = DEFAULT_CONN_ACTION;			/* Default timeout action			*/
		c->timeoutctr = DEFAULT_CONN_TIMEOUT;		/* Initialise timeout counter		*/
		c->stream = rt_alloc(RT_STREAM_ALLOC);	/* Allocate RT stream  				*/
		rt_initialise(c->stream, 0);				/* Initialise RT stream				*/
		c->komask = 1 << n;
		c->next = (n < (NUM_MSG + NUM_RT - 1)) ? (c + 1) : NULL;

		/* Create the actual message handler task */

	  //  t = TSK_create((Fxn)rtmsg_task, NULL, c);
		//t = task_create((Fxn)rtmsg_task, NULL, c, RT_TIMESLOTS);
		if (0 != pthread_create(&t, NULL, &rtmsg_task, c))
		{
			fprintf(stderr, "cannot create msg_task thread\n");
			diag_error(DIAG_MSGSERVER);
		}
		pthread_setname_np(t, thread_name);

		c->tsk = t;
		c++;
	}

	/* Setup the listening socket for realtime message server requests */

	msg = socket(AF_INET, SOCK_STREAM, 0);
	if (msg < 0) {
		fprintf(stderr, "Error opening socket for rt msg server\n\r");
		return(NULL);
	}

	// socketioctl(msg, FIONBIO, &data_true, NULL);
	setsockopt(msg, SOL_SOCKET, SO_REUSEPORT, &data_true, sizeof(data_true));

	setsockopt(msg, SOL_SOCKET, SO_SNDBUF, (const char*)&tx_buffer_size, sizeof(tx_buffer_size));
	setsockopt(msg, SOL_SOCKET, SO_RCVBUF, (const char*)&rx_buffer_size, sizeof(rx_buffer_size));

	msg_addr.sin_family = AF_INET;
	msg_addr.sin_addr.s_addr = INADDR_ANY;
	msg_addr.sin_port = htons(RTPORT);

	if (bind(msg, (struct sockaddr*)&msg_addr, sizeof(msg_addr)) < 0)
	{
		fprintf(stderr, "Error binding rt msg socket = %d\n\r", errno);
		// LOG_printf(&trace, "Error binding rt msg socket = %d", errno);
		close(msg);
		return(NULL);
	}

	/* Begin accepting connections on message port */

	listen(msg, 5);
	do {
		if (available_handler(rtmsg_list)) {

			/* A spare handler is available, so accept connections on this port */

			msgsock = accept(msg, NULL, NULL);
			printf("accept (rt): %d\n", msgsock);
			if (msgsock < 0) {
				if (errno != ECONNRESET) {
					printf("close (rt): %d\n", msgsock);
					close(msg);
					return(NULL);
				}
			}
			else {
				if (start_handler(rtmsg_list, msgsock)) {
					printf("close (rt): %d\n", msgsock);
					close(msgsock);
				}
				else {
					printf("msgserver (rt): connection accepted\n");
				}
			}
		}
		else
		{
			sleep(1);
		}
	} while (1);
}

#endif



extern connection* telnet_list;

/********************************************************************************
  FUNCTION NAME   	: serverstat
  FUNCTION DETAILS  : Show message server status.
********************************************************************************/
void serverstat(char* args[], int numargs, int skt)
{
	// not required for virtual cube
}

/********************************************************************************
  FUNCTION NAME   	: error_illegal_channel
  FUNCTION DETAILS  : Build an ILLEGAL CHANNEL response message.
********************************************************************************/
int error_illegal_channel(uint8_t* reply, uint32_t* replylength, uint32_t chan)
{
	struct msg_illegal_channel* r;

	r = (struct msg_illegal_channel*)reply;

	r->command = MSG_ERROR_ILLEGAL_CHANNEL;
	r->chan = chan;

	*replylength = offsetof(struct msg_illegal_channel, end);
	return(ERROR_ILLEGAL_CHANNEL);
}

/********************************************************************************
  FUNCTION NAME   	: error_illegal_function
  FUNCTION DETAILS  : Build an ILLEGAL FUNCTION response message.
********************************************************************************/
int error_illegal_function(uint8_t* reply, uint32_t* replylength)
{
	struct msg_illegal_function* r;

	r = (struct msg_illegal_function*)reply;

	r->command = MSG_ERROR_ILLEGAL_FUNCTION;

	*replylength = offsetof(struct msg_illegal_function, end);
	return(ERROR_ILLEGAL_FUNCTION);
}

/********************************************************************************
  FUNCTION NAME   	: error_operation_failed
  FUNCTION DETAILS  : Build an OPERATION FAILED response message.
********************************************************************************/
int error_operation_failed(uint8_t* reply, uint32_t* replylength, uint32_t reason)
{
	struct msg_operation_failed* r;

	r = (struct msg_operation_failed*)reply;

	r->command = MSG_ERROR_OPERATION_FAILED;
	r->reason = reason;

	*replylength = offsetof(struct msg_operation_failed, end);
	return(ERROR_OPERATION_FAILED);
}

/********************************************************************************
  FUNCTION NAME   	: error_illegal_parameter
  FUNCTION DETAILS  : Build an ILLEGAL PARAMETER response message.
********************************************************************************/
int error_illegal_parameter(uint8_t* reply, uint32_t* replylength)
{
	struct msg_illegal_parameter* r;

	r = (struct msg_illegal_parameter*)reply;

	r->command = MSG_ERROR_ILLEGAL_PARAMETER;

	*replylength = offsetof(struct msg_illegal_parameter, end);
	return(ERROR_ILLEGAL_PARAMETER);
}

/********************************************************************************
  FUNCTION NAME   	: error_no_stream
  FUNCTION DETAILS  : Build a NO STREAM response message.
********************************************************************************/
int error_no_stream(uint8_t* reply, uint32_t* replylength)
{
	struct msg_no_stream* r;

	r = (struct msg_no_stream*)reply;

	r->command = MSG_ERROR_NO_STREAM;

	*replylength = offsetof(struct msg_no_stream, end);
	return(ERROR_NO_STREAM);
}

/********************************************************************************
  FUNCTION NAME   	: error_too_long
  FUNCTION DETAILS  : Build a TOO LONG response message.
********************************************************************************/
int error_too_long(uint8_t* reply, uint32_t* replylength)
{
	struct msg_too_long* r;

	r = (struct msg_too_long*)reply;

	r->command = MSG_ERROR_TOO_LONG;

	*replylength = offsetof(struct msg_too_long, end);
	return(ERROR_TOO_LONG);
}

/********************************************************************************
  FUNCTION NAME   	: error_fw_corrupt
  FUNCTION DETAILS  : Build a FW CORRUPT response message.
********************************************************************************/
int error_fw_corrupt(uint8_t* reply, uint32_t* replylength)
{
	struct msg_fw_corrupt* r;

	r = (struct msg_fw_corrupt*)reply;

	r->command = MSG_ERROR_FW_CORRUPT;

	*replylength = offsetof(struct msg_fw_corrupt, end);
	return(ERROR_FW_CORRUPT);
}

/********************************************************************************
  FUNCTION NAME   	: error_security_level
  FUNCTION DETAILS  : Build a SECURITY_LEVEL response message.
********************************************************************************/
int error_security_level(uint8_t* reply, uint32_t* replylength)
{
	struct msg_security_level* r;

	r = (struct msg_security_level*)reply;

	r->command = MSG_ERROR_SECURITY_LEVEL;

	*replylength = offsetof(struct msg_security_level, end);
	return(ERROR_SECURITY_LEVEL);
}

/********************************************************************************
  FUNCTION NAME   	: error_semaphore_denied
  FUNCTION DETAILS  : Build a SEMAPHORE_DENIED response message.
********************************************************************************/
int error_semaphore_denied(uint8_t* reply, uint32_t* replylength)
{
	struct msg_semaphore_denied* r;

	r = (struct msg_semaphore_denied*)reply;

	r->command = MSG_ERROR_SEMAPHORE_DENIED;

	*replylength = offsetof(struct msg_semaphore_denied, end);
	return(ERROR_SEMAPHORE_DENIED);
}

/********************************************************************************
  FUNCTION NAME   	: process_maxmin
  FUNCTION DETAILS  : Process request for max/min values in message.
********************************************************************************/
int process_maxmin(struct msg_generic_message* m, void* r, void* max, void* min, uint32_t size, uint32_t msg)
{
	if (((struct generic_maxmin*)m)->msginfo & MSGINFO_MAX) {
		memcpy(r, max, size);
		*((uint16_t*)r) = msg;
	}
	else if (((struct generic_maxmin*)m)->msginfo & MSGINFO_MIN) {
		memcpy(r, min, size);
		*((uint16_t*)r) = msg;
	}
	return(NO_ERROR);
}

/********************************************************************************
  FUNCTION NAME   	: msg_task
  FUNCTION DETAILS  : Performs the message handler operation.

					  On entry:

					  conn	Points to the connection definition

********************************************************************************/
void* msg_task(void* c)
{
	connection* conn = (connection*)c;
	conn_msg msg;
	int msgsock;
	uint8_t* msgbuffer;
	uint8_t* replybuffer;
	int len;
	uint8_t* msgptr;
	uint16_t msgcode;
	uint32_t msglength;
	int msgerr;
	uint8_t* replyptr;
	uint32_t replylength;
	unsigned int sent;
	int istate;
//	int errno = NO_ERROR;

	struct timeval tv;

	/* Define pointer to message handler code */

	int (*msghandler)(uint8_t * msg, uint8_t * reply, uint32_t * msglength, uint32_t * replyhandler, connection * conn);

	int msg_rx = 0;

	uint8_t* msgbuf;  	/* Start of message buffer 	*/
	uint8_t* msgend;	/* End of current packet	*/

	static const int tx_buffer_size = DEFAULT_REPLYBUFFER;
	static const int rx_buffer_size = DEFAULT_MSGBUFFER;

	commhdr* hdr;

	msgbuffer = malloc(DEFAULT_MSGBUFFER);	// MEM_alloc(sysmem, DEFAULT_MSGBUFFER, 4);
	replybuffer = malloc(DEFAULT_REPLYBUFFER);	// MEM_alloc(sysmem, DEFAULT_REPLYBUFFER, 4);

	/* If unable to claim memory then show diagnostic and halt */
	if (!msgbuffer || !replybuffer) diag_error(DIAG_MALLOC);

	while (1) {
		struct mq_attr attr;
		mq_getattr(conn->mbx, &attr);
		errno = 0;
		len = mq_receive(conn->mbx, (char*)&msg, sizeof(msg), NULL); // MBX_pend(conn->mbx, &msg, SYS_FOREVER);
		eventlog_log(LogTypeMsgHandlerOpen, 1, 0);

		msgsock = msg.socket; 			/* Get socket from mailbox 		*/
		tv.tv_sec = conn->timeout;		/* get timeout */
		conn->closing = FALSE;			/* Connection not closing		*/
		conn->close = FALSE;				/* Clear the close flag			*/
		msg_rx = 0;
		printf("msg_task: start %d\n", conn->id);
//		errno = NO_ERROR;

#ifndef LOADER

		/* Initialise realtime parameters */

		rt_initialise(conn->stream, 0);

#endif

		/* Enable non-blocking operation, set keepalive option and inhibit
		   delayed ACK operation.
		*/

		// best place to set non-blocking socket, where is the socket created anyway?
		// fcntl(msgsock, F_SETFL, fcntl(msgsock, F_GETFL, 0) | O_NONBLOCK);
		//socketioctl(msgsock, FIONBIO, &data_true, NULL);
		
		setsockopt(msgsock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
		setsockopt(msgsock, SOL_SOCKET, SO_KEEPALIVE, &data_true, sizeof(data_true));
		setsockopt(msgsock, IPPROTO_TCP, TCP_NODELAY, &data_true, sizeof(data_true));

		setsockopt(msgsock, SOL_SOCKET, SO_SNDBUF, (const char*)&tx_buffer_size, sizeof(tx_buffer_size));
		setsockopt(msgsock, SOL_SOCKET, SO_RCVBUF, (const char*)&rx_buffer_size, sizeof(rx_buffer_size));
		
		/* Receive and process packets from the client */
		while (1) {
			/* use SO_RCVTIMEO for a blocking but timed recv */
			len = recv(msgsock, msgbuffer, sizeof(commhdr), 0);
			if (chk_comm_state(len, errno, conn, 1)) break;

			/* Now have packet header */
			hdr = (commhdr*)msgbuffer;
			msg_rx++;

			msgbuf = msgbuffer + sizeof(commhdr);

			/* Check if message can be processed locally or must be sent via CNet */

			if ((hdr->dst == cnet_local_addr) || (hdr->dst == CNET_ASYNC_LOCAL)) {

				/* Local message, so process all messages in the packet building a reply packet */

				replyptr = replybuffer + sizeof(commhdr);	/* Point to start of reply buffer */

				/* Read the message code (2 bytes) */
				len = recv(msgsock, msgbuf, 2, 0);
				if (chk_comm_state(len, errno, conn, 2)) break;

				fpled_ctrl(LED_PULSE);

				/* Check if message is a realtime transfer message. This is a special case to reduce the
				   number of memory copies required. If the first message is a MSG_TRANSFER_REALTIME_DATA message,
				   then the usual copy of message data to a buffer for processing is bypassed and data is read
				   directly from the socket by a special realtime transfer function.
				*/

				if (*((uint16_t*)msgbuf) == MSG_TRANSFER_REALTIME_DATA) {
#ifndef LOADER
					skt_transfer_realtime_data(msgsock, (hdr->length) - 2, conn);
#else
					goto msg_illegal;
#endif
				}
				else {

					/* Check for excessive length */

					if (hdr->length > (DEFAULT_MSGBUFFER - sizeof(commhdr))) {
						error_too_long(replyptr, &replylength);
						replyptr += replylength;
						goto skip_packet;
					}

					/* Read the remainder of the packet */

					len = recv(msgsock, (msgbuf + 2), (hdr->length - 2), 0);
					if (chk_comm_state(len, errno, conn, 3)) break;

					msgptr = msgbuf;					/* Point to start of message buffer */
					msgend = msgbuf + (hdr->length);	/* Point to end of current packet	*/

					while (msgptr < msgend) {

						msgcode = *((uint16_t*)msgptr);

						/* Check if message number is legal and lookup message handler in table */
						msghandler = (msgcode <= MAX_MSG_CODE) ? messagelist[msgcode] : NULL;

						if (msghandler) {
							replylength = 0;
							msgerr = (*msghandler)(msgptr, replyptr, &msglength, &replylength, conn);

							/* If an error was detected abort further message processing within packet */

							if (msgerr) {
								struct generic_error* r;

								r = (struct generic_error*)replyptr;

								/* Insert error position into response message */

								r->pos = msgptr - msgbuf;
							}

							msgptr += msglength;
							replyptr += replylength;

							if (msgerr) break;

						}
						else {

							struct msg_illegal_message r;

#ifdef LOADER
							msg_illegal :
#endif

							/* Illegal message, so build error response and abort processing */

							r.command = MSG_ERROR_ILLEGAL_MESSAGE;
							r.pos = msgptr - msgbuf;

							memcpy(replyptr, &r, sizeof(struct msg_illegal_message));
							msgptr += sizeof(uint16_t);										/* Added SN 09/07/08 */
							replyptr += offsetof(struct msg_illegal_message, end);			/* Added SN 09/07/08 */

							break;
						}
					}

					((commhdr*)replybuffer)->dst = 0;
					((commhdr*)replybuffer)->length = (replyptr - replybuffer) - sizeof(commhdr);

					len = send(msgsock, replybuffer, (replyptr - replybuffer), 0);
				}
			}
			else {

				/* Remote message, so send via CNet */

				if (hdr->length > (DEFAULT_MSGBUFFER - sizeof(commhdr))) {
					error_too_long(replyptr, &replylength);
					replyptr += replylength;
					goto skip_packet;
				}

				/* Read the remainder of the packet */

				len = recv(msgsock, msgbuf, hdr->length, 0);
				msg_rx++;
				if (chk_comm_state(len, errno, conn, 4)) break;

				msglength = hdr->length + sizeof(commhdr);
				hdr->pkttype = PKT_MSG;
				msgerr = cnet_async_transmit(msgbuffer, replybuffer, msglength, &replylength, FALSE);
				if (msgerr) {
					switch (msgerr) {
					case ERROR_CNET_TIMEOUT: {
						struct msg_cnet_timeout r;
						r.command = MSG_ERROR_CNET_TIMEOUT;
						r.pos = 0;
						memcpy((replybuffer + sizeof(commhdr)), &r, sizeof(struct msg_cnet_timeout));
						replylength = offsetof(struct msg_cnet_timeout, end);
					}
										   break;
					case ERROR_CNET_DESTINATION: {
						struct msg_illegal_destination r;
						r.command = MSG_ERROR_ILLEGAL_DESTINATION;
						r.dst = hdr->dst;
						r.pos = 0;
						memcpy((replybuffer + sizeof(commhdr)), &r, sizeof(struct msg_illegal_destination));
						replylength = offsetof(struct msg_illegal_destination, end);
					}
											   break;
					}
				}

			skip_packet:
				((commhdr*)replybuffer)->dst = 0;
				((commhdr*)replybuffer)->length = replylength;

				len = send(msgsock, replybuffer, (replylength + sizeof(commhdr)), 0);

			}
		}
		eventlog_log(LogTypeMsgHandlerClose, 1, 0);

		/* Connection has been closed. Close the associated socket and update connection state. */

		close(msgsock);
		conn->state = CONN_FREE;

#ifndef LOADER

		/* If the connection has not been flagged as closing, then this is probably an error
		   condition caused by the connection being forcibly closed. So take any programmed
		   timeout action.
		*/

		if (!conn->closing) {
			chk_comm_state(0, CONN_BADCLOSE, conn, 8);
		}

#endif

		/* If this connection owns the semaphore, release it immediately */

		istate = disable_int();
		if (semaphore == conn) semaphore = NULL;
		restore_int(istate);

#ifndef LOADER

		/* Update any associated event connections */

		event_connection_closing(conn);

#endif
	}
}



#ifndef LOADER

#define DIRECT_TRANSFER

/********************************************************************************
  FUNCTION NAME   	: rtmsg_task
  FUNCTION DETAILS  : Performs the realtime message handler operation.

					  On entry:

					  conn	Points to the connection definition

********************************************************************************/

void* rtmsg_task(void* c)
{
	connection* conn = (connection*)c;
	conn_msg msg;
	int msgsock;
	uint8_t* msgbuffer;
	uint8_t* replybuffer;
	int len;
	uint8_t* msgptr;
	uint16_t msgcode;
	uint32_t msglength;
	int msgerr;
	uint8_t* replyptr;
	uint32_t replylength;
	unsigned int sent;
	int istate;

	struct timeval tv;

	/* Define pointer to message handler code */

	int (*msghandler)(uint8_t * msg, uint8_t * reply, uint32_t * msglength, uint32_t * replyhandler, connection * conn);

	int msg_rx = 0;

	uint8_t* msgbuf;  	/* Start of message buffer 	*/
	uint8_t* msgend;	/* End of current packet	*/

	commhdr* hdr;

	msgbuffer = malloc(DEFAULT_RTMSGBUFFER);	// MEM_alloc(sysmem, DEFAULT_RTMSGBUFFER, 4);
	replybuffer = malloc(DEFAULT_RTREPLYBUFFER);	// MEM_alloc(sysmem, DEFAULT_RTREPLYBUFFER, 4);

	/* If unable to claim memory then show diagnostic and halt task */

	if (!msgbuffer || !replybuffer) diag_error(DIAG_MALLOC);

	while (1) {

		mq_receive(conn->mbx, (char*)&msg, sizeof(msg), NULL);
//		MBX_pend(conn->mbx, &msg, SYS_FOREVER);

		eventlog_log(LogTypeMsgHandlerOpen, 2, 0);

		msgsock = msg.socket; 			/* Get socket from mailbox 		*/
		tv.tv_sec = conn->timeout;
		conn->closing = FALSE;			/* Connection not closing		*/
		conn->close = FALSE;				/* Clear the close flag			*/
		msg_rx = 0;
		errno = NO_ERROR;
		printf("rtmsg_task: start %d\n", conn->id);

		/* Initialise realtime parameters */

		rt_initialise(conn->stream, 0);

		/* Enable non-blocking operation and set keepalive option */
		setsockopt(msgsock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
		setsockopt(msgsock, SOL_SOCKET, SO_KEEPALIVE, &data_true, sizeof(data_true));
		setsockopt(msgsock, IPPROTO_TCP, TCP_NODELAY, (const char*)&data_true, sizeof(int));

		/* Receive and process packets from the client */
		while (1) {
			usleep(10000);
			len = recv(msgsock, msgbuffer, sizeof(commhdr), 0);
			if (chk_comm_state(len, errno, conn, 5)) break;

			fpled_ctrl(LED_PULSE);

			/* Now have packet header */

			hdr = (commhdr*)msgbuffer;
			msg_rx++;

			msgbuf = msgbuffer + sizeof(commhdr);

			replyptr = replybuffer + sizeof(commhdr);	/* Point to start of reply buffer */

			/* Read the message code (2 bytes) */
			len = recv(msgsock, msgbuf, 2, 0);
			if (chk_comm_state(len, errno, conn, 6)) break;

			/* Check if message is a realtime transfer message. This is a special case to reduce the
			   number of memory copies required. If the first message is a MSG_TRANSFER_REALTIME_DATA message,
			   then the usual copy of message data to a buffer for processing is bypassed and data is read
			   directly from the socket by a special realtime transfer function.
			*/

			if (*((uint16_t*)msgbuf) == MSG_TRANSFER_REALTIME_DATA) {
				skt_transfer_realtime_data(msgsock, (hdr->length) - 2, conn);
			}
			else {

				/* Read the remainder of the packet */

				if (hdr->length > (DEFAULT_RTMSGBUFFER - sizeof(commhdr))) {
					error_too_long(replyptr, &replylength);
					replyptr += replylength;
					goto skip_packet;
				}

				len = recv(msgsock, (msgbuf + 2), (hdr->length - 2), 0);
				if (chk_comm_state(len, errno, conn, 7)) break;

				/* Destination is ignored for realtime messages, they are always processed locally */

				msgptr = msgbuf;					/* Point to start of message buffer */
				msgend = msgbuf + (hdr->length);	/* Point to end of current packet	*/

				while (msgptr < msgend) {

					msgcode = *((uint16_t*)msgptr);

					/* Check if message number is legal and lookup message handler in table */

					msghandler = (msgcode <= MAX_MSG_CODE) ? messagelist[msgcode] : NULL;

					if (msghandler) {
						replylength = 0;
						msgerr = (*msghandler)(msgptr, replyptr, &msglength, &replylength, conn);

						/* If an error was detected abort further message processing within packet */

						if (msgerr) {
							struct generic_error* r;

							r = (struct generic_error*)replyptr;

							/* Insert error position into response message */

							r->pos = msgptr - msgbuf;
						}

						msgptr += msglength;
						replyptr += replylength;

						if (msgerr) break;

					}
					else {

						struct msg_illegal_message r;

						/* Illegal message, so build error response and abort processing */

						r.command = MSG_ERROR_ILLEGAL_MESSAGE;
						r.pos = msgptr - msgbuf;

						memcpy(replyptr, &r, sizeof(struct msg_illegal_message));
						msgptr += sizeof(uint16_t);											/* Added SN 09/07/08 */
						replyptr += offsetof(struct msg_illegal_message, end);			/* Added SN 09/07/08 */

						break;
					}
				}

			skip_packet:
				((commhdr*)replybuffer)->dst = 0;
				((commhdr*)replybuffer)->length = (replyptr - replybuffer) - sizeof(commhdr);

				len = send(msgsock, replybuffer, (replyptr - replybuffer), 0);
			}
		}
		eventlog_log(LogTypeMsgHandlerClose, 2, 0);

		/* Connection has been closed. Close the associated socket and update connection state. */

		close(msgsock);
		conn->state = CONN_FREE;

#ifndef LOADER

		/* If the connection has not been flagged as closing, then this is probably an error
		   condition caused by the connection being forcibly closed. So take any programmed
		   timeout action.
		*/

		if (!conn->closing) {
			chk_comm_state(0, CONN_BADCLOSE, conn, 8);
		}

#endif

		/* If this connection owns the semaphore, release it immediately */

		istate = disable_int();
		if (semaphore == conn) semaphore = NULL;
		restore_int(istate);

	}
}
#endif /* LOADER */

/********************************************************************************
  FUNCTION NAME   	: init_cnet_msg_task
  FUNCTION DETAILS  : Initialise message handler for the CNet async comms
					  function.
********************************************************************************/
sem_t* cnet_msg_sem;
void init_cnet_msg_task(void)
{
	uint8_t* msgbuffer;
	uint8_t* replybuffer;
	
	/* Allocate space for message buffers */

	msgbuffer = malloc(DEFAULT_MSGBUFFER);	// MEM_alloc(sysmem, DEFAULT_MSGBUFFER, 4);
	replybuffer = malloc(DEFAULT_REPLYBUFFER);	// MEM_alloc(sysmem, DEFAULT_REPLYBUFFER, 4);

	/* If unable to claim memory then show diagnostic and halt task */

	if (!msgbuffer || !replybuffer) diag_error(DIAG_MALLOC);

	/* Configure the CNet incoming message structure */

	cnet_rx_msg.rxbuf = msgbuffer;
	cnet_rx_msg.txbuf = replybuffer;
	cnet_rx_msg.iprdy = 0;
	cnet_rx_msg.oprdy = 0;

	cnet_msg_sem = sem_open("/cnetsemmsg", O_CREAT | O_RDONLY, DEFFILEMODE, 0);
	if (cnet_msg_sem == SEM_FAILED)
	{
		fprintf(stderr, "Error creating cnet message semaphore (%d)\n", errno);
	}

	/* Create the CNet message handler task */
	pthread_create(&cnet_msg_thread, NULL, &cnet_msg_task, cnet_msg_sem);
	pthread_setname_np(cnet_msg_thread, "cnet_msg_thread");
}

/********************************************************************************
  FUNCTION NAME   	: cnet_msg_task
  FUNCTION DETAILS  : Performs the message handler operation for the CNet async
					  comms function.
********************************************************************************/
void* cnet_msg_task(void* semaphore)
{
	sem_t* msg_ready_semaphore = (sem_t*)semaphore;
	uint8_t* msgbuffer;
	uint8_t* replybuffer;

	uint8_t* msgptr;
	uint16_t msgcode;
	uint32_t msglength;
	int msgerr;
	uint8_t* replyptr;
	uint32_t replylength;

	/* Define pointer to message handler code */

	int (*msghandler)(uint8_t * msg, uint8_t * reply, uint32_t * msglength, uint32_t * replyhandler, connection * conn);

	uint8_t* msgbuf;  	/* Start of message buffer 	*/
	uint8_t* msgend;	/* End of current packet	*/

	commhdr* hdr;

	while (1) {
		int r;
		do {
			r = sem_wait(msg_ready_semaphore);
		} while (r != 0);

		fpled_ctrl(LED_PULSE);

		msgbuffer = cnet_rx_msg.rxbuf;
		replybuffer = cnet_rx_msg.txbuf;

		/* Now have packet header */

		hdr = (commhdr*)msgbuffer;
		msgbuf = msgbuffer + sizeof(commhdr);

		/* Process all messages in the packet building a reply packet */

		msgptr = msgbuf;					/* Point to start of message buffer */
		msgend = msgbuf + (hdr->length);	/* Point to end of current packet	*/

		replyptr = replybuffer + sizeof(commhdr);	/* Point to start of reply buffer */

		while (msgptr < msgend) {

			msgcode = *((uint16_t*)msgptr);

			/* Check if message number is legal and lookup message handler in table */

			msghandler = (msgcode <= MAX_MSG_CODE) ? messagelist[msgcode] : NULL;

			if (msghandler) {
				replylength = 0;
				msgerr = (*msghandler)(msgptr, replyptr, &msglength, &replylength, NULL);

				/* If an error was detected abort further message processing within packet */

				if (msgerr) {
					struct generic_error* r;

					r = (struct generic_error*)replyptr;

					/* Insert error position into response message */

					r->pos = msgptr - msgbuf;
				}

				msgptr += msglength;
				replyptr += replylength;

				if (msgerr) break;
			}
			else
			{
				struct msg_illegal_message r;

				/* Illegal message, so build error response and abort processing */
				r.command = MSG_ERROR_ILLEGAL_MESSAGE;
				r.pos = msgptr - msgbuf;

				memcpy(replyptr, &r, sizeof(struct msg_illegal_message));
				msgptr += sizeof(uint16_t);											/* Added SN 09/07/08 */
				replyptr += offsetof(struct msg_illegal_message, end);
				break;
			}
		}

		((commhdr*)replybuffer)->dst = 0;
		((commhdr*)replybuffer)->length = (replyptr - replybuffer) - sizeof(commhdr);

		cnet_rx_msg.txsize = replyptr - replybuffer;
		cnet_rx_msg.oprdy = TRUE;
	}
	return(NULL);
}

/********************************************************************************
  FUNCTION NAME   	: timeout_handler
  FUNCTION DETAILS  : Perform timeout checks for the server connections.
********************************************************************************/
void timeout_handler(void)
{
	// not required any more as timeout is handled within POSIX sockets
	//connection* c;

	///* Check for timeout on the standard message handlers */

	//c = msg_list;

	//while (c) {
	//	if (c->state != CONN_FREE) {
	//		if (c->timeoutctr != 0xFFFFFFFFU) c->timeoutctr--;
	//	}
	//	c = c->next;
	//};

	///* Check for timeout on the realtime message handlers */

	//c = rtmsg_list;

	//while (c) {
	//	if (c->state != CONN_FREE) {
	//		if (c->timeoutctr != 0xFFFFFFFFU) c->timeoutctr--;
	//	}
	//	c = c->next;
	//};
}
