/********************************************************************************
 * MODULE NAME       : hw_kinet.c												*
 * MODULE DETAILS    : Interface routines for KiNet card.						*
 *																				*
 ********************************************************************************/

#include <stdlib.h>
#include <string.h>
 //#include <log.h>

#include <stdint.h>
#include "porting.h"

#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"
#include "hw_kinet.h"
#include "debug.h"

//#include <hwi.h>

/* Scale factors for CNet/KiNet transfers */

#define KINET_OUTPUT_SCALE 	20000.0F
#define KINET_INPUT_SCALE 	(1/KINET_OUTPUT_SCALE)

/* Pointer to KiNet definition structure */

kinetdef* kinetptr = NULL;

kinetdef default_kinetdef = {
	NULL,							/* No hardware				*/
	0x01,							/* KiNet master, disabled 	*/
	KINET_WATCHDOG_DISABLE,			/* Watchdog disabled		*/
	0,								/* No KiNet input mappings	*/
	0,								/* No KiNet output mappings	*/
	&hw_kinet_set_mode,
	&hw_kinet_get_mode,
	&hw_kinet_set_dirn,
	&hw_kinet_get_dirn,
	&hw_kinet_set_data,
	&hw_kinet_get_data,
	&hw_kinet_map_slot,
	&hw_kinet_unmap_slot,
	&hw_kinet_read_slot_mapping,
	&hw_kinet_async_transfer
};

FK3_DATA_FORMAT async_data;



/********************************************************************************
  FUNCTION NAME     : hw_kinet_i2c_restart
  FUNCTION DETAILS  : Send I2C restart state.
********************************************************************************/

void hw_kinet_i2c_restart(hw_kinet* hw)
{
	hw->u.wr.sda = 1;			/* Release SDA high */
	delay_clk(25);				/* Quarter bit delay */
	hw->u.wr.scl = 1;			/* Release SCL high */
	delay_clk(25);				/* Quarter bit delay */
	hw->u.wr.sda = 0;			/* Pull SDA low */
	delay_clk(25);				/* Quarter bit delay */
	hw->u.wr.scl = 0;			/* Pull SCL low */
	delay_clk(25);				/* Quarter bit delay */
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_i2c_start
  FUNCTION DETAILS  : Send I2C start state.
********************************************************************************/

void hw_kinet_i2c_start(hw_kinet* hw)
{
	hw->u.wr.sda = 0;			/* Pull SDA low */
	delay_clk(25);				/* Quarter bit delay */
	hw->u.wr.scl = 0;			/* Pull SCL low */
	delay_clk(25);				/* Quarter bit delay */
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_i2c_stop
  FUNCTION DETAILS  : Generate I2C stop condition
********************************************************************************/

void hw_kinet_i2c_stop(hw_kinet* hw)
{
	hw->u.wr.scl = 1;			/* Release SCL high */
	delay_clk(25);				/* Quarter bit delay */
	hw->u.wr.sda = 1;			/* Release SDA high */
	delay_clk(25);				/* Quarter bit delay */
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_i2c_send
  FUNCTION DETAILS  : Send I2C data byte.

					  Returns 0 if the byte has been acknowledged correctly.

********************************************************************************/

int hw_kinet_i2c_send(hw_kinet* hw, uint8_t data)
{
	int bitcount = 8;
	int ack;

	while (bitcount) {
		delay_clk(25);					/* Quarter bit delay */
		if (data & 0x80) {
			hw->u.wr.sda = 1;			/* Release SDA high */
		}
		else {
			hw->u.wr.sda = 0;			/* Pull SDA low */
		}
		delay_clk(25);					/* Quarter bit delay */
		hw->u.wr.scl = 1;				/* Release SCL high */
		delay_clk(50);					/* Half bit delay */
		hw->u.wr.scl = 0;				/* Pull SCL low */
		data = data << 1;				/* Shift next bit into position */
		bitcount--;						/* Count data bits */
	}

	/* Now generate clock pulse for acknowledge bit and test if receiver has
	   acknowledged correctly.
	*/

	delay_clk(25);				/* Quarter bit delay */
	hw->u.wr.sda = 1;			/* Release SDA high */
	delay_clk(25);				/* Quarter bit delay */
	hw->u.wr.scl = 1;			/* Release SCL high */
	delay_clk(50);				/* Half bit delay */

	/* Test for acknowledge from receiver */

	ack = hw->u.rd.sda & 0x01;

	hw->u.wr.scl = 0;		/* Pull SCL low */
	delay_clk(25);			/* Quarter bit delay */
	hw->u.wr.sda = 0;		/* Pull SDA low */
	delay_clk(25);			/* Quarter bit delay */

	return(ack);
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_i2c_read
  FUNCTION DETAILS  : Read I2C data byte.

					  Returns received data byte.

					  On entry:

					  ack	indicates if an acknowledgement should be sent

********************************************************************************/

uint8_t hw_kinet_i2c_read(hw_kinet* hw, int ack)
{
	int bitcount = 8;
	uint8_t data = 0;

	hw->u.wr.sda = 1;						/* Release SDA high */
	while (bitcount) {
		delay_clk(25);						/* Quarter bit delay */
		hw->u.wr.scl = 1;					/* Release SCL high */
		delay_clk(25);						/* Half it delay */
		data = data << 1;					/* Make space for received data bit */
		data |= hw->u.rd.sda & 0x01;		/* Insert new data bit */
		hw->u.wr.scl = 0;					/* Pull SCL low */
		delay_clk(25);						/* Quarter bit delay */
		bitcount--;							/* Count data bits */
	}

	/* Now generate acknowledge bit */

	if (ack) hw->u.wr.sda = 0;				/* Pull SDA low if ack required */

	delay_clk(25);							/* Quarter bit delay */
	hw->u.wr.scl = 1;						/* Release SCL high */
	delay_clk(25);							/* Half bit delay */
	hw->u.wr.scl = 0;						/* Pull SCL low */
	delay_clk(25);							/* Quarter bit delay */

	return(data);
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_set_mode
  FUNCTION DETAILS  : Set KiNet operating mode.

					  On entry:

					  mode	KiNet operation mode
							  Bit 0 KiNet master
							  Bit 1 KiNet enable
********************************************************************************/

void hw_kinet_set_mode(uint32_t mode)
{
	kinetptr->flags = mode;
	kinetptr->hw->u.wr.control = mode;
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_get_mode
  FUNCTION DETAILS  : Get KiNet operating mode.
********************************************************************************/

uint32_t hw_kinet_get_mode(void)
{
	return(kinetptr->hw->u.wr.control);
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_set_dirn
  FUNCTION DETAILS  : Set KiNet slot direction.

					  On entry:

					  slot			KiNet slot
					  dirn			Slot direction
********************************************************************************/

void hw_kinet_set_dirn(uint32_t slot, uint32_t dirn)
{
	kinetptr->hw->u.wr.dirn[slot] = dirn;
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_get_dirn
  FUNCTION DETAILS  : Get KiNet slot direction.

					  On entry:

					  slot			KiNet slot
********************************************************************************/

uint32_t hw_kinet_get_dirn(uint32_t slot)
{
	return(kinetptr->hw->u.wr.dirn[slot]);
}



typedef struct edma_event {
	unsigned int opt;
	unsigned int src;
	unsigned int elecnt : 16;
	unsigned int frmcnt : 16;
	unsigned int dst;
	unsigned int eleidx : 16;
	unsigned int frmidx : 16;
	unsigned int link : 16;
	unsigned int elerld : 16;
} edma_event;


#define QDMA_OPT		0x02000000
#define QDMA_SRC		0x02000004
#define QDMA_CNT		0x02000008
#define QDMA_DST		0x0200000C
#define QDMA_IDX		0x02000010

#define QDMA_S_OPT		0x02000020
#define QDMA_S_SRC		0x02000024
#define QDMA_S_CNT		0x02000028
#define QDMA_S_DST		0x0200002C
#define QDMA_S_IDX		0x02000020

#define CIPR			0x01A0FFE4
#define CIER			0x01A0FFE8
#define CCER			0x01A0FFEC
#define EER				0x01A0FFF4
#define ECR				0x01A0FFF8

#define PRAM_BASE		0x01A00000

extern s16_t kinet_buffer;

/********************************************************************************
  FUNCTION NAME     : dma_init
  FUNCTION DETAILS  : Initialise a DMA transfer parameter area
********************************************************************************/

void dma_init(int param, edma_event* dma)
{
	unsigned int* dst = (unsigned int*)(PRAM_BASE + (param * sizeof(edma_event)));
	unsigned int* src = (unsigned int*)dma;
	int n;

	for (n = 0; n < 6; n++) {
		*dst++ = *src++;
	}
}

/********************************************************************************
  FUNCTION NAME     : dma_enable
  FUNCTION DETAILS  : Enable KiNet DMA transfer
********************************************************************************/

void dma_enable(void)
{
	*((uint32_t*)CCER) = 0x00000100;	/* Enable chaining using event #8 */
	*((uint32_t*)EER) = 0x00000010;	/* Enable event #4 to trigger DMA */
}

/********************************************************************************
  FUNCTION NAME     : dma_disable
  FUNCTION DETAILS  : Disable KiNet DMA transfer
********************************************************************************/

void dma_disable(void)
{
	*((uint32_t*)CCER) = 0x00000000;	/* Disable chaining using event #8 */
	*((uint32_t*)EER) = 0x00000000;	/* Disable event #4 to trigger DMA */
}

/********************************************************************************
  FUNCTION NAME     : kinet_dma_configure
  FUNCTION DETAILS  : Configure DMA operation for KiNet transfer
********************************************************************************/

void kinet_dma_configure(void)
{
	kinetmap* ptr;
	uint32_t n;
	uint32_t slot;
	uint32_t area0_start = 128;
	uint32_t area0_end = 0;
	uint32_t area1_start = 128;
	uint32_t area1_end = 0;
	edma_event kinet_edma0;
	edma_event kinet_edma1;
	edma_event null_edma;

	/* KiNet slots are split into two areas. Slot numbers 0 to 79 and slot numbers
	   80 to 127. These are transferred using two chained DMA operations.
	*/

	n = kinetptr->in_count;
	ptr = kinetptr->in_maplist;

	while (n) {
		slot = ptr->kinet_slot;
		if (slot <= 79) {
			if (slot < area0_start) area0_start = slot;
			if (slot > area0_end) area0_end = slot;
		}
		else {
			if (slot < area1_start) area1_start = slot;
			if (slot > area1_end) area1_end = slot;
		}
		ptr++;
		n--;
	}

	/* Trap if either area contains no active slots */

	if (area0_start == 128) area0_start = area0_end;
	if (area1_start == 128) area1_start = area1_end;

	/* Create two DMA parameter blocks for the KiNet transfer */

	kinet_edma0.opt = 0x29380001;	/* Chain to event #8 when complete */
	kinet_edma0.src = (unsigned int)&kinetptr->hw->u.wr.data[area0_start];
	kinet_edma0.elecnt = area0_end - area0_start + 1;
	kinet_edma0.frmcnt = 0;
	kinet_edma0.dst = (unsigned int)(&kinet_buffer + area0_start);
	kinet_edma0.eleidx = 0;
	kinet_edma0.frmidx = 0;
	kinet_edma0.link = (PRAM_BASE + (16 * sizeof(edma_event))) & 0xFFFF;
	kinet_edma0.elerld = 0;

	kinet_edma1.opt = 0x29390001;	/* Set event #9 to indicate completion */
	kinet_edma1.src = (unsigned int)&kinetptr->hw->u.wr.data[area1_start];
	kinet_edma1.elecnt = area1_end - area1_start + 1;
	kinet_edma1.frmcnt = 0;
	kinet_edma1.dst = (unsigned int)(&kinet_buffer + area1_start);
	kinet_edma1.eleidx = 0;
	kinet_edma1.frmidx = 0;
	kinet_edma1.link = (PRAM_BASE + (16 * sizeof(edma_event))) & 0xFFFF;
	kinet_edma1.elerld = 0;

	/* Create a dummy DMA parameter block */

	null_edma.opt = 0x29300001;
	null_edma.src = 0x00000000;
	null_edma.elecnt = 0;
	null_edma.frmcnt = 0;
	null_edma.dst = 0x00000000;
	null_edma.eleidx = 0;
	null_edma.frmidx = 0;
	null_edma.link = 0;
	null_edma.elerld = 0;

	/* Copy into DMA parameter area */

	IRQ_disable(IRQ_EVT_EDMAINT);		/* Disable EDMA interrupts						*/
	dma_disable();						/* Disable DMA operation during update 			*/
	*((uint32_t*)CIER) = 0x00000000;		/* Disable all EDMA interrupt sources			*/
	dma_init(4, &kinet_edma0);			/* Transfer event 4 initiated by interrupt 4 	*/
	dma_init(8, &kinet_edma1);			/* Transfer event 8 							*/
	dma_init(16, &null_edma);

	/* If input transfer is required, enable DMA operation */

	if (kinetptr->in_count) {
		*((uint32_t*)CIPR) = 0xFFFFFFFF;	/* Reset all pending event interrupt flags 		*/
		*((uint32_t*)CIER) = 0x00000200;	/* Enable EDMA interrupt from event #9 			*/
		IRQ_enable(IRQ_EVT_EDMAINT);
		dma_enable();
	}
}

/********************************************************************************
  FUNCTION NAME     : hw_kinet_map_slot
  FUNCTION DETAILS  : Map KiNet slot to CNet slot.

					  On entry:

					  slot			KiNet slot
					  cnetslot		CNet slot
					  flags			Control flags
									 Bit 0: Direction
										0 = KiNet to CNet (KINET_INPUT)
										1 = CNet to KiNet (KINET_OUTPUT)
									 Bit 1: Scaling
										0 = No scaling	  (KINET_RAW)
										1 = Scaled		  (KINET_DATA)
					  scale			Scale factor
********************************************************************************/

void hw_kinet_map_slot(uint32_t slot, uint32_t cnetslot, uint32_t flags, float scale)
{
	kinetmap* ptr;
	uint32_t n;
	uint32_t istate;
	float* cnetdata;

	cnetdata = (float*)cnet_get_slotaddr(cnetslot, PROC_DSPA);

	if (flags & KINET_OUTPUT) {

		/* CNet to KiNet transfer */

		n = kinetptr->out_count;
		ptr = kinetptr->out_maplist;

		while (n) {
			if (ptr->kinet_slot == slot) {
				kinetptr->out_count--;	/* Compensate for final increment */
				break;
			}
			ptr++;
			n--;
		}
		istate = HWI_disable();
		ptr->kinet_slot = slot;
		ptr->cnet_slot = cnetslot;
		ptr->src.cnet = cnetdata;
		ptr->dst.kinet = &kinetptr->hw->u.wr.data[slot];
		ptr->flags = flags;
		ptr->scale = scale * KINET_OUTPUT_SCALE;
		kinetptr->out_count++;
		HWI_restore(istate);
		kinet_dma_configure();
		return;
	}
	else {

		/* KiNet to CNet transfer */

		n = kinetptr->in_count;
		ptr = kinetptr->in_maplist;

		while (n) {
			if (ptr->kinet_slot == slot) {
				kinetptr->in_count--;		/* Compensate for final increment */
				break;
			}
			ptr++;
			n--;
		}
		istate = HWI_disable();
		ptr->kinet_slot = slot;
		ptr->cnet_slot = cnetslot;
		ptr->src.kinet = &kinet_buffer + slot;
		ptr->dst.cnet = cnetdata;
		ptr->flags = flags;
		ptr->scale = KINET_INPUT_SCALE / scale;
		kinetptr->in_count++;
		kinet_dma_configure();
		HWI_restore(istate);
		return;
	}
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_unmap_slot
  FUNCTION DETAILS  : Unmap KiNet slot.

					  On entry:

					  slot			KiNet slot
********************************************************************************/

void hw_kinet_unmap_slot(uint32_t slot)
{
	kinetmap* ptr;
	uint32_t n;
	uint32_t istate;

	/* Check CNet to KiNet transfer list */

	n = kinetptr->out_count;
	ptr = kinetptr->out_maplist;

	while (n) {
		if (ptr->kinet_slot == slot) {
			kinetptr->out_count--;
			if (kinetptr->out_count) {
				istate = HWI_disable();	/* Disable interrupts while manipulating list */

				/* Copy the last maplist entry over the entry being removed */

				*ptr = kinetptr->out_maplist[kinetptr->out_count];
				HWI_restore(istate);	/* Restore interrupt state */
				return;
			}
		}
		ptr++;
		n--;
	}

	/* Check KiNet to CNet transfer list */

	n = kinetptr->in_count;
	ptr = kinetptr->in_maplist;

	while (n) {
		if (ptr->kinet_slot == slot) {
			kinetptr->in_count--;
			if (kinetptr->in_count) {
				istate = HWI_disable();	/* Disable interrupts while manipulating list */

				/* Copy the last maplist entry over the entry being removed */

				*ptr = kinetptr->in_maplist[kinetptr->in_count];
				HWI_restore(istate);	/* Restore interrupt state */
				return;
			}
		}
		ptr++;
		n--;
	}
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_read_slot_mapping
  FUNCTION DETAILS  : Read mapping for a KiNet slot.

					  On entry:

					  slot			KiNet slot

					  On exit:

					  cnetslot		CNet slot
									 0xFFFFFFFF = Unmapped
					  flags			Control flags
									 Bit 0: Direction
										0 = KiNet to CNet (KINET_INPUT)
										1 = CNet to KiNet (KINET_OUTPUT)
									 Bit 1: Scaling
										0 = No scaling	  (KINET_RAW)
										1 = Scaled		  (KINET_DATA)
					  scale			Scale factor
********************************************************************************/

void hw_kinet_read_slot_mapping(uint32_t slot, uint32_t* cnetslot, uint32_t* flags, float* scale)
{
	kinetmap* ptr;
	uint32_t n;

	/* Check if this KiNet slot is mapped as an output. If so return mapping configuration */

	n = kinetptr->out_count;
	ptr = kinetptr->out_maplist;

	while (n) {
		if (ptr->kinet_slot == slot) {
			if (cnetslot) *cnetslot = ptr->cnet_slot;
			if (flags) *flags = ptr->flags;
			if (scale) *scale = ptr->scale / KINET_OUTPUT_SCALE;
			return;
		}
		ptr++;
		n--;
	}

	/* Check if this KiNet slot is mapped as an input. If so return mapping configuration */

	n = kinetptr->in_count;
	ptr = kinetptr->in_maplist;

	while (n) {
		if (ptr->kinet_slot == slot) {
			if (cnetslot) *cnetslot = ptr->cnet_slot;
			if (flags) *flags = ptr->flags;
			if (scale) *scale = KINET_INPUT_SCALE / ptr->scale;
			return;
		}
		ptr++;
		n--;
	}

	/* Slot is unmapped */

	if (cnetslot) *cnetslot = 0xFFFFFFFFU;
	if (flags) *flags = 0;
	if (scale) *scale = 0.0;
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_set_data
  FUNCTION DETAILS  : Set KiNet slot data.

					  On entry:

					  slot			KiNet slot
					  data			Slot data
********************************************************************************/

void hw_kinet_set_data(uint32_t slot, s16_t data)
{
	kinetptr->hw->u.wr.data[slot] = data;
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_get_data
  FUNCTION DETAILS  : Get KiNet slot data.

					  On entry:

					  slot			KiNet slot
********************************************************************************/

s16_t hw_kinet_get_data(uint32_t slot)
{
	return(kinetptr->hw->u.wr.data[slot]);
}



static volatile combuf asynci;	/* Asynchronous communications output buffer */
static volatile combuf asynco;	/* Asynchronous communications input buffer	 */

/********************************************************************************
  FUNCTION NAME     : hw_kinet_async_transfer
  FUNCTION DETAILS  : Send/receive asynchronous KiNet message.

					  On entry:

					  flags		Flags controlling transfer format
									Bit 0:	0 = slow
											1 = fast
					  txmsg		Pointer to transmit message
					  rxmsg		Pointer to receive message
********************************************************************************/

void hw_kinet_async_transfer(uint32_t flags, uint16_t* txmsg, uint32_t txlen, uint16_t chksum,
	uint16_t* rxmsg, uint32_t* rxlen)
{
	while (atomic_tas(&asynci.inuse, 1));	/* Claim async output buffer */

	asynci.start = (uint8_t*)txmsg;
	asynci.length = txlen;
	asynci.chksum = chksum;
	asynci.flags = flags;
	asynco.start = (uint8_t*)rxmsg;

	asynci.rdy = TRUE; /* Initiate transfer */

	while (!asynco.rdy); /* Wait for transfer to complete */

	*rxlen = asynco.length;

	asynco.rdy = FALSE;
	asynci.inuse = FALSE;
	atomic_tas(&asynci.inuse, 0);			/* Release async output buffer */
}



/********************************************************************************
  FUNCTION NAME     : init_cy22150
  FUNCTION DETAILS  : Initialise CY22150 hardware.

  Initialise CY22150 PLL circuit on KiNet board. Initialisation parameters
  based on values calculated by Cypress Semiconductor CyberClock CyClocksRT
  program.

  Source - external 1.048576MHz (2 x KiNet cycle time)
  LCLK1 39.321600MHz

********************************************************************************/

static void init_cy22150(hw_kinet* hw)
{
	hw_kinet_i2c_stop(hw);
	hw_kinet_i2c_start(hw);
	hw_kinet_i2c_send(hw, 0xD2);		/* CY22150 I2C address		*/
	hw_kinet_i2c_send(hw, 0x09);		/* Register 0x09			*/
	hw_kinet_i2c_send(hw, 0x01);
	hw_kinet_i2c_restart(hw);
	hw_kinet_i2c_send(hw, 0xD2);		/* CY22150 I2C address		*/
	hw_kinet_i2c_send(hw, 0x0C);		/* Register 0x0C			*/
	hw_kinet_i2c_send(hw, 0x09);
	hw_kinet_i2c_restart(hw);
	hw_kinet_i2c_send(hw, 0xD2);		/* CY22150 I2C address		*/
	hw_kinet_i2c_send(hw, 0x12);		/* Register 0x12 - 0x13		*/
	hw_kinet_i2c_send(hw, 0x20);
	hw_kinet_i2c_send(hw, 0x00);
	hw_kinet_i2c_restart(hw);
	hw_kinet_i2c_send(hw, 0xD2);		/* CY22150 I2C address		*/
	hw_kinet_i2c_send(hw, 0x40);		/* Registers 0x40 - 0x42	*/
	hw_kinet_i2c_send(hw, 0xD1);
	hw_kinet_i2c_send(hw, 0x4D);
	hw_kinet_i2c_send(hw, 0x80);
	hw_kinet_i2c_restart(hw);
	hw_kinet_i2c_send(hw, 0xD2);		/* CY22150 I2C address		*/
	hw_kinet_i2c_send(hw, 0x44);		/* Registers 0x44 - 0x47	*/
	hw_kinet_i2c_send(hw, 0x3F);
	hw_kinet_i2c_send(hw, 0xFF);
	hw_kinet_i2c_send(hw, 0xFF);
	hw_kinet_i2c_send(hw, 0x84);
	hw_kinet_i2c_stop(hw);
}



/********************************************************************************
  FUNCTION NAME     : hw_kinet_install
  FUNCTION DETAILS  : Install KiNet hardware.

					  On entry:

					  slot			Slot number being installed.

					  flags			Control installation process.
********************************************************************************/

uint32_t hw_kinet_install(int slot, hw_kinet* hw, int flags)
{
	int n;

	init_cy22150(hw);
	init_cy22150(hw);	/* Repeat initialisation to ensure correct operation */

	/* Configure all slots as inputs */

	for (n = 0; n < 128; n++) {
		hw->u.wr.dirn[n] = KINET_INPUT;
	}

	/* Initially disable KiNet */

	hw->u.wr.control = 0;

	/* If no KiNet hardware has been installed, install this one */

	if (!kinetptr) {

		default_kinetdef.hw = hw; /* Point to KiNet hardware */

		kinetptr = &default_kinetdef;

		/* Set all command slots to output direction */

		kinetptr->setdirn(KINET_SLOT_CMD1, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD2, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD3, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD4, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD5, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD6, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD7, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD8, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD9, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD10, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD11, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD12, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD13, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD14, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD15, KINET_OUTPUT);
		kinetptr->setdirn(KINET_SLOT_CMD16, KINET_OUTPUT);

		/* Set watchdog slot as output */

		kinetptr->setdirn(KINET_SLOT_WATCHDOG, KINET_OUTPUT);

		/* Set all bank 0 data values to zero */

		kinetptr->hw->u.wr.control = KINET_BANK0;	/* Override bank selection to access bank 0	*/
		for (n = 0; n < 128; n++) {
			kinetptr->hw->u.wr.data[n] = 0;
		}

		/* Set all bank 1 data values to zero */

		kinetptr->hw->u.wr.control = KINET_BANK1;	/* Override bank selection to access bank 1	*/
		for (n = 0; n < 128; n++) {
			kinetptr->hw->u.wr.data[n] = 0;
		}

		kinetptr->hw->u.wr.control = KINET_ENABLE | KINET_MASTER;			/* Configure KiNet as master, enabled 	*/
	//    kinetptr->hw->u.wr.control = KINET_ENABLE;						/* Configure KiNet as slave, enabled 	*/
		kinetptr->wd_state = KINET_WATCHDOG_ENABLE;							/* Enable watchdog 						*/
		kinetptr->in_count = 0;												/* No KiNet input mappings defined		*/
		kinetptr->out_count = 0;											/* No KiNet output mappings defined		*/
	//    kinetptr->mapcount = 0;											/* No CNet-KiNet mappings defined		*/
		memset(kinetptr->in_maplist, 0, sizeof(kinetptr->in_maplist));		/* Initialise input map list			*/
		memset(kinetptr->out_maplist, 0, sizeof(kinetptr->out_maplist));	/* Initialise output map list			*/
	//    memset(kinetptr->maplist, 0, sizeof(kinetptr->maplist));			/* Initialise map list					*/

	}

	/* Initialise async comms structures */

	memset((void*)&asynci, 0, sizeof(combuf));
	memset((void*)&asynco, 0, sizeof(combuf));
	memset(&async_data, 0, sizeof(FK3_DATA_FORMAT));
	FK3_init();

	//hw_kinet_map_slot(1, 8, KINET_INPUT | KINET_DATA);					/* Map KiNet slot 1 as input to CNet slot 8 		*/
	//hw_kinet_map_slot(KINET_SLOT_CMD1, 192, KINET_OUTPUT | KINET_DATA);	/* Map KiNet slot 4 as output from CNet slot 192 	*/


	return(0);
}



static const s16_t kinet_wd_pattern[4] = { 0x1000, 0x2000, 0x4000, 0x8000 };

#pragma CODE_SECTION(kinet_interrupt, "realtime")

/********************************************************************************
  FUNCTION NAME     : kinet_interrupt
  FUNCTION DETAILS  : KiNet interrupt handler. This is called on alternate CNet
					  interrupts to handle the realtime KiNet operations.
********************************************************************************/

void kinet_interrupt(void)
{

	/* Generate KiNet watchdog */

	if (kinetptr->wd_state & KINET_WATCHDOG_DISABLE) {
		kinetptr->hw->u.wr.data[KINET_SLOT_WATCHDOG] = 0x0000;	/* Watchdog disabled */
	}
	else {

		/* Watchdog enabled */

		kinetptr->hw->u.wr.data[KINET_SLOT_WATCHDOG] = kinet_wd_pattern[kinetptr->wd_state & 0x03] | ((KINET_SLOT_WATCHDOG & 0x0F) << 8);
		kinetptr->wd_state = (kinetptr->wd_state + 1) & (KINET_WATCHDOG_DISABLE | 0x03);
	}

	/* Perform KiNet asynchronous communications */

	kinet_async();
}



/********************************************************************************
  FUNCTION NAME     : FK3_gen_par
  FUNCTION DETAILS  : Generate parity bit for data word.
					  This code is based on that in file FK3.c of KIOSC6X.
********************************************************************************/

static uint16_t FK3_gen_par(uint16_t d)
{
	int i, j, k;

	d &= 0x7fff;
	j = 0x8000;
	k = d << 1;
	for (i = 15; i; i--)
	{
		j ^= k;
		k <<= 1;
	}
	j &= 0x8000;
	return(j | d);
}


/********************************************************************************
  FUNCTION NAME     : FK3_good_par
  FUNCTION DETAILS  : Test parity bit for data word.
					  This code is based on that in file FK3.c of KIOSC6X.
********************************************************************************/

static uint16_t FK3_good_par(uint16_t d)
{
	int i, j;

	j = 0;
	for (i = 16; i; i--)
	{
		j ^= d;
		d <<= 1;
	}
	return(j & 0x8000);
}



/********************************************************************************
  FUNCTION NAME     : FK3_init
  FUNCTION DETAILS  : Initialisation for asynchronous KiNet communications.
					  This code is based on that in file FK3.c of KIOSC6X.
********************************************************************************/

void FK3_init(void)
{
	FK3_DATA_FORMAT* volatile pdata;

	pdata = &async_data;

	PDATA->state = 0;
	PDATA->mr_addr = FK3_gen_par(MASTER);
	PDATA->mr_derr_flg = 0;
	PDATA->mr_flg = 0;
	PDATA->mr_data_len = 0;
	PDATA->ms_flg = 0;
	PDATA->ms_data_len = 0;
	PDATA->to_ms = ACK_TO;
	PDATA->to_val = (int)(2.048 * (float)(PDATA->to_ms));
	PDATA->iufKiCheck = FALSE;
	PDATA->counts = 0;
}



#pragma CODE_SECTION(kinet_output_transfer, "realtime")

/********************************************************************************
  FUNCTION NAME     : kinet_output_transfer
  FUNCTION DETAILS  : Perform CNet to KiNet output data transfer
********************************************************************************/

void kinet_output_transfer(void)
{
	kinetmap* ptr;
	uint32_t n;

	/* Transfer KiNet output slots */

	n = kinetptr->out_count;
	ptr = kinetptr->out_maplist;

	while (n) {
		uint32_t v = *(ptr->src.cnet_raw);
		s16_t* p = ptr->dst.kinet;

		if (ptr->flags & KINET_DATA) {

			/* Scale to standard fullscale range */

			*p = (_spint(_itof(v) * (KINET_OUTPUT_SCALE * 0x10000)) >> 16);
		}
		else {

			/* Raw data so no scaling */

			*p = (s16_t)(v & 0xFFFF);
		}
		ptr++;
		n--;
	}

}



#pragma CODE_SECTION(edma_interrupt, "realtime")

/********************************************************************************
  FUNCTION NAME     : edma_interrupt
  FUNCTION DETAILS  : Perform KiNet input data transfer
********************************************************************************/

void edma_interrupt(void)
{
	kinetmap* ptr;
	uint32_t n;

	*((uint32_t*)CIPR) = 0x00000200;	/* Reset event #9 interrupt flag */

	n = kinetptr->in_count;
	ptr = kinetptr->in_maplist;

	while (n) {
		s16_t v = *(ptr->src.kinet);
		uint32_t* p = ptr->dst.cnet_raw;

		if (ptr->flags & KINET_DATA) {

			/* Scale to standard fullscale range */

			*p = _ftoi(((float)v) * KINET_INPUT_SCALE);
		}
		else {

			/* Raw data so no scaling */

			*p = v & 0xFFFF;
		}
		ptr++;
		n--;
	}
}



#pragma CODE_SECTION(kinet_async, "realtime")

/********************************************************************************
  FUNCTION NAME     : kinet_async
  FUNCTION DETAILS  : Process KiNet asynchronous communications.
********************************************************************************/

void kinet_async(void)
{
	FK3_DATA_FORMAT* volatile pdata;
	static int FK3_state = 0;
	static uint16_t* p;
	static int l;
	uint16_t* kdata;
	uint16_t data;
	uint32_t flag;
	uint32_t bi;

	uint16_t* cbb = (uint16_t*)(asynci.start); 	/* Point to start of tx message */
	uint16_t* prbb = (uint16_t*)(asynco.start);	/* Point to reply buffer		*/
	int* prlen = (int*)&asynco.length;

	/* Functions from FK3_remote_fn moved to a state machine here */

	pdata = &async_data;

	switch (FK3_state) {

	case 0: /* Check for request for async comms operation */

		if (!asynci.rdy) return;

		/* Initialise send parameters */
	//    LOG_RECORD(3);
		PDATA->ms_addr = FK3_gen_par(cbb[1]);

		/* Prepare to receive function request acknowledgement */
		PDATA->mr_flg = 0;
		PDATA->mr_data_len = 0;

		/* Send function request */
		PDATA->ms_hdr[0] = AC_FNREQ;
		PDATA->ms_hdr[1] = FK3_gen_par(*cbb);
		PDATA->ms_hdr[2] = FK3_gen_par(asynci.length - 1);
		PDATA->ms_data_len = 0;
		PDATA->ms_flg = (0x80 | (asynci.flags & 0x01));	// Copy message flags

		/* Move to next state to wait for acknowledgement */

		FK3_state = 1;
		break;

	case 1: /* Wait for function request transmission to be completed */
  //    LOG_RECORD(4);
		if (!PDATA->ms_flg)
		{
			//    LOG_RECORD(5);
			PDATA->to_cnt = PDATA->to_val;
			FK3_state = 2;
		}
		break;

	case 2: /* Wait for acknowledgement */

		if (PDATA->mr_flg || (!PDATA->to_cnt)) {

			/* Check for time out */
			if (!PDATA->mr_flg)
			{
				//      LOG_RECORD(6);
				*prbb = ACE_ACKTO;
				*prlen = 1;	// Added SN
				asynco.rdy = TRUE;
				asynci.rdy = FALSE;
				FK3_state = 0;
				break;
			}

			/* Error? */
			if (PDATA->mr_hdr[0] == AC_FNERR)
			{
				//      LOG_RECORD(7);
				*prbb = PDATA->mr_hdr[1];
				*prlen = 1;	// Added SN
				asynco.rdy = TRUE;
				asynci.rdy = FALSE;
				FK3_state = 0;
				break;
			}

			/* Successful? */
			if (PDATA->mr_hdr[0] != AC_FNACK)
			{
				//            LOG_RECORD(8);
				*prbb = ACE_GEN;
				*prlen = 1;	// Added SN
				asynco.rdy = TRUE;
				asynci.rdy = FALSE;
				FK3_state = 0;
				break;
			}

			/* Check parity of rest of header */
			if ((!FK3_good_par(PDATA->mr_hdr[1]))
				|| (!FK3_good_par(PDATA->mr_hdr[2])))
			{
				//            LOG_RECORD(9);
				*prbb = ACE_HDRPAR;
				*prlen = 1;	// Added SN
				asynco.rdy = TRUE;
				asynci.rdy = FALSE;
				FK3_state = 0;
				break;
			}

			l = (PDATA->mr_hdr[1] & 0x7fff) + 1;
			p = prbb;

			/* Record a few facts */
			*prlen = l;
			/* Prepare to receive call parameter acknowledgement */
			PDATA->mr_flg = 0;
			PDATA->mr_data_len = 0;
			/* Send call parameters */
			PDATA->ms_hdr[0] = AC_CPREQ;
			PDATA->ms_hdr[1] = FK3_gen_par(asynci.length - 1);
			PDATA->ms_hdr[2] = asynci.chksum;
			PDATA->ms_data_ptr = cbb + 1;
			PDATA->ms_data_len = asynci.length - 1;
			PDATA->ms_flg = (0x80 | (asynci.flags & 0x01)); // Copy message flags
			FK3_state = 3;
			break;
		}
		break;

	case 3: /* Wait for call parameter transmission to complete */

		if (!PDATA->ms_flg)
		{
			//	      LOG_RECORD(10);
			PDATA->to_cnt = PDATA->to_val;
			FK3_state = 4;
		}
		break;

	case 4: /* Wait for acknowledgement */

		if (PDATA->mr_flg || (!PDATA->to_cnt)) {

			/* Check for time out */
			if (!PDATA->mr_flg)
			{
				//      LOG_RECORD(11);
				*prbb = ACE_ACKTO;
				*prlen = 1;	// Added SN
				asynco.rdy = TRUE;
				asynci.rdy = FALSE;
				FK3_state = 0;
				break;
			}

			/* Error? */
			if (PDATA->mr_hdr[0] == AC_CPERR)
			{
				//      LOG_RECORD(12);
				*p = PDATA->mr_hdr[1];
				*prlen = 1;
				asynco.rdy = TRUE;
				asynci.rdy = FALSE;
				FK3_state = 0;
				break;
			}

			/* Successful? */
			if (PDATA->mr_hdr[0] != AC_CPACK)
			{
				//      LOG_RECORD(13);
				*p = ACE_GEN;
				*prlen = 1;
				asynco.rdy = TRUE;
				asynci.rdy = FALSE;
				FK3_state = 0;
				break;
			}
			FK3_state = 5;
			break;
		}
		break;

	case 5:

		/* Prepare to receive response parameters */
	//      LOG_RECORD(14);
		PDATA->mr_flg = 0;
		PDATA->mr_derr_flg = 0;
		PDATA->mr_data_ptr = p + 1;
		PDATA->mr_data_len = l - 1;
		PDATA->chksum = 0;

		/* Request response parameters */
		PDATA->ms_hdr[0] = AC_RPREQ;
		PDATA->ms_data_len = 0;
		PDATA->ms_flg = (0x80 | (asynci.flags & 0x01)); // Copy message flags;

		FK3_state = 6;
		break;

	case 6:

		if (!PDATA->ms_flg)
		{
			//      LOG_RECORD(15);
			PDATA->to_cnt = PDATA->to_val;
			FK3_state = 7;
		}
		break;

	case 7: /* Wait for response request transmission to complete */

		if (PDATA->mr_flg || (!PDATA->to_cnt)) {

			/* Check for time out */
			if (!PDATA->mr_flg)
			{
				//      LOG_RECORD(16);
				*prbb = ACE_ACKTO;
				*prlen = 1;	// Added SN
				asynco.rdy = TRUE;
				asynci.rdy = FALSE;
				FK3_state = 0;
				break;
			}

			/* Error? */
			if (PDATA->mr_hdr[0] == AC_RPERR)
			{
				//      LOG_RECORD(17);
						/* Tolerate not ready error */
				if (PDATA->mr_hdr[1] != ACE_NCMPL)
				{
					*p = PDATA->mr_hdr[1];
					*prlen = 1;
					asynco.rdy = TRUE;
					asynci.rdy = FALSE;
					FK3_state = 0;
					break;
				}
				else
				{
					FK3_state = 5;
					break;
				}
			}
			else
			{
				/* Successful? */
				if (((PDATA->mr_hdr[0]) & 0x0000ffff) != AC_RPACK)
				{
					//      LOG_RECORD(18);
					*p = ACE_GEN;
					*prlen = 1;
					asynco.rdy = TRUE;
					asynci.rdy = FALSE;
					FK3_state = 0;
					break;
				}

				/* Check parity on data length */
				if (!FK3_good_par(PDATA->mr_hdr[1]))
				{
					//      LOG_RECORD(19);
					*p = ACE_HDRPAR;
					*prlen = 1;
					asynco.rdy = TRUE;
					asynci.rdy = FALSE;
					FK3_state = 0;
					break;
				}

				/* Data length error? */
				if (PDATA->mr_derr_flg)
				{
					//      LOG_RECORD(20);
					*p = ACE_DLEN;
					*prlen = 1;
					asynco.rdy = TRUE;
					asynci.rdy = FALSE;
					FK3_state = 0;
					break;
				}

				/* Checksum error? */
				if ((PDATA->chksum ^ (PDATA->mr_hdr[2])) & 0x0000ffff)
				{
					//      LOG_RECORD(21);
					*p = ACE_DCS;
					*prlen = 1;
					asynco.rdy = TRUE;
					asynci.rdy = FALSE;
					FK3_state = 0;
					break;
				}

				/* Successful,- copy over function number and return */
		//      LOG_RECORD(22);
				*p = *cbb;
				asynco.rdy = TRUE;
				asynci.rdy = FALSE;
				FK3_state = 0;
				break;
			}
		}
	}

	if (PDATA->to_cnt)
		PDATA->to_cnt--;

	kdata = (uint16_t*)kinetptr->hw->u.wr.data;

	switch (PDATA->state) {
	case 0:
		if (PDATA->ms_flg)
			if (PDATA->ms_flg & 0x01)
				PDATA->state = 14; /* Initiate fast async transmission */
			else
				PDATA->state = 32; /* Initiate slow async transmission */
		else {
			flag = 0;
			if (kdata[KINET_SLOT_SLOW] == AC_LEAD1) {
				PDATA->state = 1;
				break;
			}
			if (kdata[KINET_SLOT_ASYNC + 0] == AC_LEAD3) flag = 1;
			if (kdata[KINET_SLOT_ASYNC + 1] == AC_LEAD2) flag |= 2;
			if (kdata[KINET_SLOT_ASYNC + 2] == AC_LEAD1) flag |= 4;
			if (kdata[KINET_SLOT_ASYNC + 3] == PDATA->mr_addr) flag |= 8;
			if (flag == 0x0F) {

				/* Valid leader and matching address */

				PDATA->mr_hdr[0] = kdata[KINET_SLOT_ASYNC + 4];
				PDATA->mr_hdr[1] = kdata[KINET_SLOT_ASYNC + 5];
				PDATA->mr_hdr[2] = kdata[KINET_SLOT_ASYNC + 6];

				/* If data present, check if length is as expected */

				if (PDATA->mr_hdr[0] & AC_DFLG) {
					if ((PDATA->mr_skip_cnt = (PDATA->mr_hdr[1] & 0x7FFF)) == PDATA->mr_data_len) {
						if (PDATA->mr_data_len)
							PDATA->state = 7;
						else {
							PDATA->mr_flg = 1;
							PDATA->state = 0;
						}
					}
					else {
						PDATA->mr_derr_flg = 1;
						PDATA->mr_flg = 1;
						if (PDATA->mr_skip_cnt)
							PDATA->state = 8;
						else
							PDATA->state = 0;
					}
				}
				else {
					PDATA->mr_flg = 1;
					PDATA->state = 0;
				}
			}
			else if (flag == 0x07) {

				/* Valid leader, but incorrect address, so skip this message */

				PDATA->state = 9;
			}
		}
		break;

	case 1:
		if (kdata[KINET_SLOT_SLOW] == AC_LEAD2)
			PDATA->state = 2;
		else
			PDATA->state = 0;
		break;
	case 2:
		if (kdata[KINET_SLOT_SLOW] == AC_LEAD3)
			PDATA->state = 3;
		else
			PDATA->state = 0;
		break;
	case 3:
		if (kdata[KINET_SLOT_SLOW] == PDATA->mr_addr)
			PDATA->state = 4;
		else
			PDATA->state = 27; // Skip unwanted message
		break;
	case 4:
		PDATA->mr_hdr[0] = kdata[KINET_SLOT_SLOW];
		PDATA->state = 5;
		break;
	case 5:
		PDATA->mr_hdr[1] = kdata[KINET_SLOT_SLOW];
		PDATA->state = 6;
		break;
	case 6:
		PDATA->mr_hdr[2] = kdata[KINET_SLOT_SLOW];

		/* If data present, check if length is as expected */

		if (PDATA->mr_hdr[0] & AC_DFLG) {
			if ((PDATA->mr_skip_cnt = (PDATA->mr_hdr[1] & 0x7FFF)) == PDATA->mr_data_len) {
				if (PDATA->mr_data_len)
					PDATA->state = 25;  // Data present
				else {
					PDATA->mr_flg = 1;
					PDATA->state = 0;   // No data present
				}
			}
			else {
				PDATA->mr_derr_flg = 1;
				PDATA->mr_flg = 1;
				if (PDATA->mr_skip_cnt)
					PDATA->state = 25;  // Data present 
				else
					PDATA->state = 0;   // No data present
			}
		}
		else {
			PDATA->mr_flg = 1;
			PDATA->state = 0; // No data present
		}
		break;

	case 7:

		/* Fast asynchronous reception */

		for (bi = 0; bi < KINET_NUM_ASYNC_SLOTS; bi++) {
			data = kdata[KINET_SLOT_ASYNC + bi];
			*(PDATA->mr_data_ptr)++ = data;
			PDATA->chksum += data;
			if ((--PDATA->mr_data_len) == 0) {
				PDATA->mr_flg = 1;
				PDATA->state = 0;
				break;
			}
		}
		break;
	case 8:

		/* Skip unwanted messages */

		for (bi = 0; bi < KINET_NUM_ASYNC_SLOTS; bi++) {
			if ((--PDATA->mr_skip_cnt) == 0) {
				PDATA->state = 0;
				break;
			}
		}
		break;

	case 9:
		if (kdata[KINET_SLOT_ASYNC + 4] & AC_DFLG)
			PDATA->state = 12;
		else
			PDATA->state = 10;
		break;
	case 10:
		PDATA->state = 11;
		break;
	case 11:
		PDATA->state = 0;
		break;
	case 12:
		PDATA->mr_skip_cnt = kdata[KINET_SLOT_ASYNC + 4] & 0x7FFF;
		PDATA->state = 13;
		break;
	case 13:
		if (PDATA->mr_skip_cnt)
			PDATA->state = 8;
		else
			PDATA->state = 0;
		break;

		/* Prepare to send message */

	case 14:
		//      LOG_RECORD(23);
		for (bi = 0; bi < KINET_NUM_ASYNC_SLOTS; bi++) {
			hw_kinet_set_dirn(KINET_SLOT_ASYNC + bi, KINET_OUTPUT);
		}
		PDATA->state = 15;
		break;

		/* Send lead in, address and header */

	case 15:
		kdata[KINET_SLOT_ASYNC + 0] = AC_LEAD1;
		kdata[KINET_SLOT_ASYNC + 1] = AC_LEAD2;
		kdata[KINET_SLOT_ASYNC + 2] = AC_LEAD3;
		kdata[KINET_SLOT_ASYNC + 3] = PDATA->ms_addr;
		kdata[KINET_SLOT_ASYNC + 4] = PDATA->ms_hdr[0];
		kdata[KINET_SLOT_ASYNC + 5] = PDATA->ms_hdr[1];
		kdata[KINET_SLOT_ASYNC + 6] = PDATA->ms_hdr[2];

		PDATA->state = 21;
		break;

		/* Send header */

	case 21:

		/* Send data, if any */

		if (PDATA->ms_data_len) {
			for (bi = 0; bi < KINET_NUM_ASYNC_SLOTS; bi++) {
				kdata[KINET_SLOT_ASYNC + bi] = *(PDATA->ms_data_ptr)++;
				if ((--PDATA->ms_data_len) == 0) {
					PDATA->state = 23;
					break;
				}
			}
		}
		else {
			PDATA->state = 23;
		}
		break;

	case 22:
		for (bi = 0; bi < KINET_NUM_ASYNC_SLOTS; bi++) {
			kdata[KINET_SLOT_ASYNC + bi] = *(PDATA->ms_data_ptr)++;
			if ((--PDATA->ms_data_len) == 0) {
				PDATA->state = 23;
			}
		}
		break;

		/* Stop driving bus */

	case 23:
		PDATA->state = 24;
		break;

	case 24:
		for (bi = 0; bi < KINET_NUM_ASYNC_SLOTS; bi++) {
			hw_kinet_set_dirn(KINET_SLOT_ASYNC + bi, KINET_INPUT);
		}
		PDATA->ms_flg = 0;
		PDATA->state = 0;
		break;

		/* Slow asynchronous reception */

	case 25:
		data = kdata[KINET_SLOT_SLOW];
		*(PDATA->mr_data_ptr)++ = data;
		PDATA->chksum += data;
		if ((--PDATA->mr_data_len) == 0) {
			PDATA->mr_flg = 1;
			PDATA->state = 0;
		}
	case 26:
		if ((--PDATA->mr_skip_cnt) == 0)
			PDATA->state = 0;
		break;

		/* Skip unwanted messages */

	case 27:
		if (kdata[KINET_SLOT_SLOW] & AC_DFLG)
			PDATA->state = 30;
		else
			PDATA->state = 28;
		break;
	case 28:
		PDATA->state = 29;
		break;
	case 29:
		PDATA->state = 0;
		break;
	case 30:
		PDATA->mr_skip_cnt = kdata[KINET_SLOT_SLOW] & 0x7FFF;
		PDATA->state = 31;
		break;
	case 31:
		if (PDATA->mr_skip_cnt)
			PDATA->state = 26;
		else
			PDATA->state = 0;
		break;

		/* Slow asynchronous transmission */

	case 32:
		//      LOG_RECORD(24);
		hw_kinet_set_dirn(KINET_SLOT_SLOW, KINET_OUTPUT);
		PDATA->state = 33;
		break;

		/* Send lead in */

	case 33:
		kdata[KINET_SLOT_SLOW] = AC_LEAD1;
		PDATA->state = 34;
		break;
	case 34:
		kdata[KINET_SLOT_SLOW] = AC_LEAD2;
		PDATA->state = 35;
		break;
	case 35:
		kdata[KINET_SLOT_SLOW] = AC_LEAD3;
		PDATA->state = 36;
		break;

		/* Send header */

	case 36:
		kdata[KINET_SLOT_SLOW] = PDATA->ms_addr;
		PDATA->state = 37;
		break;
	case 37:
		kdata[KINET_SLOT_SLOW] = PDATA->ms_hdr[0];
		PDATA->state = 38;
		break;
	case 38:
		kdata[KINET_SLOT_SLOW] = PDATA->ms_hdr[1];
		PDATA->state = 39;
		break;
	case 39:
		kdata[KINET_SLOT_SLOW] = PDATA->ms_hdr[2];

		/* Send data, if any */

		if (PDATA->ms_data_len)
			PDATA->state = 40;
		else
			PDATA->state = 41;
		break;
	case 40:
		kdata[KINET_SLOT_SLOW] = *(PDATA->ms_data_ptr)++;
		if ((--PDATA->ms_data_len) == 0)
			PDATA->state = 41;
		break;

		/* Stop driving bus */

	case 41:
		PDATA->state = 42;
		break;
	case 42:
		hw_kinet_set_dirn(KINET_SLOT_SLOW, KINET_INPUT);
		PDATA->ms_flg = 0;
		PDATA->state = 0;
		break;

	default:
		PDATA->state = 0;

	}

}



