/********************************************************************************
 * MODULE NAME       : rtbuffer.c												*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Realtime data streaming buffer management routines.		*
 ********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>

 /* The following two lines are required to allow Events.h to compile */

#define SUPGEN
#define Parameter void

#include <stdint.h>
#include "porting.h"

#include "server.h"
#include "controller.h"
#include "hardware.h"
#include "msgserver.h"
#include "msgstruct.h"
#include "cnet.h"
#include "rtbuffer.h"
#include "debug.h"
#include "HardwareIndependent/Events.h"

extern int sysmem;					/* System memory area used for malloc	*/

/* Realtime output sample counter */

near uint32_t samplectr_lsh = 0;
near uint32_t samplectr_msh = 0;

/* State variable for turning point mode control */

near uint32_t rt_tpstate = STATE_TP_DISABLE;


/********************************************************************************
  FUNCTION NAME     : rt_alloc
  FUNCTION DETAILS  : Allocate the realtime data transfer buffers.

					  On entry:

					  size		Size of buffer allocation.

********************************************************************************/

rtctrl* rt_alloc(uint32_t size)
{
	rtctrl* r;

	/* Allocate realtime control structure */

	r = malloc(sizeof(rtctrl));
	if (r == NULL) {
		fprintf(stderr, "malloc failed - rtctrl\n");
		diag_error(DIAG_MALLOC);
	}

	/* Allocate realtime buffer area */
	r->buffer_area = malloc(size);
	if (r->buffer_area == NULL) {
		fprintf(stderr, "malloc failed - buffer_area\n");
		diag_error(DIAG_MALLOC);
	}

	/* Allocate realtime input list */
	r->inlist = malloc(sizeof(cnetlist));
	if (r->inlist == NULL) {
		fprintf(stderr, "malloc failed - buffer_area\n");
		diag_error(DIAG_MALLOC);
	}

	/* Allocate realtime output list */
	r->outlist = malloc(sizeof(cnetlist));
	if (r->outlist == NULL) {
		fprintf(stderr, "malloc failed - buffer_area\n");
		diag_error(DIAG_MALLOC);
	}

	r->bufsize = size;

	return(r);
}



/********************************************************************************
  FUNCTION NAME     : rt_initialise
  FUNCTION DETAILS  : Initialise the buffers and controls for realtime data
					  transfers.

					  On entry:

					  r			Pointer to realtime control structure.

					  flags		Flag settings for realtime transfer.

********************************************************************************/

void rt_initialise(rtctrl* r, uint32_t flags)
{
	r->enable = FALSE;		/* Disable real-time transfers	*/
	r->rate = 1;
	r->ctr = 1;

	r->inlist->size = 0;
	r->outlist->size = 0;

	r->flags = flags;

	r->inbuf.size = 0;
	r->inbuf.pktsize = 0;
	r->inbuf.free = 0;
	r->inbuf.used = 0;
	r->inbuf.overrun = FALSE;
	r->inbuf.underrun = FALSE;
	r->inbuf.start = NULL;
	r->inbuf.end = NULL;
	r->inbuf.inptr = NULL;
	r->inbuf.outptr = NULL;
	if (pthread_mutex_init(&r->inbuf.mtx, NULL))
	{
		perror("rt_initialise");
		while (1);
	}
	

	r->outbuf.size = 0;
	r->outbuf.pktsize = 0;
	r->outbuf.free = 0;
	r->outbuf.used = 0;
	r->outbuf.overrun = FALSE;
	r->outbuf.underrun = FALSE;
	r->outbuf.start = NULL;
	r->outbuf.end = NULL;
	r->outbuf.inptr = NULL;
	r->outbuf.outptr = NULL;
	if (pthread_mutex_init(&r->outbuf.mtx, NULL))
	{
		perror("rt_initialise");
		while (1);
	}
}



near uint32_t debug_rt_bufin_count = 0;
near uint32_t debug_rt_rampin_count = 0;
near uint32_t debug_rt_copy_count = 0;
near uint32_t debug_rt_notxfr_count = 0;
near uint32_t debug_rt_underrun_count = 0;
near uint32_t debug_rt_overrun_count = 0;
near uint32_t debug_rt_ramp_count = 0;
near uint32_t debug_rt_noinput_count = 0;
near uint32_t debug_rt_local_count = 0;

#define RTIN_TSR_MASK 	TSRAcqEn	// Mask in TSR slot controlling realtime sample data acquisition
#define RTOUT_TSR_TPDAV	TSRTPDAV	// Mask in TSR slot indicating realtime sample replay data available
#define RTOUT_TSR_TPACK	TSRTPACK	// Mask in TSR slot indicating realtime sample replay data acknowledge

#define RT_TSR_SLOT	CNET_DATASLOT_TSR0

#define RT_INPUT_ENABLE ((r->enable & RT_INPUT) || ((r->enable & RT_INTSR) && (CNET_MEM_READ[RT_TSR_SLOT] & RTIN_TSR_MASK)))

#define RT_OUTPUT_ENABLE ((r->enable & RT_OUTPUT) || ((r->enable & RT_OUTTSR) && (rt_tpstate == STATE_TP_OUTPUT)))

/* Macro controls the state of the TSR TPDAV line */

#define RT_TPDAV(x) CtrlTSRDAVOut((x) ? TSRTPDAV : 0)

/* Macro returns the state of the TSR TPACK line */

#define RT_TPACK	(CNET_MEM_READ[RT_TSR_SLOT] & RTOUT_TSR_TPACK)

#pragma CODE_SECTION(rt_transfer, "realtime")

/********************************************************************************
  FUNCTION NAME     : rt_transfer
  FUNCTION DETAILS  : Perform realtime data transfer operations.

					  This function must only be called with interrupts
					  disabled, i.e. normally only from within the CNet
					  interrupt handler.

					  If the copy flag is set, then a KiNet interface is
					  present and active and this is inactive phase of the
					  CNet clock (to reduce sample rate to 2048Hz), so do
					  not perform a transfer, instead just copy previous
					  output data.
********************************************************************************/

void rt_transfer(rtctrl* r, uint32_t copy, uint32_t* control)
{
	uint32_t in_overrun = FALSE;
	uint32_t out_underrun = FALSE;
	uint32_t txfr = FALSE;
	uint32_t flags = 0;
	uint32_t local = 0;

	//LOG_LED(6,1);

	/* If pointer to control flags is valid, copy flags to local variable and reset */

	if (control) {
		local = *control;
		*control = 0;
	}

	pthread_mutex_lock(&r->outbuf.mtx);
	pthread_mutex_lock(&r->inbuf.mtx);

	r->outbuf.empty = (r->outbuf.used < (r->outlist->size * sizeof(float)));

	if (local & (GENERATE_PAUSE | GENERATE_STOP)) {

		int paused = local & GENERATE_PAUSE;

		debug_rt_local_count++;

#if 0

		/* This code replaced by cleaner version below */

		flags = RT_INPUT | paused; /* Switch on inputs & clear outputs */

		/* Now disable replay as required */

		if ((local & PAUSE_COMMAND_ONLY) == 0) {
			flags = paused; /* Clear real time enable flag and set paused flag if required */
		}
#endif

#if 0
		if (local & PAUSE_COMMAND_ONLY) {
			flags = RT_INPUT | paused; 			/* Input on, output off, paused */
		}
		else {
			flags = paused; 					/* Paused */
		}
#endif

		if (local & PAUSE_COMMAND_ONLY) {
			flags = (r->enable & RT_INPUT) | paused;	/* Keep existing input state, turn output off, set paused */
		}
		else {
			flags = paused; 							/* Turn off input and output state, set paused */
		}


		/* Check for stop request flag */

		if (local & GENERATE_STOP) {
			cnetdata* cptr;
			uint32_t     ctr;
			int       time;
			float     inc;

			cptr = r->outlist->list;
			ctr = r->outlist->size;
			flags |= RT_RAMP;

			// Check type

			if (cnet_stop_type) {	/* Constant time */

				while (ctr--) {

					time = *(cptr->time);

					*(cptr->rate) = ((*(cptr->target)) - (*(cptr->copy))) / time;
					*(cptr->tcounter) = time;

					/* Move to next entry in CNet slot list */

					cptr++;
				}

			}
			else { /* Constant rate */

				while (ctr--) {
					inc = *(cptr->rate);
					inc = (inc < 0) ? -inc : inc;	/* Ensure increment is positive */
					time = (int)(((*(cptr->target)) - (*(cptr->copy))) / inc);
					if (time < 0) {
						*(cptr->tcounter) = (uint32_t)(-time);
						*(cptr->rate) = -inc;
					}
					else {
						*(cptr->tcounter) = (uint32_t)time;
						*(cptr->rate) = inc;
					}

					/* Move to next entry in CNet slot list */

					cptr++;

				}

			}

		}

		r->enable = flags;

		/* Check if flushing of buffers has been requested */

		if (local & (FLUSH_BOTH_BUFFERS | FLUSH_IN_BUFFER | FLUSH_OUT_BUFFER)) {

			/* Clear the buffers */

			r->enable = paused;
			flush_buffer(r, local & (FLUSH_BOTH_BUFFERS | FLUSH_IN_BUFFER | FLUSH_OUT_BUFFER));
		}

	}

	/* If copy flag is set and output is enabled, then just update from local CNet
	   copy data rather than reading from buffer memory. Do not process any input data.
	*/

	if (copy) {
		debug_rt_copy_count++;
		cnet_copyout(&r->outbuf, r->outlist->list, r->outlist->size);
		return;
	}

	/* If input is enabled, check for buffer overrun */

	if (RT_INPUT_ENABLE) {
		in_overrun = ((r->inlist->size * sizeof(float)) > r->inbuf.free);
		if (in_overrun) r->inbuf.overrun = TRUE;
	}
	else {
		debug_rt_noinput_count++;
	}

	/* Check decimation counter */

	if (r->ctr && (--r->ctr == 0)) {
		r->ctr = r->rate;
		txfr = TRUE;
	}

	if (r->enable & RT_RAMP) {

		/* Performing CNet ramp operation */

		debug_rt_ramp_count++;
		cnet_rampout(r, &r->outbuf, r->outlist->list, r->outlist->size);
		if ((++samplectr_lsh) == 0) samplectr_msh++;	/* Increment sample counter */

		/* Check if buffer input also required */

		if (txfr) {
			if (RT_INPUT_ENABLE && !in_overrun) {
				debug_rt_rampin_count++;
				cnet_bufin(&r->inbuf, r->inlist->list, r->inlist->size);
			}
		}
	}
	else {

		/* Normal buffered operation, perform checks for buffer overrun/underrun */

		if (RT_OUTPUT_ENABLE) {
			out_underrun = (r->outbuf.used < (r->outlist->size * sizeof(float)));
			if (out_underrun) r->outbuf.underrun = TRUE;
		}
		if (txfr) {
			if (out_underrun) debug_rt_underrun_count++;
			if (in_overrun) debug_rt_overrun_count++;
			if (!(r->enable & RT_NOSYNC) && (out_underrun || in_overrun)) {
				goto do_copyout; /* Underrun or overrun when synchronised operation is not disabled,
									so copy output data if enabled. No input data.
								 */
			}
			else {
				if (RT_INPUT_ENABLE && !in_overrun) {
					debug_rt_bufin_count++;
					cnet_bufin(&r->inbuf, r->inlist->list, r->inlist->size);
				}
				if (RT_OUTPUT_ENABLE && !out_underrun) {
					cnet_bufout(&r->outbuf, r->outlist->list, r->outlist->size);
					if ((++samplectr_lsh) == 0) samplectr_msh++; /* Increment sample counter */
					if (rt_tpstate == STATE_TP_OUTPUT) {
						RT_TPDAV(1);	/* Assert TPDAV */
						rt_tpstate = STATE_TP_WAITACK; /* Begin wait for TPACK asserted */
					}
				}
				else {
					goto do_copyout; // No new data, so repeat current state.
				}
			}
		}
		else {
			debug_rt_notxfr_count++;
		do_copyout:
			cnet_copyout(&r->outbuf, r->outlist->list, r->outlist->size);
		}
	}

	pthread_mutex_unlock(&r->outbuf.mtx);
	pthread_mutex_unlock(&r->inbuf.mtx);

	//LOG_LED(6,0);

	/* Perform handshaking when operating in turning point mode.

	   Macro RT_TPDAV(x) sets the state of the TSR TPDAV line.
	   The macro RT_TPACK returns the state of the TSR TPACK line.
	 */

	switch (rt_tpstate) {
	case STATE_TP_DISABLE:
		break;
	case STATE_TP_OUTPUT:
		break;
	case STATE_TP_WAITACK:
		if (RT_TPACK) {
			RT_TPDAV(0); /* Negate TPDAV */
			rt_tpstate = STATE_TP_WAITNACK; /* Begin wait for TPACK negated */
		}
		break;
	case STATE_TP_WAITNACK:
		if (!RT_TPACK) rt_tpstate = STATE_TP_OUTPUT;
		break;
	}
}


near uint32_t debug_cnet_bufin_count = 0;

/********************************************************************************
  FUNCTION NAME     : cnet_bufin
  FUNCTION DETAILS  : Insert CNet data into buffer ready for transmission to
					  host.

					  This function must only be called with interrupts
					  disabled, i.e. normally only from within the CNet
					  interrupt handler.
********************************************************************************/

void cnet_bufin(buffer* buf, cnetdata* list, uint32_t listsize)
{
	cnetdata* cptr;
	float* dptr;
	uint32_t     ctr;
	uint32_t     pktsize;

	debug_cnet_bufin_count++;

	//LOG_LED(5,1);

	pktsize = listsize * sizeof(float);

	/* Check if space in buffer for whole of CNet data block */

	if (pktsize <= buf->free) {

		cptr = list;
		dptr = (float*)buf->inptr;
		ctr = listsize;

#if 1
		while (ctr--) {
			*dptr++ = *(cptr->data);	/* Copy CNet data word */
			cptr++;
		}
#endif

#if 0  // Experiment for timing if contiguous mapped CNet block is faster
		__memcpy(dptr, (uint32_t*)cnet_mem_data, listsize * 4);
		dptr += listsize;
#endif

		/* Check for buffer wraparound */

		if (dptr >= (float*)buf->end)
		{
			dptr = (float*)buf->start;
		}
		buf->inptr = (uint8_t*)dptr;

		/* Adjust buffer free/used counts */

		buf->used += pktsize;
		buf->free -= pktsize;

	}
	//LOG_LED(5,0);

}



#pragma CODE_SECTION(cnet_bufout, "realtime")

/********************************************************************************
  FUNCTION NAME     : cnet_bufout
  FUNCTION DETAILS  : Read CNet data from buffer.

					  This function must only be called with interrupts
					  disabled, i.e. normally only from within the CNet
					  interrupt handler.
********************************************************************************/

void cnet_bufout(buffer* buf, cnetdata* list, uint32_t listsize)
{
	cnetdata* cptr;
	float* dptr;
	float     data;
	uint32_t     ctr;
	uint32_t     pktsize;

	//LOG_LED(4,1);

	pktsize = listsize * sizeof(float);

	/* Check if enough data in buffer for whole of CNet data block */

	if (buf->used >= pktsize) {

		cptr = list;
		dptr = (float*)buf->outptr;
		ctr = listsize;

		while (ctr--) {
			data = *dptr++;			/* Read data */
			*(cptr->data) = data;	/* Write CNet data word 	*/
			*(cptr->copy) = data;	/* Make local copy			*/

			/* Move to next entry in CNet slot list */

			cptr++;
		}

		/* Check for buffer wraparound */

		if (dptr >= (float*)buf->end) dptr = (float*)buf->start;
		buf->outptr = (uint8_t*)dptr;

		/* Adjust buffer free/used counts */

		buf->used -= pktsize;
		buf->free += pktsize;

	}
	//LOG_LED(4,0);

}



#pragma CODE_SECTION(cnet_rampout, "realtime")

/********************************************************************************
  FUNCTION NAME     : cnet_rampout
  FUNCTION DETAILS  : Generate CNet ramp to stop target value.
********************************************************************************/

void cnet_rampout(rtctrl* r, buffer* buf, cnetdata* list, uint32_t listsize)
{
	cnetdata* cptr;
	uint32_t     ctr;

	cptr = list;
	ctr = listsize;

	while (ctr--) {

		if (*(cptr->tcounter)) {
			*(cptr->copy) += *(cptr->rate);
			*(cptr->data) = *(cptr->copy);
			(*(cptr->tcounter))--;
		}
		else {
			*(cptr->copy) = *(cptr->target);
			*(cptr->data) = *(cptr->target);
			r->enable &= ~(RT_RAMP | RT_OUTPUT);
		}

		/* Move to next entry in CNet slot list */

		cptr++;
	}
}



#pragma CODE_SECTION(cnet_copyout, "realtime")

/********************************************************************************
  FUNCTION NAME     : cnet_copyout
  FUNCTION DETAILS  : Copy previous CNet data.

					  This function copies the CNet data from the local copy
					  to the CNet data memory. This is used when an underrun
					  has occurred to allow the previous output data to be
					  maintained.
********************************************************************************/

void cnet_copyout(buffer* buf, cnetdata* list, uint32_t listsize)
{
	cnetdata* cptr;
	uint32_t   ctr;

	//LOG_LED(3,1);

	cptr = list;
	ctr = listsize;

	while (ctr--) {

		/* Copy previous CNet data */

		*(cptr->data) = *(cptr->copy);

		/* Move to next entry in CNet slot list */

		cptr++;
	}
	//LOG_LED(3,0);

}



#pragma CODE_SECTION(rt_bufout, "realtime")

/*****************************************************************************
*																			 *
* Function name:	rt_bufout												 *
*																			 *
* Description:		Read data from buffer for transfer to computer.			 *
*																			 *
* On entry:																	 *
*																			 *
* dst		Pointer to destination where data is to be copied				 *
* buf		Pointer to buffer holding data									 *
* size		Number of bytes to be transferred from buffer					 *
*																			 *
*****************************************************************************/

void rt_bufout(uint8_t* dst, buffer* buf, uint32_t size)
{
	uint8_t* dptr;
	uint32_t len1;
	uint32_t copysize;

	dptr = buf->outptr;
	copysize = size;

	if ((dptr + copysize) >= buf->end) {

		/* The required size will overrun the end of the buffer, so split the copy
		   into two pieces. First determine the amount that can be copied upto the
		   end of the buffer and transfer that block. Then reset the pointer to the
		   start of the buffer and transfer the remaining block.
		*/

		len1 = buf->end - dptr;		/* Calculate first block size */
		memcpy(dst, dptr, len1);	/* And copy */

		dptr = buf->start;			/* Reset to buffer start */

		dst += len1;					/* Update destination pointer */
		copysize -= len1;				/* Calculate remaining block size */
	}

	/* Copy remaining block */
	memcpy(dst, dptr, copysize);

	dptr += copysize; /* Adjust buffer pointer */
	buf->outptr = dptr;

	/* Adjust buffer free/used counts. Interrupts are disabled during adjustment. */
	pthread_mutex_lock(&buf->mtx);
	buf->used -= size;
	buf->free += size;
	pthread_mutex_unlock(&buf->mtx);
}

/*****************************************************************************
*																			 *
* Function name:	rt_bufin												 *
*																			 *
* Description:		Write data into buffer from computer.					 *
*																			 *
* On entry:																	 *
*																			 *
* buf		Pointer to buffer where data is to be copied					 *
* src		Pointer to source holding data									 *
* size		Number of bytes to be transferred to buffer						 *
*																			 *
*****************************************************************************/
void rt_bufin(buffer* buf, uint8_t* src, uint32_t size)
{
	uint8_t* dptr;
	uint32_t len1;
	uint32_t copysize;

	dptr = buf->inptr;
	copysize = size;

	if ((dptr + copysize) >= buf->end) {

		/* The required size will overrun the end of the buffer, so split the copy
		   into two pieces. First determine the amount that can be copied upto the
		   end of the buffer and transfer that block. Then reset the pointer to the
		   start of the buffer and transfer the remaining block.
		*/

		len1 = buf->end - dptr;		/* Calculate first block size */
		memcpy(dptr, src, len1);	/* And copy */

		dptr = buf->start;			/* Reset to buffer start */

		src += len1;					/* Update source pointer */
		copysize -= len1;				/* Calculate remaining block size */
	}

	/* Copy remaining block */

	memcpy(dptr, src, copysize);

	dptr += copysize;				/* Adjust buffer pointer */
	buf->inptr = dptr;

	/* Adjust buffer free/used counts. Interrupts are disabled during adjustment. */
	pthread_mutex_lock(&buf->mtx);
	buf->used += size;
	buf->free -= size;
	pthread_mutex_unlock(&buf->mtx);
}


/*****************************************************************************
*																			 *
* Function name:	rt_sktout												 *
*																			 *
* Description:		Read data from buffer and write to socket for transfer 	 *
*					to computer.			 								 *
*																			 *
* On entry:																	 *
*																			 *
* skt		Socket to be written											 *
* buf		Pointer to buffer holding data									 *
* size		Number of bytes to be transferred from buffer					 *
*																			 *
*****************************************************************************/
void rt_sktout(int skt, buffer* buf, uint32_t size)
{
	uint8_t* dptr;
	uint32_t len1;
	uint32_t oldstate;
	uint32_t copysize;
	int err;
	uint32_t txlen;

	dptr = buf->outptr;
	copysize = size;

	if ((dptr + copysize) >= buf->end) {

		/* The required size will overrun the end of the buffer, so split the
		   transfer into two pieces. First determine the amount that can be written
		   upto the end of the buffer and transfer that block. Then reset the pointer
		   to the start of the buffer and transfer the remaining block.
		*/

		len1 = buf->end - dptr;		/* Calculate first block size */

		err = send(skt, dptr, len1, 0);	/* And transfer */
		if (err < 0 || err != len1)
			printf("rt_sktout: error 1\n");

		dptr = buf->start;			/* Reset to buffer start */

		copysize -= len1;				/* Calculate remaining block size */

	}

	/* Transfer remaining block if non-zero length */
	err = send(skt, dptr, copysize, 0);
	if (err < 0 || err != copysize)
		printf("rt_sktout: error 2\n");

	dptr += copysize;				/* Adjust buffer pointer */
	buf->outptr = dptr;

	/* Adjust buffer free/used counts. Interrupts are disabled during adjustment. */
	pthread_mutex_lock(&buf->mtx);
	buf->used -= size;
	buf->free += size;
	pthread_mutex_unlock(&buf->mtx);
}


/*****************************************************************************
*																			 *
* Function name:	rt_sktin												 *
*																			 *
* Description:		Write data into buffer from socket.						 *
*																			 *
* On entry:																	 *
*																			 *
* skt		Socket to be read												 *
* buf		Pointer to buffer where data is to be copied					 *
* size		Number of bytes to be transferred to buffer						 *
*																			 *
*****************************************************************************/
void rt_sktin(int skt, buffer* buf, uint32_t size, connection* conn)
{
	uint8_t* dptr;
	uint32_t len1;
	uint32_t oldstate;
	uint32_t copysize;
	int err;

	if (size == 0)
		return;

	dptr = buf->inptr;
	copysize = size;
	printf("%s: size=%d\n", __FUNCTION__, size);
	if ((dptr + copysize) >= buf->end) {

		/* The required size will overrun the end of the buffer, so split the
		   transfer into two pieces. First determine the amount that can be read
		   upto the end of the buffer and transfer that block. Then reset the
		   pointer to the start of the buffer and transfer the remaining block.
		*/

		len1 = buf->end - dptr;		/* Calculate first block size */

		err = recv(skt, dptr, len1, MSG_WAITALL);	/* Do transfer */

		dptr = buf->start;			/* Reset to buffer start */

		copysize -= len1;				/* Calculate remaining block size */
	}

	/* Transfer remaining block */
	if (copysize > 0)
		err = recv(skt, dptr, copysize, MSG_WAITALL);

	dptr += copysize;				/* Adjust buffer pointer */
	buf->inptr = dptr;

	/* Adjust buffer free/used counts. Interrupts are disabled during adjustment. */
	pthread_mutex_lock(&buf->mtx);
	buf->used += size;
	buf->free -= size;
	pthread_mutex_unlock(&buf->mtx);
}



/*****************************************************************************
*																			 *
* Function name:	set_bufsize												 *
*																			 *
* Description:		Set input/output buffer sizes.							 *
*																			 *
* On entry:																	 *
*																			 *
* inbuf		points to input buffer definition								 *
* outbuf	points to output buffer definition								 *
* insize	defines input buffer size (in packets)							 *
* outsize	defines output buffer size (in packets)							 *
*																			 *
* If either insize or outsize is set to BUFFER_AUTOSIZE, it will be 		 *
* calculated automatically to occupy the remaining space after the other 	 *
* buffer has been allocated. If both insize and outsize are set to 			 *
* BUFFER_AUTOSIZE then an error	will be generated.					 		 *
*																			 *
* If insufficient space is available within the buffers to satisfy the		 *
* request an error will be generated.										 *
*																			 *
*****************************************************************************/

int set_bufsize(rtctrl* r, int insize, int outsize)
{
	int inbytes;
	int outbytes;
	int enable;
	uint8_t* ptr;

	if (insize != BUFFER_AUTOSIZE) {
		inbytes = insize * r->inbuf.pktsize;
		if (outsize != BUFFER_AUTOSIZE) {
			outbytes = outsize * r->outbuf.pktsize;
		}
		else {
			outbytes = r->bufsize - inbytes;
			if (r->outbuf.pktsize > 0)
				outbytes = (outbytes / r->outbuf.pktsize) * r->outbuf.pktsize;	/* Round down to whole packet */
			else
				outbytes = 0;
		}
	}
	else {
		if (outsize != BUFFER_AUTOSIZE) {
			outbytes = outsize * r->outbuf.pktsize;
			inbytes = r->bufsize - outbytes;
			if (r->inbuf.pktsize > 0)
				inbytes = (inbytes / r->inbuf.pktsize) * r->inbuf.pktsize;		/* Round down to whole packet */
			else
				inbytes = 0;
		}
		else {
			return(BUFFER_BADREQUEST);		/* Both in & out buffer sizes automatic */
		}
	}

	ptr = r->buffer_area;		/* Point to start of buffer area */

	if ((inbytes + outbytes) > r->bufsize) {
		return(BUFFER_NOSPACE);	/* Insufficient space */
	}
	else {

		/* Disable realtime transfers whilst updates take place */

		enable = r->enable;
		r->enable = FALSE;

		/* Initialise input buffer */

		r->inbuf.size = inbytes;
		r->inbuf.free = inbytes;
		r->inbuf.used = 0;
		r->inbuf.start = ptr;
		r->inbuf.end = ptr + inbytes;
		r->inbuf.inptr = r->inbuf.start;
		r->inbuf.outptr = r->inbuf.start;

		ptr += inbytes;

		/* Initialise output buffer */

		r->outbuf.size = outbytes;
		r->outbuf.free = outbytes;
		r->outbuf.used = 0;
		r->outbuf.start = ptr;
		r->outbuf.end = ptr + outbytes;
		r->outbuf.inptr = r->outbuf.start;
		r->outbuf.outptr = r->outbuf.start;

		/* Restore realtime transfer state */

		r->enable = enable;
	}
	return(NO_ERROR);
}



/*****************************************************************************
*																			 *
* Function name:	flush_buffer											 *
*																			 *
* Description:		Flush input/output buffers as defined by flags			 *
*																			 *
*****************************************************************************/

void flush_buffer(rtctrl* r, uint32_t flags)
{
	int enable;

	/* Disable realtime transfers whilst updates take place */

	enable = r->enable;
	r->enable = FALSE;

	if (flags & (FLUSH_BOTH_BUFFERS | FLUSH_IN_BUFFER)) {

		/* Flush input buffer */

		r->inbuf.free = r->inbuf.size;
		r->inbuf.used = 0;
		r->inbuf.inptr = r->inbuf.start;
		r->inbuf.outptr = r->inbuf.start;
	}

	if (flags & (FLUSH_BOTH_BUFFERS | FLUSH_OUT_BUFFER)) {

		/* Flush output buffer */

		r->outbuf.free = r->outbuf.size;
		r->outbuf.used = 0;
		r->outbuf.inptr = r->outbuf.start;
		r->outbuf.outptr = r->outbuf.start;
	}

	/* Restore realtime transfer state */

	r->enable = enable;
}



/********************************************************************************
  FUNCTION NAME     : rt_control
  FUNCTION DETAILS  : Control realtime stream transfer.

					  On entry:

					  r			Realtime stream pointer.
					  flags		Control flags

					  On exit:

					  Returns non-zero if an error occurred.

********************************************************************************/

int rtctrl_control(rtctrl* r, uint32_t flags)
{
	if (r) {

		/* If we're enabling turning point mode, then set rt_tpstate to STATE_TP_OUTPUT so that
		   the first turning point is output immediately.
		*/

		if ((r->enable ^ flags) & RT_OUTTSR) {
			rt_tpstate = STATE_TP_OUTPUT;
		}

		r->enable = flags;

		return(NO_ERROR);
	}
	return(ERR_NOSTREAM);
}



/********************************************************************************
  FUNCTION NAME     : rt_pause
  FUNCTION DETAILS  : Pause realtime stream transfer.

					  On entry:

					  r			Realtime stream pointer.
					  flags		Control flags

					  On exit:

					  Returns non-zero if an error occurred.

********************************************************************************/

int rtctrl_pause(rtctrl* r, uint32_t flags)
{
	if (r) {
		rt_control |= (flags | GENERATE_PAUSE);
		return(NO_ERROR);
	}
	return(ERR_NOSTREAM);
}



/********************************************************************************
  FUNCTION NAME     : rt_resume
  FUNCTION DETAILS  : Resume realtime stream transfer.

					  On entry:

					  r			Realtime stream pointer.

					  On exit:

					  Returns non-zero if an error occurred.

********************************************************************************/

int rtctrl_resume(rtctrl* r)
{
	if (r) {
		r->enable = RT_INPUT | RT_OUTPUT;
		return(NO_ERROR);
	}
	return(ERR_NOSTREAM);
}

