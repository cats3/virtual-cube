/********************************************************************************
 * MODULE NAME       : global.c													*
 * MODULE DETAILS    : Global variable declarations								*
 *																				*
 ********************************************************************************/

#include <stddef.h>
#include <stdint.h>

uint32_t gbl_nv_status;	/* Non-volatile memory status */
uint32_t gbl_sys_status;	/* System status */

