﻿# CMakeList.txt : CMake project for virtual-cube, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

include_directories( "include" "HardwareIndependent")
add_compile_definitions("CONTROLCUBE" "_DSPB" "_DSPA" "VIRTUALCUBE")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fpermissive -std=c++11 -w")

file(GLOB CHANNEL_SOURCES "configuration.c" "chan_*.c" "channel.c")
file(GLOB CNET_SOURCES "mock_cnet.c")
file(GLOB SERVER_SOURCES "msgserver.c" "mdnsserver.c" "msghandler.c" "rtmsghandler.c" "rtbuffer.c" "server.c")
file(GLOB HARDWARE_SOURCES "hw_2dig.c" "hw_2gp1sv.c" "hw_4ad2da.c" "hw_digio.c" "hw_mboard.c")
file(GLOB CONTROLLER_SOURCES "controller.c" "hcsupport.c" "global.c" "shared.c")
file(GLOB EVENT_SOURCES "eventrep.c" "eventlog.c")
file(GLOB MISC_SOURCES "virtual-cube.c" "virtual-cube-config.c" "chanproc.c" "mock_hardware.c" "nonvol.c" "virtual-hardware.c" "virtual-eeprom.c" "system.c" "mock_asmcode.c" "hydraulic.c" "msgtable.c" "timer.c")

file (GLOB EXTRA_INSTALL_FILES "vcube.conf" "version.txt")

# Add source to this project's executable.
add_executable (virtual-cube
	${CNET_SOURCES}
	${CONTROLLER_SOURCES}
	${CHANNEL_SOURCES}
	${SERVER_SOURCES}
	${EVENT_SOURCES}
	${MISC_SOURCES}
	${HARDWARE_SOURCES} )

target_link_libraries(virtual-cube cube loader)
target_link_libraries(virtual-cube "pthread" "rt" "dl" "m" "stdc++")


add_executable(chanprocdump "chanprocdump.c")

# TODO: Add tests and install targets if needed.
install(TARGETS virtual-cube DESTINATION bin)
install(FILES "vcube.sh" DESTINATION bin)
install(FILES ${EXTRA_INSTALL_FILES} DESTINATION .)

