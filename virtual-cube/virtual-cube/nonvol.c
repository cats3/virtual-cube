/********************************************************************************
  MODULE NAME   	: nonvol
  PROJECT			: Control Cube / Signal Cube
  MODULE DETAILS  	: Handles operation of non-volatile memory.

  The operation of the non-volatile memory is as follows:

  An area of main memory is used as the static non-volatile (snv) memory. Data
  held in this area is made non-volatile by the nonvol code copying as required
  into the EEPROM area.

  Two areas of EEPROM are used, each holding a complete copy of the snv data.

  Two copy areas are used during the backup process. The active copy is the
  area where data changes are copied. This copy process is performed by a
  background task as part of the nonvol code. The backup copy is the area
  from where data is copied into the EEPROM. The use of two different areas
  prevents updates from occurring part way through the copy process.

  In addition, two map areas, the active map and the backup map are used to
  indicate which locations have been modified by the software. These maps are
  checked so that only those locations which have actually been modified are
  written to the EEPROM. This helps to minimise wear in the EEPROM cells and
  also speeds up the copy process because only the necessary locations are
  written

  During initialisation, the contents of the EEPROM are checked against the
  checksum value stored in the EEPROM and if valid copied to the active copy,
  the backup copy and the snv data areas.

  When a change occurs in the snv data area a call is made to the snv_change()
  function. This sets flags in the active map corresponding to the modified
  locations, indicating that changes have been made. In addition, the updated
  values from the snv data area are copied into the active copy area.

  A background function scans through the backup copy calculating a new checksum
  and through the backup map, looking for changes. When changes are found, the
  new data is copied into the EEPROM. If the active map indicates that no more
  recent changes have occurred in the active copy, then the data is also copied
  into the active copy area. This ensures that the active copy is also kept up
  to date.

  At the end of the scan, the new checksum is written to the EEPROM together
  with an incrementing count. This enables the data restoration code to
  determine which of the two EEPROM banks contains the most recent copy.

********************************************************************************/

#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "porting.h"

//#include <std.h>
//#include <log.h>

/* The non-volatile code version is stored in the data area so that in future
   different formats may be detected and handled correctly if required.
*/

#define NVVERSION	1	/* NV code version					*/

#define NV_DEFAULT_DATA	/* Require default data definition 	*/

#define nvmemcpy memcpy

#include "defines.h"
#include "hardware.h"
#include "nonvol.h"
//#include "debug.h"
//#include "headers.h"
#include "shared.h"
#include "controller.h"
#include "asmcode.h"

typedef struct nv_info {
	uint8_t  ctr;		/* Update counter						*/
	uint8_t  reserved0;
	uint8_t  reserved1;
	uint8_t  reserved2;
	uint32_t size;		/* Size of stored area (in words)		*/
	uint32_t nvversion;	/* NV code version used to write data	*/
	uint32_t fwversion;	/* Firmware version used to write data 	*/
	uint32_t chksum;		/* Checksum of data block				*/
} nv_info;

#define SNV_SIZE 32768				/* Total size of static non-volatile area (bytes)  	*/
#define SNV_COPY (SNV_SIZE-128)		/* Area of snv data copied to EEPROM				*/

#define SNV_UPDATE_BANK1 0x0000		/* Offset to snv update bank 1	   					*/
#define SNV_UPDATE_BANK2 0x8000		/* Offset to snv update bank 2						*/

/* Control Cube battery backed RAM usage

Note that although the RAM device is 32K bytes, the top 8 bytes are used for the timekeeper
registers.

Offset	Use
------  ---

0000

		DNV storage area

7800
		Event log
7F00
		Boot log
7FC0
		Run timer 1
7FD0
		Run timer 2
7FE0
		Reserved
7FF8
		RTC registers
7FFF

*/

#define DNV_SIZE (0x7800/2)			/* Total size of dynamic non-volatile area (bytes)	*/
//#define DNV_COPY (DNV_SIZE-128)		/* Area of dnv data copied to battery-backed RAM	*/
#define DNV_COPY DNV_SIZE			/* Area of dnv data copied to battery-backed RAM	*/

#define DNV_UPDATE_BANK1 0x0000		/* Offset to dnv update bank 1	   					*/
#define DNV_UPDATE_BANK2 DNV_SIZE	/* Offset to dnv update bank 2						*/

#define BANK0 0						/* Bank 0 identifier							   	*/
#define BANK1 1						/* Bank 1 identifier							   	*/

/* The following two offsets define where in the battery-backed RAM, the run time
   counters are stored. There are two copies to provide security if one is corrupted
   during powerdown.

   The two offsets must be such that they can be swapped by inverting one or more
   bits.
*/

#define DNV_RUNTIMER1 0x7FC0		/* Offset to run timer copy 1						*/
#define DNV_RUNTIMER2 0x7FD0		/* Offset to run timer copy 2						*/

/* Check if static and dynamic non-volatile structure size has exceeded the
   allowable space. Need to do it this way because the preprocessor cannot use
   sizeof. This line will generate an error if the size is such that the array
   size is calculated as a negative value.

   If an error occurs in the following statement, then the size of the snv structure
   in nonvol.h is too large to fit in the available space.
*/

typedef char snv_sizecheck[1 - 2 * ((sizeof(snv) > SYS_SNV_SIZE) != 0)];

typedef char dnv_sizecheck[1 - 2 * ((sizeof(dnv) > SYS_DNV_SIZE) != 0)];

/* Static NV data */

static uint32_t snv_data[SNV_SIZE / 4];		/* Static NV data area 							*/

static uint32_t snv_copy1[SNV_SIZE / 4];		/* Static NV copy area 1						*/
static uint32_t snv_map1[(SNV_SIZE / 32) / 4];	/* Static NV area 1 changed flags  				*/

static uint32_t snv_copy2[SNV_SIZE / 4];		/* Static NV copy area 2						*/
static uint32_t snv_map2[(SNV_SIZE / 32) / 4];	/* Static NV area 2 changed flags  				*/

static near uint32_t snv_size = 0;			/* Total size of static NV area (words)			*/
static near uint32_t snv_used = 0;			/* Used size of static NV area (words)			*/

static near uint32_t* snv_active_copy;		/* Pointer to active static NV copy area		*/
static near uint32_t* snv_active_map;		/* Pointer to active static NV map area			*/
static near uint32_t* snv_backup_copy;		/* Pointer to backup static NV copy area		*/
static near uint32_t* snv_backup_map;		/* Pointer to backup static NV map area			*/
static near uint32_t snv_update_bank;		/* Static NV update bank flag					*/
static near uint8_t  snv_update_ctr;		/* Counter for static NV update bank selection	*/
static near uint32_t snv_update_busy = 0;	/* Static NV update in progress					*/

/* Dynamic NV data */

static uint32_t dnv_data[DNV_SIZE / 4];		/* Dynamic NV data area 						*/

#if (defined(CONTROLCUBE) || defined(AICUBE))
static uint32_t dnv_copy1[DNV_SIZE / 4];		/* Dynamic NV copy area 1						*/
static uint32_t dnv_map1[(DNV_SIZE / 32) / 4];	/* Dynamic NV area 1 changed flags  			*/

static uint32_t dnv_copy2[DNV_SIZE / 4];		/* Dynamic NV copy area 2						*/
static uint32_t dnv_map2[(DNV_SIZE / 32) / 4];	/* Dynamic NV area 2 changed flags  			*/

static near uint32_t dnv_size = 0;			/* Total size of dynamic NV area (words)		*/
static near uint32_t dnv_used = 0;			/* Used size of dynamic NV area (words)			*/

static near uint32_t* dnv_active_copy;		/* Pointer to active dynamic NV copy area		*/
static near uint32_t* dnv_active_map;		/* Pointer to active dynamic NV map area		*/
static near uint32_t* dnv_backup_copy;		/* Pointer to backup dynamic NV copy area		*/
static near uint32_t* dnv_backup_map;		/* Pointer to backup dynamic NV map area		*/
static near uint32_t dnv_update_bank;		/* Dynamic NV update bank flag					*/
static near uint8_t  dnv_update_ctr;		/* Counter for dynamic NV update bank selection	*/
static near uint32_t dnv_update_busy = 0;	/* Dynamic NV update in progress				*/
#endif

snv* snvbs = (snv*)snv_data;			/* Pointer to static NV area 					*/
dnv* dnvbs = (dnv*)dnv_data;			/* Pointer to dynamic NV area 					*/

#define NV_INVALID	0xFFAA0055	/* Version number used to force NV corruption */

/* EXPORT_BEGIN */

/* Return codes for check_snv_bank and check_dnv_bank functions */

#define NV_CHKSUM_FAIL	0x01	/* Checksum fail					*/
#define NV_BAD_VERSION	0x02	/* Bad version code					*/
#define NV_SIZE_CHANGE	0x04	/* Size changed						*/

/* Bank is considered corrupt if the checksum has failed or if the
   version code is incorrect.
*/

#define NV_CORRUPT		(NV_CHKSUM_FAIL | NV_BAD_VERSION)

/* EXPORT_END */

uint32_t nv_status = 0;					/* Non-volatile memory status					*/

/* Tables of bit masks used to generate dirty map flags */

static const uint32_t left_bitmask[32] = { 0xFFFFFFFF,		/* Size 32 */
									   0x7FFFFFFF,		/* Size 31 */
									   0x3FFFFFFF,		/* Size 30 */
									   0x1FFFFFFF,		/* Size 29 */
									   0x0FFFFFFF,		/* Size 28 */
									   0x07FFFFFF,		/* Size 27 */
									   0x03FFFFFF,		/* Size 26 */
									   0x01FFFFFF,		/* Size 25 */
									   0x00FFFFFF,		/* Size 24 */
									   0x007FFFFF,		/* Size 23 */
									   0x003FFFFF,		/* Size 22 */
									   0x001FFFFF,		/* Size 21 */
									   0x000FFFFF,		/* Size 20 */
									   0x0007FFFF,		/* Size 19 */
									   0x0003FFFF,		/* Size 18 */
									   0x0001FFFF,		/* Size 17 */
									   0x0000FFFF,		/* Size 16 */
									   0x00007FFF,		/* Size 15 */
									   0x00003FFF,		/* Size 14 */
									   0x00001FFF,		/* Size 13 */
									   0x00000FFF,		/* Size 12 */
									   0x000007FF,		/* Size 11 */
									   0x000003FF,		/* Size 10 */
									   0x000001FF,		/* Size  9 */
									   0x000000FF,		/* Size  8 */
									   0x0000007F,		/* Size  7 */
									   0x0000003F,		/* Size  6 */
									   0x0000001F,		/* Size  5 */
									   0x0000000F,		/* Size  4 */
									   0x00000007,		/* Size  3 */
									   0x00000003,		/* Size  2 */
									   0x00000001 		/* Size  1 */
};

static const uint32_t right_bitmask[64] = { 0x80000000,		/* Size  1 */
										0xC0000000,		/* Size  2 */
										0xE0000000,		/* Size  3 */
										0xF0000000,		/* Size  4 */
										0xF8000000,		/* Size  5 */
										0xFC000000,		/* Size  6 */
										0xFE000000,		/* Size  7 */
										0xFF000000,		/* Size  8 */
										0xFF800000,		/* Size  9 */
										0xFFC00000,		/* Size 10 */
										0xFFE00000,		/* Size 11 */
										0xFFF00000,		/* Size 12 */
										0xFFF80000,		/* Size 13 */
										0xFFFC0000,		/* Size 14 */
										0xFFFE0000,		/* Size 15 */
										0xFFFF0000,		/* Size 16 */
										0xFFFF8000,		/* Size 17 */
										0xFFFFC000,		/* Size 18 */
										0xFFFFE000,		/* Size 19 */
										0xFFFFF000,		/* Size 20 */
										0xFFFFF800,		/* Size 21 */
										0xFFFFFC00,		/* Size 22 */
										0xFFFFFE00,		/* Size 23 */
										0xFFFFFF00,		/* Size 24 */
										0xFFFFFF80,		/* Size 25 */
										0xFFFFFFC0,		/* Size 26 */
										0xFFFFFFE0,		/* Size 27 */
										0xFFFFFFF0,		/* Size 28 */
										0xFFFFFFF8,		/* Size 29 */
										0xFFFFFFFC,		/* Size 30 */
										0xFFFFFFFE,		/* Size 31 */
										0xFFFFFFFF,		/* Size 32 */
										0xFFFFFFFF,		/* Size 33 */
										0xFFFFFFFF,		/* Size 34 */
										0xFFFFFFFF,		/* Size 35 */
										0xFFFFFFFF,		/* Size 36 */
										0xFFFFFFFF,		/* Size 37 */
										0xFFFFFFFF,		/* Size 38 */
										0xFFFFFFFF,		/* Size 39 */
										0xFFFFFFFF,		/* Size 40 */
										0xFFFFFFFF,		/* Size 41 */
										0xFFFFFFFF,		/* Size 42 */
										0xFFFFFFFF,		/* Size 43 */
										0xFFFFFFFF,		/* Size 44 */
										0xFFFFFFFF,		/* Size 45 */
										0xFFFFFFFF,		/* Size 46 */
										0xFFFFFFFF,		/* Size 47 */
										0xFFFFFFFF,		/* Size 48 */
										0xFFFFFFFF,		/* Size 49 */
										0xFFFFFFFF,		/* Size 50 */
										0xFFFFFFFF,		/* Size 51 */
										0xFFFFFFFF,		/* Size 52 */
										0xFFFFFFFF,		/* Size 53 */
										0xFFFFFFFF,		/* Size 54 */
										0xFFFFFFFF,		/* Size 55 */
										0xFFFFFFFF,		/* Size 56 */
										0xFFFFFFFF,		/* Size 57 */
										0xFFFFFFFF,		/* Size 58 */
										0xFFFFFFFF,		/* Size 59 */
										0xFFFFFFFF,		/* Size 60 */
										0xFFFFFFFF,		/* Size 61 */
										0xFFFFFFFF,		/* Size 62 */
										0xFFFFFFFF,		/* Size 63 */
										0xFFFFFFFF,		/* Size 64 */
};


#if 0

/********************************************************************************
  FUNCTION NAME     : cross_check
  FUNCTION DETAILS  : Debug code for cross checking between bank contents.
********************************************************************************/

void cross_check()
{
	uint32_t n;
	uint32_t errors = 0;
	nv_info info1;
	nv_info info2;

	/* Read the information block */

	dsp_i2c_eeprom_readblock(SYS_EEPROM, SNV_UPDATE_BANK1 + SNV_SIZE - sizeof(nv_info), (uint8_t*)&info1, sizeof(nv_info), 0);

	/* Trap for illegal size values */

	if (info1.size > (SNV_COPY / 4)) info1.size = (SNV_COPY / 4);

	/* Read data from EEPROM into temporary area */

	dsp_i2c_eeprom_readblock(SYS_EEPROM, SNV_UPDATE_BANK1, (uint8_t*)snv_copy1, (info1.size * 4), 0);

	/* Read the information block */

	dsp_i2c_eeprom_readblock(SYS_EEPROM, SNV_UPDATE_BANK2 + SNV_SIZE - sizeof(nv_info), (uint8_t*)&info2, sizeof(nv_info), 0);

	/* Trap for illegal size values */

	if (info2.size > (SNV_COPY / 4)) info2.size = (SNV_COPY / 4);

	/* Read data from EEPROM into temporary area */

	dsp_i2c_eeprom_readblock(SYS_EEPROM, SNV_UPDATE_BANK2, (uint8_t*)snv_copy2, (info2.size * 4), 0);

	if (info1.size != info2.size) debug("Cross check: size mismatch %d %d\n", info1.size, info2.size);

	for (n = 0; n < info1.size; n++) {
		if (snv_copy1[n] != snv_copy2[n]) {
			if (errors < 20) debug("Cross check: error offset %d %08X %08X\n", n, snv_copy1[n], snv_copy2[n]);
			errors++;
		}
	}

	debug("Cross check: %d error(s)\n", errors);
}

#endif



/********************************************************************************
  FUNCTION NAME     : check_snv_bank
  FUNCTION DETAILS  : Check the validity of an snv bank and return the update
					  counter value.

					  Uses area pointed to by 'temp' as temporary storage for
					  restored data.

					  Returns NV_CHKSUM_FAIL if the checksum is invalid,
					  NV_SIZE_CHANGE if the size of the non-volatile area
					  has changed, or NV_BAD_VERSION if the version number
					  in the non-volatile header is incorrect.

					  In all other cases returns zero indicating no error.
********************************************************************************/

uint32_t check_snv_bank(uint32_t bank, uint32_t* temp, uint8_t* count, uint32_t* fwversion, uint32_t* nvversion)
{
	uint32_t start;
	uint32_t chk;
	uint32_t chk_size;
	uint32_t n;
	nv_info info;

	start = (bank) ? SNV_UPDATE_BANK2 : SNV_UPDATE_BANK1;

	/* Read the information block */

	dsp_i2c_eeprom_readblock(SYS_EEPROM, start + SNV_SIZE - sizeof(nv_info), (uint8_t*)&info, sizeof(nv_info), 0);

	if (count)     *count = info.ctr;
	if (fwversion) *fwversion = info.fwversion;
	if (nvversion) *nvversion = info.nvversion;

	if (info.nvversion == NV_INVALID) return(NV_BAD_VERSION);

	/* Trap for illegal size values */

	if ((info.size == 0) || (info.size > (SNV_COPY / 4))) return(NV_CHKSUM_FAIL);

	/* Round the size in the info block up to the next multiple of 32 words to use when
	   transferring data and calculating the checksum.
	*/

	chk_size = (info.size + 31) & ~31;

	/* Read data from EEPROM into temporary area */

	dsp_i2c_eeprom_readblock(SYS_EEPROM, start, (uint8_t*)temp, (chk_size * 4), 0);

	/* Calculate checksum on recovered data. */

	chk = 0;
	for (n = 0; n < chk_size; n++) {
		chk += temp[n];
	}

	if (info.size != snv_used) return(NV_SIZE_CHANGE);

	if (chk != info.chksum) return(NV_CHKSUM_FAIL);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : update_snv_bank
  FUNCTION DETAILS  : Update an snv bank by copying data from the snv RAM area
					  to the EEPROM.
********************************************************************************/

void update_snv_bank(uint32_t bank, uint32_t* data, uint8_t count)
{
	uint32_t start;
	uint32_t chk;
	uint32_t n;
	nv_info info;

	start = (bank) ? SNV_UPDATE_BANK2 : SNV_UPDATE_BANK1;

	/* Calculate checksum on stored data. The checksum is calculated on the rounded
	   size (used size adjusted to next multiple of 32 words).
	*/

	chk = 0;
	for (n = 0; n < snv_size; n++) {
		chk += data[n];
	}

	/* Write the data */

	dsp_i2c_eeprom_writeblock(SYS_EEPROM, start, (uint8_t*)data, (snv_size * 4), 0);

	info.ctr = count;
	info.chksum = chk;
	info.fwversion = FWVERSION;
	info.nvversion = NVVERSION;
	info.size = snv_used;

	/* Write the information block */

	dsp_i2c_eeprom_writeblock(SYS_EEPROM, start + SNV_SIZE - sizeof(nv_info), (uint8_t*)&info, sizeof(nv_info), 0);
}



#if (defined(CONTROLCUBE) || defined(AICUBE))

/********************************************************************************
  FUNCTION NAME     : check_dnv_bank
  FUNCTION DETAILS  : Check the validity of a dnv bank and return the update
					  counter value.

					  Uses area pointed to by 'temp' as temporary storage for
					  restored data.

					  Returns NV_CHKSUM_FAIL if the checksum is invalid,
					  NV_SIZE_CHANGE if the size of the non-volatile area
					  has changed, or NV_BAD_VERSION if the version number
					  in the non-volatile header is incorrect.

					  In all other cases returns zero indicating no error.
********************************************************************************/

uint32_t check_dnv_bank(uint32_t bank, uint32_t* temp, uint8_t* count, uint32_t* fwversion, uint32_t* nvversion)
{
	uint32_t start;
	uint32_t chk;
	uint32_t chk_size;
	uint32_t n;
	nv_info info;

	start = (bank) ? DNV_UPDATE_BANK2 : DNV_UPDATE_BANK1;

	/* Read the information block */

	memcpy(&info, (const void*)&bbsram_base[start + DNV_SIZE - sizeof(nv_info)], sizeof(nv_info));

	if (count)     *count = info.ctr;
	if (fwversion) *fwversion = info.fwversion;
	if (nvversion) *nvversion = info.nvversion;

	if (info.nvversion == NV_INVALID) return(NV_BAD_VERSION);

	/* Trap for illegal size values */

	if (info.size > (DNV_COPY / 4)) info.size = (DNV_COPY / 4);

	/* Round the size in the info block up to the next multiple of 32 words to use when
	   transferring data and calculating the checksum.
	*/

	chk_size = (info.size + 31) & ~31;

	/* Read data from battery-backed RAM into temporary area */

	memcpy(temp, (const void*)&bbsram_base[start], (chk_size * 4));

	/* Calculate checksum on recovered data */

	chk = 0;
	for (n = 0; n < chk_size; n++) {
		chk += temp[n];
	}

	if (info.size != dnv_used) return(NV_SIZE_CHANGE);

	if (chk != info.chksum) return(NV_CHKSUM_FAIL);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : update_dnv_bank
  FUNCTION DETAILS  : Update a dnv bank by copying data from the dnv RAM area
					  to the battery-backed RAM.
********************************************************************************/

void update_dnv_bank(uint32_t bank, uint32_t* data, uint8_t count)
{
	uint32_t start;
	uint32_t chk;
	uint32_t n;
	nv_info info;

	start = (bank) ? DNV_UPDATE_BANK2 : DNV_UPDATE_BANK1;

	/* Calculate checksum on stored data. The checksum is calculated on the rounded
	   size (used size adjusted to next multiple of 32 words).
	*/

	chk = 0;
	for (n = 0; n < dnv_size; n++) {
		chk += data[n];
	}

	/* Write the data */

	memcpy((void*)&bbsram_base[start], data, (dnv_size * 4));

	info.ctr = count;
	info.chksum = chk;
	info.fwversion = FWVERSION;
	info.nvversion = NVVERSION;
	info.size = dnv_used;

	/* Write the information block */

	memcpy((void*)&bbsram_base[start + DNV_SIZE - sizeof(nv_info)], &info, sizeof(nv_info));
}

#endif



/********************************************************************************
  FUNCTION NAME     : nv_checkflush
  FUNCTION DETAILS  : Check for flush request in EEPROM data area.

					  A flush is requested by setting the first 8 bytes of the
					  system EEPROM to the string "FlushNow".

********************************************************************************/

uint32_t nv_checkflush(void)
{
	char string[8];

	/* Read the first 8 bytes and check for flush request */

	dsp_i2c_eeprom_readblock(SYS_EEPROM, 0, (uint8_t*)string, 8, 0);
	return(strncmp(string, "FlushNow", 8) == 0);
}



/********************************************************************************
  FUNCTION NAME     : nv_initialise
  FUNCTION DETAILS  : Initialise NV operation.

					  Allocate space for NV copy areas and dirty maps.
					  Read data from EEPROM and check for validity.

					  Returns flag indicating if EEPROM data was corrupt.

					  The flags word controls the operation:

						NVINIT_INIT		Initialisation only, do not access
										NV memory.

						NVINIT_FLUSH	Force flushing by inhibiting restore
										operation and using default settings.

********************************************************************************/

uint32_t nv_initialise(int flags)
{
	int snv_corrupt = FALSE;
	int snv_resize = FALSE;
	int dnv_corrupt = FALSE;
	int dnv_resize = FALSE;
	uint32_t snv_chk1, snv_chk2;
	uint32_t snv_corrupt1, snv_corrupt2;
	uint32_t snv_resize1, snv_resize2;
	uint8_t  snv_ctr1, snv_ctr2;
	uint32_t snv_valid;
#if (defined(CONTROLCUBE) || defined(AICUBE))
	uint32_t dnv_chk1, dnv_chk2;
	uint32_t dnv_corrupt1, dnv_corrupt2;
	uint32_t dnv_resize1, dnv_resize2;
	uint8_t  dnv_ctr1, dnv_ctr2;
	uint32_t dnv_valid;
#endif

	/* Determine total size in words of static and dynamic NV areas rounded to nearest
	   multiple of 32 words (128 bytes).
	*/

	snv_size = ((SYS_SNV_SIZE + CtrlGetSnvSz() + 127) & ~127) >> 2;
#if (defined(CONTROLCUBE) || defined(AICUBE))
	dnv_size = ((sizeof(dnv) + CtrlGetDnvSz() + 127) & ~127) >> 2;
#endif

	/* Determine used size in words of static and dynamic NV areas rounded to nearest word (4 bytes). */

	snv_used = (SYS_SNV_SIZE + CtrlGetSnvSz() + 3) >> 2;
#if (defined(CONTROLCUBE) || defined(AICUBE))
	dnv_used = (sizeof(dnv) + CtrlGetDnvSz() + 3) >> 2;
#endif

	memset(snv_data, 0, SNV_SIZE);
	memset(snv_map1, 0, (SNV_SIZE / 32));		/* Reset all static NV area 1 changed flags */
	memset(snv_map2, 0, (SNV_SIZE / 32));		/* Reset all static NV area 2 changed flags */
	snv_update_ctr = 0;
	snv_update_bank = 0;

#if (defined(CONTROLCUBE) || defined(AICUBE))
	memset(dnv_data, 0, DNV_SIZE);
	memset(dnv_map1, 0, (DNV_SIZE / 32));		/* Reset all dynamic NV area 1 changed flags */
	memset(dnv_map2, 0, (DNV_SIZE / 32));		/* Reset all dynamic NV area 2 changed flags */
	dnv_update_ctr = 0;
	dnv_update_bank = 0;
#endif

	/* Return immediately if this is just an initialisation request */

	if (flags & NVINIT_INIT) return(0);

	/* Restore EEPROM data if required */

	if (!(flags & NVINIT_FLUSH)) {

		/* Determine which bank of EEPROM is the most recent copy */

		snv_chk1 = check_snv_bank(BANK0, snv_copy1, &snv_ctr1, NULL, NULL);
		snv_chk2 = check_snv_bank(BANK1, snv_copy2, &snv_ctr2, NULL, NULL);

		/* Extract the checksum fail and bad version flags which indicate bank corruption */

		snv_corrupt1 = snv_chk1 & NV_CORRUPT;
		snv_corrupt2 = snv_chk2 & NV_CORRUPT;

		/* Extract the size changed flag */

		snv_resize1 = snv_chk1 & NV_SIZE_CHANGE;
		snv_resize2 = snv_chk2 & NV_SIZE_CHANGE;
		snv_resize = snv_resize1 || snv_resize2;

		/* Update the global NV status flags */

		nv_status |= (snv_corrupt1 || snv_resize1) ? SNV_BANK1_CORRUPT : 0;
		nv_status |= (snv_corrupt2 || snv_resize2) ? SNV_BANK2_CORRUPT : 0;
		nv_status |= (snv_resize) ? SNV_RESIZE : 0;

		if (snv_corrupt1 && snv_corrupt2) {	/* Neither bank is valid, so all data is corrupt */
			snv_corrupt = TRUE;
			nv_status |= SNV_CORRUPT;
		}
		else if (!snv_corrupt1 && snv_corrupt2) { snv_valid = BANK0; } 		/* Only bank 0 is valid  */
		else if (!snv_corrupt2 && snv_corrupt1) { snv_valid = BANK1; } 		/* Only bank 1 is valid  */
		else if (((snv_ctr2 + 1) & 0xFF) == snv_ctr1) { snv_valid = BANK0; }	/* Bank 0 is most recent */
		else if (((snv_ctr1 + 1) & 0xFF) == snv_ctr2) { snv_valid = BANK1; }	/* Bank 1 is most recent */
		else if (snv_ctr1 > snv_ctr2) { snv_valid = BANK0; }					/* Bank 0 is most recent */
		else { snv_valid = BANK1; }											/* Bank 1 is most recent */

		/* If any one SNV bank is valid, copy the valid data to the SNV data area and increment the update counter */

		if (!snv_corrupt) {
			memcpy(snv_data, (snv_valid == BANK0) ? snv_copy1 : snv_copy2, SNV_SIZE);
			snv_update_ctr = ((snv_valid == BANK0) ? snv_ctr1 : snv_ctr2) + 1;
			snv_update_bank = (snv_valid == BANK0) ? SNV_UPDATE_BANK1 : SNV_UPDATE_BANK2;
		}

	}

	/* Initialise active and backup pointers */

	snv_active_copy = snv_copy1;
	snv_active_map = snv_map1;

	snv_backup_copy = snv_copy2;
	snv_backup_map = snv_map2;

#if (defined(CONTROLCUBE) || defined(AICUBE))

	/* Restore battery-backed RAM data if required */

	if (!(flags & NVINIT_FLUSH)) {

		/* Determine which bank of battery-backed RAM is the most recent copy */

		dnv_chk1 = check_dnv_bank(BANK0, dnv_copy1, &dnv_ctr1, NULL, NULL);
		dnv_chk2 = check_dnv_bank(BANK1, dnv_copy2, &dnv_ctr2, NULL, NULL);

		/* Extract the checksum fail and bad version flags which indicate bank corruption */

		dnv_corrupt1 = dnv_chk1 & NV_CORRUPT;
		dnv_corrupt2 = dnv_chk2 & NV_CORRUPT;

		/* Extract the size changed flag */

		dnv_resize1 = dnv_chk1 & NV_SIZE_CHANGE;
		dnv_resize2 = dnv_chk2 & NV_SIZE_CHANGE;
		dnv_resize = dnv_resize1 || dnv_resize2;

		/* Update the global NV status flags */

		nv_status |= (dnv_corrupt1 || dnv_resize1) ? DNV_BANK1_CORRUPT : 0;
		nv_status |= (dnv_corrupt2 || dnv_resize2) ? DNV_BANK2_CORRUPT : 0;
		nv_status |= (dnv_resize) ? DNV_RESIZE : 0;

		if (dnv_corrupt1 && dnv_corrupt2) {	/* Neither checksum is valid, so all data is corrupt */
			dnv_corrupt = TRUE;
			nv_status |= DNV_CORRUPT;
		}
		else if (!dnv_corrupt1 && dnv_corrupt2) { dnv_valid = BANK0; } 		/* Only bank 0 is valid  */
		else if (!dnv_corrupt2 && dnv_corrupt1) { dnv_valid = BANK1; } 		/* Only bank 1 is valid  */
		else if (((dnv_ctr2 + 1) & 0xFF) == dnv_ctr1) { dnv_valid = BANK0; }	/* Bank 0 is most recent */
		else if (((dnv_ctr1 + 1) & 0xFF) == dnv_ctr2) { dnv_valid = BANK1; }	/* Bank 1 is most recent */
		else if (dnv_ctr1 > dnv_ctr2) { dnv_valid = BANK0; }					/* Bank 0 is most recent */
		else { dnv_valid = BANK1; }											/* Bank 1 is most recent */

		/* If any one DNV bank is valid, copy the valid data to the DNV data area and increment the update counter */

		if (!dnv_corrupt) {
			memcpy(dnv_data, (dnv_valid == BANK0) ? dnv_copy1 : dnv_copy2, DNV_SIZE);
			dnv_update_ctr = ((dnv_valid == BANK0) ? dnv_ctr1 : dnv_ctr2) + 1;
			dnv_update_bank = (dnv_valid == BANK0) ? 0 : DNV_UPDATE_BANK2;
		}

	}

	/* Initialise active and backup pointers */

	dnv_active_copy = dnv_copy1;
	dnv_active_map = dnv_map1;

	dnv_backup_copy = dnv_copy2;
	dnv_backup_map = dnv_map2;

#endif

	/* Flush appropriate areas to default settings if required */

	if (flags & NVINIT_FLUSH) {

		/* Flush has been requested, set all areas to default settings */

		memset(snv_data, 0, SNV_SIZE); 									/* Initialise SNV data area 		 	   	*/
		memcpy(snv_data, (void*)&snv_default, offsetof(struct snv, end_sys_marker));
		/* Load system default SNV data				*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
		memset(dnv_data, 0, DNV_SIZE); 									/* Initialise DNV data area 		 	   	*/
		memcpy(dnv_data, (void*)&dnv_default, sizeof(dnv_default));		/* Load system default DNV data			   	*/
#endif

		CtrlNormalise();													/* Normalise parameter settings				*/
	}
	else {

		int normalise = FALSE;

#if (defined(CONTROLCUBE) || defined(AICUBE))

		if (dnv_corrupt || dnv_resize1 || dnv_resize2) {

			/* DNV is corrupt or has been resized, set to default settings */

			memset(dnv_data, 0, DNV_SIZE); 									/* Initialise DNV data area 		 	   	*/
			memcpy(dnv_data, (void*)&dnv_default, sizeof(dnv_default));	/* Load system default DNV data			   	*/
			normalise = TRUE;												/* Request normalise of parameter data		*/
		}
#endif
		if (snv_corrupt) {

			/* SNV has failed checksum or has invalid version code, set to default settings */

			memset(snv_data, 0, SNV_SIZE); 									/* Initialise SNV data area 		 	   	*/
			memcpy(snv_data, (void*)&snv_default, offsetof(struct snv, end_sys_marker));
			/* Load system default SNV data				*/
			normalise = TRUE;												/* Request normalise of parameter data		*/
		}
		else if (snv_resize1 || snv_resize2) {

			/* SNV has changed size, retain system area and set remainder to default settings */

			int sys_size = offsetof(struct snv, end_sys_marker);
			memset(snv_data + sys_size, 0, SNV_SIZE - sys_size); 				/* Initialise SNV data area 		 	   	*/
			normalise = TRUE;												/* Request normalise of parameter data		*/
		}

		/* If there was corruption or resizing of either SNV or DNV areas we must normalise the
		   parameter settings to avoid having illegal data.
		*/

		if (normalise) CtrlNormalise();

	}

	/* If any SNV bank is invalid or a flush has been requested, update the EEPROM with current data */

	if (snv_chk1 || (flags & NVINIT_FLUSH)) update_snv_bank(BANK0, snv_data, snv_update_ctr++);
	if (snv_chk2 || (flags & NVINIT_FLUSH)) update_snv_bank(BANK1, snv_data, snv_update_ctr++);

#if (defined(CONTROLCUBE) || defined(AICUBE))

	/* If any DNV bank is invalid or a flush has been requested, update the battery-backed RAM with current data */

	if (dnv_chk1 || (flags & NVINIT_FLUSH)) update_dnv_bank(BANK0, dnv_data, dnv_update_ctr++);
	if (dnv_chk2 || (flags & NVINIT_FLUSH)) update_dnv_bank(BANK1, dnv_data, dnv_update_ctr++);

	//check_dnv_bank(BANK0, dnv_copy1, &dnv_ctr1, NULL, NULL);
	//check_dnv_bank(BANK1, dnv_copy2, &dnv_ctr2, NULL, NULL);

#endif

/* Trap for illegal peak detect cyclic window value and correct */

	if (snvbs->cycle_window == 0.0) snvbs->cycle_window = snv_default.cycle_window;

	/* Ensure both active and backup copy areas are updated from main area */

	memcpy(snv_active_copy, snv_data, SNV_SIZE);
	memcpy(snv_backup_copy, snv_data, SNV_SIZE);

#if (defined(CONTROLCUBE) || defined(AICUBE))

	memcpy(dnv_active_copy, dnv_data, DNV_SIZE);
	memcpy(dnv_backup_copy, dnv_data, DNV_SIZE);

#endif

	nv_update(TRUE); /* Initialise NV updates */

	/* Set flags to mark SNV,DNV updates in progress */

	snv_update_busy = 2;
#if (defined(CONTROLCUBE) || defined(AICUBE))
	dnv_update_busy = 2;
#endif

	return(snv_corrupt || snv_resize || dnv_corrupt || dnv_resize);
}



/********************************************************************************
  FUNCTION NAME     : nv_override
  FUNCTION DETAILS  : Override any NV values that require fixed settings.
********************************************************************************/

void nv_override(void)
{
	//snvbs->cycle_window = snv_default.cycle_window;
	snvbs->pk_rst_count = snv_default.pk_rst_count;
	//update_shared(offsetof(shared, cycle_window), &snvbs->cycle_window, sizeof(snvbs->cycle_window));
	update_shared(offsetof(shared, pk_rst_count), &snvbs->pk_rst_count, sizeof(snvbs->pk_rst_count));
}



/********************************************************************************
  FUNCTION NAME     : nv_getbase
  FUNCTION DETAILS  : Get base address of NV areas.
********************************************************************************/

void nv_getbase(uint32_t** snvbs, uint32_t** dnvbs)
{
	if (snvbs) *snvbs = snv_data;
	if (dnvbs) *dnvbs = dnv_data;
}


//TODO: fully implement _lmbd function
uint32_t _lmbd(int i, uint32_t v)
{
	/* ignores i - search for ones or zeros */
	uint32_t mask = 0x80000000;
	uint32_t p = 0;
	while (mask)
	{
		if (mask & v)
			break;
		p++;
		mask = mask >> 1;
	}
	return p;
}

/********************************************************************************
  FUNCTION NAME     : nv_update
  FUNCTION DETAILS  : Perform one NV update cycle.
********************************************************************************/

void nv_update(uint32_t init)
{
	static uint32_t snvpos = 0;
	static uint32_t snv_chksum;
	static uint32_t snv_modified;
#if (defined(CONTROLCUBE) || defined(AICUBE))
	static uint32_t dnvpos = 0;
	static uint32_t dnv_chksum;
	static uint32_t dnv_modified;
	uint32_t* bptr;
#endif
	uint32_t m;
	uint32_t n;
	uint32_t change;
	uint32_t newchange;
	uint32_t bp;
	uint32_t addr;
	uint32_t* ptr;
	uint32_t istate;
	uint32_t* temp;
	uint32_t p;
	uint32_t mask;
	nv_info info;

	if (init) {
		snvpos = 0;
		snv_chksum = 0;
		snv_modified = FALSE;
#if (defined(CONTROLCUBE) || defined(AICUBE))
		dnvpos = 0;
		dnv_chksum = 0;
		dnv_modified = FALSE;
#endif
		return;
	}

	if (!dsp_i2c_free()) return;

	/* Scan the SNV map entries four at a time so that we can cover the map in a
	   reasonable time. Update the checksum for all the entries being scanned. If any map
	   entries have been changed, then update the EEPROM.
	*/

	for (n = 0; n < 4; n++) {
		if (change = snv_backup_map[snvpos]) {
			/* Change flags are present in this word, so process updates. We find the
			   most significant bit in the change word.
			*/
			bp = _lmbd(1, change); 						/* Find most significant set bit 	*/
			newchange = change & ~(0x80000000U >> bp);	/* Clear bits in change word	 	*/
			snv_backup_map[snvpos] = newchange;			/* And update change map		 	*/

			/* Check if active copy and map need updating with this change */

			p = (snvpos << 5) + bp;   /* Initialise pointer to copy data */
			mask = 0x80000000U >> bp; /* Mask for first word			 */

			istate = disable_int(); /* Disable interrupts whilst updating active copy */

			if ((change & mask) && (!(snv_active_map[snvpos] & mask))) {

				/* Change flag is set in the backup map, but not in the active map. This means
				   that the active copy has no pending updates. In this case, the data from
				   the backup copy must be copied into the active copy to bring it up to date.
				   The flag in the active map is also set to ensure that the active copy is
				   written to the EEPROM when the banks are next swapped.

				   If the flag is set in the active map, then that indicates that a later
				   update has occurred in the active copy, so the backup data must not be
				   copied. The active data will be written to EEPROM when the banks are next
				   swapped.
				*/

				if (snv_active_copy[p] != snv_backup_copy[p]) {
					snv_active_copy[p] = snv_backup_copy[p];
					snv_active_map[snvpos] |= mask;
				}
			}

			restore_int(istate);
			snv_modified = TRUE;

			ptr = &snv_backup_copy[p]; /* Determine address in data area 	   */
			addr = p << 2; 			   /* Calculate byte address within EEPROM */

			/* Write modified word(s) to EEPROM */
			dsp_i2c_eeprom_writeblock(SYS_EEPROM, snv_update_bank + addr, (uint8_t*)ptr++, sizeof(*ptr), 0);

			//dsp_i2c_lock(); /* Obtain lock */
			//dsp_i2c_start();
			//dsp_i2c_eeprom_writeaddr(SYS_EEPROM, snv_update_bank + addr);
			//dsp_i2c_send_word(*ptr++);
			//dsp_i2c_stop();
			//dsp_i2c_unlock(); /* Release lock */
			//dsp_i2c_eeprom_poll(SYS_EEPROM);
			break;
		}
		else {

			/* All changes in this block complete, or there are no changes, so update checksum value */

			for (m = 0; m < 32; m++) {
				snv_chksum += snv_backup_copy[(snvpos << 5) + m];
			}
		}

		snvpos++;
		if (snvpos >= ((snv_size + 31) >> 5)) {

			/* End of scan reached. If EEPROM has been modified, write new checksum and
			   update NV counter. Then swap to the opposite EEPROM bank.
			*/

			if (snv_modified) {
				info.ctr = snv_update_ctr++;
				info.chksum = snv_chksum;
				info.fwversion = FWVERSION;
				info.nvversion = NVVERSION;
				info.size = snv_used;

				dsp_i2c_eeprom_writeblock(SYS_EEPROM, snv_update_bank + SNV_SIZE - sizeof(nv_info), (uint8_t*)&info, sizeof(nv_info), 0);

				snv_update_bank ^= SNV_UPDATE_BANK2;

			}

			/* Swap active/backup RAM pointers and decrement the busy status.
			   Interrupts are disabled during the operation.
			*/

			istate = disable_int();
			temp = snv_active_copy; snv_active_copy = snv_backup_copy; snv_backup_copy = temp;
			temp = snv_active_map; snv_active_map = snv_backup_map; snv_backup_map = temp;
			if (snv_update_busy) snv_update_busy--;
			restore_int(istate);

			/* Restart backup at the start of the data area */

			snvpos = 0;
			snv_chksum = 0;
			snv_modified = FALSE;
		}
	}


	/* Scan the DNV map entries four at a time so that we can cover the map in a
	   reasonable time. Update the checksum for all the entries being scanned. If any map
	   entries have been changed, then update at most two consecutive words in the RAM.
	*/

#if (defined(CONTROLCUBE) || defined(AICUBE))

	for (n = 0; n < 4; n++) {
		if (change = dnv_backup_map[dnvpos]) {
			/* Change flags are present in this word, so process updates. We find the
			   most significant bit in the change word, then also check the next
			   bit, so that we can write two consecutive words if possible.
			*/
			bp = _lmbd(1, change); 						/* Find most significant set bit 	*/
			newchange = change & ~(0xC0000000U >> bp);	/* Clear bits in change word	 	*/
			dnv_backup_map[dnvpos] = newchange;			/* And update change map		 	*/

			/* Check if active copy and map need updating with this change */

			p = (dnvpos << 5) + bp;  /* Initialise pointer to copy data */
			mask = 0x80000000U >> bp; /* Mask for first word				*/

			istate = disable_int(); // Disable interrupts whilst updating active copy
			if ((change & mask) && (!(dnv_active_map[dnvpos] & mask))) {
				if (dnv_active_copy[p] != dnv_backup_copy[p]) {
					dnv_active_copy[p] = dnv_backup_copy[p];
					dnv_active_map[dnvpos] |= mask;
				}
			}

			mask = 0x40000000U >> bp; /* Mask for second word */

			if ((change & mask) && (!(dnv_active_map[dnvpos] & mask))) {
				if (dnv_active_copy[p + 1] != dnv_backup_copy[p + 1]) {
					dnv_active_copy[p + 1] = dnv_backup_copy[p + 1];
					dnv_active_map[dnvpos] |= mask;
				}
			}
			restore_int(istate);
			dnv_modified = TRUE;

			ptr = &dnv_backup_copy[p]; /* Determine address in data area 	   			   */
			addr = p << 2; 			   /* Calculate byte address within battery-backed RAM */

			/* Write modified word(s) to battery-backed RAM */

			bptr = (uint32_t*)&bbsram_base[dnv_update_bank + addr]; /* Point to required area of battery-backed RAM */
			*bptr++ = *ptr++;					/* Write first word								*/
			if (change & mask) {				/* Check if a second word must be written 		*/
				*bptr++ = *ptr++;
			}
			break;
		}
		else {

			/* All changes in this block complete, or there are no changes, so update checksum value */

			for (m = 0; m < 32; m++) {
				dnv_chksum += dnv_backup_copy[(dnvpos << 5) + m];
			}
		}

		dnvpos++;
		if (dnvpos >= ((dnv_size + 31) >> 5)) {

			/* End of scan reached. If RAM has been modified, write new checksum and
			   update NV counter. Then swap to the opposite RAM bank.
			*/

			if (dnv_modified) {
				info.ctr = dnv_update_ctr++;
				info.chksum = dnv_chksum;
				info.fwversion = FWVERSION;
				info.nvversion = NVVERSION;
				info.size = dnv_used;
				bptr = (uint32_t*)&bbsram_base[dnv_update_bank + DNV_SIZE - sizeof(nv_info)];	/* Point to required area of battery-backed RAM */
				memcpy(bptr, &info, sizeof(nv_info));

				dnv_update_bank ^= DNV_UPDATE_BANK2;

			}

			/* Swap active/backup RAM pointers and decrement the busy status.
			   Interrupts are disabled during the operation.
			*/

			istate = disable_int();
			temp = dnv_active_copy; dnv_active_copy = dnv_backup_copy; dnv_backup_copy = temp;
			temp = dnv_active_map; dnv_active_map = dnv_backup_map; dnv_backup_map = temp;
			if (dnv_update_busy) dnv_update_busy--;
			restore_int(istate);

			/* Restart backup at the start of the data area */

			dnvpos = 0;
			dnv_chksum = 0;
			dnv_modified = FALSE;
		}
	}

#endif /* CONTROLCUBE */

	return;
}



#if (defined(CONTROLCUBE) || defined(AICUBE))

/* Offset to current run timer counter in DNV memory */

static int runtimer_select = DNV_RUNTIMER1;

#define LO		0
#define HI		1
#define LO_COPY	2
#define HI_COPY	3

/********************************************************************************
  FUNCTION NAME     : write_runtimer
  FUNCTION DETAILS  : Write run timer count to appropriate DNV memory area.
********************************************************************************/

void nv_write_runtimer(uint32_t timer_lo, uint32_t timer_hi)
{
	uint32_t temp[4];

	temp[LO] = timer_lo;
	temp[HI] = timer_hi;
	temp[LO_COPY] = ~timer_lo;
	temp[HI_COPY] = ~timer_hi;

	memcpy((void*)&bbsram_base[runtimer_select], (const void*)temp, sizeof(temp));
	runtimer_select ^= (DNV_RUNTIMER1 ^ DNV_RUNTIMER2);
}



/********************************************************************************
  FUNCTION NAME     : init_runtimer
  FUNCTION DETAILS  : Initialise run timer variable from DNV memory.
********************************************************************************/

void nv_init_runtimer(uint32_t* timer_lo, uint32_t* timer_hi)
{
	uint32_t temp1[4];
	uint32_t temp2[4];
	int latest;

	memcpy((void*)temp1, (const void*)&bbsram_base[DNV_RUNTIMER1], sizeof(temp1));
	memcpy((void*)temp2, (const void*)&bbsram_base[DNV_RUNTIMER2], sizeof(temp2));

	/* Identify which copies are valid */

	if ((temp1[LO] != ~temp1[LO_COPY]) || (temp1[HI] != ~temp1[HI_COPY])) { temp1[LO] = 0; temp1[HI] = 0; }
	if ((temp2[LO] != ~temp2[LO_COPY]) || (temp2[HI] != ~temp2[HI_COPY])) { temp2[LO] = 0; temp2[HI] = 0; }

	/* The copy with the highest count was the latest to be written, so use that as the
	   current timer count. Initialise the select variable to point to the opposite
	   copy so that the next update will be to the oldest copy.
	*/

	if (temp1[HI] == temp2[HI]) {
		latest = (temp1[LO] > temp2[LO]) ? 1 : 2;
	}
	else {
		latest = (temp1[HI] > temp2[HI]) ? 1 : 2;
	}

	if (latest == 1) {
		*timer_lo = temp1[LO];
		*timer_hi = temp1[HI];
		runtimer_select = DNV_RUNTIMER2;
	}
	else {
		*timer_lo = temp2[LO];
		*timer_hi = temp2[HI];
		runtimer_select = DNV_RUNTIMER1;
	}

}

#endif /* CONTROLCUBE */



/********************************************************************************
  FUNCTION NAME     : nvmemcpy
  FUNCTION DETAILS  : Copy memory block.

					  Define our own memcpy routine so that it can be compiled
					  with the necessary options to prevent interrupts from
					  being disabled for too long by the tight copy loop.

					  This code taken from TI runtime library source.

********************************************************************************/

//void* nvmemcpy(void* dst, const void* src, size_t len)
//{
//	char* restrict d0 = (char*)dst;
//	char* restrict s0 = (char*)src;
//	int            s_aln = ((int)s0 & 0x3);
//	int            d_aln = ((int)d0 & 0x3);
//	int            prebytes;
//	unsigned int   i;
//
//	/*------------------------------------------------------------------------*/
//	/* IF OUR SOURCE AND DESTINATION ARE NOT ALIGNED CORRECTLY FOR WORD       */
//	/* COPIES, OR OUR LENGTH IS LESS THAN EIGHT BYTES, JUST DO A BYTE COPY.   */
//	/*------------------------------------------------------------------------*/
//	if ((s_aln != d_aln) || len < 27)
//	{
//		if (len & 1) *d0++ = *s0++;
//		if (len & 2) { *d0++ = *s0++; *d0++ = *s0++; }
//		if (len & 4) { *d0++ = *s0++; *d0++ = *s0++; *d0++ = *s0++; *d0++ = *s0++; }
//
//		len &= ~7;
//
//		if (len)
//		{
//#pragma MUST_ITERATE(6);
//			for (i = 0; i < len; i++) d0[i] = s0[i];
//		}
//		return dst;
//	}
//
//	/*------------------------------------------------------------------------*/
//	/* PRECOPY 4-S_ALN BYTES, TO ALIGN SRC AND DST TO A WORD BOUNDRY.         */
//	/*------------------------------------------------------------------------*/
//	prebytes = (4 - s_aln) & 3;
//	if (prebytes & 0x1)  *d0++ = *s0++;
//	if (prebytes & 0x2) { _amem2(d0) = _amem2(s0); d0 += 2; s0 += 2; }
//
//	len -= prebytes;
//
//	/*------------------------------------------------------------------------*/
//	/* COPY BYTES IN WORD CHUNKS.                                             */
//	/*------------------------------------------------------------------------*/
//#pragma MUST_ITERATE(6)
//	for (i = 0; i < (len >> 2); i++)
//	{
//		_amem4(d0) = _amem4(s0); d0 += 4; s0 += 4;
//	}
//
//	/*------------------------------------------------------------------------*/
//	/* COPY REMAINING BYTES.                                                  */
//	/*------------------------------------------------------------------------*/
//	if (len & 0x2) { _amem2(d0) = _amem2(s0); d0 += 2; s0 += 2; }
//	if (len & 0x1) *d0 = *s0;
//
//	return dst;
//}



/********************************************************************************
  FUNCTION NAME     : CtrlSnvCB
  FUNCTION DETAILS  : Register change of an area of static NV memory. When an
					  area of memory is marked as changed, that area is copied
					  from the snv_data area into the snv_active_copy area and
					  the snv_active_map is marked as changed.

					  Called as a callback function from controller code.

					  On entry:

					  base		Pointer to start of modified area
					  size		Length of modified area in bytes
********************************************************************************/

void CtrlSnvCB(void* base, uint32_t size)
{
	uint32_t pos;
	uint32_t mask;
	uint32_t p;
	int   s;
	int   e;
	int   width;
	uint32_t istate;

	istate = disable_int(); /* Disable interrupts whilst updating */

	pos = (uint8_t*)base - (uint8_t*)snv_data; /* Position of update area */

	if (pos < (SNV_SIZE - sizeof(nv_info))) {

		/* Copy data to active copy area */

		nvmemcpy((uint8_t*)snv_active_copy + pos, base, size);

		/* Set bits in active map */

		s = pos >> 2;							/* Start of bitfield to set */
		width = ((pos + size - 1) >> 2) - s;	/* Width of bitfield to set */

		s = s & 0x1F;
		e = s + width;

		pos = pos >> 2;			/* Offset in words				*/
		p = pos >> 5;				/* Position within map array    */

		mask = left_bitmask[s];
		while (1) {
			if (e < 32) {
				mask &= right_bitmask[e];
				snv_active_map[p] |= mask;
				break;
			}
			else {
				snv_active_map[p] |= mask;
				mask = 0xFFFFFFFF;
				e -= 32;
				s = 0;
			}
			p++;
		}
		snv_update_busy = 2;		/* Flag update in progress		*/
	}
	restore_int(istate);
}



/********************************************************************************
  FUNCTION NAME     : CtrlDnvCB
  FUNCTION DETAILS  : Register change of an area of dynamic NV memory. When an
					  area of memory is marked as changed, that area is copied
					  from the dnv_data area into the dnv_active_copy area and
					  the dnv_active_map is marked as changed.

					  Called as a callback function from controller code.

					  On entry:

					  base		Pointer to start of modified area
					  size		Length of modified area in bytes
********************************************************************************/

void CtrlDnvCB(void* base, uint32_t size)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	uint32_t pos;
	uint32_t mask;
	uint32_t p;
	int   s;
	int   e;
	int   width;
	uint32_t istate;

	istate = disable_int(); /* Disable interrupts whilst updating */

	pos = (uint8_t*)base - (uint8_t*)dnv_data; /* Position of update area */

	if (pos < (DNV_SIZE - sizeof(nv_info))) {

		/* Copy data to active copy area. */

		nvmemcpy((uint8_t*)dnv_active_copy + pos, base, size);

		/* Set bits in active map */

		s = pos >> 2;							/* Start of bitfield to set */
		width = ((pos + size - 1) >> 2) - s;	/* Width of bitfield to set */

		s = s & 0x1F;
		e = s + width;

		pos = pos >> 2;			/* Offset in words				*/
		p = pos >> 5;				/* Position within map array    */

		mask = left_bitmask[s];
		while (1) {
			if (e < 32) {
				mask &= right_bitmask[e];
				dnv_active_map[p] |= mask;
				break;
			}
			else {
				dnv_active_map[p] |= mask;
				mask = 0xFFFFFFFF;
				e -= 32;
				s = 0;
			}
			p++;
		}
		dnv_update_busy = 2;		/* Flag update in progress		*/
	}
	restore_int(istate);
#endif
}



/********************************************************************************
  FUNCTION NAME     : CtrlValCB
  FUNCTION DETAILS  :

					  On entry:

					  base		Pointer to start of modified area
					  size		Length of modified area in bytes
********************************************************************************/

void CtrlValCB(void* base, uint32_t size)
{
}



/********************************************************************************
  FUNCTION NAME     : nv_control
  FUNCTION DETAILS  : Control NV operation.

					  On entry:

					  flags		Determines the operation to be performed

								Bit 0 = Flush NV
********************************************************************************/

uint32_t nv_control(uint32_t flags)
{
	nv_info info;

	if (flags & NVCTRL_FLUSH) {
		info.ctr = 0;
		info.chksum = 0;
		info.fwversion = FWVERSION;
		info.nvversion = NV_INVALID;
		info.size = 0;

		/* Write the information block to both banks */

		dsp_i2c_eeprom_writeblock(SYS_EEPROM, SNV_UPDATE_BANK1 + SNV_SIZE - sizeof(nv_info), (uint8_t*)&info, sizeof(nv_info), 0);
		dsp_i2c_eeprom_writeblock(SYS_EEPROM, SNV_UPDATE_BANK2 + SNV_SIZE - sizeof(nv_info), (uint8_t*)&info, sizeof(nv_info), 0);
#if (defined(CONTROLCUBE) || defined(AICUBE))
		memcpy((void*)&bbsram_base[DNV_UPDATE_BANK1 + DNV_SIZE - sizeof(nv_info)], &info, sizeof(nv_info));
		memcpy((void*)&bbsram_base[DNV_UPDATE_BANK2 + DNV_SIZE - sizeof(nv_info)], &info, sizeof(nv_info));
#endif
	}
	return(0);
}



/********************************************************************************
  FUNCTION NAME     : nv_get_status
  FUNCTION DETAILS  : Return NV status.

					  On exit:

					  Returns state of NV memory.
********************************************************************************/

uint32_t nv_get_status(void)
{
	return(((snv_update_busy) ? SNV_BUSY : 0) |
		((snv_active_copy == snv_copy2) ? SNV_COPY2 : 0) |
#if (defined(CONTROLCUBE) || defined(AICUBE))
		((dnv_update_busy) ? DNV_BUSY : 0) |
		((dnv_active_copy == dnv_copy2) ? DNV_COPY2 : 0) |
#endif
		nv_status
		);
}



/********************************************************************************
  FUNCTION NAME     : nv_modify_status
  FUNCTION DETAILS  : Modify NV status flags.

					  On exit:

					  Returns new state of NV flags.
********************************************************************************/

uint32_t nv_modify_status(uint32_t set, uint32_t clr)
{

#define FLAG_ALL (FLAG_BADLIST | FLAG_OLDLIST |	FLAG_BADCFG | FLAG_BADCAL)

	nv_status = (nv_status & ~clr) | set;
	slot_nv_status = (slot_nv_status & ~(clr & FLAG_ALL)) | (set & FLAG_ALL);

	return(((snv_update_busy) ? SNV_BUSY : 0) |
		((snv_active_copy == snv_copy2) ? SNV_COPY2 : 0) |
#if (defined(CONTROLCUBE) || defined(AICUBE))
		((dnv_update_busy) ? DNV_BUSY : 0) |
		((dnv_active_copy == dnv_copy2) ? DNV_COPY2 : 0) |
#endif
		nv_status
		);
}



/********************************************************************************
  FUNCTION NAME     : nv_setdata
  FUNCTION DETAILS  : Write data to non-volatile user data area.

					  On entry:

					  start		Start position within data area.
					  length	Length of data to write.
					  data		Pointer to start of data.

********************************************************************************/

uint32_t nv_setdata(int start, int length, char* data)
{
	char* p;

	if ((start < 0) || (start > sizeof(snvbs->user_data))) return(ILLEGAL_STRING);
	if ((start + length) > sizeof(snvbs->user_data)) return(ILLEGAL_STRING);

	p = &snvbs->user_data[start];
	memcpy(p, data, length);
	CtrlSnvCB(p, length);
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : nv_readdata
  FUNCTION DETAILS  : Read data from non-volatile user data area.

					  On entry:

					  start		Start position within data area.
					  length	Length of data to read.
					  data		Pointer to start of data.

********************************************************************************/

uint32_t nv_readdata(int start, int length, char* data)
{
	char* p;

	if ((start < 0) || (start > sizeof(snvbs->user_data))) return(ILLEGAL_STRING);
	if ((start + length) > sizeof(snvbs->user_data)) return(ILLEGAL_STRING);

	p = &snvbs->user_data[start];
	memcpy(data, p, length);
	return(NO_ERROR);
}
