/********************************************************************************
 * MODULE NAME       : eventrep.c												*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Event reporting.											*
 ********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>

#include <stdint.h>
#include "porting.h"
#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <errno.h>

#include "shared.h"

#include "defines.h"
#include "msgstruct.h"
#include "msgserver.h"
#include "eventrep.h"
#include "controller.h"
#include "debug.h"
#include "cnet.h"
#include "asmcode.h"

#define NEVENT 			4		/* Number of event connections			*/
#define QSIZE			4096	/* Size of event queue					*/

 /* Flags used in event connection structure */

#define EVENT_ACTIVE	0x01	/* Connection is active					*/
#define EVENT_REMOTE	0x02	/* Connection is via remote gateway		*/
#define EVENT_CONFIG	0x04	/* Event connection is being configured	*/

struct event_queue_s {
	uint32_t event_queue[TP_QUEUE_SIZE];
	uint32_t* tp_q_head;
	uint32_t* tp_q_tail;
};
extern struct event_queue_s* event_queue_ptr;

/* Define structure used to hold queue information */

typedef struct queue {
	int free;						/* Number of free bytes in  	*/
	char* head;					/* Head of event queue 			*/
	char* tail;					/* Tail of event queue 			*/
	char queue[QSIZE];			/* Queue of event reports 		*/
} queue;

/* Define structure used to hold event connection details */

typedef struct event_connection {
	int flags;					/* Control flags:
									  Bit  0:		In use
									  Bit  1:		Remote connection
									  Bits 2-31:	Reserved						*/
	unsigned int ipaddr;			/* IP address of event requester 				*/
	short port;					/* Port number of event requester 				*/
	connection* conn;				/* Connection used to create					*/
	int socket;					/* Socket used for connection					*/
	int cnet;						/* CNet address of gateway (if remote)			*/
	uint32_t interval;				/* Low-priority update interval					*/
	uint32_t lastupdate;				/* Last low-priority update time				*/
	queue hqueue;					/* Queue of high_priority event reports 		*/
	queue lqueue;					/* Queue of low-priority event reports 			*/
} event_connection;

static int data_true = 1;
static struct event_connection connlist[NEVENT];

/* The following variables and functions are used for test code within the event report module */

static int evtctr = 0;
static int timctr = 0;
static int event_testcode_bitmask = 0;
static int event_TPerror_bitmask = 0;
static int event_GenStatus_bitmask = 0;
static int event_CycPk_bitmask = 0;
extern int event_hyd_bitmask;
extern int event_hcbutton_bitmask;

static int send_errors = 0;

/* The following list holds the addresses of the bitmasks controlling
   event reporting from all available non-parameter event sources.
*/

static int* event_bitmask_list[] = {
	NULL,						/* 0 = Unused					*/
	&event_testcode_bitmask,	/* 1 = Test code (timer)		*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	&event_hyd_bitmask,			/* 2 = Hydraulic state			*/
	&event_hcbutton_bitmask,	/* 3 = Hand controller button	*/
	&event_hyd_bitmask,			/* 4 = Hydraulic tracking		*/
#else
	NULL,						/* 2 = Unused					*/
	NULL,						/* 3 = Unused					*/
	NULL,						/* 4 = Unused					*/
#endif
	&event_TPerror_bitmask,		/* 5 = Turning point error		*/
	&event_GenStatus_bitmask,	/* 6 = Generator status			*/
	&event_CycPk_bitmask		/* 7 = Cyclic peak				*/
};


/********************************************************************************
  FUNCTION NAME   	: event_copy
  FUNCTION DETAILS  : Copy all event reports from a connection queue into a
					  contiguous area of memory. Update the connection queue
					  pointers.

					  On entry:

					  dst	Points to the destination memory area
					  q		Points to the event queue structure
					  size	Holds the size of the destination memory area

					  Returns pointer to the next free location after the
					  copied area in memory.

********************************************************************************/

char* event_copy(char* dst, queue* q, int size)
{
	char* tail;
	int qfree;
	int s1;
	int length;
	int total;
	int istate;
	int qremoved = 0;

	istate = disable_int();
	tail = q->tail;
	qfree = q->free;
	restore_int(istate);

	/* Calculate total amount of data in queue */

	total = QSIZE - qfree;

	while (total > 0) {

		/* Get length of next event report in queue */

		length = ((event_report*)tail)->length;

		/* Check if report will fit into remaining buffer space */

		if (length > size) break;

		total -= length;

		size -= length;
		s1 = q->queue + QSIZE - tail;
		if (length >= s1) {

			/* Report data wraps at the end of the queue. So copy first part, then
			   reset tail pointer to the start of the queue.
			*/

			memcpy(dst, tail, s1);
			length -= s1;
			dst += s1;
			tail = q->queue;
			qremoved += s1;
		}
		if (length) {
			memcpy(dst, tail, length);
			dst += length;
		}

		tail += length;
		qremoved += length;
	}

	istate = disable_int();
	q->tail = tail;
	q->free += qremoved;
	restore_int(istate);

	return(dst);
}



/********************************************************************************
  FUNCTION NAME   	: event_connection_disable_reports
  FUNCTION DETAILS  : Disable all event reports associated with a connection.
********************************************************************************/

void event_connection_disable_reports(event_connection* event, int index)
{
	int n;
	int bitmask = 1 << index;
	int istate;

	/* Interrupts are disabled whilst modifying connection state */

	istate = disable_int();

	/* Disable all event reports for the connection being closed */

	for (n = 0; n <= EVENT_CYCPK; n++) {
		int* maskptr = event_bitmask_list[n];
		if (maskptr) {
			*maskptr &= ~((bitmask << 16) | bitmask);
		}
	}

	/* Clear event mask bits in the parameter code. Note that both low and
	   high priority mask bits must be passed to the CtrlClearEventMaskBits
	   function.
	*/

	CtrlClearEventMaskBits(0x10001 << index);
	event->flags = 0;

	/* Restore interrupt state */

	restore_int(istate);

	/* Check if all cyclic event reports have been disabled. If so, then
	   make sure that the cyclic event flag is cleared for all channels.
	*/

	if (event_CycPk_bitmask == 0) {
		chandef* c;
		int chan;

		for (chan = CHANTYPE_INPUT; chan < (CHANTYPE_INPUT + totalinchan); chan++) {
			if (c = chanptr(chan)) {
				if (c->ctrl & FLAG_CYCEVENT) {

					/* Cyclic event flag is set, so disable and update shared definition */

					istate = disable_int(); /* Disable interrupts whilst updating */

					c->ctrl &= FLAG_CYCEVENT;

					/* Update flag setting in shared channel definition */

					update_shared_channel_parameter(c, offsetof(chandef, ctrl), sizeof(int));

					restore_int(istate);

				}
			}
		}

	}

}



/********************************************************************************
  FUNCTION NAME   	: event_connection_closing
  FUNCTION DETAILS  : Called when a computer connection is closing. All
					  associated event connections must also be closed.
********************************************************************************/

void event_connection_closing(connection* conn)
{
	int index;
	event_connection* e;

	for (index = 0; index < NEVENT; index++) {
		if (conn == connlist[index].conn) {
			e = &connlist[index];
			if (e->flags & EVENT_ACTIVE) {
				close(e->socket);

				/* Disable all event reports for the connection being closed */

				event_connection_disable_reports(e, index);
			}
		}
	}
}



/********************************************************************************
  FUNCTION NAME   	: event_send
  FUNCTION DETAILS  : Send event to socket or CNet.
********************************************************************************/

int event_send(event_connection* c, queue* q, int index)
{
	uint8_t msgbuffer[1024];
	uint32_t msglength;
	uint8_t replybuffer[1024];
	uint32_t replylength;
	char* msg;
	commhdr* hdr;
	int err = 0;
	while (q->free < QSIZE) {
		hdr = (commhdr*)msgbuffer;
		if (c->flags & EVENT_REMOTE) {
			hdr->dst = c->cnet;
			hdr->pkttype = PKT_EVENT | index;
			msg = event_copy((char*)(hdr + 1), q, sizeof(msgbuffer) - sizeof(commhdr));
			msglength = msg - (char*)hdr;
			hdr->length = msglength - sizeof(commhdr);
			err = cnet_async_transmit(msgbuffer, replybuffer, msglength, &replylength, TRUE);
		}
		else {
			hdr->dst = 0;
			hdr->pkttype = PKT_EVENT;
			msg = event_copy((char*)(hdr + 1), q, sizeof(msgbuffer) - sizeof(commhdr));
			msglength = msg - (char*)hdr;
			hdr->length = msglength - sizeof(commhdr);
//			err = do_socketwrite(c->socket, hdr, msglength, NULL, NULL);
			err = send(c->socket, hdr, msglength, 0);
#if 0
			if (err) {

				/* Error occurred sending to server. Flush any pending event reports and
				   call the clear_report() function for each event source.
				*/

				c->flags = 0;
			}
#endif
		}
	}
	return(err);
}



/********************************************************************************
  FUNCTION NAME   	: event_initialise
  FUNCTION DETAILS  : Initialise event management structures.
********************************************************************************/

void event_initialise(void)
{
	event_connection* c;
	int index;

	/* Initialise all event connections to invalid */

	for (index = 0; index < NEVENT; index++) {
		c = &connlist[index];
		c->flags = 0;
		c->cnet = 0;
		c->ipaddr = 0;
		c->port = 0;
		c->interval = 0;
		c->hqueue.free = QSIZE;
		c->hqueue.head = c->hqueue.queue;
		c->hqueue.tail = c->hqueue.queue;
		c->lqueue.free = QSIZE;
		c->lqueue.head = c->lqueue.queue;
		c->lqueue.tail = c->lqueue.queue;
	}

}



/********************************************************************************
  FUNCTION NAME   	: event_reporter
  FUNCTION DETAILS  : Task used for reporting events to an attached PC.
********************************************************************************/

void* event_reporter(void* arg)
{
	event_connection* c;
	int index;

	/* Main task loop. */

	while (1) {
		/* Loop around the event connections looking for data to send */
		for (index = 0; index < NEVENT; index++) {
			c = &connlist[index];
			if (c->flags & EVENT_ACTIVE) {

				/* Process high-priority queue */
				event_send(c, &c->hqueue, index);

				/* Process low-priority queue */
				if ((CNET_MEM_READ[CNET_DATASLOT_TIMELO] - c->lastupdate) >= c->interval) {
					c->lastupdate += c->interval;
					CtrlEventPoll(1 << index);
					event_send(c, &c->lqueue, index);
				}
			}
		}
		usleep(1000);
	}
}



/********************************************************************************
  FUNCTION NAME   	: event_open_connection
  FUNCTION DETAILS  : Open a connection to report event.
********************************************************************************/

int event_open_connection(connection* conn, unsigned int ipaddr, short port,
	int handle, uint32_t update)
{
	int istate;
	event_connection* c;
	struct sockaddr_in servaddr;
	uint32_t position;
	uint32_t ringsize;
	int index;
	int n;
	uint8_t msgbuffer[256];
	uint8_t replybuffer[256];
	uint32_t msglength;
	uint32_t replylength;
	commhdr* hdr;
	struct msg_open_event_connection* msg;
	int errno;

	/* The handle parameter is used to determine if this connection request is
	   from a local PC connected directly to the Control Cube or if it has been
	   passed on by another Control Cube which does have a direct connection.

	   If the request was passed on by another Control Cube, then the handle
	   contains the information required to update the local list to match that
	   in the gateway Cube.
	*/

	if (handle) {
		istate = disable_int();
		c = &connlist[handle & (NEVENT - 1)];
		c->flags = EVENT_ACTIVE | EVENT_REMOTE;
		c->interval = (update * 4096) / 1000;
		c->lastupdate = CNET_MEM_READ[CNET_DATASLOT_TIMELO];
		c->hqueue.free = QSIZE;
		c->hqueue.head = c->hqueue.queue;
		c->hqueue.tail = c->hqueue.queue;
		c->lqueue.free = QSIZE;
		c->lqueue.head = c->lqueue.queue;
		c->lqueue.tail = c->lqueue.queue;
		c->cnet = (handle >> 8) & 0x1F;
		restore_int(istate);
	}
	else {

		/* Interrupts are disabled during the search for a free event connection to
		   prevent possible errors if a second search was started before the first
		   had completed.
		*/

		istate = disable_int();

		for (index = 0; index < NEVENT; index++) {
			if (!(connlist[index].flags & (EVENT_ACTIVE | EVENT_CONFIG))) {

				/* Free connection found. Configure parameters. */

				c = &connlist[index];
				c->flags = EVENT_CONFIG;
				restore_int(istate);
				c->ipaddr = ipaddr;
				c->port = port;
				c->conn = conn;
				c->interval = (update * 4096) / 1000;
				c->lastupdate = CNET_MEM_READ[CNET_DATASLOT_TIMELO];
				c->hqueue.free = QSIZE;
				c->hqueue.head = c->hqueue.queue;
				c->hqueue.tail = c->hqueue.queue;
				c->lqueue.free = QSIZE;
				c->lqueue.head = c->lqueue.queue;
				c->lqueue.tail = c->lqueue.queue;

				//if ((c->socket = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0)) < 0) {
				if ((c->socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
					c->flags = 0;
					return(0);
				}

				/* Enable non-blocking operation */

		  //	  socketioctl(c->socket, FIONBIO, &data_true, NULL);

				/* Enable non-blocking operation, set keepalive option and inhibit
				   delayed ACK operation.
				*/

				// not required as we can set non-blocking in the socket creation
				//socketioctl(c->socket, FIONBIO, &data_true, NULL);
				setsockopt(c->socket, SOL_SOCKET, SO_KEEPALIVE, &data_true, sizeof(data_true));
				setsockopt(c->socket, IPPROTO_TCP, TCP_NODELAY, &data_true, sizeof(data_true));

				/* Connect to remote device */

				memset(&servaddr, 0, sizeof(servaddr));
				servaddr.sin_family = AF_INET;
				servaddr.sin_port = htons(port);
				servaddr.sin_addr.s_addr = ipaddr;

				if (connect(c->socket, (const struct sockaddr*)&servaddr, sizeof(servaddr)) < 0) {
					perror("connect");
					close(c->socket);
					c->flags = 0;
					return(0);
				}

				cnet_get_info(&ringsize, &position);

				/* Send connection update to all other CNet devices */

				if (ringsize > 1) {
					for (n = 0; n < ringsize; n++) {
						if (n != position) {
							hdr = (commhdr*)msgbuffer;
							hdr->dst = n;
							hdr->pkttype = 0;
							msg = (struct msg_open_event_connection*)(hdr + 1);
							msg->command = MSG_OPEN_EVENT_CONNECTION;
							msg->flags = 0;
							msg->ipaddr = 0;
							msg->port = 0;
							msg->handle = 0x80000000 | (position << 8) | index;
							msg->update = update;
							msglength = offsetof(struct msg_open_event_connection, end) + sizeof(commhdr);
							hdr->length = msglength;

							if (cnet_async_transmit(msgbuffer, replybuffer, msglength, &replylength, FALSE) != NO_ERROR) return(0);
						}
					}
				}

				c->flags = EVENT_ACTIVE;

				/* Return connection handle */

				return(0x40000000 | index);
			}
		}

		restore_int(istate);
	}

	return(0);
}



/********************************************************************************
  FUNCTION NAME   	: event_close_connection
  FUNCTION DETAILS  : Close a connection to report event.
********************************************************************************/

int event_close_connection(int handle)
{
	//int istate;
	int index;
	uint32_t ringsize;
	uint32_t position;
	event_connection* c;
	uint8_t msgbuffer[256];
	uint8_t replybuffer[256];
	uint32_t msglength;
	uint32_t replylength;
	int msgerr;
	commhdr* hdr;
	struct msg_close_event_connection* msg;
	int n;

	index = handle & (NEVENT - 1);
	c = &connlist[index];

	/* The handle parameter is used to determine if this close connection request
	   is from a local PC connected directly to the Control Cube or if it has been
	   passed on by another Control Cube which does have a direct connection.

	   If the request was passed on by another Control Cube, then the handle
	   contains the information required to update the local list to match that
	   in the gateway Cube.
	*/


	if (handle & 0x80000000) {
		event_connection_disable_reports(c, index);
	}
	else {
		event_connection_disable_reports(c, index);

		/* Close the connected socket */

		close(c->socket);

		/* Send connection update to all other CNet devices */

		cnet_get_info(&ringsize, &position);
		if (ringsize > 1) {
			for (n = 0; n < ringsize; n++) {
				if (n != position) {
					hdr = (commhdr*)msgbuffer;
					hdr->dst = n;
					hdr->pkttype = 0;
					msg = (struct msg_close_event_connection*)(hdr + 1);
					msg->command = MSG_CLOSE_EVENT_CONNECTION;
					msg->handle = 0x80000000 | index;
					msglength = offsetof(struct msg_close_event_connection, end) + sizeof(commhdr);
					hdr->length = msglength;

					if (msgerr = cnet_async_transmit(msgbuffer, replybuffer, msglength, &replylength, FALSE)) return(msgerr);
				}
			}
		}
	}

	return(0);
}


/********************************************************************************
  FUNCTION NAME   	: event_add_report
  FUNCTION DETAILS  : Add an event report to an event connection.

					  On entry:

					  handle	Event connection handle for event report
					  priority	Event priority level (0 = low, 1 = high)
					  event		Event type
					  size		Size of additional data area (if any)
					  data		Pointer to any additional data required to
								configure event report (NULL = no data)

********************************************************************************/

int event_add_report(int handle, int priority, int event, int size, uint32_t* data)
{
	if (event >= EVENT_PARAMETER) {
	}
	else if (event <= EVENT_CYCPK) {
		int* maskptr = event_bitmask_list[event];
		if (maskptr) {
			int bitmask = event_handle_to_bitmask(handle);
			*maskptr |= ((priority == EVENT_PHIGH) ? (bitmask << 16) : bitmask);
		}
	}
	return(0);
}



/********************************************************************************
  FUNCTION NAME   	: event_remove_report
  FUNCTION DETAILS  : Remove an event report from an event connection.

					  On entry:

					  handle	Event connection handle for event report.
					  event		Event type

********************************************************************************/

int event_remove_report(int handle, int event)
{
	if (event >= EVENT_PARAMETER) {
	}
	else if (event <= EVENT_CYCPK) {
		int* maskptr = event_bitmask_list[event];
		if (maskptr) {
			int bitmask = event_handle_to_bitmask(handle);
			*maskptr &= ~((bitmask << 16) | bitmask);
		}
	}

	return(0);
}



/********************************************************************************
  FUNCTION NAME   	: event_handle_to_bitmask
  FUNCTION DETAILS  : Convert an event reporter handle to a bitmask.
********************************************************************************/

int event_handle_to_bitmask(int handle)
{
	return(1 << (handle & (NEVENT - 1)));
}



/********************************************************************************
  FUNCTION NAME   	: event_queue_report
  FUNCTION DETAILS  : Add report to queue.
********************************************************************************/

int event_queue_report(event_report* report, int size, queue* q)
{
	int istate;
	int s;
	int s1;
	char* p;
	int err = TRUE;

	/* Interrupts are disabled whilst manipulating the connection queue to prevent
	   possible errors if a second report is sent before the first has completed.
	*/

	istate = disable_int();
	if (q->free >= size) {

		p = (char*)report;
		s = size;
		q->free -= size;

		/* Determine free space before wraparound of the queue occurs */

		s1 = q->queue + QSIZE - q->head;
		if (s >= s1) {

			/* Report data will wraparound the end of the queue. So copy first part, then
			   reset head pointer to the start of the queue.
			*/

			memcpy(q->head, p, s1);
			s -= s1;
			p += s1;
			q->head = q->queue;
		}
		if (s) memcpy(q->head, p, s);
		q->head += s;
		err = FALSE;
	}
	restore_int(istate);

	return(err);
}



/********************************************************************************
  FUNCTION NAME   	: event_send_report
  FUNCTION DETAILS  : Report an event via an event connection.
********************************************************************************/

int event_send_report(int bitmask, event_report* report, int size)
{
	event_connection* c;
	int index;
	uint32_t position;
	int err = 0;

	if (!bitmask) return(0);

	cnet_get_info(NULL, &position);

	/* Update report structure with local CNet address */

	report->src = position;

	for (index = 0; index < NEVENT; index++) {
		c = &connlist[index];
		if (c->flags & EVENT_ACTIVE) {

			/* Check for high/low priority event and add to queues as required */

			if (bitmask & (0x10000 << index)) {
				if (event_queue_report(report, size, &c->hqueue)) err |= (bitmask & (0x10000 << index));
			}
			if (bitmask & (1 << index)) {
				if (event_queue_report(report, size, &c->lqueue)) err |= (bitmask & (1 << index));
			}
		}
	}

	if (err) {
		send_errors++;
	}
	return(err);
}



/********************************************************************************
  FUNCTION NAME   	: event_process_remote_event
  FUNCTION DETAILS  : Process a remote event report received via CNet.

					  Remote events are always placed on the high priority
					  queue because prioritisation has already been performed
					  at the event source and therefore all remote events
					  must be transmitted as soon as possible.
********************************************************************************/

int event_process_remote_event(int handle, event_report* report, int size)
{
	int istate;
	event_connection* c;
	int index;
	int s1;
	char* p;

	index = handle & (NEVENT - 1);
	c = &connlist[index];
	if (c->flags & EVENT_ACTIVE) {

		p = (char*)report;

		/* Interrupts are disabled whilst checking the free space in the connection
		   queue to prevent possible errors if a second report is sent before the first
		   had completed.
		*/

		istate = disable_int();
		if (c->hqueue.free < size) {
			restore_int(istate);
			return(EVENT_NOSPACE);
		}
		c->hqueue.free -= size;

		/* Determine free space before wraparound of the queue occurs */

		s1 = c->hqueue.queue + QSIZE - c->hqueue.head;
		if (size >= s1) {

			/* Report data will wraparound the end of the queue. So copy first part, then
			   reset head pointer to the start of the queue.
			*/

			memcpy(c->hqueue.head, p, s1);
			size -= s1;
			p += s1;
			c->hqueue.head = c->hqueue.queue;
		}
		if (size) memcpy(c->hqueue.head, p, size);
		c->hqueue.head += size;
		restore_int(istate);
	}
	return(0);
}



// void netprintf(int s, char* fmt, ...);

/********************************************************************************
  FUNCTION NAME   	: event_show_reportlist
  FUNCTION DETAILS  : Display event report connection list.
********************************************************************************/

void showreport(char* args[], int numargs, int skt)
{
	// not required in virtual cube?
	//int n;
	//int active;
	//int remote;
	//char gateway[32];
	//event_connection* c;

	//for (n = 0; n < NEVENT; n++) {
	//	c = &connlist[n];
	//	active = c->flags & EVENT_ACTIVE;
	//	remote = c->flags & EVENT_REMOTE;
	//	sprintf(gateway, " gateway %d", c->cnet);
	//	netprintf(skt, "Connection %d %s type:%s%s\n\r", n,
	//		(active) ? "active  " : "inactive",
	//		(remote) ? "remote" : "local",
	//		(remote) ? gateway : "");
	//	if (active) {
	//		if (!remote) {
	//			netprintf(skt, "  IP:   %d.%d.%d.%d\n\r", ((uint8_t*)&c->ipaddr)[0],
	//				((uint8_t*)&c->ipaddr)[1],
	//				((uint8_t*)&c->ipaddr)[2],
	//				((uint8_t*)&c->ipaddr)[3]);
	//			netprintf(skt, "  Port: %u\n\r", c->port);
	//		}
	//		netprintf(skt, "  Interval  : %d\n\r", c->interval);
	//		netprintf(skt, "  Last Upd  : %d\n\r", c->lastupdate);
	//		netprintf(skt, "  High Start: %08X\n\r", c->hqueue);
	//		netprintf(skt, "  High Head : %08X\n\r", c->hqueue.head);
	//		netprintf(skt, "  High Tail : %08X\n\r", c->hqueue.tail);
	//		netprintf(skt, "  High Free : %d\n\r", c->hqueue.free);
	//		netprintf(skt, "  Low  Start: %08X\n\r", c->lqueue);
	//		netprintf(skt, "  Low  Head : %08X\n\r", c->lqueue.head);
	//		netprintf(skt, "  Low  Tail : %08X\n\r", c->lqueue.tail);
	//		netprintf(skt, "  Low  Free : %d\n\r", c->lqueue.free);
	//	}
	//}

	//netprintf(skt, "\n\r");
	//netprintf(skt, "Function event_send_report errors: %d\n\r", send_errors);

}


static uint32_t read_from_queue(void)
{
	uint32_t data = *event_queue_ptr->tp_q_tail++;
	if (event_queue_ptr->tp_q_tail >= &event_queue_ptr->event_queue[TP_QUEUE_SIZE])
		event_queue_ptr->tp_q_tail = event_queue_ptr->event_queue;
	return data;
}


/********************************************************************************
  FUNCTION NAME   	: event_background
  FUNCTION DETAILS  : Background code for event reporting, called at 20ms
					  intervals to generate a timed event report every second
					  if enabled and to check for events in the DSP B queue.
********************************************************************************/

int event_background(void)
{
	char block[256];
	event_report* report = (event_report*)block;
	int tp_tail;
	int tp_head;
	int count = 32; /* Maximum number of DSP B queued events processed per interrupt */

	/* Increment local timer and check for 1 second elapsed time */

	timctr++;
	if (timctr >= 50) {
		timctr = 0;
		evtctr++;
		if (event_testcode_bitmask) {
			report->src = 0;
			report->flags = 0;
			report->length = offsetof(event_report, data) + sizeof(int);
			report->type = EVENT_TIMER;
			*((int*)(&report->data[0])) = evtctr;
			event_send_report(event_testcode_bitmask, report, report->length);
		}
	}

//#if (defined(CONTROLCUBE) || defined(AICUBE))
#if 1
	/* Check for event reports in the DSP B queue */

	//tp_tail = hpi_read4((uint32_t*)tp_q_tail_pt);
	//tp_head = hpi_read4((uint32_t*)tp_q_head_pt);
	while (count-- && (event_queue_ptr->tp_q_head != event_queue_ptr->tp_q_tail)) {
		uint32_t* p = (uint32_t*)&report->data[0];	/* Pointer to report data area			 	  */
		uint32_t TPerror;							/* Temporary copy of TPerror value from queue */

		report->src = 0;
		report->flags = 0;
		report->length = offsetof(event_report, data) + 4 * sizeof(int);

		// Values are stored in little endian form, so low half must go first
		*p++ = read_from_queue();	//TimeLo // hpi_read4((uint32_t*)tp_tail); //TimeLo
		////tp_tail += 4;
		//if ((tp_tail) >= event_queue[TP_QUEUE_SIZE]) {
		//	//then go loop back
		//	tp_tail = event_queue;
		//}

		*p++ = read_from_queue();	// TimeHi	 hpi_read4((uint32_t*)tp_tail); //TimeHi
		//tp_tail += 4;
		//if ((tp_tail) >= TP_QUEUE_END) {
		//	//then go loop back
		//	tp_tail -= TP_QUEUE_SIZE;
		//}

		*p++ = read_from_queue();	// TPcount hpi_read4((uint32_t*)tp_tail); //TPcount
		//tp_tail += 4;
		//if ((tp_tail) >= TP_QUEUE_END) {
		//	//then go loop back
		//	tp_tail -= TP_QUEUE_SIZE;
		//}

		TPerror = read_from_queue();	//Tperror	// hpi_read4((uint32_t*)tp_tail); //TPerror
		//tp_tail += 4;
		//if ((tp_tail) >= TP_QUEUE_END) {
		//	//then go loop back
		//	tp_tail -= TP_QUEUE_SIZE;
		//}

		//hpi_write4((uint32_t*)tp_q_tail_pt, tp_tail);

		/* If TPerror is a NaN of the form 0x7FC00000 - 0x7FC0000F, then this event is a genstatus message
		   If TPerror is a NaN of the form 0x7FC00100 - 0x7FC001FF,	then this event is a cyclic peak meessage
		*/

		if ((TPerror & ~0x0F) == 0x7FC00000) {
			if (event_GenStatus_bitmask) {
				report->type = EVENT_RUNSTATUS;
				*p++ = TPerror & 0x0F; // Store generator index into report area
				event_send_report(event_GenStatus_bitmask, report, report->length);
			}
		}
		else if ((TPerror & ~0xFF) == 0x7FC00100) {
			if (event_CycPk_bitmask) {
				report->type = EVENT_CYCPK;
				*p++ = TPerror & 0xFF; // Store channel number into report area
				event_send_report(event_CycPk_bitmask, report, report->length);
			}
		}
		else if (event_TPerror_bitmask) {
			report->type = EVENT_TPERROR;
			*p++ = TPerror;	// Store TPerror value into report data area
			event_send_report(event_TPerror_bitmask, report, report->length);
		}

	}

#endif

	return(0);
}


