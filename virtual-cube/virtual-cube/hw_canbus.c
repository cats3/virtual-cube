/********************************************************************************
 * MODULE NAME       : hw_can.c													*
 * PROJECT			 : Control Cube												*
 * MODULE DETAILS    : Interface routines for CAN bus card.						*
 *																				*
 ********************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <stdint.h>
#include "porting.h"

#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"

 /* Static variable used to hold hardware base address of the CAN interface.
	We assume for now that there can only be one CAN interface fitted.
 */

static hw_can* canbase;

/* Static variable used to hold the CAN bus ID of the feedback channel */

static int chan0_id = 0x01;

/* Static variable used to hold the COB_ID for the PDO used to read the
   current value from the feedback channel.
*/

uint32_t pdo_cob_id = 0;
uint32_t settings = 0;

static uint32_t candebug1 = 0xAAAA5555;
static uint32_t candebug2 = 0x5555AAAA;

static uint8_t debug0 = 0xAAAA5555;
static uint8_t debug1 = 0xAAAA5555;
static uint8_t debug2 = 0x5555AAAA;


void netprintf(int s, char* fmt, ...);

/*******************************************************
* Table of handler functions for CAN bus txdr channels *
*******************************************************/

void* hwcan_fntable[] = {
  &read_chan_status,				/* Fn  0. Ptr to readstatus function 				*/
  &chan_ad_set_pointer,				/* Fn  1. Ptr to setpointer function				*/
  &chan_ad_get_pointer,				/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  &save_chan_calibration,			/* Fn  6. Ptr to savecalibration function 			*/
  &restore_chan_calibration,		/* Fn  7. Ptr to restorecalibration function 		*/
  &flush_chan_calibration,			/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function	 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  &chan_ad_set_gain,				/* Fn 13. Ptr to setgain function 					*/
  &chan_ad_read_gain,				/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  NULL,								/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  &chan_read_adc,					/* Fn 23. Ptr to readadc function 					*/
  &set_filter,						/* Fn 24. Ptr to setfilter function	 				*/
  &read_filter,						/* Fn 25. Ptr to readfilter function 				*/
  NULL,								/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function 			*/
  NULL,								/* Fn 28. Ptr to set_caltable function 				*/
  NULL,								/* Fn 29. Ptr to read_caltable function 			*/
  NULL,								/* Fn 30. Ptr to set_calgain function 				*/
  NULL,								/* Fn 31. Ptr to read_calgain function 				*/
  NULL,								/* Fn 32. Ptr to set_caloffset function 			*/
  NULL,								/* Fn 33. Ptr to read_caloffset function 			*/
  &chan_set_cnet_output,			/* Fn 34. Ptr to set_cnet_slot function 			*/
  &chan_ad_set_gaintrim,			/* Fn 35. Ptr to set_gaintrim function 				*/
  &chan_ad_read_gaintrim,			/* Fn 36. Ptr to read_gaintrim function 			*/
  NULL,								/* Fn 37. Ptr to set_offsettrim function 			*/
  NULL,								/* Fn 38. Ptr to read_offsettrim function 			*/
  &chan_dt_set_txdrzero,			/* Fn 39. Ptr to set_txdrzero function				*/
  &chan_dt_read_txdrzero,			/* Fn 40. Ptr to read_txdrzero function				*/
  &read_peaks,						/* Fn 41. Ptr to readpeak function 					*/
  &reset_peaks,						/* Fn 42. Ptr to resetpeak function 				*/
  &chan_ad_set_flags,				/* Fn 43. Ptr to set_flags function 				*/
  &chan_ad_read_flags,				/* Fn 44. Ptr to read_flags function 				*/
  &chan_ad_write_map,				/* Fn 45. Ptr to write_map function 				*/
  &chan_ad_read_map,				/* Fn 46. Ptr to read_map function 					*/
  &chan_dt_set_refzero,				/* Fn 47. Ptr to set_refzero function 				*/
  &read_refzero,					/* Fn 48. Ptr to read_refzero function 				*/
  &undo_refzero,					/* Fn 49. Ptr to undo_refzero function 				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  NULL,								/* Fn 52. Ptr to tedsid function 					*/
  NULL,								/* Fn 53. Ptr to set_source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  NULL,								/* Fn 58. Ptr to set info function					*/
  NULL,								/* Fn 59. Ptr to read info function					*/
  NULL,								/* Fn 60. Ptr to write output function				*/
  NULL,								/* Fn 61. Ptr to read input function				*/
  NULL,								/* Fn 62. Ptr to read output function				*/
  &chan_ad_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_ad_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  &chan_dt_txdrzero_zero,			/* Fn 72. Ptr to txdrzero_zero function				*/
  &chan_read_cyclecount,			/* Fn 73. Ptr to readcycle function					*/
  &reset_cycles,					/* Fn 74. Ptr to resetcycles function 				*/
  &chan_ad_set_peak_threshold,		/* Fn 75. Ptr to setpkthreshold function 			*/
  &chan_ad_read_peak_threshold,		/* Fn 76. Ptr to readpkthreshold function 			*/
  NULL,								/* Fn 77. Ptr to writefaultmask function 			*/
  NULL,								/* Fn 78. Ptr to readfaultstate function 			*/
  &chan_dt_write_config,			/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  &chan_dt_read_config,				/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  &chan_dt_write_fullscale,			/* Fn 81. Ptr to setfullscale function				*/
  chan_dt_refzero_zero,				/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};

/* Table holding MCP2515 CNF1 settings corresponding to the available baud rates */

const uint8_t cnf1_init[] = { 0,		/* 1Mbit/s		*/
							7,		/* Reserved 	*/
							1,		/* 500kbit/s	*/
							3,		/* 250kbit/s	*/
							7,		/* 125kbit/s	*/
							19,		/* 50kbit/s		*/
							49,		/* 20kbit/s		*/
							7,		/* Reserved		*/
							7,		/* Reserved		*/
							7,		/* Reserved		*/
							7,		/* Reserved		*/
							7,		/* Reserved		*/
							7,		/* Reserved		*/
							7,		/* Reserved		*/
							7,		/* Reserved		*/
							7,		/* Reserved		*/
};

const int baudrate_list[] = { 1000,
								 0,
							   500,
							   250,
							   125,
								50,
								20,
								 0,
								 0,
								 0,
								 0,
								 0,
								 0,
								 0,
								 0,
								 0,
};


/* Table holding MCP2515 initialisation values used by hw_canbus_init_mcp2515 function */

const struct mcp2515_reg { uint8_t reg; uint8_t value; } mcp2515_init[] = {

	{CANCTRL,	CANCTRL_REQOP_CFG},		/* Set configuration mode */
#if 1
	{CANINTE,	0x00},
	{CANINTF,	0x00},

	{CNF1,		0x07},					/* 7 = 125kb, 3 = 250kb, 1 = 500kb, 0 = 1000kb */
	{CNF2,		0x92},
	{CNF3,		0x82},

	{TXB0CTRL,	0x00},
	{TXB1CTRL,	0x00},
	{TXB2CTRL,	0x00},
	{TXRTSCTRL,	0x00},					/* 0x07	*/
	{RXB0CTRL,	RXBCTRL_RXMSTD},
	{RXB1CTRL,	0x00},
	{BFPCTRL,	0x00},					/* 0x0F	*/

	{RXF0SIDH,	0xFF},
	{RXF0SIDL,	0xE0},
	{RXF0EID8,	0x00},
	{RXF0EID0,	0x00},
	{RXF1SIDH,	0x00},
	{RXF1SIDL,	0x00},
	{RXF1EID8,	0x00},
	{RXF1EID0,	0x00},
	{RXF2SIDH,	0x00},
	{RXF2SIDL,	0x00},
	{RXF2EID8,	0x00},
	{RXF2EID0,	0x00},
	{RXF3SIDH,	0x00},
	{RXF3SIDL,	0x00},
	{RXF3EID8,	0x00},
	{RXF3EID0,	0x00},
	{RXF4SIDH,	0x00},
	{RXF4SIDL,	0x00},
	{RXF4EID8,	0x00},
	{RXF4EID0,	0x00},
	{RXF5SIDH,	0x00},
	{RXF5SIDL,	0x00},
	{RXF5EID8,	0x00},
	{RXF5EID0,	0x00},
	{RXM0SIDH,	0xFF},
	{RXM0SIDL,	0xE0},
	{RXM0EID8,	0x00},
	{RXM0EID0,	0x00},
	{RXM1SIDH,	0x00},
	{RXM1SIDL,	0x00},
	{RXM1EID8,	0x00},
	{RXM1EID0,	0x00},

#endif
	//	{CANCTRL,	CANCTRL_REQOP_NORM},	/* Set normal operating mode */
		{0xFF, 0}
};


/* CAN message to read PDO mapping. This reads object 0x1800 sub ID 1, which
   returns the COB-ID currently allocated to PDO 1.
*/

static const uint8_t pdo_map[8] = { 0x40, 0x00, 0x18, 0x01, 0x00, 0x00, 0x00, 0x00 };

/* CAN message to define cycle timer. This sets object 0x6200 to the value 1, which
   sets the cycle timer interval to 1ms.
*/

static const uint8_t pdo_cyctimer[8] = { 0x22, 0x00, 0x62, 0x00, 0x01, 0x00, 0x00, 0x00 };

/* CAN message to define transmission type for PDO 1. This sets object 0x1800
   sub ID 2 to the value 254, which causes PDO 1 to be transmitted at the selected
   timer interval or on a position change if the cycle time is set to zero.
*/

static const uint8_t pdo_txtype[8] = { 0x22, 0x00, 0x18, 0x02, 0xFE, 0x00, 0x00, 0x00 };




/********************************************************************************
  FUNCTION NAME     : hw_canbus_init_mcp2515
  FUNCTION DETAILS  : Initialise MCP2515 device.
********************************************************************************/

void hw_canbus_init_mcp2515(hw_can* base, int baudrate)
{
	const struct mcp2515_reg* init;
	uint8_t canctrl;

	/* Reset to default state */

	hw_canbus_spi_instr(base, SPI_RESET);

	/* Copy register values into the MCP2515 device */

	init = mcp2515_init;
	while (init->reg != 0xFF) {
		hw_canbus_wr_mcp2515(base, init->reg, init->value);
		init++;
	}

	/* Overwrite default baud rate setting */

	hw_canbus_wr_mcp2515(base, CNF1, cnf1_init[baudrate & 0x0F]);

	/* Select normal operating mode */

	hw_canbus_mcp2515_setmode(base, CANCTRL_REQOP_NORM);
}



/********************************************************************************
  FUNCTION NAME   	: hw_can_init
  FUNCTION DETAILS  : Initialise CAN bus.

  Arguments as follows:

********************************************************************************/

void hw_can_init(hw_can* base, int enable_rxb0, int enable_remote, int baudrate)
{
	int err;
	uint8_t msg[8];
	int rxlen;
	uint8_t canctrl;
	int retry = 32;

	/* Determine CAN device settings so that we can communicate with it
	   correctly. Then perform any necessary configuration. Whilst
	   sending/receiving CAN messages, the MCP2515 must be in normal
	   mode. However, we need to switch to configuration mode in order
	   to set the RX filter registers.
	*/

	while (retry) {
		err = hw_canbus_send_msg(base, 0x600 + chan0_id, 8, (uint8_t*)&pdo_map, &rxlen, msg);
		if (!err && (rxlen == 8)) {
			memcpy(&pdo_cob_id, &msg[4], sizeof(uint32_t));

			/* Select configuration mode */

			hw_canbus_mcp2515_setmode(base, CANCTRL_REQOP_CFG);

			/* Configure MCP2515 to receive PDO message in RX buffer 0 */

			hw_canbus_wr_mcp2515(base, RXF0SIDH, pdo_cob_id >> 3);
			hw_canbus_wr_mcp2515(base, RXF0SIDL, pdo_cob_id << 5);

			hw_canbus_rd_mcp2515(base, CANCTRL, &debug0);
			hw_canbus_rd_mcp2515(base, RXF0SIDH, &debug1);
			hw_canbus_rd_mcp2515(base, RXF0SIDL, &debug2);

			settings++;
			//    if (rxlen == 8) break;
			break;
		}
		retry--;
	}

	if (candebug1 == 0xAAAA5555) { candebug1 = pdo_cob_id; }
	if (candebug1 != 0x5555AAAA) { candebug2 = pdo_cob_id; }

	/* Configure RXB0BF pin as buffer full output */

	hw_canbus_wr_mcp2515(base, BFPCTRL, BFPCTRL_B0BFE | BFPCTRL_B0BFM);

	/* Select normal operating mode */

	hw_canbus_mcp2515_setmode(base, CANCTRL_REQOP_NORM);
}



/********************************************************************************
  FUNCTION NAME   	: can_txdr_init
  FUNCTION DETAILS  : Initialise CAN bus transducer.

  Arguments as follows:

********************************************************************************/

void hw_can_txdr_init(hw_can* base, int enable_rxb0, int enable_remote)
{
	int err;
	int rxlen;

	/* Configure cycle timer */

	err = hw_canbus_send_msg(base, 0x600 + chan0_id, 8, (uint8_t*)&pdo_cyctimer, &rxlen, NULL);

	/* Configure PDO 1 transmission type */

	err = hw_canbus_send_msg(base, 0x600 + chan0_id, 8, (uint8_t*)&pdo_txtype, &rxlen, NULL);

	/* Enable RXB0 processing in FPGA */

	if (enable_rxb0) base->u.wr.ctrl = CTRL_ENRXB0;

	/* Start remote node operation */

	if (enable_remote) hw_canbus_start_remote_node(base, chan0_id);
}



/********************************************************************************
  FUNCTION NAME     : hw_can_install
  FUNCTION DETAILS  : Install CAN bus hardware on the selected channel(s).

					  On entry:

					  slot			Slot number being installed.

					  numinchan		Points to the variable holding the number of
									input channels installed for this slot.

					  numoutchan	Points to the variable holding the number of
									output channels installed for this slot.

					  nummiscchan	Points to the variable holding the number of
									miscellaneous channels installed for this slot.

					  flags			Controls if channel allocation is performed.
									If not, then only initialisation operations
									are performed.
********************************************************************************/

uint32_t hw_can_install(int slot, int* numinchan, int* numoutchan, int* nummiscchan,
	hw_can* hw, int flags)
{
	info_can* i;
	uint32_t eecal_addr = CHAN_EE_CALSTART;		/* Start of channel EEPROM calibration area	  */
	uint32_t eecfg_addr = CHAN_EEBANK_DATASTART;	/* Start of channel EEPROM configuration area */
	uint32_t eerdcfg_addr = CHAN_EEBANK_DATASTART;	/* Address for reading EEPROM configuration	  */
	int input;
	uint8_t* workspace;
	uint32_t status = 0;
	chandef* def;
	int baudrate;

	canbase = hw;

	/* Generate hardware reset pulse */

	hw->u.wr.ctrl = 0;
	dsp_delay(1000);
	hw->u.wr.ctrl = CTRL_RESET;
	dsp_delay(1000);
	hw->u.wr.ctrl = 0;
	dsp_delay(1000);

	/***************************************************************************
	* Perform initial allocation of channels and setting of default parameters *
	***************************************************************************/

	if (flags & DO_ALLOCATE) {

		/* The workspace is used by all inputs on the mainboard, so determine the
		   address of the workspace area of the first input to be allocated and use
		   that for all input workspaces.
		*/

		workspace = inchaninfo[totalinchan].hw.i_can.work;
		memset(workspace, 0, WSIZE_CAN);

	}

	/* Allocate the transducer input */

	for (input = 0; input < 1; input++) {
		cfg_digtxdr* cfg;

		def = &inchaninfo[slot_status[slot].baseinchan + input];

		/* Set defaults for digital txdr input channel */

		chan_dt_default(def, (totalinchan + input));

		if (flags & DO_ALLOCATE) {

			i = &def->hw.i_can;

			/* Define local hardware/workspace settings */

			i->hw = hw;						/* Pointer to hardware base				*/
			i->input = input;					/* Input channel number on mainboard    */
			i->ledmask = 0;						/* Mask for channel ident LED 		    */
			i->ledstate = &workspace[1];			/* Pointer to local workspace		    */

			init_fntable(def, hwcan_fntable);		/* Set pointers to control functions 	*/

			SETDEF(slot, TYPE_DIGTXDR, input, (CHANTYPE_INPUT | (totalinchan + input)));

			/* Allocate space in EEPROM/BBSRAM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
			def->eecal_start = eecal_addr; eecal_addr += SAVED_CHANCAL_SIZE; def->eecal_size = SAVED_CHANCAL_SIZE;

			/* Allocate space in DSP B memory for linearisation map */

			if (!(def->mapptr = (float*)alloc_dspb_general(sizeof(def->map)))) diag_error(DIAG_MALLOC);

			/* Define the source and destination addresses for the channel */

			switch (input) {
			case 0:
				def->sourceptr = &(hw->u.rd.txdr1);
				break;
			}
			def->outputptr = cnet_get_slotaddr_out(totalinchan + input, PROC_DSPB);

		}

		/* Restore current calibration and configuration data */

		def->status = restore_chan_calibration(def);
		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

		cfg = &def->cfg.i_digtxdr;

		/* Get the ID of the CAN device from the configuration settings */

		chan0_id = cfg->hwflags >> 24;

		/* Get the baud rate from the configuration settings */

		baudrate = (cfg->hwflags >> 20) & 0x0F;

	}

	/* Initialise MCP2515 device */

	hw_canbus_init_mcp2515(hw, baudrate);

	/* Initialise CAN bus */

	hw_can_init(hw, FALSE, FALSE, baudrate);

	/***************************************************************************
	* Perform hardware configuration                                           *
	***************************************************************************/

	/* Configure the txdr input */

	for (input = 0; input < 1; input++) {

		chandef* def;
		cal_digtxdr* cal;
		cfg_digtxdr* cfg;
		int chan = slot_status[slot].baseinchan + input;
		float fsrange;
		char* units;

		def = &inchaninfo[chan];
		cal = &def->cal.u.c_digtxdr;
		cfg = &def->cfg.i_digtxdr;

		/* Trap for illegal negative gaintrim value caused by upgrade from version
		   prior to v1.0.0099. Replace by value of 1.0.
		*/

		if (cfg->neg_gaintrim < 0.5) cfg->neg_gaintrim = 1.0;

		/* Restore the user configuration settings */

		def->setgain(def, cfg->gain,
			SETGAIN_RETAIN_GAINTRIM | chan_extract_gainflags(cfg->flags),
			NO_MIRROR);

		/* Get the current full scale range for this channel and use that to set the bit scale
		   factor for the transducer.
		*/

		if (getFullScaleUnits(chan, &fsrange, &units)) def->setfullscale(def, fsrange);


		def->offset = cal->cal_offset;
		def->set_txdrzero(def, cfg->txdrzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);
		def->set_refzero(def, def->refzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);

		/* Set the linearisation map */

		def->write_map(def, (cfg->flags & FLAG_LINEARISE) >> BIT_LINEARISE, def->map, def->filename, FALSE, NO_MIRROR);

		/* Update channel flags. Force simulation mode off and disable cyclic event generation. */

		def->set_flags(def, (cfg->flags & ~(FLAG_SIMULATION | FLAG_CYCEVENT)), 0xFFFFFFFF, NO_UPDATECLAMP, NO_MIRROR);

		/* Set filter 1 characteristic */

		set_filter(def, FILTER_ALL, 0, def->filter1.type & FILTER_ENABLE, def->filter1.type & FILTER_TYPEMASK, def->filter1.order, def->filter1.freq, def->filter1.bandwidth, NO_MIRROR);

		/* Update the shared channel definition */

		update_shared_channel(def);

	}

	/* Repeat CAN initialisation, then enable RXB0 processing and enable
	   remote device. This second call to hw_can_init() shouldn't be
	   necessary, but the value read for the PDO mapping on the first
	   call is not correct for some reason. Further investigation required.
	*/

	hw_can_init(hw, FALSE, FALSE, baudrate);
	hw_can_txdr_init(hw, TRUE, TRUE);

	/* Update channel counts */

	if (flags & DO_ALLOCATE) {
		totalinchan += 1;
		physinchan += 1;
		*numinchan = 1;
		*numoutchan = 0;
		*nummiscchan = 0;
	}

	return(status & (FLAG_BADLIST | FLAG_OLDLIST));
}



/********************************************************************************
  FUNCTION NAME     : hw_post_can_install
  FUNCTION DETAILS  : Post initialisation installation of CAN hardware.

					  On entry:

					  slot			Slot number being installed.

					  flags			Controls initialisation operation.
********************************************************************************/

uint32_t hw_can_post_install(int slot, hw_can* hw, int flags)
{
	int input;

	/* Configure the CAN bus txdr input */

	for (input = 0; input < 1; input++) {

		chandef* def;
		cal_digtxdr* cal;
		cfg_digtxdr* cfg;
		int chan = slot_status[slot].baseinchan + input;
		float fsrange;
		char* units;

		def = &inchaninfo[chan];
		cal = &def->cal.u.c_digtxdr;
		cfg = &def->cfg.i_digtxdr;

		/* Get the current full scale range for this channel and use that to set the bit scale
		   factor for the transducer.
		*/

		if (getFullScaleUnits(chan, &fsrange, &units)) def->setfullscale(def, fsrange);

		def->offset = cal->cal_offset;
		def->set_txdrzero(def, cfg->txdrzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);
		def->set_refzero(def, def->refzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);

		/* Update the shared channel definition */

		update_shared_channel(def);
	}

	return(0);
}



/********************************************************************************
  FUNCTION NAME     : hw_can_save
  FUNCTION DETAILS  : Save the CAN channel settings to EEPROM or
					  battery-backed RAM if required.

					  If a bad list or an old list format was detected on any
					  channels, then all of the channels must be updated.

					  On entry:

					  slot		Slot number being saved.

					  status	System configuration status. This indicates if
								a full save operation is required because an
								old or bad format list was detected during
								initialisation.

********************************************************************************/

void hw_can_save(int slot, uint32_t status)
{
	int input;
	chandef* def;

	/* Save the input channel */

	for (input = 0; input < 1; input++) {
		def = &inchaninfo[slot_status[slot].baseinchan + input];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

}



/********************************************************************************
  FUNCTION NAME     : hw_canbus_reset_mcp2515
  FUNCTION DETAILS  : Reset MCP2515 registers to default state.
********************************************************************************/

int hw_canbus_reset_mcp2515(hw_can* base)
{
	base->u.wr.spi_txbuf0 = SPI_RESET;
	base->u.wr.spi_txctrl = SPI_TXCTRL_WR1;

	while (base->u.rd.spi_status & SPI_TXBUSY);

	return(0);
}



/********************************************************************************
  FUNCTION NAME     : hw_canbus_wr_mcp2515
  FUNCTION DETAILS  : Write value to MCP2515 register.

					  On entry:

					  reg		Register to write

					  data		Data value to write

********************************************************************************/

int hw_canbus_wr_mcp2515(hw_can* base, uint8_t reg, uint8_t data)
{
	base->u.wr.spi_txbuf0 = SPI_WRITE | (reg << 8);
	base->u.wr.spi_txbuf2 = data;
	base->u.wr.spi_txctrl = SPI_TXCTRL_WR3;

	while (base->u.rd.spi_status & SPI_TXBUSY);

	return(0);
}



/********************************************************************************
  FUNCTION NAME     : hw_canbus_rd_mcp2515
  FUNCTION DETAILS  : Read value from MCP2515 register.

					  On entry:

					  reg		Register to read

					  data		Pointer to location to hold data value read
								from register

********************************************************************************/

int hw_canbus_rd_mcp2515(hw_can* base, uint8_t reg, uint8_t* data)
{
	base->u.wr.spi_txbuf0 = SPI_READ | (reg << 8);
	base->u.wr.spi_rxctrl = SPI_RXCTRL_WR2 | SPI_RXCTRL_RD1;

	while (base->u.rd.spi_status & (SPI_RXBUSY | SPI_TXBUSY));

	if (data) *data = base->u.rd.spi_rxbuf0;

	return(0);
}



/********************************************************************************
  FUNCTION NAME     : hw_canbus_modify_mcp2515
  FUNCTION DETAILS  : Modify value in MCP2515 register.

					  On entry:

					  reg		Register to write

					  data		Data value to write

********************************************************************************/

int hw_canbus_modify_mcp2515(hw_can* base, uint8_t reg, uint8_t mask, uint8_t data)
{
	base->u.wr.spi_txbuf0 = SPI_MODIFY | (reg << 8);
	base->u.wr.spi_txbuf2 = mask | (data << 8);
	base->u.wr.spi_txctrl = SPI_TXCTRL_WR4;

	while (base->u.rd.spi_status & SPI_TXBUSY);

	return(0);
}



/********************************************************************************
  FUNCTION NAME     : hw_canbus_mcp2515_setmode
  FUNCTION DETAILS  : Set operating mode of MCP2515.

					  On entry:

					  reg		Register to write

					  mode		Operating mode

********************************************************************************/

int hw_canbus_mcp2515_setmode(hw_can* base, uint8_t mode)
{
	uint8_t canstat;

	while (1) {
		hw_canbus_wr_mcp2515(base, CANCTRL, mode);

		/* Verify mode change. Try again if mode not correct. */

		hw_canbus_rd_mcp2515(base, CANSTAT, &canstat);
		if ((canstat & CANCTRL_OP_MASK) == mode) break;
	}
	return(0);
}



/********************************************************************************
  FUNCTION NAME     : hw_canbus_spi_instr
  FUNCTION DETAILS  : Send SPI instruction to MCP2515.
********************************************************************************/

void hw_canbus_spi_instr(hw_can* base, uint8_t instr)
{
	base->u.wr.spi_txbuf0 = instr;
	base->u.wr.spi_txctrl = SPI_TXCTRL_WR1;

	while (base->u.rd.spi_status & SPI_TXBUSY);
}



/********************************************************************************
  FUNCTION NAME     : hw_canbus_send_msg
  FUNCTION DETAILS  : Send message via CAN bus.

					  On entry:

					  cob_id	Destination COB-ID

					  len		Message length

					  data		Pointer to start of message

********************************************************************************/

int hw_canbus_send_msg(hw_can* base, uint32_t cob_id, int txlen, uint8_t* txdata, int* rxlen, uint8_t* rxdata)
{
	int n;
	int err = NO_ERROR;
	int timeout = 1000;	/* Timeout is (at least) 1000 * 100us */

	/* Reset any pending MERRF or TX0IF flags */

	hw_canbus_modify_mcp2515(base, CANINTF, 0x84, 0);

	/* Write standard 11-bit identifier */

	hw_canbus_wr_mcp2515(base, TXB0SIDH, cob_id >> 3);
	hw_canbus_wr_mcp2515(base, TXB0SIDL, cob_id << 5);

	for (n = 0; n < txlen; n++) {
		hw_canbus_wr_mcp2515(base, TXB0D0 + n, *txdata++);
	}

	hw_canbus_wr_mcp2515(base, TXB0DLC, txlen);
	hw_canbus_wr_mcp2515(base, TXB0CTRL, TXBCTRL_TXREQ | TXBCTRL_TXP3);	/* Set TXP = highest priority and TXREQ */

	/* Wait for message transmission to complete */

	while (1) {
		uint8_t canintf;
		uint8_t txb0ctrl;

		hw_canbus_rd_mcp2515(base, CANINTF, &canintf);

		if (canintf & CANINTF_TX0IF) {

			/* Transmit buffer 0 empty */

			hw_canbus_modify_mcp2515(base, CANINTF, CANINTF_TX0IF, 0);

			while (1) {
				hw_canbus_rd_mcp2515(base, CANINTF, &canintf);
				if (canintf & CANINTF_RX1IF) {
					uint8_t len;

					/* Receive buffer 1 full */

					hw_canbus_rd_mcp2515(base, RXB1DLC, &len);
					if (rxlen) *rxlen = (int)len;
					if (len > 8) len = 8;
					for (n = 0; n < (int)len; n++) {
						hw_canbus_rd_mcp2515(base, RXB1D0 + n, rxdata++);
					}
					hw_canbus_modify_mcp2515(base, CANINTF, CANINTF_RX1IF, 0);
					return(0);
				}
				else {
					dsp_delay(100);	/* 100us delay */
					timeout--;
					if (timeout == 0) {
						err = CAN_TIMEOUT;
						goto hw_canbus_send_msg_done;
					}
				}
			}
		}
		else if (canintf & CANINTF_MERRF) {

			/* Message error */

			hw_canbus_modify_mcp2515(base, CANINTF, CANINTF_MERRF, 0);
			err = CANINTF_MERRF;
			break;
		}
		else {
			hw_canbus_rd_mcp2515(base, TXB0CTRL, &txb0ctrl);
			if (txb0ctrl & TXBCTRL_MLOA) {
				err = TXBCTRL_MLOA;
				break;	/* Message lost arbitration */
			}
		}
		dsp_delay(100);	/* 100us delay */
		timeout--;
		if (timeout == 0) {
			err = CAN_TIMEOUT;
			break;
		}
	}

hw_canbus_send_msg_done:

	if (rxlen) *rxlen = 0;

	return(err);
}



/********************************************************************************
  FUNCTION NAME     : hw_canbus_send_sync
  FUNCTION DETAILS  : Send sync via CAN bus.
********************************************************************************/

int hw_canbus_send_sync(hw_can* base)
{
	return(hw_canbus_send_msg(base, 0x80, 0, NULL, NULL, NULL));
}



/********************************************************************************
  FUNCTION NAME     : hw_canbus_nmt_operational
  FUNCTION DETAILS  : Send NMT message to select operational state.

					  On entry:

					  cob_id	Destination COB-ID

********************************************************************************/

int hw_canbus_nmt_operational(hw_can* base)
{
	static const uint8_t msg[2] = { 0x01, 0x01 };

	return(hw_canbus_send_msg(base, 0, 2, (uint8_t*)msg, NULL, NULL));
}



/********************************************************************************
  FUNCTION NAME     : hw_canbus_start_remote_node
  FUNCTION DETAILS  : Send START REMOTE NODE message.

					  On entry:

					  id		Destination ID

********************************************************************************/

int hw_canbus_start_remote_node(hw_can* base, uint32_t id)
{
	static uint8_t msg[8] = { 0x01, 0, 0, 0, 0, 0, 0, 0 };

	msg[1] = id;

	return(hw_canbus_send_msg(base, 0, 8, msg, NULL, NULL));
}



/********************************************************************************
  FUNCTION NAME     : hw_canbus_sdo_download
  FUNCTION DETAILS  : Perform SDO download via CAN bus.

					  On entry:

					  cob_id	Destination COB-ID

					  index		SDO object index

					  sub_index	SDO object sub index

					  len		Data length

					  data		Pointer to start of data

********************************************************************************/

int hw_canbus_sdo_download(hw_can* base, uint32_t cob_id, uint16_t index, uint8_t sub_index, int len, uint8_t* data)
{
	uint8_t msg[8] = { 0x23 };
	int n;

	msg[0] |= (4 - len) << 2;	/* Insert data length into message byte 0 */
	msg[1] = index;
	msg[2] = index >> 8;
	msg[3] = sub_index;
	for (n = 0; n < len; n++) {
		msg[4 + n] = *data++;
	}
	return(hw_canbus_send_msg(base, 0x600 + cob_id, 8, msg, NULL, NULL));
}



/********************************************************************************
  FUNCTION NAME     : can_wr
  FUNCTION DETAILS  : Telnet write register
********************************************************************************/

void can_wr(char* args[], int numargs, int skt)
{
	uint8_t reg;
	uint8_t data;

	reg = (int)strtoul(args[0], NULL, 0);	/* Get register */
	data = (int)strtoul(args[1], NULL, 0);	/* Get data		*/
	hw_canbus_wr_mcp2515(canbase, reg, data);
}



/********************************************************************************
  FUNCTION NAME     : can_rd
  FUNCTION DETAILS  : Telnet read register
********************************************************************************/

void can_rd(char* args[], int numargs, int skt)
{
	uint8_t reg;
	uint8_t data;

	reg = (int)strtoul(args[0], NULL, 0);	/* Get register */
	hw_canbus_rd_mcp2515(canbase, reg, &data);

	netprintf(skt, "Reg %02X Data %02X\n\r", reg, data);
}



/********************************************************************************
  FUNCTION NAME     : can_show
  FUNCTION DETAILS  : Telnet show all registers
********************************************************************************/

void can_show(char* args[], int numargs, int skt)
{
	int reg;
	int n;
	uint8_t data;

	for (reg = 0; reg < 0x80; reg += 0x10) {
		netprintf(skt, "%04X ", reg);
		for (n = 0; n < 0x10; n++) {
			hw_canbus_rd_mcp2515(canbase, reg + n, &data);
			netprintf(skt, "%02X ", data);
		}
		netprintf(skt, "\n\r");
	}

}




/********************************************************************************
  FUNCTION NAME     : can_msg
  FUNCTION DETAILS  : Telnet send CAN message
********************************************************************************/

void can_msg(char* args[], int numargs, int skt)
{
	uint8_t msg[8];
	uint16_t cob_id;
	int n;
	int rxlen;
	int err;

	if ((numargs < 1) || (numargs > 9)) {
		netprintf(skt, "Illegal message length\n\r");
		return;
	}

	cob_id = (int)strtoul(args[0], NULL, 0);	/* Get COB ID */
	for (n = 1; n < numargs; n++) msg[n - 1] = (int)strtoul(args[n], NULL, 0);

	err = hw_canbus_send_msg(canbase, cob_id, numargs - 1, msg, &rxlen, msg);
	if (err == CAN_TIMEOUT) {
		netprintf(skt, "Timeout error");
	}
	else if (err) {
		netprintf(skt, "Error %d", err);
	}
	else {
		netprintf(skt, "RX: (%08X) (%08X) ", err, rxlen);
		if (rxlen > 8) rxlen = 8;
		if (rxlen < 0) rxlen = 0;
		if (rxlen) {
			uint8_t* p = msg;
			while (rxlen--) {
				netprintf(skt, "%02X ", *p++);
			}
		}
		else {
			netprintf(skt, "None");
		}
	}
	netprintf(skt, "\n\r");
}



/********************************************************************************
  FUNCTION NAME     : can_status
  FUNCTION DETAILS  : Telnet CAN status display
********************************************************************************/

void can_status(char* args[], int numargs, int skt)
{
	netprintf(skt, "CAN bus status:\n\r");
	netprintf(skt, "Base address %08X\n\r", canbase);
	netprintf(skt, "Chan 0 ID  = %d\n\r", chan0_id);
	netprintf(skt, "PDO COB ID = %04X\n\r", pdo_cob_id);
}



/********************************************************************************
  FUNCTION NAME     : can_rdfb
  FUNCTION DETAILS  : Telnet read feedback value from RX buffer 0
********************************************************************************/

void can_rdfb(char* args[], int numargs, int skt)
{
	int reg;
	uint8_t data;
	uint8_t canintf;
	uint8_t rxb0ctrl;
	uint8_t rxb0sidh;
	uint8_t rxb0sidl;
	uint8_t eflg;
	int cob_id;

	hw_canbus_rd_mcp2515(canbase, CANINTF, &canintf);
	hw_canbus_rd_mcp2515(canbase, RXB0CTRL, &rxb0ctrl);
	hw_canbus_rd_mcp2515(canbase, RXB0SIDH, &rxb0sidh);
	hw_canbus_rd_mcp2515(canbase, RXB0SIDL, &rxb0sidl);
	hw_canbus_rd_mcp2515(canbase, EFLG, &eflg);
	cob_id = ((((int)rxb0sidh) << 8) | rxb0sidl) >> 5;

	netprintf(skt, "COB_ID %04X CANINTF %02X RXB0CTRL %02X EFLG %02X\n\r", cob_id, canintf, rxb0ctrl, eflg);
	if (canintf & CANINTF_RX0IF) {

#if 1

		for (reg = RXB0D0; reg < RXB0D4; reg++) {
			hw_canbus_rd_mcp2515(canbase, reg, &data);
			netprintf(skt, "%02X ", data);
		}
		netprintf(skt, "\n\r");
		hw_canbus_modify_mcp2515(canbase, CANINTF, CANINTF_RX0IF, 0);
		hw_canbus_modify_mcp2515(canbase, EFLG, EFLG_RX0OVR, 0);

#endif



	}
	else {
		netprintf(skt, "Buffer empty\n\r");
	}

}



/********************************************************************************
  FUNCTION NAME   	: can_config
  FUNCTION DETAILS  : Set CAN bus transducer configuration.

  Arguments as follows:

  Arg 0 :	Channel number
  Arg 1 :	Device ID
  Arg 2 :   Baud rate
  Arg 3 :   Bit size (float)

********************************************************************************/

void can_config(char* args[], int numargs, int skt)
{
	int chan;
	chandef* def;
	uint32_t flags;
	uint32_t id;
	int baudrate;
	float bitsize;
	int n;

	chan = ((uint32_t)strtoul(args[0], NULL, 0));	/* Get channel */
	def = chanptr(chan);						/* Get channel definition address */
	if (!def) {
		netprintf(skt, "Illegal channel\n\r");
	}
	else {
		if (numargs < 2) {
			def->readdigtxdrconfig(def, &flags, &bitsize, NULL, NULL);
			id = flags >> 24;
			baudrate = baudrate_list[(flags >> 20) & 0x0f];
			netprintf(skt, "ID: %d Baud: %dkbit/s Bitsize: %f\n\r", id, baudrate, bitsize);
		}
		else {
			id = ((uint32_t)strtoul(args[1], NULL, 0));	/* Get device ID */
			baudrate = ((int)strtoul(args[2], NULL, 0));	/* Get baudrate */
			bitsize = (float)atof(args[3]);			/* Get bit size */
			for (n = 0; n < 16; n++) {
				if (baudrate_list[n] && (baudrate_list[n] == baudrate)) {
					def->setdigtxdrconfig(def, (id << 24) | (n << 20) | 0x01, bitsize, 0, 0, TRUE);
					return;
				}
			}
			netprintf(skt, "Illegal baud rate\n\r");
		}
	}

}



/********************************************************************************
  FUNCTION NAME   	: can_init
  FUNCTION DETAILS  : Re-initialise CAN bus.

  Arguments as follows:

********************************************************************************/

void can_init(char* args[], int numargs, int skt)
{
	int err;
	uint8_t msg[8];
	int rxlen;
	uint8_t canctrl;

#if 0

	//hw_canbus_init_mcp2515(canbase);

	/***************************************************************************
	* Determine CAN device settings so that we can communicate with it		   *
	* correctly. Then perform any necessary configuration.					   *
	***************************************************************************/

	err = hw_canbus_send_msg(canbase, 0x600 + chan0_id, 8, (uint8_t*)&pdo_map, &rxlen, msg);
	if (!err) {
		memcpy(&pdo_cob_id, &msg[4], sizeof(uint32_t));
	}

	/* Select configuration mode */

	hw_canbus_wr_mcp2515(canbase, CANCTRL, CANCTRL_REQOP_CFG);

	/* Configure MCP2515 to receive PDO message in RX buffer 0 */

	hw_canbus_wr_mcp2515(canbase, RXF0SIDH, pdo_cob_id >> 3);
	hw_canbus_wr_mcp2515(canbase, RXF0SIDL, pdo_cob_id << 5);

	/* Configure RXB0BF pin as buffer full output */

	hw_canbus_wr_mcp2515(canbase, BFPCTRL, BFPCTRL_B0BFE | BFPCTRL_B0BFM);

	/* Select normal operating mode */

	hw_canbus_wr_mcp2515(canbase, CANCTRL, CANCTRL_REQOP_NORM);

	hw_canbus_rd_mcp2515(canbase, TXB0CTRL, &canctrl);

	/* Verify mode change. Try again if mode not correct. */

	if ((canctrl & CANCTRL_OP_MASK) != CANCTRL_REQOP_NORM) hw_canbus_wr_mcp2515(canbase, CANCTRL, CANCTRL_REQOP_NORM);

	/* Enable RXB0 processing in FPGA */

	canbase->u.wr.ctrl = CTRL_ENRXB0;

	/* Start remote node operation */

	hw_canbus_start_remote_node(canbase, chan0_id);

#endif

}




/********************************************************************************
  FUNCTION NAME   	: can_test
  FUNCTION DETAILS  : Test CAN bus.

  Arguments as follows:

********************************************************************************/

void can_test(char* args[], int numargs, int skt)
{
	int state;

	netprintf(skt, "State 1 = %08X 2 = %08X\n\r", candebug1, candebug2);
	netprintf(skt, "Debug 0 = %02X\n\r", debug0);
	netprintf(skt, "Debug 1 = %02X 2 = %02X\n\r", debug1, debug2);
	netprintf(skt, "Settings = %08X\n\r", settings);

	state = ((int)strtoul(args[0], NULL, 0));	/* Get state */
	if (state) {
		hw_canbus_mcp2515_setmode(canbase, CANCTRL_REQOP_CFG);
	}
	else {
		hw_canbus_mcp2515_setmode(canbase, CANCTRL_REQOP_NORM);
	}

}
