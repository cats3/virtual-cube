/********************************************************************************
 * MODULE NAME       : hardware.c												*
 * MODULE DETAILS    : Low-level hardware interface routines					*
 *																				*
 ********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <pthread.h>

#include <stdint.h>
#include "porting.h"

#include "virtual-hardware.h"
#include "virtual-eeprom.h"

 /* The following two lines are required to allow Events.h to compile */

#define SUPGEN
#define Parameter void

#include "defines.h"
#include "asmcode.h"
#include "hardware.h"
#include "shared.h"
#include "eeprom.h"
#include "eventlog.h"
#include "hydraulic.h"
#include "HardwareIndependent/Events.h"
#include "nonvol.h"
#include "hydraulic.h"


/* Setting FORCE_FAST to 1 forces the CPU detection routine to return
   a C6713 state. This allows code to be built for revision A hardware
   that has no slave link.
*/

#define FORCE_FAST	1

/* Setting OVERRIDE_SLOW to 1 forces the CPU clock to be set to 200MHz
   even when a TMS320C6713 has been detected. This can be used to
   prevent overclocking when actually using a C6711 device.
*/

#define OVERRIDE_SLOW	1

/* The following symbol must be defined when operating with dual emulators. It is
   used to control the startup process for DSP B. When defined, the reset,
   initialisation and booting of DSP B is skipped, since it will be performed by
   the emulator.
*/

#undef DUAL_EMULATOR


/* virtual cube addition */
struct hw_params hw_params;

/* On-chip peripherals */

volatile uint32_t* const spcr0 = (uint32_t*)0x018C0008;	/* McBSP0 Serial Port Control Register	*/
volatile uint32_t* const pcr0 = (uint32_t*)0x018C0024;	/* McBSP0 Pin Control Register			*/

volatile uint32_t* const spcr1 = (uint32_t*)0x01900008;	/* McBSP1 Serial Port Control Register	*/
volatile uint32_t* const pcr1 = (uint32_t*)0x01900024;	/* McBSP1 Pin Control Register			*/

volatile uint32_t* const ctl1 = (uint32_t*)0x01980000;	/* Timer1 Timer Control Register		*/
volatile uint32_t* const prd1 = (uint32_t*)0x01980004;	/* Timer1 Timer Period Register			*/
volatile uint32_t* const cnt1 = (uint32_t*)0x01980008;	/* Timer1 Timer Counter Register		*/

/* Control bits within SPCR register */

#define XRST	0x00010000				/* Transmitter reset					*/
#define RRST	0x00000001				/* Receiver reset						*/

/* Control bits within PCR register */

#define XIOEN	0x00002000				/* Transmitter general-purpose I/O mode	*/
#define RIOEN	0x00001000				/* Receiver general-purpose I/O mode	*/
#define FSXM	0x00000800				/* Transmit frame sync mode				*/
#define FSRM	0x00000400				/* Receive frame sync mode				*/
#define CLKXM	0x00000200				/* Transmitter clock mode				*/
#define CLKRM 	0x00000100				/* Receiver clock mode					*/
#define DX_STAT	0x00000020				/* Transmit data output					*/
#define FSXP	0x00000008				/* Transmit frame sync polarity			*/
#define FSRP	0x00000004				/* Receive frame sync polarity			*/
#define CLKXP 	0x00000002				/* Transmitter clock polarity			*/
#define CLKRP 	0x00000001				/* Receiver clock polarity				*/

/* Control bits within CTL register */

#define DATOUT	0x00000004				/* Data output							*/
#define FUNC	0x00000001				/* Function of TOUT pin					*/

/* Cache control registers */

#define L2CLEAN	0x01845004				/* L2 clean register					*/
#define CCFG	0x01840000				/* Cache configuration register			*/

#define I2C_SCL_DIRN	CLKRM
#define I2C_SDA_DIRN	FSRM

#define I2C_SCL			CLKRP
#define I2C_SDA			FSRP

#define HYDCS			FSXP
#define HYDDIN			DX_STAT
#define HYDSCLK			CLKXP

//#define FPCS			FSXP		// Remove
//#define FPSEL			CLKRP		// Remove
#define FPCS1			CLKRP
#define FPCS2			FSXP
#define FPA0			FSRP
#define FPSCL			CLKXP
#define FPSI			DX_STAT


/* Hardware accessed via FPGA 1 */

static volatile uint8_t* const fpga_ctrl = &hw_params.fpga_ctrl; //(uint8_t*)0x90001000;	/* Mainboard FPGA control outputs				*/
static volatile uint8_t* const fpga_cclk = (uint8_t*)0x90001001;	/* Mainboard/expansion FPGA CCLK/DIN outputs	*/
static volatile uint8_t* const fpga_prog = (uint8_t*)0x90001002;	/* Mainboard/expansion FPGA PROGRAM outputs		*/
static volatile uint8_t* const mb_ctrl = (uint8_t*)0x90001003;	/* Mainboard control signals					*/
static volatile uint8_t* const efpga1_ctrl = (uint8_t*)0x90001004;	/* Expansion 1 FPGA control outputs				*/
static volatile uint8_t* const efpga2_ctrl = (uint8_t*)0x90001005;	/* Expansion 2 FPGA control outputs				*/
static volatile uint8_t* const efpga3_ctrl = (uint8_t*)0x90001006;	/* Expansion 3 FPGA control outputs				*/
static volatile uint8_t* const dsp_1wdat = (uint8_t*)0x90001007;	/* 1-Wire I/O									*/
static volatile uint8_t* const uart_data = (uint8_t*)0x90001008;  /* Uart data register							*/
static volatile uint8_t* const uart_ctrl = (uint8_t*)0x9000100A;	/* Uart control register (write)				*/

#define UART_9600	0x00	/* Select 9600 baud operation */
#define UART_115200	0x01	/* Select 115200 baud operation */

volatile uint8_t* const uart_stat = (uint8_t*)0x9000100A; /* Uart status register (read)					*/
volatile uint8_t* const int_en = (uint8_t*)0x9000100B; /* Interrupt enable register					*/
volatile uint8_t* const int_rst = (uint8_t*)0x9000100C; /* Interrupt reset register						*/
volatile uint8_t* const flash_page = (uint8_t*)0x9000100D; /* Flash EPROM page select						*/

volatile uint8_t* const fp_led = &hw_params.fp_led; //(uint8_t*)0x9000100E;	/* Front panel LED control						*/

volatile uint8_t* const hw_version = &hw_params.hw_version; //(uint8_t*)0x9000100F;	/* Hardware version identifier */

/* Host-port interface */

volatile uint32_t* const hpi_ctrl = (uint32_t*)0xB0004000;  /* Host-port control register               	*/
volatile uint32_t* const hpi_addr = (uint32_t*)0xB0004800;  /* Host-port address register               	*/
volatile uint32_t* const hpi_data = (uint32_t*)0xB0005000;  /* Host-port data register (autoincrement)  	*/
volatile uint32_t* const hpi_datani = (uint32_t*)0xB0005800;  /* Host-port data register (fixed address)  	*/

/* L1 Cache control registers */

volatile uint32_t* const l1d_fbar = (uint32_t*)0x01844030;  /* L1D cache flush base address register		*/
volatile uint32_t* const l1d_fwc = (uint32_t*)0x01844034;  /* L1D cache flush word count register		*/

/* L2 Cache control registers */

volatile uint32_t* const l2_cbar = (uint32_t*)0x01844010;  /* L2 cache clean base address register		*/
volatile uint32_t* const l2_cwc = (uint32_t*)0x01844014;  /* L2 cache clean word count register			*/

/* Battery-backed RAM

   See also nonvol.c for other uses of battery backed RAM.

*/

#define BBSRAM_SIZE	0x7FF8
uint8_t bbsram[BBSRAM_SIZE];
uint8_t* bbsram_base = bbsram; // (uint8_t*)0x90080000;	  /* Battery-backed RAM start					*/
//volatile uint8_t* const bbsram_end = &bbsram[BBSRAM_SIZE];// (uint8_t*)0x90087FF8;	  /* Battery-backed RAM end		*/
#define EVENTLOG_END 0x7F00
volatile eventlog* const eventlog_ptr = (eventlog*)&bbsram[EVENTLOG_END - sizeof(eventlog)]; // (eventlog*)(0x90087F00 - sizeof(eventlog));

// TODO: bbsram_rtc implementation
// volatile uint8_t* const bbsram_rtc = (uint8_t*)0x90087FF8;	  /* Real time clock registers start			*/

/* Pointer to event log memory area			*/
volatile uint8_t* const bootlog = &bbsram[EVENTLOG_END]; // (uint8_t*)0x90087F00;		  /* Pointer to boot log memory area			*/

/* Locks to control access to I/O functions */

static uint32_t dspi2c_busy;				/* DSP i2c function busy						*/

/* Global hydraulic relay drive state */

volatile uint8_t dsp_hyd_output = 0;		/* Current hydraulic output state				*/

/* Control signals for mainboard/expansion FPGA (via (e)fpga_ctrl registers) */

#define FPGA_INIT		0x01			/* FPGA INIT input/output						*/
#define FPGA_DONE		0x02			/* FPGA DONE input								*/

/* Data and clock signals for mainboard/expansion FPGA (via fpga_cclk register) */

#define FPGA_DIN0		0x01			/* Mainboard/expansion FPGA DIN0 output			*/
#define FPGA_CCLK		0x02			/* Mainboard/expansion FPGA CCLK input 			*/

/* Program signals for mainboard/expansion FPGA (via fpga_prog register) */

#define FPGA_PROGRAM	0x01			/* Mainboard FPGA PROGRAM output				*/
#define EFPGA_PROGRAM	0x02			/* Expansion FPGA PROGRAM output				*/

/* Mainboard control signals (via mb_ctrl register) */

#define PER_RESET		0x01			/* Peripheral reset signal						*/
#define SERIAL_EN		0x02			/* RS485 serial port output enable				*/

/* Uart status signals (via uart_stat register) */

#define UART_DR			0x01			/* Data ready									*/
#define UART_AR			0x02			/* Address ready								*/
#define UART_PARITY		0x04			/* Parity error									*/
#define UART_FRAMING	0x08			/* Framing error								*/
#define UART_TBRE		0x10			/* Transmit buffer empty						*/

/* Uart control signals (via uart_stat register) */

#define UART_SPEED		0x01			/* Uart fast clock select						*/
#define UART_ADDRMODE	0x02			/* Uart address mode (9 bits) select			*/

/* Interrupt flags */

#define INT_HINT		0x01			/* Host interrupt								*/
#define INT_RXRDY		0x02			/* RX ready interrupt							*/
#define INT_TXRDY		0x04			/* TX ready interrupt							*/

/* Hardware accessed via expansion bus */

uint16_t mainboard_hw[0x400];
uint16_t slot1_hw[0x400];
uint16_t slot2_hw[0x400];
uint16_t slot3_hw[0x400];

//uint16_t* slottable[4] = { (uint16_t*)0xB0003000,	/* Main board transducer hardware		*/
//						(uint16_t*)0xB0003400,	/* Expansion slot 1						*/
//						(uint16_t*)0xB0003800,	/* Expansion slot 2						*/
//						(uint16_t*)0xB0003C00		/* Expansion slot 3						*/
//};
uint16_t* slottable[4] = { mainboard_hw,	/* Main board transducer hardware		*/
						slot1_hw,	/* Expansion slot 1						*/
						slot2_hw,	/* Expansion slot 2						*/
						slot3_hw	/* Expansion slot 3						*/
};

/* General hardware addresses */

volatile uint8_t* const flash = (uint8_t*)0xA0000000;	/* Flash EPROM area					*/

/* Flags to control hardware restart operation */

uint32_t hw_restart_delay = 0;				/* Timer for hardware restart operation			*/
uint32_t hw_restart_enable = FALSE;		/* Enable flag for hardware restart operation	*/

/* Delay constant for I2C operation */

#define I2C_QBIT		25				/* Quarter bit delay							*/
#define I2C_HBIT		50				/* Half bit delay								*/

extern void netprintf(int s, char* fmt, ...);


/* extras for mock_hardware.c */
static pthread_t json_thread;
static sem_t json_update_sem;

static void* update_json_file(void* arg)
{
	FILE* json_file;
	char* json_filename = (char*)arg;
	printf("update_json_file: %s\n", json_filename);
	json_file = fopen(json_filename, "w");
	int i = 0;
	for (;;)
	{
		++i;
		sem_wait(&json_update_sem);
		fprintf(json_file, "{\"fpled\":%d}\n", *fp_led);
		fflush(json_file);
		rewind(json_file);
	}
}

/* lower level I2C/EEPROM state machine */
enum eeprom_state {
	EEPROM_DEVICE_ID = 0,
	EEPROM_ADDR_HI,
	EEPROM_ADDR_LO,
	EEPROM_WRITE_DATA,
	EEPROM_READ_DATA,
	EEPROM_RESTART,
	EEPROM_STOPPED,
};

static enum eeprom_state eeprom_state = EEPROM_STOPPED;
static enum eeprom_state eeprom_op_state = EEPROM_WRITE_DATA;

static int eeprom_addr = 0;
static int eeprom_device = 0;

/********************************************************************************
  FUNCTION NAME     : hw_bootlog_init
  FUNCTION DETAILS  : Initialise boot logging function.
********************************************************************************/

void hw_bootlog_init(void)
{
	// not required in demo mode
}



/********************************************************************************
  FUNCTION NAME     : hw_bootlog
  FUNCTION DETAILS  : Log boot operation.

					  On entry:

					  seq	Indicates position in boot sequence to log.
					  data	Optional data area to be recorded.
					  len	Length of optional data area (max 16 bytes).

********************************************************************************/

void hw_bootlog(uint32_t seq, uint8_t* data, uint32_t len)
{
	// printf("hw_bootlog: %d\n",seq);
}



/********************************************************************************
  FUNCTION NAME     : hw_tracelog
  FUNCTION DETAILS  : Log trace operation.

					  On entry:

					  seq	Indicates position in trace sequence to log.

********************************************************************************/

void hw_tracelog(uint8_t seq)
{
	// not required in demo mode
}



/********************************************************************************
  FUNCTION NAME     : hw_showlog
  FUNCTION DETAILS  : Show bootlog data.
********************************************************************************/

void hw_showlog(char* args[], int numargs, int skt)
{
	// not required in demo mode
}

/* Hardware accessed via FPGA 3 */

volatile uint16_t* const hw_fault = &hw_params.hw_fault; //  (uint16_t*)0xB000302A;	/* Hardware fault inputs */

#define HW_REVF		0x8000				/* Revision F board sense input					*/

volatile uint16_t* const hw_guard = (uint16_t*)0xB000302C;	/* Hardware guard inputs */

#define HW_GUARD1	0x0001				/* Guard input 1								*/
#define HW_GUARD2	0x0002				/* Guard input 2								*/

static uint32_t guard_state = GUARD_OPEN;	/* Guard state									*/

/********************************************************************************
  FUNCTION NAME     : debug1
  FUNCTION DETAILS  : Write debug1 string to LOG_printf function.
********************************************************************************/

void debugmsg1(void)
{
	// not required in demo mode
}



/********************************************************************************
  FUNCTION NAME     : debug2
  FUNCTION DETAILS  : Write debug2 string to LOG_printf function.
********************************************************************************/

void debugmsg2(void)
{
	// not required in demo mode
}



/********************************************************************************
  FUNCTION NAME     : debug3
  FUNCTION DETAILS  : Write debug3 string to LOG_printf function.
********************************************************************************/

void debugmsg3(void)
{
	// not required in demo mode
}



/********************************************************************************
  FUNCTION NAME     : get_hw_version
  FUNCTION DETAILS  : Returns hardware version identifier.

					  A valid version number is in the range 1 to 15.

					  If the version register reads an invalid result, then
					  version 0 is returned.

					  Flags identifies any hardware specific functions:

						Bit 0	HW_FLAG_REVF	Revision F board ident
													1 = Revision F (with safety mods)
													0 = Revisions A - E


********************************************************************************/

uint8_t get_hw_version(uint32_t* bootver, uint32_t* flags)
{
	uint8_t hw;

	hw = *hw_version;

	if ((hw & 0xF0) == 0xA0)
		hw = hw & 0x0F;
	else
		hw = 0;

	if (bootver) *bootver = hw_params.bootver; //*((uint32_t*)0x90000FC4);

	if (flags) {
		uint32_t f = 0;
		if (hw >= 0x06) {
			if (*hw_fault & HW_REVF) f |= HW_FLAG_REVF; else f &= ~HW_FLAG_REVF;
		}
		*flags = f;
	}

	return(hw);
}



/********************************************************************************
  FUNCTION NAME     : diag_error
  FUNCTION DETAILS  : Display diagnostic error on front panel LED.
********************************************************************************/

void diag_error(uint32_t err)
{
	printf("+++ diag_error: %d\n\n", err);
	exit(err);
	// tight loop warning!
	// TODO: revisit diag_error()

	//uint32_t ctr;

	//while (1) {
	//	for (ctr = 0; ctr < err; ctr++) {
	//		vh = LED_ON;
	//		delay_us(250000);
	//		*fp_led = LED_OFF;
	//		delay_us(250000);
	//	}
	//	delay_us(1000000);
	//}
}



/********************************************************************************
  FUNCTION NAME     : dsp_reset_peripherals
  FUNCTION DETAILS  : Reset peripheral devices by asserting RESETB line.
********************************************************************************/

void dsp_reset_peripherals(void)
{
	/* hijack this function to setup our virtual memory spaces */
	int file;

	file = open("bbram.dat", O_RDWR | O_CREAT, 0666);
	if (file < 0)
		perror("cannot create bbram.dat file\n");
	bbsram_base = mmap(NULL, 32 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, file, 0);
	ftruncate(file, 32 * 1024);
	if (bbsram_base == (void*)-1)
		diag_error(errno);
}

/********************************************************************************
  FUNCTION NAME     : dsp_check_c6713
  FUNCTION DETAILS  : Check if processor is a C6713.
********************************************************************************/

uint32_t dsp_check_c6713(void)
{
	//if (FORCE_FAST) return(TRUE);
	if ((*fpga_ctrl & IP_SLAVE) == 0) return(TRUE);
	return(FALSE);
}

/********************************************************************************
  FUNCTION NAME     : dsp_set_cache
  FUNCTION DETAILS  : Initialise cache operation.
********************************************************************************/
uint32_t dsp_set_cache(uint32_t mode)
{
	return (mode);
	//uint32_t ccfg;

	//*((volatile uint32_t*)L2CLEAN) = 1;			/* Request clean			  */
	//while (*((volatile uint32_t*)L2CLEAN) & 1);	/* Wait for clean to complete */
	//*((volatile uint32_t*)CCFG) = mode;			/* Select cache mode		  */
	//ccfg = *((volatile uint32_t*)CCFG);			/* Force read				  */
	//asm(" nop");
	//asm(" nop");
	//asm(" nop");
	//asm(" nop");
	//asm(" nop");
	//asm(" nop");
	//asm(" nop");
	//asm(" nop");

	//return(ccfg);
}

/********************************************************************************
  FUNCTION NAME     : dsp_io_init
  FUNCTION DETAILS  : Initialise DSP I/O lines ready for use as I2C and SPI
					  lines.
********************************************************************************/
void dsp_io_init(char* hwOutFile)
{
	// take over for reporting state via json file
	// 
	int ret = sem_init(&json_update_sem, 0, 0);
	if (ret != 0)
	{
		perror("json_update_sem");
		return;
	}

	ret = pthread_create(&json_thread, NULL, &update_json_file, hwOutFile);
	if (ret != 0)
	{
		perror("dsp_io_init (json_thread)");
		return;
	}



	// not rqeuired in virtual cube
	// 
	//*spcr0 = 0x00000000;
	//*spcr1 = 0x00000000;

	///* Set pin functions as follows:

	//   FSX0		(HYDCS) 	output
	//   CLKX0 	(HYDSCLK)  	output
	//   CLKR0	(SCL)		input
	//   FSR0		(SDA)		input

	//   Set HYDCS high.

	//   FSX1		(!FPCS)		output
	//   CLKR1	(FPSEL)		output
	//   FSR1		(FPA0)		output
	//   CLKX1	(FPSCL)		output
	//   DX1		(FPSI)		output

	//   Set !FPCS high, SCL low.

	//*/

	//atomic_pcr0_wr(XIOEN | RIOEN | FSXM | CLKXM | HYDCS, 0xFF);

	//*pcr1 = XIOEN | RIOEN | FSXM | CLKRM | FSRM | CLKXM | FPCS1 | FPCS2;

	///* Ensure the hydraulic relay driver state is initialised */

	//dsp_spi_hyd_init(0);

	///* Create lock for access to DSP i2c functions */

	//dspi2c_busy = 0;
}

/********************************************************************************
  FUNCTION NAME     : dsp_wdoga_state
  FUNCTION DETAILS  : Set state of WDOGA output.
********************************************************************************/
void dsp_wdoga_state(int state)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_lock
  FUNCTION DETAILS  : Lock the DSP I2C hardware to allow uninterrupted access.
********************************************************************************/
void dsp_i2c_lock(void)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_unlock
  FUNCTION DETAILS  : Unlock the DSP I2C hardware.
********************************************************************************/
void dsp_i2c_unlock(void)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_free
  FUNCTION DETAILS  : Check if the DSP I2C hardware is free.
********************************************************************************/
int dsp_i2c_free(void)
{
	// not required in virtual cube
	return TRUE;
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_restart
  FUNCTION DETAILS  : Send I2C restart state.
********************************************************************************/
void dsp_i2c_restart(void)
{
	eeprom_state = EEPROM_DEVICE_ID;
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_start
  FUNCTION DETAILS  : Send I2C start state.
********************************************************************************/
void dsp_i2c_start(void)
{
	eeprom_state = EEPROM_DEVICE_ID;
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_stop
  FUNCTION DETAILS  : Generate I2C stop condition
********************************************************************************/
void dsp_i2c_stop(void)
{
	eeprom_state = EEPROM_STOPPED;
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_send
  FUNCTION DETAILS  : Send I2C data byte.

					  Returns 0 if the byte has been acknowledged correctly.

********************************************************************************/
int dsp_i2c_send(uint8_t data)
{
	int ack = 0;
	switch (eeprom_state)
	{
	case EEPROM_DEVICE_ID:
		eeprom_device = (data & 0xfe);
		if ((data & 0x01U) == 0x01U)
			eeprom_state = EEPROM_READ_DATA;
		else
			eeprom_state = EEPROM_ADDR_HI;
		break;
	case EEPROM_ADDR_HI:
		eeprom_addr  = ((int)data << 8);
		eeprom_state = EEPROM_ADDR_LO;
		break;
	case EEPROM_ADDR_LO:
		eeprom_addr |= (int)data;
		eeprom_state = EEPROM_WRITE_DATA;
		break;
	case EEPROM_WRITE_DATA:
		virtual_eeprom_write(vh_get_eeprom_by_address(eeprom_device), eeprom_addr, data);
		eeprom_addr++;
	default:
		ack = 1;
		break;
	}
	return(ack);
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_read
  FUNCTION DETAILS  : Read I2C data byte.

					  Returns received data byte.

					  On entry:

					  ack	indicates if an acknowledgement should be sent

********************************************************************************/
uint8_t dsp_i2c_read(int ack)
{
	uint8_t data = 0;
	if (eeprom_state == EEPROM_READ_DATA)
	{
		virtual_eeprom_read(vh_get_eeprom_by_address(eeprom_device), eeprom_addr, &data);
		eeprom_addr++;
	}
	return(data);
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_send_word
  FUNCTION DETAILS  : Send four bytes to I2C.

					  Returns 0 if all bytes have been acknowledged correctly.

********************************************************************************/
int dsp_i2c_send_word(uint32_t word)
{
	// not required in virtual cube
	return (0);
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_read_word
  FUNCTION DETAILS  : Read four data bytes from I2C.

					  Returns received data word.

					  On entry:

					  ack	indicates if an acknowledgement should be sent
							after final byte

********************************************************************************/
uint32_t dsp_i2c_read_word(int ack)
{
	// not required in virtual cube
	return (0);
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_eeprom_writeaddr
  FUNCTION DETAILS  : Write an address to a mainboard I2C EEPROM.

					  Repeats first bus operation until acknowledgement is received.
					  This allows the function to be called when a previous EEPROM
					  write operation may not have been completed.

					  I2C bus lock must have been obtained before calling this
					  function.

					  On entry:

					  i2c_addr	Address of I2C device to be written

					  addr		Address to be written to EEPROM

********************************************************************************/
int dsp_i2c_eeprom_writeaddr(uint8_t i2c_addr, int addr)
{
	// not required in virtual cube
	return (0);
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_eeprom_poll
  FUNCTION DETAILS  : Poll for completion of an EEPROM write operation.

					  On entry:

					  i2c_addr	Address of I2C device to be polled

********************************************************************************/
void dsp_i2c_eeprom_poll(uint8_t i2c_addr)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_eeprom_write
  FUNCTION DETAILS  : Write a single byte of data to a mainboard I2C EEPROM.

					  On entry:

					  i2c_addr	Address of I2C device to be written

					  addr		Start address within EEPROM

					  data		Data byte to be written

********************************************************************************/
int dsp_i2c_eeprom_write(uint8_t i2c_addr, int addr, uint8_t data)
{
	virtual_eeprom_write(vh_get_eeprom_by_address(i2c_addr), addr, data);
	return(FALSE);
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_eeprom_read
  FUNCTION DETAILS  : Read a single byte of data from a mainboard I2C EEPROM.

					  On entry:

					  i2c_addr	Address of I2C device to be read

					  addr		Start address within EEPROM

					  On exit:

					  Returns the data	byte read from the EEPROM

********************************************************************************/
int dsp_i2c_eeprom_read(uint8_t i2c_addr, int addr, uint8_t* data)
{
	virtual_eeprom_read(vh_get_eeprom_by_address(i2c_addr), addr, data);
	return(FALSE);
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_eeprom_writeblock
  FUNCTION DETAILS  : Write a block of data to a mainboard I2C EEPROM.

					  On entry:

					  i2c_addr	Address of I2C device to be written

					  addr		Start address within EEPROM

					  data		Points to start of data block to be written

					  len		Number of bytes to be written

					  flags		Flags controlling operation

								Bit 0 : Generate checksum and write into next
										location after valid data.

********************************************************************************/
void dsp_i2c_eeprom_writeblock(uint8_t i2c_addr, int addr, uint8_t* data, int len,
	int flags)
{
	virtual_eeprom_write_block(vh_get_eeprom_by_address(i2c_addr),
		addr, data, (size_t)len, flags);
}

/********************************************************************************
  FUNCTION NAME     : dsp_i2c_eeprom_readblock
  FUNCTION DETAILS  : Read a block of data from a mainboard I2C EEPROM.

					  On entry:

					  i2c_addr	Address of I2C device to be read

					  addr		Start address within EEPROM

					  data		Points to start of data block to be read

					  len		Number of bytes to be read

					  flags		Flags controlling operation

								Bit 1 : Use checksum to validate data before
										writing to destination area.

********************************************************************************/
void dsp_i2c_eeprom_readblock(uint8_t i2c_addr, int addr, uint8_t* data, int len,
	int flags)
{
	virtual_eeprom_read_block(vh_get_eeprom_by_address(i2c_addr),
		addr, data, len, flags);
}

/********************************************************************************
  FUNCTION NAME     : dsp_spi_delay
  FUNCTION DETAILS  : Delay for one half SPI bit time.
********************************************************************************/
void dsp_spi_delay(void)
{
}

/********************************************************************************
  FUNCTION NAME     : dsp_spi_hyd_init
  FUNCTION DETAILS  : Initialise hydraulic control outputs.

********************************************************************************/
void dsp_spi_hyd_init(uint8_t data)
{
	// not required in virtual cube
	// TODO: initial state of the relays?

	//int bitcount = 8;

	//atomic_pcr0_wr(0, HYDSCLK);		/* Force SCLK low */
	//dsp_spi_delay();				/* Half bit delay */
	//atomic_pcr0_wr(0, HYDCS);		/* Set CS low */
	//dsp_spi_delay();				/* Half bit delay */

	//while (bitcount) {
	//	atomic_pcr0_wr((data & 0x80) ? HYDDIN : 0, (HYDSCLK | HYDDIN));	/* Force SCLK low, set HYDDIN to required state	*/
	//	dsp_spi_delay();				/* Half bit delay */
	//	atomic_pcr0_wr(HYDSCLK, 0);	/* Pulse SCLK high */
	//	dsp_spi_delay();				/* Half bit delay */
	//	atomic_pcr0_wr(0, HYDSCLK);	/* End of SCLK pulse */
	//	data = data << 1;				/* Shift next bit into position */
	//	bitcount--;					/* Count data bits */
	//}

	///* Now take HYDCS high to load data into output latches. */

	//dsp_spi_delay();				/* Half bit delay */
	//atomic_pcr0_wr(HYDCS, 0);		/* Set CS high */
}
// #pragma CODE_SECTION(dsp_spi_update, "realtime")

/********************************************************************************
  FUNCTION NAME     : dsp_spi_update
  FUNCTION DETAILS  : Perform update of SPI conbtrol signals.
********************************************************************************/
void dsp_spi_update(void)
{
	// TODO: updating of relays?
	
	hw_params.dsp_hyd_output = dsp_hyd_output;

	//static uint32_t spi_state = 0;
	//static uint8_t data;

	//switch (spi_state) {
	//case 0:
	//	atomic_pcr0_wr(0, HYDSCLK);	/* Force SCLK low 		*/
	//	data = dsp_hyd_output;		/* Copy required output	*/
	//	break;
	//case 1:
	//	atomic_pcr0_wr(0, HYDCS);	/* Set CS low */
	//	break;
	//case 2:
	//case 4:
	//case 6:
	//case 8:
	//case 10:
	//case 12:
	//case 14:
	//case 16:
	//	atomic_pcr0_wr((data & 0x80) ? HYDDIN : 0, (HYDSCLK | HYDDIN));	/* Force SCLK low, set HYDDIN to required state	*/
	//	data = data << 1;			/* Shift next bit into position	*/
	//	break;
	//case 3:
	//case 5:
	//case 7:
	//case 9:
	//case 11:
	//case 13:
	//case 15:
	//case 17:
	//	atomic_pcr0_wr(HYDSCLK, 0);	/* Pulse SCLK high */
	//	break;
	//case 18:
	//	atomic_pcr0_wr(0, HYDSCLK);	/* Force SCLK low */
	//	break;
	//case 19:
	//	atomic_pcr0_wr(HYDCS, 0);	/* Set CS high */
	//	break;
	//}
	//if (spi_state >= 19) spi_state = 0; else spi_state++;
}

/********************************************************************************
  FUNCTION NAME     : dsp_spi_send
  FUNCTION DETAILS  : Send data byte via SPI interface to hydraulic control
					  outputs.

********************************************************************************/
void dsp_spi_send(uint8_t data)
{
	dsp_hyd_output = data;
}

/********************************************************************************
  FUNCTION NAME     : dsp_1w_init
  FUNCTION DETAILS  : Initialise FPGA1 1-wire port.
********************************************************************************/
void dsp_1w_init(void)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : dsp_1w_reset
  FUNCTION DETAILS  : Reset all devices on FPGA1 1-wire bus.

					  Returns non-zero if no 1-wire device is present.
********************************************************************************/
int dsp_1w_reset(void)
{
	// not required in virtual cube
	int present = 0;
	return(present);
}

/********************************************************************************
  FUNCTION NAME     : dsp_1w_write
  FUNCTION DETAILS  : Write a byte to the FPGA1 1-wire bus.
********************************************************************************/
void dsp_1w_write(uint8_t data)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : dsp_1w_read
  FUNCTION DETAILS  : Read a byte from the FPGA1 1-wire bus.
********************************************************************************/
uint8_t dsp_1w_read(void)
{
	// not required in virtual cube
	return(0);
}

/********************************************************************************
  FUNCTION NAME     : dsp_1w_readeeprom
  FUNCTION DETAILS  : Read EEPROM data from the mainboard 1-wire EEPROM device.

					  Returns non-zero if no 1-wire device was detected.
********************************************************************************/
int dsp_1w_readeeprom(int addr, int len, uint8_t* data)
{
	// TODO: assuming there's no CRC checking
	virtual_eeprom_read_block(vh_get_onewire_eeprom(), addr, data, len, 0);
	return(NO_ERROR);
}

/********************************************************************************
  FUNCTION NAME     : dsp_1w_writeeeprom
  FUNCTION DETAILS  : Write EEPROM data to the mainboard 1-wire EEPROM device.

					  Returns non-zero if no 1-wire device was detected.
********************************************************************************/
int dsp_1w_writeeeprom(int addr, uint8_t data)
{
	virtual_eeprom_write(vh_get_onewire_eeprom(), addr, data);
	return(NO_ERROR);
}

/********************************************************************************
  FUNCTION NAME     : dsp_1w_writepage
  FUNCTION DETAILS  : Write EEPROM page to the mainboard 1-wire EEPROM device.

					  Returns non-zero if no 1-wire device was detected.
********************************************************************************/
int dsp_1w_writepage(int page, uint8_t* data, int len, int flags)
{
	// TODO: ignoring checksum function
	int addr = page << 5;
	if (len > 31) len = 31;

	virtual_eeprom_write_block(vh_get_onewire_eeprom(), addr, data, len, 0);
	return(NO_ERROR);
}

/********************************************************************************
  FUNCTION NAME     : dsp_1w_readpage
  FUNCTION DETAILS  : Read EEPROM page from the mainboard 1-wire EEPROM device.

					  Returns non-zero if no 1-wire device was detected or if
					  a checksum error occurred.
********************************************************************************/
int dsp_1w_readpage(int page, uint8_t* data, int len, int flags)
{
	// TODO: ignoring checksum function
	int addr = page << 5;
	if (len > 31) len = 31;

	virtual_eeprom_read_block(vh_get_onewire_eeprom(), addr, data, len, 0);
	return(NO_ERROR);
}

/********************************************************************************
  FUNCTION NAME     : dsp_1w_readrom
  FUNCTION DETAILS  : Read ROM value from the FPGA1 1-wire bus.

					  Returns non-zero if no 1-wire device was detected.
********************************************************************************/
int dsp_1w_readrom(uint32_t* lo, uint32_t* hi)
{
	*lo = hw_params.one_wire_rom_lo;
	*hi = hw_params.one_wire_rom_hi;
	return(0);
}

/********************************************************************************
  FUNCTION NAME     : hw_1w_init
  FUNCTION DETAILS  : Initialise 1-wire port.
********************************************************************************/
void hw_1w_init(uint8_t* control)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : hw_1w_reset
  FUNCTION DETAILS  : Reset all devices on 1-wire bus.

					  On exit:

					  Returns zero if a device is present on the 1-wire bus.

********************************************************************************/
int hw_1w_reset(uint8_t* control)
{
	// not required in virtual cube
	return(0);
}

/********************************************************************************
  FUNCTION NAME     : hw_1w_write
  FUNCTION DETAILS  : Write a byte to the 1-wire bus.
********************************************************************************/
void hw_1w_write(uint8_t* control, uint8_t data)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : hw_1w_read
  FUNCTION DETAILS  : Read a byte from the 1-wire bus.
********************************************************************************/
uint8_t hw_1w_read(uint8_t* control)
{
	// not required in virtual cube
	return(0);
}

/********************************************************************************
  FUNCTION NAME     : dsp_1w_checkcrc
  FUNCTION DETAILS  : Check for a valid CRC byte within the 64 bit
					  ROM value.

					  Returns non-zero if the CRC check failed.

********************************************************************************/
int dsp_1w_checkcrc(uint32_t lo, uint32_t hi)
{
	return(NO_ERROR);
}


#ifndef LOADER

/********************************************************************************
  FUNCTION NAME     : dsp_fp_delay
  FUNCTION DETAILS  : Delay for one half front panel interface bit time.
********************************************************************************/
void dsp_fp_delay(void)
{
}

/********************************************************************************
  FUNCTION NAME     : dsp_fp_pulse
  FUNCTION DETAILS  : Generate pulse on front panel CS line.
********************************************************************************/
void dsp_fp_pulse(void)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : dsp_fp_seta0
  FUNCTION DETAILS  : Set state of front panel address line A0.
********************************************************************************/

void dsp_fp_seta0(uint32_t state)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : dsp_fp_send
  FUNCTION DETAILS  : Send data byte to front panel.
********************************************************************************/

void dsp_fp_send(uint8_t data, uint8_t addr, uint8_t dest)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : fp_cls
  FUNCTION DETAILS  : Clear memory of front panel display.
********************************************************************************/
void fp_cls(void)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : fp_print
  FUNCTION DETAILS  : Print string to front panel.

					  On entry:

					  x			X position to start printing (in pixels)
					  y			Y position for printing (in character rows)
					  string	Zero terminated string to print

********************************************************************************/
void fp_print(int x, int y, char* string)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : fp_init
  FUNCTION DETAILS  : Initialise front panel display.

					  See Epson S1D10605 series data sheet for details.

********************************************************************************/
void fp_init(void)
{
	// not required in virtual cube
}

#endif /* LOADER */


/********************************************************************************
  FUNCTION NAME     : operate_relay
  FUNCTION DETAILS  : Operate relay outputs on the hydraulic/safety interface.

					  On entry:

					  set	Determines which output(s) are to be set
					  clr	Determines which output(s) are to be cleared

					  On exit:

					  Returns current relay state.

********************************************************************************/
uint8_t operate_relay(uint8_t set, uint8_t clr)
{
	static uint8_t rly_state = 0;

	/* Modify local relay state */

	rly_state = rly_state & (~clr) | set;

	/* Update hardware */

	dsp_spi_send(rly_state);

	return(rly_state);
}

/********************************************************************************
  FUNCTION NAME     : local_io
  FUNCTION DETAILS  : Control local I/O on the mainboard.

					  The following I/O lines are controlled or may be monitored.

						EM1L_SENSE (Rev F onwards)
						EM_SENSE
						HIGH_SENSE
						EM1H_SENSE (Rev F onwards)
						PILOT_SENSE
						LOW_SENSE
						GPIN1_SENSE
						GPIN2_SENSE
						ES_RELAY_SLAVE
						GPOUT1_DRIVE
						GPOUT2_DRIVE
						TXDR1LED_DRIVE
						TXDR2LED_DRIVE
						SVLED_DRIVE
						EM2H_SENSE (Rev F onwards)
						EM2L_SENSE (Rev F onwards)

					  On entry:

					  set	Determines which output(s) are to be set
					  clr	Determines which output(s) are to be cleared

					  On exit:

					  Returns current I/O state.

********************************************************************************/
uint16_t local_io(uint16_t set, uint16_t clr)
{
	static uint16_t op_state = 0;
	uint16_t ip_state = 0;

	/* Modify local output state state */

	op_state = op_state & (~clr) | set;

	hw_params.local_io_outputs = op_state;
	ip_state = hw_params.local_io_inputs;

	/* Return combined input and output state */
	return(((ip_state & 0xC1FF) ^ 0xC1FF) | (op_state & 0x3E00));
}

/********************************************************************************
  FUNCTION NAME     : local_io_rdop
  FUNCTION DETAILS  : Read local I/O outputs on the mainboard. Used by the
					  read_digio_output function to readback output states.

					  On exit:

					  Returns current output port state.

********************************************************************************/
uint16_t local_io_rdop(void)
{
	static uint16_t op_state = 0;
	op_state = hw_params.local_io_outputs;

	/* Return output state */
	return(op_state & 0x3E00);
}

/********************************************************************************
  FUNCTION NAME     : init_local_io
  FUNCTION DETAILS  : Initialise the local I/O hardware on the mainboard.
********************************************************************************/
void init_local_io(void)
{
	/* Initialise outputs to known state */
	local_io(0, 0xFFFF);
}

/********************************************************************************
  FUNCTION NAME     : fp_read_state
  FUNCTION DETAILS  : Read the switch state of the front panel.
********************************************************************************/
uint8_t fp_read_state(void)
{
	uint8_t fp_state;

	fp_state = hw_params.fp_state;
	return(fp_state);
}

/********************************************************************************
  FUNCTION NAME     : fp_write_state
  FUNCTION DETAILS  : Write the LED state of the front panel.
********************************************************************************/
void fp_write_state(uint8_t fp_state)
{
	// TODO: correct funtionality here?
	hw_params.fp_state = fp_state;
}

/********************************************************************************
  FUNCTION NAME     : get_sys_state
  FUNCTION DETAILS  : Returns state of hardware system state inputs.
********************************************************************************/
uint8_t get_sys_state(void)
{
	// TODO: fixed hardware state (only seems to be used by debug.c)
	uint8_t state;
	state = 0;
	return(state);
}

/********************************************************************************
  FUNCTION NAME     : check_initialise
  FUNCTION DETAILS  : Check for INITIALISE pushbutton request and reset IP
					  address if required.

					  Button must be active for 100 consecutive 1ms samples
					  before any action is taken.
********************************************************************************/
void check_initialise(void)
{
	// mock of the initialise button feature
	if (hw_params.initialise_button)
	{
		fpled_ctrl(LED_PULSE);
	}
}

/********************************************************************************
  FUNCTION NAME     : pulse_fpga_cclk
  FUNCTION DETAILS  : Generate high pulse on FPGA CCLK line.
********************************************************************************/

void pulse_fpga_cclk(uint8_t data)
{
	// not required in virtual cube
}

#undef TIMING
/********************************************************************************
  FUNCTION NAME     : write_bitstream
  FUNCTION DETAILS  : Write a bitstream to the selected FPGA device.

					  On entry:

					  data	Points to the bitstream code to be loaded into the
							FPGA device.

					  ctrl	Points to the control register used to address the
							device.

********************************************************************************/
void write_bitstream(uint8_t* data, volatile uint8_t* const ctrl)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : config_mainboard_fpga
  FUNCTION DETAILS  : Configure all mainboard FPGA devices.

					  On entry:

					  data	Points to the bitstream code to be loaded into the
							FPGA devices.

********************************************************************************/
void config_mainboard_fpga(uint8_t* data)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : config_exp_fpga
  FUNCTION DETAILS  : Configure all expansion board FPGA devices.

					  On entry:

					  data1	Points to the bitstream code to be loaded into the
							expansion slot 1 FPGA devices.

					  data2	Points to the bitstream code to be loaded into the
							expansion slot 2 FPGA devices.

					  data3	Points to the bitstream code to be loaded into the
							expansion slot 3 FPGA devices.

********************************************************************************/
void config_exp_fpga(uint8_t* data1, uint8_t* data2, uint8_t* data3)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : init_hpi
  FUNCTION DETAILS  : Initialise host-port interface.
********************************************************************************/
void init_hpi(void)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : hpi_blockwrite
  FUNCTION DETAILS  : Write a block of data to the memory of DSP B via the
					  host-port interface.
********************************************************************************/
void hpi_blockwrite(uint8_t* dst, uint8_t* src, uint32_t size)
{
	// TODO: need a better think about this - what really is 'dst'?
	memcpy(dst, src, size);
}

/********************************************************************************
  FUNCTION NAME     : hpi_blockread
  FUNCTION DETAILS  : Read a block of data from the memory of DSP B via the
					  host-port interface.
********************************************************************************/
void hpi_blockread(uint8_t* dst, uint8_t* src, uint32_t size)
{
	// TODO: need a better think about this - what really is 'dst'?
	memcpy(dst, src, size);
}

/********************************************************************************
  FUNCTION NAME     : hpi_write4
  FUNCTION DETAILS  : Write a 4-byte word to the memory of DSP B via host-port
					  interface.
********************************************************************************/
void hpi_write4(uint32_t* addr, uint32_t data)
{
	// TODO: need a better think about this - hpi_write4
	*addr = data;
	//*((uint32_t*)(intptr_t)addr) = data;
}

/********************************************************************************
  FUNCTION NAME     : hpi_read4
  FUNCTION DETAILS  : Read a 4-byte word from the memory of DSP B via host-port
					  interface.
********************************************************************************/
uint32_t hpi_read4(uint32_t* addr)
{
	// TODO: need a better think about this - hpi_read4
	//uint32_t data = *((uint32_t*)(intptr_t)addr);
	uint32_t data = *addr;
	return(data);
}

void hpi_modify4(uint32_t* addr, uint32_t clr, uint32_t set, uint32_t* dst)
{
	*addr = *addr & ~clr | set;
	if (dst != NULL)
		*dst = *addr;
}

/********************************************************************************
  FUNCTION NAME     : init_dspb
  FUNCTION DETAILS  : Initialise DSP B via host-port interface.
********************************************************************************/
void init_dspb(void)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : boot_dspb
  FUNCTION DETAILS  : Load code into DSP B via host-port interface and begin
					  execution.

					  Destination	Holds address where code will be loaded
									in DSP B memory space. This is assumed to
									be on a 4-byte boundary.

********************************************************************************/
void boot_dspb(uint8_t* code, uint32_t destination, uint32_t length, uint32_t start)
{
	/* we don't have a second DSP but we can fork the process to mimic this */
	int fd = open("/dev/zero", O_RDWR);
	if (fd < 0)
		diag_error(errno);
	dspb_shared_c3appdata = mmap(NULL, 16 * 1024 * 1024, PROT_EXEC | PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (dspb_shared_c3appdata == (void*)-1)
		diag_error(errno);
	dspb_shared_chandata = mmap(NULL, 16 * 1024 * 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (dspb_shared_chandata == (void*)-1)
		diag_error(errno);
	close(fd);
	printf("boot_dspb: complete\n");
}

/********************************************************************************
  FUNCTION NAME     : alloc_dspb_general
  FUNCTION DETAILS  : Allocate space in DSP B general memory area.
********************************************************************************/
uint8_t* alloc_dspb_general(uint32_t size)
{
	uint8_t* ptr;
	static uint32_t general_base = 0;
	if ((general_base + size) > 0x100000) return (NULL);

	ptr = (shared_memory + general_base);
	general_base += size;
	return (ptr);
}

/********************************************************************************
  FUNCTION NAME     : dspb_i2c_restart
  FUNCTION DETAILS  : Send DSPB I2C restart state.
********************************************************************************/
void dspb_i2c_restart(void)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : dspb_i2c_start
  FUNCTION DETAILS  : Send DSPB I2C start state.
********************************************************************************/
void dspb_i2c_start(void)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : dspb_i2c_stop
  FUNCTION DETAILS  : Generate DSPB I2C stop condition
********************************************************************************/
void dspb_i2c_stop(void)
{
	// not required in virtual cube
}

/********************************************************************************
  FUNCTION NAME     : dspb_i2c_send
  FUNCTION DETAILS  : Send DSPB I2C data byte.

					  Returns 0 if the byte has been acknowledged correctly.

********************************************************************************/
int dspb_i2c_send(uint8_t data)
{
	return(0);
}

/********************************************************************************
  FUNCTION NAME     : dspb_i2c_read
  FUNCTION DETAILS  : Read DSPB I2C data byte.

					  Returns received data byte.

					  On entry:

					  ack	indicates if an acknowledgement should be sent

********************************************************************************/
uint8_t dspb_i2c_read(int ack)
{
	return(0);
}

/********************************************************************************
  FUNCTION NAME     : fpled_ctrl
  FUNCTION DETAILS  : Control front panel LED.
********************************************************************************/
void fpled_ctrl(uint8_t ctrl)
{
	switch (ctrl) {
	case LED_ON:
	case LED_OFF:
		*fp_led = ctrl;
		break;
	case LED_PULSE:
		*fp_led |= ctrl;
		break;
	}
	if (&json_update_sem != SEM_FAILED)
		sem_post(&json_update_sem);
}

#ifdef DSPB

/********************************************************************************
  FUNCTION NAME     : handctrl
  FUNCTION DETAILS  : Read/write hand-controller I/O on the mainboard.

					  On entry:

					  led1	Controls the state of LED output 1
					  led2	Controls the state of LED output 2

					  On exit:

					  Returns current hand-controller input state.

********************************************************************************/

uint8_t handctrl(int led1, int led2)
{
	static uint8_t op_state;
	uint8_t ip_state;

	/* Modify local output state state */

	op_state = ((led1) ? 0x01 : 0) | ((led2) ? 0x02 : 0);

	/* Update output port */

	dsp_i2c_start();
	dsp_i2c_send(0x40);					/* I/O hardware I2C address			*/
	dsp_i2c_send(0x03);					/* Register 0x03 - port2 output		*/
	dsp_i2c_send(op_state);				/* Set port 2 output state			*/
	dsp_i2c_stop();

	/* Read input port */

	dsp_i2c_start();
	dsp_i2c_send(0x40);					/* I/O hardware I2C address			*/
	dsp_i2c_send(0x00);					/* Register 0x00 - port1 input		*/
	dsp_i2c_restart();
	dsp_i2c_send(0x41);					/* I/O hardware I2C address			*/
	ip_state = dsp_i2c_read(1);			/* Get port 1 input state			*/
	dsp_i2c_stop();

	/* Return current hand-controller state */

	return(ip_state & 0x3E);
}



/********************************************************************************
  FUNCTION NAME     : init_handctrl
  FUNCTION DETAILS  : Initialise the hand-controller I/O hardware on the
					  mainboard.
********************************************************************************/

void init_handctrl(void)
{
	dsp_i2c_start();
	dsp_i2c_send(0x40);		/* I/O hardware I2C address				*/
	dsp_i2c_send(0x06);		/* Register 0x06 - port1 configuration	*/
	dsp_i2c_send(0xFF);		/* Set port 1 direction					*/
	dsp_i2c_restart();
	dsp_i2c_send(0x40);		/* I/O hardware I2C address				*/
	dsp_i2c_send(0x07);		/* Register 0x07 - port2 configuration	*/
	dsp_i2c_send(0xFC);		/* Set port 2 direction					*/
	dsp_i2c_stop();

	/* Initialise LED outputs to known state */

	handctrl(0, 0);
}

#endif


#ifndef LOADER
/********************************************************************************
  FUNCTION NAME     : hw_guard_action
  FUNCTION DETAILS  : Take guard action for fault or open state.
********************************************************************************/
void hw_guard_action(void)
{
	// TODO: needs revisiting
	//uint32_t mode = 0;

	///* Get guard mode flags */

	//hyd_read_guard_mode(&mode);

	///* Take action depending on the guard mode flags */

	//if (!(mode & HYD_DIS_FORCE_SETUP)) {

	//	/* Force setup mode is not disabled, so request setup mode */

	//	CtrlForceSetup();
	//}

	//if (mode & HYD_DIS_HIGH_PRES) {

	//	/* Guard mode flag is set to disable high pressure. If the hydraulic type is HYDRAULIC and low pressure
	//	   control is available and the current state is high pressure, then request low pressure.
	//	*/

	//	if (((snvbs->hyd_type & HYD_TYPE_MASK) == HYD_TYPE_HYDRAULIC) &&
	//		(snvbs->hyd_output[1].flags & HYD_FLAG_AVAILABLE) &&
	//		((hyd_read_mrequest() & 0xFF) >= REQUEST_4)) {

	//		/* Request low pressure */

	//		hyd_control(REQUEST_3);
	//	}
	//}
}


#define GUARD_FAULT_TIME	25		/* Delay before identifying guard fault 0.5 sec */
/********************************************************************************
  FUNCTION NAME     : hw_check_guard
  FUNCTION DETAILS  : Called every 20ms from the timer interrupt.

					  Check guard status and take action if guard is open. Also
					  update state of setup mode relay.
********************************************************************************/
int CtrlIsSetup();
void hw_check_guard(void)
{
	// TODO: needs revisiting
	static uint32_t guard_fault_count = GUARD_FAULT_TIME;
	uint32_t hwflags;
	uint32_t setup;

	uint32_t newstate;

	get_hw_version(NULL, &hwflags);
	if (hwflags & HW_FLAG_REVF) {

		if (!hyd_read_simulation_mode()) {

			setup = CtrlIsSetup();

			/* Not in simulation mode, so process guard inputs */

			newstate = *hw_guard & (HW_GUARD1 | HW_GUARD2);

			/* Check the state of the guard inputs. Test for valid states and process
			   fault condition if required.

			   To prevent false indications due to race conditions, the error state must exist
			   for at least 50 consecutive samples (1 second) before the fault condition is
			   indicated.

			*/

			switch (guard_state) {

			case GUARD_OPEN:
				switch (newstate) {

				case 0: /* Closed */
					guard_fault_count = GUARD_FAULT_TIME; /* Reset fault counter */
					guard_state = GUARD_CLOSED;
					eventlog_log(LogTypeGuardStatusChange, GUARD_CLOSED, 0);
					break;

				case HW_GUARD1: /* Fault */
				case HW_GUARD2:

					/* Guard fault */

					if (guard_fault_count) {
						guard_fault_count--;
					}
					else {
						guard_state = GUARD_FAULT;	/* Fault delay has elapsed, move to fault state */
						eventlog_log(LogTypeGuardStatusChange, GUARD_FAULT, 0);
					}
					break;

				case HW_GUARD1 | HW_GUARD2: /* Open */
					guard_fault_count = GUARD_FAULT_TIME; /* Reset fault counter */
					break;


				}
				break;

			case GUARD_CLOSED:
				switch (newstate) {

				case 0: /* Closed */
					guard_fault_count = GUARD_FAULT_TIME; /* Reset fault counter */
					break;


				case HW_GUARD1: /* Fault */
				case HW_GUARD2:

					/* Guard fault */

					if (guard_fault_count) {
						guard_fault_count--;
					}
					else {

						/* Fault delay has elapsed, so take action and move to the FAULT state */

						hw_guard_action();
						guard_state = GUARD_FAULT;
						eventlog_log(LogTypeGuardStatusChange, GUARD_FAULT, 0);
					}
					break;

				case HW_GUARD1 | HW_GUARD2: /* Open */

					/* Guard open, so take action and move to the OPEN state */

					guard_fault_count = GUARD_FAULT_TIME; /* Reset fault counter */
					hw_guard_action();
					guard_state = GUARD_OPEN;
					eventlog_log(LogTypeGuardStatusChange, GUARD_OPEN, 0);
					break;
				}
				break;

			case GUARD_FAULT:
				switch (newstate) {

					/* In the fault state, we ignore direct transitions to the closed state.
					   The guard must be opened before we are allowed to sense the closed state
					   again.
					*/

				case 0: /* Closed */
					break;

				case HW_GUARD1: /* Fault */
				case HW_GUARD2:
					break;

				case HW_GUARD1 | HW_GUARD2: /* Open */
					guard_fault_count = GUARD_FAULT_TIME; /* Reset fault counter */
					guard_state = GUARD_OPEN;
					eventlog_log(LogTypeGuardStatusChange, GUARD_OPEN, 0);
					break;
				}
				break;
			}

			/* Update setup mode relay state */

			hyd_modify_op((setup ? 0 : HYD_OP_SETUP), (setup ? HYD_OP_SETUP : 0));
		}
		else {

			/* Simulation mode, so force guard closed state and turn off setup output */

			guard_state = GUARD_CLOSED;
			hyd_modify_op(0, HYD_OP_SETUP);
		}

	}
	else {
		guard_state = GUARD_NOSUPPORT;
	}
}

/********************************************************************************
  FUNCTION NAME     : hw_get_guard_state
  FUNCTION DETAILS  : Get current guard status.
********************************************************************************/
uint32_t hw_get_guard_state(void)
{
	return(guard_state);
}

#endif /* LOADER */

