/********************************************************************************
  MODULE NAME   	: timer
  PROJECT			: Control Cube / Signal Cube
  MODULE DETAILS  	: Handles regular monitoring and updating of system 
  					  configuration.
********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <std.h>
#include <sys.h>
#include <sem.h>
#include <hwi.h>
#include <log.h>
#include <c62.h>

//#include "ccubecfg.h"
#include "csl_cache.h"

#include "headers.h"
#include "tcp_ip/support.h"
#include "shared.h"
#include "cnet.h"
#include "msgserver.h"
#include "hydraulic.h"

extern volatile uint32_t prd_enable;

int event_testcode(void);
void hc_timer_support(void);

/********************************************************************************
  FUNCTION NAME   	: timerintr
  FUNCTION DETAILS  : Performs general system monitoring functions. Called every
  					  5ms by the PRD module.
********************************************************************************/

void timerintr(void)
{
static uint32_t counter = 0;

if (prd_enable == FALSE) return;

/* The variable 'counter' counts from 0 to 19 before resetting. Tasks can be
   performed at intervals between 5ms and 0.1s as required.
*/

if (++counter == 20) {
  counter = 0;
  
  /* Check for hardware restart request (after firmware load) */
  
  if (hw_restart_delay) {
    if (--hw_restart_delay == 0) {
      if (hw_restart_enable) {
        hw_restart();	/* Call restart code */
      }
    }
  }

}

/* Spread the regular tasks over a number of interrupt calls */

switch(counter & 0x03) {
  case 0:
#ifdef CONTROLCUBE
#ifndef LOADER  
    hyd_update(HYD_UPDATE);		/* Update hydraulic status */
#endif
#endif
    cnet_monitor();				/* Monitor CNet status */
    break;
  case 1:
#ifdef CONTROLCUBE
#ifndef LOADER  
    hyd_update(HYD_MONITOR);	/* Update hydraulic status */
#endif
	grp_update_status();		/* Update group status flags */
#endif
	timeout_handler();			/* Update timeout counters for message handlers */
    break;
  case 2:
#ifndef LOADER  
    nv_update(FALSE);			/* Update NV data */
#ifdef SIGNALCUBE
	process_zero();				/* Process background zero function	*/
#endif /* SIGNALCUBE */
#ifdef CONTROLCUBE
    hyd_update(HYD_UPDATE);		/* Update hydraulic status */
#endif /* CONTROLCUBE */
#endif /* LOADER */
    break;
  case 3:
	timeout_handler();			/* Update timeout counters for message handlers */
	hc_timer_support();
	event_testcode();
#ifdef CONTROLCUBE
#ifndef LOADER  
    hyd_update(HYD_UPDATE);		/* Update hydraulic status */
#endif
#endif
    break;
}

}
