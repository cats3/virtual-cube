/********************************************************************************
 * MODULE NAME       : mdnsserver.c												*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Server for MDNS requests.								*
 ********************************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
//#include <linux/inet_diag.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/ether.h>

#include "eeprom.h"

extern char sysname[32];				/* System name variable 			*/
extern volatile uint32_t prd_enable;		/* Initialisation completed			*/

unsigned char local_mac[6] = { 0x00, 0x02, 0x03, 0x04, 0x05, 0x06 };

/* Standard DNS message format */

typedef struct dns_msg {
	uint16_t ident;
	uint16_t flags;
	uint16_t num_q;
	uint16_t num_ansrr;
	uint16_t num_autrr;
	uint16_t num_addrr;
} dns_msg;

/* Definition of DNS based netconfig command structure */

typedef struct dns_netconfig {
	uint8_t mac[6];			/* Destination MAC address */
	uint8_t source[4];		/* Source for IP address
							  0 = Manual
							  1 = DHCP
							  2 = BOOTP (reserved)							*/
	uint8_t update[4];		/* Delay before update (0.1s increments)
							  0 = No update									*/
	uint8_t ipaddr[4];		/* IP address 										*/
	uint8_t subnetmask[4];	/* Subnetmask										*/
} dns_netconfig;

#define RR_TYPE_A			1			/* Type A - IP address				*/
#define RR_TYPE_TXT			16			/* Type TXT - Text					*/

#define RR_CLASS_INTERNET	1			/* Class 1 - Internet				*/

static const char ver[] = "version=1";	/* Prefix for record version number			*/
static const char name[] = "name=";		/* Prefix for controller name				*/
static const char mac[] = "mac=";		/* Prefix for MAC address					*/
static const char src[] = "src=";		/* Prefix for configured address source		*/
static const char addr[] = "addr=";		/* Prefix for configured IP address			*/
static const char mask[] = "mask=";		/* Prefix for configured subnet mask		*/
static const char opt[] = "opt=1";		/* Prefix for record option code			*/

static const char validate[] = "arihsnkomppagbsg";
/* String used to validate commands	*/
static const char cmd[] = "cmd=";		/* Command prefix					*/
static const char cmd_netconfig[] = "netconfig";
/* Netconfig command				*/

static uint8_t macaddr[6];					/* MAC address						*/
static uint32_t localipaddr;				/* IP address						*/

static struct ifreq ifr = { "ea0", {16, AF_INET, 0, 0, 0, 0, 0, 0} };

static struct in_addr mdns_addr = { 0xFB0000E0 };	/* 224.0.0.251  */
static struct in_addr if_addr = { INADDR_ANY };	/* 0.0.0.0		*/
static struct ifreq ipaddr = { "ea0", {16, AF_INET, 0, 0, 0, 0, 0, 0} };

static uint8_t mdns_buffer[1024];			/* Buffer used for mDNS query		*/
static uint8_t mdns_reply[1024];			/* Buffer used for mDNS reply		*/
static uint8_t mdns_name[256];				/* Query name buffer 				*/

static struct ip_mreq mreq;				/* Multicast address				*/
static struct sockaddr_in server_addr;	/* mDNS server address information	*/
static int skt;							/* Socket used for mdns connection 	*/

static int data_true = 1;


#define DUMPCHARS 16
static void dump_mdns_reply(uint8_t* reply, size_t len)
{
	uint8_t buf[DUMPCHARS];
	size_t c = 0;
	int i;

	printf("mdns: reply is %d bytes\n", len);

	while (len > 0)
	{
		// dump bytes as hex
		if (len > DUMPCHARS)
			c = DUMPCHARS;
		else
			c = len;
		memcpy(buf, reply, c);
		for (i = 0; i < DUMPCHARS; i++)
		{
			if (i < c)
				printf("%02X ", buf[i]);
			else
				printf("   ");
		}
		printf(" |  ");
		for (i = 0; i < c; i++)
		{
			if (isalnum(buf[i]))
				putchar(buf[i]);
			else
				putchar('.');
		}
		putchar('\n');
		len -= c;
		reply += c;
	}
}


/********************************************************************************
  FUNCTION NAME   	: ntohscpy
  FUNCTION DETAILS  : Copy a 2-byte variable to a defined destination allowing
					  for network byte ordering.
********************************************************************************/

static void ntohscpy(uint16_t* dst, uint8_t* src)
{
	uint16_t temp;

	memcpy(&temp, src, sizeof(uint16_t));
	*dst = ntohs(temp);
}



/********************************************************************************
  FUNCTION NAME   	: ntohlcpy
  FUNCTION DETAILS  : Copy a 4-byte variable to a defined destination allowing
					  for network byte ordering.
********************************************************************************/

static void ntohlcpy(uint32_t* dst, uint8_t* src)
{
	uint32_t temp;

	memcpy(&temp, src, sizeof(uint32_t));
	*dst = ntohl(temp);
}



/********************************************************************************
  FUNCTION NAME   	: htonlcpy
  FUNCTION DETAILS  : Copy a 4-byte variable to a defined destination allowing
					  for network byte ordering.
********************************************************************************/

static void htonlcpy(uint8_t* dst, uint32_t* src)
{
	uint32_t temp;

	temp = htonl(*src);
	memcpy(dst, &temp, sizeof(uint32_t));
}



/********************************************************************************
  FUNCTION NAME   	: copy_query_name
  FUNCTION DETAILS  : Copy DNS query name.

					  If destination is NULL, no data will be copied, but
					  the source pointer will be advanced by the correct amount
					  and the returned length will be correct.
********************************************************************************/

static int copy_query_name(uint8_t* dst, uint8_t** src, int maxlen)
{
	uint8_t* p;
	int len;
	int copied = 0;

	p = *src;
	while (maxlen > 0) {
		len = *p;
		if ((len + 1) <= maxlen) {
			if (dst) {
				memcpy(dst, p, (len + 1));
				dst += (len + 1);
			}
			maxlen -= (len + 1);
		}
		p += (len + 1);
		copied += (len + 1);
		if (!len) break;
	}
	*src = p;
	return(copied);
}


static int get_ipaddr(uint8_t *buffer)
{
	struct ifreq ifr;
	int sock;

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	ifr.ifr_addr.sa_family = AF_INET;

	strncpy(ifr.ifr_name, "eth0", IFNAMSIZ - 1);

	ioctl(sock, SIOCGIFADDR, &ifr);
	close(sock);

	struct sockaddr_in* ipaddr = (struct sockaddr_in*)&ifr.ifr_addr;
	//printf("IP address: %s\n", inet_ntoa(ipaddr->sin_addr));
	if (buffer)
		memcpy(buffer, &ipaddr->sin_addr, 4);

	return 0;
}

static int get_netmask(uint8_t* buffer)
{
	struct ifreq ifr;
	int sock;

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	ifr.ifr_addr.sa_family = AF_INET;

	strncpy(ifr.ifr_name, "eth0", IFNAMSIZ - 1);

	ioctl(sock, SIOCGIFNETMASK, &ifr);
	close(sock);

	struct sockaddr_in* ipaddr = (struct sockaddr_in*)&ifr.ifr_addr;
	//printf("IP netmask: %s\n", inet_ntoa(ipaddr->sin_addr));
	if (buffer)
		memcpy(buffer, &ipaddr->sin_addr, 4);

	return 0;
}

/********************************************************************************
  FUNCTION NAME   	: mdns_initialise
  FUNCTION DETAILS  : Initialise mDNS routine ready for operation.
********************************************************************************/

void mdns_initialise(void)
{
	struct ifreq ifr;
	struct ifconf ifc;
	char buffer[1024];
	int errno;
	int success = 0;

	/* Setup multicast address */

	memcpy((void*)&mreq.imr_multiaddr, &mdns_addr, sizeof(struct in_addr));
	memcpy((void*)&mreq.imr_interface, &if_addr, sizeof(struct in_addr));

	/* Setup server address */

	memset(&server_addr, 0, sizeof(struct sockaddr_in));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(5353);

	skt = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (skt < 0) {
		fprintf(stderr, "Error opening socket for mdns server\n");
	}


	ifc.ifc_len = sizeof(buffer);
	ifc.ifc_buf = buffer;
	if (ioctl(skt, SIOCGIFCONF, &ifc) < 0)
	{
		fprintf(stderr, "ioctl error: SIOCGIFCONF (%d)", errno);
	}

	struct ifreq* it = ifc.ifc_req;
	const struct ifreq* const end = it + (ifc.ifc_len / sizeof(struct ifreq));

	/* Get hardware MAC address (skipping loopback) for later use */
	for (; it != end; it++)
	{
		strcpy(ifr.ifr_name, it->ifr_name);
		if (ioctl(skt, SIOCGIFFLAGS, &ifr) == 0)
		{
			if (!(ifr.ifr_flags & IFF_LOOPBACK))
			{
				if (ioctl(skt, SIOCGIFHWADDR, &ifr) == 0) {
					success = 1;
					break;
				}
			}
		}
	}

	/* Copy MAC address into local copy for later use */
	if (success)
	{
		memcpy(macaddr, &ifr.ifr_hwaddr.sa_data[0], sizeof(macaddr));
	}

	if (ioctl(skt, SIOCGIFHWADDR, (int*)&ifr, NULL) < 0) {
		close(skt);
		fprintf(stderr, "Ioctl failed\n");
	}

	/* Join multicast group and select non-blocking mode */
	setsockopt(skt, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
	ioctl(skt, FIONBIO, &data_true, NULL);

	if (bind(skt, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0) {
		perror("mdns_initialise");
		close(skt);
	}

}



/********************************************************************************
  FUNCTION NAME   	: mdns_server
  FUNCTION DETAILS  : Server for mDNS requests.
********************************************************************************/

int mdns_server(void)
{
	struct sockaddr_in client_addr;
	int addrlen;
	int s;					/* Socket used for SIOCGIFADDR request	*/
	int msgsize;
	dns_msg* p;
	char* nptr;
	char* rptr;
	char* rstart;			/* Resource start						*/
	char* mstart;			/* Message start						*/
	int reply = 0;		/* Reply required flag					*/
	int num;				/* Counter for queries and answers		*/
	int qtype;				/* Query type							*/
	int atype;				/* Answer type							*/
	int rlen;				/* Resource length						*/
	int foo;
	//config_network config = { CONFIG_MANUAL, {0,0,0,0}, {255,255,255,0} };
	config_network config = { 0, {192,168,89,254}, {255,255,255,240} };

	uint8_t nlen;
	dns_msg* msg;
	dns_msg* rep;

	addrlen = sizeof(struct sockaddr_in);
	msgsize = recvfrom(skt, mdns_buffer, sizeof(mdns_buffer), 0, (struct sockaddr*)&client_addr, &addrlen);

	/* If there is a received message and the network configuration has been completed and the
	   system initialisation is complete (prd_enable == TRUE), then process the message.
	*/
	if (msgsize > 0) {
		//printf("mdns: recv %d bytes\n", msgsize);
//	if ((msgsize > 0) && (netconfig_complete()) && prd_enable) {

		// fpled_ctrl(LED_PULSE);

		/* DNS message received on mDNS port. Read current IP address ready for building reply */
		get_ipaddr((uint8_t*)&localipaddr);
		memcpy(&config.ipaddr, &localipaddr, 4);
		get_netmask((uint8_t*) &config.subnetmask);

		reply = 0;

		/* Process DNS message */

		p = (dns_msg*)mdns_buffer;
		msg = (dns_msg*)mdns_buffer;

		nptr = (char*)(p + 1);

		/* Check if this message has any questions. If not, then this might be a network
		   configuration message sent via multicast using DNS protocols, so check for
		   additional resource records containing netconfig commands.
		*/

		if (ntohs(msg->num_q) == 0) {

			/* Check for additional resource records containing netconfig commands */

			if (ntohs(msg->num_addrr)) {

				copy_query_name(NULL, (uint8_t**)&nptr, sizeof(mdns_name)); 	/* Skip domain name */
				nptr += 10;										/* Skip to additional RR data */

				/* Validate the record */

				if (memcmp(nptr + 1, validate, sizeof(validate)) == 0) {
					nptr += (1 + sizeof(validate));

					/* Check for a command prefix */

					if (memcmp(nptr, cmd, sizeof(cmd) - 1) == 0) {

						nptr += (sizeof(cmd) - 1);

						/* Check for a netconfig command */

						if (memcmp(nptr, cmd_netconfig, sizeof(cmd_netconfig)) == 0) {

							dns_netconfig* cmd = (dns_netconfig*)(nptr + sizeof(cmd_netconfig));
							//			uint8_t local_mac[6];
							uint32_t update;
							uint32_t source;
							int err;

							/* Process netconfig command */

							ntohlcpy(&update, cmd->update);
							ntohlcpy(&source, cmd->source);

							/* If the MAC address in the command matches our local MAC address
							   update our local network settings.
							*/

							//			dsp_1w_readpage(PAGE_MAC_ADDRESS, local_mac, 6, CHK_CHKSUM);
							if (memcmp(&cmd->mac, local_mac, sizeof(local_mac)) == 0) {

								// err = netconfig_setconfig(update, source, cmd->ipaddr, cmd->subnetmask);
							}
						}

					}

				}

			}

		}
		else {

			/* Message has questions, so assume it's a normal mDNS message. Scan the
			   questions and process as required.
			*/

			num = ntohs(msg->num_q);
			while (num) {
				nlen = copy_query_name(mdns_name, (uint8_t**)&nptr, sizeof(mdns_name)); /* Read query name */
				qtype = ((unsigned char)nptr[0] << 8) | (unsigned char)nptr[1];
				if (qtype == RR_TYPE_A) {
					if ((nlen == 13) && (strncmp((const char*)&mdns_name[1], "ControlCube", 11) == 0)) {
						reply = 1; /* Name match, so need to reply */
					}
				}
				nptr += 4; /* Skip query type and class */
				num--;
			}

		}

		/* Return immediately if no reply needs to be generated */

		if (!reply) return(0);

		/* Scan the answers in case our IP address has already been dealt with */
		num = ntohs(msg->num_ansrr);
		while (num) {
			nlen = copy_query_name(NULL, (uint8_t**)&nptr, sizeof(mdns_name)); /* Skip domain name */
			atype = ((unsigned char)nptr[0] << 8) | (unsigned char)nptr[1];
			rlen = ((unsigned char)nptr[8] << 8) | (unsigned char)nptr[9];	/* Get resource data length */
			if (atype == RR_TYPE_A) {
				int ipaddr;
				ipaddr = (((unsigned char)nptr[13] << 24) & 0xFF000000) |
					(((unsigned char)nptr[12] << 16) & 0xFF0000) |
					(((unsigned char)nptr[11] << 8) & 0xFF00) |
					(unsigned char)nptr[10] & 0xFF;
				if (ipaddr == localipaddr) reply = 0; /* Already processed so we can ignore */
			}
			nptr += (rlen + 10); /* Skip type, class, TTL and resource data */
			num--;
		}

		/* Now generate reply if required */

		if (reply) {
			rep = (dns_msg*)mdns_reply;

			rep->ident = msg->ident;
			rep->flags = htons(0x8000);
			rep->num_q = 0;
			rep->num_ansrr = htons(2);			/* 2 answers will be sent	*/
			rep->num_autrr = htons(0);
			rep->num_addrr = htons(0);

			mstart = rptr = (char*)(rep + 1);	/* Step over DNS header 	*/

			/* Insert an IP address record into the response */

			nlen = *mdns_name;					/* Get name length 			*/
			memcpy(rptr, mdns_name, nlen + 2);	/* Copy name string 		*/
			rptr += (nlen + 2);					/* Step over name string	*/

			*rptr++ = 0x00; *rptr++ = RR_TYPE_A;							/* Response type 			*/
			*rptr++ = 0x00; *rptr++ = RR_CLASS_INTERNET;					/* Response class			*/
			*rptr++ = 0x00; *rptr++ = 0x00; *rptr++ = 0x01; *rptr++ = 0x00;	/* Time to live				*/
			*rptr++ = 0x00; *rptr++ = 0x04;									/* Resource length			*/

			memcpy(rptr, &localipaddr, 4); rptr += 4;	/* Insert IP address */

			/* Insert a TXT record into the response containing record version number,
			   system name and MAC address
			*/

			nlen = *mdns_name;					/* Get name length 			*/
			memcpy(rptr, mdns_name, nlen + 2);	/* Copy name string 		*/
			rptr += (nlen + 2);					/* Step over name string	*/

			*rptr++ = 0x00; *rptr++ = RR_TYPE_TXT;								/* Response type 			*/
			*rptr++ = 0x00; *rptr++ = RR_CLASS_INTERNET;						/* Response class			*/
			*rptr++ = 0x00; *rptr++ = 0x00; *rptr++ = 0x01; *rptr++ = 0x00;		/* Time to live				*/
			rstart = rptr; rptr += 2;

			*rptr++ = sizeof(ver) - 1;
			memcpy(rptr, ver, (sizeof(ver) - 1)); rptr += (sizeof(ver) - 1);			/* Copy version prefix		*/

			*rptr++ = sizeof(opt) - 1;
			memcpy(rptr, opt, (sizeof(opt) - 1)); rptr += (sizeof(opt) - 1);			/* Copy option prefix		*/

			*rptr++ = sizeof(name) - 1 + strlen(sysname);
			memcpy(rptr, name, (sizeof(name) - 1)); rptr += (sizeof(name) - 1);		/* Copy name prefix			*/
			strcpy(rptr, sysname); rptr += strlen(sysname);						/* Copy name				*/

			*rptr++ = sizeof(mac) - 1 + 6;
			memcpy(rptr, mac, (sizeof(mac) - 1)); rptr += (sizeof(mac) - 1);			/* Copy mac prefix			*/
			memcpy(rptr, macaddr, sizeof(macaddr)); rptr += sizeof(macaddr);		/* Insert MAC address		*/

			*rptr++ = sizeof(src) - 1 + 4;
			memcpy(rptr, src, (sizeof(src) - 1)); rptr += (sizeof(src) - 1);			/* Copy src prefix			*/
			htonlcpy((uint8_t*)rptr, &config.source); rptr += sizeof(config.source);/* Insert configured source	*/

			*rptr++ = sizeof(addr) - 1 + 4;
			memcpy(rptr, addr, (sizeof(addr) - 1)); rptr += (sizeof(addr) - 1);		/* Copy addr prefix			*/
			memcpy(rptr, config.ipaddr, sizeof(config.ipaddr)); rptr += sizeof(config.ipaddr);
			/* Insert configured ipaddr	*/

			*rptr++ = sizeof(mask) - 1 + 4;
			memcpy(rptr, mask, (sizeof(mask) - 1)); rptr += (sizeof(mask) - 1);		/* Copy mask prefix			*/
			memcpy(rptr, config.subnetmask, sizeof(config.subnetmask)); rptr += sizeof(config.subnetmask);
			/* Insert configured mask	*/

			nlen = rptr - rstart - 2;										/* Calculate length			*/
			*rstart++ = 0x00; *rstart++ = nlen;								/* Resource length			*/

			*rptr++ = 0;	/* Zero length indicates end of TXT resource */

			//dump_mdns_reply(mdns_reply, 12 + (rptr - mstart));
			foo = sendto(skt, (const void*)mdns_reply, 12 + (rptr - mstart), 0, (struct sockaddr*)&client_addr, sizeof(struct sockaddr_in));
		}
	}

	return(0);
}

