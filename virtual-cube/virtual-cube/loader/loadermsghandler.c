/********************************************************************************
  MODULE NAME   	: loadermsghandler
  PROJECT			: Control Cube / Signal Cube
  MODULE DETAILS  	: Handler functions for loader messages.
********************************************************************************/

#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#define BUILD_DATA 1	/* Include build data */

#include "server.h"

#include "defines.h"

#include "hardware.h"
#include "msgserver.h"
#include "msgstruct.h"

#include "cnet.h"
#include "eeprom.h"


uint32_t testmode_address = 0;	/* Address for testmode memory access operations */
uint32_t tmp_data = 0;

/********************************************************************************
  FUNCTION NAME   	: fn_testmode_set_address
  FUNCTION DETAILS  : Handler for testmode_set_address message.
********************************************************************************/

int fn_testmode_set_address(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_set_address* m;

	*msglength = offsetof(struct msg_testmode_set_address, end);

	m = (struct msg_testmode_set_address*)msg;

	testmode_address = m->address;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_write8
  FUNCTION DETAILS  : Handler for testmode_write8 message.
********************************************************************************/

int fn_testmode_write8(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_write8* m;

	*msglength = offsetof(struct msg_testmode_write8, end);

	m = (struct msg_testmode_write8*)msg;

	//*((uint8_t*)testmode_address) = m->data;
	tmp_data = (uint32_t)m->data;
	testmode_address += 1;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_write16
  FUNCTION DETAILS  : Handler for testmode_write16 message.
********************************************************************************/

int fn_testmode_write16(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_write16* m;

	*msglength = offsetof(struct msg_testmode_write16, end);

	m = (struct msg_testmode_write16*)msg;

	//*((uint16_t*)testmode_address) = m->data;
	tmp_data = (uint32_t)m->data;
	testmode_address += 2;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_write32
  FUNCTION DETAILS  : Handler for testmode_write32 message.
********************************************************************************/

int fn_testmode_write32(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_write32* m;

	*msglength = offsetof(struct msg_testmode_write32, end);

	m = (struct msg_testmode_write32*)msg;

	//*((uint32_t*)testmode_address) = m->data;
	tmp_data = (uint32_t)m->data;
	testmode_address += 4;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_read8
  FUNCTION DETAILS  : Handler for testmode_read8 message.
********************************************************************************/

int fn_testmode_read8(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_return_read8* r;

	r = (struct msg_testmode_return_read8*)reply;

	*msglength = offsetof(struct msg_testmode_read8, end);
	*replylength = offsetof(struct msg_testmode_return_read8, end);

	r->command = MSG_TESTMODE_RETURN_READ8;
	//r->data = *((uint8_t*)testmode_address);
	r->data = (uint8_t)tmp_data;
	testmode_address += 1;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_read16
  FUNCTION DETAILS  : Handler for testmode_read16 message.
********************************************************************************/

int fn_testmode_read16(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_return_read16* r;

	r = (struct msg_testmode_return_read16*)reply;

	*msglength = offsetof(struct msg_testmode_read16, end);
	*replylength = offsetof(struct msg_testmode_return_read16, end);

	r->command = MSG_TESTMODE_RETURN_READ16;
	//r->data = *((uint16_t*)testmode_address);
	r->data = (uint16_t)tmp_data;
	testmode_address += 2;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_read32
  FUNCTION DETAILS  : Handler for testmode_read32 message.
********************************************************************************/

int fn_testmode_read32(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_return_read32* r;

	r = (struct msg_testmode_return_read32*)reply;

	*msglength = offsetof(struct msg_testmode_read32, end);
	*replylength = offsetof(struct msg_testmode_return_read32, end);

	r->command = MSG_TESTMODE_RETURN_READ32;
	//r->data = *((uint32_t*)testmode_address);
	r->data = tmp_data;
	testmode_address += 4;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_write32b
  FUNCTION DETAILS  : Handler for testmode_write32b message.
********************************************************************************/

int fn_testmode_write32b(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_write32b* m;

	*msglength = offsetof(struct msg_testmode_write32b, end);

	m = (struct msg_testmode_write32b*)msg;

	//hpi_write4(testmode_address, m->data);
	testmode_address += 4;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_read32b
  FUNCTION DETAILS  : Handler for testmode_read32b message.
********************************************************************************/

int fn_testmode_read32b(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_return_read32b* r;

	r = (struct msg_testmode_return_read32b*)reply;

	*msglength = offsetof(struct msg_testmode_read32b, end);
	*replylength = offsetof(struct msg_testmode_return_read32b, end);

	r->command = MSG_TESTMODE_RETURN_READ32B;
	//r->data = hpi_read4(testmode_address);
	r->data = 0;
	testmode_address += 4;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_i2c_start
  FUNCTION DETAILS  : Handler for testmode_i2c_start message.
********************************************************************************/

int fn_testmode_i2c_start(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	*msglength = offsetof(struct msg_testmode_i2c_start, end);

	//dsp_i2c_start();
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_i2c_restart
  FUNCTION DETAILS  : Handler for testmode_i2c_restart message.
********************************************************************************/

int fn_testmode_i2c_restart(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	*msglength = offsetof(struct msg_testmode_i2c_restart, end);

	//dsp_i2c_restart();
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_i2c_stop
  FUNCTION DETAILS  : Handler for testmode_i2c_stop message.
********************************************************************************/

int fn_testmode_i2c_stop(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	*msglength = offsetof(struct msg_testmode_i2c_stop, end);

	//dsp_i2c_stop();
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_i2c_send
  FUNCTION DETAILS  : Handler for testmode_i2c_send message.
********************************************************************************/

int fn_testmode_i2c_send(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_i2c_send* m;
	struct msg_testmode_return_i2c_send* r;

	*msglength = offsetof(struct msg_testmode_i2c_send, end);

	m = (struct msg_testmode_i2c_send*)msg;
	r = (struct msg_testmode_return_i2c_send*)reply;

	*msglength = offsetof(struct msg_testmode_i2c_send, end);
	*replylength = offsetof(struct msg_testmode_return_i2c_send, end);

	r->command = MSG_TESTMODE_RETURN_I2C_SEND;
	//r->ack = dsp_i2c_send(m->data);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_i2c_read
  FUNCTION DETAILS  : Handler for testmode_i2c_read message.
********************************************************************************/

int fn_testmode_i2c_read(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_i2c_read* m;
	struct msg_testmode_return_i2c_read* r;

	*msglength = offsetof(struct msg_testmode_i2c_read, end);

	m = (struct msg_testmode_i2c_read*)msg;
	r = (struct msg_testmode_return_i2c_read*)reply;

	*msglength = offsetof(struct msg_testmode_i2c_read, end);
	*replylength = offsetof(struct msg_testmode_return_i2c_read, end);

	r->command = MSG_TESTMODE_RETURN_I2C_READ;
	//r->data = dsp_i2c_read(m->ack);

	return(NO_ERROR);
}



#if (defined(CONTROLCUBE) || defined(AICUBE))

/********************************************************************************
  FUNCTION NAME   	: fn_testmode_dspb_i2c_start
  FUNCTION DETAILS  : Handler for testmode_dspb_i2c_start message.
********************************************************************************/

int fn_testmode_dspb_i2c_start(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	*msglength = offsetof(struct msg_testmode_dspb_i2c_start, end);

	//dspb_i2c_start();

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_dspb_i2c_restart
  FUNCTION DETAILS  : Handler for testmode_dspb_i2c_restart message.
********************************************************************************/

int fn_testmode_dspb_i2c_restart(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	*msglength = offsetof(struct msg_testmode_dspb_i2c_restart, end);

	//dspb_i2c_restart();

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_dspb_i2c_stop
  FUNCTION DETAILS  : Handler for testmode_dspb_i2c_stop message.
********************************************************************************/

int fn_testmode_dspb_i2c_stop(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	*msglength = offsetof(struct msg_testmode_dspb_i2c_stop, end);

	//dspb_i2c_stop();
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_dspb_i2c_send
  FUNCTION DETAILS  : Handler for testmode_dspb_i2c_send message.
********************************************************************************/

int fn_testmode_dspb_i2c_send(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_dspb_i2c_send* m;
	struct msg_testmode_return_dspb_i2c_send* r;

	*msglength = offsetof(struct msg_testmode_dspb_i2c_send, end);

	m = (struct msg_testmode_dspb_i2c_send*)msg;
	r = (struct msg_testmode_return_dspb_i2c_send*)reply;

	*msglength = offsetof(struct msg_testmode_dspb_i2c_send, end);
	*replylength = offsetof(struct msg_testmode_return_dspb_i2c_send, end);

	r->command = MSG_TESTMODE_RETURN_DSPB_I2C_SEND;
	//r->ack = dspb_i2c_send(m->data);
	r->ack = 0;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_dspb_i2c_read
  FUNCTION DETAILS  : Handler for testmode_dspb_i2c_read message.
********************************************************************************/

int fn_testmode_dspb_i2c_read(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_dspb_i2c_read* m;
	struct msg_testmode_return_dspb_i2c_read* r;

	*msglength = offsetof(struct msg_testmode_dspb_i2c_read, end);

	m = (struct msg_testmode_dspb_i2c_read*)msg;
	r = (struct msg_testmode_return_dspb_i2c_read*)reply;

	*msglength = offsetof(struct msg_testmode_dspb_i2c_read, end);
	*replylength = offsetof(struct msg_testmode_return_dspb_i2c_read, end);

	r->command = MSG_TESTMODE_RETURN_DSPB_I2C_READ;
	//r->data = dspb_i2c_read(m->ack);
	r->data = 0;

	return(NO_ERROR);
}



#endif

/********************************************************************************
  FUNCTION NAME   	: fn_testmode_onew_init
  FUNCTION DETAILS  : Handler for testmode_onew_init message.
********************************************************************************/

int fn_testmode_onew_init(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	//struct msg_testmode_onew_init* m;

	*msglength = offsetof(struct msg_testmode_onew_init, end);

	//m = (struct msg_testmode_onew_init*)msg;

	//hw_1w_init((uint8_t*)m->control);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_onew_reset
  FUNCTION DETAILS  : Handler for testmode_onew_reset message.
********************************************************************************/

int fn_testmode_onew_reset(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_onew_reset* m;
	struct msg_testmode_return_onew_reset* r;

	m = (struct msg_testmode_onew_reset*)msg;
	r = (struct msg_testmode_return_onew_reset*)reply;

	*msglength = offsetof(struct msg_testmode_onew_reset, end);
	*replylength = offsetof(struct msg_testmode_return_onew_reset, end);

	r->command = MSG_TESTMODE_RETURN_ONEW_RESET;
	//r->presence = hw_1w_reset((uint8_t*)m->control);
	r->presence = 0;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_onew_send
  FUNCTION DETAILS  : Handler for testmode_onew_send message.
********************************************************************************/

int fn_testmode_onew_send(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_onew_send* m;

	*msglength = offsetof(struct msg_testmode_onew_send, end);

	m = (struct msg_testmode_onew_send*)msg;

	*msglength = offsetof(struct msg_testmode_onew_send, end);

	//hw_1w_write((uint8_t*)m->control, m->data);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_onew_read
  FUNCTION DETAILS  : Handler for testmode_onew_read message.
********************************************************************************/

int fn_testmode_onew_read(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_testmode_onew_read* m;
	struct msg_testmode_return_onew_read* r;

	m = (struct msg_testmode_onew_read*)msg;
	r = (struct msg_testmode_return_onew_read*)reply;

	*msglength = offsetof(struct msg_testmode_onew_read, end);
	*replylength = offsetof(struct msg_testmode_return_onew_read, end);

	r->command = MSG_TESTMODE_RETURN_ONEW_READ;
	//r->data = hw_1w_read((uint8_t*)m->control);
	r->data = 0;

	return(NO_ERROR);
}



extern uint32_t loader_watchdog_enable;

/********************************************************************************
  FUNCTION NAME   	: fn_testmode_enable_watchdog
  FUNCTION DETAILS  : Handler for testmode_enable_watchdog message.
********************************************************************************/

int fn_testmode_enable_watchdog(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
#ifdef LOADER
	* msglength = offsetof(struct msg_testmode_enable_watchdog, end);
	loader_watchdog_enable = TRUE;

	return(NO_ERROR);
#else
	return(error_illegal_function(reply, replylength));
#endif
}



/********************************************************************************
  FUNCTION NAME   	: fn_testmode_disable_watchdog
  FUNCTION DETAILS  : Handler for testmode_disable_watchdog message.
********************************************************************************/

int fn_testmode_disable_watchdog(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
#ifdef LOADER
	* msglength = offsetof(struct msg_testmode_disable_watchdog, end);
	loader_watchdog_enable = FALSE;
	return(NO_ERROR);
#else
	return(error_illegal_function(reply, replylength));
#endif
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_firmware_version
  FUNCTION DETAILS  : Handler for read_firmware_version message.
********************************************************************************/

int fn_read_firmware_version(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_firmware_version* r;

	r = (struct msg_return_firmware_version*)reply;

	*msglength = offsetof(struct msg_read_firmware_version, end);
	*replylength = offsetof(struct msg_return_firmware_version, end);

	r->command = MSG_RETURN_FIRMWARE_VERSION;
	r->version = FWVERSION;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_firmware_information
  FUNCTION DETAILS  : Handler for read_firmware_information message.
********************************************************************************/

int fn_read_firmware_information(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_firmware_information* r;

	r = (struct msg_return_firmware_information*)reply;

	*msglength = offsetof(struct msg_read_firmware_information, end);
	*replylength = offsetof(struct msg_return_firmware_information, end);

	r->command = MSG_RETURN_FIRMWARE_INFORMATION;
	r->version = FWVERSION;
	strncpy(r->date, build_date, sizeof(r->date));
	strncpy(r->time, build_time, sizeof(r->time));
	strncpy(r->info, build_info, sizeof(r->info));
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_device_type
  FUNCTION DETAILS  : Handler for read_device_type message.
********************************************************************************/

int fn_read_device_type(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_device_type* r;

	r = (struct msg_return_device_type*)reply;

	*msglength = offsetof(struct msg_read_device_type, end);
	*replylength = offsetof(struct msg_return_device_type, end);

	r->command = MSG_RETURN_DEVICE_TYPE;
	r->device = DEVICE_TYPE;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_cnet_info
  FUNCTION DETAILS  : Handler for read_cnet_info message.
********************************************************************************/

int fn_read_cnet_info(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_cnet_info* r;

	r = (struct msg_return_cnet_info*)reply;

	*msglength = offsetof(struct msg_read_cnet_info, end);
	*replylength = offsetof(struct msg_return_cnet_info, end);

	r->command = MSG_RETURN_CNET_INFO;
	r->status = ((cnet_configured) ? 0x100 : 0) | (cnet_mode & 0xFF);
	r->ringsize = cnet_ringsize;
	r->local_addr = cnet_local_addr;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_erase_flash
  FUNCTION DETAILS  : Handler for erase_flash message.
********************************************************************************/

int fn_erase_flash(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	*msglength = offsetof(struct msg_erase_flash, end);
	//flash_erase(ERASE_NO_WAIT);
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_erase_status
  FUNCTION DETAILS  : Handler for read_erase_status message.
********************************************************************************/

int fn_read_erase_status(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_erase_status* r;

	r = (struct msg_return_erase_status*)reply;

	*msglength = offsetof(struct msg_read_erase_status, end);
	*replylength = offsetof(struct msg_return_erase_status, end);

	r->command = MSG_RETURN_ERASE_STATUS;
	//r->status = erase_status();
	r->status = 0;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_prog_flash
  FUNCTION DETAILS  : Handler for prog_flash message.
********************************************************************************/

int fn_prog_flash(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_prog_flash* m;
	struct msg_return_prog_status* r;

	m = (struct msg_prog_flash*)msg;
	r = (struct msg_return_prog_status*)reply;

	*msglength = offsetof(struct msg_prog_flash, data) + m->length;
	*replylength = offsetof(struct msg_return_prog_status, end);

	r->command = MSG_RETURN_PROG_STATUS;
	//r->status = flash_program_block(m->start, &m->data[0], m->length);
	r->status = 0;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_erase_flash_block
  FUNCTION DETAILS  : Handler for erase_flash_block message.
********************************************************************************/

int fn_erase_flash_block(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_erase_flash_block* m;
	uint32_t status;

	m = (struct msg_erase_flash_block*)msg;

	*msglength = offsetof(struct msg_erase_flash_block, end);

	//status = flash_erase_block(m->block, ERASE_NO_WAIT);
	status = 0;
	if (status == ERASE_ILLEGAL_BLOCK) return(error_illegal_parameter(reply, replylength));

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_ip_address
  FUNCTION DETAILS  : Handler for set_ip_address message.
********************************************************************************/

int fn_set_ip_address(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_ip_address* m;
	//config_network config;
	int err;

	*msglength = offsetof(struct msg_set_ip_address, end);

	m = (struct msg_set_ip_address*)msg;

	//if (err = netconfig_setconfig(m->update, m->source, m->ipaddr, m->subnetmask)) {
	//	return(error_operation_failed(reply, replylength, err));
	//}

#if 0

	config.source = m->source;
	memcpy(&config.ipaddr, m->ipaddr, 4);
	memcpy(&config.subnetmask, m->subnetmask, 4);

	if (err = dsp_1w_writepage(PAGE_NETCONFIG, (uint8_t*)&config, sizeof(config), WR_CHKSUM)) {
		return(error_operation_failed(reply, replylength, err));
	}

	dsp_delay(100000);	/* Delay while one-wire write operation completes */

	if (m->update) {
		netconfig_restart(m->update);
	}

#endif

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_ip_address
  FUNCTION DETAILS  : Handler for read_ip_address message.
********************************************************************************/

int fn_read_ip_address(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_ip_address* r;
	config_network config;
	int err;

	r = (struct msg_return_ip_address*)reply;

	*msglength = offsetof(struct msg_read_ip_address, end);
	*replylength = offsetof(struct msg_return_ip_address, end);

	if (err = dsp_1w_readpage(PAGE_NETCONFIG, (uint8_t*)&config, sizeof(config), CHK_CHKSUM)) {
		return(error_operation_failed(reply, replylength, err));
	}

	r->command = MSG_RETURN_IP_ADDRESS;
	r->source = config.source;
	memcpy(r->ipaddr, &config.ipaddr, 4);
	memcpy(r->subnetmask, &config.subnetmask, 4);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_mac_address
  FUNCTION DETAILS  : Handler for read_mac_address message.
********************************************************************************/

int fn_read_mac_address(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_mac_address* r;

	r = (struct msg_return_mac_address*)reply;

	*msglength = offsetof(struct msg_read_mac_address, end);
	*replylength = offsetof(struct msg_return_mac_address, end);

	r->command = MSG_RETURN_MAC_ADDRESS;
	//read_mac_address(&r->macaddr);
	memset(&r->macaddr, 0, 6);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_hw_restart
  FUNCTION DETAILS  : Handler for hw_restart message.
********************************************************************************/

int fn_hw_restart(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_hw_restart* m;

	*msglength = offsetof(struct msg_hw_restart, end);

	//if (fw_validate()) {
	//	return(error_fw_corrupt(reply, replylength));
	//}

	m = (struct msg_hw_restart*)msg;

	hw_restart_delay = m->delay;
	hw_restart_enable = TRUE;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_fw_validate
  FUNCTION DETAILS  : Handler for fw_validate message.
********************************************************************************/

int fn_fw_validate(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_fw_validate* r;

	r = (struct msg_return_fw_validate*)reply;

	*msglength = offsetof(struct msg_fw_validate, end);
	*replylength = offsetof(struct msg_return_fw_validate, end);

	r->command = MSG_RETURN_FW_VALIDATE;
	//r->state = fw_validate();
	r->state = 0;

	return(NO_ERROR);
}
