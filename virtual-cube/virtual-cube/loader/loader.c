/********************************************************************************
  MODULE NAME   	: start
  PROJECT			: Control Cube
  MODULE DETAILS  	: Handles startup and initialisation of the Control Cube
					  loader system.
********************************************************************************/

#include <stdlib.h>
#include <stdio.h>
//#include <std.h>
//#include <swi.h>
//#include <clk.h>
//#include <tsk.h>

//#include "loadercfg.h"
//#include "csl_irq.h"
//#include "csl_cache.h"

//#include "../tcp_ip/support.h"
#include "defines.h"
#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "eeprom.h"


void system_initialise(void);
void system_flush(void);
int  tcp_ip(void);
int  telnet_server(void);
int  msg_server(void);
void dsp_io_init(void);
void init_local_io(void);
void init_hpi(void);

char sysname[32]; /* System name variable */

extern const unsigned char mainfpga[];		/* Mainboard FPGA bitstreams 	*/

uint32_t* pllcsr = (uint32_t*)0x01B7C100;
uint32_t* pllm = (uint32_t*)0x01B7C110;
uint32_t* plldiv0 = (uint32_t*)0x01B7C114;
uint32_t* plldiv1 = (uint32_t*)0x01B7C118;
uint32_t* plldiv2 = (uint32_t*)0x01B7C11C;
uint32_t* plldiv3 = (uint32_t*)0x01B7C120;
uint32_t* oscdiv1 = (uint32_t*)0x01B7C124;

uint32_t* emif_gctl = (uint32_t*)0x01800000;
uint32_t* emif_ce1 = (uint32_t*)0x01800004;
uint32_t* emif_ce0 = (uint32_t*)0x01800008;
uint32_t* emif_ce2 = (uint32_t*)0x01800010;
uint32_t* emif_ce3 = (uint32_t*)0x01800014;
uint32_t* emif_sdramctl = (uint32_t*)0x01800018;
uint32_t* emif_sdramtiming = (uint32_t*)0x0180001C;
uint32_t* emif_sdramext = (uint32_t*)0x01800020;
uint32_t* emif_ccfg = (uint32_t*)0x01840000;

/* Flag controls operation of general PRD functions */

volatile uint32_t prd_enable = FALSE;

/********************************************************************************
  FUNCTION NAME     : main
  FUNCTION DETAILS  : Entry point for Control Cube loader firmware.
********************************************************************************/

int main(void)
{

	/* Enable timer used for I2C bus timing */

	TIMER_configArgs(hTimer1, 0x2C0, 0xFFFFFFFF, 0);

	/* Initialise PLL and clock generation */

	*pllcsr = 0x0000;	/* disable PLL */

	/* Delay while PLL completes disable operation */

	dsp_delay(200);

	*pllcsr = 0x0008; 	/* reset PLL 				*/
	*pllm = 0x0005; 	/* set PLL Multiplier		*/

	*plldiv0 = 0x8000; 	/* set PLLDIV0 to 1			*/
	*oscdiv1 = 0x0000; 	/* disable CLKOUT3			*/
	*plldiv2 = 0x8001; 	/* set PLLDIV2 to 2			*/
	*plldiv1 = 0x8000; 	/* set PLLDIV1 to 1			*/
	*plldiv3 = 0x8004;  /* set PLLDIV3 to 5			*/

	/* Delay while PLL completes reset operation	*/

	dsp_delay(200);

	*pllcsr = 0x0000; 	/* release PLL reset		*/

	/* Delay while PLL completes lock operation	*/

	dsp_delay(200);

	*pllcsr = 0x0001; 	/* enable PLL				*/

	/* Reprogram EMIF */

	*emif_ce1 = 0x22A2CA01;		/* CE1-8bit async	*/
	*emif_ce2 = 0x22A2CA01;     /* CE2-8bit async 	*/
	*emif_ce3 = 0x22A2CA11;     /* CE3-16bit async  */
}



/********************************************************************************
  FUNCTION NAME     : background
  FUNCTION DETAILS  : Background task
********************************************************************************/

void background(void)
{
	volatile int ready;
	int button_delay = 0;

	fpled_ctrl(LED_ON);			/* Turn on front panel LED whilst initialising	*/
	dsp_reset_peripherals();	/* Reset peripheral devices including DSP B 	*/
	dsp_io_init();				/* Initialise DSP peripheral registers 			*/
	init_local_io();			/* Initialise local I/O operation				*/
	init_hpi();					/* Initialise host-port interface 				*/

	/* Configure mainboard FPGAs */

	config_mainboard_fpga((uint8_t*)mainfpga);

	/* Perform basic initialisation of DSP B */

	init_dspb();

	/* Initialise TCP/IP stack */

	tcp_ip();					/* Initialise TCP/IP stack 						*/
	system_initialise();		/* Initialise general system configuration 		*/
	cnet_initialise();			/* Initialise CNet hardware						*/

	/* Create server for telnet handling */

	ready = FALSE;
	//TSK_create((Fxn)telnet_server, NULL, &ready);	/* Create telnet server	*/
	task_create((Fxn)telnet_server, NULL, NULL, 1);
	while (!ready); /* Wait for completion */

	/* Delay here to allow telnet server task creation to complete */

	dsp_delay(100000);

	/* Create server for controller standard message handling */

	//TSK_create((Fxn)msg_server, NULL);		/* Create message server		*/
	task_create((Fxn)msg_server, NULL, NULL, 1);

	/* Delay here to allow message server task creation to complete */

	dsp_delay(100000);

	IRQ_enable(IRQ_EVT_EXTINT4); 	/* Enable CNet interrupt 		*/

	fpled_ctrl(LED_OFF);

	prd_enable = TRUE;

	delay_us(1000000);

	/* Check if INITIALISE button is already pressed (it may have been held down to force
	   entry into the loader). If so, wait for it to be released before checking its state
	   for IP address initialisation operation.

	   If the button is held down for more than 10 seconds, then the non-volatile memory
	   (EEPROM and battery-backed RAM) will be flushed.
	*/

	if (get_sys_state() & IP_INITIALISE) {
		// Wait for button release
		while (get_sys_state() & IP_INITIALISE) {
			delay_us(100000);
			if (button_delay++ < 100) {
				if (button_delay == 100) {
					system_flush();	/* Request non-volatile memory flush */
				}
			}
			else {
				fpled_ctrl((button_delay & 0x01) ? LED_ON : LED_OFF);
			}
		}
		delay_us(100000); // Allow for debounce
	}

	/* Background loop flashes diagnostic pattern on front panel LED and tests for
	   IP address initialisation request.
	*/

	for (;;) {
		uint32_t ctr;
		uint32_t start;

		for (ctr = 0; ctr < DIAG_LOADER; ctr++) {
			fpled_ctrl(LED_ON);
			start = TIMER_getCount(hTimer1);
			while ((TIMER_getCount(hTimer1) - start) < (250000 * 50)) {
				check_initialise();
			}
			fpled_ctrl(LED_OFF);
			start = TIMER_getCount(hTimer1);
			while ((TIMER_getCount(hTimer1) - start) < (250000 * 50)) {
				check_initialise();
			}
		}
		start = TIMER_getCount(hTimer1);
		while ((TIMER_getCount(hTimer1) - start) < (1000000 * 50)) {
			check_initialise();
		}
	}

}



/********************************************************************************
  FUNCTION NAME   	: system_initialise
  FUNCTION DETAILS  : General system initialisation.
********************************************************************************/

void system_initialise(void)
{
	char defaultname[32] = "ControlCube_";
	uint8_t macaddr[6];

	/* Set default controller name. This is initially set to the string "ControlCube"
	   followed by the final six hex nibbles of the MAC address. This will provide
	   a unique name if the user does not define one. The default name will be
	   overwritten by the name restored from the non-volatile memory, if valid.
	*/

	read_mac_address(macaddr);
	printmac(macaddr, defaultname + 12, 3, NOSEP | ZEROTERM);
	memcpy(sysname, defaultname, sizeof(sysname));

	/* Read controller name from 1-wire memory */

	dsp_1w_readpage(PAGE_CTRLNAME, (uint8_t*)sysname, 31, CHK_CHKSUM);
}



char* flush_request = "FlushNow";

/********************************************************************************
  FUNCTION NAME   	: system_initialise
  FUNCTION DETAILS  : General system initialisation.
********************************************************************************/

void system_flush(void)
{
	dsp_i2c_eeprom_writeblock(SYS_EEPROM, 0, (uint8_t*)flush_request, 8, FALSE);
}
