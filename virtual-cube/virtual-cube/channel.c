/********************************************************************************
 * MODULE NAME       : channel.c												*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : General channel processing routines						*
 ********************************************************************************/

#include <stddef.h>
#include <string.h>
#include <math.h>

#include <stdint.h>
#include "porting.h"

 //#include "headers.h"
#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"
#include "nonvol.h"
#include "controller.h"
#include "asmcode.h"

int totalinchan;					    /* Total number of input channels  					*/
int totaloutchan;					    /* Total number of output channels 					*/
int totalmiscchan;						/* Total number of miscellaneous channels 			*/
int totaldigiochan;						/* Total number of digital I/O channels 			*/
int totalvirtualchan;				    /* Total number of virtual channels  				*/
int physinchan;							/* Number of physical input channels  				*/

chandef inchaninfo[MAXINCHAN];			/* Input channel information array  				*/
chandef outchaninfo[MAXOUTCHAN];		/* Output channel information array 				*/
chandef miscchaninfo[MAXMISCCHAN];		/* Misc channel information array  					*/
chandef digiochaninfo[MAXDIGIOCHAN];	/* Digital I/O channel information array  			*/

float no_filter[] = { 0.0, 	/* Coefficient B2 */
					 0.0, 	/* Coefficient A2 */
					 0.0, 	/* Coefficient B3 */
					 0.0,	/* Coefficient A3 */
					 0.0,	/* Coefficient B4 */
					 0.0,	/* Coefficient A4 */
					 0.0,	/* Coefficient B1 */
					 0.0,	/* Coefficient A1 */
					 1.0 };	/* Coefficient B0 */

#define B2	0
#define A2	1
#define B3	2
#define A3	3
#define B4	4
#define A4	5
#define B1	6
#define A1	7
#define B0	8	

#define TWOPI		(3.141592653 * 2.0)

#define ROOTTWO		1.414213562

#define T			(1.0/4096.0)	/* Sample period */
#define T2			(T*T)
#define T3			(T*T*T)
#define T4			(T*T*T*T)

/* For the 4th order low and high pass filters */

#define C1 2.6131259297528
#define C2 3.4142135623731

/* Configure for different EEPROM access functions depending on hardware type */

#if (defined(CONTROLCUBE) || defined(AICUBE))
#define EEPROM_READBLOCK  dsp_i2c_eeprom_readblock
#define EEPROM_WRITEBLOCK dsp_i2c_eeprom_writeblock
#endif
#ifdef SIGNALCUBE
#define EEPROM_READBLOCK  sys_i2c_eeprom_readblock
#define EEPROM_WRITEBLOCK sys_i2c_eeprom_writeblock
#endif


/********************************************************************************
  FUNCTION NAME     : read_chan_status
  FUNCTION DETAILS  : Read the channel configuration status.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void read_chan_status(chandef* def, uint32_t* ctrl, uint32_t* status)
{
	read_shared_channel_parameter(def, offsetof(chandef, ctrl), (sizeof(uint32_t) * 2));
	if (ctrl) *ctrl = def->ctrl;
	if (status) *status = def->status;
}



/********************************************************************************
  FUNCTION NAME     : init_fntable
  FUNCTION DETAILS  : Initialise table of handler functions.

					  On entry:

					  fnlist	Points to start of list containing addresses
								of handler functions.

********************************************************************************/

void init_fntable(chandef* def, void* fnlist)
{
	uint32_t* src;
	uint32_t* dst;

	src = (uint32_t*)fnlist;
	dst = (uint32_t*)&def->readstatus;

	while (dst <= (uint32_t*)&def->readcalzeroentry) {
		*dst++ = *src++;
	}
}



/********************************************************************************
  FUNCTION NAME     : get_list_size
  FUNCTION DETAILS  : Determine the total amount of space required to hold the
					  data contained in the configuration data list.

					  list	  Points to the configuration data list

********************************************************************************/

uint32_t get_list_size(configsave* list)
{
	uint32_t size = 0;

	/* Calculate total length of all list elements */

	while (list->length) {
		size += list->length;
		list++;
	}

	return(size);
}



/********************************************************************************
  FUNCTION NAME     : init_savelist
  FUNCTION DETAILS  : Initialise the bankpos entries in a configsave list.

					  On entry:

					  list	  Points to the configuration data save list

********************************************************************************/

void init_savelist(configsave* list)
{
	uint32_t offset = sizeof(listhdr);

	while (list->length) {
		list->bankpos = offset;
		offset += list->length;
		list++;
	}
}



/********************************************************************************
  FUNCTION NAME     : save_chan_config
  FUNCTION DETAILS  : Save channel configuration data into EEPROM and
					  battery-backed RAM.

					  On entry:

					  def		  Points to the channel definition structure

********************************************************************************/

void save_chan_config(chandef* def)
{
	save_chan_eeconfig(def, def->eesavetable, def->eeformat);
	//save_chan_bbconfig(def, def->bbsavetable, def->bbformat);
}



/********************************************************************************
  FUNCTION NAME     : restore_chan_config
  FUNCTION DETAILS  : Restore channel configuration data from EEPROM and
					  battery-backed RAM and update channel definition.

					  On entry:

					  def		  Points to the channel definition structure

					  On exit:

					  Returns flags indicating if list was invalid or if
					  restored data was corrupt.

********************************************************************************/

int restore_chan_config(chandef* def)
{
	uint32_t src;
	int err;

	src = def->eecfg_start;
	err = restore_chan_eeconfig(def, &src, def->eesavetable, def->eeformat, DO_RESTORE);
	//err |= restore_chan_bbconfig(def, &src, def->bbsavetable, def->bbformat, DO_RESTORE);
	return(err);
}



/********************************************************************************
  FUNCTION NAME     : flush_chan_config
  FUNCTION DETAILS  : Flush channel configuration data in EEPROM and
					  battery-backed RAM.

					  On entry:

					  def		  Points to the channel definition structure

********************************************************************************/

void flush_chan_config(chandef* def)
{
	flush_chan_eeconfig(def, def->eesavelist, def->eeformat);
	//flush_chan_bbconfig(def, def->bbsavelist, def->bbformat);
}



/********************************************************************************
  FUNCTION NAME     : mirror_chan_eeconfig
  FUNCTION DETAILS  : Mirror a channel configuration data value into the current
					  bank area within the EEPROM.

					  On entry:

					  def		Points to the channel definition structure

					  list		Pointer to update list

					  entry		Entry within update list to be mirrored

********************************************************************************/

void mirror_chan_eeconfig(chandef* def, uint32_t entry)
{
	configsave* list;
	int len;
	int n;
	uint8_t tempdata[0x800];
	int tempchksum;
	uint8_t* src;
	int dst;
	int diff;
	int slot;

	list = def->eesavelist;

	len = list[entry].length;						/* Determine source data length	 */
	src = (uint8_t*)def + list[entry].offset;			/* Determine source data address */
	dst = def->eecfg_start + list[entry].bankpos;	/* Determine offset within bank	 */

	/* Get the slot number */

	slot = EXTSLOTNUM;

	/* Read the current EEPROM channel configuration checksum value into temporary area */

	EEPROM_READBLOCK((0xA0 | (slot << 1)), def->eecfg_start + offsetof(listhdr, chksum),
		(uint8_t*)&tempchksum, sizeof(int), 0);

	/* Read the current EEPROM channel configuration data into temporary area */

	EEPROM_READBLOCK((0xA0 | (slot << 1)), dst, (uint8_t*)&tempdata, len, 0);

	/* Copy the new data into the temporary area, updating the checksum during the copy */

	diff = 0;
	for (n = 0; n < len; n++) {
		diff = src[n] - tempdata[n];
		tempdata[n] = src[n];
		tempchksum += diff;
	}

	/* Transfer new checksum back to the EEPROM */

	EEPROM_WRITEBLOCK((0xA0 | (slot << 1)), def->eecfg_start + offsetof(listhdr, chksum),
		(uint8_t*)&tempchksum, sizeof(int), 0);

	/* Transfer the new data back to the EEPROM */

	EEPROM_WRITEBLOCK((0xA0 | (slot << 1)), dst, (uint8_t*)&tempdata, len, 0);
}



/********************************************************************************
  FUNCTION NAME     : restore_chan_eeconfig
  FUNCTION DETAILS  : Restore channel configuration data from EEPROM and update
					  channel definition.

					  On entry:

					  def		  Points to the channel definition structure

					  src		  Points to variable holding the offset in the
								  bank where data should be read

					  listtable   Points to table holding list of valid save
								  lists

					  format	  Current list format

					  On exit:

					  Returns flags indicating if list was invalid, old format,
					  or if restored data was corrupt.

					  If list was valid, then the source offset is updated
					  using the length of the list read from the EEPROM.

********************************************************************************/

int restore_chan_eeconfig(chandef* def, uint32_t* src, configsave** listtable,
	int format, int flags)
{
	listdata list;			/* Temporary list workspace */
	configsave* listptr;	/* Config save list pointer */
	int listtype;
	int len;
	int n;
	int chksum;
	int err = 0;
	int slot;

	/* Get the slot number */

	slot = EXTSLOTNUM;

	/* Read the list header */

	EEPROM_READBLOCK((0xA0 | (slot << 1)), *src, (uint8_t*)&list.hdr, sizeof(listhdr), 0);

	/* Check listinfo for validity */

	if (list.hdr.listinfo != ~list.hdr.listinfo_chk) return(FLAG_BADLIST);

	listtype = (list.hdr.listinfo >> 16) & 0xFF;

	/* Check for a valid list type */

	if (listtype > format) return(FLAG_BADLIST);

	/* Check for old format list */

	if (listtype != format) err = FLAG_OLDLIST;

	len = list.hdr.listinfo & 0xFFFF;	/* Extract length */

	/* Check for invalid data length and return error */

	if ((len < sizeof(listhdr)) || (len > (SAVELIST_SIZE + sizeof(listhdr)))) return(FLAG_BADLIST);

	/* Load remainder of data to local copy to allow checksum to be validated */

	EEPROM_READBLOCK((0xA0 | (slot << 1)), (*src) + sizeof(listhdr), (uint8_t*)&list.data,
		len - sizeof(listhdr), 0);

	/* Validate checksum on restored data */

	chksum = 0;
	for (n = 0; n < (len - sizeof(listhdr)); n++) chksum += (int)list.data[n];

	/* If checksum has passed, copy to final destination */

	listptr = listtable[listtype];	/* Point to configuration list 	 	 */
	(*src) += len;					/* Adjust source offset by list size */

	if (chksum == list.hdr.chksum && (flags & DO_RESTORE)) {
		len = 0;
		while (listptr->length) {
			memcpy((((uint8_t*)def) + listptr->offset), &list.data[len], listptr->length);
			len += listptr->length;
			listptr++;
		}
	}
	else {
		err |= FLAG_BADCFG;
	}

	return(err);
}



/********************************************************************************
  FUNCTION NAME     : save_chan_eeconfig
  FUNCTION DETAILS  : Save channel configuration data in EEPROM.

					  On entry:

					  def		  Points to the channel definition structure

					  listtable   Points to table holding list of valid save
								  lists

					  format	  Current list format

********************************************************************************/

void save_chan_eeconfig(chandef* def, configsave** listtable, int format)
{
	listdata list;			/* Temporary list workspace */
	configsave* listptr;	/* Config save list pointer */
	int len;
	int n;
	int chksum;
	int slot;

	/* Get the slot number */

	slot = EXTSLOTNUM;

	/* Build save data block in memory */

	len = 0;
	listptr = listtable[format];
	while (listptr->length) {
		memcpy((uint8_t*)&list.data[len], (((uint8_t*)def) + listptr->offset), listptr->length);
		len += listptr->length;
		listptr++;
	}

	/* Calculate checksum on stored data */

	chksum = 0;
	for (n = 0; n < len; n++) chksum += (int)list.data[n];

	/* Build list header */

	list.hdr.listinfo = ((format & 0xFF) << 16) | (len + sizeof(listhdr));
	list.hdr.listinfo_chk = ~list.hdr.listinfo;
	list.hdr.chksum = chksum;

	/* Write updated structure to EEPROM */

	EEPROM_WRITEBLOCK((0xA0 | (slot << 1)), def->eecfg_start, (uint8_t*)&list,
		(len + sizeof(listhdr)), 0);
}



/********************************************************************************
  FUNCTION NAME     : flush_chan_eeconfig
  FUNCTION DETAILS  : Flush the channel configuration data in EEPROM.

					  On entry:

					  def		  Points to the channel definition structure

					  savelist    Points to save list to be used

					  format	  Current list format

********************************************************************************/

void flush_chan_eeconfig(chandef* def, configsave* savelist, int format)
{
	listdata list;	/* Temporary list workspace */
	int len;
	int n;
	int slot;

	/* Get the slot number */

	slot = EXTSLOTNUM;

	len = get_list_size(savelist);

	/* Set all saved data bytes to zero */

	for (n = 0; n < SAVELIST_SIZE; n++) {
		list.data[n] = 0;
	}

	/* Build list header */

	list.hdr.listinfo = ((format & 0xFF) << 16) | (len + sizeof(listhdr));
	list.hdr.listinfo_chk = ~list.hdr.listinfo;
	list.hdr.chksum = 0xFFFFFFFF;

	/* Write updated structure to EEPROM */

	EEPROM_WRITEBLOCK((0xA0 | (slot << 1)), def->eecfg_start, (uint8_t*)&list,
		(len + sizeof(listhdr)), 0);
}



/********************************************************************************
  FUNCTION NAME     : save_chan_calibration
  FUNCTION DETAILS  : Save the channel calibration data in non-volatile
					  memory.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void save_chan_calibration(chandef* def)
{
	int chksum;
	int* p;
	int len;
	int n;
	int slot;

	/* Get the slot number */

	slot = EXTSLOTNUM;

	/* Calculate checksum on stored data */

	len = (SAVED_CHANCAL_SIZE / sizeof(uint32_t)) - 1;
	p = (int*)&def->cal;
	chksum = 0;
	for (n = 0; n < len; n++) {
		chksum += *p++;
	}
	def->cal.checksum = ~chksum;

	EEPROM_WRITEBLOCK((0xA0 | (slot << 1)), def->eecal_start, (uint8_t*)&def->cal,
		SAVED_CHANCAL_SIZE, 0);
}



/********************************************************************************
  FUNCTION NAME     : restore_chan_calibration
  FUNCTION DETAILS  : Restore the channel calibration data from non-volatile
					  memory.

					  On entry:

					  def		Points to the channel definition structure

					  On exit:

					  Returns TRUE if restored data was corrupt.

********************************************************************************/

int restore_chan_calibration(chandef* def)
{
	struct calinfo temp;
	int chksum;
	int* p;
	int* dst;
	int len;
	int n;
	int time;
	int slot;

	/* Get the slot number */

	slot = EXTSLOTNUM;

	/* Load to local copy to allow checksum to be validated */

	//time = TIMER_getCount(hTimer1);
	EEPROM_READBLOCK((0xA0 | (slot << 1)), def->eecal_start, (uint8_t*)&temp,
		SAVED_CHANCAL_SIZE, 0);
	//LOG_printf(&trace, "Install restore_chan_calibration %d", TIMER_getCount(hTimer1) - time);

	/* Validate checksum on restored data */

	len = (SAVED_CHANCAL_SIZE / sizeof(uint32_t)) - 1;
	p = (int*)&temp;
	chksum = 0;
	for (n = 0; n < len; n++) {
		chksum += *p++;
	}
	chksum = ~chksum;

	/* If checksum has passed, copy to final destination */

	if (chksum == temp.checksum) {
		p = (int*)&temp;
		dst = (int*)&def->cal;
		for (n = 0; n < len; n++) {
			*dst++ = *p++;
		}
	}
	//LOG_printf(&trace, "Install restore_chan_calibration %d", TIMER_getCount(hTimer1) - time);

	return((chksum == temp.checksum) ? 0 : FLAG_BADCAL);
}



/********************************************************************************
  FUNCTION NAME     : flush_chan_calibration
  FUNCTION DETAILS  : Flush the channel calibration data in non-volatile
					  memory.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void flush_chan_calibration(chandef* def)
{
	uint8_t temp[SAVED_CHANCAL_SIZE];
	int n;
	int slot;

	/* Get the slot number */

	slot = EXTSLOTNUM;

	for (n = 0; n < SAVED_CHANCAL_SIZE; n++) {
		temp[n] = 0;
	}
	EEPROM_WRITEBLOCK((0xA0 | (slot << 1)), def->eecal_start, temp,
		SAVED_CHANCAL_SIZE, 0);
}



/*******************************************************************************
 FUNCTION NAME	   : set_filter
 FUNCTION DETAILS  : Calculate the coefficients for a digital filter.

					 On entry:

					 Type is the filter type

						Bit  31		Filter on/off
						Bits 0,1	Filter type

									0 = Low-pass
									1 = Notch
									2 = High-pass
									3 = Bandpass

					 Order

					 Filter order 1,2,3,4 (2,4 only allowed for notch filter)

					 Coefficients are held in memory in the following order:

					 Offset 0  b(2)
					 Offset 1  a(2)
					 Offset 2  b(3)
					 Offset 3  a(3)
					 Offset 4  b(4)
					 Offset 5  a(4)
					 Offset 6  b(1)
					 Offset 7  a(1)
					 Offset 8  b(0)
					 Offset 9  unused

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void set_filter(chandef* def, uint32_t flags, int filter_no, int enable, int type, int order, float freq,
	float bandwidth, int mirror)
{
	double cf1, cf2, bw, temp, w0, w02, w03, w04;
	int i;
	double a[5], b[5];
	filter* f;

	f = (filter_no) ? &def->filter1 : &def->filter1;

	if (flags == FILTER_FREQ) {

		/* Only updating filter frequency, so read enable, type and order from current configuration */

		enable = f->type & FILTER_ENABLE;
		type = f->type & ~FILTER_ENABLE;
		order = f->order;

	}

	/* Set the coefficients for no filter initially */

	memcpy((void*)f, no_filter, sizeof(no_filter));

	/* Initialise all coefficients to the no-filter state */

	for (i = 0; i < 5; i++) {
		a[i] = b[i] = 0.0;
	}
	b[0] = 1.0;

	/* Check if filter enabled and calculate required coefficients */

	if (enable) {

		type = type & FILTER_TYPEMASK;

		/* Check type and set frequency and bandwidth */

		if (type == FILTER_LOWPASS || type == FILTER_HIGHPASS) {
			w0 = freq * TWOPI;
		}
		else {
			cf1 = (freq - (bandwidth / 2)) * TWOPI;
			cf2 = (freq + (bandwidth / 2)) * TWOPI;
			bw = fabs(cf2 - cf1);
			w0 = sqrt(cf1 * cf2);
		}

		w02 = w0 * w0;
		w03 = w0 * w02;
		w04 = w0 * w03;

		/* Now calculate the filter coefficients */

		switch (order) {

		case 1:

			/* First Order Filters */

			switch (type) {
			case FILTER_LOWPASS:
				temp = 2.0 + w0 * T;
				b[0] = w0 * T / temp;
				a[1] = 2.0 * b[0];
				b[1] = 1.0;
				break;

			case FILTER_HIGHPASS:
				temp = 2.0 + w0 * T;
				b[0] = 2.0 / temp;
				a[1] = 2.0 * w0 * T / temp;
				break;

			case FILTER_BANDPASS:
			case FILTER_NOTCH:

				/* Filter type not supported */

				return;
				//          		break;

			default:
				break;
			}
			break;

		case 2:

			/* Second Order Filters */

			switch (type) {
			case FILTER_LOWPASS:
				b[0] = T2 * w02 / (4.0 + 2.0 * T * ROOTTWO * w0 + T2 * w02);
				b[1] = T * w0 / (ROOTTWO + T * w0);
				b[2] = 1.0;
				a[1] = 4.0 * T * (ROOTTWO * w0 + T * w02) / (4.0 + 2.0 * ROOTTWO * T * w0 + T2 * w02);
				a[2] = T * w0 / (ROOTTWO + T * w0);
				break;

			case FILTER_HIGHPASS:
				b[0] = 4.0 / (4.0 + 2.0 * T * ROOTTWO * w0 + T2 * w02);
				b[1] = 2.0 / (ROOTTWO * w0 + T * w02);
				a[1] = 4.0 * T * (ROOTTWO * w0 + T * w02) / (4.0 + 2.0 * ROOTTWO * T * w0 + T2 * w02);
				a[2] = T * w0 / (ROOTTWO + T * w0);
				break;

			case FILTER_BANDPASS:
				b[0] = 2.0 * T * bw / (4.0 + 2.0 * T * bw * w0 + T2 * w02);
				b[1] = bw / (bw + T * w02);
				a[1] = 4.0 * T * (bw + T * w02) / (4.0 + 2.0 * T * bw + T2 * w02);
				a[2] = T * w02 / (bw * w0 + T * w02);
				break;

			case FILTER_NOTCH:
				b[0] = (4.0 + T2 * w02) / (4.0 + 2.0 * T * bw * w0 + T2 * w02);
				b[1] = T * w0 / (bw + T * w0);
				b[2] = 1.0;
				a[1] = 4.0 * T * (bw * w0 + T * w02) / (4.0 + 2.0 * T * bw * w0 + T2 * w02);
				a[2] = T * w0 / (bw + T * w0);
				break;

			default:
				break;
			}
			break;

		case 3:

			/* Third Order Filters */

			switch (type) {
			case FILTER_LOWPASS:
				b[0] = w03 * T3 / (8 + 8 * w0 * T + 4 * w02 * T2 + w03 * T3);
				b[1] = 3 * w03 * T2 / (8 * w0 + 8 * w02 * T + 3 * w03 * T2);
				b[2] = 3 * w03 * T / (4 * w02 + 3 * w03 * T);
				b[3] = 1.0;
				a[1] = (16 * w0 * T + 16 * w02 * T2 + 6 * w03 * T3) / (8 + 8 * w0 * T + 4 * w02 * T2 + w03 * T3);
				a[2] = (8 * w0 * T + 6 * w02 * T2) / (8 + 8 * w0 * T + 3 * w02 * T2);
				a[3] = 2 * w0 * T / (4 + 3 * w0 * T);
				break;

			case FILTER_HIGHPASS:
				b[0] = 8.0 / (8 + 8 * w0 * T + 4 * w02 * T2 + w03 * T3);
				a[1] = (16 * w0 * T + 16 * w02 * T2 + 6 * w03 * T3) / (8 + 8 * w0 * T + 4 * w02 * T2 + w03 * T3);
				a[2] = (8 * w0 * T + 6 * w02 * T2) / (8 + 8 * w0 * T + 3 * w02 * T2);
				a[3] = 2 * w0 * T / (4 + 3 * w0 * T);
				break;

			case FILTER_BANDPASS:
			case FILTER_NOTCH:

				/* Filter type not supported */

				return;
				//          		break;

			default:
				break;
			}
			break;

		case 4:

			/* Fourth Order Filters */
			switch (type) {
			case FILTER_LOWPASS:
				b[0] = w04 * T4 / (16 + 8 * C1 * w0 * T + 4 * C2 * w02 * T2 + 2 * C1 * w03 * T3 + w04 * T4);
				b[1] = 2 * w03 * T3 / (4 * C1 + 4 * C2 * w0 * T + 3 * C1 * w02 * T2 + 2 * T3 * w03);
				b[2] = 3 * w02 * T2 / (2 * C2 + 3 * C1 * w0 * T + 3 * w02 * T2);
				b[3] = 2 * w0 * T / (C1 + 2 * w0 * T);
				b[4] = 1.0;
				a[1] = (16 * C1 * w0 * T + 16 * C2 * w02 * T2 + 12 * C1 * w03 * T3 + 8 * w04 * T4)
					/ (16 + 8 * C1 * w0 * T + 4 * C2 * w02 * T2 + 2 * C1 * w03 * T3 + w04 * T4);
				a[2] = (4 * C2 * w0 * T + 6 * C1 * w02 * T2 + 6 * w03 * T3)
					/ (4 * C1 + 4 * C2 * w0 * T + 3 * C1 * w02 * T2 + 2 * w03 * T3);
				a[3] = (2 * C1 * w0 * T + 4 * w02 * T2) / (2 * C2 + 3 * C1 * w0 * T + 3 * w02 * T2);
				a[4] = w0 * T / (C1 + 2 * w0 * T);
				break;

			case FILTER_HIGHPASS:
				b[0] = 16.0 / (16 + 8 * C1 * w0 * T + 4 * C2 * w02 * T2 + 2 * C1 * w03 * T3 + w04 * T4);
				a[1] = (16 * C1 * w0 * T + 16 * C2 * w02 * T2 + 12 * C1 * w03 * T3 + 8 * w04 * T4)
					/ (16 + 8 * C1 * w0 * T + 4 * C2 * w02 * T2 + 2 * C1 * w03 * T3 + w04 * T4);
				a[2] = (4 * C2 * w0 * T + 6 * C1 * w02 * T2 + 6 * w03 * T3)
					/ (4 * C1 + 4 * C2 * w0 * T + 3 * C1 * w02 * T2 + 2 * w03 * T3);
				a[3] = (2 * C1 * w0 * T + 4 * w02 * T2) / (2 * C2 + 3 * C1 * w0 * T + 3 * w02 * T2);
				a[4] = w0 * T / (C1 + 2 * w0 * T);
				break;

			case FILTER_BANDPASS:
				b[0] = 4.0 * bw * bw * T2 / (16.0 + 8.0 * sqrt(2) * bw * T + 4.0 * (2.0 * w02 + bw * bw) * T2 + 2.0 * sqrt(2) * bw * w02 * T3 + w04 * T4);
				b[1] = 4.0 * bw * bw * T / (4.0 * sqrt(2) * bw + 4.0 * (2.0 * w02 + bw * bw) * T + 3.0 * sqrt(2) * bw * w02 * T2 + 2.0 * w04 * T3);
				b[2] = 2.0 * bw * bw / (2.0 * (2.0 * w02 + bw * bw) + 3.0 * sqrt(2) * bw * w02 * T + 3.0 * w04 * T2);
				a[1] = (16.0 * sqrt(2) * bw * T + 16.0 * (2.0 * w02 + bw * bw) * T2 + 12.0 * sqrt(2) * bw * w02 * T3 + 8.0 * w04 * T4)
					/ (16.0 + 8.0 * sqrt(2) * bw * T + 4.0 * (2.0 * w02 + bw * bw) * T2 + 2.0 * sqrt(2) * bw * w02 * T3 + w04 * T4);
				a[2] = (4.0 * (2.0 * w02 + bw * bw) * T + 6.0 * sqrt(2) * bw * w02 * T2 + 6.0 * w04 * T3)
					/ (4.0 * sqrt(2) * bw + 4.0 * (2.0 * w02 + bw * bw) * T + 3.0 * sqrt(2) * bw * w02 * T2 + 2.0 * w04 * T3);
				a[3] = (2.0 * sqrt(2) * bw * w02 * T + 4.0 * w04 * T2) / (2.0 * (2.0 * w02 + bw * bw) + 3.0 * sqrt(2) * bw * w02 * T + 3.0 * w04 * T2);
				a[4] = w02 * T / (sqrt(2) * bw + 2.0 * w02 * T);
				break;

			case FILTER_NOTCH:
				b[0] = (16.0 + 8.0 * w02 * T2 + w04 * T4) / (16.0 + 8.0 * ROOTTWO * bw * T + 4.0 * (bw * bw + 2.0 * w02) * T2 + 2.0 * ROOTTWO * bw * w02 * T3 + w04 * T4);
				b[1] = (8.0 * w02 * T + 2.0 * w04 * T3) / (4.0 * ROOTTWO * bw + 4.0 * (bw * bw + 2.0 * w02) * T + 3.0 * ROOTTWO * bw * w02 * T2 + 2.0 * w04 * T3);
				b[2] = (4.0 * w02 + 3.0 * w04 * T2) / (2.0 * (bw * bw + 2.0 * w02) + 3.0 * ROOTTWO * bw * w02 * T + 3.0 * w04 * T2);
				b[3] = 2.0 * w04 * T / (ROOTTWO * bw * w02 + 2 * w04 * T);
				b[4] = 1.0;
				a[1] = (16.0 * ROOTTWO * bw * T + 16.0 * (bw * bw + 2.0 * w02) * T2 + 12.0 * ROOTTWO * bw * w02 * T3 + 8.0 * w04 * T4)
					/ (16.0 + 8.0 * ROOTTWO * bw * T + 4.0 * (bw * bw + 2.0 * w02) * T2 + 2.0 * ROOTTWO * bw * w02 * T3 + w04 * T4);
				a[2] = (4.0 * (bw * bw + 2.0 * w02) * T + 6.0 * ROOTTWO * bw * w02 * T2 + 6.0 * w04 * T3)
					/ (4.0 * ROOTTWO * bw + 4.0 * (bw * bw + 2.0 * w02) * T + 3.0 * ROOTTWO * bw * w02 * T2 + 2.0 * w04 * T3);
				a[3] = (2.0 * ROOTTWO * bw * w02 * T + 4.0 * w04 * T2) / (2.0 * (bw * bw + 2.0 * w02) + 3.0 * ROOTTWO * bw * w02 * T + 3.0 * w04 * T2);
				a[4] = w02 * T / (ROOTTWO * bw + 2 * w02 * T);
				break;

			default:
				break;
			}
			break;
		}

	}

	/* Copy our local coefficients to the channel definition */

	f->coeff[A1] = a[1];
	f->coeff[A2] = a[2];
	f->coeff[A3] = a[3];
	f->coeff[A4] = a[4];

	f->coeff[B0] = b[0];
	f->coeff[B1] = b[1];
	f->coeff[B2] = b[2];
	f->coeff[B3] = b[3];
	f->coeff[B4] = b[4];

	/* Copy the filter parameters to the channel definition */

	f->type = ((enable) ? FILTER_ENABLE : 0) | type;
	f->order = order;
	f->freq = freq;
	f->bandwidth = bandwidth;

	if (mirror)
		mirror_chan_eeconfig(def, 3);	/* Mirror chandef filter1 structure  */

	update_shared_channel_parameter(def, offsetof(chandef, filter1.coeff), sizeof(float) * 10);

	/* Reset any unused delta values */

	f->delta[0] = 0.0;
	f->delta[1] = 0.0;
	f->delta[2] = 0.0;
	f->delta[3] = 0.0;

	if (!(type & FILTER_ENABLE)) // No filter
		update_shared_channel_parameter(def, offsetof(chandef, filter1.delta), sizeof(float) * 4);
	else if (4 - order)
		update_shared_channel_parameter(def, offsetof(chandef, filter1.delta) + ((order - 1) * sizeof(float)),
			sizeof(float) * (4 - order));
	return;
}



/*******************************************************************************
 FUNCTION NAME	   : read_filter
 FUNCTION DETAILS  : Read the current filter characteristics.
********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void read_filter(chandef* def, int filter_no, int* enable, int* type, int* order, float* freq,
	float* bandwidth)
{
	filter* f;

	f = (filter_no) ? &def->filter1 : &def->filter1;

	/* Copy the filter parameters from the channel definition */

	if (enable) *enable = (f->type & FILTER_ENABLE) ? 1 : 0;
	if (type) *type = f->type & FILTER_TYPEMASK;
	if (order) *order = f->order;
	if (freq) *freq = f->freq;
	if (bandwidth) *bandwidth = f->bandwidth;
}



/*******************************************************************************
 FUNCTION NAME	   : set_hp_filter
 FUNCTION DETAILS  : Calculate the coefficients for the digital HP filter used
					 to implement the AC coupled option on Signal Cube
					 accelerometer cards.
********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_set_hp_filter(chandef* def, int enable, float freq, int mirror)
{
#ifdef SIGNALCUBE

	double temp, w0;
	double a[2], b[2];

	/* Initialise coefficients to the no-filter state */

	a[1] = 0.0;
	b[0] = 1.0;

	/* Check if filter enabled and calculate required coefficients */

	if (enable) {

		/* Set frequency */

		w0 = freq * TWOPI;

		/* Now calculate the filter coefficients */

		temp = 2.0 + w0 * T;
		b[0] = 2.0 / temp;
		a[1] = 2.0 * w0 * T / temp;

	}

	/* Copy our local oefficients to the channel definition */

	def->hp_coeff_a1 = a[1];
	def->hp_coeff_b0 = b[0];

	/* Copy the filter parameters to the channel definition */

	def->cfg.i_acc.flags = def->ctrl = (def->ctrl & ~FLAG_ACCOUPLED) | ((enable) ? FLAG_ACCOUPLED : 0);
	def->cfg.i_acc.hp_freq = freq;

	if (mirror)
		mirror_chan_eeconfig(def, 5);	/* Mirror chandef cfg structure  */

	  /* Reset delta value */

	def->hp_delta_d0 = 0.0;

	/* Update the shared channel definition */

	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(uint32_t));
	update_shared_channel_parameter(def, offsetof(chandef, hp_coeff_b0), sizeof(float) * 3);

#endif

	return;
}



/*******************************************************************************
 FUNCTION NAME	   : read_hp_filter
 FUNCTION DETAILS  : Read the characteristics of the digital HP filter used to
					 implement the AC coupled option on Signal Cube accelerometer
					 cards..
********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_read_hp_filter(chandef* def, int* enable, float* freq)
{

#ifdef SIGNALCUBE

	/* Copy the filter parameters from the channel definition */

	if (enable) *enable = (def->ctrl & FLAG_ACCOUPLED) ? 1 : 0;
	if (freq) *freq = def->cfg.i_acc.hp_freq;

#else

	if (enable) *enable = FALSE;
	if (freq) *freq = 0.0F;

#endif
}



/*******************************************************************************
 FUNCTION NAME	   : chan_set_cnet_output
 FUNCTION DETAILS  : Sets the CNet mapping for input channels.

					 On entry:

					 def	Pointer to channel being mapped.

					 slot	Destination CNet output slot.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_set_cnet_output(chandef* def, int slot)
{
#ifdef SIGNALCUBE
	chandef* base;
	int* slotptr;

	base = (chandef*)get_channel_base(def);
	slotptr = cnet_get_slotaddr(slot, PROC_DSPA);
	base->outputptr = slotptr;
#endif

#if (defined(CONTROLCUBE) || defined(AICUBE))
	def->outputptr = cnet_get_slotaddr_out(slot, PROC_DSPB);
	//update_shared_channel(def);
	update_shared_channel_parameter(def, offsetof(chandef, outputptr), sizeof(float*));
#endif
}



/*******************************************************************************
 FUNCTION NAME	   : chan_set_cnet_input
 FUNCTION DETAILS  : Sets the CNet mapping for output channels.

					 On entry:

					 def	Pointer to channel being mapped.

					 slot	Source CNet input slot.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_set_cnet_input(chandef* def, int slot)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	def->sourceptr = cnet_get_slotaddr(slot, PROC_DSPB);
	//update_shared_channel(def);
	update_shared_channel_parameter(def, offsetof(chandef, sourceptr), sizeof(float*));
#endif
}



/*******************************************************************************
 FUNCTION NAME	   : chan_set_source
 FUNCTION DETAILS  : Sets the source for output channels.

					 On entry:

					 def	Pointer to channel.

					 ptr	Address of data source.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_set_source(chandef* def, float* ptr)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	def->sourceptr = (int*)ptr;
	//update_shared_channel(def);
	update_shared_channel_parameter(def, offsetof(chandef, sourceptr), sizeof(float*));
#endif
}



/********************************************************************************
  FUNCTION NAME     : chan_set_pointer
  FUNCTION DETAILS  : Set the output pointer for the selected input channel.

					  On entry:

					  def		Points to the channel definition structure
					  ptr		Address to set output pointer

********************************************************************************/

void chan_set_pointer(chandef* def, int* ptr)
{
#ifdef SIGNALCUBE
	def = (chandef*)get_channel_base(def);
#endif

	def->outputptr = ptr;

#if (defined(CONTROLCUBE) || defined(AICUBE))
	update_shared_channel_parameter(def, offsetof(chandef, outputptr), sizeof(float*));
#endif
}



/********************************************************************************
  FUNCTION NAME     : chan_get_pointer
  FUNCTION DETAILS  : Get the output pointer for the selected input channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

int* chan_get_pointer(chandef* def)
{
#ifdef SIGNALCUBE
	def = (chandef*)get_channel_base(def);
#endif

	return(def->outputptr);
}



/********************************************************************************
  FUNCTION NAME     : chan_read_adc
  FUNCTION DETAILS  : Read the current filtered ADC result for the selected
					  channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

float chan_read_adc(chandef* def)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	uint32_t* ip;
	float value;

	ip = (uint32_t*)get_shared_channel_parameter_address(def, offsetof(chandef, filtered));
	*((uint32_t*)&value) = hpi_read4(ip);
	return(value);
#endif

#ifdef SIGNALCUBE
	return(((chandef*)get_channel_base(def))->filtered);
#endif
}



/********************************************************************************
  FUNCTION NAME     : chan_read_unfiltered
  FUNCTION DETAILS  : Read the current unfiltered ADC result for the selected
					  channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

float chan_read_unfiltered(chandef* def)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	uint32_t* ip;
	float value;

	ip = (uint32_t*)get_shared_channel_parameter_address(def, offsetof(chandef, value));
	*((uint32_t*)&value) = hpi_read4(ip);
	return(value);
#endif

#ifdef SIGNALCUBE
	return(((chandef*)get_channel_base(def))->value);
#endif
}



/********************************************************************************
  FUNCTION NAME     : chan_read_zerofiltop
  FUNCTION DETAILS  : Read the current zero filter result for the selected
					  channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

float chan_read_zerofiltop(chandef* def)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	uint32_t* ip;
	float value;

	ip = (uint32_t*)get_shared_channel_parameter_address(def, offsetof(chandef, zerofiltop));
	*((uint32_t*)&value) = hpi_read4(ip);
	return(value);
#endif

#ifdef SIGNALCUBE
	return(((chandef*)get_channel_base(def))->zerofiltop);
#endif
}



/********************************************************************************
  FUNCTION NAME     : chan_write_dac
  FUNCTION DETAILS  : Write DAC output.
********************************************************************************/

void chan_write_dac(chandef* def, float value)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	uint32_t* op;
	uint32_t* idata;

	idata = (uint32_t*)&value;

	op = (uint32_t*)get_shared_channel_parameter_address(def, offsetof(chandef, value));
	hpi_write4(op, *idata);
#endif
}



/*******************************************************************************
 FUNCTION NAME	   : set_ip_offsettrim
 FUNCTION DETAILS  : Sets the input offset trim value for the selected channel.

					 On entry:

					 def			Pointer to channel.

					 offsettrim		Offset trim value.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void set_ip_offsettrim(chandef* def, float offsettrim)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	float gain;

	gain = ((float)def->pos_gaintrim) / GAINTRIM_SCALE;
	def->offset = (int)((offsettrim * (float)0x80000000UL) / (gain * 1.1));
	update_shared_channel_parameter(def, offsetof(chandef, offset), sizeof(int));
#endif
}



/*******************************************************************************
 FUNCTION NAME	   : read_ip_offsettrim
 FUNCTION DETAILS  : Reads the input offset trim value for the selected channel.

					 On entry:

					 def		Pointer to channel.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

float read_ip_offsettrim(chandef* def)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	float gain;

	gain = ((float)def->pos_gaintrim) / GAINTRIM_SCALE;
	return((((float)def->offset) * gain * 1.1) / (float)0x80000000UL);
#endif

#ifdef SIGNALCUBE
	return(0.0);
#endif
}



/*******************************************************************************
 FUNCTION NAME	   : set_op_offsettrim
 FUNCTION DETAILS  : Sets the output offset trim value for the selected channel.

					 On entry:

					 def			Pointer to channel.

					 offsettrim		Offset trim value.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void set_op_offsettrim(chandef* def, float offsettrim)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	float gain;

	gain = ((float)def->pos_gaintrim) / GAINTRIM_SCALE;
	def->offset = (int)((offsettrim * (float)0x08000000UL * gain) / 1.1);
	update_shared_channel_parameter(def, offsetof(chandef, offset), sizeof(int));
#endif
}



/*******************************************************************************
 FUNCTION NAME	   : read_op_offsettrim
 FUNCTION DETAILS  : Reads the output offset trim value for the selected channel.

					 On entry:

					 def		Pointer to channel.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

float read_op_offsettrim(chandef* def)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	float gain;

	gain = ((float)def->pos_gaintrim) / GAINTRIM_SCALE;
	return((((float)def->offset) * 1.1) / ((float)0x08000000UL * gain));
#endif

#ifdef SIGNALCUBE
	return(0.0);
#endif
}



/*******************************************************************************
 FUNCTION NAME	   : read_peakinfo
 FUNCTION DETAILS  : Reads various peak reader and cyclic values for the 
 					 selected channel.

					 On entry:

					 def	Pointer to channel.
					 dst	Pointer to destination memory area
					 nval	Number of values to transfer

					 Values are stored at the following offsets from dst

					 +0		max		Maximum value.
					 +4	 	min		Minimum value.
					 +8		peak	Peak value.
					 +12	trough	Trough value.
					 +16	cycles	Cycle counter
					 +20	freq	Measured cyclic freq

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void read_peakinfo(chandef *def, void *dst, int nval)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))
	read_shared_channel_parameter(def, offsetof(chandef, def.def_ip.maximum), (sizeof(float) * nval));
#endif

#ifdef SIGNALCUBE
	def = (chandef*)get_channel_base(def);
#endif

	memcpy(dst, &def->def.def_ip.maximum, sizeof(float) * nval);

	/* If we've been asked for more than 5 values, then we need to convert
	   the measured period value from the transferred data block into a
	   frequency and store it at dst+20.
	*/

	if (nval > 5) {
		*((float*)((int)dst + 20)) = 4096.0F / (float)def->def.def_ip.meas_period;
	}
}



/*******************************************************************************
 FUNCTION NAME	   : reset_peaks
 FUNCTION DETAILS  : Reset the peak readers for the selected channel.

					 On entry:

					 def			Pointer to channel.

					 flags			Flags determine which peaks(s) to reset.
									  Bit 0	  Reset upper peak
									  Bit 1	  Reset lower peak

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void reset_peaks(chandef* def, uint32_t flags)
{
	uint32_t start;
	uint32_t diff;
	uint32_t ctrlflags = 0;

	if (flags & 0x01) ctrlflags |= FLAG_RSTUPK;
	if (flags & 0x02) ctrlflags |= FLAG_RSTLPK;

	/* Generate request */

	modify_shared_channel_parameter(def, offsetof(chandef, ctrl), 0, ctrlflags);

	/* Ensure that channel processing code has time to see request */

	start = cnet_interrupt_ctr;
	while (((diff = (cnet_interrupt_ctr - start)) < 0x02) && (diff < 0x80000000));

	/* Clear request */

	modify_shared_channel_parameter(def, offsetof(chandef, ctrl), ctrlflags, 0);
}



/*******************************************************************************
 FUNCTION NAME	   : reset_all_peaks
 FUNCTION DETAILS  : Reset the selected peak readers on all channels.

					 On entry:

					 flags			Flags determine which peaks(s) to reset.
									  Bit 0	  Reset upper peak
									  Bit 1	  Reset lower peak

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void reset_all_peaks(uint32_t flags)
{
	uint32_t start;
	uint32_t diff;
	uint32_t ctrlflags = 0;

	if (flags & 0x01) ctrlflags |= FLAG_RSTUPK;
	if (flags & 0x02) ctrlflags |= FLAG_RSTLPK;

	/* Generate request */

	modify_shared(offsetof(shared, global_control), 0, ctrlflags);

	/* Ensure that channel processing code has time to see request */

	start = cnet_interrupt_ctr;
	while (((diff = (cnet_interrupt_ctr - start)) < 0x02) && (diff < 0x80000000));

	/* Clear request */

	modify_shared(offsetof(shared, global_control), ctrlflags, 0);
}



/*******************************************************************************
 FUNCTION NAME	   : set_reset_parameters
 FUNCTION DETAILS  : Set the parameters for the peak readers.

					 On entry:

					 flags			Flags controlling operation
									 Bit 0	Enable auto reset
					 time			Auto reset time
					 pkcount		Number of peaks counted for peak/trough
									 calculations

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void set_reset_parameters(uint32_t flags, float time, uint32_t pkcount, int mirror)
{
	uint32_t rstcount;

	if (mirror == MIRROR) {
		SNV_UPDATE(autorst_flags, flags);
		SNV_UPDATE(autorst_time, time);
		SNV_UPDATE(pk_rst_count, pkcount);
	}
	else if (mirror == RESTORE) {
		flags = snvbs->autorst_flags;
		time = snvbs->autorst_time;
		pkcount = snvbs->pk_rst_count;
	}

	rstcount = (uint32_t)(time * SAMPLE_RATE); /* Convert time to sample count */
	modify_shared(offsetof(shared, global_control), FLAG_AUTORST, (flags & 0x01) ? FLAG_AUTORST : 0);
	update_shared(offsetof(shared, autorst_int), &rstcount, sizeof(uint32_t));
	update_shared(offsetof(shared, pk_rst_count), &pkcount, sizeof(uint32_t));
}



/*******************************************************************************
 FUNCTION NAME	   : set_peak_threshold
 FUNCTION DETAILS  : Set threshold parameter for peak detection.

					 On entry:

					 flags			Reserved
					 threshold		Threshold value for peak detection

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void set_peak_threshold(uint32_t flags, float threshold, int mirror)
{

	if (mirror == MIRROR) {
		SNV_UPDATE(cycle_window, threshold);
		/*
		#define SNV_UPDATE(offset, val) {snvbs->offset = (val); 							\
									   CtrlSnvCB(&snvbs->offset, sizeof(snvbs->offset));	\
									  }
									  */
	}
	else if (mirror == RESTORE) {
		threshold = snvbs->cycle_window;
	}

	update_shared(offsetof(shared, cycle_window), &threshold, sizeof(float));
}



/*******************************************************************************
 FUNCTION NAME	   : read_peak_threshold
 FUNCTION DETAILS  : Read threshold parameter for peak detection.

					 On exit:

					 flags			Reserved
					 threshold		Threshold value for peak detection

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void read_peak_threshold(uint32_t* flags, float* threshold)
{
	if (flags) *flags = 0;
	if (threshold) *threshold = snvbs->cycle_window;
}



/*******************************************************************************
 FUNCTION NAME	   : set_peak_timeout
 FUNCTION DETAILS  : Set timeout period for peak detection.

					 On entry:

					 flags			Reserved
					 timeout		Timeout period for peak detection

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void set_peak_timeout(uint32_t timeout, int mirror)
{
	uint32_t t = timeout * 4096 / 100;

	if (mirror == MIRROR) {
		SNV_UPDATE(cycle_timeout, t);
	}
	else if (mirror == RESTORE) {
		t = snvbs->cycle_timeout;
	}

	update_shared(offsetof(shared, cycle_timeout), &t, sizeof(uint32_t));
}



/*******************************************************************************
 FUNCTION NAME	   : read_peak_timeout
 FUNCTION DETAILS  : Read timeout period for peak detection.

					 On exit:

					 flags			Reserved
					 threshold		Timeout period for peak detection

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void read_peak_timeout(uint32_t* timeout)
{
	uint32_t t = snvbs->cycle_timeout * 100 / 4096;
	if (timeout) *timeout = t;
}



/*******************************************************************************
 FUNCTION NAME	   : reset_cycles
 FUNCTION DETAILS  : Reset the cycle counter for the selected channel.

					 On entry:

					 def			Pointer to channel.

					 flags			Not used.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void reset_cycles(chandef* def, uint32_t flags)
{
	uint32_t start;
	uint32_t diff;

	/* Generate request */

	modify_shared_channel_parameter(def, offsetof(chandef, ctrl), 0, FLAG_RSTCYCCTR);

	/* Ensure that channel processing code has time to see request */

	start = cnet_interrupt_ctr;
	while (((diff = (cnet_interrupt_ctr - start)) < 0x02) && (diff < 0x80000000));

	/* Clear request */

	modify_shared_channel_parameter(def, offsetof(chandef, ctrl), FLAG_RSTCYCCTR, 0);
}



/*******************************************************************************
 FUNCTION NAME	   : reset_all_cycles
 FUNCTION DETAILS  : Reset the cycle counters on all channels.

					 On entry:

					 flags			Not used.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void reset_all_cycles(uint32_t flags)
{
	uint32_t start;
	uint32_t diff;

	/* Generate request */

	modify_shared(offsetof(shared, global_control), 0, FLAG_RSTCYCCTR);

	/* Ensure that channel processing code has time to see request */

	start = cnet_interrupt_ctr;
	while (((diff = (cnet_interrupt_ctr - start)) < 0x02) && (diff < 0x80000000));

	/* Clear request */

	modify_shared(offsetof(shared, global_control), FLAG_RSTCYCCTR, 0);
}



/*******************************************************************************
 FUNCTION NAME	   : flush_map
 FUNCTION DETAILS  : Flush the map area from DSP B cache memory.

					 On entry:

					 def			Pointer to channel.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void flush_map(chandef* def)
{
	uint32_t start;
	uint32_t diff;

	/* Generate request */

	modify_shared_channel_parameter(def, offsetof(chandef, ctrl), 0, FLAG_MAPFLUSH);

	/* Ensure that channel processing code has time to see request */

	start = cnet_interrupt_ctr;
	while (((diff = (cnet_interrupt_ctr - start)) < 0x02) && (diff < 0x80000000));

	/* Clear request */

	modify_shared_channel_parameter(def, offsetof(chandef, ctrl), FLAG_MAPFLUSH, 0);
}



/********************************************************************************
  FUNCTION NAME     : update_txdrzero
  FUNCTION DETAILS  : Update the transducer zero value for the selected channel.

					  On entry:

					  track		Flag indicates if control loop should be put
								into tracking mode to adjust setpoint values
								based on offset changes.
********************************************************************************/

void update_txdrzero(chandef* def, int value, int track)
{
	uint32_t istate;

#if (defined(CONTROLCUBE) || defined(AICUBE))
	if (track) {
		simple_chandef* base;
		simple_chandef* busy;

		base = (simple_chandef*)get_channel_base(def);
		istate = disable_int();
		while (1) {
			read_shared(&busy, offsetof(shared, txdrzero_chan), sizeof(simple_chandef*));
			if (busy == NULL) break;
		}
		update_shared(offsetof(shared, txdrzero_val), &value, sizeof(int));
		update_shared(offsetof(shared, txdrzero_chan), &base, sizeof(simple_chandef*));
		def->txdrzero = value;
		restore_int(istate);
	}
	else {
#endif
		istate = disable_int();
		def->txdrzero = value;
		update_shared_channel_parameter(def, offsetof(chandef, txdrzero), sizeof(float));
		restore_int(istate);
#if (defined(CONTROLCUBE) || defined(AICUBE))
	}
#endif

}



/********************************************************************************
  FUNCTION NAME     : chan_set_refzero
  FUNCTION DETAILS  : Set the reference zero value for the selected channel.

					  On entry:

					  track		Flag indicates if control loop should be put
								into tracking mode to adjust setpoint values
								based on offset changes.
********************************************************************************/

int chan_set_refzero(chandef* def, float value, int track, int mirror)
{
	uint32_t istate;

#if (defined(CONTROLCUBE) || defined(AICUBE))
	if (track) {
		simple_chandef* base;
		simple_chandef* busy;

		base = (simple_chandef*)get_channel_base(def);
		istate = disable_int();
		while (1) {
			read_shared(&busy, offsetof(shared, refzero_chan), sizeof(simple_chandef*));
			if (busy == NULL) break;
		}
		update_shared(offsetof(shared, refzero_val), &value, sizeof(float));
		update_shared(offsetof(shared, refzero_chan), &base, sizeof(simple_chandef*));
		def->refzero = value;
		restore_int(istate);
	}
	else {
#endif
		istate = disable_int();
		def->refzero = value;
		update_shared_channel_parameter(def, offsetof(chandef, refzero), sizeof(float));
		restore_int(istate);
#if (defined(CONTROLCUBE) || defined(AICUBE))
	}
#endif

	if (mirror)
		mirror_chan_eeconfig(def, 2);	/* Mirror chandef refzero value */

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : read_refzero
  FUNCTION DETAILS  : Read the reference zero value for the selected channel.
********************************************************************************/

float read_refzero(chandef* def)
{
	return(def->refzero);
}



/********************************************************************************
  FUNCTION NAME     : undo_refzero
  FUNCTION DETAILS  : Undo any reference zero offset for the selected channel.

					  On entry:

					  track		Flag indicates if control loop should be put
								into tracking mode to adjust setpoint values
								based on offset changes.

********************************************************************************/

void undo_refzero(chandef* def, int track, int mirror)
{
	def->set_refzero(def, 0.0F, track, UPDATECLAMP, mirror);
}



/********************************************************************************
  FUNCTION NAME   	: chan_check_state
  FUNCTION DETAILS  : Check state of all channels for selected flags.
********************************************************************************/

int chan_check_state(int mask)
{
	chandef* c;
	int chan;

	for (chan = CHANTYPE_INPUT; chan < (CHANTYPE_INPUT + totalinchan); chan++) {
		if ((c = chanptr(chan)) && (c->ctrl & mask)) return(TRUE);
	}
	for (chan = CHANTYPE_OUTPUT; chan < (CHANTYPE_OUTPUT + totaloutchan); chan++) {
		if ((c = chanptr(chan)) && (c->ctrl & mask)) return(TRUE);
	}
	for (chan = CHANTYPE_MISC; chan < (CHANTYPE_MISC + totalmiscchan); chan++) {
		if ((c = chanptr(chan)) && (c->ctrl & mask)) return(TRUE);
	}

	return(FALSE);
}



/********************************************************************************
  FUNCTION NAME   	: chan_clear_all_leds
  FUNCTION DETAILS  : Clear all rear panel LEDs.
********************************************************************************/

void chan_clear_all_leds(void)
{
	chandef* c;
	int chan;

	for (chan = CHANTYPE_INPUT; chan < (CHANTYPE_INPUT + totalinchan); chan++) {
		if ((c = chanptr(chan)) && (c->ledctrl)) c->ledctrl(c, 0);
	}
	for (chan = CHANTYPE_OUTPUT; chan < (CHANTYPE_OUTPUT + totaloutchan); chan++) {
		if ((c = chanptr(chan)) && (c->ledctrl)) c->ledctrl(c, 0);
	}
	for (chan = CHANTYPE_MISC; chan < (CHANTYPE_MISC + totalmiscchan); chan++) {
		if ((c = chanptr(chan)) && (c->ledctrl)) c->ledctrl(c, 0);
	}

}



/********************************************************************************
  FUNCTION NAME   	: chan_modify_all_flags
  FUNCTION DETAILS  : Modify selected flags on all channels.
********************************************************************************/

void chan_modify_all_flags(uint32_t clr, uint32_t set)
{
	chandef* c;
	int chan;

	for (chan = CHANTYPE_INPUT; chan < (CHANTYPE_INPUT + totalinchan); chan++) {
		if (c = chanptr(chan)) c->ctrl = (c->ctrl & ~clr) | set;
	}
	for (chan = CHANTYPE_OUTPUT; chan < (CHANTYPE_OUTPUT + totaloutchan); chan++) {
		if (c = chanptr(chan)) c->ctrl = (c->ctrl & ~clr) | set;
	}
	for (chan = CHANTYPE_MISC; chan < (CHANTYPE_MISC + totalmiscchan); chan++) {
		if (c = chanptr(chan)) c->ctrl = (c->ctrl & ~clr) | set;
	}
	for (chan = CHANTYPE_DIGIO; chan < (CHANTYPE_DIGIO + totaldigiochan); chan++) {
		if (c = chanptr(chan)) c->ctrl = (c->ctrl & ~clr) | set;
	}

}



/*******************************************************************************
 FUNCTION NAME	   : chanptr
 FUNCTION DETAILS  : Return pointer to channel definition.

					 On entry:

					 Chan is the channel number.

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

chandef* chanptr(int chan)
{
	int c;
	chandef* p;

	c = chan & CHAN_MASK;

	switch (CHANTYPE(chan)) {
	case (CHANTYPE_INPUT >> 8):
		if (c >= MAXINCHAN) return(NULL);
		p = &inchaninfo[c];
		return((p->type) ? p : NULL);
	case (CHANTYPE_OUTPUT >> 8):
		if (c >= MAXOUTCHAN) return(NULL);
		p = &outchaninfo[c];
		return((p->type) ? p : NULL);
	case (CHANTYPE_MISC >> 8):
		if (c >= MAXMISCCHAN) return(NULL);
		p = &miscchaninfo[c];
		return((p->type) ? p : NULL);
	case (CHANTYPE_DIGIO >> 8):
		if (c >= MAXDIGIOCHAN) return(NULL);
		p = &digiochaninfo[c];
		return((p->type) ? p : NULL);
	}
	return(NULL);
}



/*******************************************************************************
 FUNCTION NAME	   : chansearch
 FUNCTION DETAILS  : Return pointer to channel definition for a given channel
					 type. Begin the search after the specified channel.

					 On entry:

					 Type is the channel type.
					 Start is the channel to begin the search

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

chandef* chansearch(int type, int start, int* chan)
{
	int c;
	chandef* p;

	c = start & CHAN_MASK;

	switch (CHANTYPE(start)) {
	case (CHANTYPE_INPUT >> 8):
		while (c < MAXINCHAN) {
			p = &inchaninfo[c];
			if (p && (p->type == type)) {
				if (chan) *chan = p->chanid;
				return(p);
			}
			c++;
		}
		return(NULL);
	case (CHANTYPE_OUTPUT >> 8):
		while (c < MAXOUTCHAN) {
			p = &outchaninfo[c];
			if (p && (p->type == type)) {
				if (chan) *chan = p->chanid;
				return(p);
			}
			c++;
		}
		return(NULL);
	case (CHANTYPE_MISC >> 8):
		while (c < MAXMISCCHAN) {
			p = &miscchaninfo[c];
			if (p && (p->type == type)) {
				if (chan) *chan = p->chanid;
				return(p);
			}
			c++;
		}
		return(NULL);
	case (CHANTYPE_DIGIO >> 8):
		while (c < MAXDIGIOCHAN) {
			p = &digiochaninfo[c];
			if (p && (p->type == type)) {
				if (chan) *chan = p->chanid;
				return(p);
			}
			c++;
		}
		return(NULL);
	}
	return(NULL);
}

/********************************************************************************
  FUNCTION NAME     : chan_get_identifier
  FUNCTION DETAILS  : Read the unique identification for a given channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

uint32_t chan_get_identifier(chandef* def)
{
	return(def->identifier);
}



/*******************************************************************************
 FUNCTION NAME	   : chan_modify_flags
 FUNCTION DETAILS  : Modify the channel control flags.

					 On entry:

					 def	Pointer to channel
					 clr	Flags to clear
					 set	Flags to set

					 Action is flags = (flags & ~clr) | set

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_modify_flags(chandef* def, uint32_t clr, uint32_t set)
{
	uint32_t istate;

	istate = disable_int();
	def->ctrl = (def->ctrl & ~clr) | set;
	restore_int(istate);

	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(uint32_t));
}



/*******************************************************************************
 FUNCTION NAME	   : chan_set_simptr
 FUNCTION DETAILS  : Modify the channel simulation pointer and scaling.

					 On entry:

					 def	Pointer to channel
					 ptr	Pointer to simulation value
					 scale	Simulation scale factor

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_set_simptr(chandef* def, float* ptr, float scalep, float scalen)
{
	uint32_t istate;

	istate = disable_int();
	def->simptr = ptr;
	def->simscalep = scalep;
	def->simscalen = scalen;
	restore_int(istate);

	update_shared_channel_parameter(def, offsetof(chandef, simptr),
		(sizeof(float*) + (sizeof(float) * 2)));
}



/*******************************************************************************
 FUNCTION NAME	   : chan_set_setpoint_ptr
 FUNCTION DETAILS  : Modify the channel setpoint pointer.

					 On entry:

					 def	Pointer to channel
					 ptr	Pointer to setpoint value

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_set_setpoint_ptr(chandef* def, float* ptr)
{
	def->spptr = ptr;
	update_shared_channel_parameter(def, offsetof(chandef, spptr), sizeof(float*));
}



/*******************************************************************************
 FUNCTION NAME	   : chan_set_virtualptr
 FUNCTION DETAILS  : Make selected channel virtual.

					 On entry:

					 def	Pointer to channel
					 ptr	Pointer to virtual channel

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_set_virtualptr(chandef* def, float* ptr)
{
	def->sourceptr = (int*)ptr;

	update_shared_channel_parameter(def, offsetof(chandef, sourceptr), sizeof(float*));
	chan_modify_flags(def, 0, FLAG_VIRTUAL);
}



/*******************************************************************************
 FUNCTION NAME	   : chan_set_genbusptr
 FUNCTION DETAILS  : Modify the channel GenBus pointer.

					 On entry:
					 
					 def	Pointer to channel
					 pGb	Pointer to GenBus
					 
********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :                                                           
********************************************************************************
 Called From :
 Calls:                                                                     
*******************************************************************************/

void chan_set_genbusptr(chandef *def, void *pGb)
{
	if (def)
	{
		def->gbptr = pGb;
		update_shared_channel_parameter(def, offsetof(chandef, gbptr), sizeof(void *));
	}
}



/*******************************************************************************
 FUNCTION NAME	   : chan_merge_gainflags
 FUNCTION DETAILS  : Merge gain control flags into control word.

					 On entry:

					 inflags	Gain control flags

								Bits 2,0  : 00 = Gain
											01 = Sensitivity
											10 = factor (Signal Cube)

					 On exit:

					 returns gain control flags

								Bits 17,7  : 00 = Gain
											 01 = Sensitivity
											 10 = factor (Signal Cube)

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

uint32_t chan_merge_gainflags(uint32_t flags)
{
	uint32_t flag0;
	uint32_t flag1;

	flag0 = (flags & (1 << 0)) ? (1 << 7) : 0;	/* Extract bit 0 into bit 7 position  */
	flag1 = (flags & (1 << 2)) ? (1 << 17) : 0; /* Extract bit 2 into bit 17 position */

	return(flag1 | flag0);
}



/*******************************************************************************
 FUNCTION NAME	   : chan_extract_gainflags
 FUNCTION DETAILS  : Extract gain control flags from control word.

					 On entry:

					 inflags	Gain control flags

								Bits 17,7  : 00 = Gain
											 01 = Sensitivity
											 10 = factor (Signal Cube)

					 On exit:

					 returns gain control flags

								Bits 2,0  : 00 = Gain
											01 = Sensitivity
											10 = factor (Signal Cube)

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

uint32_t chan_extract_gainflags(uint32_t flags)
{
	uint32_t flag0;
	uint32_t flag1;

	flag0 = (flags & (1 << 7)) ? (1 << 0) : 0;	/* Extract bit 7 into bit 0 position  */
	flag1 = (flags & (1 << 17)) ? (1 << 2) : 0; /* Extract bit 17 into bit 2 position */

	return(flag1 | flag0);
}



void ModifyCommandClamp(int channel, float min, float max);

#define MIN(a,b) (((a) < (b)) ? (a) : (b))

#if 1

/*******************************************************************************
 FUNCTION NAME	   : chan_validate_zero
 FUNCTION DETAILS  : Validate the zero values for the selected channel.

					 See chan_update_command_clamp for code description.
********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

int chan_validate_zero(chandef* def, int txdrzero, float refzero)
{
	float ip_txdrzero;
	float gaintrim;
	float offset;
	int valid;
	int gt;

	/* Convert the supplied txdrzero value into the equivalent normalised floating
	   point value.
	*/

	ip_txdrzero = ((float)txdrzero / (float)TXDRZERO_SCALE) * 1.1F;

	/* Get the gaintrim value. For 2-point calibration use the smallest gaintrim value
	   since this equates to the smallest allowed zero offset value. The gaintrim factor
	   is converted into the equivalent normalised floating point value.
	*/

	gt = (def->ctrl & FLAG_2PTCAL) ? MIN(def->pos_gaintrim, def->neg_gaintrim) : def->pos_gaintrim;

	gaintrim = ((float)gt / (float)GAINTRIM_SCALE);

	/* Calculate the effective input offset based on the scaled refzero value
	   and the txdrzero. Then compare with the maximum allowed analogue overrange
	   MAXZERO (8%).

	   In asymmetrical mode, the range checking is bypassed since we need to be
	   able to set the zero position anywhere in the operating range. This is an
	   oversimplification, the actual allowed range can be smaller, but we assume
	   that the command clamp keeps us away from any trouble due to the reduced
	   range.
	*/

	if (def->ctrl & FLAG_POLARITY) {
		offset = ip_txdrzero - (refzero / gaintrim);
	}
	else {
		offset = ip_txdrzero + (refzero / gaintrim);
	}

	if (def->ctrl & FLAG_ASYMMETRICAL) {
		valid = TRUE;
	}
	else {
		valid = (offset >= -MAXZERO) && (offset <= MAXZERO);
	}

	return(valid ? NO_ERROR : ZERO_RANGE);
}

#endif



/*******************************************************************************
 FUNCTION NAME	   : chan_update_command_clamp
 FUNCTION DETAILS  : Update the command clamp values for the selected channel.

					 The original value of txdrzero is calculated as:

					 Required zero / overall gain trim

					 Therefore the output component corresponding to the
					 txdrzero value is:

					 Txdrzero * overall gain trim

					 However, because the hardware gain trim is used to
					 compensate for amplifier gain errors (i.e. 100% input
					 will produce a 100% output), then its component has
					 already been taken into account.

					 Therefore, only the user gain trim component of the
					 overall gain trim value needs to be used when calculating
					 the effect of the input signal.

					 The overall calculation therefore becomes:

					 (IP * User gaintrim) + (txdrzero * gaintrim)

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_update_command_clamp(chandef* def, float txdrzero, float refzero, int noclamp, int updateclamp)
{
#if 1

	float val_pos;
	float val_neg;
	float gt_pos = 1.0F;
	float gt_neg = 1.0F;
	float ip_txdrzero;
	float ip_pos = CLAMP;
	float ip_neg = -CLAMP;
	float clamp = (def->ctrl & FLAG_ASYMMETRICAL) ? (CLAMP / 2.0F) : CLAMP;
	float clamp_pos = clamp;
	float clamp_neg = -clamp;
	int gaintrim;
	float hw_gaintrim;
	float usr_gaintrim;
	int pol;

	/* Calculate positive/negative gain trim values */

	if (def->read_gaintrim) def->read_gaintrim(def, &gt_pos, &gt_neg);
	if (!(def->ctrl & FLAG_2PTCAL)) gt_neg = gt_pos;

	/* Calculate equivalent input transducer zero value */

	ip_txdrzero = ((float)def->txdrzero / (float)TXDRZERO_SCALE) * 1.1F;

	/* Determine polarity */

	pol = ((ip_txdrzero * ((def->ctrl & FLAG_POLARITY) ? -1.0F : 1.0F)) < 0.0F) ? 1 : -1;

	/* Calculate hardware gain component of overall gain trim */

	gaintrim = def->pos_gaintrim;
	if ((def->ctrl & FLAG_2PTCAL) && (pol < 0)) gaintrim = def->neg_gaintrim;
	usr_gaintrim = (pol > 0) ? gt_pos : gt_neg;
	hw_gaintrim = ((float)gaintrim / (float)GAINTRIM_SCALE) / usr_gaintrim;

	/* Calculate the output corresponding to the maximum possible input value */

	val_pos = ip_pos + (ip_txdrzero * hw_gaintrim);
	val_pos = val_pos * ((val_pos < 0.0F) ? gt_neg : gt_pos);
	if (def->ctrl & FLAG_UNIPOLAR) {
		val_pos = val_pos * 2.0F;
		val_pos = val_pos - 1.0F;
	}
	if (def->ctrl & FLAG_ASYMMETRICAL) {
		val_pos = val_pos * 0.5F;
	}
	if (val_pos > 1.1F) val_pos = 1.1F;
	val_pos = val_pos + def->refzero;

	/* Calculate the positive clamp value adjusted by the reference zero value */

	clamp_pos = clamp_pos + def->refzero;

	/* Determine the limiting clamp value based on the two possible constraints */

	if (val_pos < clamp_pos) clamp_pos = val_pos;

	/* Calculate the output corresponding to the minimum possible input value */

	val_neg = ip_neg + (ip_txdrzero * hw_gaintrim);
	val_neg = val_neg * ((val_neg < 0.0F) ? gt_neg : gt_pos);
	if (def->ctrl & FLAG_UNIPOLAR) {
		val_neg = val_neg * 2.0F;
		val_neg = val_neg - 1.0F;
	}
	if (def->ctrl & FLAG_ASYMMETRICAL) {
		val_neg = val_neg * 0.5F;
	}
	if (val_neg < -1.1F) val_neg = -1.1F;
	val_neg = val_neg + def->refzero;

	/* Calculate the negative clamp value adjusted by the reference zero value */

	clamp_neg = clamp_neg + def->refzero;

	/* Determine the limiting clamp value based on the two possible constraints */

	if (val_neg > clamp_neg) clamp_neg = val_neg;

	if (noclamp) {
		clamp_pos = CLAMP;
		clamp_neg = -CLAMP;
	}

#endif

	//float clamp_pos;
	//float clamp_neg;

	//chan_calc_command_clamp(def, txdrzero, refzero, noclamp, &clamp_pos, &clamp_neg);

	def->upper_cmd_clamp = clamp_pos;
	def->lower_cmd_clamp = clamp_neg;

	if (updateclamp) ModifyCommandClamp(def->chanid & 0xFF, clamp_neg, clamp_pos);
}



/*******************************************************************************
 FUNCTION NAME	   : chan_initialise_command_clamp
 FUNCTION DETAILS  : Update the command clamp values for all input channels.
********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_initialise_command_clamp(void)
{
	int chan;
	chandef* def;
	float txdrzero;

	for (chan = 0; chan < 256; chan++) {
		def = chanptr(chan);
		if (def) {
			txdrzero = (def->read_txdrzero) ? def->read_txdrzero(def) : 0.0F;
			chan_update_command_clamp(def, txdrzero, def->refzero, FALSE, UPDATECLAMP);
		}
	}
#if (defined(CONTROLCUBE) || defined(AICUBE))
	CtrlClampChangedEvent();
#endif
}



/*******************************************************************************
 FUNCTION NAME	   : chan_initialise_threshold
 FUNCTION DETAILS  : Update the peak threshold setting for all input channels.
********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_initialise_threshold(void)
{
	int chan;
	chandef* def;
	uint32_t flags;
	float threshold;

	for (chan = 0; chan < 256; chan++) {
		def = chanptr(chan);
		if (def && def->readpkthreshold) {
			def->readpkthreshold(def, &flags, &threshold);

			/* Check if local threshold value is disabled and use global setting instead */

			if (!(flags & 0x40000000)) read_peak_threshold(NULL, &threshold);

			/* Inform KO code of the current threshold value */

			CtrlUpdateThreshold(chan, threshold);
		}
	}
}



/********************************************************************************
  FUNCTION NAME     : read_cyclecount
  FUNCTION DETAILS  : Read the channel cycle count.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

uint32_t chan_read_cyclecount(chandef* def)
{
	read_shared_channel_parameter(def, offsetof(chandef, def.def_ip.cyc_counter), sizeof(uint32_t));
	return(def->def.def_ip.cyc_counter);
}



/********************************************************************************
  FUNCTION NAME     : validate_map
  FUNCTION DETAILS  : Validate a linearisation table map.

					  On entry:

					  map		Points to the start of the map
					  len		Length of map

					  Returns TRUE if table contains any NaN or inf values.

********************************************************************************/

uint32_t chan_validate_map(float* map, int len)
{
	int n;
	uint32_t* p = (uint32_t*)map;

	for (n = 0; n < len; n++) {
		uint32_t data = *p++;
		if ((data & 0x7F800000) == 0x7F800000) return(TRUE);
	}
	return(FALSE);
}



/********************************************************************************
  FUNCTION NAME     : chan_normalise_setup
  FUNCTION DETAILS  : Normalise the chandef settings required to use a channel
					  as a setup mode virtual transducer.

					  Clear invert flag.
					  Clear 2-pt, linearisation table calibration flags.
					  Disable filter.
					  Set gaintrin to 1.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

/* Define the flags that must be cleared when normalising for a setup mode virtual
   transducer.
*/

#define FLAG_SETUP_CLR	(FLAG_ACTRANSDUCER | FLAG_POLARITY | FLAG_RSTUPK |		\
					   	 FLAG_AUTORST | FLAG_2PTCAL | FLAG_LINEARISE |			\
					     FLAG_UNIPOLAR | FLAG_GAINTYPE | FLAG_SIMULATION |		\
					     FLAG_RSTLPK | FLAG_ASYMMETRICAL | FLAG_MAPFLUSH |		\
					     FLAG_SHUNTCAL | FLAG_RSTCYCCTR | FLAG_CYCWINDOW |		\
					     FLAG_ACCOUPLED | FLAG_NEGSHUNTCAL | FLAG_CYCEVENT |	\
					     FLAG_DISABLE)

void chan_normalise_setup(chandef* def)
{
	const float gain = 1.0F;

	def->set_flags(def, 0, FLAG_SETUP_CLR, FALSE, TRUE);
	set_filter(def, FALSE, 0, FALSE, FILTER_LOWPASS, 1, 0.0F, 0.0F, TRUE);
	def->set_gaintrim(def, (float*)&gain, (float*)&gain, FALSE, MIRROR);
	update_shared_channel(def);
}
