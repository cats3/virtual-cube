/********************************************************************************
 * MODULE NAME       : hw_2gp1sv.h												*
 * MODULE DETAILS    : Header for 2GP1SV specific routines						*
 *																				*
 ********************************************************************************/

#ifndef __HW_2GP1SV_H
#define __HW_2GP1SV_H

#include <stddef.h>
#include <stdint.h>

/* Definition of the hardware interface on the 2GP1SV card */

typedef struct hw_2gp1sv {
	union {
		struct {
			uint16_t	excaddr;	/* 	0x00	Write excitation RAM address			*/
			short	exc0;		/* 	0x02	Write excitation channel 0				*/
			short	exc1;		/*	0x04	Write excitation channel 1				*/
			uint16_t	reserved0;	/*	0x06	Reserved	                            */
			uint16_t	reserved1;	/*	0x08	Reserved                            	*/
			short	sv0;		/*	0x0A	Write SV0 DAC  							*/
			uint16_t	reserved3;	/*	0x0C	Reserved								*/
			uint16_t	ctrl;		/*	0x0E	Write control register
												Bit 0	Channel 0 DC/AC flag
												Bit 1	Channel 1 DC/AC flag		*/
			uint16_t	reserved4;	/* 	0x10	Reserved								*/
			uint16_t	reserved5;	/* 	0x12	Reserved								*/
			uint16_t	reserved6;	/* 	0x14	Reserved								*/
			uint16_t	reserved7;	/* 	0x16	Reserved								*/
			uint16_t	reserved8;	/* 	0x18	Reserved								*/
			uint16_t	reserved9;	/* 	0x1A	Reserved								*/
			uint16_t	reserved10;	/* 	0x1C	Reserved								*/
			uint16_t	reserved11;	/* 	0x1E	Reserved								*/
			uint16_t	scl;		/*	0x20	Write I2C SCL output					*/
			uint16_t	sda;		/*	0x22	Write I2C SDA output					*/
			uint16_t	reserved12;	/*	0x24	Reserved								*/
			uint16_t	teds1;		/*	0x26	Write TEDS1 output						*/
			uint16_t	teds2;		/*	0x28	Write TEDS2 output						*/
			uint16_t	leds;		/*	0x2A	Write LED outputs
												Bit 0	Channel 0 LED
												Bit 1	Channel 1 LED
												Bit 2	Servo-valve LED				*/
			uint16_t	reserved13;	/*	0x2C	Reserved								*/
			uint16_t	reserved14;	/*	0x2E	Reserved								*/
			uint16_t	txrlycs;	/*	0x30	Write txdr relay control chip select	*/
			uint16_t	txrlydin;	/*	0x32	Write txdr relay control data input		*/
			uint16_t	txrlyclk;	/*	0x34	Write txdr relay control clock input	*/
			uint16_t	reserved15;	/*	0x36	Reserved								*/
			uint16_t	dacen;		/*	0x38	Write DAC/exc voltage output enable		
												Bit 0	Channel 0 excitation enable 
												Bit 1	Channel 1 excitation enable
												Bit 2	DAC enable					*/
			uint16_t	reserved16;	/*	0x3A	Reserved								*/
			uint16_t	test;		/*	0x3C	Write test register						*/
			uint16_t	reserved17;	/*	0x3A	Reserved								*/
		} wr;
		struct {
			uint16_t	reserved0;	/*  0x00	Reserved								*/
			uint16_t	reserved1;	/*  0x02	Reserved								*/
			uint16_t	reserved2;	/*  0x04	Reserved								*/
			uint16_t	reserved3;	/*  0x06	Reserved								*/
			uint16_t	reserved4;	/*  0x08	Reserved								*/
			uint16_t	reserved5;	/*  0x0A	Reserved								*/
			uint16_t	reserved6;	/*  0x0C	Reserved								*/
			uint16_t	ctrl;		/*	0x0E	Read control register
												Bit 0	Channel 0 DC/AC flag
												Bit 1	Channel 1 DC/AC flag		*/
			int		adc1;		/*	0x10	Read ADC1 result						*/
			int		adc2;		/*	0x14	Read ADC2 result						*/
			uint16_t	reserved7;	/*	0x18	Reserved								*/
			uint16_t	reserved8;	/*	0x1A	Reserved								*/
			uint16_t	reserved9;	/*	0x1C	Reserved								*/
			uint16_t	reserved10;	/*	0x1E	Reserved								*/
			uint16_t	scl;		/*	0x20	Read I2C SCL input						*/
			uint16_t	sda;		/*	0x22	Read I2C SDA input						*/
			uint16_t	reserved11;	/*	0x24	Reserved								*/
			uint16_t	teds1;		/*	0x26	Read TEDS1 input						*/
			uint16_t	teds2;		/*	0x28	Read TEDS2 input						*/
			uint16_t	fault;		/*  0x2A	Read fault status inputs				*/
			uint16_t	reserved12;	/*  0x2C	Reserved								*/
			uint16_t	reserved13;	/*  0x2E	Reserved								*/
			uint16_t	reserved14;	/*  0x30	Reserved								*/
			uint16_t	reserved15;	/*  0x32	Reserved								*/
			uint16_t	reserved16;	/*  0x34	Reserved								*/
			uint16_t	reserved17;	/*  0x36	Reserved								*/
			uint16_t	dacen;		/*	0x38	Read DAC/exc voltage output enable		
												Bit 0	Channel 0 excitation enable 
												Bit 1	Channel 1 excitation enable
												Bit 2	DAC enable					*/
			uint16_t	reserved18;	/*  0x3A	Reserved								*/
			uint16_t	test;		/*  0x3C	Read test register						*/
			uint16_t	ident;		/*  0x3E	Read identifier register				*/
		} rd;
	} u;
} hw_2gp1sv;

/* Hardware flags */

#define CHAN1_AC	0x01		/* Channel 1 AC transducer flag		*/
#define CHAN2_AC	0x02		/* Channel 2 AC transducer flag		*/

#define CHAN1_EXC	0x01		/* Channel 1 excitation enable flag */
#define CHAN2_EXC	0x02		/* Channel 2 excitation enable flag	*/
#define DAC_EN		0x04		/* DAC voltage enable flag			*/

#define DAC_UPDATE	0x01		/* DAC update enable				*/

#define SV_CURRENT	0			/* Current drive servo-valve type	*/
#define SV_VOLTAGE	1			/* Voltage drive servo-valve type	*/

/* Definition of the 2GP1SV specific information structure contained within
   the generic channel definition structure (see channel.h).
*/

#define WSIZE_2GP1SV	1		/* Workspace size */

/* Information structure for 2GP1SV card

   The first 5 entries of this structure must match with those in the
   4AD2DA and MBOARD information structures.
*/

typedef struct info_2gp1sv {

	hw_2gp1sv *hw;				/* Hardware base address							*/
	int   input;				/* Input channel number on 2GP1SV card				*/
	int   output;				/* Output channel number on 2GP1SV card				*/
	uint8_t *relaystate;			/* Copy of relay state outputs						*/
	uint8_t  ledmask;				/* Not used											*/
	uint8_t *ledstate;				/* Copy of LED state outputs						*/
	uint8_t  work[WSIZE_2GP1SV];	/* Local workspace
									Used to hold copy of relay state outputs for
									both channels on this card						*/
} info_2gp1sv;

/* Function prototypes */

uint32_t hw_2gp1sv_install(int slot, int *numinchan, int *numoutchan, int *nummiscchan, 
				        hw_2gp1sv *hw, int flags);
void hw_2gp1sv_save(int slot, uint32_t status);
void hw_2tx_save(int slot, uint32_t status);


#endif
