#ifndef PORTING_H
#define PORTING_H

#include <stdint.h>

#define near
#define restrict
#define far

#define TRUE 1
#define FALSE 0

uint32_t _ftoi(float f);
float _itof(uint32_t i);


#endif
