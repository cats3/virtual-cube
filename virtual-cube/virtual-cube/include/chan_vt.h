/********************************************************************************
 * MODULE NAME       : chan_vt.h												*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : Header for virtual channel specific routines				*
 ********************************************************************************/

#ifndef __CHAN_VT_H
#define __CHAN_VT_H

#include <stddef.h>
#include <stdint.h>

/* Configuration information for VT function */

typedef struct cfg_vt {
	uint32_t flags;				/* Control flags:
										Bit 0 	 : Reserved
										Bit 1 	 : Polarity inverted					
										Bit 2 	 : Reserved
										Bit 3 	 : Reserved
										Bit 4,5  : Reserved
										Bit 6    : Select unipolar mode						
										Bit 7	 : Reserved
										Bit 8	 : Reserved
										Bit 9-17 : Reserved									*/
	float gain;					/* Current overall gain setting (calibration)				*/
	float pos_gaintrim;			/* Current positive gain trim setting (calibration)			*/
	float txdrzero;				/* Current transducer zero (calibration)					*/
	float neg_gaintrim;			/* Current negative gain trim setting (calibration)			*/
	int   reserved1;			/* Reserved for future use									*/
	int   reserved2;			/* Reserved for future use									*/
	int   reserved3;			/* Reserved for future use									*/
} cfg_vt;

/* Incomplete declaration of chandef to allow function prototypes to work */

typedef struct chandef chandef;

extern void  chan_vt_initlist(void);
extern void  chan_vt_default(chandef *def, uint32_t chanid);
extern void  chan_vt_set_flags(chandef *def, int set, int clr, int updateclamp, int mirror);
extern int   chan_vt_read_flags(chandef *def);
extern void  chan_vt_write_map(chandef *def, uint32_t flags, float *value, char *filename, int flush, int mirror);
extern void  chan_vt_read_map(chandef *def, uint32_t *flags, float *value, char *filename);
extern void  chan_vt_set_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim, int calmode, int mirror);
extern void  chan_vt_read_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim);
extern int   chan_vt_set_refzero(chandef *def, float refzero, int track, int updateclamp, int mirror);
extern int   chan_vt_refzero_zero(chandef *def, int track, int updateclamp, int mirror);
extern void  chan_vt_write_userstring(chandef *def, uint32_t start, uint32_t length, char *string, int mirror);
extern void  chan_vt_read_userstring(chandef *def, uint32_t start, uint32_t length, char *string);
extern void  chan_vt_set_peak_threshold(chandef *def, uint32_t flags, float threshold, int mirror);
extern void  chan_vt_read_peak_threshold(chandef *def, uint32_t *flags, float *threshold);
extern void  chan_vt_write_faultmask(chandef *def, uint32_t mask, int mirror);
extern uint32_t chan_vt_read_faultstate(chandef *def, uint32_t *capabilities, uint32_t *mask);

#endif
