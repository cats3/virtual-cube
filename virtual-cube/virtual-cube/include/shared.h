/********************************************************************************
 * MODULE NAME       : shared.h													*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : Defines structure used for sharing data between DSPA and	*
 *					   DSPB.													*
 *																				*
 *					   Data in this structure actually resides in the memory of	*
 *					   DSPB and must be accessed by means of the host-port.		*
 *																				*
 ********************************************************************************/

#ifndef __SHARED_H
#define __SHARED_H

#include <stdint.h>

#include "channel.h"
#include "eventlog.h"

 /* Structure defines the area of shared memory used to pass data between DSP A and DSP B */

typedef struct shared {
	int   totalinchan;						/* Total number of input channels  				*/
	int   totaloutchan;						/* Total number of output channels 				*/
	int   totalmiscchan;						/* Total number of misc channels 				*/
	int   totalvirtualchan;					/* Total number of virtual input channels		*/

	uint32_t global_control;						/* Global channel control flags					*/

	float cycle_window;						/* Size of cycle detection window				*/
	uint32_t cycle_timeout;						/* Timeout period for cycle detection			*/
	uint32_t pk_rst_count;						/* Number of cycles for cyclic peak measurement	*/
	uint32_t autorst_ctr;						/* Automatic reset counter						*/
	uint32_t autorst_int;						/* Automatic reset interval						*/

	int   physinchan;							/* Number of physical input channels  			*/

	simple_chandef inchaninfo[MAXINCHAN];		/* Input channel information array  			*/
	simple_chandef outchaninfo[MAXOUTCHAN];	/* Output channel information array 			*/
	simple_chandef miscchaninfo[MAXMISCCHAN];	/* Misc channel information array  				*/

	simple_chandef* txdrzero_chan;			/* Pointer to input channel for txdrzero adjust	*/
	int    		  txdrzero_val;				/* Value for txdrzero adjust operation			*/

	simple_chandef* refzero_chan;				/* Pointer to input channel for refzero adjust	*/
	float    		  refzero_val;				/* Value for refzero adjust operation			*/

} shared;

extern shared shared_structure;
extern shared* shared_chandata;

extern shared* dspb_shared_chandata;		/* Pointer to start of channel data shared area	*/
extern void* dspb_shared_c3appdata;		/* Pointer to start of C3AppData shared area 	*/

#ifdef _DSPA

void shared_initialise(void);
void update_shared_channel(chandef* def);
void update_shared_channel_parameter(chandef* def, int offset, int len);
void modify_shared(int src, uint32_t clr, uint32_t set);
void modify_shared_channel_parameter(chandef* def, int offset, uint32_t clr, uint32_t set);
void read_shared_channel_parameter(chandef* def, int offset, int len);
uint8_t* get_shared_channel_parameter_address(chandef* def, int param);
void update_shared(int dst, void* src, int len);
void read_shared(void* dst, int src, int len);
uint8_t* get_channel_base(chandef* def);

#endif

#ifdef _DSPB

chandef* get_channel_base_dspb(int chan);

#endif

#endif
