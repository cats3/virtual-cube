/********************************************************************************
 * MODULE NAME       : chan_digtxdr.h											*
 * MODULE DETAILS    : Header for digital transducer channel specific routines	*
 *																				*
 ********************************************************************************/

#ifndef __CHAN_DIGTXDR_H
#define __CHAN_DIGTXDR_H

#include <stddef.h>
#include <stdint.h>

/* Configuration information for DIGTXDR function */

typedef struct cfg_digtxdr {
	uint32_t flags;				/* Control flags:
										Bit 0 	  : Reserved						
										Bit 1 	  : Polarity inverted
										Bit 2 	  : Reserved
										Bit 3 	  : Reserved
										Bit 4,5   : Linearisation control
													 0 = None
													 1 = 2-point
													 2 = Full
													 3 = Reserved
										Bit 6     : Select unipolar mode				
										Bit 7	  : Reserved							
										Bit 8	  : Reserved
										Bit 9	  : Reserved
										Bit 10    : Reserved
										Bit 11-15 : Reserved (transducer type)
										Bit 16	  : Select asymmetrical transducer mode
										Bit 17	  : Reserved								*/
	float gain;					/* Current overall gain setting	(calibration)				*/
	float pos_gaintrim;			/* Current positive gain trim setting (calibration)			*/
	float txdrzero;				/* Current transducer zero (calibration)					*/
	float neg_gaintrim;			/* Current negative gain trim setting (calibration)			*/
	uint32_t faultmask;			/* Current fault enable mask								*/
	uint32_t hwflags;				/* Hardware configuration flags
										Bit 0	  	: Type
													   0 = Encoder
													   1 = SSI								
										Bit 1	  	: Reserved
										Bit 2	  	: Encoding
													   0 = Binary		
													   1 = Gray
										Bits 20-23  : Baud Rate (CAN bus only)
													   0 = 1Mbit/s
													   1 = 800kbit/s
													   2 = 500kbit/s
													   3 = 250kbit/s
													   4 = 125kbit/s
													   5 = 50kbit/s
													   6 = 20kbit/s
													   7 = 10kbit/s
													   8-15 = Reserved
										Bits 24-31	: Device ID (CAN bus only)			 	*/
	uint32_t ssi_datalen;			/* SSI data length											*/
	uint32_t ssi_clkperiod;		/* SSI clock period (ns)									*/
	float bitsize;				/* Units/bit value											*/
	int   reserved1;			/* Reserved for future use									*/
	int   reserved2;			/* Reserved for future use									*/
	int   reserved3;			/* Reserved for future use									*/
	int   reserved4;			/* Reserved for future use									*/
} cfg_digtxdr;

#define DIGTXDR_TYPE_MASK		0x0001		/* Mask to extract type	*/

#define DIGTXDR_TYPE_ENCODER	0x0000		/* Encoder 				*/
#define DIGTXDR_TYPE_SSI		0x0001		/* SSI transducer		*/

/* The following flag is used when setting the transducer configuration
   but is not stored in the configuration flags word.
*/

#define DIGTXDR_SETRATE			0x0002		/* Set data rate 		*/

#define DIGTXDR_ENC_BINARY		0x0000		/* Binary encoding		*/
#define DIGTXDR_ENC_GRAY		0x0004		/* Gray encoding		*/

/* Hardware calibration information for DIGTXDR function. 

   Important - ensure that size of calibration data structure does not exceed the
			   size of the cal_gp structure.
*/

typedef struct cal_digtxdr {
	int	  cal_offset;			/* Calibrated offset value							*/
	float cal_gain;				/* Calibrated gain value							*/
} cal_digtxdr;

/* Incomplete declaration of chandef to allow function prototypes to work */

typedef struct chandef chandef;

extern void  chan_dt_initlist(void);
extern void  chan_dt_set_pointer(chandef *def, int *ptr);
extern int  *chan_dt_get_pointer(chandef *def);
extern void  chan_dt_default(chandef *def, uint32_t chanid);
extern void  chan_dt_set_caltable(chandef *def, int entry, float value);
extern float chan_dt_read_caltable(chandef *def, int entry);
extern void  chan_dt_set_calgain(chandef *def, float gain);
extern float chan_dt_read_calgain(chandef *def);
extern void  chan_dt_set_caloffset(chandef *def, float offset);
extern float chan_dt_read_caloffset(chandef *def);
extern void  chan_dt_set_gain(chandef *def, float gain, uint32_t flags, int mirror);
extern void  chan_dt_read_gain(chandef *def, float *gain, uint32_t *flags);
extern void  chan_dt_set_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim, int calmode, int mirror);
extern void  chan_dt_read_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim);
extern int   chan_dt_set_refzero(chandef *def, float refzero, int track, int updateclamp, int mirror);
extern int   chan_dt_set_txdrzero(chandef *def, float txdrzero, int track, int updateclamp, int mirror);
extern float chan_dt_read_txdrzero(chandef *def);
extern int   chan_dt_txdrzero_zero(chandef *def, int track, int updateclamp, int mirror);
extern int   chan_dt_refzero_zero(chandef *def, int track, int updateclamp, int mirror);
extern void  chan_dt_set_flags(chandef *def, int set, int clr, int updateclamp, int mirror);
//extern int   chan_dt_read_flags(chandef *def);
extern void  chan_dt_set_info(chandef *def, float range, char *units, char *name, char *serialno, int mirror);
extern void  chan_dt_read_info(chandef *def, float *range, char *units, char *name, char *serialno);
extern void  chan_dt_write_map(chandef *def, uint32_t flags, float *value, char *filename, int flush, int mirror);
extern void  chan_dt_read_map(chandef *def, uint32_t *flags, float *value, char *filename);
extern void  chan_dt_write_userstring(chandef *def, uint32_t start, uint32_t length, char *string, int mirror);
extern void  chan_dt_read_userstring(chandef *def, uint32_t start, uint32_t length, char *string);
extern void  chan_dt_set_peak_threshold(chandef *def, uint32_t flags, float threshold, int mirror);
extern void  chan_dt_read_peak_threshold(chandef *def, uint32_t *flags, float *threshold);
extern void  chan_dt_write_faultmask(chandef *def, uint32_t mask, int mirror);
extern uint32_t chan_dt_read_faultstate(chandef *def, uint32_t *capabilities, uint32_t *mask);
extern void  chan_dt_write_config(chandef *def, uint32_t flags, float bitsize, uint32_t ssi_datalen, uint32_t ssi_datarate, int mirror);
extern void  chan_dt_read_config(chandef *def, uint32_t *flags, float *bitsize, uint32_t *ssi_datalen, uint32_t *ssi_datarate);
extern void  chan_dt_write_fullscale(chandef *def, float fullscale);

#endif
