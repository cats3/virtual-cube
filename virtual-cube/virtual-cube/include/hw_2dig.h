/********************************************************************************
 * MODULE NAME       : hw_2dig.h												*
 * MODULE DETAILS    : Header for 2DIG specific routines						*
 *																				*
 ********************************************************************************/

#ifndef __HW_2DIG_H
#define __HW_2DIG_H

#include <stdint.h>
#include "channel.h"

/* Definition of the hardware interface on the 2DIG card */

typedef struct hw_2dig {
	union {
		struct {
			uint16_t	ctrl1a;		/* 	0x00	Write transducer 1 control register A	
												Bit 0	0 = Encoder, 1 = SSI
												Bit 2	0 = Binary, 1 = Gray		*/
			uint16_t	ctrl2a;		/* 	0x02	Write transducer 2 control register	A	
												Bit 0	0 = Encoder, 1 = SSI
												Bit 2	0 = Binary, 1 = Gray		*/
			uint16_t	reset1;		/* 	0x04	Write transducer 1 reset register		
												Bit 0	0 = Normal, 1 = Reset		*/
			uint16_t	reset2;		/* 	0x06	Write transducer 2 reset register		
												Bit 0	0 = Normal, 1 = Reset		*/
			uint16_t	reserved0;	/*	0x08	Reserved								*/
			uint16_t	reserved1;	/*	0x0A	Reserved								*/
			uint16_t	ctrl1b;		/* 	0x0C	Write transducer 1 control register B	
												Bits 15-10	SSI data length
												Bits 9-0 	SSI clock divider		*/
			uint16_t	ctrl2b;		/* 	0x0E	Write transducer 2 control register	B	
												Bits 15-10	SSI data length
												Bits 9-0 	SSI clock divider		*/
			uint16_t	reserved2;	/* 	0x10	Reserved   								*/
			uint16_t	reserved3;	/* 	0x12	Reserved   								*/
			uint16_t	reserved4;	/* 	0x14	Reserved   								*/
			uint16_t	reserved5;	/* 	0x16	Reserved   								*/
			uint16_t	reserved6;	/* 	0x18	Reserved   								*/
			uint16_t	reserved7;	/* 	0x1A	Reserved   								*/
			uint16_t	reserved8; 	/* 	0x1C	Reserved   								*/
			uint16_t	reserved9; 	/* 	0x1E	Reserved   								*/
			uint16_t	reserved10;	/*	0x20	Reserved               					*/
			uint16_t	reserved11;	/*	0x22	Reserved               					*/
			uint16_t	leds;		/*	0x24	Write LED outputs
												Bit 0	Channel 0 LED
												Bit 1	Channel 1 LED				*/
			uint16_t	teds1;		/*	0x26	Write TEDS1 output 						
												Bits 2-0	Command request
												Bit	 3		Reset 1-wire controller	*/
			uint16_t	teds2;		/*	0x28	Write TEDS2 output
												Bits 2-0	Command request
												Bit	 3		Reset 1-wire controller	*/
			uint16_t	reserved12;	/*	0x2A	Reserved   								*/
			uint16_t	reserved13;	/*	0x2C	Reserved   								*/
			uint16_t	reserved14;	/*	0x2E	Reserved   								*/
			uint16_t	reserved15;	/*	0x30	Reserved                              	*/
			uint16_t	reserved16;	/*	0x32	Reserved                           		*/
			uint16_t	reserved17;	/*	0x34	Reserved                               	*/
			uint16_t	reserved18;	/*	0x36	Reserved                               	*/
			uint16_t	reserved19;	/*	0x38	Reserved                               	*/
			uint16_t	reserved20;	/*	0x3A	Reserved								*/
			uint16_t	test;		/*	0x3C	Write test register						*/
			uint16_t	reserved21;	/*	0x3E	Reserved								*/
		} wr;
		struct {
			uint16_t	ctrl1a;		/* 	0x00	Read transducer 1 control register A	*/
			uint16_t	ctrl2a;		/* 	0x02	Read transducer 2 control register	A	*/
			uint16_t	reserved0;	/*  0x04	Reserved								*/
			uint16_t	reserved1;	/*  0x06	Reserved								*/
			uint16_t	reserved2;	/*  0x08	Reserved								*/
			uint16_t	reserved3;	/*  0x0A	Reserved								*/
			uint16_t	ctrl1b;		/* 	0x0C	Read transducer 1 control register B	*/
			uint16_t	ctrl2b;		/* 	0x0E	Read transducer 2 control register B	*/
			int		txdr1;		/*	0x10	Read transducer 1 result				*/
			int		txdr2;		/*	0x14	Read transducer 2 result				*/
			uint16_t	reserved4;	/*	0x18	Reserved								*/
			uint16_t	reserved5;	/*	0x1A	Reserved								*/
			uint16_t	reserved6; 	/*	0x1C	Reserved								*/
			uint16_t	reserved7; 	/*	0x1E	Reserved								*/
			uint16_t	reserved8; 	/*	0x20	Reserved								*/
			uint16_t	reserved9; 	/*	0x22	Reserved								*/
			uint16_t	reserved10;	/*	0x24	Reserved								*/
			uint16_t	teds1;		/*	0x26	Read TEDS1 input						*/
			uint16_t	teds2;		/*	0x28	Read TEDS2 input						*/
			uint16_t	fault;		/*	0x2A	Read fault inputs						
											 Bit 0	UAS_1 fault (1 = fault)
											 Bit 3	UAS_2 fault (1 = fault)			*/
			uint16_t	rawip;		/*	0x2C	Read raw inputs
											 Bit 0	UA1_1 input
											 Bit 1	UA2_1 input
											 Bit 2	UA0_1 input
											 Bit 3	UAS_1 input
											 Bit 4	UA1_2 input
											 Bit 5	UA2_2 input
											 Bit 6	UA0_2 input
											 Bit 7	UAS_2 input						*/
			uint16_t	reserved12;	/*	0x2E	Reserved								*/
			uint16_t	reserved13;	/*	0x30	Reserved								*/
			uint16_t	reserved14;	/*	0x32	Reserved								*/
			uint16_t	reserved15;	/*	0x34	Reserved								*/
			uint16_t	reserved16;	/*	0x36	Reserved								*/
			uint16_t	reserved17;	/*	0x38	Reserved								*/
			uint16_t	reserved18;	/*	0x3A	Reserved								*/
			uint16_t	test;		/*  0x3C	Read test register						*/
			uint16_t	ident;		/*  0x3E	Read identifier register				*/
		} rd;
	} u;
} hw_2dig;

/* Hardware flags */

//#define DIGTXDR_TYPEMASK	0x8000

/* Definition of the 2DIG specific information structure contained within
   the generic channel definition structure (see channel.h).
*/

#define WSIZE_2DIG	1		/* Workspace size */

/* Information structure for 2DIG card

   The first 5 entries of this structure must match with those in the
   2GP1SV and MBOARD information structures.
*/

typedef struct info_2dig {
	hw_2dig *hw;				/* Hardware base address							*/
	int   input;				/* Input channel number on 2DIG card				*/
	int   output;				/* Output channel number on 2DIG card				*/
	uint8_t  ledmask;				/* Mask for LED access								*/
	uint8_t *ledstate;				/* Copy of LED state outputs						*/
	uint8_t  work[WSIZE_2DIG];		/* Local workspace									*/
} info_2dig;

/* Incomplete declaration of chandef to allow function prototypes to work */

typedef struct chandef chandef;


/* Function prototypes */

uint32_t hw_2dig_install(int slot, int *numinchan, int *numoutchan, int *nummiscchan, 
				      hw_2dig *hw, int flags);
uint32_t hw_2dig_post_install(int slot, hw_2dig *hw, int flags);
void hw_2dig_save(int slot, uint32_t status);
void hw_2dig_ctr_reset(chandef* def);

#endif
