/********************************************************************************
 * MODULE NAME       : cnet.h													*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : CNet related constants, variables and function 			*
 *					   prototypes												*
 ********************************************************************************/

#ifndef CNET_H
#define CNET_H

#include <stdint.h>

#include "porting.h"
#include "channel.h"
#include "mock_cnet.h"

#define CNET_MODE_DISABLE			0
#define CNET_MODE_ENABLE			1

#define CNET_MODE_SLAVE				0
#define CNET_MODE_MASTER			1
#define CNET_MODE_STANDALONE		2
#define CNET_MODE_UNDEFINED			0xFFFFFFFF

#define CNET_STATE_NONE				0			/* No CNet 									*/
#define CNET_STATE_NORMAL			1			/* CNet operating normally					*/
#define CNET_STATE_MASTERINTEGRITY	2			/* Master waiting for integrity packets 	*/
#define CNET_STATE_SLAVEINTEGRITY	3			/* Slave waiting for integrity packets 		*/
#define CNET_STATE_RECONFIG			4			/* CNet being reconfigured					*/

#define CNET_STAT_INCONN			0x01
#define CNET_STAT_OUTCONN			0x02
#define CNET_STAT_LOCK				0x04
#define CNET_STAT_INTEGRITY			0x08

#define CNET_ASYNC_BROADCAST		0xFF		/* CNet async comms broadcast address 		*/
#define CNET_ASYNC_LOCAL			0xFE		/* CNet async comms local address	  		*/

/* Flags controlling CNet direction */

#define CNET_INPUT					0x00
#define CNET_OUTPUT					0x01

/* Flags controlling CNet mapping */

#define CNET_NOMAP					0x00
#define CNET_MAP					0x02

/* Flags in CNet INTCTRL register */

#define CNET_INTEN					0x0001		/* DSP A CNet interrupt enable				*/
#define CNET_INTENB					0x0002		/* DSP B CNet interrupt enable				*/

/* Define fixed physical data slot positions

   See Limit.h for equivalent definitions for HardwareIndependent code

*/

#define CNET_SYSTEM_BASE			229			/* Start of CNet system usage				*/

#define CNET_DATASLOT_GENSTAT		229			/* Generator status							*/
#define CNET_DATASLOT_FLAGS			230			/* Miscellaneous flags							
													Bits 0 - 29		Cubic engine flags
													Bits 30,31		Realtime status			
													
													Bit 30 RT_OUTPUT enabled				*/

#define CNET_DATASLOT_ENCODER		231			/* Encoder counter delta value				*/
#define CNET_DATASLOT_GRPSTATUS		232			/* Group status								*/

#define CNET_DATASLOT_TSR0			233			/* TSR word 0   (Flags)						*/
#define CNET_DATASLOT_TSR0_EXT		234			/* TSR word 0a	(Extended flags)			*/
#define CNET_DATASLOT_TSR1			235			/* TSR word 1								*/
#define CNET_DATASLOT_TSR2			236			/* TSR word 2								*/
#define CNET_DATASLOT_TSR3			237			/* TSR word 3								*/
#define CNET_DATASLOT_TSR4			238			/* TSR word 4								*/
#define CNET_DATASLOT_TSR5			239			/* TSR word 5								*/
#define CNET_DATASLOT_TSR6			240			/* TSR word 6								*/

#define CNET_DATASLOT_HYDREQ		241			/* Hydraulic request						*/
#define CNET_DATASLOT_HYDSTATUS		242			/* Hydraulic status							*/

#define CNET_DATASLOT_ASYNCCTRL		243			/* Async comms control word					*/
#define CNET_DATASLOT_ASYNCDATA		244			/* Start of async comms data block			*/
#define CNET_DATASLOT_WATCHDOG		252			/* Watchdog word							*/
#define CNET_DATASLOT_TIMELO		253			/* Timestamp low word						*/
#define CNET_DATASLOT_TIMEHI		254			/* Timestamp high word						*/
#define CNET_DATASLOT_INTEGRITY		255			/* Link integrity							*/

/* Flags in global action request word */

#define CNET_GLOBALACT_STOPRT	0x00000001		/* Stop realtime transfer					*/
#define CNET_GLOBALACT_PAUSERT	0x00000002		/* Pause realtime transfer					*/

#if 0
#define CNET_MEM_READ	cnet_mem_data
#define CNET_MEM_WRITE	cnet_mem_data
#else
#define CNET_MEM_READ	mock_cnet_controller->cnet_mem_data_in
#define CNET_MEM_WRITE	mock_cnet_controller->cnet_mem_data_out
#endif

typedef struct cnetslot {
	chandef *chan;
	uint32_t	 dirn;
} cnetslot;

typedef struct cnet_msg {
  uint8_t     dst;		/* Destination for outgoing message					*/
  uint8_t     *txbuf;	/* Pointer to start of outgoing message/reply 		*/
  uint32_t    txsize;	/* Size of outgoing message/reply					*/
  uint8_t     *rxbuf;	/* Pointer to start of incoming message/reply 		*/
  uint32_t    rxsize;	/* Size of incoming message/reply					*/
  uint32_t    iprdy;	/* Flag indicating incoming message available		*/
  uint32_t    oprdy;	/* Flag indicating outgoing message available		*/
  uint32_t	   noreply;	/* Flag indicating no reply required				*/
} cnet_msg;

/* Define return codes from CNet message handler */

#define CNET_COMPLETE		0xFFFFFFFF	/* Message transfer complete			*/


/********************************************************************************
 CNet hardware registers
********************************************************************************/

extern volatile uint32_t * cnet_mem_data;	/* CNet data memory				*/
extern volatile uint16_t * cnet_mem_ctrl;	/* CNet control memory			*/

/********************************************************************************
 Global CNet control variables
********************************************************************************/

extern uint32_t cnet_mode;						/* CNet operating mode					*/
extern uint32_t cnet_configured;				/* CNet configuration complete			*/
extern uint8_t  cnet_localid;					/* CNet local ID 						*/
extern uint32_t cnet_ringsize;					/* CNet ring size						*/
extern uint32_t cnet_local_addr;				/* Local address for CNet async comms	*/
extern uint32_t cnet_copy_data[256];			/* Copy of CNet data memory				*/
extern float cnet_stop_rate[256];			/* CNet stop ramp rates					*/
extern float cnet_stop_target[256];			/* CNet stop target values				*/
extern uint32_t cnet_stop_time[256];			/* CNet stop transition time			*/
extern uint32_t cnet_stop_tcounter[256];		/* CNet stop time counter   			*/
extern uint32_t cnet_stop_type;				/* CNet stop type
												0 = Constant rate
												1 = Constant time					*/
extern uint32_t cnet_usr_maptable[256];		/* CNet user map table					*/
extern uint32_t cnet_sys_timeslot[400];		/* CNet system timeslot usage			*/
extern uint32_t cnet_usr_timeslot[400];		/* CNet user timeslot usage				*/

extern cnet_msg cnet_rx_msg;				/* Incoming message/reply data			*/

extern uint32_t cnet_time_lo;					/* Timestamp low word					*/
extern uint32_t cnet_time_hi;					/* Timestamp high word					*/
extern uint32_t cnet_interrupt_ctr;			/* CNet interrupt counter (local time)	*/
extern uint32_t cnet_retry_count;				/* CNet async comms retry counter		*/
extern uint32_t cnet_collision_count;			/* CNet async comms collision counter	*/
extern uint32_t cnet_timeout_count;			/* CNet async comms timeout counter		*/
extern uint32_t cnet_wdog_count;				/* CNet watchdog failure counter		*/

/********************************************************************************
 Global action control variables
********************************************************************************/

extern uint32_t near cnet_glbl_actreq;			/* Global action request				*/
extern uint32_t near cnet_glbl_actmask;		/* Global action mask					*/

/********************************************************************************
 Global realtime streaming control variables
********************************************************************************/

extern uint32_t near rt_glblreq_stop_flags;	/* Flags controlling global req stop	*/
extern uint32_t near rt_glblreq_pause_flags;	/* Flags controlling global req pause	*/
extern uint32_t near rt_control;				/* Flags controlling RT operation		*/

/********************************************************************************
 Function prototypes
********************************************************************************/

void  cnet_initialise(void);
void cnet_interrupt(void);
uint32_t cnet_int_ctrl(uint32_t set, uint32_t clr);
void  cnet_read_mode(uint32_t *enable, uint32_t *master, uint32_t *localid);
void  cnet_setsync(int enable, int count);
void  cnet_rdstat(uint32_t *status, uint32_t *in, uint32_t *out, uint32_t *lock, uint32_t *integrity, uint32_t *fault,
	 			  uint32_t *chksum, uint32_t *wdog, uint32_t *ts_lo, uint32_t *ts_hi, uint32_t *mode, uint32_t *position,
				  uint32_t *ringsize);
void  cnet_rderr(uint8_t *data, uint32_t *chksum);
void  cnet_generr(uint32_t slot, uint32_t flags, uint32_t data);
void cnet_logpkt(uint32_t slot, uint8_t *data, uint8_t *chksum);
void cnet_debug(uint32_t timeslot, uint32_t dataslot, float min, float max, uint8_t *data_a, uint8_t *data_b, uint32_t *recorded);
uint32_t cnet_rdwdog(void);
void  cnet_get_info(uint32_t *ringsize, uint32_t *position);
int  *cnet_get_slotaddr(uint32_t slot, uint32_t proc);
float cnet_get_slotdata(uint32_t slot);
void  cnet_set_timeout(uint32_t timeout);
uint32_t cnet_read_timeout(void);
void  cnet_read_list(int *size, int *list);
void  cnet_status_update(uint32_t reconfig, uint32_t new_master);
void  cnet_monitor(void);
uint32_t cnet_mark_sys_slot(uint32_t src, uint32_t timeslot, uint32_t type);
uint32_t cnet_find_sys_slot(uint32_t src, uint32_t timeslot, uint32_t type, uint32_t *slot);
uint32_t cnet_config_map(uint32_t *map);
void  cnet_async_handler(uint32_t connstat);
void  cnet_read_timestamp(uint32_t *lo, uint32_t *hi);
int   cnet_async_transmit(uint8_t *msg, uint8_t *reply, uint32_t msglength, uint32_t *replylength, uint32_t noreply);




#endif
