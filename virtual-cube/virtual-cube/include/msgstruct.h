/********************************************************************************
 * MODULE NAME       : msgstruct.h												*
 * PROJECT		     : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Message structure definitions							*
 ********************************************************************************/

#include <stdint.h>

#define MSGPORT		49152	/* TCP port for standard message communication	*/
#define RTPORT		49153	/* TCP port for realtime message communication	*/

#define MSGINFO_MIN	0x0001	/* Flag for reading minimum values				*/
#define MSGINFO_MAX	0x0002	/* Flag for reading maximum values				*/

#define NAMELEN		32		/* Standard length for name strings				*/

/* The generic message structure below is used when checking for
   an error response in a message where a response is not expected.
   The generic message structure is used when no response type is
   available.
*/

struct msg_generic_message {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint8_t  end;
};


/* Message codes 1 to 255 are reserved for returned error messages.
   Note that all error message structures must have the pos element
   (position within packet) immediately following the command code.
*/

#define MSG_ERROR_ILLEGAL_MESSAGE 0x0001

struct msg_illegal_message {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint8_t  end;
};

#define MSG_ERROR_ILLEGAL_CHANNEL 0x0002

struct msg_illegal_channel {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint32_t chan;			/* Illegal channel									*/
  uint8_t  end;
};

#define MSG_ERROR_ILLEGAL_FUNCTION 0x0003

struct msg_illegal_function {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint8_t  end;
};

#define MSG_ERROR_OPERATION_FAILED 0x0004

struct msg_operation_failed {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint32_t reason;			/* Reason code										*/
  uint8_t  end;
};

#define MSG_ERROR_ILLEGAL_PARAMETER 0x0005

struct msg_illegal_parameter {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint8_t  end;
};

#define MSG_ERROR_CNET_TIMEOUT 0x0006

struct msg_cnet_timeout {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint8_t  end;
};

#define MSG_ERROR_ILLEGAL_DESTINATION 0x0007

struct msg_illegal_destination {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint8_t	dst;			/* Destination										*/
  uint8_t  end;
};

#define MSG_ERROR_NO_STREAM 0x0008

struct msg_no_stream {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint8_t  end;
};

#define MSG_ERROR_TOO_LONG 0x0009

struct msg_too_long {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint8_t  end;
};

#define MSG_ERROR_FW_CORRUPT 0x000A

struct msg_fw_corrupt {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint8_t  end;
};

#define MSG_ERROR_SECURITY_LEVEL 0x000B

struct msg_security_level {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint8_t  end;
};

#define MSG_ERROR_SEMAPHORE_DENIED 0x000C

struct msg_semaphore_denied {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint16_t	pos;			/* Position within packet							*/
  uint8_t  end;
};

#define MSG_ERROR_WINSOCK_VER 0x000D 	/* Used by the DLL to indicate an invalid
								  		   Winsock version.
							   			*/

/* Message codes 256 to 511 are reserved for messages supported by
   the loader. These must also be supported by the standard
   firmware.
*/

#define MSG_READ_FIRMWARE_VERSION 0x0100

struct msg_read_firmware_version {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint8_t  end;
};

#define MSG_RETURN_FIRMWARE_VERSION 0x0101

struct msg_return_firmware_version {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t version;		/* Firmware version number							*/
  uint8_t  end;
};

#define MSG_READ_FIRMWARE_INFORMATION 0x0134

struct msg_read_firmware_information {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint8_t  end;
};

#define MSG_RETURN_FIRMWARE_INFORMATION 0x0135

struct msg_return_firmware_information {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t version;		/* Firmware version number							*/
  char  date[32];		/* Build date										*/
  char  time[32];		/* Build time										*/
  char	info[128];		/* General information string						*/
  uint8_t  end;
};

#define MSG_READ_DEVICE_TYPE 0x0102

struct msg_read_device_type {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_DEVICE_TYPE 0x0103

struct msg_return_device_type {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	device;			/* Device type
  							0 = None
  							1 = Control Cube
  							2 = Signal Conditioning							*/
  uint8_t  end;
};

#define MSG_ERASE_FLASH 0x0104

struct msg_erase_flash {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint8_t	end;
};

#define MSG_READ_ERASE_STATUS 0x0105

struct msg_read_erase_status {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint8_t	end;
};

#define MSG_RETURN_ERASE_STATUS 0x0106

struct msg_return_erase_status {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t status;			/* Erase status:
  							0 = Busy
  							1 = Complete
  							2 = Error (erase failed)						*/
  uint8_t	end;
};

#define MSG_PROG_FLASH 0x0107

struct msg_prog_flash {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t start;			/* Offset of memory area within main flash			*/
  uint32_t length;			/* Length of memory area							*/
  uint8_t	data[1];		/* Data area										*/
  uint8_t	end;
};

#define MSG_RETURN_PROG_STATUS 0x0108

struct msg_return_prog_status {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  status;			/* Program status:
  							0 = Complete
  							2 = Error (program failed)						*/
  uint8_t	end;
};

#define MSG_ERASE_FLASH_BLOCK 0x0109

struct msg_erase_flash_block {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint32_t block;			/* Block number										*/
  uint8_t	end;
};

#define MSG_READ_CNET_INFO 0x010A

struct msg_read_cnet_info {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint8_t	end;
};

#define MSG_RETURN_CNET_INFO 0x010B

struct msg_return_cnet_info {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t status;			/* CNet status
  							 Bits 0-7 CNet mode
  							   0 = Master
  							   1 = Slave
  							   2 = Standalone
  							 Bit 8
  							   0 = Not configured
  							   1 = Configured								*/
  uint32_t ringsize;		/* CNet ring size									*/
  uint32_t local_addr;		/* Local CNet address								*/
  uint8_t	end;
};

#define MSG_SET_IP_ADDRESS 0x010C

struct msg_set_ip_address {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t source;			/* Source for IP address
  							0 = Manual
  							1 = DHCP
  							2 = BOOTP (reserved)							*/
  uint32_t update;			/* Delay before update (0.1s increments)
  							0 = No update									*/
  uint8_t  ipaddr[4];		/* IP address 										*/
  uint8_t  subnetmask[4];	/* Subnetmask										*/
  uint8_t  end;
};

#define MSG_READ_IP_ADDRESS 0x010D

struct msg_read_ip_address {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_IP_ADDRESS 0x010E

struct msg_return_ip_address {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t source;			/* Source for IP address
  							0 = Manual
  							1 = DHCP
  							2 = BOOTP										*/
  uint8_t  ipaddr[4];		/* IP address 										*/
  uint8_t  subnetmask[4];	/* Subnetmask										*/
  uint8_t  end;
};

#define MSG_READ_MAC_ADDRESS 0x0132

struct msg_read_mac_address {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_MAC_ADDRESS 0x0133

struct msg_return_mac_address {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  macaddr[6];		/* MAC address										*/
  uint8_t  end;
};

#define MSG_HW_RESTART 0x010F

struct msg_hw_restart {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t delay;			/* Delay before restart (0.1s increments)
  							0 = No restart									*/
  uint8_t  end;
};

#define MSG_FW_VALIDATE 0x0110

struct msg_fw_validate {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_FW_VALIDATE 0x0111

struct msg_return_fw_validate {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t state;			/* Firmware status
  							0 = Valid
  							1 = Invalid										*/
  uint8_t  end;
};

#define MSG_TESTMODE_SET_ADDRESS 0x0112

struct msg_testmode_set_address {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t address;		/* Address for read/write operation					*/
  uint8_t  end;
};

#define MSG_TESTMODE_WRITE8 0x0113

struct msg_testmode_write8 {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  data;			/* Data to write									*/
  uint8_t  end;
};

#define MSG_TESTMODE_WRITE16 0x0114

struct msg_testmode_write16 {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint16_t data;			/* Data to write									*/
  uint8_t  end;
};

#define MSG_TESTMODE_WRITE32 0x0115

struct msg_testmode_write32 {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t data;			/* Data to write									*/
  uint8_t  end;
};

#define MSG_TESTMODE_READ8 0x0116

struct msg_testmode_read8 {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_RETURN_READ8 0x0117

struct msg_testmode_return_read8 {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  data;			/* Data												*/
  uint8_t  end;
};

#define MSG_TESTMODE_READ16 0x0118

struct msg_testmode_read16 {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_RETURN_READ16 0x0119

struct msg_testmode_return_read16 {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint16_t data;			/* Data												*/
  uint8_t  end;
};

#define MSG_TESTMODE_READ32 0x011A

struct msg_testmode_read32 {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_RETURN_READ32 0x011B

struct msg_testmode_return_read32 {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t data;			/* Data												*/
  uint8_t  end;
};

#define MSG_TESTMODE_I2C_START 0x011C

struct msg_testmode_i2c_start {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_I2C_RESTART 0x011D

struct msg_testmode_i2c_restart {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_I2C_STOP 0x011E

struct msg_testmode_i2c_stop {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_I2C_SEND 0x011F

struct msg_testmode_i2c_send {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  data;			/* Data												*/
  uint8_t  end;
};

#define MSG_TESTMODE_RETURN_I2C_SEND 0x0120

struct msg_testmode_return_i2c_send {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  ack;			/* Acknowledge flag									*/
  uint8_t  end;
};

#define MSG_TESTMODE_I2C_READ 0x0121

struct msg_testmode_i2c_read {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  ack;			/* Acknowledge flag									*/
  uint8_t  end;
};

#define MSG_TESTMODE_RETURN_I2C_READ 0x0122

struct msg_testmode_return_i2c_read {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  data;			/* Data												*/
  uint8_t  end;
};

#define MSG_TESTMODE_DSPB_I2C_START 0x0123

struct msg_testmode_dspb_i2c_start {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_DSPB_I2C_RESTART 0x0124

struct msg_testmode_dspb_i2c_restart {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_DSPB_I2C_STOP 0x0125

struct msg_testmode_dspb_i2c_stop {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_DSPB_I2C_SEND 0x0126

struct msg_testmode_dspb_i2c_send {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  data;			/* Data												*/
  uint8_t  end;
};

#define MSG_TESTMODE_RETURN_DSPB_I2C_SEND 0x0127

struct msg_testmode_return_dspb_i2c_send {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  ack;			/* Acknowledge flag									*/
  uint8_t  end;
};

#define MSG_TESTMODE_DSPB_I2C_READ 0x0128

struct msg_testmode_dspb_i2c_read {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  ack;			/* Acknowledge flag									*/
  uint8_t  end;
};

#define MSG_TESTMODE_RETURN_DSPB_I2C_READ 0x0129

struct msg_testmode_return_dspb_i2c_read {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  data;			/* Data												*/
  uint8_t  end;
};

#define MSG_TESTMODE_ONEW_INIT 0x012A

struct msg_testmode_onew_init {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t control;		/* Control register address							*/
  uint8_t  end;
};

#define MSG_TESTMODE_ONEW_RESET 0x012B

struct msg_testmode_onew_reset {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t control;		/* Control register address							*/
  uint8_t  end;
};

#define MSG_TESTMODE_RETURN_ONEW_RESET 0x012C

struct msg_testmode_return_onew_reset {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  presence;		/* One-wire presence flag							*/
  uint8_t  end;
};

#define MSG_TESTMODE_ONEW_SEND 0x012D

struct msg_testmode_onew_send {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t control;		/* Control register address							*/
  uint8_t  data;			/* Data to write									*/
  uint8_t  end;
};

#define MSG_TESTMODE_ONEW_READ 0x012E

struct msg_testmode_onew_read {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t control;		/* Control register address							*/
  uint8_t  end;
};

#define MSG_TESTMODE_RETURN_ONEW_READ 0x012F

struct msg_testmode_return_onew_read {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  data;			/* Data												*/
  uint8_t  end;
};

#define MSG_TESTMODE_ENABLE_WATCHDOG 0x0130

struct msg_testmode_enable_watchdog {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_DISABLE_WATCHDOG 0x0131

struct msg_testmode_disable_watchdog {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_WRITE32B 0x0136

struct msg_testmode_write32b {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t data;			/* Data to write									*/
  uint8_t  end;
};

#define MSG_TESTMODE_READ32B 0x0137

struct msg_testmode_read32b {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_TESTMODE_RETURN_READ32B 0x0138

struct msg_testmode_return_read32b {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t data;			/* Data												*/
  uint8_t  end;
};

/* Message codes in the range 0x0200 to 0x02FF are reserved for
   use by parameter class messages.
*/

#define MSG_SEND_PARAMETER_STRING 0x020B

struct msg_send_parameter_string {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  char  string[256];	/* Message string									*/
  uint8_t  end;
};

#define MSG_RETURN_PARAMETER_STRING 0x020C

struct msg_return_parameter_string {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  char  string[256];	/* Response string									*/
  uint8_t  end;
};
 
/* Message codes 0x0300 onwards are available for the standard
   firmware messages.
*/

/********************************************************************************************************
 * Low-level system control messages																	*
 ********************************************************************************************************/

#define MSG_DEFINE_COMM_TIMEOUT 0x036E /* Operator */

struct msg_define_comm_timeout {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t timeout;        /* Comms timeout in csec (0 = disabled)				*/
  uint8_t  end;
};

#define MSG_READ_COMM_TIMEOUT 0x036F

struct msg_read_comm_timeout {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_COMM_TIMEOUT 0x0370

struct msg_return_comm_timeout {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t timeout;        /* Comms timeout in csec (0 = disabled)				*/
  uint8_t  end;
};

#define MSG_DEFINE_COMM_TIMEOUT_EXT 0x041C /* Operator */

struct msg_define_comm_timeout_ext {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t timeout;        /* Comms timeout in csec (0 = disabled)				*/
  uint32_t action;			/* Comms timeout action								*/
  uint8_t  end;
};

#define MSG_READ_COMM_TIMEOUT_EXT 0x041D

struct msg_read_comm_timeout_ext {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_COMM_TIMEOUT_EXT 0x041E

struct msg_return_comm_timeout_ext {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t timeout;        /* Comms timeout in csec (0 = disabled)				*/
  uint32_t action;			/* Comms timeout action								*/
  uint8_t  end;
};

#define MSG_SET_COMM_CLOSING 0x041F /* Operator */

struct msg_set_comm_closing {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t reserved;		/* Reserved for future use							*/
  uint8_t  end;
};

#define MSG_READ_COMM_ID 0x0420

struct msg_read_comm_id {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_COMM_ID 0x0421

struct msg_return_comm_id {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t id;        		/* Comms channel identifier							*/
  uint8_t  end;
};

#define MSG_FORCE_COMM_CLOSE 0x0422 /* Operator */

struct msg_force_comm_close {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t id;				/* Comms channel identifier							*/
  uint32_t reserved;		/* Reserved for future use							*/
  uint8_t  end;
};

#define MSG_READ_NV_STATE 0x03A2 /* Operator */

struct msg_read_nv_state {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t	end;
};

#define MSG_RETURN_NV_STATE 0x03A3

struct msg_return_nv_state {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	state;			/* Non-volatile state
							Bit 0		Corrupt configuration save list
							Bit 1		Old format configuration list
							Bit 2		Corrupt channel configuration
							Bit 3		Corrupt channel calibration
							Bit 4		SNV bank 1 corrupt
							Bit 5		SNV bank 2 corrupt
							Bit 6		DNV bank 1 corrupt
							Bit 7		DNV bank 2 corrupt
							Bit 8		SNV area corrupt
							Bit 9		SNV area resized
							Bit 10		DNV area corrupt
							Bit 11		DNV area resized
							Bit 12		SNV update busy
							Bit 13		DNV update busy
							Bit 14		SNV current update bank (0/1)
							Bit 15		DNV current update bank (0/1)
  							Bits 16..31	Reserved							*/
  uint8_t	end;
};

#define MSG_NV_CONTROL 0x03A7 /* Administrator */

struct msg_nv_control {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t ctrl;			/* Control word:
  							0 = Clear selected flags
  							1 = Set selected flags							*/
  uint32_t flags;			/* Flags											*/
  uint8_t  end;  							
};

#define MSG_CHAN_OVERRIDE 0x03B9 /* Administrator */

struct msg_chan_override {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  uint8_t	end;
};

#define MSG_SET_SECURITY_LEVEL 0x03CE /* Operator */

struct msg_set_security_level {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t security;		/* Security level									*/
  uint8_t  end;
};

#define MSG_READ_SECURITY_LEVEL 0x03CF /* Operator */

struct msg_read_security_level {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_SECURITY_LEVEL 0x03D0

struct msg_return_security_level {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t security;		/* Security level									*/
  uint8_t  end;
};

#define MSG_SET_SEMAPHORE 0x03F5 /* Operator */

struct msg_set_semaphore {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t semaphore;		/* New semaphore state								*/
  uint8_t  end;
};

#define MSG_READ_SEMAPHORE 0x03F6 /* Operator */

struct msg_read_semaphore {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_SEMAPHORE 0x03F7

struct msg_return_semaphore {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t semaphore;		/* Current semaphore state							*/
  uint8_t  end;
};

#define MSG_SET_FAULT_MASK 0x0402 /* Administrator */

struct msg_set_fault_mask {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel number									*/
  uint32_t mask;			/* New mask state									*/
  uint8_t  end;
};

#define MSG_READ_FAULT_STATE 0x0403 /* Operator */

struct msg_read_fault_state {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel number									*/
  uint8_t  end;
};

#define MSG_RETURN_FAULT_STATE 0x0404

struct msg_return_fault_state {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel number									*/
  uint32_t capabilities;	/* Fault detection capabilities						*/
  uint32_t mask;			/* Fault mask										*/
  uint32_t state;			/* Current fault state								*/
  uint8_t  end;
};

#define MSG_READ_GUARD_STATE 0x0405 /* Operator */

struct msg_read_guard_state {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_GUARD_STATE 0x0406

struct msg_return_guard_state {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t state;			/* Current guard state
							0 = Guard input not supported
							1 = Guard open
							2 = Guard closed
							3 = Guard fault detected						*/
  uint8_t  end;
};

#define MSG_READ_RUNTIME 0x041A /* Operator */

struct msg_read_runtime {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_RUNTIME 0x041B

struct msg_return_runtime {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t runtime_lo;		/* Runtime counter (0.1s units) low word 			*/
  uint32_t runtime_hi;		/* Runtime counter (0.1s units) high word 			*/
  uint32_t reserved1;		/* Reserved for future use							*/
  uint32_t reserved2;		/* Reserved for future use							*/
  uint8_t  end;
};



/********************************************************************************************************
 * System configuration messages																		*
 ********************************************************************************************************/

#define MSG_SET_SERNO 0x03A1 /* Administrator */

struct msg_set_serno {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  char  serno[8];		/* Serial number string								*/
  uint8_t  end;
};

#define MSG_READ_SERNO 0x0355 /* Operator */

struct msg_read_serno {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_SERNO 0x0356

struct msg_return_serno {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  char  serno[8];		/* Serial number string								*/
  uint8_t  end;
};

#define MSG_DEFINE_SYSTEM_NAME 0x036B /* Calibrator */

struct msg_define_system_name {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  char  name[32];       /* System name (zero terminated)   		    		*/
  uint8_t  end;
};

#define MSG_READ_SYSTEM_NAME 0x036C /* Operator */

struct msg_read_system_name {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_SYSTEM_NAME 0x036D

struct msg_return_system_name {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  char  name[32];       /* System name (zero terminated)      		 		*/
  uint8_t  end;
};

#define MSG_DEFINE_RIG_NAME 0x03BA /* Calibrator */

struct msg_define_rig_name {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  char  name[32];       /* Rig name (zero terminated)   		    		*/
  uint8_t  end;
};

#define MSG_READ_RIG_NAME 0x03BB /* Operator */

struct msg_read_rig_name {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_RIG_NAME 0x03BC

struct msg_return_rig_name {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  char  name[32];       /* Rig name (zero terminated)      			 		*/
  uint8_t  end;
};

#define MSG_SET_PANEL_CONFIG 0x03A4 /* Administrator */

struct msg_set_panel_config {
  uint16_t	command;		/* Command code										*/
  uint16_t pad1;			/* Padding to maintain alignment					*/
  uint8_t  paneltable[31];	/* Panel configuration array						*/
  uint8_t  pad2;			/* Padding											*/
  uint8_t	end;

  /* Offsets 28, 29 and 30 in the panel configuration array are used to
     control mapping of SV channels to output DACs.

	 For the mapping to work, all three bytes must be identical and 
	 configured as described below. Any other state, or the default
	 state where all three bytes are zero, will cause the default mapping
	 of SV1 = DAC0, SV2 = DAC1, etc. to be used.

	 paneltable[28] is split into 4 lots of 2 bits. Such that (panelBuf[28] & 0x03) corresponds to output 0. The bits contain an offset to apply.
	 For example, (panelBuf[28] & 0xC0) corresponds to output 3. If the output need to be changed to PID output 2 then panelBuf should be B11xxxxxx.

	 panelBuf[28] = BWW XX YY ZZ. WW is the offset to apply to output 3, XX is the offset to apply to output 2, YY to output 1, ZZ to output 0.

	 If panelBuf[28] = B00 00 00 00, then the physical output will look at the same number PID output. (this is to keep the functionality the same as it has been).
		physical output		PID output
			output0------>SVdrive0
			output1------>SVdrive1
			output2------>SVdrive2
			output3------>SVdrive3

	 If panelBuf[28] = B00 01 10 11, then the physical output will be linked to the PID output as shown below (all 4 physical outputs connected to PID output 3).
		physical output		PID output
			output0-\     SVdrive0
			output1--\    SVdrive1
			output2---\   SVdrive2
			output3------>SVdrive3

	 If panelBuf[28] = B10 11 11 00, then the physical output will be linked to the PID output as shown below.
		physical output 0 and 1 connect to PID output 0. physical output 2 and 3 connect to PID output 1.
		physical output		PID output
			output0------>SVdrive0
			output1-/  /->SVdrive1
			output2---/   SVdrive2
			output3--/    SVdrive3
  */

};

#define MSG_READ_PANEL_CONFIG 0x03A5 /* Operator */

struct msg_read_panel_config {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t	end;
};

#define MSG_RETURN_PANEL_CONFIG 0x03A6

struct msg_return_panel_config {
  uint16_t	command;		/* Command code										*/
  uint16_t pad1;			/* Padding to maintain alignment					*/
  uint8_t  paneltable[31];	/* Panel configuration array						*/
  uint8_t  pad2;			/* Padding											*/
  uint8_t	end;
};

#define MSG_READ_CHAN_MEMPTRS 0x0394 /* Operator */

struct msg_read_chan_memptrs {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									
  							0 - 255 	Input channels
  							256 - 511	Output channels
  							512 - 767	Miscellaneous channels
  							768 - 1023	CNet slot
  										(Addresses returned for CNet slot
  										 are for DSPA, DSPB)				*/
  uint8_t	end;
};

#define MSG_RETURN_CHAN_MEMPTRS 0x0395

struct msg_return_chan_memptrs {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  uint32_t value;			/* Address of unfiltered input/output data			*/
  uint32_t filtered;		/* Address of filtered data (input chan only )		
  						   (for CNet slots this is the DSPB address			*/
  uint8_t	end;
};

#define MSG_SET_USERDATA 0x03DA /* Operator */

struct msg_set_userdata {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	start;			/* Start position within data area					*/
  uint32_t length;			/* Length of data to write							*/
  uint8_t  data[1];		/* Data block (variable length)						*/
  uint8_t	end;
};

#define MSG_READ_USERDATA 0x03DB /* Operator */

struct msg_read_userdata {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	start;			/* Start position within data area					*/
  uint32_t length;			/* Length of data to read							*/
  uint8_t	end;
};

#define MSG_RETURN_USERDATA 0x03DC

struct msg_return_userdata {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  data[1];		/* Data block (variable length)						*/
  uint8_t	end;
};

#define MSG_READ_SYSINFO 0x0350 /* Operator */

struct msg_read_sysinfo {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t type;			/* Selection:
							0 = Message server information
							1 = Realtime server information
							2 = Telnet server information
							3 = Realtime debug information
							4 = Realtime debug information (reset counters)
							5 = Channel counts
							6 = CNet slot allocations
							7 = CNet status/statistics
							All other codes reserved						*/
  uint8_t	end;
};

#define MSG_RETURN_SYSINFO 0x0351 /* Operator */

struct msg_return_sysinfo {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  data[1];		/* Data block (variable length depends on type)						
							0 = Message server information
								data[0..3] = free
								data[4..7] = busy
								data[8..11] = total
							1 = Realtime server information
								data[0..3] = free
								data[4..7] = busy
								data[8..11] = total
							2 = Telnet server information
								data[0..3] = free
								data[4..7] = busy
								data[8..11] = total
							3,4 = Realtime debug information
								data[0..3] = debug_rt_overrun_count;
								data[4..7] = debug_rt_underrun_count;
								data[8..12] = debug_rt_bufin_count;
								data[12..15] = debug_rt_rampin_count;
								data[16..19] = debug_rt_copy_count;
								data[20..23] = debug_rt_notxfr_count;
								data[24..27] = debug_rt_ramp_count;
								data[28..31] = debug_rt_noinput_count;
								data[32..35] = debug_rt_local_count;
							5 = Channel counts
								data[0..3] = total input channels
								data[4..7] = total output channels
								data[8..11] = total sv channels
								data[12..15] = total digital I/O channels
								data[16..19] = total virtual channels
							6 = CNet slot allocation information
								data[0]  = Length of data block
								data[1]  = Length of async comms data block
								data[2]  = Slot allocation for Encoder counter delta value
								data[3]  = Slot allocation for Group status
								data[4]  = Slot allocation for TSR word 0  (Flags)
								data[5]  = Slot allocation for TSR word 0a (Extended flags)
								data[6]  = Slot allocation for TSR word 1
								data[7]  = Slot allocation for TSR word 2
								data[8]  = Slot allocation for TSR word 3
								data[9]  = Slot allocation for TSR word 4
								data[10] = Slot allocation for TSR word 5
								data[11] = Slot allocation for TSR word 6
								data[12] = Slot allocation for Hydraulic request
								data[13] = Slot allocation for Hydraulic status
								data[14] = Slot allocation for Async comms control word
								data[15] = Slot allocation for Start of async comms data block
								data[16] = Slot allocation for Watchdog word
								data[17] = Slot allocation for Timestamp low word
								data[18] = Slot allocation for Timestamp high word
								data[19] = Slot allocation for Link integrity word
								data[20] = Slot allocation for TSR generator status word
								data[21] = Slot allocation for TSR flags word
							7 = CNet status/statistics
								data[0..3]   = Enable
								data[4..7]   = Master
								data[8..11]  = ID
								data[12..15] = In connector status
								data[16..19] = Out connector status
								data[20..23] = Lock status
								data[24..27] = Integrity packet error count
								data[28..31] = Fault count
								data[32..35] = Checksum error count
								data[36..39] = Watchdog state
								data[40..43] = Timestamp low word
								data[44..47] = Timestamp high word
								data[48..51] = Mode
								data[52..55] = Ring position
								data[56..59] = Ring size
								data[60..63] = Configuration state
								data[64..67] = Watchdog error count
								data[68..71] = Async comms retry count
								data[72..75] = Async comms collision count
								data[76..79] = Async comms timeout count
								data[80..83] = Interrupt count
							8 = Processor usage statistics
							9 = Processor usage statistics with automatic reset
								data[0..3]   = DSPA CNet usage
								data[4..7]   = DSPA CNet peak usage
								data[8..11]  = DSPA timer usage
								data[12..15] = DSPA timer peak usage
								data[16..19] = DSPB total CNet usage
								data[20..23] = DSPB total CNet peak usage
								data[24..27] = DSPB CNet inchan physical usage
								data[28..31] = DSPB CNet inchan physical peak usage
								data[32..35] = DSPB CNet BCtrlIterA usage
								data[36..39] = DSPB CNet BCtrlIterA peak usage
								data[40..43] = DSPB CNet inchan virtual usage
								data[44..47] = DSPB CNet inchan virtual peak usage
								data[48..51] = DSPB CNet BCtrlIterB usage
								data[52..55] = DSPB CNet BCtrlIterB peak usage
								data[56..59] = DSPB CNet out/misc chan usage
								data[60..63] = DSPB CNet out/misc chan peak usage
						*/
  uint8_t	end;
};

#define MSG_SET_SIMULATION_MODE 0x040E /* Operator */

struct msg_set_simulation_mode {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	enable;			/* Enable simulation mode
  							0 = Simulation mode disabled
							1 = Simulation mode enabled						*/
  uint8_t	end;
};

#define MSG_READ_SIMULATION_MODE 0x040F /* Operator */

struct msg_read_simulation_mode {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t	end;
};

#define MSG_RETURN_SIMULATION_MODE 0x0410

struct msg_return_simulation_mode {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	enable;			/* Simulation mode state
  							0 = Simulation mode disabled
							1 = Simulation mode enabled						*/
  uint8_t	end;
};

/********************************************************************************************************
 * Input channel configuration messages																	*
 ********************************************************************************************************/

#define MSG_SET_RAW_EXCITATION 0x034E /* Administrator */

struct msg_set_raw_excitation {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  uint32_t type;			/* Excitation type									*/
  uint32_t volt;			/* Excitation voltage (bits)						*/
  float phase;			/* Excitation phase angle							*/
  uint8_t	end;
};

#define MSG_SET_EXCITATION 0x0300 /* Calibrator */

struct msg_set_excitation {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel											*/
  uint32_t type;			/* Excitation type DC/AC							*/
  float volt;			/* Excitation voltage								*/
  float phase;			/* Excitation phase shift							*/
  uint8_t  end;
};

#define MSG_READ_EXCITATION 0x0301 /* Operator */

struct msg_read_excitation {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info flags								*/
  uint32_t chan;			/* Channel											*/
  uint8_t  end;
};

#define MSG_RETURN_EXCITATION 0x0302

struct msg_return_excitation {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel											*/
  uint32_t type;			/* Excitation type DC/AC							*/
  float volt;			/* Excitation voltage								*/
  float phase;			/* Excitation phase shift							*/
  uint8_t  end;
};

#define MSG_SET_RAW_GAIN 0x0318 /* Administrator */

struct msg_set_raw_gain {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel											*/
  uint32_t coarsegain;		/* Coarse gain										*/
  uint32_t finegain;		/* Fine gain										*/
  uint8_t  end;
};

#define MSG_READ_RAW_GAIN 0x0319 /* Operator */

struct msg_read_raw_gain {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel											*/
  uint8_t  end;
};

#define MSG_RETURN_RAW_GAIN 0x031A

struct msg_return_raw_gain {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel											*/
  uint32_t coarsegain;		/* Coarse gain										*/
  uint32_t finegain;		/* Fine gain										*/
  uint8_t  end;
};

#define MSG_SET_GAIN 0x0303 /* Calibrator */

struct msg_set_gain {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel											*/
  float gain;			/* Gain												*/
  uint32_t flags;			/* Control flags:
  							Bits 2,0: Sensitivity/gain/factor mode					
  										00 = Gain
  										01 = Sensitivity
										10 = Gauge factor / bridge factor
							Bit 1 : Retain gain trim value
										0 = Reset gain trim value to 1.0
										1 = Retain existing gain trim value
  																			*/
  uint8_t  end;
};

#define MSG_READ_GAIN 0x0304 /* Operator */

struct msg_read_gain {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel											*/
  uint8_t  end;
};

#define MSG_RETURN_GAIN 0x0305

struct msg_return_gain {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel											*/
  float gain;			/* Gain												*/
  uint32_t flags;			/* Control flags:
  							Bit 0: 		Sensitivity/gain mode					
  										0 = Gain
  										1 = Sensitivity
  																			*/
  uint8_t  end;
};

#define MSG_SET_GAINTRIM 0x032E /* Calibrator */

struct msg_set_gaintrim {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  float gaintrim;		/* Gain trim value									*/
  uint32_t calmode;		/* Calibration mode flag							*/
  uint8_t	end;
};

#define MSG_READ_GAINTRIM 0x032F /* Operator */

struct msg_read_gaintrim {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_GAINTRIM 0x0330

struct msg_return_gaintrim {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  float gaintrim;		/* Gain trim value									*/
  uint8_t	end;
};

#define MSG_SET_NEG_GAINTRIM 0x03F9 /* Calibrator */

struct msg_set_neg_gaintrim {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  float neg_gaintrim;	/* Negative gain trim value							*/
  uint32_t calmode;		/* Calibration mode flag							*/
  uint8_t	end;
};

#define MSG_READ_NEG_GAINTRIM 0x03FA /* Operator */

struct msg_read_neg_gaintrim {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_NEG_GAINTRIM 0x03FB

struct msg_return_neg_gaintrim {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  float neg_gaintrim;	/* Negative gain trim value							*/
  uint8_t	end;
};

#define MSG_SET_TXDRZERO 0x03C9 /* Calibrator */

struct msg_set_txdrzero {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  float value;			/* Transducer zero value							*/
  uint8_t	end;
};

#define MSG_READ_TXDRZERO 0x03CA /* Operator */

struct msg_read_txdrzero {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_TXDRZERO 0x03CB

struct msg_return_txdrzero {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  float value;			/* Transducer zero value							*/
  uint8_t	end;
};

#define MSG_TXDRZERO_ZERO 0x03CC /* Calibrator */

struct msg_txdrzero_zero {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  uint8_t	end;
};

#define MSG_WRITE_MAP 0x033F /* Calibrator */

struct msg_write_map {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t flags;				/* Flags controlling operation
  							    Bit 0,1	Linearisation mode:
  						    		0 = Disabled
  						    		1 = 2-point 
  							    	2 = Full
  							    	3 = Reserved						
  							    	
  							    	Note that the firmware treats modes
  							    	1,2,3 identically and uses the full
  						    		linearisation table in all cases.	
  						    													*/
  float value[257];			/* Linearisation map								*/
  char  filename[128];		/* Linearisation map filename						*/
  uint8_t	end;
};

#define MSG_READ_MAP 0x0340 /* Operator */

struct msg_read_map {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_MAP 0x0341

struct msg_return_map {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t flags;				/* Flags controlling operation
  							    Bit 0,1	Linearisation mode:
  							    	0 = Disabled
  						    		1 = 2-point 
  							    	2 = Full
  							    	3 = Reserved								*/
  float value[257];			/* Linearisation map 								*/
  char  filename[128];		/* Linearisation map filename						*/
  uint8_t	end;
};

/* Channel flags defined as follows:

	Bit 0     : AC transducer
	Bit 1     : Polarity inverted	(0 = Normal, 1 = Inverted)
	Bit 4	  : Enable 2-point calibration
	Bit 5     : Enable linearisation table
	Bit 6     : Select unipolar mode
	Bit 17,7  : Sensitivity/Gain
				 00 = Gain
				 01 = Sensitivity
				 10 = Gauge/bridge factor (Signal Cube)
	Bit 8	  : Simulation enable
	Bit 9     : Virtual transducer
	Bit 10    : Reset minimum reading
	Bit 11-15 : Transducer type (ignored by chanproc)
				 0 = Generic DC (load cell, strain gauge)
				 1 = LVDT
				 2 = LVIT
	Bit 16	  : Select asymmetrical transducer mode
	Bit 17	  : See bit 7 above
	Bit 18	  : Request flush of map area in cache
	Bit 19	  : Shunt calibration active
	Bit 20	  : Cycle count position
				 0 = Peak
				 1 = Trough
	Bit 21	  : Reserved
	Bit 22	  : Use local cyclic window value
	Bit 23	  : AC coupled (4ACC txdrs only)
	Bit 24	  : Positive or negative shunt calibration
	Bit 25	  : Generate event on cyclic peak detection
	Bit 26	  : Channel disabled (from KO code)
	Bit 27	  : Use GenBus for cycle detection

*/

#define MSG_SET_CHAN_FLAGS 0x0339 /* Calibrator */

struct msg_set_chan_flags {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  uint32_t set;				/* Flag set mask
							   Bits to set in the channel control flags word.
							   New flags word = (old flags word & ~clr) | set
																				*/
  uint32_t clr;				/* Flag clear mask
							   Bits to clear in the channel control flags word.
							   New flags word = (old flags word & ~clr) | set	*/
  uint8_t	end;
};

#define MSG_READ_CHAN_FLAGS 0x033A /* Operator */

struct msg_read_chan_flags {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_CHAN_FLAGS 0x033B

struct msg_return_chan_flags {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t flags;				/* Channel flags									*/
  uint8_t	end;
};

#define MSG_SET_REFZERO 0x0342 /* Operator */

struct msg_set_refzero {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  float value;				/* Reference zero value								*/
  uint8_t	end;
};

#define MSG_READ_REFZERO 0x0343 /* Operator */

struct msg_read_refzero {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_REFZERO 0x0344

struct msg_return_refzero {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  float value;				/* Reference zero value								*/
  uint8_t	end;
};

#define MSG_REFZERO_ZERO 0x03A0 /* Operator */

struct msg_refzero_zero {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t  end;
};

#define MSG_UNDO_REFZERO 0x03E8 /* Operator */

struct msg_undo_refzero {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_SET_CAL 0x0307 /* Operator */

struct msg_set_cal {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	state;				/* Calibration state
  								Bit 0		Enabled
  								Bit 1		0 = Positive, 1 = Negative
  								Bits 2..7	Reserved							*/
  uint8_t	end;
};

#define MSG_SET_LED 0x0308 /* Operator */

struct msg_set_led {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	state;				/* LED state
  								Bit 0		Enabled
  								Bits 1..7	Reserved							*/
  uint8_t	end;
};

#define MSG_CLEAR_ALL_LEDS 0x03E9 /* Operator */

struct msg_clear_all_leds {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint8_t	end;
};

#define MSG_SET_FILTER 0x0309 /* Operator */

struct msg_set_filter {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t filter;				/* Filter number									*/
  uint32_t enable;				/* Filter enable									*/
  uint32_t type;				/* Filter type										*/
  uint32_t order;				/* Filter order										*/
  float freq;				/* Cutoff frequency									*/
  float bandwidth;			/* Filter bandwidth									*/	
  uint8_t	end;
};

#define MSG_READ_FILTER 0x030A /* Operator */

struct msg_read_filter {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t filter;				/* Filter number									*/
  uint8_t	end;
};

#define MSG_RETURN_FILTER 0x030B

struct msg_return_filter {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t filter;				/* Filter number									*/
  uint32_t enable;				/* Filter enable									*/
  uint32_t type;				/* Filter type										*/
  uint32_t order;				/* Filter order										*/
  float freq;				/* Cutoff frequency									*/
  float bandwidth;			/* Filter bandwidth									*/	
  uint8_t	end;
};

#define MSG_SET_FILTER_FREQ 0x03CD /* Operator */

struct msg_set_filter_freq {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t filter;				/* Filter number									*/
  float freq;				/* Cutoff frequency									*/
  float bandwidth;			/* Filter bandwidth									*/	
  uint8_t	end;
};

#define MSG_SAVE_CHAN_CONFIG 0x0321 /* Calibrator */

struct msg_save_chan_config {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint8_t  end;
};

#define MSG_RESTORE_CHAN_CONFIG 0x0322 /* Calibrator */

struct msg_restore_chan_config {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint8_t  end;
};

#define MSG_FLUSH_CHAN_CONFIG 0x0331 /* Administrator */

struct msg_flush_chan_config {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_READ_CHAN_STATUS 0x0333 /* Operator */

struct msg_read_chan_status {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_CHAN_STATUS 0x0334

struct msg_return_chan_status {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t	ctrl;				/* Control flags									*/
  uint32_t status;				/* Status flags										*/
  uint8_t	end;
};

#define MSG_SET_CALGAIN 0x031B /* Administrator */

struct msg_set_calgain {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  float gain;				/* Calibration gain value							*/
  uint8_t  end;
};

#define MSG_READ_CALGAIN 0x031C /* Operator */

struct msg_read_calgain {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint8_t  end;
};

#define MSG_RETURN_CALGAIN 0x031D

struct msg_return_calgain {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  float gain;				/* Calibration gain value							*/
  uint8_t  end;
};

#define MSG_SET_CALOFFSET 0x031E /* Administrator */

struct msg_set_caloffset {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  float offset;				/* Calibration offset value							*/
  uint8_t	end;
};

#define MSG_READ_CALOFFSET 0x031F /* Operator */

struct msg_read_caloffset {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_CALOFFSET 0x0320

struct msg_return_caloffset {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  float offset;				/* Calibration offset value							*/
  uint8_t	end;
};

#define MSG_SET_CALTABLE 0x0352 /* Administrator */

struct msg_set_caltable {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint32_t entry;				/* Calibration table entry							*/
  float value;				/* Calibration value								*/
  uint8_t  end;
};

#define MSG_READ_CALTABLE 0x0353 /* Operator */

struct msg_read_caltable {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint32_t entry;				/* Calibration table entry							*/
  uint8_t  end;
};

#define MSG_RETURN_CALTABLE 0x0354

struct msg_return_caltable {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint32_t entry;				/* Calibration table entry							*/
  float value;				/* Calibration value								*/
  uint8_t  end;
};

#define MSG_SET_EXC_CAL 0x032B /* Administrator */

struct msg_set_exc_cal {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  float exc_zero;			/* Zero calibration value							*/
  float exc_fs;				/* Full scale calibration value						*/	
  uint8_t	end;
};

#define MSG_READ_EXC_CAL 0x032C /* Operator */

struct msg_read_exc_cal {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_EXC_CAL 0x032D

struct msg_return_exc_cal {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  float exc_zero;			/* Zero calibration value							*/
  float exc_fs;				/* Full scale calibration value						*/	
  uint8_t	end;
};

#define MSG_SAVE_CHAN_CALIBRATION 0x0323 /* Administrator */

struct msg_save_chan_calibration {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint8_t  end;
};

#define MSG_RESTORE_CHAN_CALIBRATION 0x0324 /* Administrator */

struct msg_restore_chan_calibration {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint8_t  end;
};

#define MSG_FLUSH_CHAN_CALIBRATION 0x0332 /* Administrator */

struct msg_flush_chan_calibration {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_SET_GAUGE_TYPE 0x03B3 /* Calibrator - Signal Cube only */

struct msg_set_gauge_type {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t type;				/* Gauge type flags
  								Bit 0	Gauge type
  											0 = 120 ohm
  											1 = 350 ohm
  								Bit 1,2	Connection
  											00 = Quarter bridge
  											01 = Half bridge
  											10 = Full bridge
  											11 = Reserved						*/
  uint8_t	end;
};

#define MSG_READ_GAUGE_TYPE 0x03B4 /* Operator - Signal Cube only */

struct msg_read_gauge_type {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_GAUGE_TYPE 0x03B5 /* Signal Cube only */

struct msg_return_gauge_type {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t type;				/* Gauge type flags
  								Bit 0	Gauge type
  											0 = 120 ohm
  											1 = 350 ohm
  								Bit 1,2	Connection
  											00 = Quarter bridge
  											01 = Half bridge
  											10 = Full bridge
  											11 = Reserved						*/
  uint8_t	end;
};

#define MSG_SET_OFFSET 0x03B6 /* Operator - Signal Cube only */

struct msg_set_offset {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  int	offset;				/* Hardware offset									*/
  uint8_t	end;
};

#define MSG_READ_OFFSET 0x03B7 /* Operator - Signal Cube only */

struct msg_read_offset {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_OFFSET 0x03B8 /* Signal Cube only */

struct msg_return_offset {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  int	offset;				/* Hardware offset									*/
  uint8_t	end;
};

#define MSG_START_ZERO 0x03D4 /* Operator - Signal Cube only */

struct msg_start_zero {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_READ_ZERO_PROGRESS 0x03D5 /* Operator - Signal Cube only */

struct msg_read_zero_progress {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_ZERO_PROGRESS 0x03D6 /* Signal Cube only */

struct msg_return_zero_progress {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t state;				/* Zero state										*/
  uint8_t	end;
};

#define MSG_SET_ACTIVE_GAUGES 0x03BD /* Calibrator - Signal Cube only */

struct msg_set_active_gauges {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t gauges;				/* Number of active gauges (1 - 4)					*/
  uint8_t	end;
};

#define MSG_READ_ACTIVE_GAUGES 0x03BE /* Operator - Signal Cube only */

struct msg_read_active_gauges {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_ACTIVE_GAUGES 0x03BF /* Signal Cube only */

struct msg_return_active_gauges {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t gauges;				/* Number of active gauges							*/
  uint8_t	end;
};

#define MSG_SET_GAUGE_INFO 0x03E5 /* Calibrator - Signal Cube only */

struct msg_set_gauge_info {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  float sensitivity;		/* Sensitivity										*/
  float gauge_factor;		/* Gauge factor										*/
  float bridge_factor;		/* Bridge factor									*/
  uint8_t	end;
};

#define MSG_READ_GAUGE_INFO 0x03E6 /* Operator - Signal Cube only */

struct msg_read_gauge_info {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_GAUGE_INFO 0x03E7 /* Signal Cube only */

struct msg_return_gauge_info {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  float sensitivity;		/* Sensitivity										*/
  float gauge_factor;		/* Gauge factor										*/
  float bridge_factor;		/* Bridge factor									*/
  uint8_t	end;
};

#define MSG_SET_CHANSTRING 0x03DD /* Calibrator */

struct msg_set_chanstring {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t	start;				/* Start position within string area				*/
  uint32_t length;				/* Length of string to write						*/
  char  string[256];		/* String											*/
  uint8_t	end;
};

#define MSG_READ_CHANSTRING 0x03DE /* Operator */

struct msg_read_chanstring {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t	start;				/* Start position within string area				*/
  uint32_t length;				/* Length of string to read							*/
  uint8_t	end;
};

#define MSG_RETURN_CHANSTRING 0x03DF

struct msg_return_chanstring {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  char  string[256];		/* String											*/
  uint8_t	end;
};

#define MSG_READ_FULL_CALTABLE 0x0409 /* Operator */

struct msg_read_full_caltable {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint32_t len;				/* Length											*/
  uint8_t  end;
};

#define MSG_RETURN_FULL_CALTABLE 0x040A

struct msg_return_full_caltable {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint32_t len;				/* Length											*/
  float value[1];			/* Calibration table values (variable length)		*/
  uint8_t  end;
};

#define MSG_SET_DIGTXDR_CONFIG 0x040B /* Calibrator */

struct msg_set_digtxdr_config {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t flags;				/* Configuration flags
								Bit 0	Txdr type
											0 = Encoder		1 = SSI
								Bit 1	Override automatic datarate calculation
											and use supplied ssi_datarate value 
								Bit 2	Encoding
											0 = Binary		1 = Gray			*/
  float bitsize;			/* Units/bit										*/
  uint32_t ssi_datalen;		/* SSI transducer data length						*/
  uint32_t ssi_clkperiod;		/* SSI transducer clock period (ns)					*/
  uint8_t	end;
};

#define MSG_READ_DIGTXDR_CONFIG 0x040C /* Operator */

struct msg_read_digtxdr_config {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_DIGTXDR_CONFIG 0x040D

struct msg_return_digtxdr_config {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t flags;				/* Configuration flags
								Bit 0	Txdr type
											0 = Encoder		1 = SSI
								Bit 1	Override automatic datarate calculation
											and use supplied ssi_datarate value
								Bit 2	Encoding
											0 = Binary		1 = Gray			*/
  float bitsize;			/* Units/bit										*/
  uint32_t ssi_datalen;		/* SSI transducer data length						*/
  uint32_t ssi_clkperiod;		/* SSI transducer clock period (ns)					*/
  uint8_t	end;
};

#define MSG_SET_HPFILTER_CONFIG 0x0414 /* Calibrator */

struct msg_set_hpfilter_config {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  int   enable;				/* Filter enable									*/
  float freq;				/* Cutoff frequency									*/
  uint8_t	end;
};

#define MSG_READ_HPFILTER_CONFIG 0x0415 /* Operator */

struct msg_read_hpfilter_config {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_HPFILTER_CONFIG 0x0416

struct msg_return_hpfilter_config {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  int   enable;				/* Filter enable									*/
  float freq;				/* Cutoff frequency									*/
  uint8_t	end;
};

#define MSG_SET_CALZERO_ENTRY 0x0417

struct msg_set_calzero_entry {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t entry;				/* Table entry										*/
  uint32_t daczero;			/* DAC zero value									*/
  float zero;				/* Zero offset value								*/
  uint8_t	end;
};

#define MSG_READ_CALZERO_ENTRY 0x0418

struct msg_read_calzero_entry {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t entry;				/* Table entry										*/
  uint8_t	end;
};

#define MSG_RETURN_CALZERO_ENTRY 0x0419

struct msg_return_calzero_entry {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t entry;				/* Table entry										*/
  uint32_t daczero;			/* DAC zero value									*/
  float zero;				/* Zero offset value								*/
  uint8_t	end;
};


/********************************************************************************************************
 * Input channel reading messages																		*
 ********************************************************************************************************/

#define MSG_READ_IP_CHANNEL 0x030C /* Operator */

struct msg_read_ip_channel {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint8_t	end;
};

#define MSG_RETURN_IP_CHANNEL 0x030D

struct msg_return_ip_channel {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel number									*/
  float value;				/* Input channel value								*/
  uint8_t	end;
};

#define MSG_READ_IP_CYCLECOUNT 0x03FC /* Operator */

struct msg_read_ip_cyclecount {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint8_t	end;
};

#define MSG_RETURN_IP_CYCLECOUNT 0x03FD

struct msg_return_ip_cyclecount {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel number									*/
  uint32_t count;				/* Cycle count										*/
  uint8_t	end;
};

#define MSG_READ_IP_CYCLECOUNTBLOCK 0x03FE /* Operator */

struct msg_read_ip_cyclecountblock {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chanlist[2];		/* Channel list bitfields							*/
  uint8_t	end;
};

#define MSG_RETURN_IP_CYCLECOUNTBLOCK 0x03FF

struct msg_return_ip_cyclecountblock {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t count;				/* Start of channel count block (variable length)	*/
  uint8_t	end;
};

#define MSG_RESET_CYCLECOUNT 0x0400 /* Operator */

struct msg_reset_cyclecount {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint32_t flags;				/* Not used											*/
  uint8_t	end;
};

#define MSG_RESET_ALL_CYCLECOUNTS 0x0401 /* Operator */

struct msg_reset_all_cyclecounts {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t flags;				/* Not used											*/
  uint8_t	end;
};

#define MSG_READ_PEAKS 0x033C /* Operator */

struct msg_read_peaks {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint8_t	end;
};

#define MSG_RETURN_PEAKS 0x033D

struct msg_return_peaks {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel number									*/
  float maximum;			/* Maximum value									*/
  float minimum;			/* Minimum value									*/
  float peak;				/* Cyclic peak value								*/
  float trough;				/* Cyclic trough value								*/
  uint8_t	end;
};

#define MSG_RESET_PEAKS 0x033E /* Operator */

struct msg_reset_peaks {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint32_t flags;				/* Flags determine which readings to reset
  							    Bit 0	Reset max reading
  							    Bit 1	Reset min reading						*/
  uint8_t	end;
};

#define MSG_RESET_ALL_PEAKS 0x038A /* Operator */

struct msg_reset_all_peaks {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint32_t flags;				/* Flags determine which readings to reset
  							    Bit 0	Reset all max readings
  							    Bit 1	Reset all min readings					*/
  uint8_t	end;
};

#define MSG_SET_RESET_PARAMETERS 0x038B /* Operator */

struct msg_set_reset_parameters {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t flags;				/* Flags
  								Bit 0   Enable auto reset						*/
  float time;				/* Reset time										*/
  uint32_t peak_count;			/* Number of peaks for peak/trough calc				*/
  uint8_t	end;
};

#define MSG_READ_RESET_PARAMETERS 0x038C /* Operator */

struct msg_read_reset_parameters {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint8_t	end;
};

#define MSG_RETURN_RESET_PARAMETERS 0x038D

struct msg_return_reset_parameters {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t flags;				/* Flags
  								Bit 0   Enable auto reset						*/
  float time;				/* Reset time										*/
  uint32_t peak_count;			/* Number of peaks for peak/trough calc				*/
  uint8_t	end;
};

#define MSG_READ_IP_CHANNELBLOCK 0x03E0 /* Operator */

struct msg_read_ip_channelblock {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chanlist[2];		/* Channel list bitfields defining which channels
							   to read.

							   Each bit within the chanlist array represents
							   a feedback channel 0 to 63.

							   For each bit set in the chanlist array, the
							   following values for the corresponding channel
							   will be returned:

							   Current value
							   Maximum value
							   Minimum value
							   Peak value
							   Trough value

							   The returned data consists of a sequence of
							   words representing values from the channels
							   corresponding to the bits set in the chanlist
							   array.

							   Only those bits set in the chanlist array will
							   have corresponding words in the response data
							   block.

							   Data is returned in blocks in channel order
							   (channel 0 to 63).								*/
  uint8_t	end;
};

#define MSG_RETURN_IP_CHANNELBLOCK 0x03E1

struct msg_return_ip_channelblock {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  float value;				/* Start of channel value block (variable length)	*/
  uint8_t	end;
};

#define MSG_SET_PEAK_THRESHOLD 0x03EA /* Operator */

struct msg_set_peak_threshold {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t flags;				/* Flags
								Bit  31		Use extended setting
								Bit  30		Enable local channel threshold
											(when bit 31 set)
								Bits 11-0	Channel (if bit 31 set)				*/
  float threshold;			/* Peak/trough detection threshold					*/
  uint8_t	end;
};

#define MSG_READ_PEAK_THRESHOLD 0x03EB /* Operator */

struct msg_read_peak_threshold {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint8_t	end;
};

#define MSG_RETURN_PEAK_THRESHOLD 0x03EC

struct msg_return_peak_threshold {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t flags;				/* Flags (not used)									*/
  float threshold;			/* Global peak/trough detection threshold			*/
  uint8_t	end;
};

#define MSG_READ_PEAK_THRESHOLD_EXT 0x0407 /* Operator */

struct msg_read_peak_threshold_ext {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel											*/
  uint8_t	end;
};

#define MSG_RETURN_PEAK_THRESHOLD_EXT 0x0408

struct msg_return_peak_threshold_ext {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t flags;				/* Flags
								Bit  30		Enable local channel threshold		*/
  float threshold;			/* Peak/trough detection threshold					*/
  uint8_t	end;
};

#define MSG_SET_PEAK_TIMEOUT 0x0411 /* Operator */

struct msg_set_peak_timeout {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t timeout;        	/* Peak detection timeout in csec (0 = disabled)	*/
  uint8_t  end;
};

#define MSG_READ_PEAK_TIMEOUT 0x0412

struct msg_read_peak_timeout {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_PEAK_TIMEOUT 0x0413

struct msg_return_peak_timeout {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t timeout;        	/* Peak detection timeout in csec (0 = disabled)	*/
  uint8_t  end;
};


#define MSG_READ_IP_CHANNELBLOCK_EXT 0x0423 /* Operator */

struct msg_read_ip_channelblock_ext {
	uint16_t	command;			/* Command code										*/
	uint16_t pad;				/* Padding to maintain alignment					*/
	uint8_t  chanlist[64];		/* Channel list bitfields defining which channels
								 and data types to read.

								 Each byte within the chanlist array represents
								 a feedback channel 0 to 63.

								 Within each byte, the bits represent the
								 following data types:

								 Bit 0:	Current value
								 Bit 1:	Maximum value
								 Bit 2:	Minimum value
								 Bit 3:	Peak value
								 Bit 4:	Trough value
								 Bit 5:	Cycle count
								 Bit 6:	Measured cyclic frequency
								 Bit 7:	Reserved

								 The returned data consists of a sequence of
								 words representing the values requested by the
								 bits set in the chanlist array.

								 Only those bits set in the chanlist array will
								 have a corresponding word in the response data
								 block.

								 Data is returned in channel order (channel 0
								 to 63) and bit order (bit 0 to bit 6).

								 All returned values represent 32-bit single
								 precision floating point values, except for
								 the value corresponding to bit 5 (cycle count),
								 which is a 32-bit unsigned integer value.		*/
	uint8_t	end;
};

#define MSG_RETURN_IP_CHANNELBLOCK_EXT 0x0424

struct msg_return_ip_channelblock_ext {
	uint16_t	command;			/* Command code										*/
	uint16_t	pad;				/* Padding to maintain alignment					*/
	float		value[1];			/* Start of channel value block (variable length)	*/
	uint8_t		end;
};


/********************************************************************************************************
 * Output channel writing messages																		*
 ********************************************************************************************************/

#define MSG_WRITE_OP_CHANNEL 0x030E /* Administrator */

struct msg_write_op_channel {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t chan;				/* Channel number									*/
  float value;				/* Output channel value								*/
  uint8_t	end;
};



/********************************************************************************************************
 * Servo-valve channel configuration messages															*
 ********************************************************************************************************/

#define MSG_SET_SV_RANGE 0x0345 /* Calibrator */

struct msg_set_sv_range {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t type;				/* Servo-valve type
  								0 = Current drive
  								1 = Voltage drive								*/
  float range;				/* Full-scale current range							*/
  uint8_t	end;
};

#define MSG_READ_SV_RANGE 0x0346 /* Operator */

struct msg_read_sv_range {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;		
};

#define MSG_RETURN_SV_RANGE 0x0347

struct msg_return_sv_range {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t type;				/* Servo-valve type
  								0 = Current drive
  								1 = Voltage drive								*/
  float range;				/* Full-scale current range							*/
  uint8_t	end;
};

#define MSG_SET_SV_DITHER 0x038E /* Calibrator */

struct msg_set_sv_dither {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t enable;				/* Dither enable flag								*/
  float dither;				/* Dither amplitude									*/
  float freq;				/* Dither frequency									*/
  uint8_t	end;
};

#define MSG_READ_SV_DITHER 0x038F /* Operator */

struct msg_read_sv_dither {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_SV_DITHER 0x0390

struct msg_return_sv_dither {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t enable;				/* Dither enable flag								*/
  float dither;				/* Dither amplitude									*/
  float freq;				/* Dither frequency									*/
  uint8_t	end;
};

#define MSG_SET_SV_BALANCE 0x0391 /* Calibrator */

struct msg_set_sv_balance {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  float balance;			/* Valve balance									*/
  uint8_t	end;
};

#define MSG_READ_SV_BALANCE 0x0392 /* Operator */

struct msg_read_sv_balance {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_SV_BALANCE 0x0393

struct msg_return_sv_balance {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  float balance;			/* Valve balance									*/
  uint8_t	end;
};

#define MSG_SET_RAW_CURRENT 0x034B /* Administrator */

struct msg_set_raw_current {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t current;			/* Current range pot setting						*/
  uint8_t	end;
};



/********************************************************************************************************
 * Digital I/O channel messages																			*
 ********************************************************************************************************/

#define MSG_SET_DIGIO_INFO 0x03AB /* Operator */

struct msg_set_digio_info {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t flags;				/* Flags:
									Bit 0	Polarity: 0 = Normal, 1 = Invert
									Bit 1	Type: 	  Not settable				*/			
  char  name[NAMELEN];		/* Channel name										*/
  char  state0[NAMELEN];	/* State 0 string									*/
  char  state1[NAMELEN];	/* State 1 string									*/
  uint8_t	end;
};

#define MSG_READ_DIGIO_INFO 0x03AC /* Operator */

struct msg_read_digio_info {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_DIGIO_INFO 0x03AD

struct msg_return_digio_info {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t flags;				/* Flags:
									Bit 0	Polarity: 0 = Normal, 1 = Invert
									Bit 1	Type: 	  0 = Input, 1 = Output		*/			
  char  name[NAMELEN];		/* Channel name										*/
  char  state0[NAMELEN];	/* State 0 string									*/
  char  state1[NAMELEN];	/* State 1 string									*/
  uint8_t	end;
};

#define MSG_WRITE_DIGIO_OUTPUT 0x03AE /* Operator */

struct msg_write_digio_output {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t output;				/* Output value to be written						*/
  uint8_t	end;
};

#define MSG_READ_DIGIO_INPUT 0x03AF /* Operator */

struct msg_read_digio_input {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_DIGIO_INPUT 0x03B0

struct msg_return_digio_input {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t input;				/* Current input state								*/
  uint8_t	end;
};

#define MSG_READ_DIGIO_OUTPUT 0x03B1 /* Operator */

struct msg_read_digio_output {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_DIGIO_OUTPUT 0x03B2

struct msg_return_digio_output {
  uint16_t	command;			/* Command code										*/
  uint16_t pad;				/* Padding to maintain alignment					*/
  uint32_t	chan;				/* Channel number									*/
  uint32_t output;				/* Current output state								*/
  uint8_t	end;
};


/********************************************************************************************************
 * Hydraulic control messages																			*
 ********************************************************************************************************/

#define MSG_SET_HYDRAULIC_OUTPUT 0x030F /* Administrator */

struct msg_set_hydraulic_output {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t	set;			/* Hydraulic outputs to set							*/
  uint8_t  clear;			/* Hydraulic outputs to clear						*/
  uint8_t	end;
};

#define MSG_READ_HYDRAULIC_OUTPUT 0x0310 /* Operatro */

struct msg_read_hydraulic_output {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint8_t	end;
};

#define MSG_RETURN_HYDRAULIC_OUTPUT 0x0311

struct msg_return_hydraulic_output {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  state;			/* Current hydraulic output state					*/
  uint8_t	end;
};

#define MSG_READ_HYDRAULIC_INPUT 0x0312 /* Operator */

struct msg_read_hydraulic_input {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t	end;
};

#define MSG_RETURN_HYDRAULIC_INPUT 0x0313

struct msg_return_hydraulic_input {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  state;			/* Current hydraulic input state					*/
  uint8_t	end;
};

#define MSG_DEFINE_HYDRAULIC_IO 0x0348 /* Calibrator */

struct msg_define_hydraulic_io {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t ident;			/* Hydraulic identifier
  							0x0000: Contact 1
  							0x0001: Contact 2
  							0x0002: Contact 3
  							0x0100: Solenoid 1
  							0x0101: Solenoid 2
  							0x0102: Solenoid 3								*/
  uint32_t flags;			/* Control flags
  							Bit 0:	Available								
							Bit 1:	Enable switch off delay					
							Bit 2:  For type 0 hydraulic LP output only, 
									turn off LP when selecting HP			*/
  char  label[32];		/* User defined label								*/
  uint32_t timeout;		/* Timeout period in csec							*/  							
  uint8_t	end;
};

#define MSG_READ_HYDRAULIC_IO 0x0349 /* Operator */

struct msg_read_hydraulic_io {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t ident;			/* Hydraulic identifier
  							0x0000: Contact 1
  							0x0001: Contact 2
  							0x0002: Contact 3
  							0x0100: Solenoid 1
  							0x0101: Solenoid 2
  							0x0102: Solenoid 3								*/
  uint8_t	end;
};

#define MSG_RETURN_HYDRAULIC_IO 0x034A

struct msg_return_hydraulic_io {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t flags;			/* Control flags
  							Bit 0:	Available								
							Bit 1:	Enable switch off delay					
							Bit 2:  For type 0 hydraulic LP output only, 
									turn off LP when selecting HP			*/
  char  label[32];		/* User defined label								*/  							
  uint32_t timeout;		/* Timeout period in csec							*/  							
  uint8_t	end;
};

#define MSG_DEFINE_HYDRAULIC_MODE 0x03A8 /* Calibrator */

struct msg_define_hydraulic_mode {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t mode;			/* Hydraulic mode:
  							0 = Slave
  							1 = Standalone
  							2 = Master										*/
  uint32_t group;			/* Hydraulic group (0 - 3)							*/  							
  uint32_t type;			/* Hydraulic type and option flags:
							Bits 0 - 7  Select hydraulic type
  							  0 	  = Hydraulic
							  1 	  = Hydraulic simulation
							  2 	  = Pump
  							  3 	  = Pump simulation
  							  4 - 255 = Reserved								
  							Bit 8		Disable 2nd e-stop circuit test 	
							Bit 9		Disable validation requirement on 
										hand controller e-stop
							Bit 10		Disable force setup mode action when
										guard open
							Bit 11		Disable high pressure when guard
										open
							Bit 12		Enable soft stop operation for
										pump systems (hydraulic type 2)
  							*/
  uint8_t	end;
};

#define MSG_READ_HYDRAULIC_MODE 0x03A9 /* Operator */

struct msg_read_hydraulic_mode {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t	end;
};

#define MSG_RETURN_HYDRAULIC_MODE 0x03AA

struct msg_return_hydraulic_mode {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t mode;			/* Hydraulic mode:
  							0 = Slave
  							1 = Standalone
  							2 = Master										*/
  uint32_t group;			/* Hydraulic group (0 - 3)							*/  							
  uint32_t type;			/* Hydraulic type and option flags:
							Bits 0 - 7  Select hydraulic type
  							  0 	  = Hydraulic
							  1 	  = Hydraulic simulation
							  2 	  = Pump
  							  3 	  = Pump simulation
  							  4 - 255 = Reserved								
  							Bit 8		Disable 2nd e-stop circuit test
							Bit 9		Disable validation requirement on 
										hand controller e-stop
							Bit 10		Disable force setup mode action when
										guard open
							Bit 11		Disable high pressure when guard
										open
							Bit 12		Enable soft stop operation for
										pump systems (hydraulic type 2)
  							*/
  uint8_t	end;
};

#define MSG_SET_HYDRAULIC_DISSIPATION 0x03D1 /* Calibrator */

struct msg_set_hydraulic_dissipation {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t dissipation;	/* Dissipation period in csec						*/
  uint8_t	end;
};

#define MSG_READ_HYDRAULIC_DISSIPATION 0x03D2 /* Operator */

struct msg_read_hydraulic_dissipation {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t	end;
};

#define MSG_RETURN_HYDRAULIC_DISSIPATION 0x03D3

struct msg_return_hydraulic_dissipation {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t dissipation;	/* Dissipation period in csec						*/
  uint8_t	end;
};

#define MSG_SET_HYDRAULIC_NAME 0x03D7 /* Calibrator */

struct msg_set_hydraulic_name {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  char  name[32];		/* User defined name								*/
  uint8_t	end;
};

#define MSG_READ_HYDRAULIC_NAME 0x03D8 /* Operator */

struct msg_read_hydraulic_name {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t	end;
};

#define MSG_RETURN_HYDRAULIC_NAME 0x03D9

struct msg_return_hydraulic_name {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  char  name[32];		/* User defined name								*/  							
  uint8_t	end;
};

#define MSG_HYDRAULIC_CONTROL 0x034F /* Operator */

struct msg_hydraulic_control {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t request;		/* Hydraulic request								
  							1 = State 1
  							2 = State 2
  							3 = State 3										
  							4 = State 4										*/
  uint8_t	end;
};

#define MSG_READ_HYDRAULIC_STATE 0x034C /* Operator */

struct msg_read_hydraulic_state {
  uint16_t	command;		/* Command code										*/
  uint16_t msginfo;		/* Message info										*/
  uint8_t	end;
};

#define MSG_RETURN_HYDRAULIC_STATE 0x034D

struct msg_return_hydraulic_state {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t state;			/* Current internal hydraulic state					*/
  uint32_t ack;			/* Current hydraulic acknowledgement state

							Bits 0 - 7	Code indicating current state:

	  								0 = No E Stop
  									1 = State 1
  									2 = State 2
  									3 = State 3
  									4 = State 4

						    Bit 8	Emergency stop invalid
	
									Indicates that the emergency stop chain 
									has detected a fault (or it has not yet 
									been validated).

						    Bit 9	Master emergency stop not validated

									Indicates that the master emergency stop 
									circuit has not been validated.

						    Bit 10	Hand controller emergency stop not 
						  			validated.

  																			*/
  uint32_t error;			/* Current hydraulic error state					*/
  uint8_t	end;
};

/********************************************************************************************************
 * System configuration messages																		*
 ********************************************************************************************************/

#define MSG_READ_SLOT_CONFIGURATION 0x0314 /* Operator */

struct msg_read_slot_configuration {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t	slot;			/* Expansion slot number (range 0 to 3)				*/
  uint8_t  reserved[3];	/* Reserved											*/
  uint8_t	end;
};

#define MSG_RETURN_SLOT_CONFIGURATION 0x0315

struct msg_return_slot_configuration {
  uint16_t	command;		/* Command code										*/
  uint16_t pad1;			/* Padding to maintain alignment					*/
  uint8_t	slot;			/* Expansion slot number (range 0 to 3)				*/
  uint8_t  reserved[3];	/* Reserved											*/
  uint8_t  type;			/* Card type										*/
  uint8_t	numinchan;		/* Number of input channels							*/
  uint8_t	numoutchan;		/* Number of output channels						*/
  uint8_t	nummiscchan;	/* Number of misc channels							*/
  char  serialno[16];	/* Serial number of card							*/
  uint8_t	data[235];		/* Configuration information data
  							(Format depends on card type)					*/
  uint8_t  pad2[5];		/* Padding											*/
  uint8_t	end;
};

#define MSG_READ_CHAN_INFORMATION 0x0316 /* Operator */

struct msg_read_chan_information {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  uint8_t	end;
};

#define MSG_RETURN_CHAN_INFORMATION 0x0317

struct msg_return_chan_information {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t	chan;			/* Channel number									*/
  uint8_t  type;			/* Channel type										*/
  uint8_t  pad1[3];		/* Padding to maintain alignment					*/
  uint8_t	end;
};

#define MSG_SET_CHANNEL_SOURCE 0x03F8

struct msg_set_channel_source {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t flags;			/* Flags (must be 0)								*/
  uint32_t chan;			/* Channel number (0 - 255)							*/
  uint32_t cnetslot;		/* CNet slot										*/
  uint8_t	end;
};



/********************************************************************************************************
 * CNet configuration messages																			*
 ********************************************************************************************************/

#define MSG_READ_CNET_CHANNEL 0x0325 /* Operator */

struct msg_read_cnet_channel {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel											*/
  uint8_t  end;
};

#define MSG_RETURN_CNET_CHANNEL 0x0326

struct msg_return_cnet_channel {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel											*/
  float value;			/* Value											*/
  uint8_t  end;
};

#define MSG_SET_CNET_SLOT 0x0327 /* Operator */

struct msg_set_cnet_slot {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t chan;			/* Channel											*/
  uint32_t slot;			/* CNet slot number							
  							0xFFFFFFFF = No CNet slot 						*/
  uint8_t  end;
};

#define MSG_READ_CNET_STATUS 0x0329 /* Operator */

struct msg_read_cnet_status {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint8_t  end;
};

#define MSG_RETURN_CNET_STATUS 0x032A

struct msg_return_cnet_status {
  uint16_t	command;		/* Command code										*/
  uint16_t pad;			/* Padding to maintain alignment					*/
  uint32_t flags;			/* CNet status flags:
  							Bit 0 : Master
  							Bit 1 : Enabled	
  							Bit 2 : In port connected
  							Bit 3 : Out port connected
  							Bit 4 : Good integrity							*/
  uint32_t localid;		/* CNet local ID code								*/  							
  uint32_t faults;			/* CNet integrity faults detected					*/
  uint32_t chksum;			/* CNet checksum errors detected					*/  							
  uint8_t  end;
};

#define MSGFLAG_CNET_MASTER		0x01
#define MSGFLAG_CNET_ENABLE 	0x03
#define MSGFLAG_CNET_INCONN 	0x04
#define MSGFLAG_CNET_OUTCONN	0x08
#define MSGFLAG_CNET_INTEGRITY	0x10

#define MSG_SET_CNET_MAPTABLE 0x0377 /* Operator */

struct msg_set_cnet_maptable {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t map[256];		/* CNet map													
  
  						   Entries in the map table have the following
  						   format:
  						   
  						   Bit 31	  Indicates the slot is active
  						   Bit 30	  Indicates slot is an an output
					  	   Bit 29	  Do not map to hardware
  						   Bits 21-25 Destination CNet address
  						   				(output slots only)
  						   Bits 16-20 Source CNet address
  						   Bits 0-15  Local channel mapped to CNet slot		
  						   
  						   For output slots, the local channel specifies
  						   which channel is driven by the slot data.							*/
  uint8_t	end;
};

#define MSG_READ_CNET_MAPTABLE 0x0378 /* Operator */

struct msg_read_cnet_maptable {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t	end;
};

#define MSG_RETURN_CNET_MAPTABLE 0x0379

struct msg_return_cnet_maptable {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t map[256];		/* CNet map																*/
  uint8_t	end;
};

#define MSG_READ_CNET_VALID_RANGE 0x03C7 /* Operator */

struct msg_read_cnet_valid_range {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t	end;
};

#define MSG_RETURN_CNET_VALID_RANGE 0x03C8

struct msg_return_cnet_valid_range {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t max_slot;		/* Maximum valid CNet slot number										*/
  uint8_t	end;
};

#define MSG_SET_CNET_TIMEOUT 0x03E2 /* Operator */

struct msg_set_cnet_timeout {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t timeout;		/* CNet timeout															*/
  uint8_t	end;
};

#define MSG_READ_CNET_TIMEOUT 0x03E3 /* Operator */

struct msg_read_cnet_timeout {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t	end;
};

#define MSG_RETURN_CNET_TIMEOUT 0x03E4

struct msg_return_cnet_timeout {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t timeout;		/* CNet timeout															*/
  uint8_t	end;
};


/********************************************************************************************************
 * Realtime transfer messages																			*
 ********************************************************************************************************/

#define MSG_DEFINE_OUTPUT_LIST 0x0357 /* Operator */

struct msg_define_output_list {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t listsize;       /* Size of output list													*/
  uint8_t  list[1];		/* Start of channel list (variable size)								*/
};

#define MSG_READ_OUTPUT_LIST 0x039C /* Operator */

struct msg_read_output_list {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t  end;
};

#define MSG_RETURN_OUTPUT_LIST 0x039D

struct msg_return_output_list {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t listsize;       /* Size of output list													*/
  uint8_t  list[1];		/* Start of channel list (variable size)								*/
};

#define MSG_DEFINE_INPUT_LIST 0x0358 /* Operator */

struct msg_define_input_list {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t listsize;       /* Size of input list 		 											*/
  uint8_t  list[1];		/* Start of channel list (variable size)								*/
};

#define MSG_READ_INPUT_LIST 0x039E /* Operator */

struct msg_read_input_list {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t  end;
};

#define MSG_RETURN_INPUT_LIST 0x039F

struct msg_return_input_list {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t listsize;       /* Size of input list 		 											*/
  uint8_t  list[1];		/* Start of channel list (variable size)								*/
};

#define MSG_SET_REALTIME_CONFIG 0x0399 /* Operator */

struct msg_set_realtime_config {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t rate;			/* Decimation factor													*/
  uint8_t  end;
};

#define MSG_READ_REALTIME_CONFIG 0x039A /* Operator */

struct msg_read_realtime_config {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t  end;
};

#define MSG_RETURN_REALTIME_CONFIG 0x039B

struct msg_return_realtime_config {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t rate;			/* Decimation factor													*/
  uint8_t  end;
};

#define MSG_READ_TOTAL_BUFFER 0x0359 /* Operator */

struct msg_read_total_buffer {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t  end;
};

#define MSG_RETURN_TOTAL_BUFFER 0x035A

struct msg_return_total_buffer {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t buf_size;       /* Size of buffer (in floats)      								  		*/
  uint8_t  end;
};

#define MSG_READ_OUTPUT_BUFFER 0x035B /* Operator */

struct msg_read_output_buffer {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t  end;
};

#define MSG_RETURN_OUTPUT_BUFFER 0x035C

struct msg_return_output_buffer {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t buf_size;       /* Size of buffer (in packets)      								  		*/
  uint8_t  end;
};

#define MSG_READ_INPUT_BUFFER 0x035D /* Operator */

struct msg_read_input_buffer {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t  end;
};

#define MSG_RETURN_INPUT_BUFFER 0x035E

struct msg_return_input_buffer {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t buf_size;       /* Size of buffer (in packets)       							 		*/
  uint8_t  end;
};

#define MSG_DEFINE_OUTPUT_BUFFER 0x035F /* Operator */

struct msg_define_output_buffer {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t buf_size;       /* Size of buffer (in packets)      								  		*/
  uint8_t  end;
};

#define MSG_DEFINE_INPUT_BUFFER 0x0360 /* Operator */

struct msg_define_input_buffer {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t buf_size;       /* Size of buffer (in packets)      								  		*/
  uint8_t  end;
};

#define MSG_READ_BUFFER_STATUS 0x0361 /* Operator */

struct msg_read_buffer_status {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t  end;
};

#define MSG_RETURN_BUFFER_STATUS 0x0362

struct msg_return_buffer_status {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t output_free;	/* Output buffer free space												*/
  uint32_t input_used;		/* Input buffer occupied space											*/
  uint32_t status;			/* Buffer status flags													  
  							Bit 0:	Overrun has occurred (Cube to PC)
  							Bit 1:	Underrun has occurred (PC to Cube)
							Bit 2:	Output buffer empty	
							Bit 4:  Input transfer enabled
  							Bit 5:  Output transfer enabled										
  							Bit 6:	Stop ramp in progress										*/
  uint8_t  end;
};

#define MSG_CLEAR_ERROR_FLAGS 0x0363 /* Operator */

struct msg_clear_error_flags {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t  end;
};

#define MSG_CONTROL_REALTIME_TRANSFER 0x0364 /* Operator */

struct msg_control_realtime_transfer {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t flags;			/* Realtime enable flags
  							 Bit 0: Enable input 
  							 Bit 1: Enable output             
  							 Bit 2: Enable ramp
							 Bit 3: Enable TSR input control (Cube to PC)

								   Used by periodic data acquisition and controlled by TSRAcqEn 
								   bit on TSR0 slot. When the RT_INTSR flag is set, the state 
								   of the TSRAcqEn bit is logically ORed with the RT_INPUT
								   enable flag to provide the input data control.


  							 Bit 4: Enable TSR controlled turning point output control (PC to Cube)				

								   This flag is used when operating in turning point mode to 
								   enable control of the data output via the turning point 
								   handshake function.

								   When enabled, two TSR flags TSRTPDAV and TSRTPACK are used 
								   to handshake between the realtime data output and the turning 
								   point generator, allowing individual samples to be read from
								   the output buffer as required.

								   When the RT_OUTTSR flag is set, the turning point handshake 
								   state is logically ORed with the RT_OUTPUT enable flag to 
								   provide the output data control. The RT_OUTPUT and RT_OUTTSR 
								   flags should not be asserted simultaneously.

  							 Bit 5: Disable synchronisation of input/output transfers			*/
  uint8_t  end;
};

#define MSG_PAUSE_REALTIME_TRANSFER 0x0365 /* Operator */

struct msg_pause_realtime_transfer {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t flags;          /* Control flags:
  							 Bit 0: Pause command only 
  							 Bit 1: Flush input & output buffers
  							 Bit 2: Perform stop ramp
  							 Bit 3: Pause output transfer (from PC)														
							 Bit 4: Flush input buffer
							 Bit 5: Flush output buffer											*/
  uint8_t  end;
};

#define PAUSE_COMMAND_ONLY 	0x01
#define FLUSH_BOTH_BUFFERS	0x02
#define GENERATE_STOP		0x04
#define GENERATE_PAUSE		0x08
#define FLUSH_IN_BUFFER		0x10
#define FLUSH_OUT_BUFFER	0x20

#define MSG_RESUME_REALTIME_TRANSFER 0x0366 /* Operator */

struct msg_resume_realtime_transfer {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t  end;
};

#define MSG_TRANSFER_REALTIME_DATA 0x0367 /* Operator */

struct msg_transfer_realtime_data {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t num_pkts;       /* Number of output packets 											*/
  uint32_t max_pkts;		/* Maximum number of packets for input									*/
  float data[1];		/* Start of realtime data (variable size)								*/
};

#define MSG_RETURN_REALTIME_DATA 0x0368

struct msg_return_realtime_data {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t status;			/* Realtime transfer status flags										
  							Bit 0	Overrun has occurred (Cube to PC)
  							Bit 1	Underrun has occurred (PC to Cube)
							Bit 2	Output buffer empty	
							Bit 4   Input transfer enabled
  							Bit 5   Output transfer enabled										
  							Bit 6	Stop ramp in progress										*/
  uint32_t num_pkts_ack;   /* Number of packets written into output (command) buffer				*/
  uint32_t num_pkts_free; 	/* Number of free packets in output buffer (prior to this transfer) 	*/
  uint32_t num_pkts_used;	/* Number of used packets in input buffer (prior to this transfer)		*/
  uint32_t num_pkts_input; /* Number of packets in response (read from input buffer)	  			*/
  uint32_t samplectr_lsh;	/* Sample counter LSH													*/
  uint32_t samplectr_msh;	/* Sample counter MSH													*/
  float data[1];		/* Start of realtime data (variable size)								*/
};

#define MSG_SET_RT_STOP_TYPE 0x037A /* Operator */

struct msg_set_rt_stop_type {		
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t type;			/* Stop type								
  							0 = Constant rate
  							1 = Constant time													*/
  uint8_t	end;
};

#define MSG_READ_RT_STOP_TYPE 0x037B /* Operator */

struct msg_read_rt_stop_type {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint8_t	end;
};

#define MSG_RETURN_RT_STOP_TYPE 0x037C

struct msg_return_rt_stop_type {		
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t type;			/* Stop type								
  							0 = Constant rate
  							1 = Constant time													*/
  uint8_t	end;
};

#define MSG_SET_RT_STOP_PARAMETERS 0x037D /* Operator */

struct msg_set_rt_stop_parameters {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t slot;			/* CNet slot															*/
  float target;			/* Stop target															*/
  float rate;			/* Stop rate															*/
  int   time;			/* Stop time (samples)													*/
  uint8_t	end;
};

#define MSG_READ_RT_STOP_PARAMETERS 0x037E /* Operator */

struct msg_read_rt_stop_parameters {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  uint32_t slot;			/* CNet slot															*/
  uint8_t	end;
};

#define MSG_RETURN_RT_STOP_PARAMETERS 0x037F

struct msg_return_rt_stop_parameters {
  uint16_t	command;		/* Command code															*/
  uint16_t pad;			/* Padding to maintain alignment										*/
  float target;			/* Stop target															*/
  float rate;			/* Stop rate															*/
  int   time;			/* Stop time (samples)													*/
  uint8_t	end;
};



/********************************************************************************************************
 * Kinet configuration and communications messages														*
 ********************************************************************************************************/

#define MSG_SEND_KINET_MESSAGE 0x0369 /* Operator */

struct msg_send_kinet_message {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint16_t length;			/* Message length																*/
  uint8_t  flags;			/* Control flags
  							Bit 0:  0 = Slow comms (1 slot)
  									1 = Fast comms (8 slot)												*/
  uint8_t  reserved;  		/* Reserved																		*/
  uint16_t chksum;			/* Message checksum																*/
  uint16_t data[1];		/* Message data (variable size)													*/
};

#define MSG_RETURN_KINET_MESSAGE 0x036A

struct msg_return_kinet_message {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint16_t length;			/* Response length																*/
  uint16_t data[1];		/* Response data (variable size)												*/
};

#define MSG_SET_KINET_MAPPING 0x0371 /* Operator */

struct msg_set_kinet_mapping {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t kinet_slot;		/* KiNet slot to map															*/
  uint32_t cnet_slot;		/* CNet slot
  							0xFFFFFFFF = Unmapped														*/
  uint32_t flags;			/* Flags:
							Bit 0: Direction
									0 = KiNet to CNet
									1 = CNet to KiNet	
							Bit 1: Type
									0 = Raw data (no scaling)
									1 = Scaled data														*/
  float scale;			/* Scaling factor (not used)													*/
  uint8_t	end;
};

#define MSG_READ_KINET_MAPPING 0x0372 /* Operator */

struct msg_read_kinet_mapping {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t kinet_slot;		/* KiNet slot to read															*/
  uint8_t	end;
};

#define MSG_RETURN_KINET_MAPPING 0x0373

struct msg_return_kinet_mapping {	
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t kinet_slot;		/* KiNet slot																	*/
  uint32_t cnet_slot;		/* CNet slot
  							0xFFFFFFFF = Unmapped														*/
  uint32_t flags;			/* Flags:	
							Bit 0: Direction
									0 = KiNet to CNet
									1 = CNet to KiNet	
							Bit 1: Type
									0 = Raw data (no scaling)
									1 = Scaled data														*/
  float scale;			/* Scaling factor (not used)													*/
  uint8_t	end;
};

#define MSG_SET_KINET_FLAGS 0x0396 /* Operator */

struct msg_set_kinet_flags {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t flags;			/* KiNet control flags
  							Bit 0	Reserved (must be 1)
  							Bit 1	KiNet enable														*/
  uint8_t	end;
};

#define MSG_READ_KINET_FLAGS 0x0397 /* Operator */

struct msg_read_kinet_flags {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint8_t	end;
};

#define MSG_RETURN_KINET_FLAGS 0x0398

struct msg_return_kinet_flags {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t flags;			/* KiNet control flags
  							Bit 0	Reserved
  							Bit 1	KiNet enable														*/
  uint8_t	end;
};



/********************************************************************************************************
 * Event log management messages																		*
 ********************************************************************************************************/

#define MSG_READ_EVENTLOG_STATUS 0x03C0 /* Operator */

struct msg_read_eventlog_status {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint8_t	end;
};

#define MSG_RETURN_EVENTLOG_STATUS 0x03C1

struct msg_return_eventlog_status {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t size;			/* Number of entries in event log												*/
  uint8_t	end;
};

#define MSG_FLUSH_EVENTLOG 0x03C2 /* Operator */

struct msg_flush_eventlog {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint8_t	end;
};

#define MSG_READ_EVENTLOG_ENTRY 0x03C3 /* Operator */

struct msg_read_eventlog_entry {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint8_t	end;
};

#define EVENTLOG_EMPTY 1

#define MSG_RETURN_EVENTLOG_ENTRY 0x03C4	/* Reads the oldest entry in event log and removes it
											   from the log												*/
struct msg_return_eventlog_entry {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t status;			/* Eventlog entry status:
  							0 = Entry valid
  							1 = Event log empty															*/
  uint32_t timestamp_lo;	/* Entry timestamp LSH															*/
  uint32_t timestamp_hi;	/* Entry timestamp MSH															*/
  uint32_t type;			/* Entry type																	*/
  uint32_t parameter1;		/* Entry parameter 1															*/
  uint32_t parameter2;		/* Entry parameter 2															*/
  uint8_t	end;
};

#define MSG_READ_TIMESTAMP 0x03C5 /* Operator */

struct msg_read_timestamp {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint8_t	end;
};

#define MSG_RETURN_TIMESTAMP 0x03C6

struct msg_return_timestamp {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t timestamp_lo;	/* Entry timestamp LSH															*/
  uint32_t timestamp_hi;	/* Entry timestamp MSH															*/
  uint8_t	end;
};



/********************************************************************************************************
 * Event report management messages																		*
 ********************************************************************************************************/

#define MSG_OPEN_EVENT_CONNECTION 0x03ED /* Operator */

struct msg_open_event_connection {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t flags;			/* Reserved																		*/
  uint32_t ipaddr;			/* IP address to receive reports												*/
  uint16_t port;			/* Port number to receive reports												*/
  uint16_t reserved;		/* Reserved, must be zero														*/
  uint32_t update;			/* Low priority update interval in ms											*/
  uint32_t handle;			/* For internal use only, must be zero when sent from PC

							Internally the handle has the following form:

							Bit  31:	Set to 1
							Bits 0-2:	Connection number												
							Bits 8-12	Gateway CNet address											*/
  uint8_t	end;
};

#define MSG_RETURN_EVENT_CONNECTION 0x03EE /* Operator */

struct msg_return_event_connection {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t handle;			/* Connection handle															*/
  uint8_t	end;
};

#define MSG_CLOSE_EVENT_CONNECTION 0x03EF /* Operator */

struct msg_close_event_connection {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t flags;			/* Reserved																		*/
  uint32_t handle;			/* Connection handle to close													*/
  uint8_t	end;
};

#define MSG_ADD_EVENT_REPORT 0x03F0 /* Operator */

struct msg_add_event_report {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t handle;			/* Event report handle															*/
  uint32_t priority;		/* Priority level (0 = low, 1 = high)											*/
  uint32_t event;			/* Event type																	*/
  uint32_t size;			/* Size of additional data area (if required)									*/
  uint32_t data[1];		/* Start of additional data area												*/
  uint8_t	end;
};

#define MSG_REMOVE_EVENT_REPORT 0x03F1 /* Operator */

struct msg_remove_event_report {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t handle;			/* Event report handle															*/
  uint32_t event;			/* Event type																	*/
  uint8_t	end;
};

#define MSG_SEND_HC_INSTRUCTION 0x03F2 /* Operator */

struct msg_send_hc_instruction {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t instr;			/* Instruction																	*/
  uint8_t	end;
};

#define MSG_READ_HC_KEYSTATE 0x03F3 /* Operator */

struct msg_read_hc_keystate {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint8_t	end;
};

#define MSG_RETURN_HC_KEYSTATE 0x03F4 /* Operator */

struct msg_return_hc_keystate {
  uint16_t	command;		/* Command code																	*/
  uint16_t pad;			/* Padding to maintain alignment												*/
  uint32_t keystate;		/* Key state																	
  							Bits 0 - 7	Current key state
  							Bit  31		Hand controller present											*/
  uint8_t	end;
};

#ifdef LOADER

#define MAX_MSG_CODE 0x01FF /* Maximum legal message code */

#else

#define MAX_MSG_CODE 0x0424 /* Maximum legal message code */

#endif





