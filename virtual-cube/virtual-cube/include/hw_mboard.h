/********************************************************************************
 * MODULE NAME       : hw_mboard.h												*
 * MODULE DETAILS    : Header for mainboard specific routines					*
 *																				*
 ********************************************************************************/

#ifndef __HW_MBOARD_H
#define __HW_MBOARD_H

#include <stdint.h>

/* Definition of the hardware interface on the 2GP1SV card */

typedef struct hw_mboard {
	union {
		struct {
			uint16_t	excaddr;	/* 	0x00	Write excitation RAM address			*/
			short	exc0;		/* 	0x02	Write excitation channel 0				*/
			short	exc1;		/*	0x04	Write excitation channel 1				*/
			short	dac0;		/*	0x06	Write DAC 0	                    		*/
			short	dac1;		/*	0x08	Write DAC 1                 			*/
			short	sv0;		/*	0x0A	Write SV DAC							*/
			uint16_t	reserved0;	/*	0x0C	Reserved								*/
			uint16_t	ctrl;		/*	0x0E	Write control register
												Bit 0	Channel 0 DC/AC flag
												Bit 1	Channel 1 DC/AC flag		*/
			uint16_t	reserved1;	/* 	0x10	Reserved								*/
			uint16_t	reserved2;	/* 	0x12	Reserved								*/
			uint16_t	reserved3;	/* 	0x14	Reserved								*/
			uint16_t	reserved4;	/* 	0x16	Reserved								*/
			uint16_t	reserved5;	/* 	0x18	Reserved								*/
			uint16_t	reserved6;	/* 	0x1A	Reserved								*/
			uint16_t	reserved7;	/* 	0x1C	Reserved								*/
			uint16_t	reserved8;	/* 	0x1E	Reserved								*/
			uint16_t	scl;		/*	0x20	Write I2C SCL output					*/
			uint16_t	sda;		/*	0x22	Write I2C SDA output					*/
			uint16_t	onewdat;	/*	0x24	Write ONEWDAT output					*/
			uint16_t	teds1;		/*	0x26	Write TEDS1 output						*/
			uint16_t	teds2;		/*	0x28	Write TEDS2 output						*/
			uint16_t	reserved9;	/*	0x2A	Reserved								*/
			uint16_t	reserved10;	/*	0x2C	Reserved								*/
			uint16_t	reserved11;	/*	0x2E	Reserved								*/
			uint16_t	txrlycs;	/*	0x30	Write txdr relay control chip select	*/
			uint16_t	txrlydin;	/*	0x32	Write txdr relay control data input		*/
			uint16_t	txrlyclk;	/*	0x34	Write txdr relay control clock input	*/
			uint16_t   reserved12;	/*  0x36	Reserved								*/
			uint16_t	dacen;		/*	0x38	Write DAC/exc voltage output enable		
												Bit 0	Channel 0 excitation enable 
												Bit 1	Channel 1 excitation enable
												Bit 2	DAC enable					*/
			uint16_t	reserved13;	/*	0x3A	Reserved								*/
			uint16_t	test;		/*	0x3C	Write test register						*/
			uint16_t	reserved14;	/*	0x3E	Reserved								*/
		} wr;
		struct {
			uint16_t	reserved0;	/*  0x00	Reserved								*/
			uint16_t	reserved1;	/*  0x02	Reserved								*/
			uint16_t	reserved2;	/*  0x04	Reserved								*/
			uint16_t	reserved3;	/*  0x06	Reserved								*/
			uint16_t	reserved4;	/*  0x08	Reserved								*/
			uint16_t	reserved5;	/*  0x0A	Reserved								*/
			uint16_t	reserved6;	/*  0x0C	Reserved								*/
			uint16_t	ctrl;		/*	0x0E	Read control register
												Bit 0	Channel 0 DC/AC flag
												Bit 1	Channel 1 DC/AC flag		*/
			int		adc1;		/*	0x10	Read ADC1 result						*/
			int		adc2;		/*	0x14	Read ADC2 result						*/
			int		adc3;		/*	0x18	Read ADC3 result						*/
			int		adc4;		/*	0x1C	Read ADC4 result						*/
			uint16_t	scl;		/*	0x20	Read I2C SCL input						*/
			uint16_t	sda;		/*	0x22	Read I2C SDA input						*/
			uint16_t	onewdat;	/*	0x24	Read ONEWDAT input						*/
			uint16_t	teds1;		/*	0x26	Read TEDS1 input						*/
			uint16_t	teds2;		/*	0x28	Read TEDS2 input						*/
			uint16_t	fault;		/*  0x2A	Read fault status inputs				*/
			uint16_t	guard;		/*  0x2C	Read guard input status
												Bit 0	Guard input 1 state
												Bit 1	Guard input 2 state			*/
			uint16_t	reserved8;	/*  0x2E	Reserved								*/
			uint16_t	reserved9;	/*  0x30	Reserved								*/
			uint16_t	reserved10;	/*  0x32	Reserved								*/
			uint16_t	reserved11;	/*  0x34	Reserved								*/
			uint16_t	reserved7;	/*  0x36	Reserved								*/
			uint16_t	dacen;		/*	0x38	Read DAC/exc voltage output enable		
												Bit 0	Channel 0 excitation enable 
												Bit 1	Channel 1 excitation enable
												Bit 2	DAC enable					*/
			uint16_t	encoder;	/*  0x3A	Read encoder state						*/
			uint16_t	test;		/*  0x3C	Read test register						*/
			uint16_t	ident;		/*  0x3E	Read identifier register				*/
		} rd;
	} u;
} hw_mboard;

/* Hardware flags */

#define CHAN1_AC	0x01		/* Channel 1 AC transducer flag		*/
#define CHAN2_AC	0x02		/* Channel 2 AC transducer flag		*/

#define CHAN1_EXC	0x01		/* Channel 1 excitation enable flag */
#define CHAN2_EXC	0x02		/* Channel 2 excitation enable flag	*/
#define DAC_EN		0x04		/* DAC voltage enable flag			*/

/* Definition of the mainboard specific information structure contained within
   the generic channel definition structure (see channel.h).
*/

#define WSIZE_MBOARD	2		/* Workspace size */

/* Information structure for MBOARD card

   The first 5 entries of this structure must match with those in the
   2GP1SV and MBOARD information structures.
*/

typedef struct info_mboard {
	hw_mboard *hw;				/* Hardware base address							*/
	int   input;				/* Input channel number on mainboard				*/
	int   output;               /* Output channel number on mainboard               */
	uint8_t *relaystate;			/* Pointer to copy of relay state outputs			*/
	uint8_t  ledmask;				/* Mask for LED access								*/
	uint8_t *ledstate;				/* Pointer to copy of LED state outputs				*/
	uint8_t  work[WSIZE_MBOARD];	/* Local workspace
									Used to hold copy of relay and LED states for
									all channels on this card						*/
} info_mboard;

/* Function prototypes */

uint32_t hw_mboard_install(int slot, int *numinchan, int *numoutchan, int *nummiscchan, 
	 					int *numdigiochan, hw_mboard *hw, int flags, uint32_t *used_eecal_addr,
	 					uint32_t *used_eecfg_addr, uint32_t *read_eecfg_addr);
uint32_t hw_mboard_install_virtual(int slot, int *numinchan, uint32_t eecal_addr, uint32_t eecfg_addr, 
								uint32_t eecfg_rdaddr, int flags);
void hw_mboard_save(int slot, uint32_t status);

#endif
