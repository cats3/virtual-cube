/********************************************************************************
 * MODULE NAME       : debug.h													*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : Headers for debug routines.								*
 ********************************************************************************/

#ifndef __DEBUG_H
#define __DEBUG_H

#include <stdlib.h>
#include <string.h>

#undef LED_MONITOR

#ifdef _DSPA

extern uint8_t  debug_log_data[256];
extern uint32_t debug_log_ptr;

//extern void debug(char *fmt, ...);
void debug(char *msg, uint32_t val1, uint32_t val2);

#define LOG_START { debug_log_ptr = 0; memset(debug_log_data, 0, sizeof(debug_log_data)); }

//#define LOG_RECORD(x) { if (debug_log_ptr != (sizeof(debug_log_data)-1)) debug_log_data[debug_log_ptr++] = (x); }
#define LOG_RECORD(x) { debug_log_data[debug_log_ptr++] = (x); if (debug_log_ptr == sizeof(debug_log_data)) debug_log_ptr = 0; }
#define LOG_WORD(x) { LOG_RECORD(x >> 24); LOG_RECORD(x >> 16); LOG_RECORD(x >> 8); LOG_RECORD(x); }

#define LOG_DUMP(s,l) { int n; uint8_t *p=(s); n=(l); while(n--) {LOG_RECORD(*p++);} }

#endif

#ifdef LED_MONITOR
#define LOG_LED(n,s)  { *((volatile uint16_t *)(0xB0003C00+(n<<1))) = (s & 1); }
#else
#define LOG_LED(n,s)
#endif

#endif
