/********************************************************************************
 * MODULE NAME       : hw_4ad2da.h												*
 * MODULE DETAILS    : Header for 4AD2DA specific routines						*
 *																				*
 ********************************************************************************/

#ifndef __HW_4AD2DA_H
#define __HW_4AD2DA_H

#include <stdint.h>

/* Definition of the hardware interface on the 4AD2DA card */

typedef struct hw_4ad2da {
	union {
		struct {
			uint16_t	reserved0;	/* 	0x00	Reserved                       			*/
			uint16_t	reserved1;	/* 	0x02	Reserved                   				*/
			uint16_t	reserved2;	/*	0x04	Reserved                   				*/
			short	dac0;		/*	0x06	Write DAC 0                          	*/
			short	dac1;		/*	0x08	Write DAC 1                          	*/
			uint16_t	reserved3;	/*	0x0A	Reserved								*/
			uint16_t	reserved4;	/*	0x0C	Reserved								*/
			uint16_t	reserved5;	/*	0x0E	Reserved   								*/
			uint16_t	reserved6;	/* 	0x10	Reserved   								*/
			uint16_t	reserved7;	/* 	0x12	Reserved   								*/
			uint16_t	reserved8;	/* 	0x14	Reserved   								*/
			uint16_t	reserved9;	/* 	0x16	Reserved   								*/
			uint16_t	reserved10;	/* 	0x18	Reserved   								*/
			uint16_t	reserved11;	/* 	0x1A	Reserved   								*/
			uint16_t	reserved12;	/* 	0x1C	Reserved   								*/
			uint16_t	reserved13;	/* 	0x1E	Reserved   								*/
			uint16_t	reserved14;	/*	0x20	Reserved               					*/
			uint16_t	reserved15;	/*	0x22	Reserved               					*/
			uint16_t	onewdat;	/*	0x24	Write ONE-WIRE command					*/
			uint16_t	reserved16;	/*	0x26	Reserved           						*/
			uint16_t	reserved17;	/*	0x28	Reserved           						*/
			uint16_t	reserved18;	/*	0x2A	Reserved   								*/
			uint16_t	reserved19;	/*	0x2C	Reserved   								*/
			uint16_t	reserved20;	/*	0x2E	Reserved   								*/
			uint16_t	reserved21;	/*	0x30	Reserved                              	*/
			uint16_t	reserved22;	/*	0x32	Reserved                           		*/
			uint16_t	reserved23;	/*	0x34	Reserved                               	*/
			uint16_t	reserved24;	/*	0x36	Reserved                               	*/
			uint16_t	dacen;		/*	0x38	Write DAC voltage output enable			*/
			uint16_t	reserved25;	/*	0x3A	Reserved								*/
			uint16_t	test;		/*	0x3C	Write test register						*/
			uint16_t	reserved26;	/*	0x3E	Reserved								*/
		} wr;
		struct {
			uint16_t	reserved0;	/*  0x00	Reserved								*/
			uint16_t	reserved1;	/*  0x02	Reserved								*/
			uint16_t	reserved2;	/*  0x04	Reserved								*/
			uint16_t	reserved3;	/*  0x06	Reserved								*/
			uint16_t	reserved4;	/*  0x08	Reserved								*/
			uint16_t	reserved5;	/*  0x0A	Reserved								*/
			uint16_t	reserved6;	/*  0x0C	Reserved								*/
			uint16_t	reserved7;	/*	0x0E	Reserved								*/
			int		adc1;		/*	0x10	Read ADC1 result						*/
			int		adc2;		/*	0x14	Read ADC2 result 						*/
			int		adc3;		/*	0x18	Read ADC3 result 						*/
			int		adc4;		/*	0x1C	Read ADC4 result 						*/
			uint16_t	reserved8;	/*	0x20	Reserved								*/
			uint16_t	reserved9;	/*	0x22	Reserved								*/
			uint16_t	onewdat;	/*	0x24	Read ONE-WIRE status					*/
			uint16_t	reserved10;	/*	0x26	Reserved								*/
			uint16_t	reserved11;	/*	0x28	Reserved								*/
			uint16_t	fault;		/*  0x2A	Read fault status inputs				*/
			uint16_t	reserved12;	/*	0x2C	Reserved								*/
			uint16_t	reserved13;	/*	0x2E	Reserved								*/
			uint16_t	reserved14;	/*	0x30	Reserved								*/
			uint16_t	reserved15;	/*	0x32	Reserved								*/
			uint16_t	reserved16;	/*	0x34	Reserved								*/
			uint16_t	reserved17;	/*	0x36	Reserved								*/
			uint16_t	reserved18;	/*	0x38	Reserved								*/
			uint16_t	reserved19;	/*	0x3A	Reserved								*/
			uint16_t	test;		/*  0x3C	Read test register						*/
			uint16_t	ident;		/*  0x3E	Read identifier register				*/
		} rd;
	} u;
} hw_4ad2da;

/* Definition of the 4AD2DA specific information structure contained within
   the generic channel definition structure (see channel.h).
*/

#define WSIZE_4AD2DA	1		/* Workspace size */

/* Information structure for 4AD2DA card

   The first 5 entries of this structure must match with those in the
   2GP1SV and MBOARD information structures.
*/

typedef struct info_4ad2da {
	hw_4ad2da *hw;				/* Hardware base address							*/
	int   input;				/* Input channel number on 4AD2DA card				*/
	int   output;				/* Output channel number on 4AD2DA card				*/
	uint8_t  ledmask;				/* Mask for LED access								*/
	uint8_t *ledstate;				/* Copy of LED state outputs						*/
	uint8_t  work[WSIZE_4AD2DA];	/* Local workspace									*/
} info_4ad2da;

/* Function prototypes */

uint32_t hw_4ad2da_install(int slot, int *numinchan, int *numoutchan, int *nummiscchan, 
					    hw_4ad2da *hw, int flags);
void hw_4ad2da_save(int slot, uint32_t status);

#endif
