/********************************************************************************
 * MODULE NAME       : hydraulic.h												*
 * PROJECT			 : Control Cube												*
 * MODULE DETAILS    : Header for hydraulic control routines					*
 ********************************************************************************/

#ifndef __HYDRAULIC_H
#define __HYDRAULIC_H

#include <stdint.h>

#include "hardware.h"

/* Structure holding definitions for hydraulic inputs and outputs */

typedef struct hyd_def {
  uint32_t flags;								/* Flags controlling hydraulic operation
  												Bit 0:		Function available
  												Bit 1:		Enable switch off delay (output)							
												Bit 2:  	For type 0 hydraulic LP output 
															only, turn off LP when selecting 
															HP
  												Bits 3-31:	Reserved						*/
  char label[32];							/* User label									*/					
  uint32_t timeout;							/* Timeout for input function (20ms intervals)	*/
} hyd_def;

/* Hydraulic definition flags */

#define HYD_FLAG_AVAILABLE		0x00000001	/* Hydraulic function available					*/
#define HYD_FLAG_OFFDELAY		0x00000002	/* Enable switch off delay						*/
#define HYD_FLAG_HPLPOFF		0x00000004	/* For type 0 hydraulic LP output only, turn 
												off LP when selecting HP					*/

#define GLOBAL_HYD_REQUEST_DURATION 200;	/* Duration of global hydraulic request 
											    (200 = 1sec)								*/

extern uint32_t global_hyd_request_out;		/* Global hydraulic request	output to CNet		*/
extern uint32_t global_hyd_request_ctr;		/* Counter for hydraulic request auto clear		*/
extern uint32_t global_hyd_request_in;			/* Global hydraulic request	input from CNet		*/
extern uint32_t global_hyd_status_out;			/* Global hydraulic status output to CNet		*/
extern uint32_t global_grp_status_out;			/* Global group status output to CNet			*/

extern uint32_t hyd_io_state;					/* Raw hydraulic I/O state						*/

/* EXPORT BEGIN */

/* Define hydraulic operating modes */

#define HYD_SLAVE				0			/* Slave mode		*/
#define HYD_STANDALONE			1			/* Standalone mode	*/
#define HYD_MASTER				2			/* Master mode		*/

/* Define hydraulic type codes */

#define HYD_TYPE_HYDRAULIC		0			/* Direct hydraulic control						*/
#define HYD_TYPE_HYDRAULIC_SIM	1			/* Simulated direct hydraulic control			*/
#define HYD_TYPE_PUMP			2			/* Pump control									*/
#define HYD_TYPE_PUMP_SIM		3			/* Simulated pump control						*/

#define HYD_SIM_MASK			0x01		/* Simulation mode mask 						*/

#define HYD_MODE_SHIFT			30			/* Position of hydraulic mode bits				*/
#define HYD_GROUP_SHIFT			28			/* Position of hydraulic group bits				*/

/* Define hydraulic requests */

#define REQUEST_OFF				1			/* Pressure off 	*/
#define REQUEST_2				2			/* State 2			*/
#define REQUEST_3				3			/* State 3			*/
#define REQUEST_4				4			/* State 4			*/
#define REQUEST_SHUTDOWN		0xFF		/* Shutdown			*/

#define REQUEST_GLBL_SHUTDOWN	0xFFFFFFFF	/* Global shutdown	*/

/* Hydraulic mode types and masks */

#define HYD_MODE_MASK			0xC0000000	/* Hydraulic mode mask									*/
#define HYD_MODE_STANDALONE		0x40000000	/* Standalone mode										*/
#define HYD_MODE_MASTER			0x80000000	/* Master mode											*/
#define HYD_MODE_SLAVE			0x00000000	/* Slave mode											*/
#define HYD_GROUP_MASK			0x30000000	/* Hydraulic group number mask							*/
#define HYD_1CHAN_ESTOP			0x00000100	/* Single channel e-stop circuit						*/
#define HYD_HC_NOVALID			0x00000200	/* Disable validation requirement on hand 
											   controller e-stop									*/
#define HYD_DIS_FORCE_SETUP		0x00000400	/* Disable Force setup mode when guard input		
											   (Setup mode will be forces when guard input if LOW)	*/
#define HYD_DIS_HIGH_PRES		0x00000800	/* Disable high pressure when guard input			
											   (high pressure will be disabled 
										 	   when guard input if set)								*/
#define HYD_SOFT_STOP			0x00001000	/* Select soft stop option (for hydraulic types
											   2 and 3 only)										*/
#define HYD_TYPE_MASK			0x00000003	/* Hydraulic type mask
											     0 = Hydraulic
											     1 = Hydraulic simulation
											     2 = Pump
											     3 = Pump simulation								*/
							
/* Hydraulic type definitions. Simulation modes must have the least significant bit of the
   type code set.
*/

#define HYD_TYPE_HYD		0
#define HYD_TYPE_HYDSIM		1
#define HYD_TYPE_PUMP		2												
#define HYD_TYPE_PUMPSIM	3

/* Define hydraulic output flags */

#define HYD_OP_SETUP			0x10		/* Setup mode relay drive */

/* Define hydraulic acknowledgement states */

#define HYD_ACK_0				0			/* E-stop off		*/
#define HYD_ACK_1				1			/* Pressure off		*/
#define HYD_ACK_2				2
#define HYD_ACK_3				3
#define HYD_ACK_4				4

#define HYD_STATUS_MASK			0xFF		/* Mask to extract hydraulic status 			*/
#define HYD_TRACKING_MASK		0x80000000	/* Mask to extract hydraulic tracking status 	*/

#define HYD_EVENT_MASK			0x00FF00FF	/* Mask used to generate hydraulic events 		*/

#define HYD_ACK_ON				2			/* Minimum on state for hydraulics 				*/

#define HYD_ACK_INVALID			0x100		/* E-stop invalid (or not validated) 			*/
#define HYD_ACK_INVALID_MASTER	0x200		/* Master e-stop chain not validated 			*/
#define HYD_ACK_INVALID_HC		0x400		/* Hand-controller e-stop chain not validated 	*/

/* EXPORT END */

#define HYD_UPDATE				0			/* Perform hydraulic update 		*/
#define HYD_MONITOR				1			/* Monitor external hydraulic state */

/* Define group status flags */

#define GRP_HYDINVALID			0x01		/* Invalid hydraulic state 			*/
#define GRP_SHUNTCAL			0x02		/* Shunt calibration active			*/
#define GRP_STARTINHIBIT		0x04		/* Start inhibited					*/
#define GRP_INVALIDPRESSURE		0x08		/* Invalid pressure state			*/
#define GRP_TXDRFAULT			0x10		/* Transducer fault					*/
#define GRP_GUARDFAULT			0x20		/* Guard input fault				*/
#define GRP_ESTOPFAULT			0x40		/* Emergency stop fault				*/

/* Digital input states */

extern uint16_t dig_ip_state;					/* Digital input state (note 16 bit 
											   to match exp card hardware 
											   register)						*/

extern uint32_t hyd_get_status(void);
extern void  hyd_unload(void);
extern uint32_t hyd_set_dissipation(uint32_t time);
extern uint32_t hyd_read_dissipation(void);
extern uint32_t hyd_control(uint32_t req);
extern uint32_t hyd_set_io_definition(uint32_t ident, uint32_t flags, char *label, uint32_t timeout);
extern void  hyd_read_io_definition(uint32_t ident, uint32_t *flags, char *label, uint32_t *timeout);
extern uint32_t hyd_get_valid(void);
extern uint32_t hyd_set_mode(uint32_t mode, uint32_t group, uint32_t type);
extern void  hyd_read_mode(uint32_t *mode, uint32_t *group, uint32_t *type);
extern uint32_t hyd_read_simulation_mode(void);
extern void  hyd_read_state(uint32_t *state, uint32_t *ack);
extern uint32_t hyd_get_raw_io_state(void);

uint32_t hyd_set_name(char* name);
void hyd_read_name(char* name);
uint32_t hyd_check_request(uint32_t request);

extern void  hyd_initialise(void);
void hyd_control_initialise(void);

extern void  hyd_init_hw_masks(void);
extern void  hyd_update(int state);
extern uint8_t  hyd_modify_op(uint8_t set, uint8_t clr);
extern uint8_t  hyd_read_ip(void);
extern void  get_estop_state(uint32_t *estop, uint32_t *slave, uint32_t *state);

extern void  grp_update_status(void);
extern void  grp_update_shuntcal(int state);

#endif
