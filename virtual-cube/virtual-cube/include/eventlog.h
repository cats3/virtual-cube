/********************************************************************************
 * MODULE NAME       : eventlog.h												*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : Defines structure used for maintaining event log.		*
 ********************************************************************************/

#ifndef __EVENTLOG_H
#define __EVENTLOG_H

#include <stdint.h>

#define EVENTLOG_SIZE 88

/* Event log entry */

typedef struct eventlog_entry {
  uint32_t timestamp_lo;	/* Timestamp low word	*/
  uint32_t timestamp_hi;	/* Timestamp high word	*/
  uint32_t type;			/* Event type			*/
  uint32_t parameter1;		/* General parameter 1	*/
  uint32_t parameter2;		/* General parameter 2	*/
} eventlog_entry;

/* Event log storage in battery-backed RAM */

typedef struct eventlog {
  uint32_t inptr;								/* Input pointer (offset into log area)			*/
  uint32_t outptr;								/* Output pointer (offset into log area)		*/
  uint32_t free;								/* Number of free entries in log				*/
  uint32_t reserved;							/* Reserved										*/
  eventlog_entry log[EVENTLOG_SIZE];		/* Event log data area							*/  
} eventlog;

/* Structure defining FIFO used for event log transfer from DSP B to DSP A */

//typedef struct eventlog_fifo {
//  uint32_t inptr;								/* Input pointer (offset into log area)			*/
//  uint32_t outptr;								/* Output pointer (offset into log area)		*/
//  eventlog_entry log[64];					/* 1024 bytes used for FIFO data				*/  
//} eventlog_fifo;

/* Pointer to non-volatile event log memory */

extern volatile eventlog * const eventlog_ptr;	/* Event log memory area */

uint32_t eventlog_read(eventlog_entry *entry);
uint32_t eventlog_log(uint32_t type, uint32_t p1, uint32_t p2);
uint32_t eventlog_write(eventlog_entry *entry);
void  eventlog_flush(void);
void  eventlog_update(void);
void  eventlog_status(uint32_t *entries);
void  eventlog_init(void);

#endif
