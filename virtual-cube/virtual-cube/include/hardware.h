/********************************************************************************
 * MODULE NAME       : hardware.h												*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : Headers for hardware control routines					*
 ********************************************************************************/

#ifndef __HARDWARE_H
#define __HARDWARE_H

#include <stdint.h>

 /* Front panel LED diagnostic codes */

#define DIAG_SDRAM		1			/* SDRAM error (boot code only)			*/
#define DIAG_NOCODE		2			/* Invalid code (boot code only)		*/
#define DIAG_FPGAIDENT	3			/* Bad FPGA ident code (boot code only)	*/
#define DIAG_LOADER		4			/* System in loader mode				*/
#define DIAG_FPGALOAD	5			/* Failed FPGA bitstream load operation	*/
#define DIAG_ESTOP		6			/* Emergency stop fault					*/
#define DIAG_HWFAULT	7			/* Hardware fault						*/
#define DIAG_MSGSERVER	8			/* Message server startup error			*/
#define DIAG_MALLOC		9			/* Malloc failure						*/
#define DIAG_SOCKET		10			/* Socket allocation failure			*/
#define DIAG_MBUF		11			/* Mbuf allocation failure				*/
#define DIAG_MMUALLOC	12			/* Ethernet MMU allocation failure		*/
#define DIAG_CNETALLOC	13			/* CNet slot allocation failure			*/
#define DIAG_MISC		15			/* Miscellaneous failure				*/

#if (defined(CONTROLCUBE) || defined(AICUBE))

#define MAX_SLOT 4

#endif

#ifdef SIGNALCUBE

#define MAX_SLOT 7

#endif

/* Boot log data structure */

struct blog {
	uint32_t valid;						/* Validity check 			*/
	uint8_t  log;						/* Current log code			*/
	uint8_t  padding[3];
	uint32_t seq_a;						/* Log A sequence counter	*/
	uint8_t	data_a[16];					/* Log A data				*/
	uint32_t seq_b;						/* Log B sequence counter	*/
	uint8_t	data_b[16];					/* Log B data				*/
	uint32_t tseq_a;						/* Log A trace counter		*/
	uint8_t  tdata_a[64];				/* Log A trace data			*/
	uint32_t tseq_b;						/* Log B trace counter		*/
	uint8_t  tdata_b[64];				/* Log B trace data			*/
};

/* Local Ethernet MAC address */

extern unsigned char local_mac[6];

/* Expansion slot hardware table */

extern uint16_t* slottable[MAX_SLOT];

/* Pointer to battery-backed RAM area */

extern uint8_t* bbsram_base;

/* Masks for hydraulic/safety relay control */

#define RLY_HIGH		0x01		/* High pressure relay control		*/
#define RLY_LOW			0x02		/* Low pressure relay control		*/
#define RLY_PILOT		0x08		/* Pilot pressure relay control 	*/
#define RLY_SETUP		0x80		/* Setup mode relay control			*/

/* Masks for local I/O control/monitoring */

#define EM1L_SENSE		0x0001		/* E-stop 1 low side sense			*/
#define EM_SENSE		0x0002		/* Emergency stop input sense		*/
#define HIGH_SENSE		0x0004		/* High pressure input sense		*/
#define EM1H_SENSE		0x0008		/* E-stop 1 high side sense			*/
#define PILOT_SENSE		0x0010		/* Pilot pressure input sense		*/
#define LOW_SENSE		0x0020		/* Low pressure input sense			*/	
#define GPIN1_SENSE		0x0040		/* General-purpose input 1 sense	*/
#define GPIN2_SENSE		0x0080		/* General-purpose input 2 sense	*/
#define ES_RELAY_SLAVE	0x0100		/* Emergency stop relay slave sense	*/
#define GPOUT1_DRIVE	0x0200		/* General-purpose output 1 drive	*/
#define GPOUT2_DRIVE	0x0400		/* General-purpose output 2 drive	*/
#define TXDR1LED_DRIVE	0x0800		/* Transducer 1 LED drive			*/
#define TXDR2LED_DRIVE	0x1000		/* Transducer 2 LED drive			*/
#define SVLED_DRIVE		0x2000		/* SV LED drive						*/
#define EM2H_SENSE		0x4000		/* E-stop 2 high side sense			*/
#define EM2L_SENSE		0x8000		/* E-stop 2 low side sense			*/

#define BITPOS_GPIN1	6			/* Bit position for GPIN1			*/

/* System EEPROM I2C address */

#if (defined(CONTROLCUBE) || defined(AICUBE))
#define SYS_EEPROM		0xAE		/* I2C address of system EEPROM				*/
#endif
#ifdef SIGNALCUBE
#define SYS_EEPROM		0xA0		/* I2C address of system EEPROM				*/
#endif

/* One-wire action codes */

#define ONEW_RESET		0x04		/* Reset 1-wire bus and detect presence		*/
#define ONEW_READ		0x05		/* Read 1-wire bus							*/
#define ONEW_WRITE0		0x06		/* Write bit 0 to 1-wire bus				*/
#define ONEW_WRITE1		0x07		/* Write bit 1 to 1-wire bus				*/

/* One-wire control flags */

#define ONEW_INIT		0x08		/* Initialise 1-wire controller (2DIG only)	*/

/* Masks for 1-wire status */

#define ONEW_DATA		0x01		/* 1-wire data bit							*/
#define ONEW_PRES		0x02		/* Presence bit for 1-wire bus				*/
#define ONEW_BUSY		0x04		/* 1-wire bus operation is busy				*/

/* Front panel LED control flags */

#define LED_OFF			0x00		/* Turn front panel LED off					*/
#define LED_ON			0x01		/* Turn front panel LED on					*/
#define LED_PULSE		0x02		/* Pulse front panel LED					*/

/* Flag for waiting in flash erase routine */

#define ERASE_NO_WAIT		0
#define ERASE_WAIT			1

/* Flash erase status codes */

#define ERASE_BUSY			0
#define ERASE_COMPLETE		1
#define ERASE_ERROR			2
#define ERASE_ILLEGAL_BLOCK	3

/* Flash program status codes */

#define PROG_COMPLETE		1
#define PROG_ERROR			2

/* System state inputs */

#define IP_LOADER		0x04			/* LOADER sense	input							*/
#define IP_FANSENSE		0x10			/* Fan sense input								*/
#define IP_SLAVE		0x20			/* Slave board sense input						*/
#define	IP_INITIALISE	0x40			/* System re-initialise input					*/
#define IP_POWERFAIL	0x80			/* Powerfail sense input						*/

/* I2C bus control */

#define I2C_BUS			0x40			/* Select left or right system bus				*/

/* Hardware related variables */

extern uint32_t hw_restart_delay;			/* Timer for hardware restart operation			*/
extern uint32_t hw_restart_enable;			/* Enable flag for hardware restart operation	*/

extern volatile uint16_t* const hw_fault;	/* Hardware fault inputs 						*/

/* Bit masks for hw_fault register in FPGA 3 */

#define HW_REVF			0x8000			/* Revision F board ident 0 = Revision F
																  1 = Revisions A - E	*/
																  /* Hardware function flags returned by get_hw_version function */

#define HW_FLAG_REVF	0x0001			/* Revision F board ident 1 = Revision F
																  0 = Revisions A - E	*/

																  /* Guard states */

#define GUARD_NOSUPPORT	0				/* Guard input not supported					*/
#define GUARD_OPEN		1				/* Guard open									*/
#define GUARD_CLOSED	2				/* Guard closed									*/
#define GUARD_FAULT		3				/* Guard fault condition detected				*/

/* Hand controller commands and key codes */

#define HC_CMD_NORMAL		0x1E
#define HC_CMD_TESTMODE		0x1F

#define HC_KEY_TESTMODE1	0x0A		/* Testmode output loom type 1			*/
#define HC_KEY_TESTMODE2	0x0C		/* Testmode output loom type 2
										   (bits 1 & 2 swapped)					*/
#define HC_KEY_TESTMODE3	0x09		/* Testmode output loom type 3
										   (bits 0 & 1 swapped)					*/

										   /* Hand controller identification states */

#define HC_UNKNOWN			0
#define HC_IDENTIFYING		1
#define HC_NOTPRESENT1		2			/* No hand controller, loom type 1		*/
#define HC_PRESENT1			0x80000003	/* Hand controller present, loom type 1	*/
#define HC_NOTPRESENT2		0x01000004	/* No hand controller, loom type 2		*/
#define HC_PRESENT2			0x81000005	/* Hand controller present, loom type 2	*/
#define HC_PRESENT3			0x82000006	/* Hand controller present, loom type 3	*/

#define HC_LOOMMASK			0x0F000000	/* Loom type mask						*/
#define	HC_PRESENTMASK		0x80000000	/* Hand controller present mask			*/
#define HC_UNPLUGGED		0x40000000  /* Hand controller unplugged			*/

/* Hand controller shared memory addresses */

#define HC_KEYSTATE 0x30	/* DSP B address of hand controller key state word	 */
#define HC_STATUS 	0x34	/* DSP B address of hand controller status word	 	 */

/* Function prototypes */
// TODO: extern, really?
void dsp_reset_peripherals(void);
void boot_dspb(uint8_t* code, uint32_t destination, uint32_t length, uint32_t start);

extern uint8_t  get_hw_version(uint32_t* bootver, uint32_t* flags);
extern void  diag_error(uint32_t err);
extern uint32_t dsp_check_c6713(void);
extern uint32_t dsp_set_cache(uint32_t mode);

extern void  atomic_pcr0_wr(uint32_t set, uint32_t clr);

extern void  dsp_i2c_lock(void);
extern void  dsp_i2c_unlock(void);
extern int   dsp_i2c_free(void);
extern void  dsp_i2c_restart(void);
extern void  dsp_i2c_start(void);
extern void  dsp_i2c_stop(void);
extern int   dsp_i2c_send(uint8_t data);
extern uint8_t  dsp_i2c_read(int ack);
extern int   dsp_i2c_send_word(uint32_t word);
extern uint32_t dsp_i2c_read_word(int ack);
extern void  dsp_i2c_eeprom_writeblock(uint8_t i2c_addr, int addr, uint8_t* data, int len, int flags);
extern void  dsp_i2c_eeprom_readblock(uint8_t i2c_addr, int addr, uint8_t* data, int len, int flags);
int dsp_i2c_eeprom_writeaddr(uint8_t i2c_addr, int addr);

extern void  sys_i2c_lock(void);
extern void  sys_i2c_unlock(void);
extern int   sys_i2c_free(void);
extern void  sys_i2c_restart(uint8_t i2c_addr);
extern void  sys_i2c_start(uint8_t i2c_addr);
extern void  sys_i2c_stop(uint8_t i2c_addr);
extern int   sys_i2c_send(uint8_t i2c_addr, uint8_t data);
extern uint8_t  sys_i2c_read(uint8_t i2c_addr, int ack);
extern int   sys_i2c_send_word(uint8_t i2c_addr, uint32_t word);
extern uint32_t sys_i2c_read_word(uint8_t i2c_addr, int ack);
extern int   sys_i2c_eeprom_write(uint8_t i2c_addr, int addr, uint8_t data);
extern int   sys_i2c_eeprom_read(uint8_t i2c_addr, int addr, uint8_t* data);
extern void  sys_i2c_eeprom_writeblock(uint8_t i2c_addr, int addr, uint8_t* data, int len, int flags);
extern void  sys_i2c_eeprom_readblock(uint8_t i2c_addr, int addr, uint8_t* data, int len, int flags);

extern void  dspb_i2c_restart(void);
extern void  dspb_i2c_start(void);
extern void  dspb_i2c_stop(void);
extern int   dspb_i2c_send(uint8_t data);
extern uint8_t  dspb_i2c_read(int ack);

int dsp_1w_checkcrc(uint32_t lo, uint32_t hi);

extern void  hw_1w_init(uint8_t* control);
extern int   hw_1w_reset(uint8_t* control);
extern void  hw_1w_write(uint8_t* control, uint8_t data);
extern uint8_t  hw_1w_read(uint8_t* control);

extern void  dsp_spi_update(void);
extern void  dsp_spi_hyd_init(uint8_t data);
void dsp_spi_send(uint8_t data);

extern uint8_t  operate_relay(uint8_t set, uint8_t clr);
extern uint16_t local_io(uint16_t set, uint16_t clr);
extern uint16_t local_io_rdop(void);
extern void  dsp_wdoga_state(int state);

extern void  config_mainboard_fpga(uint8_t* data);
extern void  config_exp_fpga(uint8_t* data1, uint8_t* data2, uint8_t* data3);

extern void  hpi_blockwrite(uint8_t* dst, uint8_t* src, uint32_t size);
extern void  hpi_blockread(uint8_t* dst, uint8_t* src, uint32_t size);

extern uint8_t* alloc_dspb_general(uint32_t size);

extern uint32_t flash_erase(uint32_t wait);
extern uint32_t erase_status(void);
extern uint32_t flash_erase_block(uint32_t block, uint32_t wait);
extern uint32_t flash_program_block(uint32_t dst, uint8_t* src, uint32_t length);

extern void  serial_transmit(uint8_t data);
extern void  serial_write(char* fmt, ...);

extern void  dsp_fp_pulse(void);

extern void  fpled_ctrl(uint8_t ctrl);
extern void  fp_init(void);

extern uint8_t  fp_read_state(void);
extern void  fp_write_state(uint8_t fp_state);

extern uint8_t  get_sys_state(void);
extern void  check_initialise(void);

extern void  hw_bootlog_init(void);
extern void  hw_bootlog(uint32_t seq, uint8_t* data, uint32_t len);
extern void  hw_tracelog(uint8_t seq);
extern void  hw_showlog(char* args[], int numargs, int skt);

extern void  hw_check_guard(void);
extern uint32_t hw_get_guard_state(void);

#endif

