// virtual-hardware.h

#ifndef VIRTUAL_HARDWARE_H
#define VIRTUAL_HARDWARE_H

#include <stdint.h>

#include "virtual-eeprom.h"


struct hw_params {
	uint8_t hw_version;
	uint8_t bootver;
	uint16_t hw_fault;
	uint8_t fpga_ctrl;
	uint8_t dsp_hyd_output;
	uint32_t one_wire_rom_lo;
	uint32_t one_wire_rom_hi;
	uint16_t local_io_inputs;
	uint16_t local_io_outputs;
	uint8_t fp_state;
	uint8_t initialise_button;
	uint8_t fp_led;
};

void vh_initialise(void);
struct virtual_eeprom* vh_get_onewire_eeprom(void);
struct virtual_eeprom* vh_get_eeprom_by_address(uint8_t address);

#endif
