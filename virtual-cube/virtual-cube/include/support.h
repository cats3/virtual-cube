/********************************************************************************
 * MODULE NAME       : support.h												*
 * PROJECT		     : Control Cube / Signal Cube								*
 * MODULE DETAILS    : General support definitions.								*
 ********************************************************************************/


#ifndef __SUPPORT_H
#define __SUPPORT_H 

#include <stdint.h>
#include <string.h>

//typedef unsigned char   uint8_t;
//typedef unsigned short  uint16_t;
//typedef unsigned int    uint32_t;
typedef unsigned int    Uint32;
typedef signed char     s8_t;
typedef signed short    s16_t;
typedef signed int      s32_t;

typedef s32_t           int32_t;

typedef unsigned char   u_char;
typedef unsigned short  u_short;
typedef unsigned int    u_int;
typedef unsigned int    u_long;
typedef unsigned short  ushort;         /* Sys V compatibility  */

typedef char			INT8;
typedef short			INT16;
typedef int				INT32;
typedef unsigned char	UINT8;
typedef unsigned short	UINT16;
typedef unsigned int	UINT32;
typedef unsigned int	uint;
typedef void 		   	*HANDLE;

typedef unsigned char	byte;
typedef unsigned short	word;
typedef unsigned int	ulong;

typedef u_long  		n_long;         /* long as received from the net */
typedef u_short 		n_short;        /* short as received from the net */
typedef u_long  		n_time;         /* ms since 00:00 GMT, byte rev */

#define SCHAR_MAX       127             /* min value for a signed char */
#define SCHAR_MIN       (-128)          /* max value for a signed char */

#define UCHAR_MAX       255             /* max value for an unsigned char */
#define CHAR_MAX        127             /* max value for a char */
#define CHAR_MIN        (-128)          /* min value for a char */

#define USHRT_MAX       65535           /* max value for an unsigned short */
#define SHRT_MAX        32767           /* max value for a short */
#define SHRT_MIN        (-32768)        /* min value for a short */

#define UINT_MAX        0xffffffff      /* max value for an unsigned int */
#define INT_MAX         2147483647      /* max value for an int */
#define INT_MIN         (-2147483647-1) /* min value for an int */

typedef unsigned int    vm_offset_t;
typedef unsigned int    vm_size_t;
typedef unsigned char   boolean_t;
typedef unsigned int    ioctl_cmd_t;
typedef unsigned int    ino_t;          /* inode number */
typedef int             pid_t;

typedef char            *caddr_t;       /* core address */

//typedef uint32_t           off_t;          /* file offset */

typedef s32_t           mach_error_t;   /* Guessed the type SN */
typedef s32_t           kern_return_t;  /* Guessed the type SN */
typedef s32_t           integer_t;      /* Guessed the type SN */

unsigned int    		htonl(unsigned int);
unsigned short  		htons(unsigned short);
unsigned int    		ntohl(unsigned int);
unsigned short  		ntohs(unsigned short);

#define NTOHL(x)        (x) = ntohl((u_int)x)
#define NTOHS(x)        (x) = ntohs((u_short)x)
#define HTONL(x)        (x) = htonl((u_int)x)
#define HTONS(x)        (x) = htons((u_short)x)

#define swap_word(x)	htons(x)
#define swap_ulong(x)	htonl(x)

typedef struct timeval {
  int32_t tv_sec;       /* seconds */
  int32_t tv_usec;      /* and microseconds */
} timeval;

void get_time(struct timeval *time);

#define timercmp(tvp, uvp, cmp)                   	\
        (((tvp)->tv_sec == (uvp)->tv_sec) ?         \
            ((tvp)->tv_usec cmp (uvp)->tv_usec) :   \
            ((tvp)->tv_sec cmp (uvp)->tv_sec))

void panic(char *msg);
int  splimp(void);
int  splnet(void);
void splx(int s);

#define bzero(ptr, len)                 memset(ptr, 0, len)
#define bcopy(src, dst, len)            memcpy(dst, src, len)
#define bcmp(src1, src2, len)           memcmp(src1, src2, len)
#define bsd_malloc(size, type, wait)    my_malloc(size)
#define bsd_free(ptr, type)             my_free(ptr)

#define MALLOC(space, cast, size, type, flags) \
        (space) = (cast)my_malloc(size)
#define FREE(addr, type) my_free((caddr_t)(addr))

#define min(a,b)        ((a < b) ? (a) : (b))
#define max(a,b)        ((a > b) ? (a) : (b))

#define imin(a,b)       ((a < b) ? (a) : (b))

#define roundup(x, y)   ((((x)+((y)-1))/(y))*(y))

struct queue_entry {
    struct queue_entry      *next;          /* next element */
    struct queue_entry      *prev;          /* previous element */
};

typedef struct queue_entry      *queue_t;
typedef struct queue_entry      queue_head_t;
typedef struct queue_entry      queue_chain_t;
typedef struct queue_entry      *queue_entry_t;

void _insque(struct queue_entry *entry, struct queue_entry *pred);
void _remque(struct queue_entry *elt);

#define insque(q,p)     _insque((queue_t)q,(queue_t)p)
#define remque(q)       _remque((queue_t)q)

#define MSIZE           128				/* size of an m_buf		*/
#define MCLBYTES        2048			/* size of a cluster	*/
//#define MCLBYTES        8192			/* size of a cluster	*/
#define NBPG            4096            /* bytes per page 		*/

#define KERN_SUCCESS    0
                         
#define LITES           1

#define BSD             44

#define KERNEL          1
#define KLD_MODULE		1

#define COMPAT_OLDSOCK  0

#define DIAGNOSTIC      1

#define GATEWAY			0

#define INET            1               /* Internet domain */
#define NS              0
#define ISO             0
#define CCITT           0
#define NIMP            0
#define NSIP            0
#define TPIP            0
#define EON             0
#define NHY             0

#define LITTLE_ENDIAN   1234            /* LSB first: i386, vax, alpha */
#define BIG_ENDIAN      4321            /* MSB first: 68000, ibm, net */
#define PDP_ENDIAN      3412            /* LSB first in word, MSW first in long */

#define BYTE_ORDER      LITTLE_ENDIAN

#define __CONCAT(x,y)   x ## y
#define __STRING(x)     #x

#define NBBY    8               /* number of bits in a byte */

/*
 * Priorities.  Note that with 32 run queues, differences less than 4 are
 * insignificant.
 */
#define PSWP    0
#define PVM     4
#define PINOD   8
#define PRIBIO  16
#define PVFS    20
#define PZERO   22              /* No longer magic, shouldn't be here.  XXX */
#define PSOCK   24
#define PWAIT   32
#define PLOCK   36
#define PPAUSE  40
#define PUSER   50
#define MAXPRI  127             /* Priorities range from 0 through MAXPRI. */

#define PRIMASK 0x0ff
#define PCATCH  0x100           /* OR'd with pri for tsleep to check signals */

#define NZERO   0               /* default "nice" */

#define NBPW    sizeof(int)     /* number of bytes per word (integer) */

#define CMASK   022             /* default file mask: S_IWGRP|S_IWOTH */
#define NODEV   (dev_t)(-1)     /* non-existent device */

/*
 * Select uses bit masks of file descriptors in longs.  These macros
 * manipulate such bit fields (the filesystem macros use chars).
 * FD_SETSIZE may be defined by the user, but the default here should
 * be enough for most uses.
 */
#ifndef FD_SETSIZE
#define FD_SETSIZE      256
#endif

typedef int     fd_mask;
#define NFDBITS (sizeof(fd_mask) * NBBY)        /* bits per mask */

#ifndef howmany
#define howmany(x, y)   (((x)+((y)-1))/(y))
#endif

typedef struct fd_set {
	fd_mask fds_bits[howmany(FD_SETSIZE, NFDBITS)];
} fd_set;

#define FD_SET(n, p)    ((p)->fds_bits[(n)/NFDBITS] |= (1 << ((n) % NFDBITS)))
#define FD_CLR(n, p)    ((p)->fds_bits[(n)/NFDBITS] &= ~(1 << ((n) % NFDBITS)))
#define FD_ISSET(n, p)  ((p)->fds_bits[(n)/NFDBITS] & (1 << ((n) % NFDBITS)))
#define FD_COPY(f, t)   bcopy(f, t, sizeof(*(f)))
#define FD_ZERO(p)      bzero(p, sizeof(*(p)))

extern int hz;                          /* system clock's frequency */
extern int tick;                        /* usec per tick (1000000 / hz) */


#define FREAD           0x0001
#define FWRITE          0x0002

#define SIGIO           1
#define SIGURG          2
#define SIGPIPE         3

#define BYTES_STREAMDATA	(512*1024)

//#include "socketvar.h"            

//void init_my_malloc(void);
//void *my_malloc(int length);
//void my_free(void *ptr);
//uint32_t socket_alloc(uint32_t *fd, struct socket **sock_ptr);
//void socket_free(uint32_t fd);
//uint32_t getsock(uint32_t fd, struct socket **sock_ptr);
//uint32_t getsockinfo(uint32_t fd, struct socket **sock_ptr);
//void init_socket_list(void);
//int memtxfr(void *dst, const void *src, size_t len, int *resid);
//void generate_event(uint32_t reason, uint32_t fd);
//unsigned int inet_addr(const char *cp);
//char *printmac(uint8_t *mac, char *buf, int start, int flags);

/* Define control flags for printmac function */

#define NOSEP		0x01
#define ZEROTERM	0x02

#endif
