#ifndef MDNSSERVER_H
#define MDNSSERVER_H

void mdns_initialise(void);
int mdns_server(void);

#endif
