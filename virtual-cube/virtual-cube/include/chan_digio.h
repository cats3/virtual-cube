/********************************************************************************
 * MODULE NAME       : chan_io.h												*
 * MODULE DETAILS    : Header for digital I/O channel specific routines			*
 *																				*
 ********************************************************************************/

#ifndef __CHAN_IO_H
#define __CHAN_IO_H

#include <stddef.h>
#include <stdint.h>

/* Configuration information for IO function */

typedef struct cfg_io {
	uint32_t flags;				/* Flags
									Bit 0	Polarity: 0 = Normal, 1 = Invert
									Bit 1	Type: 	  0 = Input, 1 = Output		*/			
	uint32_t mask;					/* I/O bit mask									*/
	char  name[32];				/* Channel name	string							*/
	char  state0[32];			/* State 0 name string							*/
	char  state1[32];			/* State 1 name string							*/
} cfg_io;

#define DIGIO_NORMAL	0x00
#define DIGIO_INVERT	0x01

#define DIGIO_INPUT		0x00
#define DIGIO_OUTPUT	0x02

/* Incomplete declaration of chandef to allow function prototypes to work */

typedef struct chandef chandef;

extern void  chan_io_initlist(void);
extern void  chan_io_default(chandef *def, uint32_t chanid, uint32_t flags);
extern void  chan_io_set_info(chandef *def, uint32_t flags, char *name, char *state0, char *state1, int mirror);
extern void  chan_io_read_info(chandef *def, uint32_t *flags, char *name, char *state0, char *state1);
extern void  chan_io_write_output(chandef *def, uint32_t output);
extern void  chan_io_read_input(chandef *def, uint32_t *input);
extern void  chan_io_read_output(chandef *def, uint32_t *output);
extern void  chan_io_write_userstring(chandef *def, uint32_t start, uint32_t length, char *string, int mirror);
extern void  chan_io_read_userstring(chandef *def, uint32_t start, uint32_t length, char *string);
extern void  chan_io_write_faultmask(chandef *def, uint32_t mask, int mirror);
extern uint32_t chan_io_read_faultstate(chandef *def, uint32_t *capabilities, uint32_t *mask);


#endif
