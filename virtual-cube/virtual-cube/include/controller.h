/********************************************************************************
 * MODULE NAME       : controller.h												*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : Definitions for Karen's controller code.					*
 ********************************************************************************/

#include "channel.h"

#include <stdint.h>

// Controller Header - DSPA

unsigned int CtrlGetSmpSz();	// Get expected size of shared area on DSPB
unsigned int CtrlGetSnvSz();	// Get expected size of static non-volatile data
unsigned int CtrlGetDnvSz();	// Get expected size of dynamic non-volatile data
void CtrlInit(void*					// Initialise controller - must be first called function
	smpbs			//  - DSPB shared area base address
);								//
void CtrlSetSnvBs(void *snvbs);	//  - Static non-volatile data storage base
void CtrlSetDnvBs(void *dnvbs);	//  - Dynamic non-volatile data storage base
void CtrlPoll();				// Perform background poll
void CtrlValidate();			// Validate non-volatile data
void CtrlNormalise();			// Normalise (set factory default) non-volatile data
void CtrlPrime();				// Prime settings in DSPB
void CtrlCmd(char *s, int n);	// Issue console command (n==0 means s points to string)
char *CtrlRspStr();				// Get console response (as string)
int CtrlRspBuf(char *s, int n);	// Copy console response into buffer
typedef void CBFunc(			// Callback function prototype
	void *bs,					//  - Start of range affected
	unsigned int sz				//  - Size of range affected
);								//
void CtrlRegValCB(CBFunc *p);	// Register validation error callback function
void CtrlRegSnvCB(CBFunc *p);	// Register static non-volatile change callback function
void CtrlRegDnvCB(CBFunc *p);	// Register dynamic non-volatile change callback function
void CtrlTSRPoll();
uint32_t CtrlGetGenStatus(void);  // Get combined generator status word
void CubicEngineIter();			// Perform CNet interrupt related updates in Cubic Engine code
void CtrlOffsetChanged(chandef *def, float offset);
								// Offset changed
void CtrlClampChangedEvent(void);
								// Update command clamps

int MsgGetHandle(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);			// Obtain parameter handle
int MsgGetParType(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);		// Determine parameter type
int MsgGetParFlags(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);		// Determine parameter flags
int MsgGetParMin4(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);		// Determine parameter minimum (payload<=4)
int MsgGetParMax4(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);		// Determine parameter maximum (payload<=4)
int MsgSetParValue4(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);		// Set parameter value (payload<=4)
int MsgGetParValue4(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);		// Get parameter value (payload<=4)
int MsgSetParValue256(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);	// Set parameter value (payload<=256)
int MsgGetParValue256(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);	// Get parameter value (payload<=256)
int MsgParSpecial256(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);		// Perform parameter special function (payload<=256)
int MsgGetParMember256(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);	// Get parameter sub-member name (payload<=256)
int MsgGetParLive4(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);		// Get parameter live value (payload<=4)
int MsgGetParUnits256(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);	// Get parameter live value (payload<=4)
int MsgAbortAdjustment(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);	// Get parameter live value (payload<=4)
int MsgGetParAddr4(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);		// Get parameter live value (payload<=4)
int MsgSetEvent(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);			// Set parameter event report options
int MsgSetInstant(unsigned char *msg, unsigned char *rply, unsigned int *msglen, unsigned int *rplylen, void *conn);		// Set parameter value instant

/* Interface functions */

void CtrlClearEventMaskBits(int msk);
void CtrlEventPoll(int msk);
void ctrl_initialise1(void);
void ctrl_initialise2(void);
void CtrlUpdateThreshold(uint32_t chan, float threshold);				// Threshold changed
void informRangeChange(int chandefIndex, float range, char *units);	// Range changed callback
void informEnableChange(int chandefIndex, int enable);				// Enable change callback
void CtrlSetSimMode(uint32_t state);									// Update simulation mode state
void ctrl_txdrfault(int chan);
void CtrlWdogOut(uint32_t out);
void CtrlTSRDAVOut(uint32_t out);
uint32_t CtrlGetActionInfo(uint32_t action);
uint32_t CtrlGetCubicExecutiveFlags(void);
uint32_t CtrlGetGenStatus(void);
void CubicEngineIter(void);

int getFullScaleUnits(int chandefIndex, float* pFullScale, char** pUnitsStr);

// Controller Header - DSPB

void *BCtrlGetSmpBs();				// Get base address of shared area
unsigned int BCtrlGetSmpSz();		// Get expected size of shared area
void BCtrlInit();					// Initialise DSPB code - optional - just zeros shared area
void BCtrlIter();					// Perform DSPB sample iteration
uint32_t ctrl_sat_state(void); 		// Returns global saturation state word

void ctrl_initialise1(void);
void ctrl_initialise2(void);
