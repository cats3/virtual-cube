/********************************************************************************
 * MODULE NAME       : system.h													*
 * PROJECT		     : Control Cube / Signal Cube								*
 * MODULE DETAILS    : System processing routines								*
 ********************************************************************************/

#ifndef __SYSTEM_H
#define __SYSTEM_H

#include <stdint.h>

//#include "shared.h"
#include "msgserver.h"

typedef struct taskinfo {
	int timeslots;	/* Number of consecutive timeslots allocated to the task */
	int ts_ctr;		/* Current timeslot counter								 */
} taskinfo;

/* Define security levels */

#define SECURITY_MONITOR		2
#define SECURITY_OPERATOR		6
#define SECURITY_CALIBRATOR		10
#define SECURITY_ADMINISTRATOR	14

/* Define semaphore states */

#define SEMAPHORE_INVALID		0		/* Semaphore not owned by this connection 	*/
#define SEMAPHORE_FREE			1		/* Semaphore not owned by any connection  	*/
#define SEMAPHORE_VALID			2		/* Semaphore is owned by this connection  	*/

/* Flags for allowed operation checks */

#define ALLOW_PR_PILOT	0x00000001
#define ALLOW_PR_LOW	0x00000002
#define ALLOW_PR_HIGH	0x00000004

#define ALLOW_PR_ANY	(ALLOW_PR_PILOT | ALLOW_PR_LOW | ALLOW_PR_HIGH)

/* Configuration memory addresses and sizes */

#define SYS_EEBANK_DATASTART	0x0200	/* Start of EEPROM bank data area 	*/
#define SYS_EEBANK_SIZE			0x2600	/* Size of EEPROM bank				*/

#define SYS_BBBANK_DATASTART	0x0000	/* Start of BBSRAM bank data area 	*/
#define SYS_BBBANK_SIZE			0x1400	/* Size of BBSRAM bank				*/

//void sys_initlist(void);
//uint32_t sys_initialise(uint32_t* bbsram_alloc, uint32_t* bbsram_addr);
//void sys_save(int status);
//void save_sys_eeconfig(uint32_t* dst, configsave** listtable, int format);
//void mirror_sys_eeconfig(uint32_t entry);
//void mirror_sys_bbconfig(uint32_t entry);

void system_initialise(void);
void system_set_security(uint32_t security);
uint32_t system_get_security(void);
uint32_t system_check_security(uint32_t required);
int system_set_semaphore(connection* conn, uint32_t state);
uint32_t system_get_semaphore(connection* conn);
uint32_t system_check_allowed(uint32_t must_have, uint32_t must_not_have);
// TSK_Handle task_create(Fxn fxn, TSK_Attrs* attrs, connection* c, int timeslots);

#endif
