/********************************************************************************
 * MODULE NAME       : netconfig.h												*
 * PROJECT		     : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Headers for network configuration routines.				*
 ********************************************************************************/

#ifndef __NETCONFIG_H
#define __NETCONFIG_H

#include <stdint.h>

#define CONFIG_MANUAL	0
#define CONFIG_DHCP		1
#define CONFIG_BOOTP	2		// BOOTP not used
#define CONFIG_NONE		3

void read_interface(uint32_t *addr, uint32_t *netmask);
void netconfig_restart(uint32_t delay);
int  netconfig_complete(void);
int  netconfig_setconfig(uint32_t update, uint32_t source, uint8_t *ipaddr, uint8_t *subnetmask);

#endif
