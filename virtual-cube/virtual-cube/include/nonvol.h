/********************************************************************************
  MODULE NAME   	: nonvol
  PROJECT		    : Control Cube / Signal Cube
  MODULE DETAILS  	: Definitions for non-volatile operation.
********************************************************************************/

#ifndef __NONVOL_H
#define __NONVOL_H

#include <stdint.h>

#include "hydraulic.h"

/* Define sizes of static and dynamic non-volatile areas for system use */

#define SYS_SNV_SIZE 4096		/* Space allocated in SNV for general system use   	*/
#define SYS_DNV_SIZE 256		/* Space allocated in DNV for general system use   	*/

/* The following structure defines the layout of the system static non-volatile data area */

typedef struct snv {
	float   cycle_window;			/* Window for cycle detection						*/
	uint32_t   pk_rst_count;			/* Peak reset count for peak/trough calculation		*/
	float   autorst_time;			/* Peak reader automatic reset time					*/
	float   autorst_flags;		/* Peak reader automatic reset control flags		*/
	uint32_t   hyd_type;				/* Hydraulic type									*/
	hyd_def hyd_input[3];			/* Hydraulic input definitions						*/
	hyd_def hyd_output[3];		/* Hydraulic output definitions						*/
	uint32_t   hyd_dissipation;		/* Hydraulic dissipation time						*/
	char    hyd_name[32];			/* Hydraulic name									*/
	uint32_t   cycle_timeout;		/* Cycle detection timeout							*/
	uint32_t   end_sys_marker;		/* Dummy element used to mark end of system area	*/
	char	  padding[1744];		/* Padding to accomodate new entries				*/
	char	  user_data[2048];		/* User data area									*/
} snv;

/* The following structure defines the layout of the system dynamic non-volatile data area */

typedef struct dnv {
	//  uint32_t   valid_runtime;		/* Validation code for runtime counter				*/
	//  uint32_t   runtime_lo;			/* Runtime counter bits 0 - 31						*/
	//  uint32_t   runtime_hi;			/* Runtime counter bits 32 - 63						*/
	uint32_t	  reserved[63];			/* Reserved for future use							*/
	uint32_t	  dummy;				/* Dummy test location								*/
} dnv;

#ifdef NV_DEFAULT_DATA

/* Default system static NV data */

snv snv_default = {
  0.02,									/* Window for cycle detection						*/
  1,									/* Peak reset count for peak/trough calculation		*/
  10.0,									/* Peak reader automatic reset time					*/
  0,									/* Peak reader automatic reset control flags		*/
  (HYD_STANDALONE << HYD_MODE_SHIFT) | HYD_TYPE_HYDRAULIC,
  /* Default hydraulic mode							*/
{{0x00, "Contact 1", 100},			/* Default settings for contact 1					*/
 {0x00, "Contact 2", 100},			/* Default settings for contact 2					*/
 {0x00, "Contact 3", 100}},			/* Default settings for contact 3					*/
{{0x00, "Solenoid 1", 100},			/* Default settings for solenoid 1					*/
 {0x00, "Solenoid 2", 100},			/* Default settings for solenoid 2					*/
 {0x00, "Solenoid 3", 100}},			/* Default settings for solenoid 3					*/
500,									/* Default hydraulic dissipation time (10s)			*/
"None",								/* Default hydraulic name							*/
40960,								/* Default timeout for cycle detection (10s)		*/
0,									/* Dummy element used to mark end of system area	*/
{0},									/* Padding to accomodate new entries				*/
"",									/* User string area									*/
};

/* Default system dynamic NV data */

dnv dnv_default = {
  {0},									/* Reserved for future use							*/
};

#endif

/* Non-volatile status flags. These flags are used alongside the channel NV status
   flags, so there must not be any overlap in bit usage. See channel.h for details.
*/

#define SNV_BANK1_CORRUPT	0x0010	/* SNV bank 1 corrupt				*/
#define SNV_BANK2_CORRUPT	0x0020	/* SNV bank 2 corrupt				*/
#define DNV_BANK1_CORRUPT	0x0040	/* DNV bank 1 corrupt				*/
#define DNV_BANK2_CORRUPT	0x0080	/* DNV bank 2 corrupt				*/
#define SNV_CORRUPT			0x0100	/* SNV area corrupt					*/
#define SNV_RESIZE			0x0200	/* SNV area resized					*/
#define DNV_CORRUPT			0x0400	/* DNV area corrupt					*/
#define DNV_RESIZE			0x0800	/* DNV area resized					*/
#define SNV_BUSY			0x1000	/* SNV update busy					*/
#define DNV_BUSY			0x2000	/* DNV update busy					*/
#define SNV_COPY2			0x4000	/* SNV bank 2 update				*/
#define DNV_COPY2			0x8000	/* DNV bank 2 update				*/

/* Flags control initialisation of NV via nv_initialise() function */

#define NVINIT_INIT			0x01	/* Perform initialisation only		*/
#define NVINIT_FLUSH		0x02	/* Flush NV data (do not restore)	*/

/* Flags control operation of NV via nv_control() function */

#define NVCTRL_FLUSH		0x01	/* Flush NV data					*/
#define NVCTRL_UPDATE		0x02	/* Force NV data update				*/

/* Macro updates value in static NV data area and calls CtrlSnvCB to force
   write to NV memory.
*/

#define SNV_UPDATE(offset, val) {snvbs->offset = (val); 							\
						  	     CtrlSnvCB(&snvbs->offset, sizeof(snvbs->offset));	\
								}

extern snv* snvbs;	/* Pointer to static NV area */
extern dnv* dnvbs;	/* Pointer to dynamic NV area */

uint32_t nv_checkflush(void);									/* Check for flush request					*/
uint32_t nv_initialise(int flags);								/* Initialise NV operation 					*/
void  nv_override(void);
uint32_t nv_control(uint32_t flags);								/* Control NV operation						*/
uint32_t nv_get_status(void);									/* Get NV status							*/
uint32_t nv_set_status(uint32_t set, uint32_t clr);					/* Modify NV status flags					*/
void  nv_getbase(uint32_t** snvbs, uint32_t** dnvbs);				/* Get base addresses of NV areas			*/
void  nv_update(uint32_t init);								/* Update NV areas							*/
void  CtrlSnvCB(void* base, uint32_t size);					/* Callback function for static NV change	*/
void  CtrlDnvCB(void* base, uint32_t size);					/* Callback function for dynamic NV change	*/
void  CtrlValCB(void* base, uint32_t size);					/* Callback function for NV validation		*/
uint32_t nv_setdata(int start, int length, char* data);		/* Write user data							*/
uint32_t nv_readdata(int start, int length, char* data);		/* Read user data							*/

uint32_t nv_modify_status(uint32_t set, uint32_t clr);

void  nv_write_runtimer(uint32_t timer_lo, uint32_t timer_hi);	/* Write runtimer count						*/
void  nv_init_runtimer(uint32_t* timer_lo, uint32_t* timer_hi);	/* Initialise runtimer variables from DNV	*/

#endif /* __NONVOL_H */
