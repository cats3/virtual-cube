/********************************************************************************
 * MODULE NAME       : global.h													*
 * MODULE DETAILS    : Global variable definitions								*
 *																				*
 ********************************************************************************/

#include <stddef.h>
#include <std.h>
#include <stdint.h>


extern uint32_t gbl_nv_status;		/* Non-volatile memory status */

#define GBL_NV_STATUS_SNV_CORRUPT	0x00000001	/* Corrupt SNV memory					 */
#define GBL_NV_STATUS_DNV_CORRUPT	0x00000002	/* Corrupt DNV memory					 */
#define GBL_NV_STATUS_CFG_CORRUPT	0x00000004	/* One or more channels CFG data corrupt */
#define GBL_NV_STATUS_CAL_CORRUPT	0x00000008	/* One or more channels CAL data corrupt */

extern uint32_t gbl_sys_status;	/* System status */

#define GBL_SYS_CNET_INTEGRITY		0x00000001	/* CNet loss of integrity				 */
#define GBL_SYS_CNET_WDOG			0x00000002	/* CNet watchdog failure				 */


