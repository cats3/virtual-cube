// virtual-eeprom.h

#ifndef VIRTUAL_EEPROM_H
#define VIRTUAL_EEPROM_H

#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

struct virtual_eeprom
{
	FILE* file;
	size_t size;
	uint8_t* memory;
	uint8_t addr;
};

extern void* shared_memory;

struct virtual_eeprom* virtual_eeprom_open(const char* filename, size_t size, uint8_t addr);
void virtual_eeprom_close(struct virtual_eeprom* eeprom);
void virtual_eeprom_write(struct virtual_eeprom* eeprom, int addr, uint8_t data);
void virtual_eeprom_write_block(struct virtual_eeprom* eeprom, int addr, uint8_t* data, size_t len, int flags);
void virtual_eeprom_read(struct virtual_eeprom* eeprom, int addr, uint8_t* data);
void virtual_eeprom_read_block(struct virtual_eeprom* eeprom, int addr, uint8_t* data, size_t len, int flags);

#endif
