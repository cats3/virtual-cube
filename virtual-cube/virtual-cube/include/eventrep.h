/********************************************************************************
 * MODULE NAME       : eventrep.h												*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Event reporting.											*
 ********************************************************************************/

#include <stdint.h>
#include "msgserver.h"

typedef struct event_report {
	uint8_t  src;		/* Offset 0	 Source CNet address			*/
	uint8_t  flags;		/* Offset 1	 Flags							*/
	uint16_t length;		/* Offset 2	 Total length of event report	*/
	uint32_t type;		/* Offset 4	 Event type						*/
	char  data[1];	/* Offset 8	 Data block (variable length) 	*/
} event_report;

#define EVENT_PLOW		0				/* Low priority report				*/
#define EVENT_PHIGH		1				/* High priority report				*/

#define EVENT_TIMER		0x00000001		/* System timer increment			*/
#define EVENT_HYDSTATE	0x00000002		/* Hydraulic status change			*/
#define EVENT_HCBUTTON	0x00000003		/* Hand controller button press		*/
#define EVENT_HYDTRACK	0x00000004		/* Tracking mode status change		*/
#define EVENT_TPERROR	0x00000005		/* Turning point error 				*/
#define EVENT_RUNSTATUS	0x00000006		/* Generator status change			*/
#define EVENT_CYCPK		0x00000007		/* Cyclic peak						*/
#define EVENT_PARAMETER	0x80000000		/* Base of parameter change events	*/

#define TP_QUEUE_START	0x0200			/* Address in DSP B memory			*/
#define TP_QUEUE_SIZE	0x0200			/* Size of queue in bytes			*/
#define TP_QUEUE_END	(TP_QUEUE_START + TP_QUEUE_SIZE)
#define TP_QUEUE_MASK	0x01FF			/* Mask for wraparound				*/

#ifdef _DSPB
int tp_add_to_queue(uint32_t CnetTsLo, uint32_t CnetTsHi, uint32_t tpCount, uint32_t tpError);
int asm_tp_add_to_queue(uint32_t CnetTsLo, uint32_t CnetTsHi, uint32_t tpCount, uint32_t tpError);
#endif

void event_initialise(void);
void* event_reporter(void* arg);
void event_connection_closing(connection* conn);
int event_open_connection(connection* conn, unsigned int ipaddr, short port,
	int handle, uint32_t interval);
int event_close_connection(int handle);
int event_send_report(int bitmask, event_report* report, int size);
int event_process_remote_event(int handle, event_report* report, int size);
int event_add_report(int handle, int priority, int event, int size, uint32_t* data);
int event_remove_report(int handle, int event);
int event_handle_to_bitmask(int handle);
