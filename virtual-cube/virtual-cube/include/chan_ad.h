/********************************************************************************
 * MODULE NAME       : chan_ad.h												*
 * MODULE DETAILS    : Header for high-level ADC transducer channel specific 	*
 *					   routines													*
 *																				*
 ********************************************************************************/

#ifndef __CHAN_AD_H
#define __CHAN_AD_H

#include <stddef.h>
#include <stdint.h>

/* Configuration information for AD function */

typedef struct cfg_ad {
	uint32_t flags;				/* Control flags:
										Bit 0 	  : Reserved						
										Bit 1 	  : Polarity inverted
										Bit 2 	  : Reserved
										Bit 3 	  : Reserved
										Bit 4,5   : Linearisation control
													 0 = None
													 1 = 2-point
													 2 = Full
													 3 = Reserved
										Bit 6     : Select unipolar mode				
										Bit 7	  : Reserved							
										Bit 8	  : Reserved
										Bit 9	  : Reserved
										Bit 10    : Reserved
										Bit 11-15 : Reserved (transducer type)
										Bit 16	  : Select asymmetrical transducer mode
										Bit 17	  : Reserved								*/
	float gain;					/* Current overall gain setting	(calibration)				*/
	float pos_gaintrim;			/* Current positive gain trim setting (calibration)			*/
	float txdrzero;				/* Current transducer zero (calibration)					*/
	float neg_gaintrim;			/* Current negative gain trim setting (calibration)			*/
	uint32_t faultmask;			/* Current fault enable mask								*/
	int   reserved1;			/* Reserved for future use									*/
	int   reserved2;			/* Reserved for future use									*/
} cfg_ad;

/* Hardware calibration information for AD function. 

   Important - ensure that size of calibration data structure does not exceed the
			   size of the cal_gp structure.
*/

typedef struct cal_ad {
	int	  cal_offset;			/* Calibrated offset value							*/
	float cal_gain;				/* Calibrated gain value							*/
} cal_ad;

/* Incomplete declaration of chandef to allow function prototypes to work */

typedef struct chandef chandef;

extern void  chan_ad_initlist(void);
extern void  chan_ad_set_pointer(chandef *def, int *ptr);
extern int  *chan_ad_get_pointer(chandef *def);
extern void  chan_ad_default(chandef *def, uint32_t chanid);
extern void  chan_ad_set_caltable(chandef *def, int entry, float value);
extern float chan_ad_read_caltable(chandef *def, int entry);
extern void  chan_ad_set_calgain(chandef *def, float gain);
extern float chan_ad_read_calgain(chandef *def);
extern void  chan_ad_set_caloffset(chandef *def, float offset);
extern float chan_ad_read_caloffset(chandef *def);
extern void  chan_ad_set_gain(chandef *def, float gain, uint32_t flags, int mirror);
extern void  chan_ad_read_gain(chandef *def, float *gain, uint32_t *flags);
extern void  chan_ad_set_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim, int calmode, int mirror);
extern void  chan_ad_read_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim);
extern int   chan_ad_set_refzero(chandef *def, float refzero, int track, int updateclamp, int mirror);
extern int   chan_ad_set_txdrzero(chandef *def, float txdrzero, int track, int updateclamp, int mirror);
extern float chan_ad_read_txdrzero(chandef *def);
extern int   chan_ad_txdrzero_zero(chandef *def, int track, int updateclamp, int mirror);
extern int   chan_ad_refzero_zero(chandef *def, int track, int updateclamp, int mirror);
extern void  chan_ad_set_flags(chandef *def, int set, int clr, int updateclamp, int mirror);
extern int   chan_ad_read_flags(chandef *def);
extern void  chan_ad_set_info(chandef *def, float range, char *units, char *name, char *serialno, int mirror);
extern void  chan_ad_read_info(chandef *def, float *range, char *units, char *name, char *serialno);
extern void  chan_ad_write_map(chandef *def, uint32_t flags, float *value, char *filename, int flush, int mirror);
extern void  chan_ad_read_map(chandef *def, uint32_t *flags, float *value, char *filename);
extern void  chan_ad_write_userstring(chandef *def, uint32_t start, uint32_t length, char *string, int mirror);
extern void  chan_ad_read_userstring(chandef *def, uint32_t start, uint32_t length, char *string);
extern void  chan_ad_set_peak_threshold(chandef *def, uint32_t flags, float threshold, int mirror);
extern void  chan_ad_read_peak_threshold(chandef *def, uint32_t *flags, float *threshold);
extern void  chan_ad_write_faultmask(chandef *def, uint32_t mask, int mirror);
extern uint32_t chan_ad_read_faultstate(chandef *def, uint32_t *capabilities, uint32_t *mask);
extern void  chan_ad_dummy_write_faultmask(chandef *def, uint32_t mask, int mirror);
extern uint32_t chan_ad_dummy_read_faultstate(chandef *def, uint32_t *capabilities, uint32_t *mask);

#endif
