/********************************************************************************
 * MODULE NAME       : rtbuffer.h												*
 * PROJECT		     : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Realtime data streaming buffer definitions.				*
 ********************************************************************************/

#ifndef __RTBUFFER_H
#define __RTBUFFER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>

#define BUFFER_AUTOSIZE		-1			/* Allocate buffer size automatically				*/

/* Define the buffer control structure */

typedef struct buffer {
  uint32_t  size;       	/* Total size of buffer (bytes) 			*/
  uint32_t  free;       	/* Free space in buffer (bytes)				*/
  uint32_t  used;			/* Used space in buffer (bytes)				*/
  uint32_t  pktsize;		/* Size of packet used with buffer (bytes)	*/
  uint32_t  overrun;		/* Flag indicating if overrun has occurred	*/
  uint32_t  underrun;		/* Flag indicating if underrun has occurred	*/
  uint32_t  empty;			/* Flag indicating if buffer is empty		*/
  uint8_t *start;      	/* Start of buffer            				*/
  uint8_t *end;        	/* End of buffer              				*/
  uint8_t *inptr;      	/* Input data pointer         				*/
  uint8_t *outptr;     	/* Output data pointer        				*/
  pthread_mutex_t mtx;
} buffer;

/* Define the CNet data structure */

typedef struct cnetdata {
  volatile float *data; /* Pointer to CNet data      				*/
  float *copy;			/* Pointer to local CNet output data copy	*/
  uint32_t  slot;       	/* CNet slot number          				*/
  float *target;		/* Pointer to CNet ramp target value		*/
  float *rate;			/* Pointer to CNet ramp rate value			*/
  uint32_t *time;			/* Pointer to CNet ramp time value			*/
  uint32_t *tcounter;		/* Pointer to CNet ramp time counter       	*/
} cnetdata;

/* Define the CNet input/output data list structure */

typedef struct cnetlist {
  uint32_t    size;		/* Number of entries in list */
  cnetdata list[256];	/* CNet list				 */
} cnetlist;

/* Structure controlling realtime transfer operation */

typedef struct rtctrl {
  uint32_t		bufsize;		/* Overall buffer area size							*/
  uint32_t		flags;			/* Transfer settings								*/
  uint8_t     *buffer_area;	/* Pointer to realtime buffer area					*/
  cnetlist *inlist;			/* Pointer to input list							*/
  cnetlist *outlist;		/* Pointer to output list							*/
  buffer    inbuf;			/* Input buffer										*/
  buffer    outbuf;			/* Output buffer									*/
  uint32_t		enable;			/* Control flags									*/
//  uint32_t		trigger;		/* Trigger flags									*/
  uint32_t		rate;			/* Sample rate (decimation factor)					*/
  uint32_t		ctr;			/* Sample rate counter								*/
  float		step;			/* Interpolation step size (for decimated output)	*/
} rtctrl;

/* Realtime output sample counter */

extern uint32_t samplectr_lsh;
extern uint32_t samplectr_msh;

/* State variable for turning point mode control */

extern uint32_t rt_tpstate;

/* Realtime transfer status flags */

#define RTSTATUS_OVERRUN	0x0001		/* Overrun in input transfer			*/
#define RTSTATUS_UNDERRUN	0x0002		/* Underrun in output transfer			*/
#define RTSTATUS_EMPTY		0x0004		/* Output buffer empty					*/

/* Realtime transfer control flags */

#define RT_INPUT			0x0001		/* Realtime input control (Cube to PC)	*/
#define RT_OUTPUT			0x0002		/* Realtime output control (PC to Cube)	*/
#define RT_RAMP				0x0004		/* Realtime ramp control				*/
#define RT_INTSR			0x0008		/* Realtime input (Cube to PC) controlled by TSR		

										   Used by periodic data acquisition and
										   controlled by TSRAcqEn bit on TSR0
										   slot. When the RT_INTSR flag is set,
										   the state of the TSRAcqEn bit is
										   logically ORed with the RT_INPUT
										   enable flag to provide the input
										   data control.						*/

#define RT_OUTTSR			0x0010		/* Realtime turning point output (PC to Cube) controlled by TSR	

										   This flag is used when operating in
										   turning point mode to enable control
										   of the data output via the turning
										   point handshake function.

										   When enabled, two TSR flags TSRTPDAV
										   and TSRTPACK are used to handshake
										   between the realtime data output and
										   the turning point generator, allowing
										   individual samples to be read from
										   the output buffer as required.		
										   
										   When the RT_OUTTSR flag is set, the 
										   turning point handshake state is logically 
										   ORed with the RT_OUTPUT enable flag to 
										   provide the output data control. The 
										   RT_OUTPUT and RT_OUTTSR flags should not 
										   be asserted simultaneously.			*/

#define RT_NOSYNC			0x0020		/* Disable input/output sync			*/
#define RT_PAUSED			0x0040		/* Realtime output (PC to Cube) paused	*/

/* Turning point mode operational states */

#define STATE_TP_DISABLE	0			/* Turning point mode disabled			*/
#define STATE_TP_OUTPUT		1			/* Turning point data output			*/
#define STATE_TP_WAITACK	2			/* Waiting for ACK from slave			*/
#define STATE_TP_WAITNACK	3			/* Waiting for NACK from slave			*/

rtctrl *rt_alloc(uint32_t size);
void rt_initialise(rtctrl *r, uint32_t flags);
void rt_transfer(rtctrl *r, uint32_t copy, uint32_t *control);
void cnet_bufin(buffer *buf, cnetdata *list, uint32_t listsize);
void cnet_bufout(buffer *buf, cnetdata *list, uint32_t listsize);
void cnet_rampout(rtctrl *r, buffer *buf, cnetdata *list, uint32_t listsize);
void cnet_copyout(buffer *buf, cnetdata *list, uint32_t listsize);
void rt_bufout(uint8_t *dst, buffer *buf, uint32_t size);
void rt_bufin(buffer *buf, uint8_t *src, uint32_t size);
int  set_bufsize(rtctrl *r, int insize, int outsize);
void flush_buffer(rtctrl *r, uint32_t flags);
int  rtctrl_control(rtctrl *r, uint32_t flags);
int  rtctrl_pause(rtctrl *r, uint32_t flags);
int  rtctrl_resume(rtctrl *r);

#endif
