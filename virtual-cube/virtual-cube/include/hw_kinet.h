/********************************************************************************
 * MODULE NAME       : hw_kinet.h												*
 * PROJECT			 : Control Cube												*
 * MODULE DETAILS    : Header for KiNet specific routines						*
 ********************************************************************************/

#ifndef __HW_KINET_H
#define __HW_KINET_H

#include <stddef.h>
#include <stdint.h>

/* Define KiNet direction control flags */

#define KINET_INPUT		0x00				/* KiNet slot is an input			*/
#define KINET_OUTPUT	0x01				/* KiNet slot is an output			*/

/* Define KiNet scaling control flags */

#define KINET_RAW		0x00				/* No scaling (raw data)			*/
#define KINET_DATA		0x02				/* Scale to floating range +/-1.0	*/

/* Define KiNet control flags */

#define KINET_MASTER	0x01
#define KINET_ENABLE	0x02
#define KINET_NORMAL	0x00				/* Normal KiNet banking operation 	*/
#define KINET_BANK0		0x04				/* Force bank 0 memory access		*/
#define KINET_BANK1		0x0C				/* Force bank 1 memory access		*/

/* KiNet slot definitions */

#define KINET_SLOT_CMD1			4			/* Command channel 1 slot			*/
#define KINET_SLOT_CMD2			9			/* Command channel 2 slot			*/
#define KINET_SLOT_CMD3			14			/* Command channel 3 slot			*/
#define KINET_SLOT_CMD4			19			/* Command channel 4 slot			*/
#define KINET_SLOT_CMD5			24			/* Command channel 5 slot			*/
#define KINET_SLOT_CMD6			29			/* Command channel 6 slot			*/
#define KINET_SLOT_CMD7			34			/* Command channel 7 slot			*/
#define KINET_SLOT_CMD8			39			/* Command channel 8 slot			*/
#define KINET_SLOT_CMD9			44			/* Command channel 9 slot			*/
#define KINET_SLOT_CMD10		49			/* Command channel 10 slot			*/
#define KINET_SLOT_CMD11		54			/* Command channel 11 slot			*/
#define KINET_SLOT_CMD12		59			/* Command channel 12 slot			*/
#define KINET_SLOT_CMD13		64			/* Command channel 13 slot			*/
#define KINET_SLOT_CMD14		69			/* Command channel 14 slot			*/
#define KINET_SLOT_CMD15		74			/* Command channel 15 slot			*/
#define KINET_SLOT_CMD16		79			/* Command channel 16 slot			*/

#define KINET_SLOT_WATCHDOG		126			/* KiNet watchdog slot				*/
#define KINET_SLOT_ASYNC		112			/* First KiNet async comms slot		*/

#define KINET_SLOT_SLOW			127			/* KiNet async slow comms slot		*/

#define KINET_NUM_ASYNC_SLOTS	8			/* Number of slots for async comms	*/

#define KINET_WATCHDOG_DISABLE	0x80000000	/* KiNet watchdog disabled state	*/
#define KINET_WATCHDOG_ENABLE	0x00000000	/* KiNet watchdog enable state		*/


/********************************************************************************
 * Definitions related to KiNet asynchronous communications						*
 ********************************************************************************/

/* Communications buffer definition */

typedef struct combuf {
  uint8_t  *start;		/* Pointer to start of buffer		*/
  uint8_t  *end;		/* Pointer to end of buffer			*/
  uint32_t rdy;		/* Buffer ready for transmission	*/
  uint32_t length;		/* Message length within buffer		*/
  uint16_t chksum;		/* Message checksum 				*/
  uint32_t inuse;		/* Buffer in use					*/
  uint32_t flags;		/* Message control flags
						Bit 0:  0 = Slow comms (1 slot)
								1 = Fast comms (8 slot)	*/
} combuf;

/* Communications function data flag bit */

#define AC_DFLG	0x4000

/* Communications errors */

#define EC_IIADDR       0xE000      /* Illegal instrument address */

#define ACE_HDRPAR		0xD000
#define ACE_CODE		0xD001
#define ACE_URFN		0xD002
#define ACE_CPCNT		0xD003
#define ACE_MEM			0xD004
#define ACE_DLEN		0xD005
#define ACE_DCS			0xD006
#define ACE_NCMPL		0xD007
#define ACE_GEN			0xD008
#define ACE_NOKINET		0xD009
#define ACE_ACKTO		0xD00A
#define ACE_BADCH		0xD00B

/* Default acknowledgement timout (milliseconds) */

#define ACK_TO			2000

/* Master address */

#define MASTER			0x7FFF

/* Packet codes	*/

#define AC_FNREQ		0x8000
#define AC_FNACK		0x0001
#define AC_FNERR		0x0002
#define AC_CPREQ		0x4003
#define AC_CPACK		0x0004
#define AC_CPERR		0x8005
#define AC_RPREQ		0x8006
#define AC_RPACK		0xC007
#define AC_RPERR		0x0008

/* Lead in values */

#define AC_LEAD1		0xAA55
#define AC_LEAD2		0x0FF0
#define AC_LEAD3		0x55AA

typedef uint16_t word;
typedef struct kinet_chk {
  word active;
  word counts;
  word presence_errs;
  word parity_errs;
  word status;
} KI_CHK;

/* Private data format */

typedef struct {
  int	  state;				/* Current state 						*/
  int	  to_ms;
  int	  to_val;
  int	  to_cnt;
  word	  mr_addr;				/* Receive address						*/
  int	  mr_flg;
  word	  mr_hdr[3];			/* Receive header						*/
  word	 *mr_data_ptr;			/* Pointer to receive data				*/
  word 	  mr_data_len;			/* Receive data length					*/
  int	  mr_skip_cnt;			
  int	  mr_derr_flg;
  int	  ms_flg;
  int	  ms_addr;				/* Transmit address						*/
  word	  ms_hdr[3];			/* Transmit header						*/
  word	 *ms_data_ptr;			/* Pointer to transmit data				*/
  word	  ms_data_len;			/* Transmit data length					*/
  int	  iufKiCheck;
  struct kinet_chk *iupKiChk;
  int	  counts;
  int	  prbuf1;
  int	  prbuf2;
  word	  chksum;				/* Checksum calculated during transfer 	*/
} FK3_DATA_FORMAT;
                                                      
#define PDATA	((FK3_DATA_FORMAT *)pdata)



/* Definition of the hardware interface on the KINET card */

typedef struct hw_kinet {
	union {
		struct {
			uint16_t	control;		/*  0x00		Control register
													 Bit 0:	KiNet Master
													 Bit 1: KiNet Enable					*/
			uint16_t	phase;			/* 	0x02		Phase register							
													 Bit 0: Phase control					*/
			uint16_t	reserved1;		/*	0x04		Reserved								*/
			uint16_t	reserved2;		/*	0x06		Reserved	                            */
			uint16_t	reserved3;		/*	0x08		Reserved                            	*/
			uint16_t	reserved4;		/*	0x0A		Reserved								*/
			uint16_t	reserved5;		/*	0x0C		Reserved								*/
			uint16_t	reserved6;		/*	0x0E		Reserved								*/
			uint16_t	reserved7;		/* 	0x10		Reserved								*/
			uint16_t	reserved8;		/* 	0x12		Reserved								*/
			uint16_t	reserved9; 		/* 	0x14		Reserved								*/
			uint16_t	reserved10;		/* 	0x16		Reserved								*/
			uint16_t	reserved11;		/* 	0x18		Reserved								*/
			uint16_t	reserved12;		/* 	0x1A		Reserved								*/
			uint16_t	reserved13;		/* 	0x1C		Reserved								*/
			uint16_t	reserved14;		/* 	0x1E		Reserved								*/
			uint16_t	scl;			/*	0x20		Write I2C SCL output					*/
			uint16_t	sda;			/*	0x22		Write I2C SDA output					*/
			uint16_t	reserved16;		/*	0x24		Reserved								*/
			uint16_t	reserved17;		/*	0x26		Reserved								*/
			uint16_t	reserved18;		/*	0x28		Reserved								*/
			uint16_t	reserved19;		/*	0x2A		Reserved								*/
			uint16_t	reserved20;		/*	0x2C		Reserved								*/
			uint16_t	reserved21;		/*	0x2E		Reserved								*/
			uint16_t	reserved22;		/*	0x30		Reserved								*/
			uint16_t	reserved23;		/*	0x32		Reserved								*/
			uint16_t	reserved24;		/*	0x34		Reserved								*/
			uint16_t	reserved25;		/*	0x36		Reserved								*/
			uint16_t	reserved26;		/*	0x38		Reserved								*/
			uint16_t	reserved27;		/*	0x3A		Reserved								*/
			uint16_t	test;			/*	0x3C		Write test register						*/
			uint16_t	reserved28;		/*	0x3E		Reserved								*/
			uint16_t	reserved[0x60];	/*  0x40-0xFF	Reserved								*/
			int16_t	data[0x80];		/*  0x100-0x1FF	KiNet data memory						*/
			uint16_t	dirn[0x80];		/*  0x200-0x2FF	KiNet direction memory					*/
		} wr;
		struct {
			uint16_t	control;		/*  0x00		Control register
													 Bit 0:	KiNet Master
													 Bit 1: KiNet Enable					*/
			uint16_t	status;			/*  0x02		Status register
													 Bit 0: KiNet phase						*/
			uint16_t	reserved2;		/*  0x04		Reserved								*/
			uint16_t	reserved3;		/*  0x06		Reserved								*/
			uint16_t	reserved4;		/*  0x08		Reserved								*/
			uint16_t	reserved5;		/*  0x0A		Reserved								*/
			uint16_t	reserved6;		/*  0x0C		Reserved								*/
			uint16_t	reserved7;		/*  0x0E		Reserved								*/
			uint16_t	reserved8;		/*  0x10		Reserved								*/
			uint16_t	reserved9;		/*	0x12		Reserved								*/
			uint16_t	reserved10;		/*	0x14		Reserved								*/
			uint16_t	reserved11;		/*	0x16		Reserved								*/
			uint16_t	reserved12;		/*	0x18		Reserved								*/
			uint16_t	reserved13;		/*  0x1A		Reserved								*/
			uint16_t	reserved14;		/*  0x1C		Reserved								*/
			uint16_t	reserved15;		/*  0x1E		Reserved								*/
			uint16_t	scl;			/*	0x20		Read I2C SCL input						*/
			uint16_t	sda;			/*	0x22		Read I2C SDA input						*/
			uint16_t	reserved16;		/*	0x24		Reserved								*/
			uint16_t	reserved17;		/*	0x26		Reserved								*/
			uint16_t	reserved18;		/*	0x28		Reserved								*/
			uint16_t	fault;			/*  0x2A		Read fault status inputs				*/
			uint16_t	reserved19;		/*	0x2C		Reserved								*/
			uint16_t	reserved20;		/*	0x2E		Reserved								*/
			uint16_t	reserved21;		/*	0x30		Reserved								*/
			uint16_t	reserved22;		/*	0x32		Reserved								*/
			uint16_t	reserved23;		/*	0x34		Reserved								*/
			uint16_t	reserved24;		/*	0x36		Reserved								*/
			uint16_t	reserved25;		/*	0x38		Reserved								*/
			uint16_t	reserved26;		/*	0x3A		Reserved								*/
			uint16_t	test;			/*  0x3C		Read test register						*/
			uint16_t	ident;			/*  0x3E		Read identifier register				*/
			uint16_t	reserved[0x60];	/*  0x40-0xFF	Reserved								*/
			int16_t		data[0x80];		/*  0x100-0x1FF	KiNet data memory						*/
			uint16_t	dirn[0x80];		/*  0x200-0x2FF	KiNet direction memory					*/
		} rd;
	} u;
} hw_kinet;



/* Structure holding definition of CNet - KiNet mapping */

typedef struct kinet_mapentry {
	union {
		float *cnet;
		uint32_t *cnet_raw;
		int16_t *kinet;
	} src;				/* Source data pointer					*/
	union {
		float *cnet;
		uint32_t *cnet_raw;
		int16_t *kinet;
	} dst;				/* Destination data pointer				*/
	float scale;		/* Scale factor							*/
	uint8_t flags;			/* Flags:
							Bit 0: Direction
									0 = KiNet to CNet
									1 = CNet to KiNet	
							Bit 1: Type
									0 = Raw data (no scaling)
									1 = Scaled data				*/
	uint8_t kinet_slot;	/* KiNet slot number					*/
	uint8_t cnet_slot;		/* CNet slot number						*/
	uint8_t reserved;		/* Reserved								*/
} kinetmap;



/* Information structure for KiNet card */

typedef struct kinetdef {
	hw_kinet *hw;				/* Pointer to KiNet hardware base		*/
	uint32_t	  flags;			/* Control flags
									Bit 0	KiNet master
									Bit 1	KiNet enable				*/
	uint32_t	  wd_state;			/* Current watchdog state				*/
//	uint32_t	  mapcount;			/* Count of CNet-KiNet mappings			*/
	uint32_t	  in_count;			/* Count of KiNet input slot mappings	*/
	uint32_t	  out_count;		/* Count of KiNet output slot mappings	*/
		
	/* Pointers to control functions */
	
	void  (*setmode)(uint32_t mode);								/* Pointer to set mode function				*/
	uint32_t (*getmode)(void);										/* Pointer to get mode function				*/
	void  (*setdirn)(uint32_t slot, uint32_t dirn);					/* Pointer to set slot direction function	*/
	uint32_t (*getdirn)(uint32_t slot);								/* Pointer to get slot direction function	*/
	void  (*setdata)(uint32_t slot, int16_t dirn);					/* Pointer to set slot data function		*/
	int16_t (*getdata)(uint32_t slot);								/* Pointer to get slot data function		*/
	void  (*mapslot)(uint32_t slot, uint32_t cnetslot, uint32_t flags, float scale);	
																/* Pointer to map slot function				*/
	void  (*unmapslot)(uint32_t slot);								/* Pointer to unmap slot function			*/
	void  (*readslotmap)(uint32_t slot, uint32_t *cnetslot, uint32_t *flags, float *scale);	
																/* Pointer to read slot mapping function	*/
	void  (*asynccomms)(uint32_t flags, uint16_t *txmsg, uint32_t txlen, uint16_t chksum, uint16_t *rxmsg, uint32_t *rxlen);
																/* Pointer to async comms function			*/

	/* Slot mapping list */

//	kinetmap  maplist[128];		/* List of KiNet mappings				*/
	kinetmap  in_maplist[128];	/* List of KiNet input mappings			*/
	kinetmap  out_maplist[128];	/* List of KiNet output mappings		*/
	
} kinetdef;



extern kinetdef *kinetptr;	/* Pointer to KiNet definition */


/* Function prototypes */

void  hw_kinet_set_mode(uint32_t mode);
uint32_t hw_kinet_get_mode(void);
void  hw_kinet_set_dirn(uint32_t slot, uint32_t dirn);
uint32_t hw_kinet_get_dirn(uint32_t slot);
void  hw_kinet_map_slot(uint32_t slot, uint32_t cnetslot, uint32_t flags, float scale);
void  hw_kinet_unmap_slot(uint32_t slot);
void  hw_kinet_read_slot_mapping(uint32_t slot, uint32_t *cnetslot, uint32_t *flags, float *scale);
void  hw_kinet_set_data(uint32_t slot, int16_t data);
int16_t hw_kinet_get_data(uint32_t slot);
void  hw_kinet_async_transfer(uint32_t flags, uint16_t *txmsg, uint32_t txlen, uint16_t chksum, uint16_t *rxmsg, uint32_t *rxlen);
void  kinet_interrupt(void);
void  kinet_output_transfer(void);
//void  kinet_transfer(void);
void  kinet_async(void);
uint32_t hw_kinet_install(int slot, hw_kinet *hw, int flags);
void  FK3_init(void);

#endif
