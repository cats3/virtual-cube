/*
* chanproc.h
*/

#ifndef _CHANPROC_H
#define _CHANPROC_H

void inchanproc(int channel_index, int channels);
void outchanproc(void);
void miscchanproc(void);
void finalproc(void);

#endif
