/********************************************************************************
 * MODULE NAME       : chan_gp.h												*
 * MODULE DETAILS    : Header for general-purpose transducer channel specific 	*
 *					   routines													*
 *																				*
 ********************************************************************************/

#ifndef __CHAN_GP_H
#define __CHAN_GP_H

#include <stddef.h>
#include <stdint.h>

/* Configuration information for GP function */

typedef struct cfg_gp {
	uint32_t flags;				/* Control flags:
										Bit 0 	  : AC transducer (ignored by chanproc)						
										Bit 1 	  : Polarity inverted					
										Bit 2 	  : Reserved
										Bit 3 	  : Reserved
										Bit 4,5   : Linearisation control
												 	 0 = None
													 1 = 2-point (chanproc uses full table)
													 2 = Full
													 3 = Reserved
										Bit 6     : Select unipolar mode						
										Bit 7	  : Sensitivity/Gain (ignored by chanproc)
													 0 = Gain
													 1 = Sensitivity	
										Bit 8	  : Reserved
										Bit 9	  : Reserved
										Bit 10    : Reserved
										Bit 11-15 : Transducer type (ignored by chanproc)
													 0 = Generic DC (load cell, strain gauge)
													 1 = LVDT
													 2 = LVIT								
										Bit 16	  : Select asymmetrical transducer mode	    */
	float gain;					/* Current overall gain setting (calibration)				*/
	float pos_gaintrim;			/* Current positive gain trim setting (calibration)			*/
	float txdrzero;				/* Current transducer zero (calibration)					*/
	int   coarsegain;			/* Current coarse gain setting								*/
	int   finegain;				/* Current fine gain setting								*/
	float excitation;			/* Current excitation value									*/
	float phase;				/* Current excitation phase (AC transducer only)			*/
	float neg_gaintrim;			/* Current negative gain trim setting (calibration)			*/
	uint32_t faultmask;			/* Current fault enable mask								*/
	int   reserved1;			/* Reserved for future use									*/
	int   reserved2;			/* Reserved for future use									*/
} cfg_gp;

/* Hardware calibration information for GP function. 

   Important - do not change the size of the cal_gp structure.

*/

typedef struct cal_gp {
	float exc_zero;				/* Excitation board zero calibration value					*/
	float exc_fs;				/* Excitation board full-scale calibration value			*/
	int   cal_offset;			/* Calibration board offset value							*/
	float gaintable[0x300];		/* Table of board calibration gain values					*/					
} cal_gp;

#define MAX_ZERO_RANGE	0.08F	/* Maximum range of zero offset								*/

/* Incomplete declaration of chandef to allow function prototypes to work */

typedef struct chandef chandef;

extern void  chan_gp_initlist(void);
extern void  chan_gp_set_pointer(chandef *def, int *ptr);
extern int  *chan_gp_get_pointer(chandef *def);
extern void  chan_gp_default(chandef *def, uint32_t chanid);
extern void  chan_gp_set_gain(chandef *def, float gain, uint32_t flags, int mirror);
extern void  chan_gp_read_gain(chandef *def, float *gain, uint32_t *flags);
extern void  chan_gp_set_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim, int calmode, int mirror);
extern void  chan_gp_read_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim);
extern int   chan_gp_set_refzero(chandef *def, float refzero, int track, int updateclamp, int mirror);
extern int   chan_gp_set_txdrzero(chandef *def, float txdrzero, int track, int updateclamp, int mirror);
extern float chan_gp_read_txdrzero(chandef *def);
extern int   chan_gp_txdrzero_zero(chandef *def, int track, int updateclamp, int mirror);
extern int   chan_gp_refzero_zero(chandef *def, int track, int updateclamp, int mirror);
extern void  chan_gp_set_excitation(chandef *def, int type, float volt, float phase, int mirror);
extern void  chan_gp_read_excitation(chandef *def, int *type, float *volt, float *phase);
extern void  chan_gp_set_caltable(chandef *def, int entry, float value);
extern float chan_gp_read_caltable(chandef *def, int entry);
extern void  chan_gp_set_calgain(chandef *def, float gain);
extern float chan_gp_read_calgain(chandef *def);
extern void  chan_gp_set_caloffset(chandef *def, float offset);
extern float chan_gp_read_caloffset(chandef *def);
extern void  chan_gp_set_exccal(chandef *def, uint32_t flags, float exc_zero, float exc_fs);
extern void  chan_gp_read_exccal(chandef *def, uint32_t flags, float *exc_zero, float *exc_fs);
extern void  chan_gp_set_flags(chandef *def, int set, int clr, int updateclamp, int mirror);
extern int   chan_gp_read_flags(chandef *def);
extern void  chan_gp_set_info(chandef *def, float range, char *units, char *name, char *serialno, int mirror);
extern void  chan_gp_read_info(chandef *def, float *range, char *units, char *name, char *serialno);
extern void  chan_gp_write_map(chandef *def, uint32_t flags, float *value, char *filename, int flush, int mirror);
extern void  chan_gp_read_map(chandef *def, uint32_t *flags, float *value, char *filename);
extern void  chan_gp_write_userstring(chandef *def, uint32_t start, uint32_t length, char *string, int mirror);
extern void  chan_gp_read_userstring(chandef *def, uint32_t start, uint32_t length, char *string);
extern void  chan_gp_set_peak_threshold(chandef *def, uint32_t flags, float threshold, int mirror);
extern void  chan_gp_read_peak_threshold(chandef *def, uint32_t *flags, float *threshold);
extern void  chan_gp_write_faultmask(chandef *def, uint32_t mask, int mirror);
extern uint32_t chan_gp_read_faultstate(chandef *def, uint32_t *capabilities, uint32_t *mask);
extern float calc_fine_gain(float rp, int potset);

#endif
