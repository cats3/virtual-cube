/********************************************************************************
 * MODULE NAME       : asmcode.h												*
 * PROJECT		     : Signal Cube												*
 * MODULE DETAILS    : Headers for assembly language support code				*
 ********************************************************************************/

#ifndef __ASMCODE_H
#define __ASMCODE_H

#include <stdint.h>

int   fw_validate(void);
void  hw_restart(void);
void  clr_gie(void);

int   read_ier(void);
int   disable_int(void);
void  restore_int(int state);

uint32_t read_timer(void);
void  delay_us(uint32_t delay);
void  delay_clk(uint32_t delay);
void  dsp_delay(uint32_t delay);

uint8_t  atomic_set1(uint8_t *loc, uint8_t set);
uint8_t  atomic_clr1(uint8_t *loc, uint8_t clr);
uint8_t  atomic_modify1(uint8_t *loc, uint8_t set, uint8_t clr);
uint32_t atomic_clr(uint32_t *loc, uint32_t clr);
uint32_t atomic_tas(uint32_t *loc, uint32_t val);
uint32_t atomic_tac(uint32_t *loc, uint32_t val);
uint32_t atomic_rdclr(uint32_t *loc);

uint32_t hpi_read4(uint32_t* addr);
void  hpi_write4(uint32_t* addr, uint32_t data);
void  hpi_modify4(uint32_t* addr, uint32_t clr, uint32_t set, uint32_t *dst);

//extern void hpi_blockwrite(uint8_t *dst, uint8_t *src, uint32_t size);
//extern void hpi_blockread(uint8_t *dst, uint8_t *src, uint32_t size);

#endif

