/********************************************************************************
 * MODULE NAME       : channel.h												*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : Headers for channel processing routines					*
 ********************************************************************************/

#ifndef __CHANNEL_H
#define __CHANNEL_H

#include <stdint.h>

#include "defines.h"
#include "hardware.h"

#if (defined(CONTROLCUBE) || defined(AICUBE))

#include "hw_2gp1sv.h"
#include "hw_4ad2da.h"
#include "hw_digio.h"
#include "hw_2dig.h"
#include "hw_canbus.h"
#include "hw_mboard.h"

#include "chan_gp.h"
#include "chan_ad.h"
#include "chan_da.h"
#include "chan_sv.h"
#include "chan_digio.h"
#include "chan_digtxdr.h"

#endif

#ifdef SIGNALCUBE

#include "SignalCube\hw_4sg.h"
#include "SignalCube\hw_4lvdt.h"
#include "SignalCube\hw_5adc.h"
#include "SignalCube\hw_4acc.h"
#include "SignalCube\chan_sg.h"
#include "SignalCube\chan_lvdt.h"
#include "SignalCube\chan_acc.h"
#include "chan_ad.h"

#endif

#include "chan_vt.h"

typedef struct slot_info {
	uint8_t	type;			/* Card type						*/
	char    serialno[16];	/* Card serial number string		*/
	uint8_t	data[235];		/* EEPROM data area					*/
	uint32_t   numchan;		/* Total number of channels			*/
	uint32_t   numinchan;		/* Number of input channels			*/
	uint32_t   numoutchan;		/* Number of output channels		*/
	uint32_t   nummiscchan;	/* Number of misc channels			*/
	uint32_t   numdigiochan;	/* Number of digital I/O channels	*/
	uint32_t	baseinchan;		/* Base input channel				*/
	uint32_t	baseoutchan;	/* Base output channel				*/
	uint32_t	basemiscchan;	/* Base misc channel				*/
	uint32_t	basedigiochan;	/* Base digital I/O channel			*/
	uint32_t	faultmask;		/* Safety fault input mask			*/
	uint32_t	txdrfault;		/* Current transducer fault state	*/
	int		*digip_cnetptr;	/* Pointer to digital i/p CNet slot */
	uint16_t	*digip_srcptr;	/* Pointer to digital i/p source	*/
	uint32_t	digip_invert;	/* Inversion state for digital i/p  */
	uint32_t	digip_mask;		/* Mask for digital i/p acq			*/
} slot_info;

/* Type codes for expansion cards. The initialisation code 
   must be changed if any of these type codes are modified.
*/

/* EXPORT BEGIN */

#define CARD_NONE		0	/* No card identified 			*/
#define CARD_MAINBOARD	1	/* Mainboard hardware			*/
#define CARD_2TX		2	/* 2TX card						*/
#define CARD_4AD2DA		3	/* 2DA4AD card					*/
#define CARD_DIGIO		4	/* Digital I/O card				*/
#define CARD_KINET		5	/* KiNet card					*/
#define CARD_4SG		6	/* 4SG card (Signal Cube)		*/
#define CARD_2GP1SV		7	/* 1SV2TX card					*/
#define CARD_4AD		8	/* 4AD card						*/
#define CARD_1DA4AD		9	/* 1DA4AD card					*/
#define CARD_4LVDT		10	/* 4LVDT card (Signal Cube)		*/
#define CARD_4ACC		11	/* 4ACC card (Signal Cube)		*/
#define CARD_5ADC		12	/* 5ADC card (Signal Cube)		*/
#define CARD_2DIG		14	/* 2DIG card					*/
#define CARD_CANBUS		15	/* CAN bus card					*/

/* EXPORT END */

/* Hardware functionality flags:

   Slot data byte 0
   
     Bit 0	Disable servo-valve function (Mainboard, 1SV2TX card only)
     Bit 1	Unused
     Bit 2	Unused
     Bit 3	Unused
     Bit 4	Unused
     Bit 5	Unused
     Bit 6	Unused
     Bit 7	Unused
     
*/

#define MASK_HWFN_NOSV	0x01

/* The following macros allow reading of various hardware functionality flags from
   the general configuration data area of a slot.
*/

//#define HWFN_NOSV(slot)		(slot_status[slot].data[0] & MASK_HWFN_NOSV)	/* No servo-valve drive */
#define HWFN_NOSV(slot)		(0)

/* Macro to set identifier, slot type and channel ID in a chandef structure. Parameters are
   slot no, type, slot channel number, overall channel number. 
*/

#define SETDEF(s, t, p, c) { def->identifier = ((t) << 24) | ((p) << 16) | ((c) << 4) | (s); 	\
                             def->type = (t); 													\
                             def->chanid = (c); 												\
                           }

/* Macros to extract slot number, channel type, slot channel number from a channel definition */

#define EXTSLOTNUM  (def->identifier & 0x0F)
#define EXTSLOTTYPE ((def->identifier >> 24) & 0xFF)
#define EXTSLOTCHAN ((def->identifier >> 16) & 0xFF)

/* Macro to allocate space in EEPROM area for configuration save data */

#define ALLOCATE_EEPROM(def, addr, list) {def = (*addr); (*addr) += (get_list_size(list) + sizeof(listhdr));}

/* Maximum size of a savelist in EEPROM or BBSRAM */

#define SAVELIST_SIZE	0x800

/* Structure holding list header */

typedef struct listhdr {
  uint32_t listinfo;		/* List info data (as below)
  						   
  						   +--------+--------+--------+--------+
  						   |        |  List  | Length of list  |
  						   |        |  Type  | data in bytes   |
  						   +--------+--------+--------+--------+
  																 */
  uint32_t listinfo_chk;	/* Inverted listinfo data for validation */
  uint32_t chksum;			/* List checksum						 */
} listhdr;

typedef struct listdata {
  struct listhdr hdr;		/* List header	*/
  uint8_t data[SAVELIST_SIZE];	/* List data	*/
} listdata;

/* Structure holding the address and length of saved chandef entries */

typedef struct configsave {
  uint32_t offset;		/* Source offset into chandef structure  */
  uint32_t length;		/* Size of source object in bytes        */
  uint32_t bankpos;	/* Destination position within save area */
} configsave;

#define CHAN_EEBANK_DATASTART	0x0200	/* Start of EEPROM bank data area 			*/
#define CHAN_EE_CALSTART		0x8000	/* Start of EEPROM calibration data area 	*/

/* Size of saved channel calibration data */

#define SAVED_CHANCAL_SIZE	(offsetof(struct calinfo, checksum) + sizeof(int))

/* Flags controlling operation of save/restore functions */

#define NO_RESTORE			0		/* Do not perform restoration operation 			*/
#define DO_RESTORE			1		/* Perform restoration operation 					*/

/* Flags controlling operation of hardware install functions */

#define DO_ALLOCATE			0x01	/* Perform allocation of hardware channels 			*/
#define DO_FLUSH			0x02	/* Flush non-volatile data							*/
#define NO_SV				0x04	/* Disable SV function (2TX card only)				*/
#define NO_DA				0x08	/* Disable DA function (4AD card only)				*/

/* Flags controlling mirroring to/from EEPROM or battery-backed RAM */

#define NO_MIRROR			0		/* Do not perform mirror copy 						*/
#define MIRROR				1		/* Perform mirror copy 								*/
#define RESTORE				2		/* Restore from copy								*/

/* Flags controlling updating of parameter command clamp when performing set_txdrzero
   and set_refzero functions.
*/

#define NO_UPDATECLAMP		0		/* Do not perform parameter command clamp update	*/
#define UPDATECLAMP			1		/* Perform parameter command clamp update			*/


extern slot_info slot_status[MAX_SLOT];		/* Information for each slot				*/
extern uint32_t slot_nv_status;				/* Combined NV status for expansion slots	*/
extern uint32_t slot_txdrfault;				/* Combined transducer fault status			*/

/* Define the maximum number of input channels supported for each Cube type.
		
   Any changes to these values must have corresponding changes to the values in 
   the chanproc.asm file. Also note that the values in C3Std.h must match.	
*/

#if defined(CONTROLCUBE)

#define MAXINCHAN	24

#elif defined(SIGNALCUBE)

#define MAXINCHAN	32

#elif defined(AICUBE)

#define MAXINCHAN	24

#endif

#define MAXOUTCHAN				8			/* Maximum number of output channels supported		*/
#define MAXMISCCHAN 			4			/* Maximum number of misc channels supported		*/
#define MAXDIGIOCHAN 			52			/* Maximum number of digital I/O channels supported	*/

/* Notes on channel numbering.

   Channel numbers will be 8 bits, but a further 3 bits will identify the
   type of channel.
   
   +---+---+---+---+---+---+---+---+---+---+---+
   |   Type    |        Channel Number         |
   +---+---+---+---+---+---+---+---+---+---+---+
   
   Types are defined as follows:
   
   000 = Input channel
   001 = Output channel
   010 = Servo-valve channel
   011 = CNet channel
   100 = Digital I/O channel
   
   Channel types are not required for most operations since the type of channel is
   obvious from the context, for example setting excitation on an output channel is
   illegal.
   
   However, for a small number of operations, the type code is required. Such as
   controlling the LED associated with the I/O connector. Input channel 0 and output
   channel 0 both have LEDs and therefore the type code is required to determine the 
   LED to be illuminated. Similarly the servo-valve connector also has an LED and
   so the miscellaneous channel type is also checked.
*/

#define CHANTYPE_INPUT			0x0000		/* Input channel									*/
#define CHANTYPE_OUTPUT			0x0100		/* Output channel									*/
#define CHANTYPE_MISC			0x0200		/* Servo-valve channel								*/
#define CHANTYPE_CNET			0x0300		/* CNet channel										*/
#define CHANTYPE_DIGIO			0x0400		/* Digital I/O channel								*/

#define CHANTYPE_MASK			0x0700

#define CHAN_MASK				0x00FF

#define EXTCHAN 				(def->chanid & CHAN_MASK)

#define CHANNUM(x)				((x) & CHAN_MASK)
#define CHANTYPE(x) 			(((x) & CHANTYPE_MASK) >> 8)

#define ISINPUTCHAN(x)  		(((x) & CHANTYPE_MASK) == CHANTYPE_INPUT)
#define ISOUTPUTCHAN(x) 		(((x) & CHANTYPE_MASK) == CHANTYPE_OUTPUT)
#define ISMISCCHAN(x)   		(((x) & CHANTYPE_MASK) == CHANTYPE_MISC)
#define ISDIGIOCHAN(x)   		(((x) & CHANTYPE_MASK) == CHANTYPE_DIGIO)

#define MAPSIZE 257							/* Number of entries in linearisation map 			*/

/* Fine gain scaling value */

#define GAINTRIM_SCALE	0x20000000
#define TXDRZERO_SCALE	0x80000000UL

#define CLAMP					(1.08F)		/* Command clamping limit value 				*/
#define MAXZERO 				(0.08F)		/* Maximum zero offset							*/


/* EXPORT_BEGIN */

/* Channel control flags flags */

#define FLAG_ACTRANSDUCER	0x0000001		/* AC transducer type							*/		
#define FLAG_POLARITY		0x0000002		/* Polarity										*/
#define FLAG_RSTUPK			0x0000004		/* Reset maximum readings						*/
#define FLAG_AUTORST		0x0000008		/* Enable automatic reset						*/
#define FLAG_2PTCAL			0x0000010		/* Two point calibration control				*/
#define FLAG_LINEARISE		0x0000020		/* Linearisation table control					*/
#define FLAG_UNIPOLAR		0x0000040		/* Select unipolar mode							*/
#define FLAG_GAINTYPE		0x0020080		/* Gain type:									

										   		Bits 17,7

												00 = Gain
												01 = Sensitivity
												10 = Factor (Signal Cube)					
																							*/
#define FLAG_SIMULATION		0x0000100		/* Simulation mode								*/
#define FLAG_VIRTUAL		0x0000200		/* Virtual transducer							*/
#define FLAG_RSTLPK			0x0000400		/* Reset minimum readings						*/
#define FLAG_ASYMMETRICAL	0x0010000		/* Asymmetrical transducer						*/
#define FLAG_MAPFLUSH		0x0040000		/* Request flush of map area in cache			*/
#define FLAG_SHUNTCAL		0x0080000		/* Shunt calibration active						*/
#define FLAG_RSTCYCCTR		0x0200000		/* Reset cycle count							*/
#define FLAG_CYCWINDOW		0x0400000		/* Use local cyclic window value				*/
#define FLAG_ACCOUPLED		0x0800000		/* Enable AC coupling filter					*/
#define FLAG_NEGSHUNTCAL	0x1000000		/* Positive or negative shunt calibration		*/
#define FLAG_CYCEVENT		0x2000000		/* Generate event on cyclic peak detection		*/
#define FLAG_DISABLE		0x4000000		/* Channel disabled (from KO code)				*/
#define FLAG_GBCYC			0x8000000		/* Use GenBus for cycle detection				*/

/* EXPORT_END */

#define BIT_LINEARISE		4				/* Bit position for linearisation mode control 	*/

/* EXPORT_BEGIN */

/* Channel status flags */

#define FLAG_BADLIST		0x0001			/* Corrupt configuration save list				*/
#define FLAG_OLDLIST		0x0002			/* Old format configuration list				*/
#define FLAG_BADCFG			0x0004			/* Corrupt channel configuration				*/
#define FLAG_BADCAL			0x0008			/* Corrupt channel calibration					*/
#define FLAG_SATURATION		0x0020			/* Saturation detected							*/

/* Mask for extracting NV status from channel status */

#define FLAG_NVSTATUS		(FLAG_BADLIST | FLAG_OLDLIST | FLAG_BADCFG | FLAG_BADCAL)

/* EXPORT_END */

/* Define structure holding filter definition (total length 18 (0x12) words) */

typedef struct filter {
	uint32_t type;								/* 0	Current filter type							
													 Bit  31	Filter on/off
													 Bits 0-1	Filter type						*/
	uint32_t order;							/* 1	Current filter order						*/
	float freq;								/* 2	Current filter frequency					*/
	float bandwidth;						/* 3	Current filter bandwidth					*/
	float coeff[10];						/* 4	Filter coefficients in the order 
													b2,a2,b3,a3,b4,a4,b1,a1,b0,a0(unused)		*/
	float delta[4];							/* 14	Filter delta values							*/
} filter;

/* EXPORT_BEGIN */

/* Filter type codes */

#define FILTER_LOWPASS		0				/* Low pass											*/
#define FILTER_NOTCH		1				/* Notch											*/
#define FILTER_HIGHPASS		2				/* Highpass											*/
#define FILTER_BANDPASS		3				/* Bandpass											*/

/* Fault detection capability flags */

#define FAULT_INPUT			0x00000001		/* Input fault detection							*/
#define FAULT_SENSE			0x00000002		/* Excitation sense fault detection					*/
#define FAULT_DRIVE			0x00000004		/* Excitation/drive fault detection					*/

/* EXPORT_END */

#define FILTER_DISABLE		0x00000000		/* Filter disable									*/
#define FILTER_ENABLE		0x80000000		/* Filter enable flag								*/

#define FILTER_TYPEMASK		0x00000003		/* Mask for filter type								*/

/* Flags control setting of filter definition */

#define FILTER_ALL			0				/* Update all filter definition						*/
#define FILTER_FREQ			1				/* Update only filter frequency						*/

/* General channel definition structure used for all channels irrespective
   of type.
   
   Note that the info parameter is a union of the information structures for
   all possible channel hardware types.
*/

typedef struct chandef {
	float 	filtered;						/* 0x00	Processed filtered value 							*/
	float 	value;							/* 0x01	Processed unfiltered value 							*/
	int   	type;							/* 0x02	Channel type										*/								
	uint32_t  	ctrl;							/* 0x03	Channel control flags									
													Bit 0     : AC transducer						
													Bit 1     : Polarity inverted
													Bit 2     : Reset maximum reading
													Bit 3     : Enable automatic reset					
													Bit 4	  : Two-point calibration mode
													Bit 5	  : Linearisation table enabled
													Bit 6     : Select unipolar mode					
													Bit 17,7  : Sensitivity/Gain
																 00 = Gain
																 01 = Sensitivity
																 10 = Gauge/bridge factor (Signal Cube)
													Bit 8	  : Simulation enable							
													Bit 9     : Virtual transducer						
													Bit 10    : Reset minimum reading
													Bit 11-15 : Transducer type (ignored by chanproc)
																 0 = Generic DC (load cell, strain gauge)
																 1 = LVDT
																 2 = LVIT								
													Bit 16	  : Select asymmetrical transducer mode	    
													Bit 17	  : See bit 7 above							
													Bit 18	  : Request flush of map area in cache		
													Bit 19	  : Shunt calibration active				
													Bit 20	  : Cycle count position
																 0 = Peak
																 1 = Trough
													Bit 21	  : Reserved							
													Bit 22	  : Use local cyclic window value			
													Bit 23	  : AC coupled (4ACC txdrs only)			
													Bit 24	  : Positive or negative shunt calibration	
													Bit 25	  : Generate event on cyclic peak detection	
													Bit 26	  : Channel disabled (from KO code)			
													Bit 27	  : Use GenBus for cycle detection			*/
	uint32_t	status;							/* 0x04	Channel status flags
													Bit 0 : Configuration list invalid
													Bit 1 : Old format configuration list
													Bit 2 : Configuration data invalid
													Bit 3 : Calibration data invalid
													Bit 4 : Cyclic direction							
													Bit 5 : Saturation detected
													Bit 6 : Peak detected
													Bit 7 : Trough detected								*/
	int   	chanid;							/* 0x05	Channel number										*/
	int   	identifier;						/* 0x06	Channel unique identifier code						*/
	int   	*sourceptr; 					/* 0x07	Address of input register							*/
	int   	*outputptr;						/* 0x08	Address of output register 							*/
	int   	offset;							/* 0x09	Calibration offset adjustment						*/
	int   	pos_gaintrim;					/* 0x0A	Positive calibration gain adjustment				*/
	int   	neg_gaintrim;					/* 0x0B	Negative calibration gain adjustment				*/
	int 	txdrzero;						/* 0x0C	Transducer zero offset								
													(used for SV current zero offset (float)			*/
	float	refzero;						/* 0x0D Reference zero offset								
													(used for SV valve balance on SV channel)			*/
	float	cycwindow;						/* 0x0E	Local cycle detection window value					*/
	int		bitscale;						/* 0x0F Bit scaling factor for digital transducers			*/
	uint32_t   *gbptr;							/* 0x10 GenBus pointer 										*/
	float   prev_theta;						/* 0x11 Previous theta value								*/
	float	*spptr;							/* 0x12	Pointer to setpoint value							*/
	float   *simptr;						/* 0x13	Pointer to simulation value							*/
	float	simscalep;						/* 0x14	Simulation positive scale factor					*/
	float	simscalen;						/* 0x15	Simulation negative scale factor					*/
	int		satpos;							/* 0x16	Positive saturation detection limit					*/
	int		satneg;							/* 0x17	Negative saturation detection limit					*/
	float	zerofilt;						/* 0x18 Zero filter previous value							*/
	float	zerofiltop;						/* 0x19 Zero filter output									*/
	filter 	filter1;						/* 0x1A	Current filter 1 definition							*/
	float	hp_coeff_b0;					/* 0x2C HP filter coefficient b0							*/
	float	hp_coeff_a1;					/* 0x2D HP filter coefficient a1							*/
	float	hp_delta_d0;					/* 0x2E HP filter d0 value									*/
	union {
		struct def_ip {
			float	maximum;				/* 0x2F	Maximum recorded value								*/
			float	minimum;				/* 0x30	Minimum recorded value								*/
			float	peak;					/* 0x31 Cyclic peak value									*/
			float   trough;					/* 0x32 Cyclic trough value									*/
			uint32_t	cyc_counter;			/* 0x33 Cycle counter for cyclic measurements				*/
			uint32_t	meas_period;			/* 0x34	Measured cycle period								*/
			float	pk;						/* 0x35 Current peak (used for cycle detection)				*/
			float   cyc_peak;				/* 0x36 Temporary peak value								*/
			float   cyc_trough;				/* 0x37 Temporary trough value								*/
			uint32_t	timeout;				/* 0x38 Timeout counter for cycle detection					*/
			float	max;					/* 0x39	Temporary maximum value								*/
			float	min;					/* 0x3A	Temporary minimum value								*/
			uint32_t   prev_timer;				/* 0x3B Previous cycle detect timer count					*/
		} def_ip;
		struct def_op {
			int		dummy;					/* Keep the compiler happy */
		} def_op;
		struct def_sv {
			uint32_t	dither_freq;			/* 0x2F Dither frequency									*/
			float	dither_ampl;			/* 0x30 Dither amplitude									*/
			uint32_t	dither_phase;			/* 0x31 Dither phase accumulator							*/
		} def_sv;
	} def;
	float *mapptr;							/* 0x3C Pointer to linearisation map						*/

	/* The remainder of this structure is not present in simple_chandef */

	float 	map[MAPSIZE];					/* Linearisation map										*/
	char    filename[128];					/* Linearisation table filename								*/

#ifdef SIGNALCUBE
    chandef *procnext;						/* Next channel in processing list							*/
	uint32_t    zerostate;						/* State number for zero function							*/
	uint32_t    zerocount;						/* Counter for zero function								*/
	float	 zerosum;						/* Average filter sum for zero function						*/
	s32_t    zerostep;						/* Step size for zero function								*/
	s32_t	 zerohigh;						/* Upper bound for zero function							*/
	s32_t	 zerolow;						/* Lower bound for zero function							*/
#endif

    /* The following union holds the configuration data related to specific
       channel type.
    */
    
    union cfginfo {
#if (defined(CONTROLCUBE) || defined(AICUBE))
        cfg_gp i_gp;						/* General-purpose transducer configuration					*/
        cfg_da i_da;						/* High-level output configuration							*/
        cfg_sv i_sv;						/* Servo-valve configuration								*/
        cfg_io i_io;						/* Digital I/O configuration								*/
		cfg_digtxdr i_digtxdr;				/* Digital transducer configuration							*/
#endif
        cfg_ad i_ad;						/* High-level input configuration							*/
        cfg_vt i_vt;						/* Virtual transducer configuration							*/
#ifdef SIGNALCUBE
        cfg_sg i_sg;						/* Strain gauge configuration								*/
        cfg_lvdt i_lvdt;					/* LVDT configuration										*/
        cfg_acc i_acc;						/* Accelerometer configuration								*/
#endif
    } cfg;

	char	userstring[128];				/* User string area											*/

	/* The following union holds the hardware specific data for the channel */
	
	union hwinfo {
#if (defined(CONTROLCUBE) || defined(AICUBE))
		info_2gp1sv i_2gp1sv;
		info_4ad2da i_4ad2da;
		info_digio  i_digio;
		info_2dig   i_2dig;
		info_can    i_can;
		info_mboard i_mboard;
#endif
#ifdef SIGNALCUBE
		info_4sg 	i_4sg;
		info_4lvdt 	i_4lvdt;
		info_5adc	i_5adc;
		info_4acc	i_4acc;
#endif
	} hw;

	/* The following union holds the calibration data related to specific channel type. 
	   This data is saved as the channel calibration.
	*/

	struct calinfo {
		union {
#if (defined(CONTROLCUBE) || defined(AICUBE))
			cal_gp c_gp;					/* General-purpose transducer calibration 					*/
			cal_da c_da;					/* High-level output calibration							*/
			cal_sv c_sv;					/* Servo-valve calibration									*/
			cal_digtxdr c_digtxdr;			/* Digital transducer calibration							*/
#endif
#ifdef SIGNALCUBE
			cal_sg c_sg;					/* Strain gauge input calibration 							*/
			cal_lvdt c_lvdt;				/* LVDT input calibration 									*/
			cal_acc c_acc;					/* Accelerometer input calibration 							*/
#endif
			cal_ad c_ad;					/* High-level input calibration								*/
		} u;
		int checksum;
	} cal;			

	uint32_t  	    eecal_start;				/* Offset of EEPROM calibration data area					*/
	uint32_t	    eecal_size;					/* Size of EEPROM calibration data area						*/
	uint32_t	    eecfg_start;				/* Offset of EEPROM configuration data area					*/
	uint32_t	    eeformat;					/* Current EEPROM channel save format						*/
	configsave *eesavelist;					/* Pointer to current EEPROM config save list				*/
	configsave **eesavetable;				/* Pointer to channel EEPROM save table						*/

	float		upper_cmd_clamp;			/* Upper command clamp level								*/
	float		lower_cmd_clamp;			/* Lower command clamp level								*/
		
	/* Pointers to control functions */
	
	void (*readstatus)(struct chandef *def, uint32_t *ctrl, uint32_t *status);
											/* Fn  0. Pointer to read status function					*/
    void (*setpointer)(struct chandef *def, int *ptr);
    										/* Fn  1. Pointer to set pointer function 					*/
    int *(*getpointer)(struct chandef *def);
    										/* Fn  2. Pointer to get pointer function 					*/
	void (*saveconfig)(struct chandef *def);
											/* Fn  3. Pointer to save config function					*/
	int (*restoreconfig)(struct chandef *def);
											/* Fn  4. Pointer to restore config function				*/
	void (*flushconfig)(struct chandef *def);
											/* Fn  5. Pointer to flush config function					*/
	void (*savecalibration)(struct chandef *def);
											/* Fn  6. Pointer to save calibration function				*/
	int (*restorecalibration)(struct chandef *def);
											/* Fn  7. Pointer to restore calibration function			*/
	void (*flushcalibration)(struct chandef *def);
											/* Fn  8. Pointer to flush calibration function				*/
	void (*setexc)(struct chandef *def, int type, float volt, float phase, int mirror);
											/* Fn  9. Pointer to set excitation function				*/
	void (*readexc)(struct chandef *def, int *type, float *volt, float *phase);
											/* Fn 10. Pointer to read excitation function				*/
	void (*setexccal)(struct chandef *def, uint32_t flags, float exc_zero, float exc_fs);
											/* Fn 11. Pointer to set excitation calibration function	*/
	void (*readexccal)(struct chandef *def, uint32_t flags, float *exc_zero, float *exc_fs);
											/* Fn 12. Pointer to read excitation calibration function	*/
	void (*setgain)(struct chandef *def, float gain, uint32_t flags, int mirror);	
											/* Fn 13. Pointer to set gain function						*/
	void (*readgain)(struct chandef *def, float *gain, uint32_t *flags);	
											/* Fn 14. Pointer to read gain function						*/
	void (*setbalance)(struct chandef *def, float balance);	
											/* Fn 15. Pointer to set balance function					*/
	void (*readbalance)(struct chandef *def, float *balance);
											/* Fn 16. Pointer to read balance function					*/
	void (*calctrl)(struct chandef *def, int state);	
											/* Fn 17. Pointer to calibration relay control function		*/
	void (*ledctrl)(struct chandef *def, int state);
											/* Fn 18. Pointer to LED control function					*/
	void (*setrawexc)(struct chandef *def, int type, int volt, float phase, int offset);
											/* Fn 19. Pointer to set raw excitation function			*/
	void (*enableexc)(struct chandef *def, int enable);
											/* Fn 20. Pointer to enable excitation function				*/
	void (*setrawgain)(struct chandef *def, uint32_t coarse, uint32_t fine);	
											/* Fn 21. Pointer to set raw gain function					*/
	void (*readrawgain)(struct chandef *def, uint32_t *coarse, uint32_t *fine);	
											/* Fn 22. Pointer to read raw gain function					*/
	float (*readadc)(struct chandef *def);	
											/* Fn 23. Pointer to read ADC function						*/
	void (*setfilter)(struct chandef *def, uint32_t flags, int filter, int enable, int type, int order, 
					  float freq, float bandwidth, int mirror);	
											/* Fn 24. Pointer to set filter function					*/
	void (*readfilter)(struct chandef *def, int filter, int *enable, int *type, int *order, 
					   float *freq, float *bandwidth);	
											/* Fn 25. Pointer to read filter function					*/
	void (*writedac)(struct chandef *def, float value);
											/* Fn 26. Pointer to write DAC function						*/
	void (*setrawcurrent)(struct chandef *def, int current);
											/* Fn 27. Pointer to set raw current range function			*/
	void (*set_caltable)(struct chandef *def, int entry, float value);
											/* Fn 28. Pointer to set cal table function					*/
	float (*read_caltable)(struct chandef *def, int entry);
											/* Fn 29. Pointer to read cal table function				*/
	void (*set_calgain)(struct chandef *def, float gain);
											/* Fn 30. Pointer to set cal gain function					*/
	float (*read_calgain)(struct chandef *def);
											/* Fn 31. Pointer to read cal gain function					*/
	void (*set_caloffset)(struct chandef *def, float offsettrim);
											/* Fn 32. Pointer to set cal offset function				*/
	float (*read_caloffset)(struct chandef *def);
											/* Fn 33. Pointer to read cal offset function				*/
	void (*set_cnet_slot)(struct chandef *def, int slot);
											/* Fn 34. Pointer to set cnet slot function					*/
	void (*set_gaintrim)(struct chandef *def, float *pos_gaintrim, float *neg_gaintrim, int calmode, int mirror);
											/* Fn 35. Pointer to set gain trim function					*/
	void (*read_gaintrim)(struct chandef *def, float *pos_gaintrim, float *neg_gaintrim);
											/* Fn 36. Pointer to read gain trim function				*/
	void (*set_offsettrim)(struct chandef *def, float offsettrim);
											/* Fn 37. Pointer to set offset trim function				*/
	float (*read_offsettrim)(struct chandef *def);
											/* Fn 38. Pointer to read offset trim function				*/
	int (*set_txdrzero)(struct chandef *def, float txdrzero, int track, int updateclamp, int mirror);
											/* Fn 39. Pointer to set transducer zero function			*/
	float (*read_txdrzero)(struct chandef *def);
											/* Fn 40. Pointer to read transducer zero function			*/
	void (*read_peakinfo)(struct chandef *def, void *dst, int nval);
											/* Fn 41. Pointer to read peakinfo function					*/
	void (*reset_peaks)(struct chandef *def, int flags);
											/* Fn 42. Pointer to reset peaks function					*/
	void (*set_flags)(struct chandef *def, int set, int clr, int updateclamp, int mirror);
											/* Fn 43. Pointer to set flags function						*/
	int (*read_flags)(struct chandef *def);
											/* Fn 44. Pointer to read flags function					*/
	void (*write_map)(struct chandef *def, uint32_t flags, float *value, char *filename, int flush, int mirror);
											/* Fn 45. Pointer to write map function						*/
	void (*read_map)(struct chandef *def, uint32_t *flags, float *value, char *filename);
											/* Fn 46. Pointer to read map function						*/
	int (*set_refzero)(struct chandef *def, float refzero, int track, int updateclamp, int mirror);
											/* Fn 47. Pointer to set refzero function					*/
	float (*read_refzero)(struct chandef *def);
											/* Fn 48. Pointer to read refzero function					*/
	void (*undo_refzero)(struct chandef *def, int track, int mirror);
											/* Fn 49. Pointer to undo refzero function					*/
	void (*set_current)(struct chandef *def, uint32_t type, float current, uint32_t flags, int mirror);
											/* Fn 50. Pointer to set servo-valve current function		*/
	void (*read_current)(struct chandef *def, uint32_t *type, float *current);
											/* Fn 51. Pointer to read servo-valve current function		*/
	int (*tedsid)(struct chandef *def, uint32_t *hi, uint32_t *lo);
											/* Fn 52. Pointer to read TEDS ID function					*/
	void (*set_source)(struct chandef *def, float *ptr);
											/* Fn 53. Pointer to set channel source function			*/
	void (*set_dither)(struct chandef *def, uint32_t enable, float dither, float freq, int mirror);
											/* Fn 54. Pointer to set dither function					*/
	void (*read_dither)(struct chandef *def, uint32_t *enable, float *dither, float *freq);
											/* Fn 55. Pointer to read dither function					*/
	void (*set_balance)(struct chandef *def, float balance, int mirror);
											/* Fn 56. Pointer to set balance function					*/
	void (*read_balance)(struct chandef *def, float *balance);
											/* Fn 57. Pointer to read balance function					*/								
	void (*set_io_info)(struct chandef *def, uint32_t flags, char *name, char *state0, char *state1, int mirror);
											/* Fn 58. Pointer to set digital I/O channel info			*/								
	void (*read_io_info)(struct chandef *def, uint32_t *flags, char *name, char *state0, char *state1);
											/* Fn 59. Pointer to read digital I/O channel info			*/								
	void (*write_io_output)(struct chandef *def, uint32_t output);
											/* Fn 60. Pointer to write digital output					*/								
	void (*read_io_input)(struct chandef *def, uint32_t *input);
											/* Fn 61. Pointer to read digital input						*/								
	void (*read_io_output)(struct chandef *def, uint32_t *output);
											/* Fn 62. Pointer to read digital output 					*/								
	void (*write_userstring)(struct chandef *def, uint32_t start, uint32_t length, char *string, int mirror);
											/* Fn 63. Pointer to write user string 						*/								
	void (*read_userstring)(struct chandef *def, uint32_t start, uint32_t length, char *string);
											/* Fn 64. Pointer to read user string 						*/								
	void (*setgaugetype)(struct chandef *def, uint32_t type, int mirror);
											/* Fn 65. Pointer to set gauge type function				*/
	uint32_t (*readgaugetype)(struct chandef *def);
											/* Fn 66. Pointer to read gauge type function				*/
	void (*setoffset)(struct chandef *def, int offset, int mirror);
											/* Fn 67. Pointer to set offset function					*/
	int (*readoffset)(struct chandef *def);
											/* Fn 68. Pointer to read offset function					*/
	uint32_t (*startzero)(struct chandef *def);
											/* Fn 69. Pointer to start zero function					*/
	void (*setgaugeinfo)(struct chandef *def, float sensitivity, float gauge_factor, 
						 float bridge_factor, int mirror);
											/* Fn 70. Pointer to set gauge info function				*/
	uint32_t (*readgaugeinfo)(struct chandef *def, float *sensitivity, float *gauge_factor, 
						 float *bridge_factor);
											/* Fn 71. Pointer to read gauge info function				*/
	int (*txdrzero_zero)(struct chandef *def, int track, int updateclamp, int mirror);
											/* Fn 72. Ptr to txdrzero_zero function						*/
	uint32_t (*readcycle)(struct chandef *def);
											/* Fn 73. Ptr to readcycle function							*/
	void (*resetcycles)(struct chandef *def, int flags);
											/* Fn 74. Ptr to resetcycles function 						*/
	void (*setpkthreshold)(struct chandef *def, uint32_t flags, float threshold, int mirror);
											/* Fn 75. Ptr to setpkthreshold function 					*/
	void (*readpkthreshold)(struct chandef *def, uint32_t *flags, float *threshold);
											/* Fn 76. Ptr to readpkthreshold function 					*/
	void (*writefaultmask)(struct chandef *def, uint32_t mask, int mirror);
											/* Fn 77. Ptr to writefaultmask function 					*/
	uint32_t (*readfaultstate)(struct chandef *def, uint32_t *capabilities, uint32_t *mask);
											/* Fn 78. Ptr to readfaultstate function 					*/
	void (*setdigtxdrconfig)(struct chandef *def, uint32_t flags, float bitsize, uint32_t ssi_datalen,
							 uint32_t ssi_clkperiod, int mirror);
											/* Fn 79. Ptr to setdigtxdrconfig function		 			*/
	void (*readdigtxdrconfig)(struct chandef *def, uint32_t *flags, float *bitsize, uint32_t *ssi_datalen,
							  uint32_t *ssi_clkperiod);
											/* Fn 80. Ptr to readdigtxdrconfig function 				*/
	void (*setfullscale)(struct chandef *def, float fullscale);
											/* Fn 81. Ptr to setfullscale function 						*/
	int (*refzero_zero)(struct chandef *def, int track, int updateclamp, int mirror);
											/* Fn 82. Ptr to refzero_zero function						*/
	void (*do_zero)(struct chandef *def);   /* Fn 83. Ptr to background zero function					*/
	void (*sethpfilter)(struct chandef *def, int enable, float freq, int mirror);
											/* Fn 84. Ptr to sethpfilter function		 				*/
	void (*readhpfilter)(struct chandef *def, int *enable, float *freq);
											/* Fn 85. Ptr to readhpfilter function 						*/
	void (*setcalzeroentry)(struct chandef *def, uint32_t entry, uint32_t daczero, float zero, int mirror);
											/* Fn 86. Ptr to setcalzeroentry function	 				*/
	void (*readcalzeroentry)(struct chandef *def, uint32_t entry, uint32_t *daczero, float *freq);
											/* Fn 87. Ptr to readcalzeroentry function 					*/
} chandef;

#if 1

/* Simplified channel definition used in shared memory.
   This structure must correspond to the initial part of the full chandef
   structure defined above. 
*/

typedef struct simple_chandef {
	float 	filtered;			/* 0x00	Processed filtered value 							*/
	float 	value;				/* 0x01	Processed unfiltered value 							*/
	int   	type;				/* 0x02	Channel type										*/								
	uint32_t  	ctrl;				/* 0x03	Channel control flags									
										Bit 0 	: AC Transducer Type (Control Cube only)						
										Bit 1 	: Polarity inverted
										Bit 2 	: Reset maximum reading
										Bit 3 	: Reset minimum reading					
										Bit 4,5 : Linearisation control
													0 = None
													1 = 2-point (chanproc uses full)
													2 = Full
													3 = Reserved
										Bit 6   : Select unipolar mode					
										Bit 7	: Sensitivity/Gain
													0 = Gain
													1 = Sensitivity	(ignored by chanproc)	
										Bit 8	: Simulation enable	(Control Cube only)
										Bit 23	: AC coupled (4ACC txdrs only)
										Bit 24	: Positive or negative shunt calibration	
										Bit 25	: Generate event on cyclic peak detection	
										Bit 26	: Channel disabled (from KO code)			
										Bit 27	: Use GenBus for cycle detection			*/

	uint32_t	status;				/* 0x04	Channel status flags
										Bit 0 : Configuration data invalid
										Bit 1 : Calibration data invalid
										Bit 2 : Upper limit detected
										Bit 3 : Lower limit detected
										Bit 4 : Cyclic direction							
										Bit 5 : Saturation detected
										Bit 6 : Peak detected
										Bit 7 : Trough detected								*/
	int   	chanid;				/* 0x05	Channel number										*/
	int   	slot;				/* 0x06	Physical slot location								*/
	int   	*sourceptr; 		/* 0x07	Address of input register							*/
	int   	*outputptr;			/* 0x08	Address of output register							*/
	int   	offset;				/* 0x09	Calibration offset adjustment						*/
	int   	pos_gaintrim;		/* 0x0A	Positive calibration gain adjustment				*/
	int   	neg_gaintrim;		/* 0x0B	Negative calibration gain adjustment				*/
	int 	txdrzero;			/* 0x0C	Transducer zero offset								*/
	float	refzero;			/* 0x0D Reference zero offset								*/
	float	cycwindow;			/* 0x0E	Local cycle detection window value					*/
	int		bitscale;			/* 0x0F Bit scaling factor for digital transducers			*/
	uint32_t   *gbptr;				/* 0x10 GenBus pointer 										*/
	float   prev_theta;			/* 0x11 Previous theta value								*/
	float	*spptr;				/* 0x12	Pointer to setpoint value							*/
	float   *simptr;			/* 0x13	Pointer to simulation value							*/
	float	simscalep;			/* 0x14	Simulation positive scale factor					*/
	float	simscalen;			/* 0x15	Simulation negative scale factor					*/
	int		satpos;				/* 0x16	Positive saturation detection limit					*/
	int		satneg;				/* 0x17	Negative saturation detection limit					*/
	float	zerofilt;			/* 0x18 Zero filter previous value							*/
	float	zerofiltop;			/* 0x19 Zero filter output									*/
	filter 	filter1;			/* 0x1A	Current filter 1 definition							*/
	float	ac_coeff_b0;		/* 0x2C AC coupled filter coefficient b0					*/
	float	ac_coeff_a1;		/* 0x2D AC coupled filter coefficient a1					*/
	float	ac_delta_d0;		/* 0x2E AC coupled filter d0 value							*/
	float	maximum;			/* 0x2F	Maximum recorded value								*/
	float	minimum;			/* 0x30	Minimum recorded value								*/
	float	peak;				/* 0x31 Cyclic peak value									*/
	float   trough;				/* 0x32 Cyclic trough value									*/
	uint32_t	cyc_counter;		/* 0x33 Cycle counter for cyclic measurements				*/
	uint32_t	meas_period;		/* 0x34	Measured cycle period								*/
	float	pk;					/* 0x35 Current peak (used for cycle detection)				*/
	float   cyc_peak;			/* 0x36 Temporary peak value								*/
	float   cyc_trough;			/* 0x37 Temporary trough value								*/
	uint32_t	timeout;			/* 0x38 Timeout counter for cycle detection					*/
	float	max;				/* 0x39	Temporary maximum value								*/
	float	min;				/* 0x3A	Temporary minimum value								*/
	uint32_t   prev_timer;			/* 0x3B Previous cycle detect timer count					*/
	float  *mapptr;				/* 0x3C Pointer to linearisation map						*/
} simple_chandef;

#endif



extern chandef inchaninfo[MAXINCHAN];		/* Input channel information array			*/
extern chandef outchaninfo[MAXOUTCHAN];		/* Output channel information array			*/
extern chandef miscchaninfo[MAXMISCCHAN];	/* Misc channel information array			*/
extern chandef digiochaninfo[MAXDIGIOCHAN];	/* Digital I/O channel information array	*/

extern int totalinchan;						/* Total number of input channels 			*/
extern int physinchan;						/* Number of physical input channels 		*/
extern int totaloutchan;					/* Total number of output channels 			*/
extern int totalmiscchan;					/* Total number of misc channels			*/
extern int totaldigiochan;					/* Total number of digital I/O channels		*/
extern int totalvirtualchan;				/* Total number of virtual channels 		*/


	
/* Channel types */

#define TYPE_NONE		0					/* Channel not present						*/
#define TYPE_GP			1					/* Channel is a GP transducer input			*/
#define TYPE_ADC		2					/* Channel is an ADC input					*/
#define TYPE_SV			3					/* Channel is an SV output					*/
#define TYPE_DAC		4					/* Channel is a DAC output					*/
#define TYPE_DIGIO		5					/* Channel is a digital input/output		*/
#define TYPE_SG			6					/* Channel is a strain gauge input (S3)		*/
#define TYPE_VIRTUAL	7					/* Channel is a virtual input				*/
#define TYPE_LVDT		8					/* Channel is an LVDT input (S3)			*/
#define TYPE_DIGTXDR	9					/* Channel is a digital transducer input	*/
#define TYPE_ACC		10					/* Channel is an accelerometer input (S3)	*/

/* External functions */

extern chandef *chanptr(int chan);

extern uint32_t chan_get_identifier(chandef *def);

extern void  read_chan_status(chandef *def, uint32_t *ctrl, uint32_t *status);
extern void  init_fntable(chandef *def, void *fnlist);

extern uint32_t get_list_size(configsave *list);
extern void  init_savelist(configsave *list);

extern void  save_chan_config(chandef *def);
extern int   restore_chan_config(chandef *def);
extern void  flush_chan_config(chandef *def);

extern int   restore_chan_eeconfig(chandef *def, uint32_t *src, configsave **listtable, int format, int flags);
extern void  save_chan_eeconfig(chandef *def, configsave **listtable, int format);
extern void  flush_chan_eeconfig(chandef *def, configsave *savelist, int format);
extern void  mirror_chan_eeconfig(chandef *def, uint32_t entry);

/* Flags used to control operation of set gain function */

#define SETGAIN_GAINTYPE			0x05	/* Gain type: 		flag bits 2 & 0	*/
#define SETGAIN_RETAIN_GAINTRIM		0x02	/* Retain gaintrim:	flag bit 1		*/

extern void  save_chan_calibration(chandef *def);
extern int   restore_chan_calibration(chandef *def);
extern void  flush_chan_calibration(chandef *def);

extern void  set_filter(chandef *def, uint32_t flags, int filter, int enable, int type, int order, float freq, float bandwidth, int mirror);
extern void  read_filter(chandef *def, int filter, int *enable, int *type, int *order, float *freq, float *bandwidth);
extern void  set_limit_value(chandef *def, uint32_t index, float value);
extern void  read_limit_value(chandef *def, uint32_t index, float *value);
extern void  set_limit_persistance(chandef *def, uint32_t index, uint32_t persistance);
extern void  read_limit_persistance(chandef *def, uint32_t index, uint32_t *persistance);
extern void  set_limit_action(chandef *def, uint32_t index, uint32_t action);
extern void  read_limit_action(chandef *def, uint32_t index, uint32_t *action);
extern void  enable_limits(chandef *def, uint32_t flags);
extern void  disable_limits(chandef *def, uint32_t flags);
extern void  clear_limits(chandef *def, uint32_t flags);
extern void  read_limit_enable(chandef *def, uint32_t *flags);
extern void  read_peakinfo(chandef *def, void *dst, int nval);
extern void  reset_peaks(chandef *def, uint32_t flags);
extern void  reset_all_peaks(uint32_t flags);
extern void  reset_cycles(chandef *def, uint32_t flags);
extern void  reset_all_cycles(uint32_t flags);
extern void  flush_map(chandef *def);
extern void  chan_set_cnet_output(chandef *def, int slot);
extern void  chan_set_cnet_input(chandef *def, int slot);
extern void  chan_set_source(chandef *def, float *ptr);
extern void  chan_set_pointer(chandef *def, int *ptr);
extern int   *chan_get_pointer(chandef *def);
extern void  chan_set_virtualptr(chandef *def, float *ptr);
extern float chan_read_adc(chandef *def);
extern float chan_read_unfiltered(chandef *def);
extern float chan_read_zerofiltop(chandef *def);
extern void  chan_write_dac(chandef *def, float value);
extern void  set_ip_offsettrim(chandef *def, float gaintrim);
extern float read_ip_offsettrim(chandef *def);
extern void  set_op_offsettrim(chandef *def, float gaintrim);
extern float read_op_offsettrim(chandef *def);
extern void  write_map(chandef *def, uint32_t flags, float *value, int mirror);
extern void  read_map(chandef *def, uint32_t *flags, float *value);
extern int   chan_set_refzero(chandef *def, float value, int track, int mirror);
extern float read_refzero(chandef *def);
extern void  undo_refzero(chandef *def, int track, int mirror);
extern void  set_reset_parameters(uint32_t flags, float time, uint32_t pkcount, int mirror);
extern void  set_peak_threshold(uint32_t flags, float threshold, int mirror);
extern void  read_peak_threshold(uint32_t *flags, float *threshold);
extern void  set_peak_timeout(uint32_t timeout, int mirror);
extern void  read_peak_timeout(uint32_t *timeout);
extern void  update_txdrzero(chandef *def, int value, int track);
extern void  chan_modify_flags(chandef *def, uint32_t clr, uint32_t set);
extern void  chan_modify_all_flags(uint32_t clr, uint32_t set);
extern void  chan_set_setpoint_ptr(chandef *def, float *ptr);
extern void  chan_set_simptr(chandef *def, float *ptr, float scalep, float scalen);
extern void  chan_set_genbusptr(chandef *def, void *pGb);
extern uint32_t chan_merge_gainflags(uint32_t flags);
extern uint32_t chan_extract_gainflags(uint32_t flags);
extern void  chan_update_command_clamp(chandef *def, float txdrzero, float refzero, int noclamp, int updateclamp);
extern void  chan_initialise_command_clamp(void);
extern int   chan_validate_zero(chandef *def, int txdrzero, float refzero);
extern void  chan_initialise_threshold(void);
extern int   chan_check_state(int mask);
extern void  chan_clear_all_leds(void);
extern uint32_t chan_read_cyclecount(chandef *def);
extern void  chan_set_hp_filter(chandef *def, int enable, float freq, int mirror);
extern void  chan_read_hp_filter(chandef *def, int *enable, float *freq);
extern uint32_t chan_validate_map(float *map, int len);

extern uint32_t slot_safetyscan(void);
extern void  chan_normalise_setup(chandef *def);

#endif
