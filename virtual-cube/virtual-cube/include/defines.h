/********************************************************************************
 * MODULE NAME       : defines.h												*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : General system definitions								*
 ********************************************************************************/

#ifndef __DEFINES_H
#define __DEFINES_H

#if (defined(CONTROLCUBE) || defined(AICUBE))
#include <stdint.h>
#endif

#ifdef SIGNALCUBE
#include <stdint.h>
#endif

#ifdef VIRTUALCUBE
#include "virtual-cube-config.h"
#endif

#ifdef LOADER
#define FWVERSION	0x8104001c	/* Version 1.4.28 */
#else
#ifdef VIRTUALCUBE
#define FWVERSION	config_get_fwversion()
#else
#define FWVERSION	0x0104001c	/* Version 1.4.28 */
#endif
#endif

/* Insert build data strings */

#ifdef BUILD_DATA

const char build_date[] = __DATE__;			/* Date of current version 		 */
const char build_time[] = __TIME__;			/* Time of current version 		 */
const char build_info[] = "Unified";		/* Any special build information */

#endif

/* Define all possible device types */

#define DEVICE_CONTROLCUBE	1
#define DEVICE_SIGNALCUBE	2
#define DEVICE_AICUBE		3
#define DEVICE_VIRTUALCUBE	1

#ifdef VIRTUALCUBE
	#define DEVICE_TYPE DEVICE_VIRTUALCUBE
#else
	#ifdef CONTROLCUBE
	#define DEVICE_TYPE DEVICE_CONTROLCUBE		/* This device is a Control Cube */
#endif

#ifdef SIGNALCUBE
#define DEVICE_TYPE DEVICE_SIGNALCUBE		/* This device is a Signal Cube */
#endif

#ifdef AICUBE
#define DEVICE_TYPE DEVICE_AICUBE			/* This device is an AI Cube */
#endif
#endif

/* General internal error codes */

#define NO_ERROR			0	/* No error												*/
#define ERROR				1	/* General error										*/
#define CRC_ERROR			2	/* CRC error detected									*/
#define NO_1WIRE			3	/* No 1-wire device detected							*/
#define BAD_WRITE			4	/* Error detected during 1-wire write operation			*/
#define BUFFER_BADREQUEST	5	/* Bad realtime buffer size request						*/
#define BUFFER_NOSPACE		6	/* No space for realtime buffer request					*/
#define BAD_CHECKSUM		7	/* Bad checksum											*/
#define NO_SLOT				8	/* Unable to allocate CNet slot							*/
#define HYDRAULICS_ACTIVE	9	/* Operation not allowed whilst hydraulics active		*/
#define LOG_FULL			10	/* Event log full										*/
#define NO_ESTOP			11	/* Emergency stop disabled								*/
#define START_INHIBIT		12	/* Start inhibit active									*/
#define INVALID_HYDRAULIC	13	/* Invalid hydraulic configuration						*/
#define OPERATION_BUSY		14	/* Requested operation is busy							*/
#define ILLEGAL_STRING		15	/* Illegal string (start/length invalid)				*/
#define EVENT_NOCONNECT		16	/* Unable to establish event reporting connection		*/
#define EVENT_NOSPACE		17	/* No space in event report buffer						*/
#define SHUNTCAL_ACTIVE		18	/* Operation inhibited because shunt cal is active		*/
#define INVALID_PRESSURE	19	/* Operation inhibited because pressure state invalid	*/
#define NO_HANDCTRL			20	/* Hand controller not present							*/
#define TXDR_FAULT			21	/* Operation inhibited - transducer fault				*/
#define ESTOP_FAULT			22	/* Operation inhibited - e-stop fault					*/
#define ESTOP_CHECK			23	/* Operation inhibited - e-stop check not performed		*/
#define CONN_TIMEOUT		24	/* Communications connection timeout					*/
#define CAN_TIMEOUT			25	/* Timeout during CAN bus message transmission			*/
#define CONN_BADCLOSE 		26	/* Unexpected communications connection close			*/
#define GUARD_INPUT			27  /* Operation disallowed due to guard input				*/
#define CONN_BADID	 		28	/* Bad connection ID									*/
#define ZERO_RANGE			29	/* Zero range exceeded									*/
#define ERR_NOSTREAM		30	/* Invalid realtime stream pointer						*/

#define PASS				0	/* Alternative to NO_ERROR								*/
#define FAIL				1	/* General error code									*/

/* System parameters */

#define SAMPLE_RATE			4096.0	/* System sample rate */

/* Useful macros */

#define BIT_COPY(dst, bit, src) (dst) = (((dst) & ~(1 << bit)) | ((src) ? (1 << bit) : 0))
#define BIT_READ(src, bit) (((src) & (1 << bit)) ? 1 : 0)

/* Processor identifiers */

#define PROC_DSPA	0
#define PROC_DSPB	1

#endif

