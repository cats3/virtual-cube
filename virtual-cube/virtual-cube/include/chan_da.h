/********************************************************************************
 * MODULE NAME       : chan_da.h												*
 * MODULE DETAILS    : Header for high-level DAC channel specific routines 		*
 ********************************************************************************/

#ifndef __CHAN_DA_H
#define __CHAN_DA_H

#include <stddef.h>
#include <stdint.h>

/* Configuration information for DA channel */

typedef struct cfg_da {
	uint32_t flags;				/* Control flags:
										Bit 0 	  : Reserved						
										Bit 1 	  : Polarity inverted					
										Bit 2 	  : Reserved
										Bit 3 	  : Reserved
										Bit 4,5   : Reserved							
										Bit 6	  : Reserved							
										Bit 7	  : Reserved							
										Bit 8	  : Reserved
										Bit 9	  : Reserved
										Bit 10	  : Reserved
										Bit 11-15 : Reserved (transducer type)
										Bit 16	  : Reserved 					   */
	float gain;					/* Current overall gain setting						*/
	float gaintrim;				/* Current gain trim setting						*/
	int   reserved1;			/* Reserved for future use							*/
	int   reserved2;			/* Reserved for future use							*/
	int   reserved3;			/* Reserved for future use							*/
	int   reserved4;			/* Reserved for future use							*/
} cfg_da;

/* Hardware calibration information for DA channel. 

   Important - ensure that size of calibration data structure does not exceed the
			   size of the cal_gp structure.
*/

typedef struct cal_da {
	int	  cal_offset;			/* Calibrated offset value							*/
	float cal_gain;				/* Calibrated gain value							*/
} cal_da;

/* Incomplete declaration of chandef to allow function prototypes to work */

typedef struct chandef chandef;

extern void  chan_da_initlist(void);
extern void  chan_da_set_pointer(chandef *def, int *ptr);
extern int  *chan_da_get_pointer(chandef *def);
extern void  chan_da_default(chandef *def, uint32_t chanid);
extern void  chan_da_set_caltable(chandef *def, int entry, float value);
extern float chan_da_read_caltable(chandef *def, int entry);
extern void  chan_da_set_calgain(chandef *def, float gain);
extern float chan_da_read_calgain(chandef *def);
extern void  chan_da_set_caloffset(chandef *def, float offset);
extern float chan_da_read_caloffset(chandef *def);
extern void  chan_da_set_gain(chandef *def, float gain, uint32_t flags, int mirror);
extern void  chan_da_read_gain(chandef *def, float *gain, uint32_t *flags);
extern void  chan_da_set_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim, int calmode, int mirror);
extern void  chan_da_read_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim);
extern void  chan_da_set_flags(chandef *def, int set, int clr, int updateclamp, int mirror);
extern int   chan_da_read_flags(chandef *def);
extern void  chan_da_set_info(chandef *def, float range, char *units, char *name, char *serialno, int mirror);
extern void  chan_da_read_info(chandef *def, float *range, char *units, char *name, char *serialno);
extern void  chan_da_write_userstring(chandef *def, uint32_t start, uint32_t length, char *string, int mirror);
extern void  chan_da_read_userstring(chandef *def, uint32_t start, uint32_t length, char *string);
extern void  chan_da_write_faultmask(chandef *def, uint32_t mask, int mirror);
extern uint32_t chan_da_read_faultstate(chandef *def, uint32_t *capabilities, uint32_t *mask);

#endif
