/********************************************************************************
 * MODULE NAME       : bootp.h													*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : Headers for bootp routines.								*
 ********************************************************************************/

#ifndef __BOOTP_H
#define __BOOTP_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <std.h>
#include <log.h>

#include "headers.h"
#include "hardware.h"

#define CHADDR_MAX      16
#define SNAME_MAX       64
#define FILE_MAX        128
#define VEND_MAX        312

#define BOOTP_MIN		300		/* Minimum size of a BOOTP response */

typedef struct { /* C = Must be set by client,  c = may be set by client  */
                 /* S = Must be set by server,  s = may be set by server  */
                 /* G = Must be set by gateway, g = may be set by gateway */
                 
  u_char  op,                 /* C S - BOOTREQUEST or BOOTREPLY 				*/
          htype,              /* C   - Hardware type 							*/
          hlen,               /* C   - Length of hardware address 				*/
          hops;               /*  g  - Hop count (used by gateways) 			*/
  u_long  xid;                /* C   - Transaction id 							*/
  u_short secs,               /* C   - Seconds elapsed since start of boot 		*/
          flags;			  /*     - Flags									*/
  u_long  ciaddr,             /* c   - Client  IP address (if known by client) 	*/
          yiaddr,             /*   S - Client  IP address 						*/
          siaddr,             /*   S - Server  IP address 						*/
          giaddr;             /*  gs - Gateway IP address 						*/
  u_char  chaddr[CHADDR_MAX]; /* C   - Client hardware address 					*/
  u_char  sname[SNAME_MAX];   /* c S - Server hostname name 					*/
  u_char  file[FILE_MAX];     /* c S - File name to boot 						*/
  u_char  vend[VEND_MAX];     /* c s - vendor specific 							*/
} BOOTP;

#define BOOTREQUEST     1
#define BOOTREPLY       2

#define ETHERNET_TYPE   1       /* ethernet hardware type */
#define ETHERNET_LEN    6       /* ethernet hardware byte length */
#define ETHERLEN		6

#define VENDOR_COOKIE   0x63825363 /* host byte order */

/* Vendor extension options */

#define VENDOR_PAD              			0       /* Length=0 	*/
#define VENDOR_SUBNETMASK          			1       /* Length=4 	*/
#define VENDOR_TIMEOFFSET       			2       /* Length=4 	*/
#define VENDOR_GATEWAY          			3
#define VENDOR_TIMESERVER       			4
#define VENDOR_IEN116SERVER     			5
#define VENDOR_NAMESERVER       			6
#define VENDOR_LOGSERVER        			7
#define VENDOR_QUOTESERVER      			8
#define VENDOR_LPRSERVER        			9
#define VENDOR_IMPRESSSERVER    			10
#define VENDOR_RLPSERVER        			11
#define VENDOR_HOSTNAME         			12
#define VENDOR_BOOTFILESIZE     			13
#define VENDOR_DHCP_REQUESTEDIPADDRESS		50
#define VENDOR_DHCP_IPADDRESSLEASETIME		51		/* Length = 4	*/
#define VENDOR_DHCP_MESSAGETYPE				53		/* Length = 1	*/
#define VENDOR_DHCP_SERVERIDENTIFIER		54		/* Length = 4	*/
#define VENDOR_DHCP_PARAMETERREQUESTLIST	55
#define VENDOR_DHCP_CLIENTIDENTIFIER		61
#define VENDOR_END              			255

/* DHCP message types */

#define DHCP_DHCPDISCOVER 					1
#define DHCP_DHCPOFFER						2
#define DHCP_DHCPREQUEST					3
#define DHCP_DHCPDECLINE					4
#define DHCP_DHCPACK						5
#define DHCP_DHCPNAK						6
#define DHCP_DHCPRELEASE					7

int bootp_request(int s, int transaction, uint8_t *macaddr);
int dhcp_discover(int s, int transaction, uint8_t *macaddr);
int dhcp_request(int s, int transaction, uint8_t *macaddr, struct sockaddr_in *serverid, 
				 struct sockaddr_in *clientid, struct sockaddr_in *requested, int broadcast);
int dhcp_response(int s, int transaction, uint8_t *macaddr, struct sockaddr_in *serverid, int *leasetime, 
				  struct sockaddr_in *addr, struct sockaddr_in *netmask);

#endif
