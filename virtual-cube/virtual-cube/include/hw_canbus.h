/********************************************************************************
 * MODULE NAME       : hw_can.h													*
 * MODULE DETAILS    : Header for CAN specific routines							*
 *																				*
 ********************************************************************************/

#ifndef __HW_CAN_H
#define __HW_CAN_H

#include <stdint.h>

/* Definition of the hardware interface on the CAN bus card */

typedef struct hw_can {
	union {
		struct {
			uint16_t	reserved0;	/* 	0x00	Reserved								*/
			uint16_t	reserved1;	/* 	0x02	Reserved								*/
			uint16_t	reserved2;	/* 	0x04	Reserved								*/
			uint16_t	reserved3;	/* 	0x06	Reserved								*/
			uint16_t	reserved4;	/*	0x08	Reserved								*/
			uint16_t	reserved5;	/*	0x0A	Reserved								*/
			uint16_t	reserved6;	/* 	0x0C	Reserved								*/
			uint16_t	reserved7;	/* 	0x0E	Reserved								*/
			uint16_t	reserved8;	/* 	0x10	Reserved   								*/
			uint16_t	reserved9;	/* 	0x12	Reserved   								*/
			uint16_t	reserved10;	/* 	0x14	Reserved   								*/
			uint16_t	reserved11;	/* 	0x16	Reserved   								*/
			uint16_t	reserved12;	/* 	0x18	Reserved   								*/
			uint16_t	reserved13;	/* 	0x1A	Reserved   								*/
			uint16_t	reserved14;	/* 	0x1C	Reserved   								*/
			uint16_t	reserved15;	/* 	0x1E	Reserved   								*/
			uint16_t	spi_txctrl;	/*	0x20	Write SPI TX control register
												Bit 0	Transmit txbuf byte 0
												Bit 1	Transmit txbuf byte 1
												Bit 2	Transmit txbuf byte 2
												Bit 3	Transmit txbuf byte 3		*/
			uint16_t	reserved16;	/* 	0x22	Reserved   								*/
			uint16_t	spi_rxctrl;	/*	0x24	Write SPI RX control register
												Bit 0	Transmit txbuf byte 0
												Bit 1	Transmit txbuf byte 1
												Bit 2	Receive rxbuf byte 0		*/
			uint16_t	reserved17;	/* 	0x26	Reserved   								*/
			uint16_t	spi_txbuf0;	/*  0x28	Write SPI TX buffer bytes 0,1			*/
			uint16_t	spi_txbuf2;	/*  0x2A	Write SPI TX buffer bytes 2,3			*/
			uint16_t	reserved18;	/*	0x2C	Reserved   								*/
			uint16_t	reserved19;	/*	0x2E	Reserved   								*/
			uint16_t	reserved20;	/*	0x30	Reserved                              	*/
			uint16_t	reserved21;	/*	0x32	Reserved                           		*/
			uint16_t	reserved22;	/*	0x34	Reserved                               	*/
			uint16_t	reserved23;	/*	0x36	Reserved                               	*/
			uint16_t	reserved24;	/*	0x38	Reserved                               	*/
			uint16_t	ctrl;		/*	0x3A	Control register						
												Bit 0	Assert RESET to MCP2515
												Bit 1	Enable RXB0 processing		*/
			uint16_t	test;		/*	0x3C	Write test register						*/
			uint16_t	reserved25;	/*	0x3E	Reserved								*/
		} wr;
		struct {
			uint16_t	reserved0;	/* 	0x00	Reserved								*/
			uint16_t	reserved1;	/* 	0x02	Reserved								*/
			uint16_t	reserved2;	/*  0x04	Reserved								*/
			uint16_t	reserved3;	/*  0x06	Reserved								*/
			uint16_t	reserved4;	/*  0x08	Reserved								*/
			uint16_t	reserved5;	/*  0x0A	Reserved								*/
			uint16_t	reserved6;	/* 	0x0C	Reserved								*/
			uint16_t	reserved7;	/* 	0x0E	Reserved								*/
			int		txdr1;		/*	0x10	Read transducer 1 result				*/
			uint16_t	reserved10;	/*	0x14	Reserved								*/
			uint16_t	reserved11;	/*	0x16	Reserved								*/
			uint16_t	reserved12;	/*	0x18	Reserved								*/
			uint16_t	reserved13;	/*	0x1A	Reserved								*/
			uint16_t	reserved14;	/*	0x1C	Reserved								*/
			uint16_t	reserved15;	/*	0x1E	Reserved								*/
			uint16_t	spi_status;	/*  0x20	Read SPI status register				*/
			uint16_t	reserved16;	/*	0x22	Reserved								*/
			uint16_t	reserved17;	/*	0x24	Reserved								*/
			uint16_t	reserved18;	/*	0x26	Reserved								*/
			uint16_t	spi_rxbuf0;	/*	0x28	Read SPI RX buffer bytes 0,1			*/
			uint16_t	reserved19;	/*	0x2A	Reserved								*/
			uint16_t	reserved20;	/*	0x2C	Reserved								*/
			uint16_t	reserved21;	/*	0x2E	Reserved								*/
			uint16_t	reserved22;	/*	0x30	Reserved								*/
			uint16_t	reserved23;	/*	0x32	Reserved								*/
			uint16_t	reserved24;	/*	0x34	Reserved								*/
			uint16_t	reserved25;	/*	0x36	Reserved								*/
			uint16_t	reserved26;	/*	0x38	Reserved								*/
			uint16_t	reserved27;	/*	0x3A	Reserved								*/
			uint16_t	test;		/*  0x3C	Read test register						*/
			uint16_t	ident;		/*  0x3E	Read identifier register				*/
		} rd;
	} u;
} hw_can;

/* Control register definitions */

#define CTRL_RESET		0x01	/* Assert RESET to CAN bus controller				*/
#define CTRL_ENRXB0		0x02	/* Enable RXB0 processing							*/

/* SPI TX control register definitions */

#define SPI_TXCTRL_WR1	0x01	/* Write 1 byte to SPI bus							*/
#define SPI_TXCTRL_WR2	0x03	/* Write 2 bytes to SPI bus							*/
#define SPI_TXCTRL_WR3	0x07	/* Write 3 bytes to SPI bus							*/
#define SPI_TXCTRL_WR4	0x0F	/* Write 4 bytes to SPI bus							*/

/* SPI RX control register definitions */

#define SPI_RXCTRL_WR1	0x01	/* Write 1 byte to SPI bus							*/
#define SPI_RXCTRL_WR2	0x03	/* Write 2 bytes to SPI bus							*/
#define SPI_RXCTRL_RD1	0x04	/* Read 1 byte from SPI bus							*/

/* SPI status register definitions */

#define SPI_TXBUSY		0x01	/* SPI transmit busy								*/
#define SPI_RXBUSY		0x02	/* SPI receive busy									*/

/* Register definitions for MC2515 CAN bus interface */

#define RXF0SIDH	0x00		/* Filter 0 standard identifier high register			*/
#define RXF0SIDL	0x01		/* Filter 0 standard identifier low register			*/
#define RXF0EID8	0x02		/* Filter 0 extended identifier high register			*/
#define RXF0EID0	0x03		/* Filter 0 extended identifier low register			*/
#define RXF1SIDH	0x04		/* Filter 1 standard identifier high register			*/
#define RXF1SIDL	0x05		/* Filter 1 standard identifier low register			*/
#define RXF1EID8	0x06		/* Filter 1 extended identifier high register			*/
#define RXF1EID0	0x07		/* Filter 1 extended identifier low register			*/
#define RXF2SIDH	0x08		/* Filter 2 standard identifier high register			*/
#define RXF2SIDL	0x09		/* Filter 2 standard identifier low register			*/
#define RXF2EID8	0x0A		/* Filter 2 extended identifier high register			*/
#define RXF2EID0	0x0B		/* Filter 2 extended identifier low register			*/
#define BFPCTRL		0x0C		/* RXnBF pin control and status register				*/
#define TXRTSCTRL	0x0D		/* TXnRTS pin control and status register				*/
#define CANSTAT		0x0E		/* CAN status register									*/
#define CANCTRL		0x0F		/* CAN control register									*/
#define RXF3SIDH	0x10		/* Filter 3 standard identifier high register			*/
#define RXF3SIDL	0x11		/* Filter 3 standard identifier low register			*/
#define RXF3EID8	0x12		/* Filter 3 extended identifier high register			*/
#define RXF3EID0	0x13		/* Filter 3 extended identifier low register			*/
#define RXF4SIDH	0x14		/* Filter 4 standard identifier high register			*/
#define RXF4SIDL	0x15		/* Filter 4 standard identifier low register			*/
#define RXF4EID8	0x16		/* Filter 4 extended identifier high register			*/
#define RXF4EID0	0x17		/* Filter 4 extended identifier low register			*/
#define RXF5SIDH	0x18		/* Filter 5 standard identifier high register			*/
#define RXF5SIDL	0x19		/* Filter 5 standard identifier low register			*/
#define RXF5EID8	0x1A		/* Filter 5 extended identifier high register			*/
#define RXF5EID0	0x1B		/* Filter 5 extended identifier low register			*/
#define TEC			0x1C		/* Transmit error counter register						*/
#define REC			0x1D		/* Receive error counter register						*/
#define RXM0SIDH	0x20		/* Mask 0 standard identifier high register				*/
#define RXM0SIDL	0x21		/* Mask 0 standard identifier low register				*/
#define RXM0EID8	0x22		/* Mask 0 extended identifier high register				*/
#define RXM0EID0	0x23		/* Mask 0 extended identifier low register				*/
#define RXM1SIDH	0x24		/* Mask 1 standard identifier high register				*/
#define RXM1SIDL	0x25		/* Mask 1 standard identifier low register				*/
#define RXM1EID8	0x26		/* Mask 1 extended identifier high register				*/
#define RXM1EID0	0x27		/* Mask 1 extended identifier low register				*/
#define CNF3		0x28		/* Configuration 3 register								*/
#define CNF2		0x29		/* Configuration 2 register								*/
#define CNF1		0x2A		/* Configuration 1 register								*/
#define CANINTE		0x2B		/* Interrupt enable register							*/
#define CANINTF		0x2C		/* Interrupt flag register								*/
#define EFLG		0x2D		/* Error flag register									*/
#define TXB0CTRL	0x30		/* Transmit buffer 0 control register					*/
#define TXB0SIDH	0x31		/* Transmit buffer 0 standard identifier high register	*/
#define TXB0SIDL	0x32		/* Transmit buffer 0 standard identifier low register	*/
#define TXB0EID8	0x33		/* Transmit buffer 0 extended identifier high register	*/
#define TXB0EID0	0x34		/* Transmit buffer 0 extended identifier low register	*/
#define TXB0DLC		0x35		/* Transmit buffer 0 data length code register			*/
#define TXB0D0		0x36		/* Transmit buffer 0 data byte 0 register				*/
#define TXB0D1		0x37		/* Transmit buffer 0 data byte 1 register				*/
#define TXB0D2		0x38		/* Transmit buffer 0 data byte 2 register				*/
#define TXB0D3		0x39		/* Transmit buffer 0 data byte 3 register				*/
#define TXB0D4		0x3A		/* Transmit buffer 0 data byte 4 register				*/
#define TXB0D5		0x3B		/* Transmit buffer 0 data byte 5 register				*/
#define TXB0D6		0x3C		/* Transmit buffer 0 data byte 6 register				*/
#define TXB0D7		0x3D		/* Transmit buffer 0 data byte 7 register				*/
#define TXB1CTRL	0x40		/* Transmit buffer 1 control register					*/
#define TXB1SIDH	0x41		/* Transmit buffer 1 standard identifier high register	*/
#define TXB1SIDL	0x42		/* Transmit buffer 1 standard identifier low register	*/
#define TXB1EID8	0x43		/* Transmit buffer 1 extended identifier high register	*/
#define TXB1EID0	0x44		/* Transmit buffer 1 extended identifier low register	*/
#define TXB1DLC		0x45		/* Transmit buffer 1 data length code register			*/
#define TXB1D0		0x46		/* Transmit buffer 1 data byte 0 register				*/
#define TXB1D1		0x47		/* Transmit buffer 1 data byte 1 register				*/
#define TXB1D2		0x48		/* Transmit buffer 1 data byte 2 register				*/
#define TXB1D3		0x49		/* Transmit buffer 1 data byte 3 register				*/
#define TXB1D4		0x4A		/* Transmit buffer 1 data byte 4 register				*/
#define TXB1D5		0x4B		/* Transmit buffer 1 data byte 5 register				*/
#define TXB1D6		0x4C		/* Transmit buffer 1 data byte 6 register				*/
#define TXB1D7		0x4D		/* Transmit buffer 1 data byte 7 register				*/
#define TXB2CTRL	0x50		/* Transmit buffer 2 control register					*/
#define TXB2SIDH	0x51		/* Transmit buffer 2 standard identifier high register	*/
#define TXB2SIDL	0x52		/* Transmit buffer 2 standard identifier low register	*/
#define TXB2EID8	0x53		/* Transmit buffer 2 extended identifier high register	*/
#define TXB2EID0	0x54		/* Transmit buffer 2 extended identifier low register	*/
#define TXB2DLC		0x55		/* Transmit buffer 2 data length code register			*/
#define TXB2D0		0x56		/* Transmit buffer 2 data byte 0 register				*/
#define TXB2D1		0x57		/* Transmit buffer 2 data byte 1 register				*/
#define TXB2D2		0x58		/* Transmit buffer 2 data byte 2 register				*/
#define TXB2D3		0x59		/* Transmit buffer 2 data byte 3 register				*/
#define TXB2D4		0x5A		/* Transmit buffer 2 data byte 4 register				*/
#define TXB2D5		0x5B		/* Transmit buffer 2 data byte 5 register				*/
#define TXB2D6		0x5C		/* Transmit buffer 2 data byte 6 register				*/
#define TXB2D7		0x5D		/* Transmit buffer 2 data byte 7 register				*/
#define RXB0CTRL	0x60		/* Receive buffer 0 control register					*/
#define RXB0SIDH	0x61		/* Receive buffer 0 standard identifier high register	*/
#define RXB0SIDL	0x62		/* Receive buffer 0 standard identifier low register	*/
#define RXB0EID8	0x63		/* Receive buffer 0 extended identifier high register	*/
#define RXB0EID0	0x64		/* Receive buffer 0 extended identifier low register	*/
#define RXB0DLC		0x65		/* Receive buffer 0 data length code register			*/
#define RXB0D0		0x66		/* Receive buffer 0 data byte 0 register				*/
#define RXB0D1		0x67		/* Receive buffer 0 data byte 1 register				*/
#define RXB0D2		0x68		/* Receive buffer 0 data byte 2 register				*/
#define RXB0D3		0x69		/* Receive buffer 0 data byte 3 register				*/
#define RXB0D4		0x6A		/* Receive buffer 0 data byte 4 register				*/
#define RXB0D5		0x6B		/* Receive buffer 0 data byte 5 register				*/
#define RXB0D6		0x6C		/* Receive buffer 0 data byte 6 register				*/
#define RXB0D7		0x6D		/* Receive buffer 0 data byte 7 register				*/
#define RXB1CTRL	0x70		/* Receive buffer 1 control register					*/
#define RXB1SIDH	0x71		/* Receive buffer 1 standard identifier high register	*/
#define RXB1SIDL	0x72		/* Receive buffer 1 standard identifier low register	*/
#define RXB1EID8	0x73		/* Receive buffer 1 extended identifier high register	*/
#define RXB1EID0	0x74		/* Receive buffer 1 extended identifier low register	*/
#define RXB1DLC		0x75		/* Receive buffer 1 data length code register			*/
#define RXB1D0		0x76		/* Receive buffer 1 data byte 0 register				*/
#define RXB1D1		0x77		/* Receive buffer 1 data byte 1 register				*/
#define RXB1D2		0x78		/* Receive buffer 1 data byte 2 register				*/
#define RXB1D3		0x79		/* Receive buffer 1 data byte 3 register				*/
#define RXB1D4		0x7A		/* Receive buffer 1 data byte 4 register				*/
#define RXB1D5		0x7B		/* Receive buffer 1 data byte 5 register				*/
#define RXB1D6		0x7C		/* Receive buffer 1 data byte 6 register				*/
#define RXB1D7		0x7D		/* Receive buffer 1 data byte 7 register				*/

#define CANCTRL_REQOP_CFG	0x80
#define CANCTRL_REQOP_NORM	0x00
#define CANCTRL_ABAT		0x10
#define CANCTRL_OSM			0x08
#define CANCTRL_CLKEN		0x04
#define CANCTRL_CLKPRE1		0x00
#define CANCTRL_CLKPRE2		0x01
#define CANCTRL_CLKPRE4		0x02
#define CANCTRL_CLKPRE8		0x03
#define CANCTRL_OP_MASK		0xE0


#define CANINTF_MERRF		0x80
#define CANINTF_WAKIF		0x40
#define CANINTF_ERRIF		0x20
#define CANINTF_TX2IF		0x10
#define CANINTF_TX1IF		0x08
#define CANINTF_TX0IF		0x04
#define CANINTF_RX1IF		0x02
#define CANINTF_RX0IF		0x01

#define TXBCTRL_ABTF		0x40
#define TXBCTRL_MLOA		0x20
#define TXBCTRL_TXERR		0x10
#define TXBCTRL_TXREQ		0x08
#define TXBCTRL_TXP3		0x03
#define TXBCTRL_TXP2		0x02
#define TXBCTRL_TXP1		0x01
#define TXBCTRL_TXP0		0x00

#define RXBCTRL_RXMOFF		0x60
#define RXBCTRL_RXMEXT		0x40
#define RXBCTRL_RXMSTD		0x20
#define RXBCTRL_RXMEXTSTD	0x00
#define RXBCTRL_RXRTR		0x08
#define RXBCTRL_FILHIT		0x01

#define EFLG_RX1OVR			0x80
#define EFLG_RX0OVR			0x40

#define BFPCTRL_B1BFS		0x20
#define BFPCTRL_B0BFS		0x10
#define BFPCTRL_B1BFE		0x08
#define BFPCTRL_B0BFE		0x04
#define BFPCTRL_B1BFM		0x02
#define BFPCTRL_B0BFM		0x01


/* MCP2515 SPI instructions */

#define SPI_RESET	0xC0		/* Reset internal registers to default state			*/
#define SPI_READ	0x03		/* Read data from register								*/
#define SPI_RDBUF	0x90		/* Read receive buffer									*/
#define SPI_WRITE	0x02		/* Write data to register								*/
#define SPI_LDBUF	0x40		/* Load transmit buffer									*/
#define SPI_RTS		0x80		/* Request message transmission							*/
#define SPI_RDSTAT	0xA0		/* Read status bits										*/
#define SPI_RXSTAT	0xB0		/* Read receive status bits								*/
#define SPI_MODIFY	0x05		/* Modify bits in register								*/

/* Hardware flags */

/* Definition of the CAN specific information structure contained within
   the generic channel definition structure (see channel.h).
*/

#define WSIZE_CAN	1		/* Workspace size */

/* Information structure for CAN card

   The first 5 entries of this structure must match with those in the
   2GP1SV and MBOARD information structures.
*/

typedef struct info_can {
	hw_can *hw;					/* Hardware base address							*/
	int   input;				/* Input channel number on CAN card					*/
	int   output;				/* Output channel number on CAN card				*/
	uint8_t  ledmask;				/* Mask for LED access								*/
	uint8_t *ledstate;				/* Copy of LED state outputs						*/
	uint8_t  work[WSIZE_CAN];		/* Local workspace									*/
} info_can;

/* Function prototypes */

uint32_t hw_can_install(int slot, int *numinchan, int *numoutchan, int *nummiscchan, 
				     hw_can *hw, int flags);
uint32_t hw_can_post_install(int slot, hw_can *hw, int flags);
void hw_can_save(int slot, uint32_t status);
int hw_canbus_reset_mcp2515(hw_can *base);
int hw_canbus_wr_mcp2515(hw_can *base, uint8_t reg, uint8_t data);
int hw_canbus_rd_mcp2515(hw_can *base, uint8_t reg, uint8_t *data);
int hw_canbus_modify_mcp2515(hw_can *base, uint8_t reg, uint8_t mask, uint8_t data);
int hw_canbus_mcp2515_setmode(hw_can *base, uint8_t mode);
void hw_canbus_spi_instr(hw_can *base, uint8_t instr);
int hw_canbus_send_msg(hw_can *base, uint32_t cob_id, int txlen, uint8_t *txdata, int *rxlen, uint8_t *rxdata);
int hw_canbus_start_remote_node(hw_can *base, uint32_t id);

#endif
