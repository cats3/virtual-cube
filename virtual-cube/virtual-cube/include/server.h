/********************************************************************************
 * MODULE NAME       : server.h													*
 * PROJECT		     : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Headers for general server routines.						*
 ********************************************************************************/

#ifndef __SERVER_H
#define __SERVER_H

#include <stdint.h>
#include <mqueue.h>
#include <pthread.h>

 //#include <std.h>
 //#include <tsk.h>
 //#include <mbx.h>
#include "rtbuffer.h"

/* Structure holding details of server connections */

typedef struct connection {
	int		 id;			/* Connection ID							*/
	int 		 state;			/* Current connection state					*/
	int		 closing;		/* Connection about to close				*/
	int		 close;			/* Force connection to close				*/
	uint32_t 	 timeout;		/* Handler timeout period					*/
	uint32_t		 action;		/* Handler timeout action					*/
	uint32_t 	 timeoutctr;	/* Counter for timeout detection			*/
	pthread_t tsk;	/* POSIX pthread handle */
	mqd_t mbx;	/* POSIX message queue (mailbox) */
	//TSK_Handle tsk;			/* Handler task handle						*/
	//MBX_Handle mbx;			/* Mailbox handle							*/
	rtctrl* streamdef;	/* Pointer to realtime stream definition	*/
	rtctrl* stream;		/* Pointer to realtime stream control		*/
	unsigned int komask;
	struct connection* next;	/* Pointer to next connection				*/
} connection;

/* Message sent from server when posting to mailbox to start handler task */

typedef struct conn_msg {
	int socket;				/* Socket to use for connection */
} conn_msg;

#define CONN_FREE	0		/* Connection is free	*/
#define CONN_BUSY	1		/* Connection is busy	*/

/* Loader has no communications timeout */

#ifdef LOADER
#define DEFAULT_CONN_TIMEOUT	0xFFFFFFFFU	/* Loader default no connection timeout 	*/
#define DEFAULT_CONN_ACTION		0			/* Loader default timeout action	 		*/
#else
#define DEFAULT_CONN_TIMEOUT	300*100		/* Default connection timeout = 300 seconds	*/
											/* No timeout = 0xFFFFFFFFU					*/
#define DEFAULT_CONN_ACTION		0			/* Default timeout action	 				*/
#endif								

void rt_sktout(int skt, buffer* buf, uint32_t size);
void rt_sktin(int skt, buffer* buf, uint32_t size, connection* conn);
void handler_status(connection* list, int* free, int* busy, int* total);

#endif
