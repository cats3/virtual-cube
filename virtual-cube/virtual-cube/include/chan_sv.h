/********************************************************************************
 * MODULE NAME       : chan_sv.h												*
 * MODULE DETAILS    : Header for servo-valve output channel specific routines	*
 ********************************************************************************/

#ifndef __CHAN_SV_H
#define __CHAN_SV_H

#include <stddef.h>
#include <stdint.h>

/* Configuration information for SV output */

typedef struct cfg_sv {
	uint32_t flags;				/* Control flags:
										Bit 0 	  : Reserved						
										Bit 1 	  : Polarity inverted					
										Bit 2 	  : Reserved
										Bit 3 	  : Reserved
										Bit 4,5   : Reserved							
										Bit 6	  : Reserved							
										Bit 7	  : Reserved							
										Bit 8	  : Reserved
										Bit 9	  : Reserved
										Bit 10	  : Reserved
										Bit 11-15 : Reserved (transducer type)
										Bit 16	  : Reserved 					   */
	float gain;					/* Current gain setting								
									For current driven valves, this is the ratio of 
									required current to	actual current (from the 
									current calibration table)
									For voltage driven valves, this is the 
									calibration gain value.							*/
	float gaintrim;				/* Current gain trim setting						*/
	uint32_t svtype;				/* Selected SV type									*/
	float current;				/* Selected full-scale SV current range	(mA)		*/
	float balance;				/* Balance adjustment value							*/
	uint32_t dither_enable;		/* Dither enable									*/
	float dither_freq;			/* Dither frequency									*/
	float dither_ampl;			/* Dither amplitude									*/
	uint32_t faultmask;			/* Current fault enable mask						*/
	int   reserved1;			/* Reserved for future use							*/
	int   reserved2;			/* Reserved for future use							*/
	int   reserved3;			/* Reserved for future use							*/
} cfg_sv;

/* Hardware calibration information for SV output 

   Important - ensure that size of calibration data structure does not exceed the
			   size of the cal_gp structure.
*/

typedef struct cal_sv {
	int	  cal_offset;			/* Calibrated voltage offset						*/
	float cal_gain;				/* Calibrated voltage gain							*/
	float cal_svitable[0x200];	/* Table of calibration SV current values (mA)		
								   Entry 0x1FF is used as a marker to indicate that
								   current zero calibration values are available in
								   entries 0x100 - 0x1FE.
								*/
} cal_sv;

/* Incomplete declaration of chandef to allow function prototypes to work */

typedef struct chandef chandef;

extern void  chan_sv_initlist(void);
extern void  chan_sv_set_pointer(chandef *def, int *ptr);
extern int  *chan_sv_get_pointer(chandef *def);
extern void  chan_sv_default(chandef *def, uint32_t chanid);
extern void  chan_sv_set_gain(chandef *def, float gain, uint32_t flags, int mirror);
extern void  chan_sv_read_gain(chandef *def, float *gain, uint32_t *flags);
extern void  chan_sv_set_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim, int calmode, int mirror);
extern void  chan_sv_read_gaintrim(chandef *def, float *pos_gaintrim, float *neg_gaintrim);
extern void  chan_sv_set_caltable(chandef *def, int entry, float value);
extern float chan_sv_read_caltable(chandef *def, int entry);
extern void  chan_sv_set_calgain(chandef *def, float gain);
extern float chan_sv_read_calgain(chandef *def);
extern void  chan_sv_set_caloffset(chandef *def, float offset);
extern float chan_sv_read_caloffset(chandef *def);
extern void  chan_sv_set_current(chandef *def, uint32_t type, float current, uint32_t flags, int mirror);
extern void  chan_sv_read_current(chandef *def, uint32_t *type, float *current);
extern void  chan_sv_set_rpcal(chandef *def, float rpcal);
extern float chan_sv_read_rpcal(chandef *def);
extern void  chan_sv_set_flags(chandef *def, int set, int clr, int updateclamp, int mirror);
extern int   chan_sv_read_flags(chandef *def);
extern void  chan_sv_set_info(chandef *def, float range, char *units, char *name, char *serialno, int mirror);
extern void  chan_sv_read_info(chandef *def, float *range, char *units, char *name, char *serialno);
extern void  chan_sv_set_dither(chandef *def, uint32_t enable, float dither, float freq, int mirror);
extern void  chan_sv_read_dither(chandef *def, uint32_t *enable, float *dither, float *freq);
extern void  chan_sv_set_balance(chandef *def, float balance, int mirror);
extern void  chan_sv_read_balance(chandef *def, float *balance);
extern void  chan_sv_write_userstring(chandef *def, uint32_t start, uint32_t length, char *string, int mirror);
extern void  chan_sv_read_userstring(chandef *def, uint32_t start, uint32_t length, char *string);
extern void  chan_sv_write_faultmask(chandef *def, uint32_t mask, int mirror);
extern uint32_t chan_sv_read_faultstate(chandef *def, uint32_t *capabilities, uint32_t *mask);

#endif
