/********************************************************************************
 * MODULE NAME       : hw_digio.h												*
 * MODULE DETAILS    : Header for Digital I/O card specific routines			*
 *																				*
 ********************************************************************************/

#ifndef __HW_DIGIO_H
#define __HW_DIGIO_H

#include <stdint.h>

/* Definition of the hardware interface on the Digital I/O card */

typedef struct hw_digio {
	union {
		struct {
			uint16_t	output;		/* 	0x00	Write output port              			*/
		} wr;
		struct {
			uint16_t	input;		/*  0x00	Read input port							*/
			uint16_t	output;		/*  0x02	Read output port state					*/
			uint16_t	reserved1;	/*  0x04	Reserved								*/
			uint16_t	reserved2;	/*  0x06	Reserved								*/
			uint16_t	reserved3;	/*  0x08	Reserved								*/
			uint16_t	reserved4;	/*  0x0A	Reserved								*/
			uint16_t	reserved5;	/*  0x0C	Reserved								*/
			uint16_t	reserved6;	/*	0x0E	Reserved								*/
			uint16_t	reserved7;	/*	0x10	Reserved								*/
			uint16_t	reserved8;	/*	0x12	Reserved								*/
			uint16_t	reserved9; 	/*	0x14	Reserved								*/
			uint16_t	reserved10;	/*	0x16	Reserved								*/
			uint16_t	reserved11;	/*	0x18	Reserved								*/
			uint16_t	reserved12;	/*	0x1A	Reserved								*/
			uint16_t	reserved13;	/*	0x1C	Reserved								*/
			uint16_t	reserved14;	/*	0x1E	Reserved								*/
			uint16_t	reserved15;	/*	0x20	Reserved								*/
			uint16_t	reserved16;	/*	0x22	Reserved								*/
			uint16_t	reserved17;	/*	0x24	Reserved								*/
			uint16_t	reserved18;	/*	0x26	Reserved								*/
			uint16_t	reserved19;	/*	0x28	Reserved								*/
			uint16_t	fault;		/*  0x2A	Read fault status inputs				*/
			uint16_t	reserved20;	/*	0x2C	Reserved								*/
			uint16_t	reserved21;	/*	0x2E	Reserved								*/
			uint16_t	reserved22;	/*	0x30	Reserved								*/
			uint16_t	reserved23;	/*	0x32	Reserved								*/
			uint16_t	reserved24;	/*	0x34	Reserved								*/
			uint16_t	reserved25;	/*	0x36	Reserved								*/
			uint16_t	reserved26;	/*	0x38	Reserved								*/
			uint16_t	reserved27;	/*	0x3A	Reserved								*/
			uint16_t	test;		/*  0x3C	Read test register						*/
			uint16_t	ident;		/*  0x3E	Read identifier register				*/
		} rd;
	} u;
} hw_digio;

/* Definition of the Digital I/O specific information structure contained within
   the generic channel definition structure (see channel.h).
*/

#define WSIZE_DIGIO		1		/* Workspace size */

/* Information structure for Digital I/O card

   The first 5 entries of this structure must match with those in the
   2GP1SV and MBOARD information structures.
*/

typedef struct info_digio {
	hw_digio *hw;				/* Hardware base address							*/
	int   input;				/* Input channel number on 4AD2DA card				*/
	int   output;				/* Output channel number on 4AD2DA card				*/
	uint8_t  ledmask;				/* Mask for LED access								*/
	uint8_t *ledstate;				/* Copy of LED state outputs						*/
	uint8_t  work[WSIZE_DIGIO];	/* Local workspace									*/
} info_digio;

/* Function prototypes */

uint32_t hw_digio_install(int slot, int *numinchan, int *numoutchan, int *nummiscchan, 
					    int *numdigiochan, hw_digio *hw, int flags);
void hw_digio_save(int slot, uint32_t status);

#endif
