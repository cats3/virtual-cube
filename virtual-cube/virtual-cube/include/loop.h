/********************************************************************************
 * MODULE NAME       : loop.h													*
 * MODULE DETAILS    : Definitions related to control loop operation.			*
 ********************************************************************************/

#ifndef __LOOP_H
#define __LOOP_H

#include "tcp_ip/support.h"
#include "channel.h"

typedef enum  FaderStatus
{
  FaderStopped = 0,
  StaticFadeIn,
  DynamicFadeIn,
  FaderRunning,
  DynamicFadeOut,
  StaticFadeOut,
  FaderPaused
} FaderStatus;


typedef struct loopinfo {
  uint32_t status;								/* Status flags
  												Bit 0:	Loop enabled							*/
  uint32_t	control;							/* Control flags
  												Bit 0:	Disable loop							*/
  uint32_t chan;								/* Control channel									*/
  
  float servoBalance;						/* Offset for servo-valve           				*/
  float ditherIncrement;					/* Increment for dither oscillator  				*/
  float ditherAmplitude;                    /* Amplitude for dither oscillator  				*/
  float ditherPhase;					    /* Phase for dither oscillator      				*/ 
  float ditherOutput ;						/* Output of dither oscillator      				*/
  float unequalAreaCompensation;			/* Scaling factor for unequal area  				*/
  s32_t unequalAreaDirection;               /* Flag to indicate direction of 
                                               unequal area scaling 1 = Positive				*/
  float setpoint;							/* Current setpoint value           				*/
  float testBias;							/* Current test bias value 							*/
  float span;                               /* Current span level               				*/

  FaderStatus faderStatus;					/* Status of faders 								*/
  float       staticFader;
  float       dynamicFader;
  float       staticInIncrement;
  float       staticOutIncrement;
  float       dynamicInIncrement;
  float       dynamicOutIncrement;

  s32_t simEnable;                          /* Simulation mode enable flag						*/
  float simControlFeedback;					/* Simulation control feedback						*/
  float simCap;								/* Simulation integrator capacitor  				*/
  float simGain;							/* Simulation mode gain (time constant factor)		*/
  float lastSimInput;						/* Store for last input value into Sim capacitor 	*/
       

} loopinfo;

/* Masks for control loop status word */

#define LOOP_ENABLED	0x01				/* Loop enabled						*/

/* Masks for control loop control word */

#define LOOP_DISABLE	0x01				/* Disable loop						*/


/* Constants for derivative high pass filter */
#define Samplerate 4096.0
#define Tby2	   (1.0 / 8192.0) 

#define DerivA1		 	1.21072803
#define DerivB0 			0.39463598 
#define TwoPi 		6.283185307179586476925286766559
#endif
