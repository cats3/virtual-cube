#ifndef __VIRTUAL_CUBE_CONFIG_H
#define __VIRTUAL_CUBE_CONFIG_H

struct configuration {
	uint32_t fw_version;
	uint8_t slots[3];
};

void config_read(const char* conf_filename);
uint32_t config_get_fwversion(void);
uint8_t config_get_slot_type(const int slot);


#endif
