/********************************************************************************
 * MODULE NAME       : msgserver.h												*
 * PROJECT		     : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Headers for message server routines.						*
 ********************************************************************************/

#ifndef __MSGSERVER_H
#define __MSGSERVER_H

#include <stdint.h>

#include "server.h"

/* Message packet header */

typedef struct commhdr {
  uint8_t  dst;			/* Packet destination					*/
  uint8_t  pkttype;		/* Packet type				
  							Bit 7:	0	Comms msg
  									1	Event report			*/
  uint8_t  reserved1;		/* Reserved, set to zero				*/
  uint8_t  reserved2;		/* Reserved, set to zero				*/
  uint32_t length;			/* Length of data area within packet	*/
} commhdr;

#define PKT_MSG		0x00
#define PKT_EVENT	0x80

/* Generic error response */

struct generic_error {
  uint16_t	command;		/* Command code				*/
  uint16_t	pos;			/* Position within packet	*/
  uint8_t  end;
};

/* Generic message form for use with max/min message processing */

struct generic_maxmin {
  uint16_t	command;		/* Command code				*/
  uint16_t	msginfo;		/* Message information word	*/
};

/* Macro to check for max/min flags in message info word and generate required reply
   where available.
*/

//#define CHECK_MAXMIN(m, r, max, min, reply, msg) if (m->msginfo & 0x03) return(process_maxmin(m, r, max, min, reply, msg));

#define DEFAULT_MSGBUFFER		32768		/* Size of default message buffer		*/
#define DEFAULT_REPLYBUFFER		32768		/* Size of default reply buffer 		*/

#define DEFAULT_RTMSGBUFFER		(65536*4)	/* Size of default message buffer 		*/
#define DEFAULT_RTREPLYBUFFER	(65536*4)	/* Size of default reply buffer 		*/

/* Message handler error codes */

#define NO_ERROR				0			/* No error								*/
#define ERROR_ILLEGAL_MESSAGE	1			/* Error - illegal message				*/
#define ERROR_ILLEGAL_CHANNEL	2			/* Error - illegal channel				*/
#define ERROR_ILLEGAL_FUNCTION	3			/* Error - illegal function				*/
#define ERROR_OPERATION_FAILED	4			/* Error - requested operation failed	*/
#define ERROR_ILLEGAL_PARAMETER	5			/* Error - illegal parameter			*/
#define ERROR_CNET_TIMEOUT		6			/* Error - timeout on CNet comms		*/
#define ERROR_CNET_DESTINATION	7			/* Error - illegal CNet destination		*/
#define ERROR_NO_STREAM			8			/* Error - no stream defined			*/
#define ERROR_TOO_LONG			9			/* Error - message too long				*/
#define ERROR_FW_CORRUPT		10			/* Error - firmware corrupt				*/
#define ERROR_SECURITY_LEVEL	11			/* Error - invalid security level		*/
#define ERROR_SEMAPHORE_DENIED	12			/* Error - semaphore access denied		*/

/* Connection lists */

extern connection *msg_list;	/* List of standard message connections */
extern connection *rtmsg_list;	/* List of realtime message connections */
extern connection *semaphore;	/* Semaphore for controlled access		*/

/* Error handler functions */

int error_illegal_channel(uint8_t *reply, uint32_t *replylength, uint32_t chan);
int error_illegal_function(uint8_t *reply, uint32_t *replylength);
int error_operation_failed(uint8_t *reply, uint32_t *replylength, uint32_t reason);
int error_illegal_parameter(uint8_t *reply, uint32_t *replylength);
int error_no_stream(uint8_t *reply, uint32_t *replylength);
int error_too_long(uint8_t *reply, uint32_t *replylength);
int error_fw_corrupt(uint8_t *reply, uint32_t *replylength);
int error_security_level(uint8_t *reply, uint32_t *replylength);
int error_semaphore_denied(uint8_t *reply, uint32_t *replylength);

void timeout_handler(void);
int  comm_close(uint32_t id);

void* msg_server(void* arg);
void* rtmsg_server(void* arg);

#endif
