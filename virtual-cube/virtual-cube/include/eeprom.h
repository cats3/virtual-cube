/********************************************************************************
 * MODULE NAME       : eeprom.h													*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : Defines structure holding data to be saved in EEPROM.	*
 ********************************************************************************/

#ifndef __EEPROM_H
#define __EEPROM_H

#include <stdint.h>

typedef struct config_network {
	uint32_t source;			/* Network configuration source */
	uint8_t  ipaddr[4];		/* Manual IP address setting	*/
	uint8_t  subnetmask[4];	/* Manual subnet mask setting	*/
} config_network;

/* Page numbers used within 1-wire EEPROM */

#define PAGE_MAC_ADDRESS	0		/* Page 0 used for MAC address					*/
#define PAGE_BOARDSN		1		/* Page 1 used for mainboard serial number		*/
#define PAGE_NETCONFIG		2		/* Page 2 used for network configuration data	*/
#define PAGE_CTRLSN			3		/* Page 3 used for controller serial number		*/
#define PAGE_CTRLNAME		4		/* Page 4 used for controller name string		*/
#define PAGE_PANELCFG		5		/* Page 5 used for rear panel configuration map	*/
#define PAGE_RIGNAME		6		/* Page 6 used for rig name	string				*/

/* Flags to control 1-wire read/write operations */

#define WR_CHKSUM			1		/* Write checksum to EEPROM						*/
#define CHK_CHKSUM			2		/* Check checksum when reading					*/

/* Flags used to control configuration EEPROM read/write operations */

#define EEPROM_WRITE_CHECKSUM	0x01
#define EEPROM_READ_CHECKSUM	0x02

/* Offsets into EEPROM board identification data */

#define EEPROM_OFFSET_VALID		1	/* Start of validity check area					*/

#if (defined(CONTROLCUBE) || defined(AICUBE))

#define EEPROM_OFFSET_SERIALNO	5	/* Start of serial number area (not mainboard)	*/
#define EEPROM_OFFSET_GENERAL	13	/* Start of general board data					*/

#endif

#ifdef SIGNALCUBE

#define EEPROM_OFFSET_SERIALNO	6	/* Start of serial number area (not mainboard)	*/
#define EEPROM_OFFSET_GENERAL	14	/* Start of general board data					*/

#endif

void slot_ident(uint32_t flush);
void slot_post_init(void);
int dsp_1w_writepage(int page, uint8_t* data, int len, int flags);
int dsp_1w_readpage(int page, uint8_t* data, int len, int flags);
void write_eeprom(int addr, int size, void* src);
void read_eeprom(int addr, int size, void* dst);

#endif
