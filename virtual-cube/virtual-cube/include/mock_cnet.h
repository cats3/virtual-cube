#ifndef __MOCK_CNET_H
#define __MOCK_CNET_H

#include <stdint.h>

struct mock_cnet_address_space
{
	uint32_t cnet_mem_data_all[512];

	uint32_t* cnet_mem_data_in;
	uint32_t* cnet_mem_data_out;

	uint16_t cnet_mem_ctrl[512];

	/* Write registers */
	uint16_t cnet_reg_ctrl; /* Control register							*/
	uint16_t cnet_reg_comm; 	/* Communications control register			*/
	uint16_t cnet_reg_syncctrl; 	/* ADC/DAC sync output control register		*/
	uint16_t cnet_reg_rstint; 	/* Reset CNet interrupt flag				*/
	uint16_t cnet_reg_intctrl; 	/* CNet interrupt control register			*/
	uint16_t cnet_reg_expintctrl; 	/* CNet exp bus interrupt control register	*/
	uint16_t cnet_reg_localid; 	/* CNet local ID register					*/
	uint16_t cnet_reg_generr; 	/* Error generation control register		*/
	uint16_t cnet_reg_logctrl; 	/* Packet logging control register			*/
	uint32_t cnet_reg_errdata; 	/* Error packet data register				*/
			  
	/* Read registers */
	uint16_t cnet_reg_connstat;	/* Connection status flags					*/
	uint16_t cnet_reg_position;	/* Ring position							*/
	uint16_t cnet_reg_intstat;	/* Interrupt status flags					*/
	uint16_t cnet_reg_chksum_ctr;	/* Checksum error counter					*/
	uint16_t cnet_reg_err0; 	/* Error data register 0					*/
	uint16_t cnet_reg_err1; 	/* Error data register 1					*/
	uint16_t cnet_reg_err2; 	/* Error data register 2					*/
	uint16_t cnet_reg_err3; 	/* Error data register 3					*/
	uint16_t cnet_reg_errchk; 	/* Error checksum register 					*/
	uint16_t cnet_reg_loga0; 	/* Log bank A data register 0				*/
	uint16_t cnet_reg_loga1; 	/* Log bank A data register 1				*/
	uint16_t cnet_reg_loga2; 	/* Log bank A data register 2				*/
	uint16_t cnet_reg_loga3; 	/* Log bank A data register 3				*/
	uint16_t cnet_reg_logchksum0; 	/* Log checksum register 0					*/
	uint16_t cnet_reg_logchksum1; 	/* Log checksum register 1					*/
	uint16_t cnet_reg_logchksum2; 	/* Log checksum register 2					*/
	uint16_t cnet_reg_logchksum3; 	/* Log checksum register 3					*/
	uint16_t cnet_reg_logb0; 	/* Log bank B data register 0				*/
	uint16_t cnet_reg_logb1; 	/* Log bank B data register 1				*/
	uint16_t cnet_reg_logb2; 	/* Log bank B data register 2				*/
	uint16_t cnet_reg_logb3; 	/* Log bank B data register 3				*/
};

extern struct mock_cnet_address_space* mock_cnet_controller;

int* cnet_get_slotaddr_out(uint32_t slot, uint32_t proc);
void cnet_swap(void);

#endif
