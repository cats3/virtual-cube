/********************************************************************************
 * MODULE NAME       : msghandler.h												*
 * PROJECT			 : Control Cube	/ Signal Cube								*
 * MODULE DETAILS    : Control Cube message handler function prototypes			*
 ********************************************************************************/

#include <stdint.h>

#include "server.h"

typedef struct {
  int(* const messagelist)(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
  int security;
} msgdef;

/* Loader messages */

int fn_read_firmware_version(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_firmware_information(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_device_type(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_erase_flash(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_erase_status(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_prog_flash(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_erase_flash_block(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_cnet_info(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_ip_address(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_ip_address(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_mac_address(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_hw_restart(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_fw_validate(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

int fn_testmode_set_address(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_write8(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_write16(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_write32(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_read8(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_read16(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_read32(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

int fn_testmode_i2c_start(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_i2c_restart(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_i2c_stop(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_i2c_send(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_i2c_read(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

int fn_testmode_dspb_i2c_start(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_dspb_i2c_restart(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_dspb_i2c_stop(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_dspb_i2c_send(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_dspb_i2c_read(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

int fn_testmode_onew_init(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_onew_reset(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_onew_send(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_onew_read(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

int fn_testmode_enable_watchdog(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_disable_watchdog(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

int fn_testmode_write32b(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_testmode_read32b(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

/* Parameter messages */

int fn_send_parameter_string(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

/* Standard messages */

int fn_set_excitation(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_excitation(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_exc_cal(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_exc_cal(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_gain(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_gain(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_balance(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_cal(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_led(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_clear_all_leds(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_filter(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_filter(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_filter_freq(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_gaintrim(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_gaintrim(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_neg_gaintrim(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_neg_gaintrim(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_txdrzero(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_txdrzero(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_txdrzero_zero(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_ip_channel(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_ip_channelblock(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_ip_cyclecount(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_ip_cyclecountblock(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_ip_channelblock_ext(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn);
int fn_reset_cyclecount(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_reset_all_cyclecounts(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_write_op_channel(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_slot_configuration(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_chan_information(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_channel_source(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_raw_gain(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_raw_gain(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_calgain(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_calgain(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_caloffset(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_caloffset(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_save_chan_config(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_restore_chan_config(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_save_chan_calibration(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_restore_chan_calibration(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_cnet_channel(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_cnet_slot(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_cnet_mode(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_cnet_valid_range(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_cnet_status(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_hydraulic_output(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_hydraulic_output(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_hydraulic_input(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_define_hydraulic_io(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_hydraulic_io(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_define_hydraulic_mode(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_hydraulic_mode(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_hydraulic_dissipation(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_hydraulic_dissipation(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_hydraulic_name(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_hydraulic_name(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_hydraulic_state(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_hydraulic_control(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_flush_chan_config(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_flush_chan_calibration(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_chan_status(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_peaks(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_reset_peaks(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_reset_all_peaks(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_chan_flags(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_chan_flags(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_write_map(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_map(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_refzero(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_refzero(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_refzero_zero(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_undo_refzero(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_sv_range(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_sv_range(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_raw_current(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_raw_excitation(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_caltable(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_sv_dither(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_sv_dither(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_sv_balance(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_sv_balance(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_chan_info(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_chan_info(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_caltable(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_full_caltable(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_serno(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_sysinfo(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_define_system_name(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_system_name(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_define_rig_name(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_rig_name(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_define_comm_timeout(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_comm_timeout(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_define_comm_timeout_ext(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_comm_timeout_ext(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_comm_closing(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_comm_id(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_force_comm_close(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_security_level(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_security_level(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_semaphore(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_semaphore(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_kinet_mapping(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_kinet_mapping(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_cnet_maptable(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_cnet_maptable(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_cnet_timeout(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_cnet_timeout(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_reset_parameters(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_reset_parameters(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_chan_memptrs(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_kinet_flags(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_kinet_flags(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_realtime_config(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_realtime_config(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_serno(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_nv_state(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_panel_config(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_panel_config(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_nv_control(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_chan_override(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_userdata(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_userdata(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_chanstring(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_chanstring(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_peak_threshold(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_peak_threshold(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_peak_threshold_ext(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_peak_timeout(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_peak_timeout(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_fault_mask(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_fault_state(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_guard_state(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_digtxdr_config(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_digtxdr_config(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_simulation_mode(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_simulation_mode(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_runtime(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

/* Realtime data transfer messages */

int fn_define_output_list(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_output_list(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_define_input_list(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_input_list(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_total_buffer(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_output_buffer(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_input_buffer(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_define_output_buffer(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_define_input_buffer(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_buffer_status(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_clear_error_flags(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_control_realtime_transfer(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_pause_realtime_transfer(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_resume_realtime_transfer(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_rt_stop_type(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_rt_stop_type(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_rt_stop_parameters(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_rt_stop_parameters(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_transfer_realtime_data(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

int fn_read_eventlog_status(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_flush_eventlog(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_eventlog_entry(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

int fn_read_timestamp(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

/* KiNet asynchronous communications messages */

int fn_send_kinet_message(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

int fn_set_digio_info(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_digio_info(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_write_digio_output(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_digio_input(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_digio_output(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

/* Signal Cube messages */

int fn_set_gauge_type(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_gauge_type(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_offset(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_offset(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_start_zero(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_zero_progress(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_active_gauges(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_active_gauges(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_gauge_info(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_gauge_info(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_hpfilter_config(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_hpfilter_config(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_set_calzero_entry(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_calzero_entry(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

/* Event report management messages */

int fn_open_event_connection(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_close_event_connection(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_add_event_report(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_remove_event_report(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);

/* Hand controller messages */

int fn_send_hc_instruction(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
int fn_read_hc_keystate(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn);
