/********************************************************************************
 * MODULE NAME       : controller.c												*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Functions to interface with Karen's controller code.		*
 ********************************************************************************/

#include <stdlib.h>
#include <stdint.h>

 //#include <std.h>
 //#include <log.h>
#include "defines.h"
//#include "headers.h"
#include "shared.h"
#include "nonvol.h"
#include "controller.h"

#ifdef _DSPA

#if (defined(CONTROLCUBE) || defined(AICUBE))

/********************************************************************************
  FUNCTION NAME     : ctrl_initialise1
  FUNCTION DETAILS  : First stage initialise controller functions

  Initialises the controller functions provided by Karen Orton's parameter
  class code.

********************************************************************************/

void ctrl_initialise1(void)
{
	uint32_t smpbs;	/* DSP B Shared area base address */

	smpbs = (intptr_t)dspb_shared_c3appdata;			/* Get sample shared address	*/
	CtrlInit(dspb_shared_c3appdata);								/* Initialise controller code	*/
}

/********************************************************************************
  FUNCTION NAME     : ctrl_initialise2
  FUNCTION DETAILS  : Second stage initialise controller functions

  Initialises the controller functions provided by Karen Orton's parameter
  class code.

********************************************************************************/

void ctrl_initialise2(void)
{
	void* snvbs;	/* Static non-volatile data storage base	*/
	void* dnvbs;	/* Dynamic non-volatile data storage base	*/

	nv_getbase((uint32_t**)&snvbs, (uint32_t**)&dnvbs);		/* Get NV base addresses	*/
	CtrlSetSnvBs((void*)((intptr_t)snvbs + sizeof(snv)));	/* Set parameter SNV area	*/
	CtrlSetDnvBs((void*)((intptr_t)dnvbs + sizeof(dnv)));	/* Set parameter DNV area	*/
}

/********************************************************************************
  FUNCTION NAME     : ctrl_txdrfault
  FUNCTION DETAILS  : Inform control loop of a transducer fault and dump
					  hydraulics.
********************************************************************************/

void ctrl_txdrfault(int chan)
{
	chandef* def = chanptr(chan);

	if (def && !(def->ctrl & FLAG_DISABLE)) {

		/* Channel not disabled, so dump hydraulics */

		if ((hyd_get_status() & HYD_STATUS_MASK) >= HYD_ACK_ON) {
			hyd_unload();
		}
	}
}

/********************************************************************************
  FUNCTION NAME     : informRangeChange
  FUNCTION DETAILS  : Callback generated whenever an input channel range
					  change occurs.
********************************************************************************/

void informRangeChange(int chandefIndex, float range, char* units)
{
	struct chandef* def = &inchaninfo[chandefIndex];
	if (def->setfullscale) {
		def->setfullscale(def, range);
	}
}

#endif /* CONTROLCUBE */

/********************************************************************************
  FUNCTION NAME     : informEnableChange
  FUNCTION DETAILS  : Callback generated whenever an input channel enable
					  change occurs.
********************************************************************************/

void informEnableChange(int chandefIndex, int enable)
{
	struct chandef* def = chanptr(chandefIndex);
	if (def) {
		chan_modify_flags(def, FLAG_DISABLE, enable ? 0 : FLAG_DISABLE);
	}
}

#ifdef SIGNALCUBE

volatile uint32_t tracking_request = FALSE;	/* Dummy request for control loop tracking */
extern void* C3AppData;

/********************************************************************************
  FUNCTION NAME     : ctrl_initialise
  FUNCTION DETAILS  : Initialise controller functions

  Initialises the controller functions provided by Karen Orton's parameter
  class code.

********************************************************************************/

void ctrl_initialise1(void)
{
	uint32_t smpbs;	/* DSP B Shared area base address			*/

	dspb_shared_c3appdata = &C3AppData;
	smpbs = (uint32_t)dspb_shared_c3appdata;			/* Get sample shared address	*/
	CtrlInit(smpbs);								/* Initialise controller code	*/
}

/********************************************************************************
  FUNCTION NAME     : ctrl_initialise2
  FUNCTION DETAILS  : Second stage initialise controller functions

  Initialises the controller functions provided by Karen Orton's parameter
  class code.

********************************************************************************/

void ctrl_initialise2(void)
{
	void* snvbs;	/* Static non-volatile data storage base	*/
	void* dnvbs;	/* Dynamic non-volatile data storage base	*/

	nv_getbase((uint32_t**)&snvbs, (uint32_t**)&dnvbs);		/* Get NV base addresses	*/
	CtrlSetSnvBs((void*)((uint32_t)snvbs + sizeof(snv)));	/* Set parameter SNV area	*/
	CtrlSetDnvBs(dnvbs);								/* Set parameter DNV area	*/
}

/********************************************************************************
  FUNCTION NAME     : informRangeChange
  FUNCTION DETAILS  : Callback generated whenever an input channel range
					  change occurs.
********************************************************************************/

void informRangeChange(int chandefIndex, float range, char* units)
{
}

#endif /* SIGNALCUBE */

#endif


#ifdef _DSPB

extern volatile uint32_t tmp_sat_state;	/* Temp saturation state		*/
extern volatile uint32_t saturation_state;	/* Global saturation state		*/

/********************************************************************************
  FUNCTION NAME     : ctrl_sat_state
  FUNCTION DETAILS  : Returns global saturation state word.
********************************************************************************/

uint32_t ctrl_sat_state(void)
{
	return(saturation_state);
}

#endif
