/********************************************************************************
 * MODULE NAME       : chan_ad.c												*
 * MODULE DETAILS    : Routines to support a high-level ADC transducer channel.	*
 ********************************************************************************/

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "porting.h"

 //#include <std.h>
 //#include <log.h>

#include "defines.h"
//#include "ccubecfg.h"

#include "asmcode.h"
#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"



/******************************************************************
* List of entries in chandef structure that are saved into EEPROM *
*																  *
* Entries 0 to 3 must be compatible across all channel types.	  *
*																  *
******************************************************************/

static configsave chan_ad_eelist_type0[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 4 */
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 5 */
  {0,0}
};

/* Type 1 list added filename for linearisation map table. */

static configsave chan_ad_eelist_type1[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 4 */
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 5 */
  {offsetof(chandef, filename), 128},							/* Entry 6 */
  {0,0}
};

/* Type 2 list added user string area. */

static configsave chan_ad_eelist_type2[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 4 */
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 5 */
  {offsetof(chandef, filename), 128},							/* Entry 6 */
  {offsetof(chandef, userstring), 128},							/* Entry 7 */
  {0,0}
};

/* Type 3 list added negative gain trim value. */

static configsave chan_ad_eelist_type3[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 4 */
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 5 */
  {offsetof(chandef, filename), 128},							/* Entry 6 */
  {offsetof(chandef, userstring), 128},							/* Entry 7 */
  {offsetof(chandef, neg_gaintrim), sizeof(int)},				/* Entry 8	*/
  {0,0}
};

/* Type 4 list added cyclic threshold value. */

static configsave chan_ad_eelist_type4[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 4 */
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 5 */
  {offsetof(chandef, filename), 128},							/* Entry 6 */
  {offsetof(chandef, userstring), 128},							/* Entry 7 */
  {offsetof(chandef, neg_gaintrim), sizeof(int)},				/* Entry 8	*/
  {offsetof(chandef, cycwindow), sizeof(float)},				/* Entry 9	*/
  {0,0}
};

#define CURRENT_CHAN_AD_EELIST_TYPE 4

static configsave* chan_ad_eelist[] = {
  chan_ad_eelist_type0,
  chan_ad_eelist_type1,
  chan_ad_eelist_type2,
  chan_ad_eelist_type3,
  chan_ad_eelist_type4,
};



/* Raw fullscale value depends on the hardware type.

   For Control Cube systems each raw sample is made up from the sum of
   16 x 16-bit samples. The sum is left shifted to align with the MSB,
   therefore the fullscale value is 0x80000000.

   For Signal Cube systems each raw sample is made up from the sum of
   5 x 24-bit samples. The sum is left shifted four places, therefore
   the fullscale value is 0x27FFFFB0.
*/

#if (defined(CONTROLCUBE) || defined(AICUBE))
#define RAW_FS 0x80000000UL
#else
#define RAW_FS 0x27FFFFB0UL
#endif

/********************************************************************************
  FUNCTION NAME     : chan_ad_initlist
  FUNCTION DETAILS  : Initialise configsave list for high-level ADC transducer
					  channel.
********************************************************************************/

void chan_ad_initlist(void)
{
	init_savelist(chan_ad_eelist[CURRENT_CHAN_AD_EELIST_TYPE]);
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_default
  FUNCTION DETAILS  : Load default values for the selected high-level ADC
					  input channel.
********************************************************************************/

void chan_ad_default(chandef* def, uint32_t chanid)
{
	cfg_ad* cfg;
	cal_ad* cal;

	cfg = &def->cfg.i_ad;
	cal = &def->cal.u.c_ad;

	/* Define save information */

	def->eeformat = CURRENT_CHAN_AD_EELIST_TYPE;
	def->eesavelist = chan_ad_eelist[CURRENT_CHAN_AD_EELIST_TYPE];
	def->eesavetable = chan_ad_eelist;

	cal->cal_offset = 0;
	cal->cal_gain = 1.0;

	cfg->flags = FLAG_CYCWINDOW;
	cfg->pos_gaintrim = 1.0;	/* Default positive gaintrim 	*/
	cfg->neg_gaintrim = 1.0;	/* Default negative gaintrim 	*/
	cfg->gain = 1.0;			/* Default gain 				*/
	cfg->txdrzero = 0.0;		/* Default transducer zero		*/

	/* Set reserved locations to zero */

	cfg->faultmask = 0;
	cfg->reserved1 = 0;
	cfg->reserved2 = 0;
	def->filename[0] = '\0';

	def->pos_gaintrim = GAINTRIM_SCALE;
	def->neg_gaintrim = GAINTRIM_SCALE;
	def->txdrzero = 0;
	def->cycwindow = 0.005F; 	/* Default threshold 0.5% 		*/

	/* Set default saturation detection values. These are based on experimental
	   values measured on actual hardware, adjusted by a small safety margin.
	*/

	def->satpos = 467700;
	def->satneg = -467700;

	def->filter1.type = FILTER_DISABLE | FILTER_LOWPASS;
	def->filter1.order = 2;
	def->filter1.freq = 500.0;
	def->filter1.bandwidth = 0.0;
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_set_pointer
  FUNCTION DETAILS  : Set the output pointer for the selected high-level ADC
					  input channel.

					  On entry:

					  def		Points to the channel definition structure
					  ptr		Address to set output pointer

********************************************************************************/

void chan_ad_set_pointer(chandef* def, int* ptr)
{
	def->outputptr = ptr;

	update_shared_channel_parameter(def, offsetof(chandef, outputptr), sizeof(float*));
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_get_pointer
  FUNCTION DETAILS  : Get the output pointer for the selected high-level ADC
					  input channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

int* chan_ad_get_pointer(chandef* def)
{
	return(def->outputptr);
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_set_caltable
  FUNCTION DETAILS  : Sets a calibration table entry for the selected high-level
					  ADC input channel.
********************************************************************************/

void chan_ad_set_caltable(chandef* def, int entry, float value)
{
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_read_caltable
  FUNCTION DETAILS  : Reads a calibration table entry for the selected
					  high-level ADC input channel.
********************************************************************************/

float chan_ad_read_caltable(chandef* def, int entry)
{
	return(0.0);
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_set_calgain
  FUNCTION DETAILS  : Sets the calibration gain for the selected high-level ADC
					  input channel.
********************************************************************************/

void chan_ad_set_calgain(chandef* def, float gain)
{
	cal_ad* c;

	c = &def->cal.u.c_ad;	/* AD specific calibration	*/
	c->cal_gain = gain;
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_read_calgain
  FUNCTION DETAILS  : Reads the calibration gain for the selected high_level ADC
					  input channel.
********************************************************************************/

float chan_ad_read_calgain(chandef* def)
{
	cal_ad* c;

	c = &def->cal.u.c_ad;	/* AD specific calibration	*/
	return(c->cal_gain);
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_set_caloffset
  FUNCTION DETAILS  : Sets the calibration offset for the selected high-level
					  ADC input channel.
********************************************************************************/

void chan_ad_set_caloffset(chandef* def, float offset)
{
	cal_ad* cal;

	cal = &def->cal.u.c_ad;	/* AD specific calibration	*/

	cal->cal_offset = (int)((offset * (float)RAW_FS) / 1.1F);
	def->offset = cal->cal_offset;
	update_shared_channel_parameter(def, offsetof(chandef, offset), sizeof(int));
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_read_caloffset
  FUNCTION DETAILS  : Reads the calibration offset for the selected high-level
					  ADC input channel.
********************************************************************************/

float chan_ad_read_caloffset(chandef* def)
{
	cal_ad* cal;

	cal = &def->cal.u.c_ad;		/* AD specific calibration	*/

	return((((float)cal->cal_offset) * 1.1F) / (float)RAW_FS);
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_set_gain
  FUNCTION DETAILS  : Set the gain for the selected high-level ADC input
					  channel.

					  On entry:

					  def		Points to the channel definition structure
					  gain		Required gain value
					  flags		Bit 0 : Reserved
								Bit 1 : Retain gain trim value
										0 = Reset gain trim value to 1.0
										1 = Retain existing gain trim value
								Bit 2 : Reserved

********************************************************************************/

void chan_ad_set_gain(chandef* def, float gain, uint32_t flags, int mirror)
{
	cal_ad* cal;
	cfg_ad* cfg;
	float trim;
	float ptrim;
	float ntrim;

	cal = &def->cal.u.c_ad;	/* AD specific calibration		*/
	cfg = &def->cfg.i_ad;	/* AD specific configuration	*/

	/* Re-calculate current gain value to get current hardware gain trim value */

	trim = 1.0F / cal->cal_gain;

	/* Combine hardware trim with user trim to produce an overall trim value */

	ptrim = cfg->pos_gaintrim * trim;
	ntrim = cfg->neg_gaintrim * trim;

	if (ptrim > 3.999) ptrim = 3.999;
	if (ntrim > 3.999) ntrim = 3.999;

	def->pos_gaintrim = (uint32_t)(ptrim * (float)GAINTRIM_SCALE);
	def->neg_gaintrim = (uint32_t)(ntrim * (float)GAINTRIM_SCALE);

	update_shared_channel_parameter(def, offsetof(chandef, pos_gaintrim), sizeof(int) * 2);
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_read_gain
  FUNCTION DETAILS  : Read the gain for the selected high-level ADC input
					  channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_ad_read_gain(chandef* def, float* gain, uint32_t* flags)
{
	if (gain) *gain = 1.0F;
	if (flags) *flags = 0;
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_set_gaintrim
  FUNCTION DETAILS  : Set the gaintrim value for the selected high-level ADC
					  input channel.

					  On entry:

					  def			Points to the channel definition structure
					  pos_gaintrim	Pointer to required positive gaintrim value
					  neg_gaintrim	Pointer to required negative gaintrim value
					  calmode		Flag set when calibration mode operating.
									Disables calculation of overall trim and
									forces use of supplied value.

********************************************************************************/

void chan_ad_set_gaintrim(chandef* def, float* pos_gaintrim, float* neg_gaintrim, int calmode, int mirror)
{
	cal_ad* cal;
	cfg_ad* cfg;
	float trim;
	float ptrim;
	float ntrim;

	cal = &def->cal.u.c_ad;	/* AD specific calibration		*/
	cfg = &def->cfg.i_ad;	/* AD specific configuration	*/

	if (pos_gaintrim) {
		ptrim = *pos_gaintrim;
		if (ptrim > 3.999) ptrim = 3.999;
		cfg->pos_gaintrim = ptrim;
	}

	if (neg_gaintrim) {
		ntrim = *neg_gaintrim;
		if (ntrim > 3.999) ntrim = 3.999;
		cfg->neg_gaintrim = ntrim;
	}

	if (!calmode) {

		/* Re-calculate current gain value to get current hardware gain trim value */

		trim = 1.0F / cal->cal_gain;

		/* Combine hardware trim with user trim to produce an overall trim value */

		ptrim = trim * ptrim;
		ntrim = trim * ntrim;
	}

	if (ptrim > 3.999) ptrim = 3.999;
	if (ntrim > 3.999) ntrim = 3.999;

	if (pos_gaintrim) {
		def->pos_gaintrim = (uint32_t)(ptrim * (float)GAINTRIM_SCALE);
		update_shared_channel_parameter(def, offsetof(chandef, pos_gaintrim), sizeof(int));
		if (mirror) mirror_chan_eeconfig(def, 1);		/* Mirror pos gaintrim value	*/
	}
	if (neg_gaintrim) {
		def->neg_gaintrim = (uint32_t)(ntrim * (float)GAINTRIM_SCALE);
		update_shared_channel_parameter(def, offsetof(chandef, neg_gaintrim), sizeof(int));
		if (mirror) mirror_chan_eeconfig(def, 8);		/* Mirror neg gaintrim value	*/
	}

	/* Mirror config to EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 5);		/* Mirror cfg area				*/
	}

}



/********************************************************************************
  FUNCTION NAME     : chan_ad_read_gaintrim
  FUNCTION DETAILS  : Read the gaintrim value for the selected high-level ADC
					  input channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_ad_read_gaintrim(chandef* def, float* pos_gaintrim, float* neg_gaintrim)
{
	cfg_ad* cfg;

	cfg = &def->cfg.i_ad;	/* AD specific configuration	*/

	if (pos_gaintrim) *pos_gaintrim = cfg->pos_gaintrim;
	if (neg_gaintrim) *neg_gaintrim = cfg->neg_gaintrim;
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_set_refzero
  FUNCTION DETAILS  : Set the reference zero value for the selected high-level
					  transducer channel.

					  On entry:

					  track		Flag indicates if control loop should be put
								into tracking mode to adjust setpoint values
								based on offset changes.
********************************************************************************/

int chan_ad_set_refzero(chandef* def, float refzero, int track, int updateclamp, int mirror)
{
	cfg_ad* cfg = &def->cfg.i_ad;	/* AD specific configuration	*/

	/* Validate the supplied reference zero offset */

	if (chan_validate_zero(def, def->txdrzero, refzero) != NO_ERROR) return(ZERO_RANGE);

	/* Inhibit command clamping while we update the offset */

	chan_update_command_clamp(def, 0.0F, 0.0F, TRUE, updateclamp);

	/* Change the reference zero offset */

	chan_set_refzero(def, refzero, track, mirror);

	/* Update the command clamp */

	chan_update_command_clamp(def, cfg->txdrzero, def->refzero, FALSE, updateclamp);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_set_txdrzero
  FUNCTION DETAILS  : Set the transducer zero value for the selected high-level
					  ADC input channel.

					  On entry:

					  def		Points to the channel definition structure
					  txdrzero	Required transducer zero value

********************************************************************************/

int chan_ad_set_txdrzero(chandef* def, float txdrzero, int track, int updateclamp, int mirror)
{
	cfg_ad* cfg;
	int zero;
	float zeroscale;
	int gaintrim;

	cfg = &def->cfg.i_ad;	/* AD specific configuration */

	/* Transducer zero value must be processed to calculate the required offset
	   that needs to be applied at the input to the channel processing path.
	   This must include the effects of:

	   a) Positive/negative calibration gain
	   b) Asymmetrical transducer scaling
	   c) Polarity inversion
	   d) Unipolar transducer scaling

	   Note that when determining whether to use the positive or negative gain trim
	   value in the scaling calculation, the test is reversed from the expected sense
	   because the txdrzero value being used is the required offset to achieve zero
	   and so has the opposite polarity to the actual input signal.

	*/

	/* Select appropriate gaintrim value based on polarity and 2-point cal flag */

	gaintrim = (cfg->flags & FLAG_2PTCAL) ?
		(((txdrzero * ((cfg->flags & FLAG_POLARITY) ? -1.0F : 1.0F)) < 0.0F) ? def->pos_gaintrim : def->neg_gaintrim) :
		def->pos_gaintrim;

	zeroscale = ((float)gaintrim / (float)GAINTRIM_SCALE) *
		((cfg->flags & FLAG_ASYMMETRICAL) ? 0.5F : 1.0F) *
		((cfg->flags & FLAG_POLARITY) ? -1.0F : 1.0F) *
		((cfg->flags & FLAG_UNIPOLAR) ? 2.0F : 1.0F);

	zero = (int)(((txdrzero / zeroscale) * (float)RAW_FS) / 1.1F);

	/* Validate the supplied transducer zero offset */

	if (chan_validate_zero(def, zero, def->refzero) != NO_ERROR) return(ZERO_RANGE);

	cfg->txdrzero = txdrzero;

	/* Inhibit command clamping while we update the offset */

	chan_update_command_clamp(def, 0.0F, 0.0F, TRUE, updateclamp);

	update_txdrzero(def, zero, track);

	/* Update the command clamp */

	chan_update_command_clamp(def, cfg->txdrzero, def->refzero, FALSE, updateclamp);

	if (mirror) {
		mirror_chan_eeconfig(def, 5); 	/* Mirror cfg area		*/
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_read_txdrzero
  FUNCTION DETAILS  : Read the transducer zero value for the selected high-level
					  ADC input channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

float chan_ad_read_txdrzero(chandef* def)
{
	cfg_ad* cfg;

	cfg = &def->cfg.i_ad;	/* AD specific configuration	*/

	return(cfg->txdrzero);
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_txdrzero_zero
  FUNCTION DETAILS  : Zero the current transducer value using the transducer zero
					  offset.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

int chan_ad_txdrzero_zero(chandef* def, int track, int updateclamp, int mirror)
{
	cfg_ad* cfg;
	int zero;
	int gaintrim;
	float fb;
	float fbscale;
	float fbpol;

	cfg = &def->cfg.i_ad;	/* AD specific configuration */

	/* Should disable interrupts around this code */

	fb = chan_read_zerofiltop(def) - def->refzero;	// Calculate input value excluding ref zero

	/* Determine actual polarity of raw feedback signal, taking into account any polarity
	   inversion that has been applied.
	*/

	fbpol = fb * ((cfg->flags & FLAG_POLARITY) ? -1.0F : 1.0F);

	/* Select appropriate gaintrim value based on polarity and 2-point cal flag */

	gaintrim = (cfg->flags & FLAG_2PTCAL) ?
		((fbpol >= 0.0F) ? def->pos_gaintrim : def->neg_gaintrim) :
		def->pos_gaintrim;

	/* Feedback value must be processed to calculate the actual raw feedback signal
	   that is present at the input to the channel processing path.
	   This must include the effects of:

	   a) Positive/negative calibration gain
	   b) Asymmetrical transducer scaling
	   c) Polarity inversion
	   d) Unipolar transducer scaling

	*/

	fbscale = ((float)gaintrim / (float)GAINTRIM_SCALE) *
		((cfg->flags & FLAG_ASYMMETRICAL) ? 0.5 : 1.0) *
		((cfg->flags & FLAG_POLARITY) ? -1.0 : 1.0) *
		((cfg->flags & FLAG_UNIPOLAR) ? 2.0 : 1.0);

	zero = (int)(((fb / fbscale) * (float)RAW_FS) / 1.1F);

	/* Validate the calculated transducer zero offset */

	if (chan_validate_zero(def, (def->txdrzero - zero), 0.0F) != NO_ERROR) return(ZERO_RANGE);

	/* Inhibit command clamping while we update the offset */

	chan_update_command_clamp(def, 0.0F, 0.0F, TRUE, updateclamp);

	def->set_refzero(def, 0.0F, TRUE, UPDATECLAMP, MIRROR);	// Remove ref zero offset

	/* Change the transducer zero offset */

	update_txdrzero(def, (def->txdrzero - zero), track);
	cfg->txdrzero = cfg->txdrzero - fb;

	/* Update the command clamp */

	chan_update_command_clamp(def, cfg->txdrzero, def->refzero, FALSE, updateclamp);

	if (mirror) {
		mirror_chan_eeconfig(def, 5); 	/* Mirror cfg area		*/
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_refzero_zero
  FUNCTION DETAILS  : Zero the current transducer value using the reference zero
					  offset.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

int chan_ad_refzero_zero(chandef* def, int track, int updateclamp, int mirror)
{
	return(chan_ad_set_refzero(def, def->refzero - chan_read_zerofiltop(def), TRUE, UPDATECLAMP, MIRROR));
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_set_flags
  FUNCTION DETAILS  : Modify the control flags for the selected high-level ADC
					  input channel.
********************************************************************************/

void chan_ad_set_flags(chandef* def, int set, int clr, int updateclamp, int mirror)
{
	uint32_t istate;
	cfg_ad* cfg;
	uint32_t oldstate;
	uint32_t newstate;

	cfg = &def->cfg.i_ad; /* AD specific configuration */

/* Determine the new flag state by setting and clearing bits as specified by
   set and clr parameters. Note that the simulation and disable flags in the 
   chandef structure may have been modified by KO code and this will not be 
   reflected in the cfg structure. Therefore, we must read their states directly 
   from the chandef structure.

	   Similarly, the virtual channel flag is stored in the chandef structure, but is
	   not present in the config structure. Therefore, it must also be read directly
	   from the chandef structure.
	*/

oldstate = (cfg->flags & ~(FLAG_SIMULATION | FLAG_VIRTUAL | FLAG_DISABLE)) | (def->ctrl & (FLAG_SIMULATION | FLAG_VIRTUAL | FLAG_DISABLE));
newstate = (oldstate & ~clr) | set;

	/* Update configuration data and channel control flags. Need to protect against interrupts
	   between updating DSP A memory and copying to DSP B memory.
	*/

	cfg->flags = newstate;

	istate = disable_int(); /* Disable interrupts whilst updating */

	def->ctrl = newstate;

	/* Update transducer flags setting in shared channel definition */

	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(int));

	restore_int(istate);

	/* If the asymmetrical, unipolar or polarity flags have changed then update the
	   command clamp values.
	*/

	if ((newstate ^ oldstate) & (FLAG_ASYMMETRICAL | FLAG_UNIPOLAR | FLAG_POLARITY))
		chan_update_command_clamp(def, cfg->txdrzero, def->refzero, FALSE, updateclamp);

	/* Mirror into EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 5);	/* Mirror cfg area		*/
	}

	/* Update transducer flags setting in shared channel definition */

	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(int));
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_read_flags
  FUNCTION DETAILS  : Read the control flags for the selected high-level ADC
					  input channel.
********************************************************************************/

int chan_ad_read_flags(chandef* def)
{
	cfg_ad* cfg;

	cfg = &def->cfg.i_ad;		/* AD specific configuration	*/

	return(cfg->flags);
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_write_map
  FUNCTION DETAILS  : Write a linearisation map table entry for the selected
					  high-level ADC input channel.

					  On entry:

					  flags		Control flags.
									Bit 0,1	Linearisation mode:
										0 		 = Disabled
										Non-zero = Enabled
					  value		Points to the start of linearisation data
								  to copy into definition. If NULL, then
								  no data is copied.
					  filename	Points to the start of the linearisation
								table filename. If NULL then no filename
								is copied.
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_ad_write_map(chandef* def, uint32_t flags, float* value, char* filename,
	int flush, int mirror)
{

#ifdef SIGNALCUBE
	chandef* base;
	base = (chandef*)get_channel_base(def);
#endif

	if (value) memcpy(def->map, value, sizeof(def->map));
	if (filename) memcpy(def->filename, filename, sizeof(def->filename));
	def->cfg.i_ad.flags = (def->cfg.i_ad.flags & ~FLAG_LINEARISE) | ((flags & 0x03) ? FLAG_LINEARISE : 0);
	def->ctrl = (def->ctrl & ~FLAG_LINEARISE) | ((flags & 0x03) ? FLAG_LINEARISE : 0);

#ifdef SIGNALCUBE
	base->ctrl = def->ctrl;
#endif

	if (mirror) {
		mirror_chan_eeconfig(def, 5);				/* Mirror chandef configuration data   */
		if (value) mirror_chan_eeconfig(def, 4);	/* Mirror chandef linearisation table  */
		if (filename) mirror_chan_eeconfig(def, 6);	/* Mirror chandef linearisation filename */
	}
#if (defined(CONTROLCUBE) || defined(AICUBE))
	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(uint32_t));
	if (value) {
		hpi_blockwrite((uint8_t*)def->mapptr, (uint8_t*)def->map, sizeof(float) * MAPSIZE);	/* Copy map to DSP B */
		if (flush) flush_map(def);														/* Flush cache area	 */
	}
#endif
}


/********************************************************************************
  FUNCTION NAME     : chan_ad_read_map
  FUNCTION DETAILS  : Read a linearisation map table entry for the selected
					  high-level ADC input channel.
********************************************************************************/

void chan_ad_read_map(chandef* def, uint32_t* flags, float* value, char* filename)
{
	if (flags) *flags = (def->cfg.i_ad.flags & FLAG_LINEARISE) >> BIT_LINEARISE;
	if (value) memcpy(value, def->map, sizeof(float) * MAPSIZE);			/* Copy map 	 */
	if (filename) memcpy(filename, def->filename, sizeof(def->filename));	/* Copy filename */
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_write_userstring
  FUNCTION DETAILS  : Write user string for the selected high-level ADC channel.

					  On entry:

					  string	Points to the start of the user string.
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_ad_write_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string, int mirror)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(&def->userstring[start], string, length);
	if (mirror) {
		if (string) mirror_chan_eeconfig(def, 7);	/* Mirror user string */
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_read_userstring
  FUNCTION DETAILS  : Read user string for the selected high_level ADC channel.
********************************************************************************/

void chan_ad_read_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(string, &def->userstring[start], length);
}



/*******************************************************************************
 FUNCTION NAME	   : chan_ad_set_peak_threshold
 FUNCTION DETAILS  : Set threshold parameter for peak detection.

					 On entry:

					 flags			Flags:
										Bit  30		Enable local channel threshold
					 threshold		Threshold value for peak detection

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_ad_set_peak_threshold(chandef* def, uint32_t flags, float threshold, int mirror)
{
	cfg_ad* cfg;

	cfg = &def->cfg.i_ad; /* AD specific configuration */

	cfg->flags = (cfg->flags & ~FLAG_CYCWINDOW) | ((flags & 0x40000000) ? FLAG_CYCWINDOW : 0);
	def->ctrl = (def->ctrl & ~FLAG_CYCWINDOW) | ((flags & 0x40000000) ? FLAG_CYCWINDOW : 0);
	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(uint32_t));
	def->cycwindow = threshold;
	update_shared_channel_parameter(def, offsetof(chandef, cycwindow), sizeof(float));
	if (mirror) {
		mirror_chan_eeconfig(def, 9);	/* Mirror cyclic window value */
		mirror_chan_eeconfig(def, 5);	/* Mirror cfg area		*/
	}
}



/*******************************************************************************
 FUNCTION NAME	   : chan_ad_read_peak_threshold
 FUNCTION DETAILS  : Read threshold parameter for peak detection.

					 On exit:

					 flags			Flags:
										Bit  30		Enable local channel threshold
					 threshold		Threshold value for peak detection

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_ad_read_peak_threshold(chandef* def, uint32_t* flags, float* threshold)
{
	if (flags) *flags = (def->ctrl & FLAG_CYCWINDOW) ? 0x40000000 : 0;
	if (threshold) *threshold = def->cycwindow;
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_write_faultmask
  FUNCTION DETAILS  : Write fault mask value for the selected high-level ADC
					  channel.

					  On entry:

					  mask		Mask flags.
									Bit 0	Input fault enable
									Bit 1	Reserved
									Bit 2	Reserved
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_ad_write_faultmask(chandef* def, uint32_t mask, int mirror)
{
	uint32_t m;
	int lp;		/* Local channel number on card */
	int slot;

	/* Modify the mask in the channel definition */

	def->cfg.i_ad.faultmask = mask;

	/* Get the slot number and the local channel number on the card */

	slot = EXTSLOTNUM;
	lp = EXTSLOTCHAN;

	/* Modify the mask in the slot table */

	m = slot_status[slot].faultmask;
	slot_status[slot].faultmask = m & ~(0x07 << (lp * 3)) | ((mask & 0x07) << (lp * 3));

	if (mirror) {
		mirror_chan_eeconfig(def, 5);	/* Mirror chandef configuration data     */
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_read_faultstate
  FUNCTION DETAILS  : Read fault state for the selected high-level ADC channel.
********************************************************************************/

uint32_t chan_ad_read_faultstate(chandef* def, uint32_t* capabilities, uint32_t* mask)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	uint32_t state;
	hw_mboard* base;

	/* Read the current fault state. Since all fault registers are at the same offset
	   we can use the hw_mboard structure as a generic means to access them.
	*/

	base = def->hw.i_mboard.hw;
	state = (uint32_t)base->u.rd.fault;

	/* The fault capability word depends on the board revision. Check if any fault
	   detection is available by testing bit 15 of the fault register.
	*/

	if (capabilities) *capabilities = (state & 0x8000) ? FAULT_INPUT : 0;
	if (mask) *mask = def->cfg.i_ad.faultmask;

	/* Extract the fault bits corresponding to the selected channel */

	return(((state & 0x7FFF) >> (EXTSLOTCHAN * 3)) & 0x07);

#else

	return(0);

#endif
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_dummy_write_faultmask
  FUNCTION DETAILS  : Dummy function used with 2DA4AD cards on firmware versions
					  that do not support fault detection on AD channels.

					  On entry:

					  mask		Mask flags.
									Bit 0	Input fault enable
									Bit 1	Reserved
									Bit 2	Reserved
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_ad_dummy_write_faultmask(chandef* def, uint32_t mask, int mirror)
{
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_dummy_read_faultstate
  FUNCTION DETAILS  : Dummy function used with 2DA4AD cards on firmware versions
					  that do not support fault detection on AD channels.
********************************************************************************/

uint32_t chan_ad_dummy_read_faultstate(chandef* def, uint32_t* capabilities, uint32_t* mask)
{
	if (capabilities) *capabilities = 0;
	if (mask) *mask = 0;
	return(0);
}
