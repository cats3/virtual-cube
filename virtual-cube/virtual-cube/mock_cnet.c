/********************************************************************************
 * MODULE NAME       : cnet.c													*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : CNet routines											*
 ********************************************************************************/

#define _GNU_SOURCE
#include <stdio.h>
#include <math.h>

#include <stdint.h>
#include "porting.h"

#include <fcntl.h>
#include <sys/stat.h>        /* For mode constants */
#include <sys/mman.h>
#include <unistd.h>

#include <mqueue.h>
#include <semaphore.h>
#include <errno.h>

#include <assert.h>

 /* The following two lines are required to allow Events.h to compile */

#define SUPGEN
#define Parameter void

#ifdef LOADER
#include "loader\loadercfg.h"
#else
//#include "ccubecfg.h"		// TODO: come back to this
#endif

#include "defines.h"
#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "eeprom.h"
//#include "hw_kinet.h"
#include "server.h"
#include "msgserver.h"
#include "msgstruct.h"
#include "debug.h"
#include "rtbuffer.h"
#include "hydraulic.h"
#include "eventlog.h"
//#include "HardwareIndependent/ControlCube.h"
#include "HardwareIndependent/Events.h"
//#include "HardwareIndependent/C3Std.h"
//#include "csl_irq.h"
#include "controller.h"
#include "eventrep.h"
//#include "mock_cnet.h"

void inchanproc(int start, int count);

#define EXTRA_CHANNELS (ExpNExp + C3AppNChans)	/* Number of virtual + command channels */

/* Define number of tracking cycles after tare offset adjustment */

#define TARE_OFFSET_TRACKING	2		

#if !defined(DEFFILEMODE)
#define DEFFILEMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)
#endif

/********************************************************************************
  General hardware register definitions
********************************************************************************/

static volatile uint8_t* const fpga_ctrl = (uint8_t*)0x90001000;	/* Mainboard FPGA control outputs			*/
static volatile uint16_t* const hc_encoder = (uint16_t*)0xB000303A;	/* Hand controller encoder count			*/

/********************************************************************************
  CNet hardware register definitions
********************************************************************************/
void* shared_cnet_memory;
struct mock_cnet_address_space* mock_cnet_controller;

// volatile uint32_t* cnet_mem_data;	/* Data memory								*/
volatile uint16_t* cnet_mem_ctrl;	/* Control memory							*/
//
///* Write registers */
//
//volatile uint16_t* const cnet_reg_ctrl = (uint16_t*)mock_cnet_controller.cnet_reg_ctrl; 	/* Control register							*/
//volatile uint16_t* const cnet_reg_comm = (uint16_t*)&mock_cnet_controller.cnet_reg_comm; 	/* Communications control register			*/
//volatile uint16_t* const cnet_reg_syncctrl = (uint16_t*)&mock_cnet_controller.cnet_reg_syncctrl; 	/* ADC/DAC sync output control register		*/
//volatile uint16_t* const cnet_reg_rstint = (uint16_t*)&mock_cnet_controller.cnet_reg_rstint; 	/* Reset CNet interrupt flag				*/
//volatile uint16_t* const cnet_reg_intctrl = (uint16_t*)&mock_cnet_controller.cnet_reg_intctrl; 	/* CNet interrupt control register			*/
//volatile uint16_t* const cnet_reg_expintctrl = (uint16_t*)&mock_cnet_controller.cnet_reg_expintctrl; 	/* CNet exp bus interrupt control register	*/
//volatile uint16_t* const cnet_reg_localid = (uint16_t*)&mock_cnet_controller.cnet_reg_localid; 	/* CNet local ID register					*/
//volatile uint16_t* const cnet_reg_generr = (uint16_t*)&mock_cnet_controller.cnet_reg_generr; 	/* Error generation control register		*/
//volatile uint16_t* const cnet_reg_logctrl = (uint16_t*)&mock_cnet_controller.cnet_reg_logctrl; 	/* Packet logging control register			*/
//volatile uint32_t* const cnet_reg_errdata = (uint32_t*)&mock_cnet_controller.cnet_reg_errdata; 	/* Error packet data register				*/
//
///* Read registers */
//
//volatile uint16_t* const cnet_reg_connstat = (uint16_t*)&mock_cnet_controller.cnet_reg_connstat;	/* Connection status flags					*/
//volatile uint16_t* const cnet_reg_position = (uint16_t*)&mock_cnet_controller.cnet_reg_position;	/* Ring position							*/
//volatile uint16_t* const cnet_reg_intstat = (uint16_t*)&mock_cnet_controller.cnet_reg_intstat;	/* Interrupt status flags					*/
//volatile uint16_t* const cnet_reg_chksum_ctr = (uint16_t*)&mock_cnet_controller.cnet_reg_chksum_ctr;	/* Checksum error counter					*/
//volatile uint16_t* const cnet_reg_err0 = (uint16_t*)&mock_cnet_controller.cnet_reg_err0; 	/* Error data register 0					*/
//volatile uint16_t* const cnet_reg_err1 = (uint16_t*)&mock_cnet_controller.cnet_reg_err1; 	/* Error data register 1					*/
//volatile uint16_t* const cnet_reg_err2 = (uint16_t*)&mock_cnet_controller.cnet_reg_err2; 	/* Error data register 2					*/
//volatile uint16_t* const cnet_reg_err3 = (uint16_t*)&mock_cnet_controller.cnet_reg_err3; 	/* Error data register 3					*/
//volatile uint16_t* const cnet_reg_errchk = (uint16_t*)&mock_cnet_controller.cnet_reg_errchk; 	/* Error checksum register 					*/
//volatile uint16_t* const cnet_reg_loga0 = (uint16_t*)&mock_cnet_controller.cnet_reg_loga0; 	/* Log bank A data register 0				*/
//volatile uint16_t* const cnet_reg_loga1 = (uint16_t*)&mock_cnet_controller.cnet_reg_loga1; 	/* Log bank A data register 1				*/
//volatile uint16_t* const cnet_reg_loga2 = (uint16_t*)&mock_cnet_controller.cnet_reg_loga2; 	/* Log bank A data register 2				*/
//volatile uint16_t* const cnet_reg_loga3 = (uint16_t*)&mock_cnet_controller.cnet_reg_loga3; 	/* Log bank A data register 3				*/
//volatile uint16_t* const cnet_reg_logchksum0 = (uint16_t*)&mock_cnet_controller.cnet_reg_logchksum0; 	/* Log checksum register 0					*/
//volatile uint16_t* const cnet_reg_logchksum1 = (uint16_t*)&mock_cnet_controller.cnet_reg_logchksum1; 	/* Log checksum register 1					*/
//volatile uint16_t* const cnet_reg_logchksum2 = (uint16_t*)&mock_cnet_controller.cnet_reg_logchksum2; 	/* Log checksum register 2					*/
//volatile uint16_t* const cnet_reg_logchksum3 = (uint16_t*)&mock_cnet_controller.cnet_reg_logchksum3; 	/* Log checksum register 3					*/
//volatile uint16_t* const cnet_reg_logb0 = (uint16_t*)&mock_cnet_controller.cnet_reg_logb0; 	/* Log bank B data register 0				*/
//volatile uint16_t* const cnet_reg_logb1 = (uint16_t*)&mock_cnet_controller.cnet_reg_logb1; 	/* Log bank B data register 1				*/
//volatile uint16_t* const cnet_reg_logb2 = (uint16_t*)&mock_cnet_controller.cnet_reg_logb2; 	/* Log bank B data register 2				*/
//volatile uint16_t* const cnet_reg_logb3 = (uint16_t*)&mock_cnet_controller.cnet_reg_logb3; 	/* Log bank B data register 3				*/

/* Flags in CNet CONNSTAT register */

#define CNET_OUTCONN			0x0001				/* CNet OUT port connected						*/
#define CNET_INCONN				0x0002				/* CNet IN port connected						*/
#define CNET_LOCK				0x0004				/* CNet PLL locked								*/
#define CNET_INTEGRITY			0x0008				/* CNet integrity good							*/
#define CNET_CHECKSUM			0x0010				/* CNet checksum failure						*/
#define CNET_LOS				0x0020				/* CNet loss of signal							*/

/* Flags in CNet CTRL register */

#define CNET_MASTER				0x0001				/* CNet master flag								*/
#define CNET_RESET				0x0002				/* CNet reset flag								*/
#define CNET_ENABLE				0x0010				/* CNet valid flag								*/

/* Flags in CNet communications control register */

#define CNET_COMMCTRL			0x0001				/* Enable CNet async comms ctrl slots			*/
#define CNET_COMMDATA			0x0003				/* Enable CNet async comms ctrl & data slots	*/

/* Flags in CNet SYNCCTRL register */

#define CNET_CLKEN				0x0100				/* CNet ADC/DAC clock output enable				*/


/* Define CNet transmit control types */

#define CNET_CTRL_NOP			0x0000				/* Do nothing									*/
#define CNET_CTRL_PASS			0x0100				/* Pass through data packet						*/
#define CNET_CTRL_TX			0x0200				/* Transmit standard data packet 				*/
#define CNET_CTRL_ORTX			0x0300				/* Transmit wired-or data packet 				*/
#define CNET_CTRL_COMMCTRLTX	0x0500				/* Transmit comms ctrl packet 					*/
#define CNET_CTRL_COMMCTRLORTX	0x0600				/* Transmit wired-or comms ctrl packet 			*/
#define CNET_CTRL_COMMDATATX	0x0D00				/* Transmit comms data packet 					*/
#define CNET_CTRL_COMMDATAORTX	0x0E00				/* Transmit wired-or comms data packet 			*/
#define CNET_CTRL_INTEGRITY		0x0F00				/* Transmit integrity packet					*/

/* Define fixed timeslot positions */

#define CNET_TIMESLOT_INTEGRITY	0					/* Link integrity								*/
#define CNET_TIMESLOT_TIMELO	1					/* Timestamp low word							*/
#define CNET_TIMESLOT_TIMEHI	2					/* Timestamp high word							*/
#define CNET_TIMESLOT_WATCHDOG	3					/* Watchdog										*/
#define CNET_TIMESLOT_ASYNCCTRL	4					/* Async comms control							*/
#define CNET_TIMESLOT_ASYNCDATA	5					/* Start of async comms block					*/

#define CNET_NUM_ASYNC_SLOTS	8					/* Number of slots used for async comms data	*/
#define CNET_ASYNC_TIMEOUT		8192				/* Default timeout period for async comms		*/
#define CNET_ASYNC_BUSY_RETRY	256					/* Maximum retries on bus busy					*/
#define CNET_ASYNC_MSG_RETRY	3					/* Maximum retries on async message				*/
#define CNET_WDOG_DELAY			4096				/* Count to detect valid CNet watchdog			*/
#define CNET_WDOG_DECAY			4					/* Decay rate for invalid CNet watchdog detect	*/

/* Define flags in link integrity packet data field */

#define CNET_LINKVALID 			0x0100				/* CNet link is valid							*/
#define CNET_IDCOMPLETE			0x0200				/* CNet system identification completed			*/

/* Define flags in async comms control packet */

#define BUSY					0x80000000			/* Comms busy									*/
#define INIT					0x40000000			/* Initial block of message						*/
#define FINAL					0x20000000			/* Final block of message						*/
#define REPLY					0x10000000			/* Message is a reply							*/

#define CNET_STABLE				32					/* Number of samples for stable CNet status 	*/

#define CNET_STABLE_MAX 		((2*CNET_STABLE)-1)	/* Max value of CNet status count 				*/

/* Macros return filtered state of CNet input and output connections */

#define CNET_IN_VALID			((cnet_inctr >= CNET_STABLE) ? 1 : 0)
#define CNET_OUT_VALID			((cnet_outctr >= CNET_STABLE) ? 1 : 0)

// extern SEM_Handle cnet_msg_sem;
static sem_t* cnet_msg_sem;
extern int hc_button_state;

/********************************************************************************
 Global variables
********************************************************************************/

uint32_t near cnet_time_lo = 0;					/* CNet timestamp low word						*/
uint32_t near cnet_time_hi = 0xDEADBEEF;			/* CNet timestamp high word						*/
uint32_t cnet_usr_maptable[256];					/* CNet user map table							*/
uint32_t cnet_sys_timeslot[400];					/* CNet system timeslot usage					*/
uint32_t cnet_usr_timeslot[400];					/* CNet user timeslot usage						*/
uint32_t near proc_usage = 0;						/* Processor usage in current interrupt			*/
uint32_t near max_proc_usage = 0;					/* Maximum processor usage						*/
uint32_t near cnet_local_mask = 0;					/* Mask for local watchdog status				*/
uint32_t near cnet_global_mask = 0;				/* Mask for global watchdog status				*/
uint32_t near cnet_local_wdog = 0;					/* Local watchdog status						*/
uint32_t near cnet_global_wdog = 0;				/* Global watchdog status						*/
static near uint32_t powerfail = FALSE;			/* Powerfail detected							*/
static near uint32_t powerfail_valid = TRUE;		/* Powerfail detection valid					*/

/********************************************************************************
 Global action control variables
********************************************************************************/

uint32_t near cnet_glbl_actreq;					/* Global action request						*/
uint32_t near cnet_glbl_actmask = 0;				/* Global action mask							*/

/********************************************************************************
 Global CNet control variables
********************************************************************************/

uint32_t cnet_copy_data[256];						/* Copy of CNet data memory						*/
float cnet_stop_rate[256];						/* CNet stop ramp rates							*/
float cnet_stop_target[256];					/* CNet stop target values						*/
uint32_t cnet_stop_time[256];						/* CNet stop transition time					*/
uint32_t cnet_stop_tcounter[256];					/* CNet stop time counter   					*/
uint32_t cnet_stop_type;							/* CNet stop type
													0 = Constant rate
													1 = Constant time							*/

uint8_t cnet_localid = 0;							/* CNet local ID 								*/
mqd_t cnet_async_txqueue;	/* POSIX message queue for outgoing CNet async messages */
char* cnet_txqueue_buffer;					/* pointer to malloc'd txqueue buffer */
//QUE_Obj cnet_async_txqueue;						/* Queue for outgoing CNet async messages		*/


/********************************************************************************
 CNet status variables
********************************************************************************/

uint32_t near cnet_retry_count = 0;				/* Number of async retries performed			*/
uint32_t near cnet_collision_count = 0;			/* Number of async collisions detected			*/
uint32_t near cnet_timeout = CNET_ASYNC_TIMEOUT;	/* Set async timeout period						*/
uint32_t near cnet_timeout_count = 0;				/* Number of async timeouts						*/
uint32_t near cnet_wdog_count = 0;					/* Number of watchdog errors detected			*/

static near uint32_t cnet_inctr = 0;				/* Counter for CNet input connection status		*/
static near uint32_t cnet_outctr = 0;				/* Counter for CNet output connection status	*/

/********************************************************************************
 Local CNet control variables
********************************************************************************/

cnetslot cnet_slottable[256];					/* Table for CNet slot allocation data 			*/
uint8_t  near cnet_numchan = 0;					/* Number of channels							*/
uint32_t near cnet_fault = 0;						/* CNet integrity faults detected				*/
uint32_t near cnet_chksum = 0;						/* CNet checksum failures detected				*/
uint32_t near cnet_integrity_valid = 0;			/* Count of valid integrity packets received	*/
static uint32_t near cnet_wdog_validctr = 0;		/* Count of valid watchdog packets received		*/
static uint32_t near cnet_wdog_valid = FALSE;		/* CNet watchdog valid							*/
uint32_t near cnet_mode = CNET_MODE_STANDALONE;	/* CNet operating mode							*/
uint32_t near cnet_position = 0;					/* Position in CNet ring						*/
uint32_t near cnet_ringsize = 0;					/* Number of devices in CNet ring				*/
uint32_t near cnet_configured = FALSE;				/* CNet configuration complete					*/
uint32_t near cnet_integrity = 0;					/* CNet integrity data							*/
uint32_t near cnet_local_addr = 0;					/* Local address for CNet async comms			*/
uint32_t near cnet_bus_addr = 0;					/* Bus acquisition address						*/

/********************************************************************************
 CNet debug variables
********************************************************************************/

float near cnet_debug_min = 0.0F;				/* Minimum value for log trigger				*/
float near cnet_debug_max = 0.0F;				/* Maximum value for log trigger				*/
uint32_t near cnet_debug_recorded = 0;				/* Recorded value for log trigger				*/
volatile uint32_t near cnet_debug_dataslot = 0;	/* Dataslot for trigger value					*/
uint16_t near cnet_debug_loga[4];					/* Logged packet data bank A					*/
uint16_t near cnet_debug_logb[4];					/* Logged packet data bank B					*/

/********************************************************************************
 Global realtime streaming control variables
********************************************************************************/

uint32_t near rt_glblreq_stop_flags = FLUSH_BOTH_BUFFERS | GENERATE_STOP;
/* Flags controlling global req stop			*/
uint32_t near rt_glblreq_pause_flags = GENERATE_PAUSE;
/* Flags controlling global req pause			*/
uint32_t near rt_control = 0;						/* Flags controlling RT operation				*/

#ifdef LOADER

/********************************************************************************
 Global loader watchdog flag
********************************************************************************/

uint32_t loader_watchdog_enable = FALSE;

#endif

/* Table holds mask values for all possible CNet ring sizes */

static uint32_t global_mask_table[32] = { 0x00000001,
									  0x00000003,
									  0x00000007,
									  0x0000000F,
									  0x0000001F,
									  0x0000003F,
									  0x0000007F,
									  0x000000FF,
									  0x000001FF,
									  0x000003FF,
									  0x000007FF,
									  0x00000FFF,
									  0x00001FFF,
									  0x00003FFF,
									  0x00007FFF,
									  0x0000FFFF,
									  0x0001FFFF,
									  0x0003FFFF,
									  0x0007FFFF,
									  0x000FFFFF,
									  0x001FFFFF,
									  0x003FFFFF,
									  0x007FFFFF,
									  0x00FFFFFF,
									  0x01FFFFFF,
									  0x03FFFFFF,
									  0x07FFFFFF,
									  0x0FFFFFFF,
									  0x1FFFFFFF,
									  0x3FFFFFFF,
									  0x7FFFFFFF,
									  0xFFFFFFFF
};



/********************************************************************************
  FUNCTION NAME     : cnet_initialise
  FUNCTION DETAILS  : Initialise the CNet hardware.
********************************************************************************/

void cnet_initialise(void)
{
	int n;

	//serial_transmit('a');
	int fd = open("/dev/zero", O_RDWR);
	if (fd < 0)
		diag_error(errno);

	/* allocate shared memory for cnet */
	mock_cnet_controller = (struct mock_cnet_address_space*) mmap(NULL, sizeof(struct mock_cnet_address_space), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (shared_cnet_memory < 0)
		diag_error(errno);
	close(fd);

	mock_cnet_controller->cnet_mem_data_in = mock_cnet_controller->cnet_mem_data_all;
	//mock_cnet_controller->cnet_mem_data_out = &mock_cnet_controller->cnet_mem_data_all[0];
	mock_cnet_controller->cnet_mem_data_out = &mock_cnet_controller->cnet_mem_data_all[256];

	//mock_cnet_controller->cnet_mem_data = mock_cnet_controller->cnet_mem_data_in;
	//cnet_mem_data = mock_cnet_controller->cnet_mem_data;
	cnet_mem_ctrl = mock_cnet_controller->cnet_mem_ctrl;

	/* Initialise slot table */

	for (n = 0; n < 256; n++) {
		cnet_slottable[n].chan = NULL;
		cnet_slottable[n].dirn = CNET_INPUT;
	}

	/* Initialise timeslot allocation map */

	memset(cnet_sys_timeslot, 0, sizeof(cnet_sys_timeslot));
	memset(cnet_usr_timeslot, 0, sizeof(cnet_usr_timeslot));
	memset(cnet_usr_maptable, 0, sizeof(cnet_usr_maptable));

	/* Initialise copy data */

	memset(cnet_copy_data, 0, sizeof(cnet_copy_data));

	//serial_transmit('b');

	mock_cnet_controller->cnet_reg_localid = 0;

	//serial_transmit('c');

	/* Initialise all control memory locations */

	for (n = 0; n < 512; n++) {
		mock_cnet_controller->cnet_mem_ctrl[n] = CNET_CTRL_NOP;
	}

	//serial_transmit('g');

	/* Prepare queue for CNet async comms */

	// TODO: is O_RDWR correct here?  What is the direction of the message queue?
	struct mq_attr cnet_async_txqueue_attr = {
		.mq_maxmsg = 10,	// maximum number of messages to queue
							// TODO: magic number 10 - see https://man7.org/linux/man-pages/man7/mq_overview.7.html
		.mq_msgsize = sizeof(cnet_msg),
	};

	cnet_async_txqueue = mq_open("/cnetasynctx", (O_RDWR | O_CREAT | O_NONBLOCK), DEFFILEMODE, &cnet_async_txqueue_attr);
	if (cnet_async_txqueue < 0)
	{
		perror(__FUNCTION__);
		fprintf(stderr, "Error creating cnet message queue (%d)\n", errno);
	}

	mq_getattr(cnet_async_txqueue, &cnet_async_txqueue_attr);
	if (cnet_async_txqueue_attr.mq_msgsize < sizeof(cnet_msg))
	{
		fprintf(stderr, "message queue size insufficient");
	}
	cnet_txqueue_buffer = malloc(cnet_async_txqueue_attr.mq_msgsize);

	// QUE_new(&cnet_async_txqueue);

	/* prepare a semaphore - semaphore names must match (see msgserver.c) */
	cnet_msg_sem = sem_open("/cnetsemmsg", O_CREAT | O_WRONLY, DEFFILEMODE, 0);
	if (cnet_msg_sem == SEM_FAILED)
	{
		fprintf(stderr, "Error creating cnet message semaphore (%d)\n", errno);
	}
	//serial_transmit('h');

	cnet_status_update(TRUE, CNET_MODE_STANDALONE);	/* Set initial CNet state */

	//serial_transmit('i');

	//LOG_START;

	//serial_transmit('j');

	/* Test if powerfail input is already asserted. If so, set the powerfail flag so
	   that the CNet interrupt handler will not flag this as a valid powerdown and
	   make an incorrect entry in the event log. Also clear the powerfail_valid
	   flag so that routines that check the powerfail state can ignore it.
	*/

	//if (!((*fpga_ctrl) & IP_POWERFAIL)) {
	//	powerfail = TRUE;
	//	powerfail_valid = FALSE;		/* Powerfail detection invalid */
	//}

	/* Enable interrupts from CNet */

	mock_cnet_controller->cnet_reg_intctrl = CNET_INTEN;

	// SJE - force PLL lock
	mock_cnet_controller->cnet_reg_connstat |= CNET_LOCK;

	//serial_transmit('k');
}



/********************************************************************************
  FUNCTION NAME     : cnet_int_ctrl
  FUNCTION DETAILS  : Control the CNet interrupt flags.
********************************************************************************/

uint32_t cnet_int_ctrl(uint32_t set, uint32_t clr)
{
	uint32_t state;

	state = mock_cnet_controller->cnet_reg_intctrl;
	state = (state & ~clr) | set;
	mock_cnet_controller->cnet_reg_intctrl = state;
	return(state);
}



near uint32_t cnet_interrupt_ctr = 0;
near uint32_t debug_rt_txfr_count = 0;	/* Debug */

#define PCR1	0x01900024	/* McBSP1 Pin Control Register */

/********************************************************************************
  FUNCTION NAME     : cnet_interrupt
  FUNCTION DETAILS  : CNet interrupt handler.
********************************************************************************/

void cnet_interrupt(void)
{
	static near uint32_t time_lo = 0;
	static near uint32_t time_hi = 0x55AA55AA;
	register uint32_t connstat;
#ifndef LOADER
	uint32_t rt_flags = FALSE;
	connection* c;
	//uint32_t flags = 0;
	int n;
#endif
	uint32_t proc_starttime;
	uint32_t proc_duration;
	uint32_t wdog;
#if (defined(CONTROLCUBE) || defined(AICUBE))
	static near uint32_t cnet_hc_count = 0;	/* Local hand controller encoder count value */
	uint16_t hc_ctr;
	int16_t hc_delta;
	uint32_t hc_broadcast;
#endif

	if (cnet_debug_dataslot) {
		uint32_t slot = cnet_debug_dataslot & 0xFF;
		uint32_t val = mock_cnet_controller->cnet_mem_data_in[slot];
		float f = (float)val; //_itof(val);
		if ((f < cnet_debug_min) || (f > cnet_debug_max)) {
			memcpy(cnet_debug_loga, &(mock_cnet_controller->cnet_reg_loga0), 8);
			memcpy(cnet_debug_logb, &(mock_cnet_controller->cnet_reg_logb0), 8);
			cnet_debug_recorded = val;
			cnet_debug_dataslot = 0;
		}
	}


	cnet_interrupt_ctr++;

#ifdef CONTROLCUBE

	/* Update hydraulic relay SPI output state */

	dsp_spi_update();

#endif

#ifndef LOADER
#if (defined(CONTROLCUBE) || defined(AICUBE))

	/* Update digital i/p CNet states */

	for (n = 0; n < 4; n++) {
		slot_info* s = &slot_status[n];
		if (s->digip_cnetptr) *s->digip_cnetptr = (int)((*s->digip_srcptr ^ s->digip_invert) & s->digip_mask);
	}

#endif /* CONTROLCUBE || AICUBE */
#endif /* LOADER */

	/* Update checksum error counter */

	cnet_chksum += mock_cnet_controller->cnet_reg_chksum_ctr;

	/* Get connection status */

	connstat = mock_cnet_controller->cnet_reg_connstat;

	if (connstat & CNET_INTEGRITY) {
		if (cnet_integrity_valid < 4096) cnet_integrity_valid++;
		wdog = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_WATCHDOG];
	}
	else {
		cnet_fault++;
		wdog = 0;

		/* No valid integrity packet, so decrement counter */

		if (cnet_integrity_valid < 16)
			cnet_integrity_valid = 0;
		else
			cnet_integrity_valid -= 16;
	}

#ifndef LOADER
#if (defined(CONTROLCUBE) || defined(AICUBE))

	/* Process hand controller encoder count and generate CNet slot data made up from
	   encoder delta value, enable switch state and test/setup switch state.
	*/

	//TODO: handle hand controller encoder properly
	//hc_ctr = *hc_encoder;
	hc_ctr = 0;
	hc_delta = hc_ctr - cnet_hc_count;
	hc_broadcast = (hc_delta & 0xFFFF) |
		(((hc_button_state & 0x0F) == 0x07) ? 0x80000000 : 0) |
		((hc_button_state & 0x10) ? 0x40000000 : 0);
	cnet_hc_count = hc_ctr;

#endif /* CONTROLCUBE || AICUBE */
#endif /* LOADER */

	/* Master CNet must perform the following operations:

		 Broadcast integrity packets for all other systems
		 Generate and broadcast the timestamp counter
		 Output hand controller encoder delta values
	*/

	if ((cnet_mode == CNET_MODE_MASTER) || (cnet_mode == CNET_MODE_STANDALONE)) {
		mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_INTEGRITY] = cnet_integrity;
		if (++time_lo == 0) time_hi++;

		mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_TIMELO] = time_lo;
		mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_TIMEHI] = time_hi;
#if (defined(CONTROLCUBE) || defined(AICUBE))
#ifndef LOADER
		mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ENCODER] = hc_broadcast;
#endif /* LOADER */
#endif /* CONTROLCUBE */
	}
	else {
		time_lo = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_TIMELO];
		time_hi = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_TIMEHI];
	}

#if (defined(CONTROLCUBE) || defined(AICUBE))
#ifndef LOADER
	// No KiNet - removed not necessary in virtual cube
	rt_flags = FALSE;

	global_hyd_request_in = (cnet_mode != CNET_MODE_STANDALONE) ? mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_HYDREQ] : 0;
	mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_HYDREQ] = global_hyd_request_out;

#endif
#endif

	/* Update global timestamp value */

	cnet_time_lo = time_lo;
	cnet_time_hi = time_hi;

	/* Process CNet watchdog */

	//wdog = cnet_mem_data[CNET_DATASLOT_WATCHDOG];

	if (cnet_mode != CNET_MODE_STANDALONE) {

		/* CNet active, so test watchdog operation */

		if (time_lo & 1) {
			cnet_local_wdog ^= cnet_local_mask;
			if ((cnet_global_wdog ^ cnet_global_mask) == wdog) {
				if (cnet_wdog_validctr < CNET_WDOG_DELAY) cnet_wdog_validctr++;
			}
			else {
				cnet_wdog_count++;
				cnet_wdog_validctr = (cnet_wdog_validctr < CNET_WDOG_DECAY) ? 0 : (cnet_wdog_validctr - CNET_WDOG_DECAY);
			}
			if (cnet_wdog_valid) {
				if (cnet_wdog_validctr < CNET_WDOG_DECAY) {
#ifndef LOADER
					eventlog_entry log;

					cnet_wdog_valid = FALSE;

					log.type = LogTypeCNetWdogFail;
					log.parameter1 = cnet_position;
					log.parameter2 = 0;
					log.timestamp_hi = cnet_time_hi;
					log.timestamp_lo = cnet_time_lo;
					eventlog_write(&log);

#if (defined(CONTROLCUBE) || defined(AICUBE))

					/* No TSR operation can be guaranteed if the CNet has died, so the only sensible action
					   is to dump the hydraulics. This will only operate on the local controller, but other
					   CNet devices should have seen the same watchdog failure and will be able to take
					   their own action.
					*/

					hyd_unload();

#endif

#endif
				}
			}
			else {
				if (cnet_wdog_validctr >= CNET_WDOG_DELAY) {
					cnet_wdog_valid = TRUE;
				}
			}
			cnet_global_wdog = wdog; /* Re-sync watchdog */
		}
	}
	else {

		/* Standalone CNet mode, so ignore watchdog and force valid state */

		cnet_wdog_valid = TRUE;
	}

	mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_WATCHDOG] = cnet_local_wdog;

#ifndef LOADER
#if (defined(CONTROLCUBE) || defined(AICUBE))

	/* Update the CNet hydraulic status and group status */

	mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_HYDSTATUS] = global_hyd_status_out;
	mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_GRPSTATUS] = global_grp_status_out;

	//rt_control |= flags;

#endif /* CONTROLCUBE || AICUBE */

	/* Output various status and flags on TSR */

	mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_FLAGS] = ((rtmsg_list->stream->enable & RT_OUTPUT) ? 0x40000000 : 0x00000000) | (CtrlGetCubicExecutiveFlags() & 0x3FFFFFFF);
	mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_GENSTAT] = CtrlGetGenStatus();

	/* Process data streaming on the dedicated realtime message connection */

	debug_rt_txfr_count++;
	rt_transfer(rtmsg_list->stream, rt_flags, &rt_control);

	/* Process data streaming on the standard message connections */

	c = msg_list;
	while (c) {
		if (c->stream) rt_transfer(c->stream, rt_flags, NULL);
		c = c->next;
	}

#if (defined(CONTROLCUBE) || defined(AICUBE))

	// TODO: handle power fail properly
	if(0){
	//if ((!((*fpga_ctrl) & IP_POWERFAIL)) && (!powerfail)) {
		eventlog_entry log;
		log.type = LogTypePowerdown;
		log.parameter1 = 0;
		log.parameter2 = 0;
		log.timestamp_hi = cnet_time_hi;
		log.timestamp_lo = cnet_time_lo;
		if (!eventlog_write(&log)) powerfail = TRUE;
	}

	/* Perform any updates required by Cubic Engine */

	CubicEngineIter();

#endif /* CONTROLCUBE || AICUBE */

#endif /* LOADER */

	mock_cnet_controller->cnet_reg_rstint = 0;

	cnet_async_handler(connstat);	/* Process CNet async comms */
	dsp_wdoga_state(1);				/* Drive watchdog			*/

	/* Update processor time usage */

	//proc_duration = TIMER_getCount(hTimer1) - proc_starttime;
	//proc_usage = proc_duration;
	//if (proc_duration > max_proc_usage) max_proc_usage = proc_duration;
}



/********************************************************************************
  FUNCTION NAME     : cnet_read_mode
  FUNCTION DETAILS  : Read CNet mode.
********************************************************************************/

void cnet_read_mode(uint32_t* enable, uint32_t* master, uint32_t* localid)
{
	*enable = ((mock_cnet_controller->cnet_reg_ctrl) & CNET_ENABLE) ? 1 : 0;
	*master = ((mock_cnet_controller->cnet_reg_ctrl) & CNET_MASTER) ? 1 : 0;
	*localid = (mock_cnet_controller->cnet_reg_localid) & 0x1F;
}



/********************************************************************************
  FUNCTION NAME     : cnet_setsync
  FUNCTION DETAILS  : Define the operation of the ADC/DAC sync clock outputs.
********************************************************************************/

void cnet_setsync(int enable, int count)
{
	mock_cnet_controller->cnet_reg_syncctrl = ((enable) ? CNET_CLKEN : 0) | (count & 0xFF);
}



/********************************************************************************
  FUNCTION NAME     : cnet_rdstat
  FUNCTION DETAILS  : Read the current CNet state.
********************************************************************************/

void cnet_rdstat(uint32_t* status, uint32_t* in, uint32_t* out, uint32_t* lock, uint32_t* integrity, uint32_t* fault,
	uint32_t* chksum, uint32_t* wdog, uint32_t* ts_lo, uint32_t* ts_hi, uint32_t* mode, uint32_t* position,
	uint32_t* ringsize)
{
	uint32_t connstat;

	connstat = mock_cnet_controller->cnet_reg_connstat;

	if (status) {
		*status = ((connstat & CNET_INCONN) ? CNET_STAT_INCONN : 0) |
			((connstat & CNET_OUTCONN) ? CNET_STAT_OUTCONN : 0) |
			(((connstat & (CNET_LOCK | CNET_LOS)) == CNET_LOCK) ? CNET_STAT_LOCK : 0) |
			(((connstat & (CNET_INTEGRITY | CNET_LOS)) == CNET_INTEGRITY) ? CNET_STAT_INTEGRITY : 0);
	}
	if (in)  	   *in = CNET_IN_VALID;
	if (out) 	   *out = CNET_OUT_VALID;
	if (lock) 	   *lock = ((connstat & (CNET_LOCK | CNET_LOS)) == CNET_LOCK) ? 1 : 0;
	if (integrity) *integrity = ((connstat & (CNET_INTEGRITY | CNET_LOS)) == CNET_INTEGRITY) ? 1 : 0;
	if (fault) 	   *fault = cnet_fault;
	if (chksum)    *chksum = cnet_chksum;
	if (wdog)	   *wdog = cnet_wdog_valid;
	if (mode)	   *mode = cnet_mode;
	if (position)  *position = cnet_position;
	if (ringsize)  *ringsize = cnet_ringsize;
	if (ts_lo)	   *ts_lo = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_TIMELO];
	if (ts_hi)	   *ts_hi = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_TIMEHI];
}



/********************************************************************************
  FUNCTION NAME     : cnet_rderr
  FUNCTION DETAILS  : Read the current error register state.
********************************************************************************/

void cnet_rderr(uint8_t* data, uint32_t* chksum)
{
	uint32_t word0 = mock_cnet_controller->cnet_reg_err0;
	uint32_t word1 = mock_cnet_controller->cnet_reg_err2;

	data[0] = word1 >> 24;
	data[1] = word1 >> 16;
	data[2] = word1 >> 8;
	data[3] = word1;
	data[4] = word0 >> 24;
	data[5] = word0 >> 16;
	data[6] = word0 >> 8;
	data[7] = word0;

	*chksum = mock_cnet_controller->cnet_reg_errchk & 0xFF;	/* Read calculated checksum		*/
}



/********************************************************************************
  FUNCTION NAME     : cnet_generr
  FUNCTION DETAILS  : Generate a CNet error for diagnostic use.
********************************************************************************/

void cnet_generr(uint32_t slot, uint32_t flags, uint32_t data)
{
	uint32_t reg = ((flags & 0x03) << 14) | (slot & 0xFF);

	mock_cnet_controller->cnet_reg_errdata = data;	/* Set error packet data */
	mock_cnet_controller->cnet_reg_generr = reg;		/* Generate error state	 */
}



/********************************************************************************
  FUNCTION NAME     : cnet_logpkt
  FUNCTION DETAILS  : Log a CNet packet for diagnostic use.
********************************************************************************/

void cnet_logpkt(uint32_t slot, uint8_t* data, uint8_t* chksum)
{
	mock_cnet_controller->cnet_reg_logctrl = 0x8000 | slot;	/* Request packet logging */
	while ((mock_cnet_controller->cnet_reg_logctrl) & 0x8000);
	data[0] = (mock_cnet_controller->cnet_reg_loga3 >> 8) & 0xFF;
	data[1] = mock_cnet_controller->cnet_reg_loga3 & 0xFF;
	data[2] = (mock_cnet_controller->cnet_reg_loga2 >> 8) & 0xFF;
	data[3] = mock_cnet_controller->cnet_reg_loga2 & 0xFF;
	data[4] = (mock_cnet_controller->cnet_reg_loga1 >> 8) & 0xFF;
	data[5] = mock_cnet_controller->cnet_reg_loga1 & 0xFF;
	data[6] = (mock_cnet_controller->cnet_reg_loga0 >> 8) & 0xFF;
	data[7] = mock_cnet_controller->cnet_reg_loga0 & 0xFF;

	chksum[0] = mock_cnet_controller->cnet_reg_logchksum0 & 0xFF;
	chksum[1] = (mock_cnet_controller->cnet_reg_logchksum0 >> 8) & 0xFF;
	chksum[2] = mock_cnet_controller->cnet_reg_logchksum1 & 0xFF;
	chksum[3] = (mock_cnet_controller->cnet_reg_logchksum1 >> 8) & 0xFF;
	chksum[4] = mock_cnet_controller->cnet_reg_logchksum2 & 0xFF;
	chksum[5] = (mock_cnet_controller->cnet_reg_logchksum2 >> 8) & 0xFF;
	chksum[6] = mock_cnet_controller->cnet_reg_logchksum3 & 0xFF;
	chksum[7] = (mock_cnet_controller->cnet_reg_logchksum3 >> 8) & 0xFF;
}



/********************************************************************************
  FUNCTION NAME     : cnet_debug
  FUNCTION DETAILS  : Debug CNet behaviour.
********************************************************************************/

void cnet_debug(uint32_t timeslot, uint32_t dataslot, float min, float max, uint8_t* data_a, uint8_t* data_b, uint32_t* recorded)
{
	uint16_t loga0, loga1, loga2, loga3;
	uint16_t logb0, logb1, logb2, logb3;

	cnet_debug_dataslot = 0;
	cnet_debug_min = min;
	cnet_debug_max = max;

	mock_cnet_controller->cnet_reg_logctrl = 0x8000 | timeslot;	/* Request packet logging */

	cnet_debug_dataslot = 0x8000 | dataslot;

	/* Wait for log to trigger */

	while (cnet_debug_dataslot);

	loga0 = mock_cnet_controller->cnet_reg_loga0;
	loga1 = mock_cnet_controller->cnet_reg_loga1;
	loga2 = mock_cnet_controller->cnet_reg_loga2;
	loga3 = mock_cnet_controller->cnet_reg_loga3;

	logb0 = mock_cnet_controller->cnet_reg_logb0;
	logb1 = mock_cnet_controller->cnet_reg_logb1;
	logb2 = mock_cnet_controller->cnet_reg_logb2;
	logb3 = mock_cnet_controller->cnet_reg_logb3;

	data_a[0] = (loga3 >> 8) & 0xFF;
	data_a[1] = loga3 & 0xFF;
	data_a[2] = (loga2 >> 8) & 0xFF;
	data_a[3] = loga2 & 0xFF;
	data_a[4] = (loga1 >> 8) & 0xFF;
	data_a[5] = loga1 & 0xFF;
	data_a[6] = (loga0 >> 8) & 0xFF;
	data_a[7] = loga0 & 0xFF;

	data_b[0] = (logb3 >> 8) & 0xFF;
	data_b[1] = logb3 & 0xFF;
	data_b[2] = (logb2 >> 8) & 0xFF;
	data_b[3] = logb2 & 0xFF;
	data_b[4] = (logb1 >> 8) & 0xFF;
	data_b[5] = logb1 & 0xFF;
	data_b[6] = (logb0 >> 8) & 0xFF;
	data_b[7] = logb0 & 0xFF;

	*recorded = cnet_debug_recorded;
}



/********************************************************************************
  FUNCTION NAME     : cnet_rdwdog
  FUNCTION DETAILS  : Read the current CNet watchdog state.
********************************************************************************/

uint32_t cnet_rdwdog(void)
{
	return(cnet_wdog_valid);
}



/********************************************************************************
  FUNCTION NAME     : cnet_get_info
  FUNCTION DETAILS  : Read CNet info.
********************************************************************************/

void cnet_get_info(uint32_t* ringsize, uint32_t* position)
{
	if (ringsize) *ringsize = (cnet_ringsize) ? cnet_ringsize : 1;
	if (position) *position = cnet_position;
}



/********************************************************************************
  FUNCTION NAME     : cnet_get_slotaddr
  FUNCTION DETAILS  : Returns the memory address of the requested CNet slot.

					  On entry:

					  slot	CNet slot number.

					  proc	Determines which processor the memory address will
							be used with. This is necessary because of
							differences in addresses between DSPA and DSPB on
							Control Cube hardware revision A.

							PROC_DSPA 	No adjustment is performed to the slot
										address.

							PROC_DSPB	The address is corrected if required
										for DSPB usage (revision A units only).

							Note proc setting is only checked on Control Cube
							firmware.
********************************************************************************/

int* cnet_get_slotaddr(uint32_t slot, uint32_t proc)
{
	if (slot & 0x8000000) return(NULL);
//#if (defined(CONTROLCUBE) || defined(AICUBE))
//	if ((proc == PROC_DSPB) && (get_hw_version(NULL, NULL) < 2))
//		return((int*)(((int)&cnet_mem_data[slot & 0xFF]) & 0x9FFFFFFF));
//#endif
	// no need for processor fixup
	return((int*)&mock_cnet_controller->cnet_mem_data_in[slot & 0xFF]);
}


int* cnet_get_slotaddr_out(uint32_t slot, uint32_t proc)
{
	if (slot & 0x8000000) return(NULL);
	return((int*)&mock_cnet_controller->cnet_mem_data_out[slot & 0xFF]);
}


/********************************************************************************
  FUNCTION NAME     : cnet_get_slotdata
  FUNCTION DETAILS  : Returns the data value from the requested CNet slot.
********************************************************************************/

float cnet_get_slotdata(uint32_t slot)
{
	return(*((float*)&mock_cnet_controller->cnet_mem_data_in[slot & 0xFF]));
}



/********************************************************************************
  FUNCTION NAME     : cnet_read_list
  FUNCTION DETAILS  : Read CNet output list.
********************************************************************************/

void cnet_read_list(int* size, int* list)
{
}



/********************************************************************************
  FUNCTION NAME     : cnet_set_timeout
  FUNCTION DETAILS  : Sets the timeout period for CNet asynchronous
					  communications.
********************************************************************************/

void cnet_set_timeout(uint32_t timeout)
{
	cnet_timeout = timeout;
}



/********************************************************************************
  FUNCTION NAME     : cnet_read_timeout
  FUNCTION DETAILS  : Reads the timeout period for CNet asynchronous
					  communications.
********************************************************************************/

uint32_t cnet_read_timeout(void)
{
	return(cnet_timeout);
}



static const uint32_t cnet_state_table[4] = { CNET_MODE_STANDALONE,	/* Out = FALSE	In = FALSE 	*/
										  CNET_MODE_SLAVE,		/* Out = FALSE	In = TRUE	*/
										  CNET_MODE_MASTER,		/* Out = TRUE	In = FALSE	*/
										  CNET_MODE_SLAVE		/* Out = TRUE	In = TRUE	*/
};

/********************************************************************************
  FUNCTION NAME     : cnet_mark_sys_slot
  FUNCTION DETAILS  : Mark cnet system timeslot as used.

					  This routine makes the necessary entries in the CNet
					  system timeslot map as a timeslot is allocated.
********************************************************************************/

uint32_t cnet_mark_sys_slot(uint32_t src, uint32_t timeslot, uint32_t type)
{
	uint32_t cnet_position;
	uint32_t cnet_ringsize;
	int s;
	int c;

	cnet_get_info(&cnet_ringsize, &cnet_position);

	/* Mark timeslot in source controller */

	cnet_sys_timeslot[timeslot] |= (1 << src);

	/* For all other devices, mark appropriate slots */

	s = (src + 1) % cnet_ringsize;
	timeslot += 3;
	for (c = 1; c < cnet_ringsize; c++) {
		cnet_sys_timeslot[timeslot] |= (1 << s);
		s = (s + 1) % cnet_ringsize;
		timeslot += 3;
	}

	if (type) {

		/* For wired-or slots, the data will make a second pass around the ring, so
		   additional timeslots must be marked. The last device in the ring does not
		   need to pass on the packet, so the timeslot remains free.
		*/

		cnet_sys_timeslot[timeslot] |= (1 << src);
		timeslot += 3;
		s = (src + 1) % cnet_ringsize;
		for (c = 1; c < cnet_ringsize; c++) {
			if ((c + 1) != cnet_ringsize) cnet_sys_timeslot[timeslot] |= (1 << s);
			s = (s + 1) % cnet_ringsize;
			timeslot += 3;
		}
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : cnet_find_sys_slot
  FUNCTION DETAILS  : Find free CNet system timeslot.

					  This routine checks the CNet system timeslot map to locate
					  a valid timeslot allocation.

					  On entry

					  timeslot	Indicates the minimum timeslot number to be
								used.

					  type		Indicates if the data type is wired-or, which
								requires two passes around the CNet ring.
********************************************************************************/

uint32_t cnet_find_sys_slot(uint32_t src, uint32_t timeslot, uint32_t type, uint32_t* slot)
{
	uint32_t cnet_position;
	uint32_t cnet_ringsize;
	int s;
	int c;
	int m;
	int n;

	cnet_get_info(&cnet_ringsize, &cnet_position);

	m = timeslot;

	for (m = timeslot; m < 399; m++) {

		/* Test for free slot in source controller */

		if (cnet_sys_timeslot[m] & (1 << src)) continue;

		/* Check if valid in all other controllers */

		s = (src + 1) % cnet_ringsize;
		n = m + 3;
		if (n > 399) return(NO_SLOT); /* No more timeslots, so generate error */
		for (c = 1; c < cnet_ringsize; c++) {
			if ((cnet_sys_timeslot[n]) & (1 << s)) continue; /* In use, so try next slot */
			s = (s + 1) % cnet_ringsize;
			n += 3;
			if (n > 399) return(NO_SLOT); /* No more timeslots, so generate error */
		}

		/* For wired-or slots, the data will make a second pass around the ring. */

		if (type) {

			/* Test for free slot in source controller */

			if (cnet_sys_timeslot[n] & (1 << src)) continue;
			n += 3;
			if (n > 399) return(NO_SLOT); /* No more timeslots, so generate error */
			s = (src + 1) % cnet_ringsize;
			for (c = 1; c < cnet_ringsize; c++) {
				if ((cnet_sys_timeslot[n]) & (1 << s)) continue; /* In use, so try next slot */
				s = (s + 1) % cnet_ringsize;
				n += 3;
				if (n > 399) return(NO_SLOT); /* No more timeslots, so generate error */
			}
		}

		/* Valid timeslot located, return timeslot number */

		if (slot) *slot = m;
		return(NO_ERROR);
	}
	return(NO_SLOT);
}



#ifndef LOADER

/********************************************************************************
  FUNCTION NAME     : cnet_allocate_sys_slot
  FUNCTION DETAILS  : Find free CNet system timeslot.

					  This routine allocates a valid timeslot for the selected
					  dataslot.

					  On entry

					  timeslot	Indicates the minimum timeslot number to be
								used.

					  type		Indicates if the data type is wired-or, which
								requires two passes around the CNet ring.

					  slottype	Indicates the packet type.

								For standard data slots:
								  Master CNET_CTRL_TX
								  Slave  CNET_CTRL_PASS

								For wired-or data slots:
								  Master CNET_CTRL_TX
								  Slave  CNET_CTRL_ORTX

********************************************************************************/

uint32_t cnet_allocate_sys_slot(uint32_t master, uint32_t src, uint32_t timeslot, uint32_t dataslot,
	uint32_t slottype, uint32_t* slot)
{
	uint32_t position;
	uint32_t ringsize;
	int last;
	int pass1;
	int pass2;
	uint32_t type;
	uint32_t type1;
	uint32_t type2;
	uint32_t s;

	type = (slottype == CNET_CTRL_ORTX) ? TRUE : FALSE;

	/* Find a suitable timeslot position, return if not available */

	if (cnet_find_sys_slot(src, timeslot, type, &s) == NO_SLOT) return(NO_SLOT);

	/* Get CNet ring size */

	cnet_get_info(&ringsize, &position);

	/* Calculate the timeslot offsets for pass1 and optional pass2 packets */

	pass1 = (master) ? 0 : (position * 3);
	pass2 = pass1 + (ringsize * 3);

	/* Check if this device is the last one in the ring */

	last = ((position + 1) == ringsize) ? TRUE : FALSE;

	/* Update control settings for pass1 and optional pass2 timeslots */

	if (slottype == CNET_CTRL_ORTX) {

		/* Master must use TX instead of ORTX for 1st pass */

		type1 = (master) ? CNET_CTRL_TX : slottype;

		/* Second pass is always PASS for master, PASS or NOP for slave depending on ring position */

		type2 = (master) ? CNET_CTRL_PASS : ((last) ? CNET_CTRL_NOP : CNET_CTRL_PASS);

		mock_cnet_controller->cnet_mem_ctrl[s + pass1] = type1 | dataslot;
		mock_cnet_controller->cnet_mem_ctrl[s + pass2] = type2 | dataslot;
	}
	else {

		/* Slave must use PASS */

		mock_cnet_controller->cnet_mem_ctrl[s + pass1] = ((master) ? slottype : CNET_CTRL_PASS) | dataslot;

	}

	mock_cnet_controller->cnet_mem_data_out[s] = 0;
	cnet_mark_sys_slot(src, s, type);

	if (slot) *slot = s;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : cnet_config_map
  FUNCTION DETAILS  : Load a CNet configuration map.

					  Each entry in the CNet map array is configured as
					  follows:

					  Bit 31		Indicates map entry is used
					  Bit 30		Indicates slot is an output
					  Bit 29		Do not map to hardware
					  Bits 21-25	Destination CNet address (output slots only)
					  Bits 16-20	Source CNet address
					  Bits 0-15		Local channel mapped to CNet slot

					  When bit 30 is set, then the channel mapping is used for
					  the destination.

********************************************************************************/

uint32_t cnet_config_map(uint32_t* map)
{
	chandef* cp;
	uint32_t cnet_position;
	uint32_t cnet_ringsize;
	int slot;
	int src;
	int dst;
	int chan;
	int m;
	int s;
	int n;
	int c;

	/* Unmap any digital input slots that may have been used previously */

	for (n = 0; n < 4; n++) {
		slot_status[n].digip_cnetptr = NULL;
	}

	/* Copy map into system map */

	memcpy(cnet_usr_maptable, map, sizeof(cnet_usr_maptable));

	/* Get CNet ring size */

	cnet_get_info(&cnet_ringsize, &cnet_position);

	/* Initialise the user timeslot table */

	memset(cnet_usr_timeslot, 0, sizeof(cnet_usr_timeslot));

	for (slot = 0; slot < CNET_SYSTEM_BASE; slot++) {

		if (!(map[slot] & 0x80000000)) continue;	/* Unused map entry */

		src = (map[slot] >> 16) & 0x1F;
		dst = (map[slot] >> 21) & 0x1F;
		chan = map[slot] & 0xFFFF;

		for (m = 0; m < 400; m++) {

			/* Test for free slot in source controller */

			if ((cnet_sys_timeslot[m] | cnet_usr_timeslot[m]) & (1 << src)) continue;

			/* Check if valid in all other controllers */

			s = (src + 1) % cnet_ringsize;
			n = m + 3;
			for (c = 1; c < cnet_ringsize; c++) {
				if ((cnet_sys_timeslot[n] | cnet_usr_timeslot[n]) & (1 << s)) goto trynext; /* In use, so try next slot */
				s = (s + 1) % cnet_ringsize;
				n += 3;
				if (n > 399) return(NO_SLOT); /* No more timeslots, so generate error */
			}

			/* Valid allocation located, update allocation table */

			cnet_usr_timeslot[m] |= (1 << src);

			/* If NOMAP flag is clear, map either source or destination channel to CNet slot */

			if (!(map[slot] & 0x20000000)) {
				if (map[slot] & 0x40000000) {
					if (cnet_position == dst) {
						cp = chanptr(chan);
						if (cp) cp->set_cnet_slot(cp, slot); /* Map channel to CNet slot */
					}
				}
				else if (cnet_position == src) {
					cp = chanptr(chan);
					if (cp) cp->set_cnet_slot(cp, slot); /* Map channel to CNet slot */
				}
			}

			/* If this controller is the source for the data, then configure the CNet timeslot
			   as a transmit slot.
			*/

			if (cnet_position == src) {
				mock_cnet_controller->cnet_mem_ctrl[m] = CNET_CTRL_TX | slot;
			}

			/* For all other slots, the CNet hardware must be configured to pass through */

			s = (src + 1) % cnet_ringsize;
			n = m + 3;
			for (c = 1; c < cnet_ringsize; c++) {
				cnet_usr_timeslot[n] |= (1 << s);
				if (cnet_position == s) mock_cnet_controller->cnet_mem_ctrl[n] = CNET_CTRL_PASS | slot;
				s = (s + 1) % cnet_ringsize;
				n += 3;
			}
			break;

		trynext:
			{}
		}

	}

	/* Scan through all slots and configure any unused slots to NOPs */

	for (m = 0; m < 400; m++) {
		if (!((cnet_sys_timeslot[m] | cnet_usr_timeslot[m]) & (1 << cnet_position))) {
			mock_cnet_controller->cnet_mem_ctrl[m] = CNET_CTRL_NOP;
		}
	}

	return(NO_ERROR);
}

#endif /* LOADER */



/********************************************************************************
  FUNCTION NAME     : cnet_init_sys_slots
  FUNCTION DETAILS  : Initialise CNet system slot allocations.
********************************************************************************/

void cnet_init_sys_slots(uint32_t master, uint32_t position, uint32_t ringsize)
{

	/* Initialise timeslot allocation map */

	memset(cnet_sys_timeslot, 0, sizeof(cnet_sys_timeslot));
	memset(cnet_usr_timeslot, 0, sizeof(cnet_usr_timeslot));

	/* Mark timeslot usage for fixed system timeslots */

	cnet_mark_sys_slot(0, CNET_TIMESLOT_INTEGRITY, FALSE);
	cnet_mark_sys_slot(0, CNET_TIMESLOT_TIMELO, FALSE);
	cnet_mark_sys_slot(0, CNET_TIMESLOT_TIMEHI, FALSE);
	cnet_mark_sys_slot(0, CNET_TIMESLOT_WATCHDOG, TRUE);
}



/********************************************************************************
  FUNCTION NAME     : cnet_init_asynccomms
  FUNCTION DETAILS  : Initialise async comms and configure hardware.
********************************************************************************/

void cnet_init_asynccomms(uint32_t master, uint32_t position, uint32_t ringsize)
{
	int last;
	int pass1;
	int pass2;
	int n;
	uint32_t s;

	/* Calculate the timeslot offsets for pass1 and pass2 packets */

	pass1 = (master) ? 0 : (position * 3);
	pass2 = pass1 + (ringsize * 3);

	/* Check if this device is the last one in the ring */

	last = ((position + 1) == ringsize) ? TRUE : FALSE;

	/* Identify valid position for async comms control slot */

	cnet_find_sys_slot(0, CNET_TIMESLOT_ASYNCCTRL, TRUE, &s);
	mock_cnet_controller->cnet_mem_ctrl[s + pass1] = ((master) ? CNET_CTRL_COMMCTRLTX : CNET_CTRL_COMMCTRLORTX) | CNET_DATASLOT_ASYNCCTRL;
	mock_cnet_controller->cnet_mem_ctrl[s + pass2] = (last) ? CNET_CTRL_NOP : (CNET_CTRL_PASS | CNET_DATASLOT_ASYNCCTRL);
	mock_cnet_controller->cnet_mem_data_out[s] = 0;
	cnet_mark_sys_slot(0, s, TRUE);

	/* Identify valid positions for async comms data slots */

	for (n = 0; n < 8; n++) {
		cnet_find_sys_slot(0, CNET_TIMESLOT_ASYNCDATA, TRUE, &s);
		mock_cnet_controller->cnet_mem_ctrl[s + pass1] = ((master) ? CNET_CTRL_COMMDATATX : CNET_CTRL_COMMDATAORTX) | (CNET_DATASLOT_ASYNCDATA + n);
		mock_cnet_controller->cnet_mem_ctrl[s + pass2] = (last) ? CNET_CTRL_NOP : (CNET_CTRL_PASS | (CNET_DATASLOT_ASYNCDATA + n));
		cnet_mark_sys_slot(0, s, TRUE);
	}

}



/********************************************************************************
  FUNCTION NAME     : cnet_log_change
  FUNCTION DETAILS  : Called when a change in CNet state has occurred. generates
					  an entry in the event log.
********************************************************************************/

void cnet_log_change(int state, int ringpos)
{
#ifndef LOADER
	eventlog_entry log;

	log.type = LogTypeCNetStateChange;
	log.parameter1 = ringpos;
	log.parameter2 = state;
	log.timestamp_hi = cnet_time_hi;
	log.timestamp_lo = cnet_time_lo;
	eventlog_write(&log);
#endif
}

typedef struct cnet_slotdef {
	uint32_t slot;
	uint32_t config;
} cnet_slotdef;

cnet_slotdef cnet_alloc_list[] = {
  { CNET_DATASLOT_GENSTAT,   CNET_CTRL_ORTX },
  { CNET_DATASLOT_FLAGS,     CNET_CTRL_ORTX },
  { CNET_DATASLOT_ENCODER,   CNET_CTRL_TX,  },
  { CNET_DATASLOT_HYDREQ,    CNET_CTRL_ORTX },
  { CNET_DATASLOT_HYDSTATUS, CNET_CTRL_ORTX },
  { CNET_DATASLOT_GRPSTATUS, CNET_CTRL_ORTX },
  { CNET_DATASLOT_TSR0,      CNET_CTRL_ORTX },
  { CNET_DATASLOT_TSR0_EXT,  CNET_CTRL_ORTX },
  { CNET_DATASLOT_TSR1,      CNET_CTRL_ORTX },
  { CNET_DATASLOT_TSR2,      CNET_CTRL_ORTX },
  { CNET_DATASLOT_TSR3,      CNET_CTRL_ORTX },
  { CNET_DATASLOT_TSR4,      CNET_CTRL_ORTX },
  { CNET_DATASLOT_TSR5,      CNET_CTRL_ORTX },
  { CNET_DATASLOT_TSR6,      CNET_CTRL_ORTX },
};

#define LISTSIZE(x) (sizeof(x) / sizeof(x[0]))

/********************************************************************************
  FUNCTION NAME     : cnet_status_update
  FUNCTION DETAILS  : Called every 20ms to manage the changes in state of the
					  CNet system.
********************************************************************************/

void cnet_status_update(uint32_t reconfig, uint32_t new_mode)
{
	uint32_t n;
	static uint32_t cnet_state = CNET_STATE_NONE;
	static uint32_t cnet_reconfig_count = 0;

	/* Check if CNet is being reconfigured */

	if (reconfig) {

		cnet_configured = FALSE;

		/* CNet reconfiguration. Reset all slots to NOP state */

		for (n = 0; n < 512; n++) {
			mock_cnet_controller->cnet_mem_ctrl[n] = CNET_CTRL_NOP;
		}
		cnet_position = 0;
		cnet_local_addr = 0;
		cnet_bus_addr = cnet_local_addr | ((~cnet_local_addr & 0xFF) << 8);
		cnet_ringsize = 0;
		cnet_mode = new_mode;
		cnet_integrity_valid = 0;
		switch (new_mode) {
		case CNET_MODE_MASTER:
			mock_cnet_controller->cnet_mem_ctrl[CNET_TIMESLOT_INTEGRITY] = CNET_CTRL_INTEGRITY | CNET_DATASLOT_INTEGRITY;	/* Force transmission of link integrity packet 	*/
			cnet_integrity = 0;
			mock_cnet_controller->cnet_reg_ctrl = CNET_RESET;	/* Reset CNet hardware 	*/
			mock_cnet_controller->cnet_reg_ctrl = 0;			/* Release CNet reset 	*/
			mock_cnet_controller->cnet_reg_ctrl = CNET_MASTER | CNET_ENABLE;
			cnet_state = CNET_STATE_MASTERINTEGRITY;
			break;
		case CNET_MODE_SLAVE:
			mock_cnet_controller->cnet_reg_ctrl = CNET_RESET;	/* Reset CNet hardware 	*/
			mock_cnet_controller->cnet_reg_ctrl = 0;			/* Release CNet reset 	*/
			mock_cnet_controller->cnet_reg_ctrl = CNET_ENABLE;
			cnet_state = CNET_STATE_SLAVEINTEGRITY;
			break;
		case CNET_MODE_STANDALONE:
			mock_cnet_controller->cnet_reg_ctrl = CNET_RESET;	/* Reset CNet hardware 	*/
			mock_cnet_controller->cnet_reg_ctrl = 0;			/* Release CNet reset 	*/
			cnet_state = CNET_STATE_NONE;

			/* Initialise CNet watchdog variables */

			cnet_local_mask = 1;
			cnet_global_mask = global_mask_table[0];
			cnet_local_wdog = 0;

			cnet_log_change(CNET_MODE_STANDALONE, 0);
			cnet_configured = TRUE;
			break;
		}
		return;
	}

	switch (cnet_state) {
	case CNET_STATE_MASTERINTEGRITY:
		if (cnet_integrity_valid >= 256) {

			/* Master integrity check passed, so reconfigure */

			cnet_position = 0;								/* Master is first position in ring	*/
			cnet_local_addr = cnet_position;
			cnet_bus_addr = cnet_local_addr | ((~cnet_local_addr & 0xFF) << 8);
			cnet_ringsize = mock_cnet_controller->cnet_reg_position;				/* Get ring size					*/

			cnet_init_sys_slots(TRUE, 0, cnet_ringsize);		/* Initialise system slots 			*/

			/* Configure single pass slots as TX */

			mock_cnet_controller->cnet_mem_ctrl[CNET_TIMESLOT_TIMELO] = CNET_CTRL_TX | CNET_DATASLOT_TIMELO;
			mock_cnet_controller->cnet_mem_ctrl[CNET_TIMESLOT_TIMEHI] = CNET_CTRL_TX | CNET_DATASLOT_TIMEHI;

			/* Configure double pass slots as TX on the first pass and pass-through on the second pass, */

			mock_cnet_controller->cnet_mem_ctrl[CNET_TIMESLOT_WATCHDOG] = CNET_CTRL_TX | CNET_DATASLOT_WATCHDOG;
			mock_cnet_controller->cnet_mem_ctrl[CNET_TIMESLOT_WATCHDOG + (cnet_ringsize * 3)] = CNET_CTRL_PASS | CNET_DATASLOT_WATCHDOG;

			/* Allocate variable slots */

			cnet_init_asynccomms(TRUE, 0, cnet_ringsize);		/* Initialise async comms	*/

#ifndef LOADER	  

			for (n = 0; n < LISTSIZE(cnet_alloc_list); n++) {
				if (cnet_allocate_sys_slot(TRUE, 0, 0, cnet_alloc_list[n].slot, cnet_alloc_list[n].config, NULL) == NO_SLOT) diag_error(DIAG_CNETALLOC);
			}

#endif

			/* Configure link integrity packet data */

			cnet_integrity = CNET_LINKVALID | CNET_IDCOMPLETE | cnet_ringsize;

			/* Initialise CNet watchdog variables */

			cnet_local_mask = 1 << cnet_position;
			cnet_global_mask = global_mask_table[cnet_ringsize - 1];
			cnet_local_wdog = 0;

			/* Move to CNet reconfiguration state to allow operation to stabilise */

			cnet_state = CNET_STATE_RECONFIG;
			cnet_reconfig_count = 0;
		}
		break;
	case CNET_STATE_SLAVEINTEGRITY:
		if (cnet_integrity_valid >= 256) {
			if ((mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_INTEGRITY] & (CNET_LINKVALID | CNET_IDCOMPLETE)) == (CNET_LINKVALID | CNET_IDCOMPLETE)) {
				int pass1;
				int pass2;
				int last;

				/* Slave integrity check passed, so reconfigure */

				cnet_position = mock_cnet_controller->cnet_reg_position;								/* Get position in ring 	*/
				cnet_local_addr = cnet_position;
				cnet_bus_addr = cnet_local_addr | ((~cnet_local_addr & 0xFF) << 8);
				cnet_ringsize = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_INTEGRITY] & 0x3F;	/* Get ring size			*/

				cnet_init_sys_slots(FALSE, cnet_position, cnet_ringsize);		/* Initialise system slots 	*/

				/* Calculate the timeslot offsets for pass1 and pass2 packets */

				pass1 = cnet_position * 3;
				pass2 = pass1 + (cnet_ringsize * 3);

				/* Check if this device is the last one in the ring */

				last = ((cnet_position + 1) == cnet_ringsize) ? TRUE : FALSE;

				/* Configure single pass slots as pass-through */

				mock_cnet_controller->cnet_mem_ctrl[CNET_TIMESLOT_INTEGRITY + pass1] = CNET_CTRL_PASS | CNET_DATASLOT_INTEGRITY;
				mock_cnet_controller->cnet_mem_ctrl[CNET_TIMESLOT_TIMELO + pass1] = CNET_CTRL_PASS | CNET_DATASLOT_TIMELO;
				mock_cnet_controller->cnet_mem_ctrl[CNET_TIMESLOT_TIMEHI + pass1] = CNET_CTRL_PASS | CNET_DATASLOT_TIMEHI;

				/* Configure double pass slots as OR on the first pass and pass-through on the second pass,
				   except for the last device in the ring which must not pass on the packet.
				*/

				mock_cnet_controller->cnet_mem_ctrl[CNET_TIMESLOT_WATCHDOG + pass1] = CNET_CTRL_ORTX | CNET_DATASLOT_WATCHDOG;
				mock_cnet_controller->cnet_mem_ctrl[CNET_TIMESLOT_WATCHDOG + pass2] = (last) ? CNET_CTRL_NOP : (CNET_CTRL_PASS | CNET_DATASLOT_WATCHDOG);

				/* Allocate variable slots */

				cnet_init_asynccomms(FALSE, cnet_position, cnet_ringsize);	/* Initialise async comms	*/

#ifndef LOADER

				for (n = 0; n < LISTSIZE(cnet_alloc_list); n++) {
					if (cnet_allocate_sys_slot(FALSE, 0, 0, cnet_alloc_list[n].slot, cnet_alloc_list[n].config, NULL) == NO_SLOT) diag_error(DIAG_CNETALLOC);
				}

#endif

				/* Initialise CNet watchdog variables */

				cnet_local_mask = 1 << cnet_position;
				cnet_global_mask = global_mask_table[cnet_ringsize - 1];
				cnet_local_wdog = 0;

				/* Move to CNet reconfiguration state to allow operation to stabilise */

				cnet_state = CNET_STATE_RECONFIG;
				cnet_reconfig_count = 0;
			}
		}
		break;

	case CNET_STATE_RECONFIG:
		cnet_reconfig_count++;
		if (cnet_reconfig_count >= 16) {
			cnet_configured = TRUE;
			cnet_state = CNET_STATE_NORMAL;
			cnet_log_change(cnet_mode, cnet_position);
		}
		break;

	case CNET_STATE_NORMAL:
		if (cnet_integrity_valid < 256) {

			/* Integrity packets missing, so need to reconfigure CNet */

			cnet_state = (cnet_mode == CNET_MODE_MASTER) ? CNET_STATE_MASTERINTEGRITY : CNET_STATE_SLAVEINTEGRITY;
			cnet_configured = FALSE;
			cnet_position = 0;			/* Default ring position in ring */
			cnet_ringsize = 0;			/* Default ring size */
			cnet_local_addr = cnet_position;
			cnet_bus_addr = cnet_local_addr | ((~cnet_local_addr & 0xFF) << 8);
		}
		else if (cnet_mode == CNET_MODE_MASTER) {

			/* Master test for change in ring size */

			if (cnet_ringsize != mock_cnet_controller->cnet_reg_position) {

				/* Change in ring size from previous configuration, so need to reconfigure CNet */

				cnet_state = CNET_STATE_MASTERINTEGRITY;
				cnet_configured = FALSE;
				cnet_position = 0;			/* Default ring position in ring */
				cnet_ringsize = 0;			/* Default ring size */
				cnet_local_addr = cnet_position;
				cnet_bus_addr = cnet_local_addr | ((~cnet_local_addr & 0xFF) << 8);
			}
		}
		else {

			/* Slave test for change in ring size */

			if (cnet_ringsize != (mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_INTEGRITY] & 0x3F)) {

				/* Change in ring size from previous configuration, so need to reconfigure CNet */

				cnet_state = CNET_STATE_SLAVEINTEGRITY;
				cnet_configured = FALSE;
				cnet_position = 0;			/* Default ring position in ring */
				cnet_ringsize = 0;			/* Default ring size */
				cnet_local_addr = cnet_position;
				cnet_bus_addr = cnet_local_addr | ((~cnet_local_addr & 0xFF) << 8);
			}
		}
		break;

	case CNET_STATE_NONE:
		break;
	}
}



/********************************************************************************
  FUNCTION NAME     : cnet_monitor
  FUNCTION DETAILS  : Monitor state of CNet system. Called every 20ms.
********************************************************************************/

void cnet_monitor(void)
{
	uint32_t cnet_instate;
	uint32_t cnet_outstate;
	uint32_t cnet_state;
	uint32_t connstat;
	static uint32_t cnet_prev_instate = CNET_MODE_UNDEFINED;
	static uint32_t cnet_prev_outstate = CNET_MODE_UNDEFINED;

	/* Read current CNet IN and OUT connection states */

	connstat = mock_cnet_controller->cnet_reg_connstat;

	/* Filter to avoid excessive reconfiguring when status changes */

	if (connstat & CNET_INCONN) {
		cnet_inctr = (cnet_inctr < CNET_STABLE_MAX) ? cnet_inctr + 1 : cnet_inctr;
	}
	else {
		cnet_inctr = (cnet_inctr > 0) ? cnet_inctr - 1 : cnet_inctr;
	}
	if (connstat & CNET_OUTCONN) {
		cnet_outctr = (cnet_outctr < CNET_STABLE_MAX) ? cnet_outctr + 1 : cnet_outctr;
	}
	else {
		cnet_outctr = (cnet_outctr > 0) ? cnet_outctr - 1 : cnet_outctr;
	}

	/* Check if CNet connection state has changed and reconfigure if required */

	cnet_instate = CNET_IN_VALID;
	cnet_outstate = CNET_OUT_VALID;

	if ((cnet_instate != cnet_prev_instate) || (cnet_outstate != cnet_prev_outstate)) {
		cnet_state = cnet_state_table[((cnet_outstate & 0x01) << 1) | (cnet_instate & 0x01)];
		cnet_status_update(TRUE, cnet_state);
	}
	else {
		cnet_status_update(FALSE, 0);
	}

	cnet_prev_instate = cnet_instate;
	cnet_prev_outstate = cnet_outstate;
}



#define TXIDLE		0
#define TXRETRY		1
#define TXSTART1	2
#define TXMSG		3
#define TXWAIT		4
#define TXREPLY		5

#define RXIDLE		0
#define RXMSG		1
#define RXREPLY		2
#define RXSTART		3
#define RXSEND		4

cnet_msg cnet_rx_msg;			/* Incoming message/reply data			*/

int tx_state = TXIDLE;			/* Current transmit handler state		*/
int rx_state = RXIDLE;			/* Current receive handler state		*/

#ifndef LOADER
#pragma CODE_SECTION(cnet_async_handler, "realtime")
#endif

/********************************************************************************
  FUNCTION NAME     : cnet_async_handler
  FUNCTION DETAILS  : CNet async comms transmission handler.
********************************************************************************/

void cnet_async_handler(uint32_t connstat)
{

	/* Outgoing message handler variables */

	static cnet_msg* tx_msg;		/* Outgoing message/reply data			*/
	static int tx_msg_retry;		/* Count of message retries remaining 	*/
	static int tx_busy_retry;		/* Count of collision retries remaining	*/
	static uint8_t* tx_ptr;			/* Transmit/receive data pointer		*/
	static int tx_ctr;				/* Transmit/receive data counter		*/
	static int tx_timeout;			/* Timeout counter						*/
	static int backoff = 0;			/* Collision backoff counter			*/

	/* Incoming message handler variables */

	static uint8_t* rx_ptr;			/* Transmit/receive data pointer		*/
	static int rx_ctr;				/* Transmit/receive data counter		*/
	static int rx_src;				/* Source address of incoming message	*/

	static int debugtimer = 0;

	uint32_t asyncctrl;
	uint32_t busstate;

	struct mq_attr txqueue_attr;

	ssize_t bytes;
	debugtimer++;

	/* If a checksum error was detected on CNet data, retry any current transmit operation
	   and revert to the idle state for receive operations.
	*/

	if (connstat & CNET_CHECKSUM) {
		if (tx_state) tx_state = TXRETRY;
		rx_state = RXIDLE;
	}

	switch (tx_state) {
	case TXIDLE:
		mock_cnet_controller->cnet_reg_comm = 0; /* Release the bus */
		if (rx_state)
			break; /* Cannot transmit if already receiving */

		mq_getattr(cnet_async_txqueue, &txqueue_attr);
		bytes = mq_receive(cnet_async_txqueue, cnet_txqueue_buffer, txqueue_attr.mq_msgsize, 0);
		if (bytes < 0)
		{
			if (errno != EAGAIN)
			{
				printf("errno: %d\n", errno);
				perror("cnet_async_handler");
			}
			break;
			// message queue is non-blocking so there's no data if result is -1
		}
		tx_msg = (cnet_msg*)cnet_txqueue_buffer;

		//if ((cnet_msg*)(tx_msg = QUE_get(&cnet_async_txqueue)) == (cnet_msg*)&cnet_async_txqueue)
		//	break; /* Nothing to transmit */
		tx_msg_retry = CNET_ASYNC_MSG_RETRY;	/* Reset msg retry counter  */
		tx_busy_retry = CNET_ASYNC_BUSY_RETRY;	/* Reset busy retry counter */
		backoff = 0;
		tx_state = TXRETRY;

		/* Data to transmit, so fall through to begin transmission */

	case TXRETRY:
		if (backoff) {
			backoff--;
			mock_cnet_controller->cnet_reg_comm = 0; /* Release the bus */
		}
		else if (!(mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_ASYNCCTRL] & BUSY)) {
			tx_ptr = tx_msg->txbuf;
			tx_ctr = tx_msg->txsize;
			memcpy((void*)&mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCDATA], tx_ptr, 32);
			tx_ctr = (tx_ctr > 32) ? (tx_ctr - 32) : 0;
			tx_ptr += 32;
			mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCCTRL] = cnet_bus_addr |
				(tx_msg->dst << 16) |
				BUSY |
				INIT |
				((tx_ctr) ? 0 : FINAL);
			mock_cnet_controller->cnet_reg_comm = CNET_COMMDATA; /* Drive ctrl and data slots */
			tx_state = TXSTART1;
		}
		else {
			mock_cnet_controller->cnet_reg_comm = 0; /* Release the bus */
		}
		break;

	case TXSTART1:
		if (tx_ctr) {
			memcpy((void*)&mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCDATA], tx_ptr, 32);
			tx_ctr = (tx_ctr > 32) ? (tx_ctr - 32) : 0;
			tx_ptr += 32;
			mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCCTRL] = cnet_bus_addr |
				(tx_msg->dst << 16) |
				BUSY |
				((tx_ctr) ? 0 : FINAL);
			mock_cnet_controller->cnet_reg_comm = CNET_COMMDATA; /* Drive ctrl and data slots */
		}
		else {
			mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCCTRL] = BUSY;
			mock_cnet_controller->cnet_reg_comm = CNET_COMMCTRL; /* Release data slots */
		}
		tx_state = TXMSG;
		break;

	case TXMSG:
		busstate = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_ASYNCCTRL];
		if ((busstate & 0xFFFF) == cnet_bus_addr) {
			/* No conflict on the bus, so proceed with remainder of message */
			if (tx_ctr) {
				memcpy((void*)&mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCDATA], tx_ptr, 32);
				tx_ctr = (tx_ctr > 32) ? (tx_ctr - 32) : 0;
				tx_ptr += 32;
				mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCCTRL] = cnet_bus_addr |
					(tx_msg->dst << 16) |
					BUSY |
					((tx_ctr) ? 0 : FINAL);
				tx_state = TXMSG;
				mock_cnet_controller->cnet_reg_comm = CNET_COMMDATA; /* Drive ctrl and data slots */
			}
			else {
				if (tx_msg->noreply) {
					tx_msg->oprdy = CNET_COMPLETE;
					tx_msg->rxsize = 0;
					tx_state = TXIDLE;
					mock_cnet_controller->cnet_reg_comm = 0; /* Release the bus */
				}
				else {
					mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCCTRL] = BUSY;
					tx_state = TXWAIT;
					tx_timeout = cnet_timeout;
					mock_cnet_controller->cnet_reg_comm = CNET_COMMCTRL;	/* Release the data slots */
				}
			}
		}
		else {
			/* Bus conflict, so retry or abort this attempt */
			cnet_collision_count++;
			tx_busy_retry--;
			if (tx_busy_retry) {
				/* Retry the message */
				backoff = ((rand() ^ (cnet_local_addr << 2)) & 0x3F) + 16;
				tx_state = TXRETRY;
			}
			else {
				/* Abort the message */
				cnet_timeout_count++;
				tx_msg->oprdy = ERROR_CNET_TIMEOUT;
				tx_state = TXIDLE;
			}
			mock_cnet_controller->cnet_reg_comm = 0; /* Release the bus */
		}
		break;

	case TXWAIT: /* Wait for reply */
		mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCCTRL] = BUSY;
		if (tx_timeout) tx_timeout--;
		if (!tx_timeout) {
			/* Timeout on reply */
			tx_msg_retry--;
			if (tx_msg_retry) {
				cnet_retry_count++;
				/* Retry the message */
				tx_busy_retry = CNET_ASYNC_BUSY_RETRY; /* Reset busy retry counter */
				backoff = 0;
				tx_state = TXRETRY;
			}
			else {
				/* Abort the message */
				cnet_timeout_count++;
				tx_msg->oprdy = ERROR_CNET_TIMEOUT;
				tx_state = TXIDLE;
			}
			mock_cnet_controller->cnet_reg_comm = 0; /* Release the bus */
			break;
		}
		asyncctrl = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_ASYNCCTRL];
		if ((asyncctrl & (BUSY | INIT | REPLY)) == (BUSY | INIT | REPLY)) {
		txrestart:
			/* Start of reply message */
			if ((((asyncctrl & 0x00FF0000) >> 16) == cnet_local_addr) &&
				((asyncctrl & 0xFF) == ((~asyncctrl & 0xFF00) >> 8))) {
				tx_ctr = 0;
				tx_ptr = tx_msg->rxbuf;
				memcpy((void*)tx_ptr, (const void*)&mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_ASYNCDATA], 32);
				tx_ctr += 32;
				tx_ptr += 32;
				if (asyncctrl & FINAL) {
					/* Complete reply received */
					tx_msg->rxsize = tx_ctr;
					tx_msg->oprdy = CNET_COMPLETE;
					tx_state = TXIDLE;
				}
				else {
					tx_state = TXREPLY;
				}
			}
			mock_cnet_controller->cnet_reg_comm = 0; /* Release the bus */
		}
		else {
			mock_cnet_controller->cnet_reg_comm = CNET_COMMCTRL;
		}
		break;

	case TXREPLY:  /* Receive reply */
		asyncctrl = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_ASYNCCTRL];
		mock_cnet_controller->cnet_reg_comm = 0; /* Release the bus */
		if ((asyncctrl & (BUSY | INIT | REPLY)) == (BUSY | INIT | REPLY)) {
			goto txrestart; /* Reply has restarted */
		}
		if ((asyncctrl & BUSY) == BUSY) {
			if ((((asyncctrl & 0x00FF0000) >> 16) == cnet_local_addr) &&
				((asyncctrl & 0xFF) == ((~asyncctrl & 0xFF00) >> 8))) {
				memcpy((void*)tx_ptr, (const void*)&mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_ASYNCDATA], 32);
				tx_ctr += 32;
				tx_ptr += 32;
				if (asyncctrl & FINAL) {
					/* Complete reply received */
					tx_msg->rxsize = tx_ctr;
					tx_msg->oprdy = CNET_COMPLETE;
					tx_state = TXIDLE;
				}
				else {
					tx_state = TXREPLY; /* More data to receive */
				}
			}
			else {
				tx_state = TXWAIT; /* Bad data so return to waiting state */
			}
		}
		else {
			tx_state = TXWAIT; /* Bad data so return to waiting state */
		}
		break;
	default:
		tx_state = TXIDLE;
		break;
	}

	/* CNet async comms reception handler */

	switch (rx_state) {
	case RXIDLE:
		asyncctrl = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_ASYNCCTRL];
		if ((asyncctrl & (BUSY | INIT | REPLY)) == (BUSY | INIT)) {
		rxrestart:
			if ((((asyncctrl & 0x00FF0000) >> 16) == cnet_local_addr) &&
				((asyncctrl & 0xFF) == ((~asyncctrl & 0xFF00) >> 8))) {
				rx_src = asyncctrl & 0xFF;
				rx_ctr = 0;
				rx_ptr = cnet_rx_msg.rxbuf;
				memcpy((void*)rx_ptr, (const void*)&mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_ASYNCDATA], 32);
				rx_ctr += 32;
				rx_ptr += 32;
				if (asyncctrl & FINAL) {
					/* Complete message received, check if comms message or event report */
					if (cnet_rx_msg.rxbuf[1] & PKT_EVENT) {
#ifndef LOADER
						event_process_remote_event(cnet_rx_msg.rxbuf[1] & 0x7F, (event_report*)&cnet_rx_msg.rxbuf[8], *((int*)&cnet_rx_msg.rxbuf[4]));
#endif
						rx_state = RXIDLE; /* No reply from event reports */
					}
					else {
						cnet_rx_msg.rxsize = rx_ctr;
						cnet_rx_msg.oprdy = FALSE;
						cnet_rx_msg.iprdy = TRUE;
						sem_post(cnet_msg_sem);
						rx_state = RXREPLY; /* Wait for message to be processed */
					}
				}
				else {
					rx_state = RXMSG;
				}
			}
			else {
				rx_state = RXIDLE; /* Bad control word, so abort receipt of this message */
			}
		}
		break;

	case RXMSG:
		asyncctrl = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_ASYNCCTRL];
		if ((asyncctrl & (BUSY | INIT | REPLY)) == (BUSY | INIT)) {	/* Message restart */
			goto rxrestart;
		}
		if ((asyncctrl & BUSY) == BUSY) {
			if ((((asyncctrl & 0x00FF0000) >> 16) == cnet_local_addr) &&
				((asyncctrl & 0xFF) == ((~asyncctrl & 0xFF00) >> 8))) {
				memcpy((void*)rx_ptr, (const void*)&mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_ASYNCDATA], 32);
				rx_ctr += 32;
				rx_ptr += 32;
				if (rx_ctr >= DEFAULT_MSGBUFFER) {
					rx_state = RXIDLE; /* Buffer overflow, so abort receipt of this message */
				}
				else if (asyncctrl & FINAL) {
					/* Complete message received, check if comms message or event report */
					if (cnet_rx_msg.rxbuf[1] & PKT_EVENT) {
#ifndef LOADER
						event_process_remote_event(cnet_rx_msg.rxbuf[1] & 0x7F, (event_report*)&cnet_rx_msg.rxbuf[8], *((int*)&cnet_rx_msg.rxbuf[4]));
#endif
						rx_state = RXIDLE; /* No reply from event reports */
					}
					else {
						cnet_rx_msg.rxsize = rx_ctr;
						cnet_rx_msg.iprdy = TRUE;
						cnet_rx_msg.oprdy = FALSE;
						sem_post(cnet_msg_sem);
						rx_state = RXREPLY; /* Wait for message to be processed */
					}
				}
				else {
					rx_state = RXMSG;
				}
			}
			else {
				rx_state = RXIDLE; /* Bad control word, so abort receipt of this message */
			}
		}
		else {
			rx_state = RXIDLE; /* No comms, so abort receipt of this message */
		}
		break;

	case RXREPLY: /* Send reply */
		if (cnet_rx_msg.oprdy) {
			/* Received message has been processed and reply generated */
			cnet_rx_msg.iprdy = FALSE;
			cnet_rx_msg.oprdy = FALSE;
			rx_state = RXSTART;
			rx_ptr = cnet_rx_msg.txbuf;
			rx_ctr = cnet_rx_msg.txsize;
			memcpy((void*)&mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCDATA], rx_ptr, 32);
			rx_ctr = (rx_ctr > 32) ? (rx_ctr - 32) : 0;
			rx_ptr += 32;
			mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCCTRL] = cnet_bus_addr |
				(rx_src << 16) |
				BUSY |
				INIT |
				REPLY |
				((rx_ctr) ? 0 : FINAL);
			mock_cnet_controller->cnet_reg_comm = CNET_COMMDATA;
			rx_state = RXSTART;
		}
		break;

	case RXSTART:
		if (rx_ctr) {
			memcpy((void*)&mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCDATA], rx_ptr, 32);
			rx_ctr = (rx_ctr > 32) ? (rx_ctr - 32) : 0;
			rx_ptr += 32;
			mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCCTRL] = cnet_bus_addr |
				(rx_src << 16) |
				BUSY |
				REPLY |
				((rx_ctr) ? 0 : FINAL);
			mock_cnet_controller->cnet_reg_comm = CNET_COMMDATA;
		}
		else {
			mock_cnet_controller->cnet_reg_comm = 0;
		}
		rx_state = RXSEND;
		break;

	case RXSEND:
		busstate = mock_cnet_controller->cnet_mem_data_in[CNET_DATASLOT_ASYNCCTRL];
		if ((busstate & (0xFFFF)) == (cnet_local_addr | ((~cnet_local_addr & 0xFF) << 8))) {
			/* No conflict on the bus, so proceed with remainder of message */
			if (rx_ctr) {
				memcpy((void*)&mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCDATA], rx_ptr, 32);
				rx_ctr = (rx_ctr > 32) ? (rx_ctr - 32) : 0;
				rx_ptr += 32;
				mock_cnet_controller->cnet_mem_data_out[CNET_DATASLOT_ASYNCCTRL] = cnet_bus_addr |
					(rx_src << 16) |
					BUSY |
					REPLY |
					((rx_ctr) ? 0 : FINAL);
				mock_cnet_controller->cnet_reg_comm = CNET_COMMDATA;
				rx_state = RXSEND;
			}
			else {
				mock_cnet_controller->cnet_reg_comm = 0;
				rx_state = RXIDLE;
			}
		}
		else {
			/* Bus conflict, so abort this attempt */
			mock_cnet_controller->cnet_reg_comm = 0;
			rx_state = RXIDLE;
		}
		break;
	default:
		rx_state = RXIDLE;
		break;
	}

}



/********************************************************************************
  FUNCTION NAME     : cnet_async_transmit
  FUNCTION DETAILS  :
********************************************************************************/

int cnet_async_transmit(uint8_t* msg, uint8_t* reply, uint32_t msglength, uint32_t* replylength, uint32_t noreply)
{
	volatile cnet_msg m;
	commhdr* hdr;

	hdr = (commhdr*)msg;

	/* Check for no CNet or illegal address and return error */

	if (!cnet_configured || (cnet_mode == CNET_MODE_STANDALONE) || (hdr->dst >= cnet_ringsize)) {
		serial_write("CNet destination error: configured = %d, cnet_mode = %d, dst = %d, ringsize = %d\n", cnet_configured, cnet_mode, hdr->dst, cnet_ringsize);
		return(ERROR_CNET_DESTINATION);
	}

	/* Build CNet message structure */

	m.txbuf = msg;
	m.txsize = msglength;
	m.dst = hdr->dst;
	m.rxbuf = reply;
	m.iprdy = FALSE;
	m.noreply = noreply;

	/* Request CNet transfer */

	//SWI_disable();
	// TODO: interrupt disabling?
	m.oprdy = FALSE;
	mq_send(cnet_async_txqueue, (const char*)&m, sizeof(cnet_msg), 0);
	//QUE_put(&cnet_async_txqueue, (Ptr)&m);	/* Put into CNet transmit queue */
	// SWI_enable();

	/* Wait until CNet output processing complete */

	while (!m.oprdy);

	/* If the response is CNET_COMPLETE, then the transaction has been completed
	   without any errors, so return NO_ERROR. Otherwise, return the error code.
	*/

	if (m.oprdy == CNET_COMPLETE) {
		//  *replylength = m.rxsize - sizeof(commhdr);
		*replylength = ((commhdr*)m.rxbuf)->length;
		return(NO_ERROR);
	}
	*replylength = 0;
	return(m.oprdy);
}



/********************************************************************************
  FUNCTION NAME     : cnet_read_timestamp
  FUNCTION DETAILS  : Read CNet timestamp.
********************************************************************************/

void cnet_read_timestamp(uint32_t* lo, uint32_t* hi)
{
	uint32_t istate;

	// TODO: disabling interrupts?
//	istate = HWI_disable();
	if (lo) *lo = cnet_time_lo;
	if (hi) *hi = cnet_time_hi;
	//HWI_restore(istate);
}


void cnet_swap(void)
{
	memcpy(mock_cnet_controller->cnet_mem_data_in, mock_cnet_controller->cnet_mem_data_out, 1024);
}
