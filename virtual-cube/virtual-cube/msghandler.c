/********************************************************************************
  MODULE NAME   	: msghandler
  PROJECT			: Control Cube / Signal Cube
  MODULE DETAILS  	: Handler functions for system messages.
********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>

#include <stdint.h>
#include "porting.h"
#include "server.h"

#include "shared.h"
#include "defines.h"
#include "nonvol.h"

#include "hardware.h"
#include "hydraulic.h"
#include "msgserver.h"
#include "msgstruct.h"

#include "channel.h"
#include "system.h"
#include "eeprom.h"
#include "hw_kinet.h"
#include "variables.h"
#include "cnet.h"
#include "controller.h"
#include "debug.h"
#include "eventlog.h"
#include "eventrep.h"
#include "asmcode.h"

#include "HardwareIndependent/C3Std.h"

/* Prototype for function used to retrieve processor usage statistics */

void getprocuse(float* res, int reset);



#define CHECK_MAXMIN(m, r, max, min, reply, msg) if (((struct generic_maxmin *)m)->msginfo & 0x03) return(process_maxmin(m, r, max, min, reply, msg));

#define CHECK_SECURITY(required, reply, replylength) { if (!system_check_security(required)) return(error_security_level(reply, replylength)); }



/********************************************************************************
  FUNCTION NAME   	: fn_set_excitation
  FUNCTION DETAILS  : Handler for set_excitation message.
********************************************************************************/

int fn_set_excitation(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_excitation* m;
	chandef* c;

#if 0  /* Removed trap to allow phase change with pressure on */
	uint32_t err;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));
#endif

	* msglength = offsetof(struct msg_set_excitation, end);

	m = (struct msg_set_excitation*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setexc) {
		c->setexc(c, m->type, m->volt, m->phase, MIRROR);	/* Set required excitation */
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/* Values for max/min messages */

const struct msg_return_excitation msg_read_excitation_max = { 0,0,0,0,20.0,360.0 };
const struct msg_return_excitation msg_read_excitation_min = { 0,0,0,0,0.0,0.0 };

/********************************************************************************
  FUNCTION NAME   	: fn_read_excitation
  FUNCTION DETAILS  : Handler for read_excitation message.
********************************************************************************/

int fn_read_excitation(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_excitation* m;
	struct msg_return_excitation* r;
	chandef* c;

	m = (struct msg_read_excitation*)msg;
	r = (struct msg_return_excitation*)reply;

	*msglength = offsetof(struct msg_read_excitation, end);
	*replylength = offsetof(struct msg_return_excitation, end);

	//CHECK_MAXMIN(m, r, &msg_read_excitation_max, &msg_read_excitation_min, sizeof(struct msg_return_excitation), MSG_RETURN_EXCITATION);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->readexc) {
		c->readexc(c, (int*)&r->type, &r->volt, &r->phase);
		r->command = MSG_RETURN_EXCITATION;
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_exc_cal
  FUNCTION DETAILS  : Handler for set_exc_cal message.
********************************************************************************/

int fn_set_exc_cal(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_exc_cal* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_set_exc_cal, end);

	m = (struct msg_set_exc_cal*)msg;

	c = chanptr(m->chan & 0xFFFF);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setexccal) {
		c->setexccal(c, (m->chan >> 16), m->exc_zero, m->exc_fs);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_exc_cal
  FUNCTION DETAILS  : Handler for read_exc_cal message.
********************************************************************************/

int fn_read_exc_cal(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_exc_cal* m;
	struct msg_return_exc_cal* r;
	chandef* c;

	m = (struct msg_read_exc_cal*)msg;
	r = (struct msg_return_exc_cal*)reply;

	*msglength = offsetof(struct msg_read_exc_cal, end);
	*replylength = offsetof(struct msg_return_exc_cal, end);

	c = chanptr(m->chan & 0xFFFF);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->readexccal) {
		c->readexccal(c, (m->chan >> 16), &r->exc_zero, &r->exc_fs);
		r->command = MSG_RETURN_EXC_CAL;
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_gain
  FUNCTION DETAILS  : Handler for set_gain message.
********************************************************************************/

int fn_set_gain(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_gain* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_set_gain, end);

	m = (struct msg_set_gain*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setgain) {
		c->setgain(c, m->gain, (m->flags & SETGAIN_GAINTYPE) | SETGAIN_RETAIN_GAINTRIM, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_gain
  FUNCTION DETAILS  : Handler for read_gain message.
********************************************************************************/

int fn_read_gain(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_gain* m;
	struct msg_return_gain* r;
	chandef* c;

	m = (struct msg_read_gain*)msg;
	r = (struct msg_return_gain*)reply;

	*msglength = offsetof(struct msg_read_gain, end);
	*replylength = offsetof(struct msg_return_gain, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->readgain) {
		r->chan = m->chan;
		c->readgain(c, &r->gain, &r->flags);
		r->command = MSG_RETURN_GAIN;
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_cal
  FUNCTION DETAILS  : Handler for set_cal message.
********************************************************************************/

int fn_set_cal(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_cal* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_set_cal, end);

	m = (struct msg_set_cal*)msg;

#if 0
	if ((hyd_get_status() & HYD_STATUS_MASK) >= HYD_ACK_ON) {

		/* Operation illegal whilst hydraulics on */

		return(error_operation_failed(reply, replylength, HYDRAULICS_ACTIVE));
	}
#endif

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->calctrl) {
		c->calctrl(c, m->state);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_led
  FUNCTION DETAILS  : Handler for set_led message.
********************************************************************************/

int fn_set_led(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_led* m;
	chandef* c;

	*msglength = offsetof(struct msg_set_led, end);

	m = (struct msg_set_led*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->ledctrl) {
		c->ledctrl(c, m->state);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_clear_all_leds
  FUNCTION DETAILS  : Handler for clear_all_leds message.
********************************************************************************/

int fn_clear_all_leds(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	*msglength = offsetof(struct msg_clear_all_leds, end);
	chan_clear_all_leds();
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_filter
  FUNCTION DETAILS  : Handler for set_filter message.
********************************************************************************/

int fn_set_filter(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_filter* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_set_filter, end);

	m = (struct msg_set_filter*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setfilter) {
		c->setfilter(c, FILTER_ALL, m->filter, m->enable, m->type, m->order, m->freq, m->bandwidth, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_filter_freq
  FUNCTION DETAILS  : Handler for set_filter_freq message.
********************************************************************************/

int fn_set_filter_freq(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_filter_freq* m;
	chandef* c;

	*msglength = offsetof(struct msg_set_filter_freq, end);

	m = (struct msg_set_filter_freq*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setfilter) {
		c->setfilter(c, FILTER_FREQ, m->filter, 0, 0, 0, m->freq, m->bandwidth, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_filter
  FUNCTION DETAILS  : Handler for read_filter message.
********************************************************************************/

int fn_read_filter(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_filter* m;
	struct msg_return_filter* r;
	chandef* c;

	m = (struct msg_read_filter*)msg;
	r = (struct msg_return_filter*)reply;

	*msglength = offsetof(struct msg_read_filter, end);
	*replylength = offsetof(struct msg_return_filter, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->readfilter) {
		r->chan = m->chan;
		r->filter = m->filter;
		r->command = MSG_RETURN_FILTER;
		c->readfilter(c, m->filter, (int*)&r->enable, (int*)&r->type, (int*)&r->order, &r->freq, &r->bandwidth);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_ip_channel
  FUNCTION DETAILS  : Handler for read_ip_channel message.
********************************************************************************/

int fn_read_ip_channel(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_ip_channel* m;
	struct msg_return_ip_channel* r;
	chandef* c;

	m = (struct msg_read_ip_channel*)msg;
	r = (struct msg_return_ip_channel*)reply;

	*msglength = offsetof(struct msg_read_ip_channel, end);
	*replylength = offsetof(struct msg_return_ip_channel, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->readadc) {
		r->chan = m->chan;
		r->value = c->readadc(c);
		r->command = MSG_RETURN_IP_CHANNEL;
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_ip_cyclecount
  FUNCTION DETAILS  : Handler for read_ip_cyclecount message.
********************************************************************************/

int fn_read_ip_cyclecount(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_ip_cyclecount* m;
	struct msg_return_ip_cyclecount* r;
	chandef* c;

	m = (struct msg_read_ip_cyclecount*)msg;
	r = (struct msg_return_ip_cyclecount*)reply;

	*msglength = offsetof(struct msg_read_ip_cyclecount, end);
	*replylength = offsetof(struct msg_return_ip_cyclecount, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->readcycle) {
		r->chan = m->chan;
		r->count = c->readcycle(c);
		r->command = MSG_RETURN_IP_CYCLECOUNT;
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_ip_cyclecountblock
  FUNCTION DETAILS  : Handler for read_ip_cyclecountblock message.
********************************************************************************/

int fn_read_ip_cyclecountblock(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_ip_cyclecountblock* m;
	struct msg_return_ip_cyclecountblock* r;
	uint32_t clist;
	uint32_t b;
	uint32_t ch;
	uint32_t n;
	chandef* c;
	uint32_t* p;

	m = (struct msg_read_ip_cyclecountblock*)msg;
	r = (struct msg_return_ip_cyclecountblock*)reply;

	*msglength = offsetof(struct msg_read_ip_cyclecountblock, end);

	r->command = MSG_RETURN_IP_CYCLECOUNTBLOCK;
	p = &r->count;

	for (b = 0; b < 2; b++) {
		ch = b * 32;
		clist = m->chanlist[b];
		for (n = 0; (n < 32) && clist; n++) {
			if (clist & 0x01) {
				if (c = chanptr(ch)) {
					*p++ = c->readcycle(c);
				}
				else {
					return(error_illegal_channel(reply, replylength, ch));
				}
			}
			clist = clist >> 1;
			ch++;
		}
	}
	*replylength = offsetof(struct msg_return_ip_cyclecountblock, count) + ((p - &r->count) * sizeof(uint32_t));

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_reset_cyclecount
  FUNCTION DETAILS  : Handler for reset_cyclecount message.
********************************************************************************/

int fn_reset_cyclecount(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_reset_cyclecount* m;
	chandef* c;

	*msglength = offsetof(struct msg_reset_cyclecount, end);

	m = (struct msg_reset_cyclecount*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->resetcycles) {
		c->resetcycles(c, m->flags);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_reset_all_cyclecounts
  FUNCTION DETAILS  : Handler for reset_all_cyclecounts message.
********************************************************************************/

int fn_reset_all_cyclecounts(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_reset_all_cyclecounts* m;

	*msglength = offsetof(struct msg_reset_all_cyclecounts, end);

	m = (struct msg_reset_all_cyclecounts*)msg;

	reset_all_cycles(m->flags);
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_write_op_channel
  FUNCTION DETAILS  : Handler for write_op_channel message.
********************************************************************************/

int fn_write_op_channel(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_write_op_channel* m;
	chandef* c;

	*msglength = offsetof(struct msg_write_op_channel, end);

	m = (struct msg_write_op_channel*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->writedac) {
		c->writedac(c, m->value);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



#if (defined(CONTROLCUBE) || defined(AICUBE))

/********************************************************************************
  FUNCTION NAME   	: fn_set_hydraulic_output
  FUNCTION DETAILS  : Handler for set_hydraulic_output message.
********************************************************************************/

int fn_set_hydraulic_output(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_hydraulic_output* m;

	*msglength = offsetof(struct msg_set_hydraulic_output, end);

	m = (struct msg_set_hydraulic_output*)msg;

	hyd_modify_op(m->set, m->clear);
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_hydraulic_output
  FUNCTION DETAILS  : Handler for read_hydraulic_output message.
********************************************************************************/

int fn_read_hydraulic_output(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_hydraulic_output* r;

	r = (struct msg_return_hydraulic_output*)reply;

	*msglength = offsetof(struct msg_read_hydraulic_output, end);
	*replylength = offsetof(struct msg_return_hydraulic_output, end);

	r->command = MSG_RETURN_HYDRAULIC_OUTPUT;
	r->state = hyd_modify_op(0, 0);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_hydraulic_input
  FUNCTION DETAILS  : Handler for read_hydraulic_input message.
********************************************************************************/

int fn_read_hydraulic_input(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_hydraulic_input* r;

	r = (struct msg_return_hydraulic_input*)reply;

	*msglength = offsetof(struct msg_read_hydraulic_input, end);
	*replylength = offsetof(struct msg_return_hydraulic_input, end);

	r->command = MSG_RETURN_HYDRAULIC_INPUT;
	r->state = hyd_read_ip();
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_hydraulic_state
  FUNCTION DETAILS  : Handler for read_hydraulic_state message.
********************************************************************************/

int fn_read_hydraulic_state(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_hydraulic_state* r;

	r = (struct msg_return_hydraulic_state*)reply;

	*msglength = offsetof(struct msg_read_hydraulic_state, end);
	*replylength = offsetof(struct msg_return_hydraulic_state, end);

	r->command = MSG_RETURN_HYDRAULIC_STATE;
	hyd_read_state(&r->state, &r->ack);
	r->error = hyd_check_request(REQUEST_2);	/* Check if start would be valid */
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_define_hydraulic_io
  FUNCTION DETAILS  : Handler for define_hydraulic_io message.
********************************************************************************/

int fn_define_hydraulic_io(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_define_hydraulic_io* m;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_define_hydraulic_io, end);

	m = (struct msg_define_hydraulic_io*)msg;

	if (m->timeout > 65535) return(error_illegal_parameter(reply, replylength));

	if (err = hyd_set_io_definition(m->ident, m->flags, m->label, m->timeout))
		return(error_operation_failed(reply, replylength, err));

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_hydraulic_io
  FUNCTION DETAILS  : Handler for read_hydraulic_io message.
********************************************************************************/

int fn_read_hydraulic_io(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_hydraulic_io* m;
	struct msg_return_hydraulic_io* r;

	m = (struct msg_read_hydraulic_io*)msg;
	r = (struct msg_return_hydraulic_io*)reply;

	*msglength = offsetof(struct msg_read_hydraulic_io, end);
	*replylength = offsetof(struct msg_return_hydraulic_io, end);

	r->command = MSG_RETURN_HYDRAULIC_IO;
	hyd_read_io_definition(m->ident, &r->flags, r->label, &r->timeout);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_define_hydraulic_mode
  FUNCTION DETAILS  : Handler for define_hydraulic_mode message.
********************************************************************************/

int fn_define_hydraulic_mode(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_define_hydraulic_mode* m;
	uint32_t hyd_type;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_define_hydraulic_mode, end);

	m = (struct msg_define_hydraulic_mode*)msg;

	/* Read current hydraulic type */

	hyd_read_mode(NULL, NULL, &hyd_type);

	/* Keep existing simulation mode flag, but update the rest of the hydraulic type */

	hyd_type = (hyd_type & HYD_SIM_MASK) | (m->type & ~HYD_SIM_MASK);

	if (err = hyd_set_mode(m->mode, m->group, hyd_type))
		return(error_operation_failed(reply, replylength, err));

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_hydraulic_mode
  FUNCTION DETAILS  : Handler for read_hydraulic_mode message.
********************************************************************************/

int fn_read_hydraulic_mode(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_hydraulic_mode* r;

	r = (struct msg_return_hydraulic_mode*)reply;

	*msglength = offsetof(struct msg_read_hydraulic_mode, end);
	*replylength = offsetof(struct msg_return_hydraulic_mode, end);

	r->command = MSG_RETURN_HYDRAULIC_MODE;
	hyd_read_mode(&r->mode, &r->group, &r->type);
	//hyd_read_mode(NULL, NULL, &r->type);
	//r->master = 0;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_hydraulic_dissipation
  FUNCTION DETAILS  : Handler for set_hydraulic_dissipation message.
********************************************************************************/

int fn_set_hydraulic_dissipation(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_hydraulic_dissipation* m;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_set_hydraulic_dissipation, end);

	m = (struct msg_set_hydraulic_dissipation*)msg;

	if (err = hyd_set_dissipation(m->dissipation))
		return(error_operation_failed(reply, replylength, err));

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_hydraulic_dissipation
  FUNCTION DETAILS  : Handler for read_hydraulic_dissipation message.
********************************************************************************/

int fn_read_hydraulic_dissipation(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_hydraulic_dissipation* r;

	r = (struct msg_return_hydraulic_dissipation*)reply;

	*msglength = offsetof(struct msg_read_hydraulic_dissipation, end);
	*replylength = offsetof(struct msg_return_hydraulic_dissipation, end);

	r->command = MSG_RETURN_HYDRAULIC_DISSIPATION;
	r->dissipation = hyd_read_dissipation();

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_hydraulic_name
  FUNCTION DETAILS  : Handler for set_hydraulic_name message.
********************************************************************************/

int fn_set_hydraulic_name(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_hydraulic_name* m;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_set_hydraulic_name, end);

	m = (struct msg_set_hydraulic_name*)msg;

	if (err = hyd_set_name(m->name))
		return(error_operation_failed(reply, replylength, err));

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_hydraulic_name
  FUNCTION DETAILS  : Handler for read_hydraulic_name message.
********************************************************************************/

int fn_read_hydraulic_name(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	//struct msg_read_hydraulic_name *m;
	struct msg_return_hydraulic_name* r;

	//m = (struct msg_read_hydraulic_name *)msg;
	r = (struct msg_return_hydraulic_name*)reply;

	*msglength = offsetof(struct msg_read_hydraulic_name, end);
	*replylength = offsetof(struct msg_return_hydraulic_name, end);

	r->command = MSG_RETURN_HYDRAULIC_NAME;
	hyd_read_name(r->name);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_hydraulic_control
  FUNCTION DETAILS  : Handler for hydraulic_control message.
********************************************************************************/

int fn_hydraulic_control(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_hydraulic_control* m;

	*msglength = offsetof(struct msg_hydraulic_control, end);

	m = (struct msg_hydraulic_control*)msg;
	if (err = hyd_control(m->request))
		return(error_operation_failed(reply, replylength, err));

	return(NO_ERROR);
}

#endif /* CONTROLCUBE */



/********************************************************************************
  FUNCTION NAME   	: fn_read_slot_configuration
  FUNCTION DETAILS  : Handler for read_slot_configuration message.
********************************************************************************/

int fn_read_slot_configuration(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_slot_configuration* m;
	struct msg_return_slot_configuration* r;

	m = (struct msg_read_slot_configuration*)msg;
	r = (struct msg_return_slot_configuration*)reply;

	*msglength = offsetof(struct msg_read_slot_configuration, end);
	*replylength = offsetof(struct msg_return_slot_configuration, end);

	r->command = MSG_RETURN_SLOT_CONFIGURATION;
	r->slot = m->slot;
	r->type = slot_status[m->slot].type;
	r->numinchan = slot_status[m->slot].numinchan;
	r->numoutchan = slot_status[m->slot].numoutchan;
	r->nummiscchan = slot_status[m->slot].nummiscchan;
	memcpy(r->serialno, slot_status[m->slot].serialno, sizeof(r->serialno));
	memcpy(r->data, slot_status[m->slot].data, sizeof(r->data));

	return(NO_ERROR);
}



extern connection* msg_list;
extern connection* rtmsg_list;
extern connection* telnet_list;

extern near uint32_t debug_rt_bufin_count;
extern near uint32_t debug_rt_rampin_count;
extern near uint32_t debug_rt_copy_count;
extern near uint32_t debug_rt_notxfr_count;
extern near uint32_t debug_rt_underrun_count;
extern near uint32_t debug_rt_overrun_count;
extern near uint32_t debug_rt_ramp_count;
extern near uint32_t debug_rt_noinput_count;
extern near uint32_t debug_rt_local_count;

const uint32_t virtualinchan = ExpNExp;
const uint32_t commandinchan = C3AppNChans;
const uint32_t maxinchan = MAXINCHAN;

const uint8_t cnetinfo[] = { 22,							/* Length of data block 			*/
						  8,							/* Length of async comms data block */
						 CNET_DATASLOT_ENCODER,
						 CNET_DATASLOT_GRPSTATUS,
						 CNET_DATASLOT_TSR0,
						 CNET_DATASLOT_TSR0_EXT,
						 CNET_DATASLOT_TSR1,
						 CNET_DATASLOT_TSR2,
						 CNET_DATASLOT_TSR3,
						 CNET_DATASLOT_TSR4,
						 CNET_DATASLOT_TSR5,
						 CNET_DATASLOT_TSR6,
						 CNET_DATASLOT_HYDREQ,
						 CNET_DATASLOT_HYDSTATUS,
						 CNET_DATASLOT_ASYNCCTRL,
						 CNET_DATASLOT_ASYNCDATA,
						 CNET_DATASLOT_WATCHDOG,
						 CNET_DATASLOT_TIMELO,
						 CNET_DATASLOT_TIMEHI,
						 CNET_DATASLOT_INTEGRITY,
						 CNET_DATASLOT_GENSTAT,
						 CNET_DATASLOT_FLAGS
};

/********************************************************************************
  FUNCTION NAME   	: fn_read_sysinfo
  FUNCTION DETAILS  : Handler for read_sysinfo message.
********************************************************************************/

int fn_read_sysinfo(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_sysinfo* m;
	struct msg_return_sysinfo* r;
	int reset = FALSE;

	m = (struct msg_read_sysinfo*)msg;
	r = (struct msg_return_sysinfo*)reply;

	*msglength = offsetof(struct msg_read_sysinfo, end);

	r->command = MSG_RETURN_SYSINFO;

	switch (m->type) {
	case 0: /* Message server info */
		handler_status(msg_list, (int*)&r->data[0], (int*)&r->data[4], (int*)&r->data[8]);
		*replylength = offsetof(struct msg_return_sysinfo, data) + (3 * sizeof(int));
		break;
	case 1: /* Realtime server info */
		handler_status(rtmsg_list, (int*)&r->data[0], (int*)&r->data[4], (int*)&r->data[8]);
		*replylength = offsetof(struct msg_return_sysinfo, data) + (3 * sizeof(int));
		break;
	case 2: /* Telnet server info */
		// TODO: telnet?
		//handler_status(telnet_list, (int*)&r->data[0], (int*)&r->data[4], (int*)&r->data[8]);
		*replylength = offsetof(struct msg_return_sysinfo, data) + (3 * sizeof(int));
		break;
	case 4: /* Realtime debug info (reset counters) */
		reset = TRUE;
	case 3: /* Realtime debug info */
		memcpy(&r->data[0], &debug_rt_overrun_count, sizeof(uint32_t));
		memcpy(&r->data[4], &debug_rt_underrun_count, sizeof(uint32_t));
		memcpy(&r->data[8], &debug_rt_bufin_count, sizeof(uint32_t));
		memcpy(&r->data[12], &debug_rt_rampin_count, sizeof(uint32_t));
		memcpy(&r->data[16], &debug_rt_copy_count, sizeof(uint32_t));
		memcpy(&r->data[20], &debug_rt_notxfr_count, sizeof(uint32_t));
		memcpy(&r->data[24], &debug_rt_ramp_count, sizeof(uint32_t));
		memcpy(&r->data[28], &debug_rt_noinput_count, sizeof(uint32_t));
		memcpy(&r->data[32], &debug_rt_local_count, sizeof(uint32_t));
		if (reset) {
			debug_rt_bufin_count = 0;
			debug_rt_rampin_count = 0;
			debug_rt_copy_count = 0;
			debug_rt_notxfr_count = 0;
			debug_rt_underrun_count = 0;
			debug_rt_overrun_count = 0;
			debug_rt_ramp_count = 0;
			debug_rt_noinput_count = 0;
			debug_rt_local_count = 0;
		}
		*replylength = offsetof(struct msg_return_sysinfo, data) + (9 * sizeof(uint32_t));
		break;
	case 5: /* Channel counts */
		memcpy(&r->data[0], &totalinchan, sizeof(uint32_t));
		memcpy(&r->data[4], &totaloutchan, sizeof(uint32_t));
		memcpy(&r->data[8], &totalmiscchan, sizeof(uint32_t));
		memcpy(&r->data[12], &totaldigiochan, sizeof(uint32_t));
		memcpy(&r->data[16], &totalvirtualchan, sizeof(uint32_t));
		memcpy(&r->data[20], &physinchan, sizeof(uint32_t));
		memcpy(&r->data[24], &virtualinchan, sizeof(uint32_t));
		memcpy(&r->data[28], &commandinchan, sizeof(uint32_t));
		memcpy(&r->data[32], &maxinchan, sizeof(uint32_t));
		*replylength = offsetof(struct msg_return_sysinfo, data) + (9 * sizeof(uint32_t));
		break;
	case 6: /* CNet slot allocation information */
		memcpy(&r->data[0], &cnetinfo, cnetinfo[0]);
		*replylength = offsetof(struct msg_return_sysinfo, data) + cnetinfo[0];
		break;
	case 7: /* CNet status/statistics */
		cnet_read_mode((uint32_t*)&r->data[0], 				/* Enable						*/
			(uint32_t*)&r->data[4],				/* Master						*/
			(uint32_t*)&r->data[8]);				/* ID							*/
		cnet_rdstat(NULL, (uint32_t*)&r->data[12], 			/* In connector status			*/
			(uint32_t*)&r->data[16],			/* Out connector status			*/
			(uint32_t*)&r->data[20],			/* Lock status					*/
			(uint32_t*)&r->data[24],			/* Integrity packet error count	*/
			(uint32_t*)&r->data[28],			/* Fault count					*/
			(uint32_t*)&r->data[32],			/* Checksum error count			*/
			(uint32_t*)&r->data[36],			/* Watchdog state				*/
			(uint32_t*)&r->data[40],			/* Timestamp low word			*/
			(uint32_t*)&r->data[44],			/* Timestamp high word			*/
			(uint32_t*)&r->data[48],			/* Mode							*/
			(uint32_t*)&r->data[52],			/* Ring position				*/
			(uint32_t*)&r->data[56]);			/* Ring size					*/
		*((uint32_t*)&r->data[60]) = cnet_configured;			/* Configuration state			*/
		*((uint32_t*)&r->data[64]) = cnet_wdog_count;			/* Watchdog error count			*/
		*((uint32_t*)&r->data[68]) = cnet_retry_count;		/* Async comms retry count		*/
		*((uint32_t*)&r->data[72]) = cnet_collision_count;	/* Async comms collision count	*/
		*((uint32_t*)&r->data[76]) = cnet_timeout_count;		/* Async comms timeout count	*/
		*((uint32_t*)&r->data[80]) = cnet_interrupt_ctr;		/* Interrupt count				*/
		*replylength = offsetof(struct msg_return_sysinfo, data) + 84;
		break;
	case 8: /* Processor usage statistics */
		//getprocuse((float*)&r->data[0], FALSE);
		*replylength = offsetof(struct msg_return_sysinfo, data) + (16 * sizeof(float));
		break;
	case 9: /* Processor usage statistics with automatic reset */
		//getprocuse((float*)&r->data[0], TRUE);
		*replylength = offsetof(struct msg_return_sysinfo, data) + (16 * sizeof(float));
		break;

	default:
		/* Illegal parameter */
		return(error_illegal_parameter(reply, replylength));
	}
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_chan_information
  FUNCTION DETAILS  : Handler for read_chan_information message.
********************************************************************************/

int fn_read_chan_information(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_chan_information* m;
	struct msg_return_chan_information* r;
	chandef* c;

	m = (struct msg_read_chan_information*)msg;
	r = (struct msg_return_chan_information*)reply;

	*msglength = offsetof(struct msg_read_chan_information, end);
	*replylength = offsetof(struct msg_return_chan_information, end);

	r->command = MSG_RETURN_CHAN_INFORMATION;
	r->chan = m->chan;

	if (c = chanptr(m->chan)) {
		r->type = c->type;
	}
	else {
		return(error_illegal_channel(reply, replylength, m->chan));
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_channel_source
  FUNCTION DETAILS  : Handler for set_channel_source message.
********************************************************************************/

int fn_set_channel_source(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_channel_source* m;
	chandef* c;

	*msglength = offsetof(struct msg_set_channel_source, end);

	m = (struct msg_set_channel_source*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	chan_set_cnet_input(c, m->cnetslot);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_raw_gain
  FUNCTION DETAILS  : Handler for set_raw_gain message.
********************************************************************************/

int fn_set_raw_gain(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_raw_gain* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	m = (struct msg_set_raw_gain*)msg;

	*msglength = offsetof(struct msg_set_raw_gain, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setrawgain) {
		c->setrawgain(c, m->coarsegain, m->finegain);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_raw_gain
  FUNCTION DETAILS  : Handler for read_raw_gain message.
********************************************************************************/

int fn_read_raw_gain(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_raw_gain* m;
	struct msg_return_raw_gain* r;
	chandef* c;

	m = (struct msg_read_raw_gain*)msg;
	r = (struct msg_return_raw_gain*)reply;

	*msglength = offsetof(struct msg_read_raw_gain, end);
	*replylength = offsetof(struct msg_return_raw_gain, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->readrawgain) {
		r->command = MSG_RETURN_RAW_GAIN;
		r->chan = m->chan;
		c->readrawgain(c, (uint32_t*)&(r->coarsegain), (uint32_t*)&(r->finegain));
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_calgain
  FUNCTION DETAILS  : Handler for set_calgain message.
********************************************************************************/

int fn_set_calgain(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_calgain* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	m = (struct msg_set_calgain*)msg;

	*msglength = offsetof(struct msg_set_calgain, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_calgain) {
		c->set_calgain(c, m->gain);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_calgain
  FUNCTION DETAILS  : Handler for read_calgain message.
********************************************************************************/

int fn_read_calgain(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_calgain* m;
	struct msg_return_calgain* r;
	chandef* c;

	m = (struct msg_read_calgain*)msg;
	r = (struct msg_return_calgain*)reply;

	*msglength = offsetof(struct msg_read_calgain, end);
	*replylength = offsetof(struct msg_return_calgain, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->read_calgain) {
		r->command = MSG_RETURN_CALGAIN;
		r->chan = m->chan;
		r->gain = c->read_calgain(c);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_save_chan_config
  FUNCTION DETAILS  : Handler for save_chan_config message.
********************************************************************************/

int fn_save_chan_config(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_save_chan_config* m;
	chandef* c;

	m = (struct msg_save_chan_config*)msg;

	*msglength = offsetof(struct msg_save_chan_config, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->saveconfig) {
		c->saveconfig(c);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_restore_chan_config
  FUNCTION DETAILS  : Handler for restore_chan_config message.
********************************************************************************/

int fn_restore_chan_config(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_restore_chan_config* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	m = (struct msg_restore_chan_config*)msg;

	*msglength = offsetof(struct msg_restore_chan_config, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->restoreconfig) {

		/* Perform restore operation, but generate an error if a bad checksum was detected */

		if (!c->restoreconfig(c)) return(error_operation_failed(reply, replylength, BAD_CHECKSUM));
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_save_chan_calibration
  FUNCTION DETAILS  : Handler for save_chan_calibration message.
********************************************************************************/

int fn_save_chan_calibration(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_save_chan_calibration* m;
	chandef* c;

	m = (struct msg_save_chan_calibration*)msg;

	*msglength = offsetof(struct msg_save_chan_calibration, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->savecalibration) {
		c->savecalibration(c);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_restore_chan_calibration
  FUNCTION DETAILS  : Handler for restore_chan_calibration message.
********************************************************************************/

int fn_restore_chan_calibration(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_restore_chan_calibration* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	m = (struct msg_restore_chan_calibration*)msg;

	*msglength = offsetof(struct msg_restore_chan_calibration, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->restorecalibration) {

		/* Perform restore operation, but generate an error if a bad checksum was detected */

		if (!c->restorecalibration(c)) return(error_operation_failed(reply, replylength, BAD_CHECKSUM));
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_cnet_channel
  FUNCTION DETAILS  : Handler for read_cnet_channel message.
********************************************************************************/

int fn_read_cnet_channel(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_cnet_channel* m;
	struct msg_return_cnet_channel* r;

	m = (struct msg_read_cnet_channel*)msg;
	r = (struct msg_return_cnet_channel*)reply;

	*msglength = offsetof(struct msg_read_cnet_channel, end);
	*replylength = offsetof(struct msg_return_cnet_channel, end);

	if (m->chan > 255) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	r->command = MSG_RETURN_CNET_CHANNEL;
	r->chan = m->chan;
	r->value = cnet_get_slotdata(m->chan);
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_cnet_slot
  FUNCTION DETAILS  : Handler for set_cnet_slot message.
********************************************************************************/

int fn_set_cnet_slot(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_cnet_slot* m;
	chandef* c;

	m = (struct msg_set_cnet_slot*)msg;

	*msglength = offsetof(struct msg_set_cnet_slot, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_cnet_slot) {
		c->set_cnet_slot(c, m->slot);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_cnet_status
  FUNCTION DETAILS  : Handler for read_cnet_status message.
********************************************************************************/

int fn_read_cnet_status(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_cnet_status* r;
	uint32_t enable;
	uint32_t master;
	uint32_t in;
	uint32_t out;
	uint32_t lock;
	uint32_t integrity;

	r = (struct msg_return_cnet_status*)reply;

	*msglength = offsetof(struct msg_read_cnet_status, end);
	*replylength = offsetof(struct msg_return_cnet_status, end);

	cnet_read_mode(&enable, &master, &r->localid);
	cnet_rdstat(NULL, &in, &out, &lock, &integrity, &r->faults, &r->chksum, NULL, NULL, NULL, NULL, NULL, NULL);

	r->command = MSG_RETURN_CNET_STATUS;
	r->flags = ((master) ? MSGFLAG_CNET_MASTER : 0) |
		((enable) ? MSGFLAG_CNET_ENABLE : 0) |
		((in) ? MSGFLAG_CNET_INCONN : 0) |
		((out) ? MSGFLAG_CNET_OUTCONN : 0) |
		((integrity) ? MSGFLAG_CNET_INTEGRITY : 0);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_cnet_valid_range
  FUNCTION DETAILS  : Handler for read_cnet_valid_range message.
********************************************************************************/

int fn_read_cnet_valid_range(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_cnet_valid_range* r;

	r = (struct msg_return_cnet_valid_range*)reply;

	*msglength = offsetof(struct msg_read_cnet_valid_range, end);
	*replylength = offsetof(struct msg_return_cnet_valid_range, end);

	r->command = MSG_RETURN_CNET_VALID_RANGE;
	r->max_slot = CNET_SYSTEM_BASE - 1;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_gaintrim
  FUNCTION DETAILS  : Handler for set_gaintrim message.
********************************************************************************/

int fn_set_gaintrim(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_gaintrim* m;
	chandef* c;

	*msglength = offsetof(struct msg_set_gaintrim, end);

	m = (struct msg_set_gaintrim*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_gaintrim) {
		c->set_gaintrim(c, &m->gaintrim, NULL, m->calmode, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_gaintrim
  FUNCTION DETAILS  : Handler for read_gaintrim message.
********************************************************************************/

int fn_read_gaintrim(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_gaintrim* m;
	struct msg_return_gaintrim* r;
	chandef* c;

	m = (struct msg_read_gaintrim*)msg;
	r = (struct msg_return_gaintrim*)reply;

	*msglength = offsetof(struct msg_read_gaintrim, end);
	*replylength = offsetof(struct msg_return_gaintrim, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_gaintrim) {
		r->command = MSG_RETURN_GAINTRIM;
		c->read_gaintrim(c, &r->gaintrim, NULL);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_neg_gaintrim
  FUNCTION DETAILS  : Handler for set_neg_gaintrim message.
********************************************************************************/

int fn_set_neg_gaintrim(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_neg_gaintrim* m;
	chandef* c;

	*msglength = offsetof(struct msg_set_neg_gaintrim, end);

	m = (struct msg_set_neg_gaintrim*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_gaintrim) {
		c->set_gaintrim(c, NULL, &m->neg_gaintrim, m->calmode, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_neg_gaintrim
  FUNCTION DETAILS  : Handler for read_neg_gaintrim message.
********************************************************************************/

int fn_read_neg_gaintrim(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_neg_gaintrim* m;
	struct msg_return_neg_gaintrim* r;
	chandef* c;

	m = (struct msg_read_neg_gaintrim*)msg;
	r = (struct msg_return_neg_gaintrim*)reply;

	*msglength = offsetof(struct msg_read_neg_gaintrim, end);
	*replylength = offsetof(struct msg_return_neg_gaintrim, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_gaintrim) {
		r->command = MSG_RETURN_NEG_GAINTRIM;
		c->read_gaintrim(c, NULL, &r->neg_gaintrim);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_txdrzero
  FUNCTION DETAILS  : Handler for set_txdrzero message.
********************************************************************************/

int fn_set_txdrzero(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_txdrzero* m;
	chandef* c;

	*msglength = offsetof(struct msg_set_txdrzero, end);

	m = (struct msg_set_txdrzero*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_txdrzero) {
		int err = c->set_txdrzero(c, m->value, TRUE, UPDATECLAMP, MIRROR);
		if (err) return(error_operation_failed(reply, replylength, err));

		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_txdrzero
  FUNCTION DETAILS  : Handler for read_txdrzero message.
********************************************************************************/

int fn_read_txdrzero(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_txdrzero* m;
	struct msg_return_txdrzero* r;
	chandef* c;

	m = (struct msg_read_txdrzero*)msg;
	r = (struct msg_return_txdrzero*)reply;

	*msglength = offsetof(struct msg_read_txdrzero, end);
	*replylength = offsetof(struct msg_return_txdrzero, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_txdrzero) {
		r->command = MSG_RETURN_TXDRZERO;
		r->value = c->read_txdrzero(c);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_txdrzero_zero
  FUNCTION DETAILS  : Handler for txdrzero_zero message.
********************************************************************************/

int fn_txdrzero_zero(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_txdrzero_zero* m;
	chandef* c;

	*msglength = offsetof(struct msg_txdrzero_zero, end);

	m = (struct msg_txdrzero_zero*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

#if (defined(CONTROLCUBE) || defined(AICUBE))

	if (c->txdrzero_zero) {
		int err = c->txdrzero_zero(c, TRUE, UPDATECLAMP, MIRROR);
		if (err) return(error_operation_failed(reply, replylength, err));
		return(NO_ERROR);
	}

#endif

#ifdef SIGNALCUBE

	if (c->startzero) {
		int err;

		err = c->startzero(c);
		if (err) return(error_operation_failed(reply, replylength, err));

		return(NO_ERROR);
	}
	else if (c->txdrzero_zero) {
		c->txdrzero_zero(c, TRUE, UPDATECLAMP, MIRROR);
		return(NO_ERROR);
	}

#endif


	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_caloffset
  FUNCTION DETAILS  : Handler for set_caloffset message.
********************************************************************************/

int fn_set_caloffset(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_caloffset* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_set_caloffset, end);

	m = (struct msg_set_caloffset*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_caloffset) {
		c->set_caloffset(c, m->offset);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_caloffset
  FUNCTION DETAILS  : Handler for read_caloffset message.
********************************************************************************/

int fn_read_caloffset(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_caloffset* m;
	struct msg_return_caloffset* r;
	chandef* c;

	m = (struct msg_read_caloffset*)msg;
	r = (struct msg_return_caloffset*)reply;

	*msglength = offsetof(struct msg_read_caloffset, end);
	*replylength = offsetof(struct msg_return_caloffset, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_caloffset) {
		r->command = MSG_RETURN_CALOFFSET;
		r->offset = c->read_caloffset(c);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_chan_flags
  FUNCTION DETAILS  : Handler for set_chan_flags message.
********************************************************************************/

int fn_set_chan_flags(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_chan_flags* m;
	chandef* c;

	*msglength = offsetof(struct msg_set_chan_flags, end);

	m = (struct msg_set_chan_flags*)msg;

	/* Trap any attempt to modify any flag except FLAG_GBCYC (bit 27) while pressure is on */

	if (((m->set | m->clr) & ~FLAG_GBCYC) && (err = system_check_allowed(0, ALLOW_PR_ANY))) {
		return(error_operation_failed(reply, replylength, err));
	}

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_flags) {
		c->set_flags(c, m->set, m->clr, UPDATECLAMP, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_chan_flags
  FUNCTION DETAILS  : Handler for read_chan_flags message.
********************************************************************************/

int fn_read_chan_flags(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_chan_flags* m;
	struct msg_return_chan_flags* r;
	chandef* c;

	m = (struct msg_read_chan_flags*)msg;
	r = (struct msg_return_chan_flags*)reply;

	*msglength = offsetof(struct msg_read_chan_flags, end);
	*replylength = offsetof(struct msg_return_chan_flags, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_flags) {
		r->command = MSG_RETURN_CHAN_FLAGS;
		r->flags = c->read_flags(c);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_peaks
  FUNCTION DETAILS  : Handler for read_peaks message.
********************************************************************************/

int fn_read_peaks(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_peaks* m;
	struct msg_return_peaks* r;
	chandef* c;

	m = (struct msg_read_peaks*)msg;
	r = (struct msg_return_peaks*)reply;

	*msglength = offsetof(struct msg_read_peaks, end);
	*replylength = offsetof(struct msg_return_peaks, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_peakinfo) {
		float local[4];
		r->command = MSG_RETURN_PEAKS;
		c->read_peakinfo(c, local, 4);
		memcpy(&r->maximum, local, sizeof(float) * 4);
		return(NO_ERROR);
	}
	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_reset_peaks
  FUNCTION DETAILS  : Handler for reset_peaks message.
********************************************************************************/

int fn_reset_peaks(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_reset_peaks* m;
	chandef* c;

	*msglength = offsetof(struct msg_reset_peaks, end);

	m = (struct msg_reset_peaks*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->reset_peaks) {
		c->reset_peaks(c, m->flags);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_reset_all_peaks
  FUNCTION DETAILS  : Handler for reset_all_peaks message.
********************************************************************************/

int fn_reset_all_peaks(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_reset_all_peaks* m;

	*msglength = offsetof(struct msg_reset_all_peaks, end);

	m = (struct msg_reset_all_peaks*)msg;

	reset_all_peaks(m->flags);
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_ip_channelblock
  FUNCTION DETAILS  : Handler for read_ip_channelblock message.
********************************************************************************/

int fn_read_ip_channelblock(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_ip_channelblock* m;
	struct msg_return_ip_channelblock* r;
	uint32_t clist;
	uint32_t b;
	uint32_t n;
	uint32_t ch;
	chandef* c;
	float* p;

	m = (struct msg_read_ip_channelblock*)msg;
	r = (struct msg_return_ip_channelblock*)reply;

	*msglength = offsetof(struct msg_read_ip_channelblock, end);

	r->command = MSG_RETURN_IP_CHANNELBLOCK;
	p = &r->value;

	for (b = 0; b < 2; b++) {
		ch = b * 32;
		clist = m->chanlist[b];
		for (n = 0; (n < 32) && clist; n++) {
			if (clist & 0x01) {
				if (c = chanptr(ch)) {
					p[0] = c->readadc(c);			/* Read current value */
					c->read_peakinfo(c, &p[1], 4);	/* Read max,min,peak,trough */
					p += 5;
				}
				else {
					return(error_illegal_channel(reply, replylength, ch));
				}
			}
			clist = clist >> 1;
			ch++;
		}
	}
	*replylength = offsetof(struct msg_return_ip_channelblock, value) + ((p - &r->value) * sizeof(float));

	return(NO_ERROR);
}





/********************************************************************************
  FUNCTION NAME   	: fn_read_ip_channelblock_ext
  FUNCTION DETAILS  : Handler for read_ip_channelblock_ext message.
********************************************************************************/

int fn_read_ip_channelblock_ext(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_ip_channelblock_ext* m;
	struct msg_return_ip_channelblock_ext* r;
	uint32_t clist;
	float local[8];
	uint32_t ch;
	chandef* c;
	float* p;
	float* s;

	m = (struct msg_read_ip_channelblock_ext*)msg;
	r = (struct msg_return_ip_channelblock_ext*)reply;

	*msglength = offsetof(struct msg_read_ip_channelblock_ext, end);

	r->command = MSG_RETURN_IP_CHANNELBLOCK_EXT;
	p = r->value;

	for (ch = 0; ch < 64; ch++) {
		clist = m->chanlist[ch];
		if (clist) {
			if (c = chanptr(ch)) {
				if (clist & 0x01) local[0] = c->readadc(c);				/* Read current value */
				if (clist & 0x7E) c->read_peakinfo(c, &local[1], 6);	/* Read max,min,peak,trough,cycles,period */

				/* Copy local values into response buffer */

				s = local;
				while (clist) {
					if (clist & 0x01) *p++ = *s++;
					clist = clist >> 1;
				}
			}
			else {
				return(error_illegal_channel(reply, replylength, ch));
			}
		}
	}
	*replylength = offsetof(struct msg_return_ip_channelblock, value) + ((p - r->value) * sizeof(float));

	return(NO_ERROR);
}


/********************************************************************************
  FUNCTION NAME   	: fn_write_map
  FUNCTION DETAILS  : Handler for write_map message.
********************************************************************************/

int fn_write_map(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	//uint32_t err;
	struct msg_write_map* m;
	chandef* c;

	//if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_write_map, end);

	m = (struct msg_write_map*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (chan_validate_map(m->value, 257)) {
		return(error_illegal_parameter(reply, replylength));
	}

	if (c->write_map) {
		c->write_map(c, m->flags, m->value, m->filename, TRUE, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_map
  FUNCTION DETAILS  : Handler for read_map message.
********************************************************************************/

int fn_read_map(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_map* m;
	struct msg_return_map* r;
	chandef* c;

	m = (struct msg_read_map*)msg;
	r = (struct msg_return_map*)reply;

	*msglength = offsetof(struct msg_read_map, end);
	*replylength = offsetof(struct msg_return_map, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_map) {
		r->command = MSG_RETURN_MAP;
		c->read_map(c, &r->flags, r->value, r->filename);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_refzero
  FUNCTION DETAILS  : Handler for set_refzero message.
********************************************************************************/

int fn_set_refzero(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_refzero* m;
	chandef* c;

	*msglength = offsetof(struct msg_set_refzero, end);

	m = (struct msg_set_refzero*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_refzero) {
		int err = c->set_refzero(c, m->value, TRUE, UPDATECLAMP, MIRROR);
		if (err) return(error_operation_failed(reply, replylength, err));
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_refzero
  FUNCTION DETAILS  : Handler for read_refzero message.
********************************************************************************/

int fn_read_refzero(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_refzero* m;
	struct msg_return_refzero* r;
	chandef* c;

	m = (struct msg_read_refzero*)msg;
	r = (struct msg_return_refzero*)reply;

	*msglength = offsetof(struct msg_read_refzero, end);
	*replylength = offsetof(struct msg_return_refzero, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_refzero) {
		r->command = MSG_RETURN_REFZERO;
		r->value = c->read_refzero(c);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_refzero_zero
  FUNCTION DETAILS  : Handler for refzero_zero message.
********************************************************************************/

int fn_refzero_zero(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_refzero_zero* m;
	chandef* c;

	*msglength = offsetof(struct msg_refzero_zero, end);

	m = (struct msg_refzero_zero*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

#if (defined(CONTROLCUBE) || defined(AICUBE))

	if (c->refzero_zero) {
		int err = c->refzero_zero(c, TRUE, UPDATECLAMP, MIRROR);
		if (err) return(error_operation_failed(reply, replylength, err));

		return(NO_ERROR);
	}

#endif

#ifdef SIGNALCUBE

	/* For the Signal Cube, if a startzero function is available then use it, because that
	   will use any hardware zero capabilities. Otherwise, try to do a zero by adjusting
	   the reference zero setting.
	*/

	if (c->startzero) {
		int err;

		err = c->startzero(c);
		if (err) return(error_operation_failed(reply, replylength, err));

		return(NO_ERROR);
	}
	else if ((c->set_refzero) && (c->read_refzero) && (c->readadc)) {

		/* Should disable interrupts around this code */

		c->set_refzero(c, (c->read_refzero(c)) - (c->readadc(c)), TRUE, UPDATECLAMP, MIRROR);
		return(NO_ERROR);
	}

#endif

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_undo_refzero
  FUNCTION DETAILS  : Handler for undo_refzero message.
********************************************************************************/

int fn_undo_refzero(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_undo_refzero* m;
	chandef* c;

	*msglength = offsetof(struct msg_undo_refzero, end);

	m = (struct msg_undo_refzero*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->undo_refzero) {
		c->undo_refzero(c, TRUE, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_sv_range
  FUNCTION DETAILS  : Handler for set_sv_range message.
********************************************************************************/

int fn_set_sv_range(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_sv_range* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_set_sv_range, end);

	m = (struct msg_set_sv_range*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_current) {
		c->set_current(c, m->type, m->range, 0, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_sv_range
  FUNCTION DETAILS  : Handler for read_sv_range message.
********************************************************************************/

int fn_read_sv_range(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_sv_range* m;
	struct msg_return_sv_range* r;
	chandef* c;

	m = (struct msg_read_sv_range*)msg;
	r = (struct msg_return_sv_range*)reply;

	*msglength = offsetof(struct msg_read_sv_range, end);
	*replylength = offsetof(struct msg_return_sv_range, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_current) {
		r->command = MSG_RETURN_SV_RANGE;
		c->read_current(c, &r->type, &r->range);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_raw_current
  FUNCTION DETAILS  : Handler for set_raw_current message.
********************************************************************************/

int fn_set_raw_current(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_raw_current* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_set_raw_current, end);

	m = (struct msg_set_raw_current*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setrawcurrent) {
		c->setrawcurrent(c, m->current);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_raw_excitation
  FUNCTION DETAILS  : Handler for set_raw_excitation message.
********************************************************************************/

int fn_set_raw_excitation(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_raw_excitation* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_set_raw_excitation, end);

	m = (struct msg_set_raw_excitation*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setrawexc) {
		c->setrawexc(c, m->type, m->volt, m->phase, 0);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_flush_chan_config
  FUNCTION DETAILS  : Handler for flush_chan_config message.
********************************************************************************/

int fn_flush_chan_config(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_flush_chan_config* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_flush_chan_config, end);

	m = (struct msg_flush_chan_config*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->flushconfig) {
		c->flushconfig(c);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_flush_chan_calibration
  FUNCTION DETAILS  : Handler for flush_chan_calibration message.
********************************************************************************/

int fn_flush_chan_calibration(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_flush_chan_calibration* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	*msglength = offsetof(struct msg_flush_chan_calibration, end);

	m = (struct msg_flush_chan_calibration*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->flushcalibration) {
		c->flushcalibration(c);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_chan_status
  FUNCTION DETAILS  : Handler for read_chan_status message.
********************************************************************************/

int fn_read_chan_status(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_chan_status* m;
	struct msg_return_chan_status* r;
	chandef* c;

	m = (struct msg_read_chan_status*)msg;
	r = (struct msg_return_chan_status*)reply;

	*msglength = offsetof(struct msg_read_chan_status, end);
	*replylength = offsetof(struct msg_return_chan_status, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->readstatus) {
		r->command = MSG_RETURN_CHAN_STATUS;
		c->readstatus(c, &r->ctrl, &r->status);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_caltable
  FUNCTION DETAILS  : Handler for set_caltable message.
********************************************************************************/

int fn_set_caltable(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_set_caltable* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	m = (struct msg_set_caltable*)msg;

	*msglength = offsetof(struct msg_set_caltable, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_caltable) {
		c->set_caltable(c, m->entry, m->value);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_caltable
  FUNCTION DETAILS  : Handler for read_caltable message.
********************************************************************************/

int fn_read_caltable(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_caltable* m;
	struct msg_return_caltable* r;
	chandef* c;

	m = (struct msg_read_caltable*)msg;
	r = (struct msg_return_caltable*)reply;

	*msglength = offsetof(struct msg_read_caltable, end);
	*replylength = offsetof(struct msg_return_caltable, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->read_caltable) {
		r->command = MSG_RETURN_CALTABLE;
		r->chan = m->chan;
		r->entry = m->entry;
		r->value = c->read_caltable(c, m->entry);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_full_caltable
  FUNCTION DETAILS  : Handler for read_full_caltable message.
********************************************************************************/

int fn_read_full_caltable(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_full_caltable* m;
	struct msg_return_full_caltable* r;
	chandef* c;
	int n;

	m = (struct msg_read_full_caltable*)msg;
	r = (struct msg_return_full_caltable*)reply;

	*msglength = offsetof(struct msg_read_full_caltable, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->read_caltable) {
		*replylength = offsetof(struct msg_return_full_caltable, value) + (m->len * sizeof(float));
		r->command = MSG_RETURN_CALTABLE;
		r->chan = m->chan;
		r->len = m->len;
		for (n = 0; n < m->len; n++) {
			r->value[n] = c->read_caltable(c, n);
		}
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_digtxdr_config
  FUNCTION DETAILS  : Handler for set_digtxdr_config message.
********************************************************************************/

int fn_set_digtxdr_config(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_digtxdr_config* m;
	chandef* c;

	m = (struct msg_set_digtxdr_config*)msg;

	*msglength = offsetof(struct msg_set_digtxdr_config, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setdigtxdrconfig) {
		c->setdigtxdrconfig(c, m->flags, m->bitsize, m->ssi_datalen, m->ssi_clkperiod, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_digtxdr_config
  FUNCTION DETAILS  : Handler for read_digtxdr_config message.
********************************************************************************/

int fn_read_digtxdr_config(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_digtxdr_config* m;
	struct msg_return_digtxdr_config* r;
	chandef* c;

	m = (struct msg_read_digtxdr_config*)msg;
	r = (struct msg_return_digtxdr_config*)reply;

	*msglength = offsetof(struct msg_read_digtxdr_config, end);
	*replylength = offsetof(struct msg_return_digtxdr_config, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->readdigtxdrconfig) {
		r->command = MSG_RETURN_DIGTXDR_CONFIG;
		r->chan = m->chan;
		c->readdigtxdrconfig(c, &r->flags, &r->bitsize, &r->ssi_datalen, &r->ssi_clkperiod);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_hpfilter_config
  FUNCTION DETAILS  : Handler for set_hpfilter_config message.
********************************************************************************/

int fn_set_hpfilter_config(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_hpfilter_config* m;
	chandef* c;

	m = (struct msg_set_hpfilter_config*)msg;

	*msglength = offsetof(struct msg_set_hpfilter_config, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->sethpfilter) {
		c->sethpfilter(c, m->enable, m->freq, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_hpfilter_config
  FUNCTION DETAILS  : Handler for read_hpfilter_config message.
********************************************************************************/

int fn_read_hpfilter_config(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_hpfilter_config* m;
	struct msg_return_hpfilter_config* r;
	chandef* c;

	m = (struct msg_read_hpfilter_config*)msg;
	r = (struct msg_return_hpfilter_config*)reply;

	*msglength = offsetof(struct msg_read_hpfilter_config, end);
	*replylength = offsetof(struct msg_return_hpfilter_config, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->readhpfilter) {
		r->command = MSG_RETURN_HPFILTER_CONFIG;
		r->chan = m->chan;
		c->readhpfilter(c, &r->enable, &r->freq);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_sv_dither
  FUNCTION DETAILS  : Handler for set_sv_dither message.
********************************************************************************/

int fn_set_sv_dither(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_sv_dither* m;
	chandef* c;

	m = (struct msg_set_sv_dither*)msg;

	*msglength = offsetof(struct msg_set_sv_dither, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_dither) {
		c->set_dither(c, m->enable, m->dither, m->freq, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_sv_dither
  FUNCTION DETAILS  : Handler for read_sv_dither message.
********************************************************************************/

int fn_read_sv_dither(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_sv_dither* m;
	struct msg_return_sv_dither* r;
	chandef* c;

	m = (struct msg_read_sv_dither*)msg;
	r = (struct msg_return_sv_dither*)reply;

	*msglength = offsetof(struct msg_read_sv_dither, end);
	*replylength = offsetof(struct msg_return_sv_dither, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->read_dither) {
		r->command = MSG_RETURN_SV_DITHER;
		r->chan = m->chan;
		c->read_dither(c, &r->enable, &r->dither, &r->freq);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_sv_balance
  FUNCTION DETAILS  : Handler for set_sv_balance message.
********************************************************************************/

int fn_set_sv_balance(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_sv_balance* m;
	chandef* c;

	m = (struct msg_set_sv_balance*)msg;

	*msglength = offsetof(struct msg_set_sv_balance, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_balance) {
		c->set_balance(c, m->balance, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_sv_balance
  FUNCTION DETAILS  : Handler for read_sv_balance message.
********************************************************************************/

int fn_read_sv_balance(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_sv_balance* m;
	struct msg_return_sv_balance* r;
	chandef* c;

	m = (struct msg_read_sv_balance*)msg;
	r = (struct msg_return_sv_balance*)reply;

	*msglength = offsetof(struct msg_read_sv_balance, end);
	*replylength = offsetof(struct msg_return_sv_balance, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->read_balance) {
		r->command = MSG_RETURN_SV_BALANCE;
		r->chan = m->chan;
		c->read_balance(c, &r->balance);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_serno
  FUNCTION DETAILS  : Handler for read_serno message.
********************************************************************************/

int fn_read_serno(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_serno* r;
	uint32_t err;

	r = (struct msg_return_serno*)reply;

	*msglength = offsetof(struct msg_read_serno, end);
	*replylength = offsetof(struct msg_return_serno, end);

	if (err = dsp_1w_readpage(PAGE_CTRLSN, (uint8_t*)r->serno, 8, CHK_CHKSUM)) {
		serial_write("Read serial no. failed, err = %d\n", err);
		return(error_operation_failed(reply, replylength, BAD_CHECKSUM));
	}

	r->command = MSG_RETURN_SERNO;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_serno
  FUNCTION DETAILS  : Handler for set_serno message.
********************************************************************************/

int fn_set_serno(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_serno* m;
	uint32_t err;

	m = (struct msg_set_serno*)msg;

	*msglength = offsetof(struct msg_set_serno, end);

	if (err = dsp_1w_writepage(PAGE_CTRLSN, (uint8_t*)m->serno, 8, WR_CHKSUM)) {
		serial_write("Read serial no. failed, err = %d\n", err);
		return(error_operation_failed(reply, replylength, BAD_WRITE));
	}

	return(NO_ERROR);
}



#if (defined(CONTROLCUBE) || defined(AICUBE))

/********************************************************************************
  FUNCTION NAME   	: fn_send_kinet_message
  FUNCTION DETAILS  : Handler for send_kinet_message message.
********************************************************************************/

int fn_send_kinet_message(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	// TODO: no kinet support
	//struct msg_send_kinet_message* m;
	//struct msg_return_kinet_message* r;
	//uint32_t rxlength;

	//m = (struct msg_send_kinet_message*)msg;
	//r = (struct msg_return_kinet_message*)reply;

	//*msglength = offsetof(struct msg_send_kinet_message, data) + (m->length * sizeof(uint16_t));

	//if (kinetptr && (kinetptr->flags & KINET_ENABLE)) {
	//	kinetptr->asynccomms(m->flags, m->data, m->length, m->chksum, r->data, &rxlength);
	//	r->length = rxlength;
	//	*replylength = offsetof(struct msg_return_kinet_message, data) + (rxlength * sizeof(uint16_t));
	//}
	//else {

	//	/* No Kinet hardware or KiNet disabled - build error response and return error code */

	//	return(error_illegal_function(reply, replylength));
	//}

	//r->command = MSG_RETURN_KINET_MESSAGE;
	return(NO_ERROR);
}

#endif



/********************************************************************************
  FUNCTION NAME   	: fn_define_system_name
  FUNCTION DETAILS  : Handler for define_system_name message.
********************************************************************************/

int fn_define_system_name(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_define_system_name* m;
	uint8_t name[32];
	int err;

	m = (struct msg_define_system_name*)msg;

	*msglength = offsetof(struct msg_define_system_name, end);

	memset(name, 0, sizeof(name));
	strncpy((char*)name, m->name, 31);

	if (err = dsp_1w_writepage(PAGE_CTRLNAME, name, 31, WR_CHKSUM)) {
		return(error_operation_failed(reply, replylength, err));
	}

	strncpy(sysname, (const char*)name, 32);

	//strncpy(snvbs->sysname, m->name, sizeof(snvbs->sysname));
	//CtrlSnvCB(snvbs->sysname, sizeof(snvbs->sysname));

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_system_name
  FUNCTION DETAILS  : Handler for read_system_name message.
********************************************************************************/

int fn_read_system_name(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	//struct msg_read_system_name *m;
	struct msg_return_system_name* r;

	//m = (struct msg_read_system_name *)msg;
	r = (struct msg_return_system_name*)reply;

	*msglength = offsetof(struct msg_read_system_name, end);
	*replylength = offsetof(struct msg_return_system_name, end);

	r->command = MSG_RETURN_SYSTEM_NAME;

	/* Before copying the name string, fill the response name array with all zeroes to ensure that
	   there will always be a valid terminator for the name.
	*/

	memset(r->name, 0, sizeof(r->name));
	strncpy(r->name, sysname, 31); /* Copy maximum of 31 characters to ensure a valid terminator */
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_define_rig_name
  FUNCTION DETAILS  : Handler for define_rig_name message.
********************************************************************************/

int fn_define_rig_name(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_define_rig_name* m;
	uint8_t name[32];
	int err;

	m = (struct msg_define_rig_name*)msg;

	*msglength = offsetof(struct msg_define_rig_name, end);

	memset(name, 0, sizeof(name));
	strncpy((char*)name, m->name, 31);

	if (err = dsp_1w_writepage(PAGE_RIGNAME, name, 31, WR_CHKSUM)) {
		return(error_operation_failed(reply, replylength, err));
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_rig_name
  FUNCTION DETAILS  : Handler for read_rig_name message.
********************************************************************************/

int fn_read_rig_name(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_rig_name* r;
	uint8_t name[31];

	r = (struct msg_return_rig_name*)reply;

	*msglength = offsetof(struct msg_read_rig_name, end);
	*replylength = offsetof(struct msg_return_rig_name, end);

	r->command = MSG_RETURN_RIG_NAME;

	/* Before copying the name string, fill the response name array with all zeroes to ensure that
	   there will always be a valid terminator for the name.
	*/

	if (dsp_1w_readpage(PAGE_RIGNAME, (uint8_t*)&name, sizeof(name), CHK_CHKSUM)) {
		memset(name, 0, sizeof(name));
	}

	memset(r->name, 0, sizeof(r->name));
	strncpy(r->name, (const char*)name, 31); /* Copy maximum of 31 characters to ensure a valid terminator */
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_define_comm_timeout
  FUNCTION DETAILS  : Handler for define_comm_timeout message.
********************************************************************************/

int fn_define_comm_timeout(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_define_comm_timeout* m;
	connection* c = (connection*)conn;

	m = (struct msg_define_comm_timeout*)msg;

	*msglength = offsetof(struct msg_define_comm_timeout, end);

	c->timeout = (m->timeout) ? m->timeout : 0xFFFFFFFFU;
	c->action = DEFAULT_CONN_ACTION;

	c->timeoutctr = c->timeout;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_comm_timeout
  FUNCTION DETAILS  : Handler for read_comm_timeout message.
********************************************************************************/

int fn_read_comm_timeout(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_comm_timeout* r;
	connection* c = (connection*)conn;

	r = (struct msg_return_comm_timeout*)reply;

	*msglength = offsetof(struct msg_read_comm_timeout, end);
	*replylength = offsetof(struct msg_return_comm_timeout, end);

	r->command = MSG_RETURN_COMM_TIMEOUT;
	r->timeout = (c->timeout == 0xFFFFFFFFU) ? 0 : c->timeout;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_define_comm_timeout_ext
  FUNCTION DETAILS  : Handler for define_comm_timeout_ext message.
********************************************************************************/

int fn_define_comm_timeout_ext(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_define_comm_timeout_ext* m;
	connection* c = (connection*)conn;

	m = (struct msg_define_comm_timeout_ext*)msg;

	*msglength = offsetof(struct msg_define_comm_timeout_ext, end);

	c->timeout = (m->timeout) ? m->timeout : 0xFFFFFFFFU;
	c->action = m->action;

	c->timeoutctr = c->timeout;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_comm_timeout_ext
  FUNCTION DETAILS  : Handler for read_comm_timeout_ext message.
********************************************************************************/

int fn_read_comm_timeout_ext(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_comm_timeout_ext* r;
	connection* c = (connection*)conn;

	r = (struct msg_return_comm_timeout_ext*)reply;

	*msglength = offsetof(struct msg_read_comm_timeout_ext, end);
	*replylength = offsetof(struct msg_return_comm_timeout_ext, end);

	r->command = MSG_RETURN_COMM_TIMEOUT_EXT;
	r->timeout = (c->timeout == 0xFFFFFFFFU) ? 0 : c->timeout;
	r->action = c->action;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_comm_closing
  FUNCTION DETAILS  : Handler for set_comm_closing message.
********************************************************************************/

int fn_set_comm_closing(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	connection* c = (connection*)conn;

	*msglength = offsetof(struct msg_set_comm_closing, end);

	c->closing = TRUE;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_comm_id
  FUNCTION DETAILS  : Handler for read_comm_id message.
********************************************************************************/

int fn_read_comm_id(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_comm_id* r;
	connection* c = (connection*)conn;

	r = (struct msg_return_comm_id*)reply;

	*msglength = offsetof(struct msg_read_comm_id, end);
	*replylength = offsetof(struct msg_return_comm_id, end);

	r->command = MSG_RETURN_COMM_ID;
	r->id = c->id;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_force_comm_close
  FUNCTION DETAILS  : Handler for force_comm_close message.
********************************************************************************/

int fn_force_comm_close(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_force_comm_close* m;

	m = (struct msg_force_comm_close*)msg;

	*msglength = offsetof(struct msg_force_comm_close, end);

	if (comm_close(m->id)) return(error_illegal_parameter(reply, replylength));

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_security_level
  FUNCTION DETAILS  : Handler for set_security_level message.
********************************************************************************/

int fn_set_security_level(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_security_level* m;
	//connection *c = (connection *)conn;;

	m = (struct msg_set_security_level*)msg;

	*msglength = offsetof(struct msg_set_security_level, end);

	system_set_security(m->security);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_security_level
  FUNCTION DETAILS  : Handler for read_security_level message.
********************************************************************************/

int fn_read_security_level(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	//struct msg_read_security_level *m;
	struct msg_return_security_level* r;
	//connection *c = (connection *)conn;

	//m = (struct msg_read_security_level *)msg;
	r = (struct msg_return_security_level*)reply;

	*msglength = offsetof(struct msg_read_security_level, end);
	*replylength = offsetof(struct msg_return_security_level, end);

	r->command = MSG_RETURN_SECURITY_LEVEL;
	r->security = system_get_security();

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_semaphore
  FUNCTION DETAILS  : Handler for set_semaphore message.
********************************************************************************/

int fn_set_semaphore(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_semaphore* m;

	m = (struct msg_set_semaphore*)msg;

	*msglength = offsetof(struct msg_set_semaphore, end);

	if (system_set_semaphore((connection*)conn, m->semaphore) != NO_ERROR) {
		return(error_semaphore_denied(reply, replylength));
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_semaphore
  FUNCTION DETAILS  : Handler for read_semaphore message.
********************************************************************************/

int fn_read_semaphore(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_semaphore* r;

	r = (struct msg_return_semaphore*)reply;

	*msglength = offsetof(struct msg_read_semaphore, end);
	*replylength = offsetof(struct msg_return_semaphore, end);

	r->command = MSG_RETURN_SEMAPHORE;
	r->semaphore = (system_get_semaphore((connection*)conn) == SEMAPHORE_VALID) ? 1 : 0;

	return(NO_ERROR);
}



#if (defined(CONTROLCUBE) || defined(AICUBE))

/********************************************************************************
  FUNCTION NAME   	: fn_set_kinet_mapping
  FUNCTION DETAILS  : Handler for set_kinet_mapping message.
********************************************************************************/

int fn_set_kinet_mapping(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	// TODO: no kinet support
	//struct msg_set_kinet_mapping* m;

	//m = (struct msg_set_kinet_mapping*)msg;

	//*msglength = offsetof(struct msg_set_kinet_mapping, end);

	//if (!kinetptr) return(error_illegal_function(reply, replylength));

	///* Check for legal KiNet and CNet slot numbers */

	//if ((m->kinet_slot > 127) || ((m->cnet_slot > 255) && (m->cnet_slot < 0xFFFFFFFFU)))
	//	return(error_illegal_parameter(reply, replylength));

	//if (m->cnet_slot == 0xFFFFFFFFU) {
	//	kinetptr->unmapslot(m->kinet_slot);
	//}
	//else {
	//	kinetptr->mapslot(m->kinet_slot, m->cnet_slot, m->flags, m->scale);
	//}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_kinet_mapping
  FUNCTION DETAILS  : Handler for read_kinet_mapping message.
********************************************************************************/

int fn_read_kinet_mapping(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	// TODO: no kinet support
	//struct msg_read_kinet_mapping* m;
	//struct msg_return_kinet_mapping* r;

	//m = (struct msg_read_kinet_mapping*)msg;
	//r = (struct msg_return_kinet_mapping*)reply;

	//*msglength = offsetof(struct msg_read_kinet_mapping, end);

	//if (!kinetptr) return(error_illegal_function(reply, replylength));

	//if (m->kinet_slot > 127) return(error_illegal_parameter(reply, replylength));

	//*replylength = offsetof(struct msg_return_kinet_mapping, end);

	//r->command = MSG_RETURN_KINET_MAPPING;
	//kinetptr->readslotmap(m->kinet_slot, &r->cnet_slot, &r->flags, &r->scale);

	return(NO_ERROR);
}

#endif



/********************************************************************************
  FUNCTION NAME   	: fn_set_cnet_maptable
  FUNCTION DETAILS  : Handler for set_cnet_maptable message.
********************************************************************************/

int fn_set_cnet_maptable(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_cnet_maptable* m;
	uint32_t err;

	m = (struct msg_set_cnet_maptable*)msg;

	*msglength = offsetof(struct msg_set_cnet_maptable, end);

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

#if 0
	if ((hyd_get_status() & HYD_STATUS_MASK) >= HYD_ACK_ON) {

		/* Operation illegal whilst hydraulics on */

		return(error_operation_failed(reply, replylength, HYDRAULICS_ACTIVE));
	}
#endif

	cnet_config_map(m->map);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_cnet_maptable
  FUNCTION DETAILS  : Handler for read_cnet_maptable message.
********************************************************************************/

int fn_read_cnet_maptable(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_cnet_maptable* r;

	r = (struct msg_return_cnet_maptable*)reply;

	*msglength = offsetof(struct msg_read_cnet_maptable, end);
	*replylength = offsetof(struct msg_return_cnet_maptable, end);

	r->command = MSG_RETURN_CNET_MAPTABLE;
	memcpy(r->map, cnet_usr_maptable, sizeof(cnet_usr_maptable));

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_cnet_timeout
  FUNCTION DETAILS  : Handler for set_cnet_timeout message.
********************************************************************************/

int fn_set_cnet_timeout(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_cnet_timeout* m;

	m = (struct msg_set_cnet_timeout*)msg;

	*msglength = offsetof(struct msg_set_cnet_timeout, end);

	cnet_set_timeout(m->timeout);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_cnet_timeout
  FUNCTION DETAILS  : Handler for read_cnet_timeout message.
********************************************************************************/

int fn_read_cnet_timeout(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_cnet_timeout* r;

	r = (struct msg_return_cnet_timeout*)reply;

	*msglength = offsetof(struct msg_read_cnet_timeout, end);
	*replylength = offsetof(struct msg_return_cnet_timeout, end);

	r->command = MSG_RETURN_CNET_TIMEOUT;
	r->timeout = cnet_read_timeout();

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_reset_parameters
  FUNCTION DETAILS  : Handler for set_reset_parameters message.
********************************************************************************/

int fn_set_reset_parameters(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_reset_parameters* m;

	m = (struct msg_set_reset_parameters*)msg;

	*msglength = offsetof(struct msg_set_reset_parameters, end);

	set_reset_parameters(m->flags, m->time, m->peak_count, MIRROR);
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_reset_parameters
  FUNCTION DETAILS  : Handler for read_reset_parameters message.
********************************************************************************/

int fn_read_reset_parameters(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_reset_parameters* r;

	r = (struct msg_return_reset_parameters*)reply;

	*msglength = offsetof(struct msg_read_reset_parameters, end);
	*replylength = offsetof(struct msg_return_reset_parameters, end);

	r->command = MSG_RETURN_RESET_PARAMETERS;
	r->peak_count = snvbs->pk_rst_count;
	r->flags = snvbs->autorst_flags;
	r->time = snvbs->autorst_time;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_peak_threshold
  FUNCTION DETAILS  : Handler for set_peak_threshold message.
********************************************************************************/

int fn_set_peak_threshold(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_peak_threshold* m;
	float threshold;
	int err = FALSE;

	m = (struct msg_set_peak_threshold*)msg;

	*msglength = offsetof(struct msg_set_peak_threshold, end);

	threshold = m->threshold;

	/* Clip threshold to be within 0.001% to 100%. Note that the clipping is
	   performed using normalised values.
	*/

	if (threshold < 0.00001F) { err = TRUE; threshold = 0.00001F; }
	if (threshold > 1.0F) { err = TRUE; threshold = 1.0F; }

	if (m->flags & 0x80000000) {

		/* Use extended message form */

		chandef* c;
		c = chanptr(m->flags & 0x3FF);

		if (!c) {

			/* Illegal channel - build error response and return error code */

			return(error_illegal_channel(reply, replylength, m->flags & 0x3FF));
		}

		if (c->setpkthreshold) {
			c->setpkthreshold(c, m->flags & 0x40000000, threshold, MIRROR);
			CtrlUpdateThreshold(m->flags & 0x3FF, threshold);
			return(err ? error_illegal_parameter(reply, replylength) : NO_ERROR);
		}

		/* Illegal function - build error response and return error code */

		return(error_illegal_function(reply, replylength));
	}
	else {

		/* Use standard message form */

		set_peak_threshold(m->flags, threshold, MIRROR);

		/* Need to inform KO code of the new threshold value for all channels, so just call
		   the initialise function to loop around all channels.
		*/

		chan_initialise_threshold();
	}
	return(err ? error_illegal_parameter(reply, replylength) : NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_peak_threshold
  FUNCTION DETAILS  : Handler for read_peak_threshold message.
********************************************************************************/

int fn_read_peak_threshold(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_peak_threshold* r;

	r = (struct msg_return_peak_threshold*)reply;

	*msglength = offsetof(struct msg_read_peak_threshold, end);
	*replylength = offsetof(struct msg_return_peak_threshold, end);

	r->command = MSG_RETURN_PEAK_THRESHOLD;
	r->flags = 0;
	r->threshold = snvbs->cycle_window;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_peak_threshold_ext
  FUNCTION DETAILS  : Handler for read_peak_threshold (extended) message.
********************************************************************************/

int fn_read_peak_threshold_ext(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_peak_threshold_ext* m;
	struct msg_return_peak_threshold_ext* r;
	chandef* c;

	m = (struct msg_read_peak_threshold_ext*)msg;
	r = (struct msg_return_peak_threshold_ext*)reply;

	*msglength = offsetof(struct msg_read_peak_threshold_ext, end);
	*replylength = offsetof(struct msg_return_peak_threshold_ext, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->readpkthreshold) {
		c->readpkthreshold(c, &r->flags, &r->threshold);
		r->command = MSG_RETURN_PEAK_THRESHOLD_EXT;
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_peak_timeout
  FUNCTION DETAILS  : Handler for set_peak_timeout message.
********************************************************************************/

int fn_set_peak_timeout(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_peak_timeout* m;

	m = (struct msg_set_peak_timeout*)msg;

	*msglength = offsetof(struct msg_set_peak_timeout, end);

	set_peak_timeout(m->timeout, MIRROR);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_peak_timeout
  FUNCTION DETAILS  : Handler for read_peak_timeout message.
********************************************************************************/

int fn_read_peak_timeout(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_peak_timeout* r;

	r = (struct msg_return_peak_timeout*)reply;

	*msglength = offsetof(struct msg_read_peak_timeout, end);
	*replylength = offsetof(struct msg_return_peak_timeout, end);

	r->command = MSG_RETURN_PEAK_TIMEOUT;
	read_peak_timeout(&r->timeout);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_chan_memptrs
  FUNCTION DETAILS  : Handler for read_chan_memptrs message.
********************************************************************************/

int fn_read_chan_memptrs(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_chan_memptrs* m;
	struct msg_return_chan_memptrs* r;
	chandef* c;
	uint8_t* base;

	m = (struct msg_read_chan_memptrs*)msg;
	r = (struct msg_return_chan_memptrs*)reply;

	*msglength = offsetof(struct msg_read_chan_memptrs, end);
	*replylength = offsetof(struct msg_return_chan_memptrs, end);

	r->command = MSG_RETURN_CHAN_MEMPTRS;
	r->chan = m->chan;

	if (m->chan >= CHANTYPE_CNET) {
		if (m->chan > (CHANTYPE_CNET + 255)) {
			return(error_illegal_channel(reply, replylength, m->chan));
		}
		else {
			r->value = (intptr_t)cnet_get_slotaddr(m->chan, PROC_DSPA);	/* Get DSPA CNet address */
			r->filtered = (intptr_t)cnet_get_slotaddr(m->chan, PROC_DSPB);	/* Get DSPB CNet address */
		}
	}
	else {
		c = chanptr(m->chan);
		if (!c) {

			/* Illegal channel - build error response and return error code */

			return(error_illegal_channel(reply, replylength, m->chan));
		}
		base = get_channel_base(c);
		r->value = (intptr_t)(base + offsetof(struct chandef, value));
		r->filtered = (intptr_t)(base + offsetof(struct chandef, filtered));
	}

	return(NO_ERROR);
}



#if (defined(CONTROLCUBE) || defined(AICUBE))

/********************************************************************************
  FUNCTION NAME   	: fn_set_kinet_flags
  FUNCTION DETAILS  : Handler for set_kinet_flags message.
********************************************************************************/

int fn_set_kinet_flags(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	// TODO: no kinet support
	//struct msg_set_kinet_flags* m;

	//m = (struct msg_set_kinet_flags*)msg;

	//*msglength = offsetof(struct msg_set_kinet_flags, end);

	//if (!kinetptr) return(error_illegal_function(reply, replylength));

	//kinetptr->setmode(m->flags);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_kinet_flags
  FUNCTION DETAILS  : Handler for read_kinet_flags message.
********************************************************************************/

int fn_read_kinet_flags(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	// TODO: no kinet support
	//struct msg_return_kinet_flags* r;

	//r = (struct msg_return_kinet_flags*)reply;

	//*msglength = offsetof(struct msg_read_kinet_flags, end);

	//if (!kinetptr) return(error_illegal_function(reply, replylength));

	//*replylength = offsetof(struct msg_return_kinet_flags, end);

	//r->command = MSG_RETURN_KINET_FLAGS;
	//r->flags = kinetptr->getmode();

	return(NO_ERROR);
}

#endif



/********************************************************************************
  FUNCTION NAME   	: fn_send_parameter_string
  FUNCTION DETAILS  : Handler for send_parameter_string message.
********************************************************************************/

int fn_send_parameter_string(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_send_parameter_string* m;
	struct msg_return_parameter_string* r;
	char* p;

	m = (struct msg_send_parameter_string*)msg;
	r = (struct msg_return_parameter_string*)reply;

	*msglength = offsetof(struct msg_send_parameter_string, end);
	*replylength = offsetof(struct msg_return_parameter_string, end);

	r->command = MSG_RETURN_PARAMETER_STRING;

	m->string[255] = '\0';	/* Force terminator onto end of input string */

	CtrlCmd(m->string, 0);  /* Pass to Karen's code */
	if (p = CtrlRspStr()) {
		strncpy(r->string, p, sizeof(r->string));
	}
	else {
		r->string[0] = '\0';	/* Illegal command, so return empty string */
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_nv_state
  FUNCTION DETAILS  : Handler for read_nv_state message.
********************************************************************************/

int fn_read_nv_state(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_nv_state* r;

	r = (struct msg_return_nv_state*)reply;

	*msglength = offsetof(struct msg_read_nv_state, end);

	*replylength = offsetof(struct msg_return_nv_state, end);

	r->command = MSG_RETURN_NV_STATE;
	r->state = (slot_nv_status & FLAG_NVSTATUS) | nv_get_status();

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_panel_config
  FUNCTION DETAILS  : Handler for set_panel_config message.
********************************************************************************/

int fn_set_panel_config(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_panel_config* m;
	int err;

	m = (struct msg_set_panel_config*)msg;

	*msglength = offsetof(struct msg_set_panel_config, end);

	if (err = dsp_1w_writepage(PAGE_PANELCFG, m->paneltable, 31, WR_CHKSUM)) {
		return(error_operation_failed(reply, replylength, err));
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_panel_config
  FUNCTION DETAILS  : Handler for read_panel_config message.
********************************************************************************/

int fn_read_panel_config(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_panel_config* r;
	uint8_t config[31];

	r = (struct msg_return_panel_config*)reply;

	*msglength = offsetof(struct msg_read_panel_config, end);

	*replylength = offsetof(struct msg_return_panel_config, end);

	if (dsp_1w_readpage(PAGE_PANELCFG, (uint8_t*)&config, sizeof(config), CHK_CHKSUM)) {
		memset(config, 0, sizeof(config));
	}

	r->command = MSG_RETURN_PANEL_CONFIG;
	memcpy(r->paneltable, config, sizeof(r->paneltable));

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_nv_control
  FUNCTION DETAILS  : Handler for nv_control message.
********************************************************************************/

int fn_nv_control(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_nv_control* m;

	m = (struct msg_nv_control*)msg;

	*msglength = offsetof(struct msg_nv_control, end);

	switch (m->ctrl) {
	case 0:
		nv_modify_status(0, m->flags);
		break;
	case 1:
		nv_modify_status(m->flags, 0);
		break;
	default:
		break;
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_digio_info
  FUNCTION DETAILS  : Handler for set_digio_info message.
********************************************************************************/

int fn_set_digio_info(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_digio_info* m;
	chandef* c;

	*msglength = offsetof(struct msg_set_digio_info, end);

	m = (struct msg_set_digio_info*)msg;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_io_info) {
		c->set_io_info(c, m->flags, m->name, m->state0, m->state1, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_digio_info
  FUNCTION DETAILS  : Handler for read_digio_info message.
********************************************************************************/

int fn_read_digio_info(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_digio_info* m;
	struct msg_return_digio_info* r;
	chandef* c;

	*msglength = offsetof(struct msg_read_digio_info, end);
	*replylength = offsetof(struct msg_return_digio_info, end);

	m = (struct msg_read_digio_info*)msg;
	r = (struct msg_return_digio_info*)reply;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_io_info) {
		c->read_io_info(c, &r->flags, r->name, r->state0, r->state1);
		r->command = MSG_RETURN_DIGIO_INFO;
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_write_digio_output
  FUNCTION DETAILS  : Handler for write_digio_output message.
********************************************************************************/

int fn_write_digio_output(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_write_digio_output* m;
	chandef* c;

	m = (struct msg_write_digio_output*)msg;

	*msglength = offsetof(struct msg_write_digio_output, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->write_io_output) {
		c->write_io_output(c, m->output);
		return(NO_ERROR);
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_digio_input
  FUNCTION DETAILS  : Handler for read_digio_input message.
********************************************************************************/

int fn_read_digio_input(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_digio_input* m;
	struct msg_return_digio_input* r;
	chandef* c;

	*msglength = offsetof(struct msg_read_digio_input, end);
	*replylength = offsetof(struct msg_return_digio_input, end);

	m = (struct msg_read_digio_input*)msg;
	r = (struct msg_return_digio_input*)reply;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_io_input) {
		c->read_io_input(c, &r->input);
		r->command = MSG_RETURN_DIGIO_INPUT;
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_digio_output
  FUNCTION DETAILS  : Handler for read_digio_output message.
********************************************************************************/

int fn_read_digio_output(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_digio_output* m;
	struct msg_return_digio_output* r;
	chandef* c;

	*msglength = offsetof(struct msg_read_digio_output, end);
	*replylength = offsetof(struct msg_return_digio_output, end);

	m = (struct msg_read_digio_output*)msg;
	r = (struct msg_return_digio_output*)reply;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->read_io_output) {
		c->read_io_output(c, &r->output);
		r->command = MSG_RETURN_DIGIO_OUTPUT;
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_chan_override
  FUNCTION DETAILS  : Handler for chan_override message.
********************************************************************************/

int fn_chan_override(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	uint32_t err;
	struct msg_chan_override* m;
	chandef* c;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(error_operation_failed(reply, replylength, err));

	m = (struct msg_chan_override*)msg;

	*msglength = offsetof(struct msg_chan_override, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->set_source) {
		c->set_source(c, NULL);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_eventlog_status
  FUNCTION DETAILS  : Handler for read_eventlog_status message.
********************************************************************************/

int fn_read_eventlog_status(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	//struct msg_read_eventlog_status *m;
	struct msg_return_eventlog_status* r;

	*msglength = offsetof(struct msg_read_eventlog_status, end);
	*replylength = offsetof(struct msg_return_eventlog_status, end);

	//m = (struct msg_read_eventlog_status *)msg;
	r = (struct msg_return_eventlog_status*)reply;

	eventlog_status(&r->size);
	r->command = MSG_RETURN_EVENTLOG_STATUS;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_flush_eventlog
  FUNCTION DETAILS  : Handler for flush_eventlog message.
********************************************************************************/

int fn_flush_eventlog(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	//struct msg_flush_eventlog *m;

	//m = (struct msg_flush_eventlog *)msg;

	*msglength = offsetof(struct msg_flush_eventlog, end);

	eventlog_flush();

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_eventlog_entry
  FUNCTION DETAILS  : Handler for read_eventlog_entry message.
********************************************************************************/

int fn_read_eventlog_entry(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	//struct msg_read_eventlog_entry *m;
	struct msg_return_eventlog_entry* r;
	eventlog_entry entry;

	*msglength = offsetof(struct msg_read_eventlog_entry, end);
	*replylength = offsetof(struct msg_return_eventlog_entry, end);

	//m = (struct msg_read_eventlog_entry *)msg;
	r = (struct msg_return_eventlog_entry*)reply;

	r->command = MSG_RETURN_EVENTLOG_ENTRY;
	if (eventlog_read(&entry)) {
		r->status = NO_ERROR;
		memcpy(&r->timestamp_lo, &entry, sizeof(eventlog_entry));
	}
	else {
		r->status = EVENTLOG_EMPTY;
	}
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_timestamp
  FUNCTION DETAILS  : Handler for read_timestamp message.
********************************************************************************/

int fn_read_timestamp(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	//struct msg_read_timestamp *m;
	struct msg_return_timestamp* r;

	*msglength = offsetof(struct msg_read_timestamp, end);
	*replylength = offsetof(struct msg_return_timestamp, end);

	//m = (struct msg_read_timestamp *)msg;
	r = (struct msg_return_timestamp*)reply;

	r->command = MSG_RETURN_TIMESTAMP;
	cnet_read_timestamp(&r->timestamp_lo, &r->timestamp_hi);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_userdata
  FUNCTION DETAILS  : Handler for set_userdata.
********************************************************************************/

int fn_set_userdata(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_userdata* m = (struct msg_set_userdata*)msg;

	*msglength = offsetof(struct msg_set_userdata, data) + m->length;

	if (m->length > sizeof(snvbs->user_data)) {
		return(error_illegal_parameter(reply, replylength));
	}

	if (nv_setdata(m->start, m->length, (char*)m->data)) {
		return(error_illegal_parameter(reply, replylength));
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_userdata
  FUNCTION DETAILS  : Handler for read_userdata message.
********************************************************************************/

int fn_read_userdata(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_userdata* m;
	struct msg_return_userdata* r;

	m = (struct msg_read_userdata*)msg;
	r = (struct msg_return_userdata*)reply;

	*msglength = offsetof(struct msg_read_userdata, end);
	*replylength = offsetof(struct msg_return_userdata, data) + m->length;

	if (m->length > sizeof(snvbs->user_data)) {
		return(error_illegal_parameter(reply, replylength));
	}

	if (nv_readdata(m->start, m->length, (char*)r->data)) {
		return(error_illegal_parameter(reply, replylength));
	}

	r->command = MSG_RETURN_USERDATA;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_chanstring
  FUNCTION DETAILS  : Handler for set_chanstring.
********************************************************************************/

int fn_set_chanstring(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_chanstring* m;
	chandef* c;

	m = (struct msg_set_chanstring*)msg;

	*msglength = offsetof(struct msg_set_chanstring, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (m->length > sizeof(m->string)) {
		return(error_illegal_parameter(reply, replylength));
	}

	c->write_userstring(c, m->start, m->length, m->string, MIRROR);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_chanstring
  FUNCTION DETAILS  : Handler for read_chanstring message.
********************************************************************************/

int fn_read_chanstring(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_chanstring* m;
	struct msg_return_chanstring* r;
	chandef* c;

	*msglength = offsetof(struct msg_read_chanstring, end);
	*replylength = offsetof(struct msg_return_chanstring, end);

	m = (struct msg_read_chanstring*)msg;
	r = (struct msg_return_chanstring*)reply;

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (m->length > sizeof(r->string)) {
		return(error_illegal_parameter(reply, replylength));
	}

	c->read_userstring(c, m->start, m->length, r->string);

	r->command = MSG_RETURN_CHANSTRING;
	return(NO_ERROR);
}



#ifdef SIGNALCUBE

/********************************************************************************
  FUNCTION NAME   	: fn_set_gauge_type
  FUNCTION DETAILS  : Handler for set_gauge_type message.
********************************************************************************/

int fn_set_gauge_type(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_gauge_type* m;
	chandef* c;
	uint32_t type;

	m = (struct msg_set_gauge_type*)msg;

	*msglength = offsetof(struct msg_set_gauge_type, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setgaugetype) {
		type = c->readgaugetype(c) & ACTIVE_MASK;	/* Retain active gauge count */
		type |= (m->type & ~ACTIVE_MASK);
		c->setgaugetype(c, type, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_gauge_type
  FUNCTION DETAILS  : Handler for read_gauge_type message.
********************************************************************************/

int fn_read_gauge_type(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_gauge_type* m;
	struct msg_return_gauge_type* r;
	chandef* c;

	m = (struct msg_read_gauge_type*)msg;
	r = (struct msg_return_gauge_type*)reply;

	*msglength = offsetof(struct msg_read_gauge_type, end);
	*replylength = offsetof(struct msg_return_gauge_type, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->readgaugetype) {
		r->command = MSG_RETURN_GAUGE_TYPE;
		r->chan = m->chan;
		r->type = c->readgaugetype(c) & ~ACTIVE_MASK;	/* Remove active gauge count */
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_active_gauges
  FUNCTION DETAILS  : Handler for set_active_gauges message.
********************************************************************************/

int fn_set_active_gauges(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_active_gauges* m;
	chandef* c;
	uint32_t type;

	m = (struct msg_set_active_gauges*)msg;

	*msglength = offsetof(struct msg_set_active_gauges, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setgaugetype) {
		type = c->readgaugetype(c) & ~ACTIVE_MASK;
		type |= (((m->gauges - 1) << ACTIVE_SHIFT) & ACTIVE_MASK);
		c->setgaugetype(c, type, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_active_gauges
  FUNCTION DETAILS  : Handler for read_active_gauges message.
********************************************************************************/

int fn_read_active_gauges(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_active_gauges* m;
	struct msg_return_active_gauges* r;
	chandef* c;

	m = (struct msg_read_active_gauges*)msg;
	r = (struct msg_return_active_gauges*)reply;

	*msglength = offsetof(struct msg_read_active_gauges, end);
	*replylength = offsetof(struct msg_return_active_gauges, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->readgaugetype) {
		r->command = MSG_RETURN_ACTIVE_GAUGES;
		r->chan = m->chan;
		r->gauges = ((c->readgaugetype(c) & ACTIVE_MASK) >> ACTIVE_SHIFT) + 1;
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_gauge_info
  FUNCTION DETAILS  : Handler for set_gauge_info message.
********************************************************************************/

int fn_set_gauge_info(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_gauge_info* m;
	chandef* c;
	//uint32_t type;

	m = (struct msg_set_gauge_info*)msg;

	*msglength = offsetof(struct msg_set_gauge_info, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setgaugeinfo) {
		c->setgaugeinfo(c, m->sensitivity, m->gauge_factor, m->bridge_factor, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_gauge_info
  FUNCTION DETAILS  : Handler for read_gauge_info message.
********************************************************************************/

int fn_read_gauge_info(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_gauge_info* m;
	struct msg_return_gauge_info* r;
	chandef* c;

	m = (struct msg_read_gauge_info*)msg;
	r = (struct msg_return_gauge_info*)reply;

	*msglength = offsetof(struct msg_read_gauge_info, end);
	*replylength = offsetof(struct msg_return_gauge_info, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->readgaugeinfo) {
		r->command = MSG_RETURN_GAUGE_INFO;
		r->chan = m->chan;
		c->readgaugeinfo(c, &r->sensitivity, &r->gauge_factor, &r->bridge_factor);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}

#endif /* SIGNALCUBE */



/********************************************************************************
  FUNCTION NAME   	: fn_set_offset
  FUNCTION DETAILS  : Handler for set_offset message.
********************************************************************************/

int fn_set_offset(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_offset* m;
	chandef* c;

	m = (struct msg_set_offset*)msg;

	*msglength = offsetof(struct msg_set_offset, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setoffset) {
		c->setoffset(c, m->offset, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_offset
  FUNCTION DETAILS  : Handler for read_offset message.
********************************************************************************/

int fn_read_offset(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_offset* m;
	struct msg_return_offset* r;
	chandef* c;

	m = (struct msg_read_offset*)msg;
	r = (struct msg_return_offset*)reply;

	*msglength = offsetof(struct msg_read_offset, end);
	*replylength = offsetof(struct msg_return_offset, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->readoffset) {
		r->command = MSG_RETURN_OFFSET;
		r->chan = m->chan;
		r->offset = c->readoffset(c);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



#ifdef SIGNALCUBE

/********************************************************************************
  FUNCTION NAME   	: fn_start_zero
  FUNCTION DETAILS  : Handler for start_zero message.
********************************************************************************/

int fn_start_zero(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_start_zero* m;
	chandef* c;
	//uint32_t err;

	m = (struct msg_start_zero*)msg;

	*msglength = offsetof(struct msg_start_zero, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->startzero) {
		if (c->startzero(c)) return(error_operation_failed(reply, replylength, OPERATION_BUSY));
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_zero_progress
  FUNCTION DETAILS  : Handler for read_zero_progress message.
********************************************************************************/

int fn_read_zero_progress(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_zero_progress* m;
	struct msg_return_zero_progress* r;
	chandef* c;

	m = (struct msg_read_zero_progress*)msg;
	r = (struct msg_return_zero_progress*)reply;

	*msglength = offsetof(struct msg_read_zero_progress, end);
	*replylength = offsetof(struct msg_return_zero_progress, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	r->command = MSG_RETURN_ZERO_PROGRESS;
	r->chan = m->chan;
	r->state = (c->zerostate) ? OPERATION_BUSY : 0;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_calzero_entry
  FUNCTION DETAILS  : Handler for set_calzero_entry message.
********************************************************************************/

int fn_set_calzero_entry(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_set_calzero_entry* m;
	chandef* c;

	m = (struct msg_set_calzero_entry*)msg;

	*msglength = offsetof(struct msg_set_calzero_entry, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->setcalzeroentry) {
		c->setcalzeroentry(c, m->entry, m->daczero, m->zero, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_calzero_entry
  FUNCTION DETAILS  : Handler for read_calzero_entry message.
********************************************************************************/

int fn_read_calzero_entry(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_calzero_entry* m;
	struct msg_return_calzero_entry* r;
	chandef* c;

	m = (struct msg_read_calzero_entry*)msg;
	r = (struct msg_return_calzero_entry*)reply;

	*msglength = offsetof(struct msg_read_calzero_entry, end);
	*replylength = offsetof(struct msg_return_calzero_entry, end);

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->readcalzeroentry) {
		r->command = MSG_RETURN_CALZERO_ENTRY;
		r->chan = m->chan;
		c->readcalzeroentry(c, m->entry, &r->daczero, &r->zero);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));
}



#endif /* SIGNALCUBE */



/********************************************************************************
  FUNCTION NAME   	: fn_open_event_connection
  FUNCTION DETAILS  : Handler for open_event_connection message.
********************************************************************************/

int fn_open_event_connection(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_open_event_connection* m;
	struct msg_return_event_connection* r;
	int handle;

	m = (struct msg_open_event_connection*)msg;
	r = (struct msg_return_event_connection*)reply;

	*msglength = offsetof(struct msg_open_event_connection, end);
	*replylength = offsetof(struct msg_return_event_connection, end);

	if (handle = event_open_connection(conn, m->ipaddr, m->port, m->handle, m->update)) {
		r->command = MSG_RETURN_EVENT_CONNECTION;
		r->handle = handle;
		return(NO_ERROR);
	}
	else {
		return(error_operation_failed(reply, replylength, EVENT_NOCONNECT));
	}
}



/********************************************************************************
  FUNCTION NAME   	: fn_close_event_connection
  FUNCTION DETAILS  : Handler for close_event_connection message.
********************************************************************************/

int fn_close_event_connection(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_close_event_connection* m;

	m = (struct msg_close_event_connection*)msg;

	*msglength = offsetof(struct msg_close_event_connection, end);

	event_close_connection(m->handle);

	return(NO_ERROR);
}


/********************************************************************************
  FUNCTION NAME   	: fn_add_event_report
  FUNCTION DETAILS  : Handler for add_event_report message.
********************************************************************************/

int fn_add_event_report(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_add_event_report* m;

	m = (struct msg_add_event_report*)msg;

	/* Calculate actual message size based on variable data size */

	*msglength = offsetof(struct msg_add_event_report, data) + m->size;

	event_add_report(m->handle, m->priority, m->event, m->size, (m->size) ? &m->data[0] : NULL);
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_remove_event_report
  FUNCTION DETAILS  : Handler for remove_event_report message.
********************************************************************************/

int fn_remove_event_report(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_remove_event_report* m;

	m = (struct msg_remove_event_report*)msg;

	*msglength = offsetof(struct msg_remove_event_report, end);

	event_remove_report(m->handle, m->event);
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_set_fault_mask
  FUNCTION DETAILS  : Handler for set_fault_mask message.
********************************************************************************/

int fn_set_fault_mask(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	struct msg_set_fault_mask* m;
	chandef* c;

	m = (struct msg_set_fault_mask*)msg;

#endif

	* msglength = offsetof(struct msg_set_fault_mask, end);

#if (defined(CONTROLCUBE) || defined(AICUBE))

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}

	if (c->writefaultmask) {
		c->writefaultmask(c, m->mask, MIRROR);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));

#else

	return(NO_ERROR);

#endif
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_fault_state
  FUNCTION DETAILS  : Handler for read_fault_state message.
********************************************************************************/

int fn_read_fault_state(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_read_fault_state* m;
	struct msg_return_fault_state* r;

#if (defined(CONTROLCUBE) || defined(AICUBE))

	chandef* c;

#endif

	m = (struct msg_read_fault_state*)msg;
	r = (struct msg_return_fault_state*)reply;

	*msglength = offsetof(struct msg_read_fault_state, end);
	*replylength = offsetof(struct msg_return_fault_state, end);

#if (defined(CONTROLCUBE) || defined(AICUBE))

	c = chanptr(m->chan);

	if (!c) {

		/* Illegal channel - build error response and return error code */

		return(error_illegal_channel(reply, replylength, m->chan));
	}
	if (c->readfaultstate) {
		r->command = MSG_RETURN_FAULT_STATE;
		r->chan = m->chan;
		r->state = c->readfaultstate(c, &r->capabilities, &r->mask);
		return(NO_ERROR);
	}

	/* Illegal function - build error response and return error code */

	return(error_illegal_function(reply, replylength));

#else

	r->command = MSG_RETURN_FAULT_STATE;
	r->chan = m->chan;
	r->state = 0;
	r->capabilities = 0;
	r->mask = 0;

	return(NO_ERROR);

#endif
}



#if (defined(CONTROLCUBE) || defined(AICUBE))
extern void* shared_memory;

#define HC_INSTR	0x2C	/* DSP B address of hand controller instruction word */
#define HC_KEYSTATE 0x30	/* DSP B address of hand controller key state word	 */
#define HC_STATUS 	0x34	/* DSP B address of hand controller status word	 	 */

/********************************************************************************
  FUNCTION NAME   	: fn_send_hc_instruction
  FUNCTION DETAILS  : Handler for send_hc_instruction message.
********************************************************************************/

int fn_send_hc_instruction(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_send_hc_instruction* m;

	*msglength = offsetof(struct msg_send_hc_instruction, end);

	m = (struct msg_send_hc_instruction*)msg;

	//TODO: hand controller support
	//if (!hpi_read4((uint32_t*)HC_INSTR)) {
	//	hpi_write4((uint32_t*)HC_INSTR, (0x80000000 | m->instr));
	//}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_hc_keystate
  FUNCTION DETAILS  : Handler for read_hc_keystate message.
********************************************************************************/

int fn_read_hc_keystate(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_hc_keystate* r;

	r = (struct msg_return_hc_keystate*)reply;

	*msglength = offsetof(struct msg_read_hc_keystate, end);
	*replylength = offsetof(struct msg_return_hc_keystate, end);

	r->command = MSG_RETURN_HC_KEYSTATE;
	// TODO: hand controller support
//	r->keystate = (hpi_read4((uint32_t*)HC_KEYSTATE) & 0xFF) | (hpi_read4((uint32_t*)HC_STATUS) & (HC_PRESENTMASK | HC_UNPLUGGED));
	r->keystate = HC_UNPLUGGED;

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_guard_state
  FUNCTION DETAILS  : Handler for read_guard_state message.
********************************************************************************/

int fn_read_guard_state(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	struct msg_return_guard_state* r;

	r = (struct msg_return_guard_state*)reply;

	*msglength = offsetof(struct msg_read_guard_state, end);
	*replylength = offsetof(struct msg_return_guard_state, end);

	r->command = MSG_RETURN_GUARD_STATE;
	r->state = hw_get_guard_state();
	return(NO_ERROR);
}

#endif /* CONTROLCUBE */

/********************************************************************************
  FUNCTION NAME   	: fn_set_simulation_mode
  FUNCTION DETAILS  : Handler for set_simulation_mode message.
********************************************************************************/

int fn_set_simulation_mode(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
#if (defined(CONTROLCUBE) || defined(AICUBE))

	uint32_t err;
	struct msg_set_simulation_mode* m;
	uint32_t hyd_mode;
	uint32_t hyd_group;
	uint32_t hyd_type;

	m = (struct msg_set_simulation_mode*)msg;

	*msglength = offsetof(struct msg_set_simulation_mode, end);

	/* Read current hydraulic mode */

	hyd_read_mode(&hyd_mode, &hyd_group, &hyd_type);
	hyd_type = (hyd_type & ~HYD_SIM_MASK) | (m->enable ? HYD_SIM_MASK : 0);

	if (err = hyd_set_mode(hyd_mode, hyd_group, hyd_type))
		return(error_operation_failed(reply, replylength, err));

	return(NO_ERROR);

#else

	return(NO_ERROR);

#endif
}



/********************************************************************************
  FUNCTION NAME   	: fn_read_simulation_mode
  FUNCTION DETAILS  : Handler for read_simulation_mode message.
********************************************************************************/

int fn_read_simulation_mode(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	//struct msg_read_simulation_mode *m;
	struct msg_return_simulation_mode* r;
	uint32_t hyd_type;

	//m = (struct msg_read_simulation_mode *)msg;
	r = (struct msg_return_simulation_mode*)reply;

	*msglength = offsetof(struct msg_read_simulation_mode, end);
	*replylength = offsetof(struct msg_return_simulation_mode, end);

	r->command = MSG_RETURN_SIMULATION_MODE;

#if (defined(CONTROLCUBE) || defined(AICUBE))

	/* Read current hydraulic mode */

	hyd_read_mode(NULL, NULL, &hyd_type);
	r->enable = (hyd_type & HYD_SIM_MASK) ? 1 : 0;

#else

	r->enable = 0;

#endif

	return(NO_ERROR);
}



void runtimer_read(uint32_t* timer_lo, uint32_t* timer_hi);

/********************************************************************************
  FUNCTION NAME   	: fn_read_runtime
  FUNCTION DETAILS  : Handler for read_runtime message.
********************************************************************************/

int fn_read_runtime(uint8_t* msg, uint8_t* reply, uint32_t* msglength, uint32_t* replylength, void* conn)
{
	// TODO: read runtime?
	//struct msg_return_runtime* r;

	//r = (struct msg_return_runtime*)reply;

	//*msglength = offsetof(struct msg_read_runtime, end);
	//*replylength = offsetof(struct msg_return_runtime, end);

	//r->command = MSG_RETURN_RUNTIME;

	//runtimer_read(&r->runtime_lo, &r->runtime_hi);

	return(NO_ERROR);
}

