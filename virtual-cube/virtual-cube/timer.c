/********************************************************************************
  MODULE NAME   	: timer
  PROJECT			: Control Cube / Signal Cube
  MODULE DETAILS  	: Handles regular monitoring and updating of system
					  configuration.
********************************************************************************/
#define _GNU_SOURCE
#define _POSIX_C_SOURCE 199309
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdint.h>

#include <time.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>

#include "asmcode.h"
#include "cnet.h"
#include "msgserver.h"
#include "hydraulic.h"
#include "nonvol.h"

extern volatile uint32_t prd_enable;

uint32_t near timer_usage = 0;			/* Processor usage in current interrupt	*/
uint32_t near max_timer_usage = 0;		/* Maximum processor usage				*/

int event_background(void);
void hc_timer_support(void);
//extern void CtrlFastPoll();

//#define VALID_RUNTIME_CODE	0x43655473

static uint32_t runtime_lo;
static uint32_t runtime_hi;

static void* timer_thread(void* ptr);
static void timerintr(void);

/********************************************************************************
  FUNCTION NAME   	: runtimer_init
  FUNCTION DETAILS  : Initialise runtimer operation.
********************************************************************************/

void runtimer_init(void)
{
	int result;
	pthread_t timerThread;
#if (defined(CONTROLCUBE) || defined(AICUBE))
#ifndef LOADER
	nv_init_runtimer(&runtime_lo, &runtime_hi);

	int ret = pthread_create(&timerThread, NULL, &timer_thread, NULL);
	if (ret != 0)
		perror("runtimer_init");

#endif /* LOADER */
#endif
}



/********************************************************************************
  FUNCTION NAME   	: runtimer_read
  FUNCTION DETAILS  : Read current runtimer.
********************************************************************************/

void runtimer_read(uint32_t* timer_lo, uint32_t* timer_hi)
{
	uint32_t istate = disable_int();

#if (defined(CONTROLCUBE) || defined(AICUBE))

	if (timer_lo) *timer_lo = runtime_lo;
	if (timer_hi) *timer_hi = runtime_hi;

#else

	if (timer_lo) *timer_lo = 0;
	if (timer_hi) *timer_hi = 0;

#endif /* CONTROLCUBE */

	restore_int(istate);
}



/********************************************************************************
  FUNCTION NAME   	: timerintr
  FUNCTION DETAILS  : Performs general system monitoring functions. Called every
					  5ms by the PRD module.
********************************************************************************/

static void* timer_thread(void* ptr)
{
	for (;;)
	{
		usleep(5000);
		timerintr();
	}
}

static void timerintr(void)
{
	static uint32_t counter = 0;
	uint32_t timer_starttime;
	uint32_t timer_duration;
	uint32_t istate;

	/* Read initial timer value for processor usage calculation */

	//timer_starttime = TIMER_getCount(hTimer1);

	if (prd_enable == FALSE) return;

	/* The variable 'counter' counts from 0 to 19 before resetting. Tasks can be
	   performed at intervals between 5ms and 0.1s as required.
	*/

	if (++counter == 20) {
		counter = 0;

		/* Check for hardware restart request (after firmware load) */

		if (hw_restart_delay) {
			if (--hw_restart_delay == 0) {
				if (hw_restart_enable) {
					//hw_restart();	/* Call restart code */
				}
			}
		}

		istate = disable_int();
		if (++runtime_lo == 0) runtime_hi++;
#if (defined(CONTROLCUBE) || defined(AICUBE))
#ifndef LOADER
		nv_write_runtimer(runtime_lo, runtime_hi);
#endif
#endif
		restore_int(istate);
	}

#ifdef SIGNALCUBE
#ifndef LOADER    
	CtrlTSRPoll();	/* Perform TSR polling */
#endif
#endif

/* Spread the regular tasks over a number of interrupt calls */

	switch (counter & 0x03) {
	case 0:
#if (defined(CONTROLCUBE) || defined(AICUBE))
#ifndef LOADER  
		hyd_update(HYD_UPDATE);		/* Update hydraulic status */
#endif
#endif
		cnet_monitor();				/* Monitor CNet status */
		break;
	case 1:
#if (defined(CONTROLCUBE) || defined(AICUBE))
#ifndef LOADER  
		hyd_update(HYD_MONITOR);	/* Update hydraulic status */
		slot_safetyscan();			/* Perform transducer safety scan */
		hw_check_guard();			/* Perform guard status checks */
		grp_update_status();		/* Update group status flags */
#endif /* LOADER */
#endif /* CONTROLCUBE */
		timeout_handler();			/* Update timeout counters for message handlers */
		break;
	case 2:
#ifndef LOADER  
		nv_update(FALSE);			/* Update NV data */
#ifdef SIGNALCUBE
		process_zero();				/* Process background zero function	*/
#endif /* SIGNALCUBE */
#if (defined(CONTROLCUBE) || defined(AICUBE))
		hyd_update(HYD_UPDATE);		/* Update hydraulic status */
#endif /* CONTROLCUBE */
#endif /* LOADER */
		break;
	case 3:
		timeout_handler();			/* Update timeout counters for message handlers */
#ifndef LOADER
		hc_timer_support();
		event_background();
#endif /* LOADER */
#if (defined(CONTROLCUBE) || defined(AICUBE))
#ifndef LOADER  
		hyd_update(HYD_UPDATE);		/* Update hydraulic status */
#endif /* LOADER */
#endif /* CONTROLCUBE */
//	CtrlFastPoll();
		break;
	}

	/* Update processor time usage */

	//timer_duration = TIMER_getCount(hTimer1) - timer_starttime;
	//timer_usage = timer_duration;
	//if (timer_duration > max_timer_usage) max_timer_usage = timer_duration;
}
