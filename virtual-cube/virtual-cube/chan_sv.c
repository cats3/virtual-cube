/********************************************************************************
 * MODULE NAME       : chan_sv.c												*
 * PROJECT			 : Control Cube												*
 * MODULE DETAILS    : Routines to support a servo-valve output channel.		*
 ********************************************************************************/

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

#include <stdint.h>
#include "porting.h"

#include "defines.h"
 //#include "ccubecfg.h"
#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"
#include "asmcode.h"

#undef isnan
#define isnan(x)	(((*((uint32_t *)(&(x))))&0x7F800000)==0x7F800000)

/* Define fixed resistor values in servo-valve current circuit */

#define SV_R1		470.0
#define SV_RA		330.0
#define SV_R3		33000.0
#define SV_RB1		1000.0
#define SV_RB2		47.0

float calc_sv_max(float rp, int potset);

/******************************************************************
* List of entries in chandef structure that are saved into EEPROM *
*																  *
* Entries 0 to 3 must be compatible across all channel types.	  *
*																  *
******************************************************************/

/* Lists for servo-valve output channels */

static configsave chan_sv_eelist_type0[] = {
  {offsetof(chandef, offset), sizeof(int)},			/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},	/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},		/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},		/* Entry 3	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},	/* Entry 4	*/
  {0,0}
};

/* Type 1 list added user string area. */

static configsave chan_sv_eelist_type1[] = {
  {offsetof(chandef, offset), sizeof(int)},			/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},	/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},		/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},		/* Entry 3	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},	/* Entry 4	*/
  {offsetof(chandef, userstring), 128},				/* Entry 5 */
  {0,0}
};

#define CURRENT_CHAN_SV_EELIST_TYPE 1

static configsave* chan_sv_eelist[] = {
  chan_sv_eelist_type0,
  chan_sv_eelist_type1,
};



/********************************************************************************
  FUNCTION NAME     : chan_sv_initlist
  FUNCTION DETAILS  : Initialise configsave list for servo-valve output channel.
********************************************************************************/

void chan_sv_initlist(void)
{
	init_savelist(chan_sv_eelist[CURRENT_CHAN_SV_EELIST_TYPE]);
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_default
  FUNCTION DETAILS  : Load default values for a high-level ADC transducer
					  channel.
********************************************************************************/

void chan_sv_default(chandef* def, uint32_t chanid)
{
	cfg_sv* cfg;
	cal_sv* cal;
	int n;

	cfg = &def->cfg.i_sv;
	cal = &def->cal.u.c_sv;

	/* Define save information */

	def->eeformat = CURRENT_CHAN_SV_EELIST_TYPE;
	def->eesavelist = chan_sv_eelist[CURRENT_CHAN_SV_EELIST_TYPE];
	def->eesavetable = chan_sv_eelist;

	/* Build default calibration table */

	for (n = 0; n < 256; n++) {
		cal->cal_svitable[n] = calc_sv_max(10000.0, n);
	}

	/* Set default calibration gain and offset values */

	cal->cal_offset = 0;		/* Default voltage output offset 	*/
	cal->cal_gain = 1.0;		/* Default voltage output gain 		*/

	/* Define default SV type and current */

	cfg->svtype = SV_CURRENT;
	cfg->current = 0.0;
	cfg->balance = 0.0;
	cfg->dither_freq = 100.0;
	cfg->dither_ampl = 0.0;

	/* Define default gain/offset settings */

	cfg->flags = 0;
	cfg->gaintrim = 1.0;	/* Default gaintrim 			*/
	cfg->gain = 1.0;		/* Default gain 				*/

	/* Set reserved locations to zero */

	cfg->faultmask = 0;
	cfg->reserved1 = 0;
	cfg->reserved2 = 0;
	cfg->reserved3 = 0;

	def->pos_gaintrim = GAINTRIM_SCALE;
	def->neg_gaintrim = GAINTRIM_SCALE;
	def->offset = 0;

	/* Define default filter1 settings */

	def->filter1.type = 0;
	def->filter1.order = 0;
	def->filter1.freq = 0.0;
	def->filter1.bandwidth = 0.0;
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_set_pointer
  FUNCTION DETAILS  : Set the source pointer for the selected servo-valve
					  channel.

					  On entry:

					  def		Points to the channel definition structure
					  ptr		Address to set source pointer

********************************************************************************/

void chan_sv_set_pointer(chandef* def, int* ptr)
{
	def->sourceptr = ptr;

	update_shared_channel_parameter(def, offsetof(chandef, sourceptr), sizeof(float*));
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_get_pointer
  FUNCTION DETAILS  : Get the source pointer for the selected servo-valve
					  channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

int* chan_sv_get_pointer(chandef* def)
{
	return(def->sourceptr);
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_calc_gain_setting
  FUNCTION DETAILS  : Calculate the required gain setting for a given current
					  value.

					  On entry:

					  def		Points to the channel definition structure
					  type		Output type
									0 = Current
									1 = Voltage
					  current	Required current range

********************************************************************************/

void chan_sv_calc_gain_setting(chandef* def, uint32_t type, float current, int* iset,
	float* gaintrim, float* izero)
{
	cal_sv* cal;
	//cfg_sv *cfg;
	int n;
	int entry = 255;
	float trim;
	float i0 = 0.0F;

	cal = &def->cal.u.c_sv;	/* SV specific calibration		*/
	//cfg = &def->cfg.i_sv;	/* SV specific configuration	*/

	if (type == SV_CURRENT) {

		/* Find nearest higher current from available settings */

		entry = 0;
		for (n = 0; n < 256; n++) {
			if (cal->cal_svitable[n] >= current) entry = n;
		}

		trim = current / cal->cal_svitable[entry];

		/* Check if the cal_svitable has valid current offset values by checking for a magic
		   value in entry 0x1FF.
		*/

		if (!isnan(cal->cal_svitable[0x01FF]) && (cal->cal_svitable[0x01FF] >= 100000.0F)) {

			/* The current offset is expressed as a fraction of the fullscale current (i.e. the
			   current corresponding to the output current at the maximum DAC output).
			*/

			i0 = cal->cal_svitable[0x0100 + entry] / cal->cal_svitable[entry];
		}
	}
	else {
		trim = 1.0F / cal->cal_gain;
	}

	if (trim > 3.999) trim = 3.999;

	if (iset) *iset = entry;
	if (gaintrim) *gaintrim = trim;
	if (izero) *izero = i0;
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_set_gain
  FUNCTION DETAILS  : Set the gain for the selected servo-valve output channel.

					  On entry:

					  def		Points to the channel definition structure
					  gain		Required gain value
					  flags		Bits 2-0 : Reserved

********************************************************************************/

void chan_sv_set_gain(chandef* def, float gain, uint32_t flags, int mirror)
{
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_read_gain
  FUNCTION DETAILS  : Read the gain for the selected servo-valve output channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_sv_read_gain(chandef* def, float* gain, uint32_t* flags)
{
	//cfg_sv *cfg;

	//cfg = &def->cfg.i_sv;	/* SV specific configuration	*/

	if (gain) *gain = 1.0;
	if (flags) *flags = 0;
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_set_gaintrim
  FUNCTION DETAILS  : Set the gaintrim value for the selected servo-valve
					  channel.

					  On entry:

					  def			Points to the channel definition structure
					  pos_gaintrim	Pointer to required positive gaintrim value
					  neg_gaintrim	Pointer to required negative gaintrim value
					  calmode		Not used

********************************************************************************/

void chan_sv_set_gaintrim(chandef* def, float* pos_gaintrim, float* neg_gaintrim, int calmode, int mirror)
{
	cfg_sv* cfg;
	float trim;
	float ptrim;

	cfg = &def->cfg.i_sv;	/* SV specific configuration	*/

	if (pos_gaintrim) {
		ptrim = *pos_gaintrim;
		if (ptrim > 3.999) ptrim = 3.999;
		cfg->gaintrim = ptrim;
	}

	if (!calmode) {

		/* Re-calculate current gain value to get current hardware gain trim value */

		chan_sv_calc_gain_setting(def, cfg->svtype, cfg->current, NULL, &trim, NULL);

		/* Combine hardware trim with user trim to produce an overall trim value */

		ptrim = trim * ptrim;
	}

	if (ptrim > 3.999) ptrim = 3.999;

	/* Update fine software gain trim. */

	if (pos_gaintrim) {
		def->pos_gaintrim = (uint32_t)(ptrim * (float)GAINTRIM_SCALE);
		update_shared_channel_parameter(def, offsetof(chandef, pos_gaintrim), sizeof(int));
		if (mirror) mirror_chan_eeconfig(def, 1);		/* Mirror pos gaintrim value	*/
	}

	/* Mirror config to EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 4);		/* Mirror cfg area				*/
	}

}



/********************************************************************************
  FUNCTION NAME     : chan_sv_read_gaintrim
  FUNCTION DETAILS  : Read the gaintrim value for the selected servo-valve
					  output channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_sv_read_gaintrim(chandef* def, float* pos_gaintrim, float* neg_gaintrim)
{
	cfg_sv* cfg;

	cfg = &def->cfg.i_sv;	/* SV specific configuration	*/

	if (pos_gaintrim) *pos_gaintrim = cfg->gaintrim;
	if (neg_gaintrim) *neg_gaintrim = cfg->gaintrim;
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_set_caltable
  FUNCTION DETAILS  : Sets a calibration table entry for a servo-valve output
					  channel.
********************************************************************************/

void chan_sv_set_caltable(chandef* def, int entry, float value)
{
	cal_sv* cal;

	cal = &def->cal.u.c_sv;	/* SV specific calibration	*/
	cal->cal_svitable[entry] = value;
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_read_caltable
  FUNCTION DETAILS  : Reads a calibration table entry for a servo-valve output
					  channel.
********************************************************************************/

float chan_sv_read_caltable(chandef* def, int entry)
{
	cal_sv* cal;

	cal = &def->cal.u.c_sv;	/* SV specific calibration	*/
	return(cal->cal_svitable[entry]);
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_set_calgain
  FUNCTION DETAILS  : Sets the calibration gain for a servo-valve output
					  channel.
********************************************************************************/

void chan_sv_set_calgain(chandef* def, float gain)
{
	cal_sv* cal;
	cfg_sv* cfg;

	cal = &def->cal.u.c_sv;	/* SV specific calibration		*/
	cfg = &def->cfg.i_sv;	/* SV specific configuration	*/

	cal->cal_gain = gain;

	/* If valve is currently set to voltage mode, then the change in calibration gain
	   has an immediate effect. So recalculate configured gain and fine software
	   gain trim value.
	*/

	if (cfg->svtype == SV_VOLTAGE) {
		cfg->gain = 1.0F / cal->cal_gain;
		def->pos_gaintrim = (uint32_t)(cfg->gain * cfg->gaintrim * (float)GAINTRIM_SCALE);
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_read_calgain
  FUNCTION DETAILS  : Reads the calibration gain for a servo-valve output
					  channel.
********************************************************************************/

float chan_sv_read_calgain(chandef* def)
{
	cal_sv* cal;

	cal = &def->cal.u.c_sv;	/* SV specific calibration	*/
	return(cal->cal_gain);
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_set_caloffset
  FUNCTION DETAILS  : Sets the calibration offset for a servo-valve output
					  channel.
********************************************************************************/

void chan_sv_set_caloffset(chandef* def, float offset)
{
	cal_sv* cal;

	cal = &def->cal.u.c_sv;	/* SV specific calibration	*/

	cal->cal_offset = (int)((offset * (float)0x08000000UL) / 1.1F);
	def->offset = cal->cal_offset;
	update_shared_channel_parameter(def, offsetof(chandef, offset), sizeof(int));
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_read_caloffset
  FUNCTION DETAILS  : Reads the calibration offset for a servo-valve output
					  channel.
********************************************************************************/

float chan_sv_read_caloffset(chandef* def)
{
	cal_sv* cal;

	cal = &def->cal.u.c_sv;	/* SV specific calibration	*/

	return((((float)cal->cal_offset) * 1.1F) / (float)0x08000000UL);
}



/********************************************************************************
  FUNCTION NAME     : calc_sv_max
  FUNCTION DETAILS  : Calculate the maximum servo-valve current for a given
					  trim pot setting.

					  On entry:

					  rp		Calibration value for rp
					  potset	Current pot setting 0 to 255

					  On exit:

					  Returns calculated max SV current in mA

********************************************************************************/

float calc_sv_max(float rp, int potset)
{
	float r;
	float g;
	float rp1;
	float rp2;
	float i;

	rp1 = ((float)(255 - potset) * rp / 255.0) + SV_RB1;
	rp2 = ((float)potset * rp / 255.0) + SV_RB2;

	r = SV_RA + ((SV_RA * rp2) / rp1) + rp2;
	g = (SV_R3 / SV_R1) * (r / (SV_R3 + r));

	i = 200.0 / g;

	return(i);
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_set_current
  FUNCTION DETAILS  : Set the servo-valve output type and current for the
					  selected servo-valve output channel.

					  On entry:

					  def		Points to the channel definition structure
					  type		Output type
									0 = Current
									1 = Voltage
					  current	Required current range
					  flags		Bit 0 : Reserved
								Bit 1 : Retain gain trim value
										0 = Reset gain trim value to 1.0
										1 = Retain existing gain trim value

********************************************************************************/

void chan_sv_set_current(chandef* def, uint32_t type, float current, uint32_t flags,
	int mirror)
{
	cfg_sv* cfg;
	int iset;
	//float gain;
	float trim;
	float izero;

	cfg = &def->cfg.i_sv;	/* SV specific configuration	*/

	/* Save new settings and reset gain trim */

	cfg->svtype = type;
	cfg->current = current;
	if (!(flags & SETGAIN_RETAIN_GAINTRIM)) cfg->gaintrim = 1.0;

	chan_sv_calc_gain_setting(def, type, current, &iset, &trim, &izero);

	def->setrawcurrent(def, iset);
	def->pos_gaintrim = (uint32_t)(cfg->gaintrim * trim * (float)GAINTRIM_SCALE);
	def->txdrzero = _ftoi(izero);

	/* Mirror to EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 1);		/* Mirror pos gaintrim value	*/
		mirror_chan_eeconfig(def, 4);		/* Mirror cfg area				*/
	}

	update_shared_channel_parameter(def, offsetof(chandef, pos_gaintrim), sizeof(int));
	update_shared_channel_parameter(def, offsetof(chandef, txdrzero), sizeof(int));
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_read_current
  FUNCTION DETAILS  : Read the servo-valve output type and current for the
					  selected servo-valve output channel.

					  On entry:

					  def		Points to the channel definition structure
					  type		Pointer to output type variable
									0 = Current
									1 = Voltage
					  current	Pointer to current range variable

********************************************************************************/

void chan_sv_read_current(chandef* def, uint32_t* type, float* current)
{
	cfg_sv* cfg;

	cfg = &def->cfg.i_sv;	/* SV specific configuration	*/

	*type = cfg->svtype;
	*current = cfg->current;
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_set_flags
  FUNCTION DETAILS  : Modify the control flags for the selected servo-valve
					  output channel.
********************************************************************************/

void chan_sv_set_flags(chandef* def, int set, int clr, int updateclamp, int mirror)
{
	uint32_t istate;
	cfg_sv* cfg;
	uint32_t oldstate;
	uint32_t newstate;

	cfg = &def->cfg.i_sv;		/* SV specific configuration	*/

	/* Update configuration data and channel control flags */

	/* Determine the new flag state by setting and clearing bits as specified by
	   set and clr parameters. Note that the simulation and disable flags in the
	   chandef structure may have been modified by KO code and this will not be
	   reflected in the cfg structure. Therefore, we must read their states directly
	   from the chandef structure.

	   Similarly, the virtual channel flag is stored in the chandef structure, but is
	   not present in the config structure. Therefore, it must also be read directly
	   from the chandef structure.
	*/

	oldstate = (cfg->flags & ~(FLAG_SIMULATION | FLAG_VIRTUAL | FLAG_DISABLE)) | (def->ctrl & (FLAG_SIMULATION | FLAG_VIRTUAL | FLAG_DISABLE));
	newstate = (oldstate & ~clr) | set;

	/* Update configuration data and channel control flags. Need to protect against interrupts
	   between updating DSP A memory and copying to DSP B memory.
	*/

	cfg->flags = newstate;

	istate = disable_int(); /* Disable interrupts whilst updating */

	def->ctrl = newstate;

	/* Update transducer flags setting in shared channel definition */

	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(int));

	restore_int(istate);

	/* Mirror into EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 4);	/* Mirror cfg area		*/
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_read_flags
  FUNCTION DETAILS  : Read the control flags for the selected servo-valve output
					  channel.
********************************************************************************/

int chan_sv_read_flags(chandef* def)
{
	cfg_sv* cfg;

	cfg = &def->cfg.i_sv;		/* SV specific configuration	*/

	return(cfg->flags);
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_set_dither
  FUNCTION DETAILS  : Set the servo-valve dither amplitude and frequency.

					  On entry:

					  def		Points to the channel definition structure
					  enable	Dither enable flag
					  dither	Dither amplitude
					  freq		Dither frequency

********************************************************************************/

void chan_sv_set_dither(chandef* def, uint32_t enable, float dither, float freq, int mirror)
{
	cfg_sv* cfg;

	cfg = &def->cfg.i_sv;	/* SV specific configuration	*/

	cfg->dither_enable = enable;
	cfg->dither_ampl = dither;
	cfg->dither_freq = freq;

	def->def.def_sv.dither_ampl = (enable) ? dither : 0.0;
	def->def.def_sv.dither_freq = (uint32_t)((freq / 128.0) * 65536.0);

	/* Mirror to EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 4);		/* Mirror cfg area		*/
	}

	update_shared_channel_parameter(def, offsetof(chandef, def.def_sv.dither_ampl), sizeof(uint32_t));
	update_shared_channel_parameter(def, offsetof(chandef, def.def_sv.dither_freq), sizeof(uint32_t));
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_read_dither
  FUNCTION DETAILS  : Read the servo-valve dither amplitude and frequency.

					  On entry:

					  def		Points to the channel definition structure
					  enable	Pointer to variable to hold dither enable flag
					  dither	Pointer to variable to hold dither amplitude
					  freq		Pointer to variable to hold dither frequency

********************************************************************************/

void chan_sv_read_dither(chandef* def, uint32_t* enable, float* dither, float* freq)
{
	cfg_sv* cfg;

	cfg = &def->cfg.i_sv;	/* SV specific configuration	*/

	if (enable) *enable = cfg->dither_enable;
	if (dither) *dither = cfg->dither_ampl;
	if (freq) *freq = cfg->dither_freq;
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_set_balance
  FUNCTION DETAILS  : Set the servo-valve balance.

					  On entry:

					  def		Points to the channel definition structure
					  balance	Valve balance

********************************************************************************/

void chan_sv_set_balance(chandef* def, float balance, int mirror)
{
	cfg_sv* cfg;

	cfg = &def->cfg.i_sv;	/* SV specific configuration	*/

	cfg->balance = balance;

	def->refzero = balance;

	/* Mirror to EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 4);		/* Mirror cfg area		*/
	}

	update_shared_channel_parameter(def, offsetof(chandef, refzero), sizeof(float));
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_read_balance
  FUNCTION DETAILS  : Read the servo-valve balance.

					  On entry:

					  def		Points to the channel definition structure
					  balance	Pointer to variable to hold valve balance

********************************************************************************/

void chan_sv_read_balance(chandef* def, float* balance)
{
	cfg_sv* cfg;

	cfg = &def->cfg.i_sv;	/* SV specific configuration	*/

	*balance = cfg->balance;
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_write_userstring
  FUNCTION DETAILS  : Write user string for the selected servo-valve channel.

					  On entry:

					  string	Points to the start of the user string.
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_sv_write_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string, int mirror)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(&def->userstring[start], string, length);
	if (mirror) {
		if (string) mirror_chan_eeconfig(def, 5);	/* Mirror user string */
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_read_userstring
  FUNCTION DETAILS  : Read user string for the selected servo-valve channel.
********************************************************************************/

void chan_sv_read_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(string, &def->userstring[start], length);
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_write_faultmask
  FUNCTION DETAILS  : Write fault mask value for the selected servo-valve
					  channel.

					  On entry:

					  mask		Mask flags.
									Bit 0	Reserved
									Bit 1	Reserved
									Bit 2	Drive current fault enable
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_sv_write_faultmask(chandef* def, uint32_t mask, int mirror)
{
	uint32_t m;
	int lp;		/* Local channel number on card */
	int slot;

	/* Modify the mask in the channel defintion */

	def->cfg.i_sv.faultmask = mask;

	/* Get the slot number and the local channel number on the card */

	slot = EXTSLOTNUM;
	lp = EXTSLOTCHAN;

	/* Modify the mask in the slot table */

	m = slot_status[slot].faultmask;
	slot_status[slot].faultmask = m & ~(0x07 << ((lp * 3) + 12)) | ((mask & 0x07) << ((lp * 3) + 12));

	if (mirror) {
		mirror_chan_eeconfig(def, 4);	/* Mirror chandef configuration data     */
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_sv_read_faultstate
  FUNCTION DETAILS  : Read fault state for the selected servo-valve channel.
********************************************************************************/

uint32_t chan_sv_read_faultstate(chandef* def, uint32_t* capabilities, uint32_t* mask)
{
	uint32_t state;
	hw_mboard* base;

	/* Read the current fault state. Since all fault registers are at the same offset
	   we can use the hw_mboard structure as a generic means to access them.
	*/

	base = def->hw.i_mboard.hw;
	state = (uint32_t)base->u.rd.fault;

	/* The fault capability word depends on the board revision. Check if any fault
	   detection is available by testing bit 15 of the fault register.
	*/

	if (capabilities) *capabilities = (state & 0x8000) ? FAULT_DRIVE : 0;
	if (mask) *mask = def->cfg.i_sv.faultmask;

	/* Extract the fault bits corresponding to the selected channel */

	return(((state & 0x7FFF) >> 12) & 0x07);
}
