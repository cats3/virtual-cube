#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>

#include "channel.h"
#include "virtual-cube-config.h"

struct configuration configuration = {
	.fw_version = 0x0104001f,	// 1.4.31
	.slots[0] = CARD_NONE,
	.slots[1] = CARD_NONE,
	.slots[2] = CARD_NONE,
};

static void config_set_slot_type(const int slot, const uint8_t type)
{
	if (slot > 0 && slot < 4)
		configuration.slots[slot - 1] = type;
}

static void config_parse_slot(const int slot, const char* value)
{
	if (strncmp(value, "1sv2tx", 6) == 0)
		config_set_slot_type(slot, CARD_2GP1SV);
	else if (strncmp(value, "2tx", 3) == 0)
		config_set_slot_type(slot, CARD_2TX);
	else if (strncmp(value, "none", 3) == 0)
		config_set_slot_type(slot, CARD_NONE);
	else
		fprintf(stderr, "config: unknown card type for slot %d: %s\n", slot, value);
}

static void config_parse_firmware(const char* value)
{
	int major, minor, build;
	if (sscanf(value, "%d.%d.%d", &major, &minor, &build) == 3)
	{
		configuration.fw_version = (uint32_t)(major & 0xff) << 24 |
			(uint32_t)(minor & 0xff) << 16 |
			(uint32_t)(build & 0xffff);
	}
}

static void config_parse_line(char* line)
{
	char* parameter;
	char* value;

	if (strlen(line) > 0)
	{
		parameter = strtok(line, "=");
		if (parameter)
		{
			value = strtok(NULL, "");
			if (strncmp("slot", parameter, 4) == 0)
				config_parse_slot(atoi(&parameter[4]), value);
			else if (strncmp("firmware", parameter, 8) == 0)
				config_parse_firmware(value);
			else
				fprintf(stderr, "config: unrecognised parameter %s\n", parameter);
		}
	}
}

const char* default_conf_filename = "/etc/vcube.conf";
void config_read(const char* conf_filename)
{
	FILE* f = NULL;
	char* line = NULL;
	size_t len = 0;
	size_t read;

	if (conf_filename == NULL)
		conf_filename = default_conf_filename;
	
	f = fopen(conf_filename, "r");
	if (f != NULL)
	{
		while ((read = getline(&line, &len, f) != -1))
		{
			if (line[0] == '#' || !isalnum(line[0]))
				continue;	// a comment or blank line
			config_parse_line(line);
		}
		fclose(f);
		if (line)
			free(line);
	}
	else
	{
		fprintf(stderr, "config: could not find configuration file '%s'.  Using defaults.\n", conf_filename);
	}
}

uint32_t config_get_fwversion(void)
{
	return configuration.fw_version;
}

uint8_t config_get_slot_type(const int slot)
{
	if (slot > 0 && slot < 4)
		return configuration.slots[slot - 1];
	else
		return CARD_NONE;
}

