/********************************************************************************
  MODULE NAME   	: hcsupport
  PROJECT			: Control Cube
  MODULE DETAILS  	: Hand controller support code.
********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
//#include <std.h>
//#include <sys.h>
//#include <sem.h>
//#include <hwi.h>
//#include <log.h>
//#include <c62.h>

//#include "ccubecfg.h"
//#include "csl_cache.h"

#include <stdint.h>
#include "porting.h"
#include "asmcode.h"

#include "shared.h"
#include "eventrep.h"

int hc_button_state = 0;

int event_hcbutton_bitmask = 0;

#if 0

#define HC_KEYSTATE 0x30	/* DSP B address of hand controller key state word	 */
#define HC_STATUS 	0x34	/* DSP B address of hand controller status word	 	 */

#endif

/********************************************************************************
  FUNCTION NAME   	: hc_timer_support
  FUNCTION DETAILS  : Support code for hand controller event reporting.
					  Called at 20ms intervals and generates an event report
					  whenever a hand controller button press change of state
					  occurs.
********************************************************************************/

void hc_timer_support(void)
{
	int s = 0;

	// TODO: hand controller support
	//s = hpi_read4((uint32_t*)HC_KEYSTATE) & 0xFF;
	if (s != hc_button_state) {

		char block[256];
		event_report* report = (event_report*)block;

		if (event_hcbutton_bitmask) {
			report->length = offsetof(event_report, data) + sizeof(int);
			report->type = EVENT_HCBUTTON;
			*((int*)(&report->data[0])) = s;
			event_send_report(event_hcbutton_bitmask, report, report->length);
		}
		hc_button_state = s;
	}

}

