﻿// virtual-cube.c : Defines the entry point for the application.
//
#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <pthread.h>
#include <sched.h>
#include <sys/prctl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <sys/shm.h>
#include <semaphore.h>
#include <signal.h>
#include <limits.h>
#include <time.h>


#include "virtual-cube.h"
#include "virtual-hardware.h"
#include "chanproc.h"
#include "system.h"
#include "eeprom.h"

#include "HardwareIndependent/ControlCube.h"
#include "HardwareIndependent/C3Std.h"

#include "hardware.h"
#include "mdnsserver.h"
#include "hydraulic.h"
#include "eventlog.h"
#include "eventrep.h"
#include "cnet.h"
#include "nonvol.h"
#include "controller.h"
#include "timer.h"
#include "shared.h"

#include "virtual-cube-config.h"

#include "msgserver.h"

char defaultname[32] = "XXXX";
char sysname[32];

volatile uint32_t prd_enable = 0;
//#define TP_QUEUE_SIZE 1024
//volatile uint32_t event_queue[TP_QUEUE_SIZE];


FILE* cnet_dump;


struct event_queue_s {
	uint32_t event_queue[TP_QUEUE_SIZE];
	uint32_t* tp_q_head;
	uint32_t* tp_q_tail;
};

struct event_queue_s* event_queue_ptr;

uint32_t* event_queue;
uint32_t saturation_state = 0;		// TODO: saturation_state always zero
volatile uint32_t tracking_request;
//extern uint32_t event_queue_end;
volatile uint32_t** tp_q_head;
uint32_t** tp_q_tail;
uint32_t event_queue_end;

static void write_to_queue(uint32_t data)
{
	*event_queue_ptr->tp_q_head++ = data;
	if (event_queue_ptr->tp_q_head >= &event_queue_ptr->event_queue[TP_QUEUE_SIZE])
		event_queue_ptr->tp_q_head = event_queue_ptr->event_queue;
}

int tp_add_to_queue(uint32_t CnetTsLo, uint32_t CnetTsHi, uint32_t tpCount, uint32_t tpError)
{
	volatile uint32_t* head = event_queue_ptr->tp_q_head;
	//u32_t *p;

	/* Advance head pointer by queue entry size. Note that the size is 4
	   because head is a pointer to a u32_t type (size 4 bytes).
	*/

	head = head + 4;

	//Check if the head will overtake the tail before the wrap..
	//then check again after the wrap.
	//If this is the case then there is no room in the queue.

	if ((event_queue_ptr->tp_q_head < event_queue_ptr->tp_q_tail) && (head >= event_queue_ptr->tp_q_tail)) {
		// then full
		return 1;
	}
	if (head >= &event_queue_ptr->event_queue[TP_QUEUE_SIZE]) {
		head = event_queue_ptr->event_queue;
		 //..then check for overtaking
		if (head >= event_queue_ptr->tp_q_tail) {
			// then full
			return 1;
		}
	}
	// queue not full so add data to the queue
	write_to_queue(CnetTsLo);
	write_to_queue(CnetTsHi);
	write_to_queue(tpCount);
	write_to_queue(tpError);

	return 0;
}

static void show_version_info(void)
{
	uint32_t fw_version = FWVERSION;
	uint8_t major = (uint8_t)(fw_version >> 24);
	uint8_t minor = (uint8_t)(fw_version >> 16);
	uint16_t build = (uint16_t)(fw_version & 0xffff);

	printf("built: %s - %s\n", __DATE__, __TIME__);
	printf("FW version: %d.%d.%d\n", major, minor, build);
	printf("Cube Type: %d\n", DEVICE_TYPE);
}

static FILE* open_chanproc_log(const char* filename)
{
	FILE* fp = fopen(filename, "w+");
	if (fp == NULL)
		perror("open_chanproc_log");
}

static void log_chanproc_info(const int ch, FILE* fp)
{
	struct simple_chandef* chan;
	if (fp)
	{
		chan = &dspb_shared_chandata->inchaninfo[ch];
		if (ftell(fp) > (4096L * 4L * (long)sizeof(struct simple_chandef)))
			rewind(fp);
		fwrite(chan, sizeof(struct simple_chandef), 1, fp);
	}
}

#define EXTRA_CHANNELS 	(ExpNExp + C3AppNChans)	/* Number of virtual + command channels */
#define ZERO_OFFSET_TRACKING	2	
static void* dspb_cnet_interrupt(void* ptr)
{

	sem_t* cnet_semaphore;
	cnet_semaphore = sem_open("/cnetintr", O_RDONLY);
	if (cnet_semaphore == SEM_FAILED)
	{
		perror("cnet_semaphore");
	}
	// FILE* chanproc_fp = open_chanproc_log("chanproc.bin");

	sleep(5);
	for (;;)
	{
		sem_wait(cnet_semaphore);
		simple_chandef* offset_chan;


		/* Process txdrzero and refzero request from DSP A */
		if (offset_chan = dspb_shared_chandata->txdrzero_chan) {
			offset_chan->txdrzero = dspb_shared_chandata->txdrzero_val;	/* Set txdrzero value in chandef	*/
			tracking_request = ZERO_OFFSET_TRACKING;					/* Request tracking mode cycles 	*/
			dspb_shared_chandata->txdrzero_chan = NULL;					/* Clear txdrzero request			*/
		}

		if (offset_chan = dspb_shared_chandata->refzero_chan) {
			offset_chan->refzero = dspb_shared_chandata->refzero_val;		/* Set refzero value in chandef		*/
			tracking_request = ZERO_OFFSET_TRACKING;					/* Request tracking mode cycles 	*/
			dspb_shared_chandata->refzero_chan = NULL;						/* Clear refzero request			*/
		}

		// TODO: look at tmp_sat_state
		//tmp_sat_state = 0;											/* Initialise temp saturation state	*/

		// log_chanproc_info(0, chanproc_fp);

		inchanproc(0, dspb_shared_chandata->physinchan);					/* Process physical input channels 	*/
		BCtrlIterA();				/* Virtual channel processing code			 */
		inchanproc((MAXINCHAN - EXTRA_CHANNELS), EXTRA_CHANNELS);		/* Process virtual input channels 	*/
		//saturation_state = tmp_sat_state;							/* Update global saturation state	*/
		BCtrlIterB();				/* Function generation and control loop code */
		outchanproc();				/* Process output channels					 */
		miscchanproc();				/* Process miscellaneous channels			 */
		finalproc();				/* Final interrupt processing				 */

	}
}

void setupSharedArea(void* ptr);
static void* dspb_background(void* arg)
{
	pthread_t cnet_interrupt_thread;
	FILE* fp;

	printf("dspb: started, pid = %d\n", getpid());

	setupSharedArea(dspb_shared_c3appdata);

	const char* name = "dspb-proc";
	if (prctl(PR_SET_NAME, (unsigned long)name) < 0)
		perror("prctl");

	int ret = pthread_create(&cnet_interrupt_thread, NULL, &dspb_cnet_interrupt, NULL);
	if (ret != 0)
	{
		perror("pthread_create: cnet_interrupt_thread");
	}
	pthread_setname_np(cnet_interrupt_thread, "cnet-intr-b");

	BCtrlInit();
	printf("dspb: bctrlinit done\n");

	fp = fopen("chandata.txt", "w");
	int i;
	while (1)
	{
		BCtrlPoll();
		usleep(5000);
		//for (i=0;i<MAXINCHAN;i++)
		//	fprintf(fp, "chan(%d),outptr=0x%lx,srcptr=0x%lx\n", i, (long unsigned int)dspb_shared_chandata->inchaninfo[i].outputptr, (long unsigned int)dspb_shared_chandata->inchaninfo[i].sourceptr);
		//fprintf(fp, "------ outputs ------\n");
		//for (i = 0; i < MAXOUTCHAN; i++)
		//	fprintf(fp, "chan(%d),outptr=0x%lx,srcptr=0x%lx\n", i, (long unsigned int)dspb_shared_chandata->inchaninfo[i].outputptr, (long unsigned int)dspb_shared_chandata->inchaninfo[i].sourceptr);
		//fflush(fp);
		//rewind(fp);
	}

	while (1)
	{
		BCtrlPoll();
		usleep(5000);
	}
}

#define INNER_CYCLES	100U
#define LOOP_RATE		4096U
#define US_PER_LOOP		((1000000U * INNER_CYCLES) / LOOP_RATE)
static void* cnet_thread(void* ptr)
{
	struct timespec tsnow, tspast;
	long ns_diff, usToWait;

	sem_t* cnet_semaphore = (sem_t*)ptr;
	for (;;)
	{

		clock_gettime(CLOCK_REALTIME, &tspast);

		for (int j = 0; j < INNER_CYCLES; j++)
		{
			sem_post(cnet_semaphore);
			usleep(5);
			cnet_interrupt();
			cnet_swap();
		}

		clock_gettime(CLOCK_REALTIME, &tsnow);


		ns_diff = tsnow.tv_nsec - tspast.tv_nsec;
		if (tsnow.tv_nsec < tspast.tv_nsec)
			ns_diff += 1000000000;		// cope with roll-over in tv_nsec

		usToWait = US_PER_LOOP - (ns_diff / 1000U);

		// if we are delayed by some silly amount, cope by just delaying a tiny amount of time
		if (usToWait < 10)
			usToWait = 10;

		usleep(usToWait);
	}
}

static uint32_t sigfpe_count = 0;
static void sigfpe_handler(int sig)
{
	sigfpe_count++;
}

static pid_t dspb_process_id = 0;
const char segv_error[] = "Encountered segmentation fault. Exiting\n";
static void sigsegv_handler(int sig)
{
	if (sig == SIGSEGV)
	{
		write(STDERR_FILENO, segv_error, strlen(segv_error));
		if (dspb_process_id > 0)
			kill(dspb_process_id, SIGTERM);
		_exit(1);
	}
}

static void sigterm_handler(int sig)
{
	if (sig == SIGTERM)
	{
		if (dspb_process_id > 0)
			kill(dspb_process_id, SIGTERM);
		/* TODO: close any other files and exit gracefully */
		exit(0);
	}
}

/* https://stackoverflow.com/questions/16400820/how-to-use-posix-semaphores-on-forked-processes-in-c */
static void init_shared_event_queue(void)
{
	size_t s = sizeof(struct event_queue_s);
	int shmid = shmget(IPC_PRIVATE, s, 0644 | IPC_CREAT);
	if (shmid < 0)
	{
		perror(__FUNCTION__);
		exit(1);
	}
	event_queue_ptr = (struct event_queue_s*)shmat(shmid, NULL, 0);
	event_queue_ptr->tp_q_head = event_queue_ptr->event_queue;
	event_queue_ptr->tp_q_tail = event_queue_ptr->event_queue;
}

extern void dsp_io_init(char*);

int main(int argc, char** argv)
{
	int do_flush;

	pthread_t msg_server_thread;
	pthread_t rtmsg_server_thread;
	pthread_t event_reporter_thread;
	pthread_t cnet_interrupt_thread;
	pthread_t json_thread;

	struct sched_param sched_param;
	sem_t* cnet_semaphore;

	char* hwOutFile = "hardware.json";
	
	sleep(1);

	int i;
	printf("argc: %d\n", argc);
	for (i = 0; i < argc; i++)
	{
		puts(argv[i]);
	}

	if (argc == 3)
	{
		config_read(argv[2]);
	}
	else
		config_read(NULL);
	show_version_info();

	if (argc >= 2)
	{
		hwOutFile = argv[1];
	}
	char* fifo = "/tmp/status_update";

	setbuf(stdout, NULL);

	/* handle SIGFPE - it's assumed that the real DSP silently handles div by zero */
	struct sigaction sa;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	sa.sa_handler = sigfpe_handler;
	sigaction(SIGFPE, &sa, NULL);

	sa.sa_handler = sigsegv_handler;
	sigaction(SIGSEGV, &sa, NULL);

	sa.sa_handler = sigterm_handler;
	sigaction(SIGTERM, &sa, NULL);

	init_shared_event_queue();

	cnet_semaphore = sem_open("/cnetintr", O_CREAT | O_WRONLY, 0666, 0);	/* initial value of sem4 is zero */
	if (cnet_semaphore == SEM_FAILED)
	{
		perror("Semaphore failed");
		diag_error(errno);
	}

	uint32_t buffer;

	vh_initialise();

	fpled_ctrl(LED_ON);
	printf("dsp_reset_peripherals:\n");
	dsp_reset_peripherals();

	printf("dsp_io_init:\n");
	dsp_io_init(hwOutFile);
	printf("init_local_io:\n");

	hyd_initialise();
	mdns_initialise();		// part of tcp_ip();
	system_initialise();
	eventlog_init();
	cnet_initialise();
	event_initialise();

	boot_dspb(NULL, 0, 0, 0);

	dspb_process_id = fork();
	if (dspb_process_id == -1)
	{
		diag_error(errno);
	}
	if (dspb_process_id == 0)
		/* this is now the child process */
		dspb_background(NULL);

	int ret;
	/* from here we are in dspa land */
	ret = pthread_create(&msg_server_thread, NULL, &msg_server, NULL);
	if (ret != 0)
	{
		perror("error creating msg_server_thread\n");
	}
	pthread_setname_np(msg_server_thread, "msg-server");

	ret = pthread_create(&rtmsg_server_thread, NULL, &rtmsg_server, NULL);
	if (ret != 0)
	{
		perror("error creating rtmsg_server_thread\n");
	}
	pthread_setname_np(rtmsg_server_thread, "rtmsg-server");

	prctl(PR_SET_NAME, (unsigned long)"dspa-proc");

	chan_gp_initlist();
	chan_ad_initlist();
	chan_da_initlist();
	chan_sv_initlist();
	chan_io_initlist();
	chan_vt_initlist();
	chan_dt_initlist();

	do_flush = nv_checkflush();
	if (do_flush) {
		dsp_1w_writepage(PAGE_RIGNAME, (uint8_t*)"None", 31, WR_CHKSUM);	/* Reset rig name */
	}
	slot_ident(do_flush ? DO_FLUSH : FALSE);			/* Scan hardware and restore system config 	*/

	ctrl_initialise1();
	ctrl_initialise2();
	nv_initialise(NVINIT_INIT);
	nv_initialise(do_flush ? NVINIT_FLUSH : FALSE);
	runtimer_init();

	CtrlValidate();
	CtrlPrime();
	slot_post_init();
	chan_initialise_command_clamp();
	hyd_control_initialise();
	shared_initialise();
	chan_initialise_threshold();

	ret = pthread_create(&event_reporter_thread, NULL, event_reporter, NULL);
	if (ret != 0)
	{
		perror("Error creating error reporting thread\n");
		diag_error(errno);
	}
	pthread_setname_np(event_reporter_thread, "event-report");
	int policy, s;
	s = pthread_getschedparam(event_reporter_thread, &policy, &sched_param);
	// TODO: think about event_reporter thread - don't want to hog the CPU

	hyd_init_hw_masks();

	/* start cnet 'interrupts' */
	ret = pthread_create(&cnet_interrupt_thread, NULL, cnet_thread, cnet_semaphore);
	if (ret != 0)
	{
		perror("Error creating cnet interrupt thread\n");
		diag_error(errno);
	}
	pthread_setname_np(cnet_interrupt_thread, "cnet-intr");
	// TODO: think about cnet_interrupt thread - this is real-time stuff!
	
	fpled_ctrl(LED_OFF);
	prd_enable = TRUE;

	printf("dspa: entering background loop\n");
	int* cnet_mem = cnet_get_slotaddr(0, 0);
	for (;;)
	{
		mdns_server();
		CtrlPoll();
		usleep(10000);

		if (sigfpe_count)
		{
			printf("+++ SIGFPE:\n");
			sigfpe_count--;
		}
	}

	return 0;
}

