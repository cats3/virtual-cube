/********************************************************************************
 * MODULE NAME       : hw_2gp1sv.c												*
 * PROJECT			 : Control Cube												*
 * MODULE DETAILS    : Interface routines for 2GP1SV card (or main board		*
 *					   equivalent functions).									*
 *																				*
 *					   This module also handles some functions of the 2TX card,	*
 *					   which is a 1SV2TX card without servo-valve capability.	*
 *																				*
 ********************************************************************************/

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include <stdint.h>
#include "porting.h"


#include "defines.h"
 //#include "ccubecfg.h"
#include "hydraulic.h"
#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"
#include "asmcode.h"

 /* Define TEST8CHAN to make a test version to simulate having a system
	with three additional 2TX cards, giving a total of 8 standard feedback
	channels.
 */

#undef TEST8CHAN

 /* External functions required from hw_mboard module */

extern void hw_mboard_set_raw_current(chandef* def, int current);

/* Hardware input/output lines */

#define GAIN400_0	0			/* Input 0 gain x400 select relay					*/
#define GAIN20_0	1			/* Input 0 gain x20 select relay					*/
#define GAIN400_1	2			/* Input 1 gain x400 select relay					*/
#define GAIN20_1	3			/* Input 1 gain x20 select relay					*/
#define CALEN		4			/* Calibration enable relay							*/
#define CALPOL		5			/* Calibration polarity select relay 0=NEG,1=POS	*/

#define RLYMASKCAL	((1 << CALPOL) | (1 << CALEN))

/* LED output lines */

#define LED_0		0			/* Channel 0 LED output								*/
#define LED_1		1			/* Channel 1 LED output								*/
#define LED_SV		2			/* Servo-valve LED output							*/

/* Define general constants */

#define PI			3.141592653
#define EXCDAC_CYC	192				/* Number of excitation DAC outputs per cycle */
#define EXCDAC_HALF	96				/* Half cycle */



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_i2c_restart
  FUNCTION DETAILS  : Send I2C restart state.
********************************************************************************/

void hw_2gp1sv_i2c_restart(info_2gp1sv* info)
{
	// not required in virtual cube
	//hw_2gp1sv* base;
	//int scl;
	//int sda;

	//base = info->hw;

	//base->u.wr.sda = 1;			/* Release SDA high */
	//sda = base->u.rd.sda;		/* Force write buffer to flush */
	//delay_clk(25);				/* Quarter bit delay */
	//base->u.wr.scl = 1;			/* Release SCL high */
	//scl = base->u.rd.scl;		/* Force write buffer to flush */
	//delay_clk(25);				/* Quarter bit delay */
	//base->u.wr.sda = 0;			/* Pull SDA low */
	//sda = base->u.rd.sda;		/* Force write buffer to flush */
	//delay_clk(25);				/* Quarter bit delay */
	//base->u.wr.scl = 0;			/* Pull SCL low */
	//scl = base->u.rd.scl;		/* Force write buffer to flush */
	//delay_clk(25);				/* Quarter bit delay */
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_i2c_start
  FUNCTION DETAILS  : Send I2C start state.
********************************************************************************/

void hw_2gp1sv_i2c_start(info_2gp1sv* info)
{
	// not required in virtual cube
	//hw_2gp1sv* base;
	//int scl;
	//int sda;

	//base = info->hw;

	//base->u.wr.sda = 0;			/* Pull SDA low */
	//sda = base->u.rd.sda;		/* Force write buffer to flush */
	//delay_clk(25);				/* Quarter bit delay */
	//base->u.wr.scl = 0;			/* Pull SCL low */
	//scl = base->u.rd.scl;		/* Force write buffer to flush */
	//delay_clk(25);				/* Quarter bit delay */
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_i2c_stop
  FUNCTION DETAILS  : Generate I2C stop condition
********************************************************************************/

void hw_2gp1sv_i2c_stop(info_2gp1sv* info)
{
	// not required in virtual cube
	//hw_2gp1sv* base;
	//int scl;
	//int sda;

	//base = info->hw;

	//base->u.wr.scl = 1;			/* Release SCL high */
	//scl = base->u.rd.scl;		/* Force write buffer to flush */
	//delay_clk(25);				/* Quarter bit delay */
	//base->u.wr.sda = 1;			/* Release SDA high */
	//sda = base->u.rd.sda;		/* Force write buffer to flush */
	//delay_clk(25);				/* Quarter bit delay */
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_i2c_send
  FUNCTION DETAILS  : Send I2C data byte.

					  Returns 0 if the byte has been acknowledged correctly.

********************************************************************************/

int hw_2gp1sv_i2c_send(info_2gp1sv* info, uint8_t data)
{
	// not required in virtual cube
	//hw_2gp1sv* base;
	//int bitcount = 8;
	//int ack;
	//int scl;
	//int sda;

	//base = info->hw;

	//while (bitcount) {
	//	delay_clk(25);					/* Quarter bit delay */
	//	if (data & 0x80) {
	//		base->u.wr.sda = 1;			/* Release SDA high */
	//	}
	//	else {
	//		base->u.wr.sda = 0;			/* Pull SDA low */
	//	}
	//	sda = base->u.rd.sda;			/* Force write buffer to flush */
	//	delay_clk(25);					/* Quarter bit delay */
	//	base->u.wr.scl = 1;				/* Release SCL high */
	//	scl = base->u.rd.scl;			/* Force write buffer to flush */
	//	delay_clk(50);					/* Half bit delay */
	//	base->u.wr.scl = 0;				/* Pull SCL low */
	//	scl = base->u.rd.scl;			/* Force write buffer to flush */
	//	data = data << 1;				/* Shift next bit into position */
	//	bitcount--;						/* Count data bits */
	//}

	///* Now generate clock pulse for acknowledge bit and test if receiver has
	//   acknowledged correctly.
	//*/

	//delay_clk(25);				/* Quarter bit delay */
	//base->u.wr.sda = 1;			/* Release SDA high */
	//sda = base->u.rd.sda;		/* Force write buffer to flush */
	//delay_clk(25);				/* Quarter bit delay */
	//base->u.wr.scl = 1;			/* Release SCL high */
	//scl = base->u.rd.scl;		/* Force write buffer to flush */
	//delay_clk(50);				/* Half bit delay */

	///* Test for acknowledge from receiver */

	//ack = base->u.rd.sda & 0x01;

	//base->u.wr.scl = 0;		/* Pull SCL low */
	//scl = base->u.rd.scl;	/* Force write buffer to flush */
	//delay_clk(25);			/* Quarter bit delay */
	//base->u.wr.sda = 0;		/* Pull SDA low */
	//sda = base->u.rd.sda;	/* Force write buffer to flush */
	//delay_clk(25);			/* Quarter bit delay */

	//return(ack);
	return(0);
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_i2c_read
  FUNCTION DETAILS  : Read I2C data byte.

					  Returns received data byte.

					  On entry:

					  ack	indicates if an acknowledgement should be sent

********************************************************************************/

uint8_t hw_2gp1sv_i2c_read(info_2gp1sv* info, int ack)
{
	// not required in virtual cube
	//hw_2gp1sv* base;
	//int bitcount = 8;
	//uint8_t data = 0;
	//int scl;
	//int sda;

	//base = info->hw;

	//base->u.wr.sda = 1;						/* Release SDA high */
	//sda = base->u.rd.sda;					/* Force write buffer to flush */
	//while (bitcount) {
	//	delay_clk(25);						/* Quarter bit delay */
	//	base->u.wr.scl = 1;					/* Release SCL high */
	//	scl = base->u.rd.scl;				/* Force write buffer to flush */
	//	delay_clk(50);						/* Half bit delay */
	//	data = data << 1;					/* Make space for received data bit */
	//	data |= base->u.rd.sda & 0x01;		/* Insert new data bit */
	//	base->u.wr.scl = 0;					/* Pull SCL low */
	//	scl = base->u.rd.scl;				/* Force write buffer to flush */
	//	delay_clk(25);						/* Quarter bit delay */
	//	bitcount--;							/* Count data bits */
	//}

	///* Now generate acknowledge bit */

	//if (ack) base->u.wr.sda = 0;			/* Pull SDA low if ack required */
	//sda = base->u.rd.sda;					/* Force write buffer to flush */

	//delay_clk(25);							/* Quarter bit delay */
	//base->u.wr.scl = 1;						/* Release SCL high */
	//scl = base->u.rd.scl;					/* Force write buffer to flush */
	//delay_clk(50);							/* Half bit delay */
	//base->u.wr.scl = 0;						/* Pull SCL low */
	//scl = base->u.rd.scl;					/* Force write buffer to flush */
	//delay_clk(25);							/* Quarter bit delay */

	//return(data);
	return (0);
}

#if 0

SCL.equ	0x20; Offset of SCL from hw_2gp1sv base address
SDA.equ	0x22; Offset of SDA from hw_2gp1sv base address

stw		b3, * sp--(4); Save return address
ldw		a5, * a4[HW]; Load base address
mvk		8, b0; Initialise bit count
clr		a2; Initialise data byte
mvk		1, a0
nop		2

mvkl	delay_clk, b5; Initialise delay routine call address
mvkh	delay_clk, b5

sth		a0, * a5(SDA); Release SDA high
ldh* a5(SDA), a1; Force write buffer to flush

L1			b		b5; Quarter bit delay
mvkl	L2, b3
mvkh	L2, b3
mvk		25, a4
nop		2
L2
mvk		1, a0
sth		a0, * a5(SCL); Release SCL high
ldh* a5(SCL), a1; Force write buffer to flush
b		b5; Half bit delay
mvkl	L3, b3
mvkh	L3, b3
mvk		50, a4
nop		2
L3			ldh * a5(SDA), a1; Read SDA state
shlu	a2, 1, a2
nop		3
extu	a1, 0, 0, a1; Extract data bit
or a2, a1, a2; Insert data bit
mvk		0, a0
sth		a0, * a5(SCL); Pull SCL low
ldh* a5(SCL), a1; Force write buffer to flush
b		b5; Quarter bit delay
mvkl	L4, b3
mvkh	L4, b3
mvk		25, a4
nop		2
L4			sub		b0, 1, b0; Count bits
[b0]	b		L1; Loop if more bits
nop		5

mov		b4, b0; Test ack
[b0]	mvk		0, a0
[b0]	sth		a0, * a5(SDA); Pull SDA low if ack required
[b0]	ldh* a5(SDA), a1; Force write buffer to flush

b		b5; Quarter bit delay
mvkl	L5, b3
mvkh	L5, b3
mvk		25, a4
L5
mvk		1, a0
sth		a0, * a5(SCL); Release SCL high
ldh* a5(SCL), a1; Force write buffer to flush
b		b5; Half bit delay
mvkl	L6, b3
mvkh	L6, b3
mvk		50, a4
nop		2
L6
mvk		0, a0
sth		a0, * a5(SCL); Pull SCL low
ldh* a5(SCL), a1; Force write buffer to flush
b		b5; Quarter bit delay
mvkl	L7, b3
mvkh	L7, b3
mvk		25, a4
nop		2
L7
mov		a2, a4; Copy data

ldw		b3, * ++sp(4); Restore return address
nop		4
b		b3; Return

#endif



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_1w_init
  FUNCTION DETAILS  : Initialise 1-wire port.
********************************************************************************/

void hw_2gp1sv_1w_init(info_2gp1sv * info, uint32_t bus)
{
	//hw_2gp1sv* base;

	//base = info->hw;
	//if (bus) {
	//	base->u.wr.teds2 = ONEW_RESET;	/* Reset TEDS2 1-wire bus */
	//}
	//else {
	//	base->u.wr.teds1 = ONEW_RESET;	/* Reset TEDS1 1-wire bus */
	//}
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_1w_reset
  FUNCTION DETAILS  : Reset all devices on 1-wire bus.

					  On entry:

					  bus	Determines which TEDS bus to access.
********************************************************************************/

int hw_2gp1sv_1w_reset(info_2gp1sv * info, uint32_t bus)
{
	// not required in virtual cube
	//hw_2gp1sv* base;
	//int present;

	//base = info->hw;

	//if (bus) {

	//	/* Access TEDS bus 2 */

	//	while (base->u.rd.teds2 & ONEW_BUSY);	/* Wait for not busy */
	//	base->u.wr.teds2 = ONEW_RESET;			/* Reset 1-wire bus */
	//	while (base->u.rd.teds2 & ONEW_BUSY);	/* Wait for completion */
	//	present = (base->u.rd.teds2 & ONEW_PRES) ? 1 : 0;	/* Read presence flag */

	//}
	//else {

	//	/* Access TEDS bus 1 */

	//	while (base->u.rd.teds1 & ONEW_BUSY);	/* Wait for not busy */
	//	base->u.wr.teds1 = ONEW_RESET;			/* Reset 1-wire bus */
	//	while (base->u.rd.teds1 & ONEW_BUSY);	/* Wait for completion */
	//	present = (base->u.rd.teds1 & ONEW_PRES) ? 1 : 0;	/* Read presence flag */

	//}

	//return(present);
	return (0);
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_1w_write
  FUNCTION DETAILS  : Write a byte to the 1-wire bus.

					  On entry:

					  bus	Determines which TEDS bus to access.

********************************************************************************/

void hw_2gp1sv_1w_write(info_2gp1sv * info, uint32_t bus, uint8_t data)
{
	// not required in virtual cube
	//hw_2gp1sv* base;
	//int n;

	//base = info->hw;

	//n = 8;

	//if (bus) {

	//	/* Access TEDS bus 2 */

	//	while (n) {
	//		while (base->u.rd.teds2 & ONEW_BUSY);			/* Wait for not busy */
	//		base->u.wr.teds2 = ONEW_WRITE0 | (data & 0x01);	/* Write bit to 1-wire bus */
	//		data = data >> 1;
	//		n--;
	//	}

	//}
	//else {

	//	/* Access TEDS bus 1 */

	//	while (n) {
	//		while (base->u.rd.teds1 & ONEW_BUSY);			/* Wait for not busy */
	//		base->u.wr.teds1 = ONEW_WRITE0 | (data & 0x01);	/* Write bit to 1-wire bus */
	//		data = data >> 1;
	//		n--;
	//	}

	//}
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_1w_read
  FUNCTION DETAILS  : Read a byte from the 1-wire bus.

					  On entry:

					  bus	Determines which TEDS bus to access.

********************************************************************************/

uint8_t hw_2gp1sv_1w_read(info_2gp1sv * info, uint32_t bus)
{
	// not required in virtual cube

	//hw_2gp1sv* base;
	//uint8_t data = 0;
	//int n;

	//base = info->hw;

	//if (bus) {

	//	/* Access TEDS bus 2 */

	//	n = 8;
	//	while (n) {
	//		while (base->u.rd.teds2 & ONEW_BUSY);	/* Wait for not busy */
	//		base->u.wr.teds2 = ONEW_READ;			/* Initiate read operation */
	//		while (base->u.rd.teds2 & ONEW_BUSY);	/* Wait for completion */
	//		data = data >> 1;
	//		if (base->u.rd.teds2 & ONEW_DATA) {		/* Test 1-wire input */
	//			data |= 0x80;						/* Set received data bit if required */
	//		}
	//		n--;
	//	}

	//}
	//else {

	//	/* Access TEDS bus 1 */

	//	n = 8;
	//	while (n) {
	//		while (base->u.rd.teds1 & ONEW_BUSY);	/* Wait for not busy */
	//		base->u.wr.teds1 = ONEW_READ;			/* Initiate read operation */
	//		while (base->u.rd.teds1 & ONEW_BUSY);	/* Wait for completion */
	//		data = data >> 1;
	//		if (base->u.rd.teds1 & ONEW_DATA) {		/* Test 1-wire input */
	//			data |= 0x80;						/* Set received data bit if required */
	//		}
	//		n--;
	//	}

	//}
	//return(data);
	return(0);
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_spi_send
  FUNCTION DETAILS  : Send data byte via SPI interface to transducer control
					  outputs.

********************************************************************************/

void hw_2gp1sv_spi_send(info_2gp1sv * info, uint8_t data)
{
	//hw_2gp1sv* base;
	//int bitcount = 8;

	//base = info->hw;

	//base->u.wr.txrlyclk = 0;			/* Force SCLK low */
	//dsp_spi_delay();					/* Half bit delay */
	//base->u.wr.txrlycs = 0;				/* Set CS low */
	//dsp_spi_delay();					/* Half bit delay */

	//while (bitcount) {
	//	base->u.wr.txrlydin = (data & 0x80) ? 1 : 0;	/* Write bit to DIN */
	//	dsp_spi_delay();				/* Half bit delay */
	//	base->u.wr.txrlyclk = 1;		/* Pulse SCLK high */
	//	dsp_spi_delay();				/* Half bit delay */
	//	base->u.wr.txrlyclk = 0;		/* End of SCLK pulse */
	//	data = data << 1;				/* Shift next bit into position */
	//	bitcount--;						/* Count data bits */
	//}

	///* Now take CS high to load data into output latches. */

	//dsp_spi_delay();			/* Half bit delay */
	//base->u.wr.txrlycs = 1;		/* Set CS high */
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_tedsid
  FUNCTION DETAILS  : Read ID of attached TEDS device.
********************************************************************************/

int hw_2gp1sv_tedsid(chandef * def, uint32_t * lo, uint32_t * hi)
{
	//info_2gp1sv* i;
	//uint32_t bus;
	struct rom {
		union {
			uint32_t w;
			uint8_t  b[4];
		} lo;
		union {
			uint32_t w;
			uint8_t  b[4];
		} hi;
	} rom;

	//i = &def->hw.i_2gp1sv;	/* 2GP1SV specific information 	*/
	//bus = i->input;

	//if (hw_2gp1sv_1w_reset(i, bus)) return(NO_1WIRE);

	//hw_2gp1sv_1w_write(i, bus, 0x33);		/* Send READ_ROM command */

	///* Read 8 byte ROM code from device */

	//rom.hi.b[3] = hw_2gp1sv_1w_read(i, bus);
	//rom.hi.b[2] = hw_2gp1sv_1w_read(i, bus);
	//rom.hi.b[1] = hw_2gp1sv_1w_read(i, bus);
	//rom.hi.b[0] = hw_2gp1sv_1w_read(i, bus);

	//rom.lo.b[3] = hw_2gp1sv_1w_read(i, bus);
	//rom.lo.b[2] = hw_2gp1sv_1w_read(i, bus);
	//rom.lo.b[1] = hw_2gp1sv_1w_read(i, bus);
	//rom.lo.b[0] = hw_2gp1sv_1w_read(i, bus);

	///* Store into supplied variables */

	rom.hi.w = 0xdeadbeef;
	rom.lo.w = 0x55aa55aa;
	*hi = rom.hi.w;
	*lo = rom.lo.w;

	return(dsp_1w_checkcrc(rom.lo.w, rom.hi.w));
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_enable_excitation
  FUNCTION DETAILS  : Enable the excitation output for the selected transducer
					  channel.
********************************************************************************/

void hw_2gp1sv_enable_excitation(chandef * def, int enable)
{
	fprintf(stdout, "%s: [%d]\n", __FUNCTION__, enable);
	//info_2gp1sv* i;
	//hw_2gp1sv* base;

	//i = &def->hw.i_2gp1sv;	/* 2GP1SV specific information 	*/
	//base = i->hw;

	//base->u.wr.dacen &= (i->input) ? ~CHAN2_EXC : ~CHAN1_EXC;
	//if (enable) {
	//	base->u.wr.dacen |= (i->input) ? CHAN2_EXC : CHAN1_EXC;
	//}
}



/********************************************************************************
  FUNCTION NAME     : dacram_addr
  FUNCTION DETAILS  : Convert the excitation table entry into an address.
********************************************************************************/

unsigned int dacram_addr(unsigned int entry)
{
	return((entry < (EXCDAC_HALF)) ? entry : ((entry - (EXCDAC_HALF)) | 0x80));
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_set_raw_excitation
  FUNCTION DETAILS  : Set the excitation for the selected transducer channel
					  using raw bit values.
********************************************************************************/

void hw_2gp1sv_set_raw_excitation(chandef * def, int type, int value, float phase, int offset)
{
	fprintf(stdout, "%s: [type=%d, value=%d, phase=%3.3f, offset=%d]\n", __FUNCTION__, type, value, phase, offset);
//	info_2gp1sv* i;
//	hw_2gp1sv* base;
//	int n;
//	float angle;
//	float val;
//	short i_val;
//	uint16_t dacen;
//
//#define PHASE_OFFSET 0.0F	/* Default phase shift for AC transducers. Requested phase
//							   is added to the PHASE_OFFSET value.
//							*/
//
//	i = &def->hw.i_2gp1sv;	/* 2GP1SV specific information 	*/
//	base = i->hw;
//
//	/* Clip excitation value to DAC full-scale */
//
//	if (value > 0x1FFF) value = 0x1FFF;
//
//	/* Disable excitation whilst updating DAC RAM */
//
//	dacen = base->u.wr.dacen;
//	base->u.wr.dacen &= (i->input) ? ~CHAN2_EXC : ~CHAN1_EXC;
//
//	if (type) {
//
//		/* AC excitation */
//
//		for (n = 0; n < EXCDAC_CYC; n++) {
//			angle = (((float)n) * 2 * PI / EXCDAC_CYC) + ((phase + PHASE_OFFSET) * 2 * PI / 360) + (0.5 * PI);
//			val = sinf(angle) * (float)value;
//			i_val = (((short)(val + 0.5)) ^ 0x2000) - offset;
//			base->u.wr.excaddr = dacram_addr(n);
//			if (i->input)
//				base->u.wr.exc1 = i_val;
//			else
//				base->u.wr.exc0 = i_val;
//		}
//
//		/* Set AC transducer flag for selected input channel */
//
//		base->u.wr.ctrl |= (i->input) ? CHAN2_AC : CHAN1_AC;
//		def->ctrl |= FLAG_ACTRANSDUCER;
//
//	}
//	else {
//
//		/* DC excitation */
//
//		val = (float)value;
//		i_val = (((short)val) ^ 0x2000) - offset;
//		for (n = 0; n < EXCDAC_CYC; n++) {
//			base->u.wr.excaddr = dacram_addr(n);
//			if (i->input)
//				base->u.wr.exc1 = i_val;
//			else
//				base->u.wr.exc0 = i_val;
//		}
//
//		/* Clear AC transducer flag for selected input channel */
//
//		base->u.wr.ctrl &= (i->input) ? ~CHAN2_AC : ~CHAN1_AC;
//		def->ctrl &= ~FLAG_ACTRANSDUCER;
//
//	}
//
//	/* Restore previous excitation state */
//
//	base->u.wr.dacen = dacen;
//
//	/* Update transducer flags setting in shared channel definition */
//
	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(int));
}



/* Table to convert gaintable index into corresponding relay states. Note that
   the table uses the relays for input channel 0. These must be shifted left
   two places to convert for input channel 1.
*/

static uint8_t gainctrl[] = { 0,
						   1 << GAIN20_0,
						   1 << GAIN400_0,
						   0
};

#define RLYMASK0	((1 << GAIN20_0) | (1 << GAIN400_0))
#define RLYMASK1	((1 << GAIN20_1) | (1 << GAIN400_1))



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_set_raw_gain
  FUNCTION DETAILS  : Set the raw gain values for the selected transducer
					  channel.

					  On entry:

					  def		Points to the channel definition structure
					  coarse	Coarse gain setting
									0 = x1
									1 = x20
									2 = x400
					  fine		Fine gain setting (0 to 255)

********************************************************************************/

void hw_2gp1sv_set_raw_gain(chandef * def, uint32_t coarse, uint32_t fine)
{
	fprintf(stdout, "%s: [coarse=%d, fine=%d]\n", __FUNCTION__, coarse, fine);
	//info_2gp1sv* i;
	////cal_gp *cal;
	//cfg_gp* cfg;
	//uint8_t gainrly;

	//i = &def->hw.i_2gp1sv;	/* 2GP1SV specific information 	*/
	////cal = &def->cal.u.c_gp;	/* GP specific calibration		*/
	//cfg = &def->cfg.i_gp;	/* GP specific configuration	*/

	//cfg->coarsegain = (coarse > 2) ? 0 : coarse;
	//cfg->finegain = fine & 0xFF;

	///* Convert coarse gain setting into required relay state */

	//gainrly = gainctrl[cfg->coarsegain] << (i->input ? 2 : 0);

	///* Clear current coarse settings for this input */

	//*(i->relaystate) &= ~(i->input ? RLYMASK1 : RLYMASK0);

	///* Update new settings for this input */

	//*(i->relaystate) |= gainrly;

	///* Write new state to relay control outputs */

	//hw_2gp1sv_spi_send(i, *(i->relaystate));

	///* Update fine hardware gain setting */

	//hw_2gp1sv_i2c_start(i);
	//hw_2gp1sv_i2c_send(i, i->input ? 0x5A : 0x58);
	//hw_2gp1sv_i2c_send(i, 0x00);
	//hw_2gp1sv_i2c_send(i, (255 - cfg->finegain) & 0xFF);
	//hw_2gp1sv_i2c_stop(i);

	///* Update saturation detection thresholds depending on selected
	//   fine gain setting.
	//*/

	if (fine < 2) {
		def->satpos = 470000;
		def->satneg = -470000;
	}
	else {
		def->satpos = 513800;
		def->satneg = -511700;
	}
	update_shared_channel_parameter(def, offsetof(chandef, satpos), sizeof(int) * 2);

}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_read_raw_gain
  FUNCTION DETAILS  : Read the raw gain values for the selected transducer
					  channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void hw_2gp1sv_read_raw_gain(chandef * def, uint32_t * coarse, uint32_t * fine)
{
	fprintf(stdout, "%s:\n", __FUNCTION__);
	cfg_gp* cfg;

	cfg = &def->cfg.i_gp;	/* GP specific configuration	*/

	if (coarse) *coarse = ((cfg->coarsegain) > 2) ? 0 : cfg->coarsegain;
	if (fine) *fine = (cfg->finegain) & 0xFF;
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_cal_ctrl
  FUNCTION DETAILS  : Control the calibration relays for the selected
					  transducer channel.

					  On entry:

					  def		Points to the channel definition structure
					  state		Calibration state
									Bit 0		Enabled
									Bit 1		0 = Positive, 1 = Negative
									Bits 2..7	Reserved

********************************************************************************/

void hw_2gp1sv_cal_ctrl(chandef * def, int state)
{
	fprintf(stdout, "%s: [state=%d]\n", __FUNCTION__, state);

	info_2gp1sv* i;
	uint8_t calrly;
	int istate;

	i = &def->hw.i_2gp1sv;	/* 2GP1SV specific information 	*/

	/* Update the state of the chandef ctrl word and the corresponding word in DSP B memory */

	istate = disable_int();

	def->ctrl &= ~(FLAG_SHUNTCAL | FLAG_NEGSHUNTCAL);		/* Clear FLAG_SHUNTCAL and FLAG_NEGSHUNTCAL 	 */
	def->ctrl |= ((state & 0x01) ? FLAG_SHUNTCAL : 0) |		/* Set FLAG_SHUNTCAL if enabled					 */
		((state & 0x02) ? FLAG_NEGSHUNTCAL : 0);	/* Set FLAG_NEGSHUNTCAL if negative cal selected */

	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(uint32_t));

	restore_int(istate);

	/* If channel is inverted, then shunt calibration polarity must also be inverted */

	if (def->ctrl & FLAG_POLARITY) state ^= 0x02;

	calrly = (state & 0x01) ? ((1 << CALEN) | ((state & 0x02) ? (1 << CALPOL) : 0)) : 0;

	/* Clear existing relay state */

	*(i->relaystate) &= ~RLYMASKCAL;

	/* Update new settings */

	*(i->relaystate) |= calrly;

	/* Write new state to relay control outputs */

	hw_2gp1sv_spi_send(i, *(i->relaystate));

	grp_update_shuntcal(chan_check_state(FLAG_SHUNTCAL));
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_set_balance
  FUNCTION DETAILS  : Set the bridge balance adjustment for the selected
					  transducer channel.

					  On entry:

					  def		Points to the channel definition structure
					  balance	Required balance setting
									Adjustment range is -1.0 to +1.0

********************************************************************************/

void hw_2gp1sv_set_balance(chandef * def, float balance)
{
	fprintf(stdout, "%s: [balance=%3.2f]\n", __FUNCTION__, balance);

	//info_2gp1sv* i;
	//uint8_t bal;

	//i = &def->hw.i_2gp1sv;	/* 2GP1SV specific information 	*/

	//if (balance > 1.0)
	//	balance = 1.0;
	//else if (balance < -1.0)
	//	balance = -1.0;

	//bal = 0x80 - (uint8_t)(balance * 127.0);

	///* Update hardware balance setting */

	//hw_2gp1sv_i2c_start(i);
	//hw_2gp1sv_i2c_send(i, 0x5C);
	//hw_2gp1sv_i2c_send(i, i->input ? 0x80 : 0x00);
	//hw_2gp1sv_i2c_send(i, bal);
	//hw_2gp1sv_i2c_stop(i);
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_led_ctrl
  FUNCTION DETAILS  : Control LEDs on 2GP1SV card.
********************************************************************************/

void hw_2gp1sv_led_ctrl(chandef * def, int state)
{
	fprintf(stdout, "%s: [state=%d]\n", __FUNCTION__, state);

	//info_2gp1sv* i;
	//hw_2gp1sv* base;

	//i = &def->hw.i_2gp1sv;	/* 2GP1SV specific information 	*/
	//base = i->hw;			/* Hardware base address 		*/

	//switch (def->type) {
	//case TYPE_GP:
	//	*(i->ledstate) &= ~(i->ledmask);				/* Clear current LED state 	*/
	//	*(i->ledstate) |= (state) ? (i->ledmask) : 0;	/* Update with new state	*/
	//	base->u.wr.leds = *(i->ledstate);
	//	break;
	//case TYPE_SV:
	//	*(i->ledstate) &= ~(i->ledmask);				/* Clear current LED state 	*/
	//	*(i->ledstate) |= (state) ? (i->ledmask) : 0;	/* Update with new state	*/
	//	base->u.wr.leds = *(i->ledstate);
	//	break;
	//}

}



/*************************************************
* Table of handler functions for 2GP1SV channels *
*************************************************/

void* hw2gp1sv_gp_fntable[] = {
  &read_chan_status,				/* Fn  0. Ptr to readstatus function 				*/
  &chan_gp_set_pointer,				/* Fn  1. Ptr to setpointer function				*/
  &chan_gp_get_pointer,				/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  &save_chan_calibration,			/* Fn  6. Ptr to savecalibration function 			*/
  &restore_chan_calibration,		/* Fn  7. Ptr to restorecalibration function 		*/
  &flush_chan_calibration,			/* Fn  8. Ptr to flushcalibration function 			*/
  &chan_gp_set_excitation,			/* Fn  9. Ptr to setexc function 					*/
  &chan_gp_read_excitation,			/* Fn 10. Ptr to readexc function 					*/
  &chan_gp_set_exccal,				/* Fn 11. Ptr to setexccal function 				*/
  &chan_gp_read_exccal,				/* Fn 12. Ptr to readexccal function 				*/
  &chan_gp_set_gain,				/* Fn 13. Ptr to setgain function 					*/
  &chan_gp_read_gain,				/* Fn 14. Ptr to readgain function 					*/
  &hw_2gp1sv_set_balance,			/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  &hw_2gp1sv_cal_ctrl,				/* Fn 17. Ptr to calctrl function 					*/
  &hw_2gp1sv_led_ctrl,				/* Fn 18. Ptr to ledctrl function 					*/
  &hw_2gp1sv_set_raw_excitation,	/* Fn 19. Ptr to setrawexc function 				*/
  &hw_2gp1sv_enable_excitation,		/* Fn 20. Ptr to enableexc function 				*/
  &hw_2gp1sv_set_raw_gain,			/* Fn 21. Ptr to setrawgain function 				*/
  &hw_2gp1sv_read_raw_gain,			/* Fn 22. Ptr to readrawgain function 				*/
  &chan_read_adc,					/* Fn 23. Ptr to readadc function 					*/
  &set_filter,						/* Fn 24. Ptr to setfilter function 				*/
  &read_filter,						/* Fn 25. Ptr to readfilter function 				*/
  NULL,								/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function 			*/
  &chan_gp_set_caltable,			/* Fn 28. Ptr to set_caltable function 				*/
  &chan_gp_read_caltable,			/* Fn 29. Ptr to read_caltable function 			*/
  &chan_gp_set_calgain,				/* Fn 30. Ptr to set_calgain function 				*/
  &chan_gp_read_calgain,			/* Fn 31. Ptr to read_calgain function 				*/
  &chan_gp_set_caloffset,			/* Fn 32. Ptr to set_caloffset function 			*/
  &chan_gp_read_caloffset,			/* Fn 33. Ptr to read_caloffset function 			*/
  &chan_set_cnet_output,			/* Fn 34. Ptr to set_cnet_slot function 			*/
  &chan_gp_set_gaintrim,			/* Fn 35. Ptr to set_gaintrim function 				*/
  &chan_gp_read_gaintrim,			/* Fn 36. Ptr to read_gaintrim function 			*/
  &set_ip_offsettrim,				/* Fn 37. Ptr to set_offsettrim function	 		*/
  &read_ip_offsettrim,				/* Fn 38. Ptr to read_offsettrim function 			*/
  &chan_gp_set_txdrzero,			/* Fn 39. Ptr to set_txdrzero function				*/
  &chan_gp_read_txdrzero,			/* Fn 40. Ptr to read_txdrzero function				*/
  &read_peakinfo,					/* Fn 41. Ptr to readpeakinfo function 				*/
  &reset_peaks,						/* Fn 42. Ptr to resetpeak function 				*/
  &chan_gp_set_flags,				/* Fn 43. Ptr to set_flags function 				*/
  &chan_gp_read_flags,				/* Fn 44. Ptr to read_flags function 				*/
  &chan_gp_write_map,				/* Fn 45. Ptr to write_map function 				*/
  &chan_gp_read_map,				/* Fn 46. Ptr to read_map function 					*/
  &chan_gp_set_refzero,				/* Fn 47. Ptr to set_refzero function 				*/
  &read_refzero,					/* Fn 48. Ptr to read_refzero function 				*/
  &undo_refzero,					/* Fn 49. Ptr to undo_refzero function 				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  &hw_2gp1sv_tedsid,				/* Fn 52. Ptr to tedsid function 					*/
  NULL,								/* Fn 53. Ptr to set_source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  NULL,								/* Fn 58. Ptr to set info function					*/
  NULL,								/* Fn 59. Ptr to read info function					*/
  NULL,								/* Fn 60. Ptr to write output function				*/
  NULL,								/* Fn 61. Ptr to read input function				*/
  NULL,								/* Fn 62. Ptr to read output function				*/
  &chan_gp_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_gp_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  &chan_gp_txdrzero_zero,			/* Fn 72. Ptr to txdrzero_zero function				*/
  &chan_read_cyclecount,			/* Fn 73. Ptr to readcycle function					*/
  &reset_cycles,					/* Fn 74. Ptr to resetcycles function 				*/
  &chan_gp_set_peak_threshold,		/* Fn 75. Ptr to setpkthreshold function 			*/
  &chan_gp_read_peak_threshold,		/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_gp_write_faultmask,			/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_gp_read_faultstate,			/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  &chan_gp_refzero_zero,			/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};

void* hw2gp1sv_sv_fntable[] = {
  &read_chan_status,				/* Fn  0. Ptr to readstatus function 				*/
  &chan_sv_set_pointer,				/* Fn  1. Ptr to setpointer function				*/
  &chan_sv_get_pointer,				/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  &save_chan_calibration,			/* Fn  6. Ptr to savecalibration function 			*/
  &restore_chan_calibration,		/* Fn  7. Ptr to restorecalibration function 		*/
  &flush_chan_calibration,			/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  &chan_sv_set_gain,				/* Fn 13. Ptr to setgain function 					*/
  &chan_sv_read_gain,				/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  &hw_2gp1sv_led_ctrl,				/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  NULL,								/* Fn 23. Ptr to readadc function 					*/
  NULL,								/* Fn 24. Ptr to setfilter function 				*/
  NULL,								/* Fn 25. Ptr to readfilter function 				*/
  &chan_write_dac,					/* Fn 26. Ptr to writedac function 					*/
  &hw_mboard_set_raw_current,		/* Fn 27. Ptr to setrawcurrent function 			*/
  &chan_sv_set_caltable,			/* Fn 28. Ptr to set_caltable function 				*/
  &chan_sv_read_caltable,			/* Fn 29. Ptr to read_caltable function	 			*/
  &chan_sv_set_calgain,				/* Fn 30. Ptr to set_calgain function 				*/
  &chan_sv_read_calgain,			/* Fn 31. Ptr to read_calgain function 				*/
  &chan_sv_set_caloffset,			/* Fn 32. Ptr to set_caloffset function	 			*/
  &chan_sv_read_caloffset,			/* Fn 33. Ptr to read_caloffset function 			*/
  NULL,								/* Fn 34. Ptr to set_cnet_slot function 			*/
  &chan_sv_set_gaintrim,			/* Fn 35. Ptr to set_gaintrim function 				*/
  &chan_sv_read_gaintrim,			/* Fn 36. Ptr to read_gaintrim function 			*/
  &set_op_offsettrim,				/* Fn 37. Ptr to set_offsettrim function 			*/
  &read_op_offsettrim,				/* Fn 38. Ptr to read_offsettrim function 			*/
  NULL,								/* Fn 39. Ptr to set_txdrzero function				*/
  NULL,								/* Fn 40. Ptr to read_txdrzero function				*/
  NULL,								/* Fn 41. Ptr to readpeak function 					*/
  NULL,								/* Fn 42. Ptr to resetpeak function 				*/
  &chan_sv_set_flags,				/* Fn 43. Ptr to set_flags function 				*/
  &chan_sv_read_flags,				/* Fn 44. Ptr to read_flags function 				*/
  NULL,								/* Fn 45. Ptr to write_map function 				*/
  NULL,								/* Fn 46. Ptr to read_map function 					*/
  NULL,								/* Fn 47. Ptr to set_refzero function 				*/
  NULL,								/* Fn 48. Ptr to read_refzero function 				*/
  NULL,								/* Fn 49. Ptr to undo_refzero function 				*/
  &chan_sv_set_current,				/* Fn 50. Ptr to set_current function				*/
  &chan_sv_read_current,			/* Fn 51. Ptr to read_current function				*/
  NULL,								/* Fn 52. Ptr to tedsid function 					*/
  &chan_set_source,					/* Fn 53. Ptr to set_source function				*/
  &chan_sv_set_dither,				/* Fn 54. Ptr to set dither function				*/
  &chan_sv_read_dither,				/* Fn 55. Ptr to read dither function				*/
  &chan_sv_set_balance,				/* Fn 56. Ptr to set balance function				*/
  &chan_sv_read_balance,			/* Fn 57. Ptr to read balance function				*/
  NULL,								/* Fn 58. Ptr to set info function					*/
  NULL,								/* Fn 59. Ptr to read info function					*/
  NULL,								/* Fn 60. Ptr to write output function				*/
  NULL,								/* Fn 61. Ptr to read input function				*/
  NULL,								/* Fn 62. Ptr to read output function				*/
  &chan_sv_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_sv_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  NULL,								/* Fn 72. Ptr to txdrzero_zero function				*/
  NULL,								/* Fn 73. Ptr to readcycle function					*/
  NULL,								/* Fn 74. Ptr to resetcycles function 				*/
  NULL,								/* Fn 75. Ptr to setpkthreshold function 			*/
  NULL,								/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_sv_write_faultmask,			/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_sv_read_faultstate,			/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  NULL,								/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_install
  FUNCTION DETAILS  : Install a 2GP1SV card on the selected channel(s).

					  On entry:

					  base		Base channel number

********************************************************************************/

uint32_t hw_2gp1sv_install(int slot, int* numinchan, int* numoutchan, int* nummiscchan,
	hw_2gp1sv * hw, int flags)
{
	info_2gp1sv* i;
	uint32_t eecal_addr = CHAN_EE_CALSTART;		/* Start of channel EEPROM calibration area	  */
	uint32_t eecfg_addr = CHAN_EEBANK_DATASTART;	/* Start of channel EEPROM configuration area */
	uint32_t eerdcfg_addr = CHAN_EEBANK_DATASTART;	/* Address for reading EEPROM configuration	  */
	int input;
	int sv;
	uint8_t* workspace;
	uint32_t status = 0;
	chandef* def;

	/***************************************************************************
	* Perform initial allocation of channels and setting of default parameters *
	***************************************************************************/

#ifndef TEST8CHAN

	if (flags & DO_ALLOCATE) {

		/* The workspace is used by all inputs on the board, so determine the
		   address of the workspace area of the first input to be allocated and use
		   that for all input workspaces.
		*/

		workspace = inchaninfo[totalinchan].hw.i_2gp1sv.work;
		memset(workspace, 0, WSIZE_2GP1SV);

	}

#endif

	/* Allocate the two general-purpose transducers */

	for (input = 0; input < 2; input++) {

		def = &inchaninfo[slot_status[slot].baseinchan + input];

#ifndef TEST8CHAN

		/* Set defaults for general-purpose transducer channel */

		chan_gp_default(def, (totalinchan + input));

		if (flags & DO_ALLOCATE) {

			i = &def->hw.i_2gp1sv;

			/* Define local hardware/workspace settings */

			i->hw = hw;										/* Pointer to 2GP1SV hardware base		*/
			i->input = input;	 								/* Input channel number on 2GP1SV card	*/
			i->relaystate = &workspace[0];							/* Pointer to local workspace		    */
			i->ledmask = (input) ? (1 << LED_1) : (1 << LED_0);	/* Mask for channel ident LED			*/
			i->ledstate = &workspace[1];							/* Pointer to local workspace			*/

			init_fntable(def, hw2gp1sv_gp_fntable);	/* Set pointers to control functions */

			SETDEF(slot, TYPE_GP, input, (CHANTYPE_INPUT | (totalinchan + input)));

			/* Allocate space in EEPROM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
			def->eecal_start = eecal_addr; eecal_addr += SAVED_CHANCAL_SIZE; def->eecal_size = SAVED_CHANCAL_SIZE;

			/* Allocate space in DSP B memory for linearisation map */

			if (!(def->mapptr = (float*)alloc_dspb_general(sizeof(def->map)))) diag_error(DIAG_MALLOC);

			/* Define the source and destination addresses for the channel */

			def->sourceptr = (input) ? &(hw->u.rd.adc2) : &(hw->u.rd.adc1);
			def->outputptr = cnet_get_slotaddr(totalinchan + input, PROC_DSPB);

			/* Initialise the TEDS interface */

			hw_2gp1sv_1w_init((info_2gp1sv*)i, input);

			/* Initialise the I2C interface */

			hw_2gp1sv_i2c_stop((info_2gp1sv*)i);

		}

		/* Restore current calibration and configuration data */

		def->status = restore_chan_calibration(def);
		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

#else

		/* Copy definition from mainboard transducer 0 */

		memcpy(def, &inchaninfo[0], sizeof(chandef));
		def->chanid = slot_status[slot].baseinchan + input;
		SETDEF(slot, TYPE_GP, input, (CHANTYPE_INPUT | (totalinchan + input)));

#endif

	}

	/* Allocate the SV output */

	if (!(flags & NO_SV)) {

		for (sv = 0; sv < 1; sv++) {

			def = &miscchaninfo[slot_status[slot].basemiscchan + sv];

#ifndef TEST8CHAN

			/* Set defaults for servo-valve output channel */

			chan_sv_default(def, (totalmiscchan + sv));

			if (flags & DO_ALLOCATE) {

				//      cfg_sv *cfg;

				i = &def->hw.i_2gp1sv;
				//      cfg = &def->cfg.i_sv;

					  /* Define local hardware/workspace settings */

				i->hw = hw;				/* Pointer to 2GP1SV hardware base		*/
				i->output = sv;				/* Output channel number on 2GP1SV card */
				i->ledmask = 1 << LED_SV;	/* Mask for channel ident LED			*/
				i->ledstate = &workspace[1];	/* Pointer to local workspace			*/

				init_fntable(def, hw2gp1sv_sv_fntable);	/* Set pointers to control functions */

				SETDEF(slot, TYPE_SV, sv, (CHANTYPE_MISC | (totalmiscchan + sv)));

				/* Allocate space in EEPROM for configuration and calibration data */

				ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
				def->eecal_start = eecal_addr; eecal_addr += SAVED_CHANCAL_SIZE; def->eecal_size = SAVED_CHANCAL_SIZE;

				//      cfg->gain = 1.0;

					  /* Define the source and destination addresses for the channel */

				def->sourceptr = NULL;
				def->outputptr = (int*)&(hw->u.wr.sv0);

			}

			/* Restore current calibration and configuration data */

			def->status = restore_chan_calibration(def);
			if (!(flags & DO_FLUSH)) {
				def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
			}
			status |= def->status;

#endif

		}
	}

	/***************************************************************************
	* Perform hardware configuration                                           *
	***************************************************************************/

	/* Configure the two general-purpose transducers */

	for (input = 0; input < 2; input++) {

		chandef* def = &inchaninfo[slot_status[slot].baseinchan + input];

#ifndef TEST8CHAN

		cal_gp* cal;
		cfg_gp* cfg;

		cal = &def->cal.u.c_gp;
		cfg = &def->cfg.i_gp;

		/* Trap for illegal negative gaintrim value caused by upgrade from version
		   prior to v1.0.0099. Replace by value of 1.0.
		*/

		if (cfg->neg_gaintrim < 0.5) cfg->neg_gaintrim = 1.0;

		/* Restore the user configuration settings */

		/* Update channel flags. Force simulation mode off and disable cyclic event generation. */

		def->set_flags(def, (cfg->flags & ~(FLAG_SIMULATION | FLAG_CYCEVENT)), 0xFFFFFFFF, NO_UPDATECLAMP, NO_MIRROR);

		def->setgain(def, cfg->gain,
			SETGAIN_RETAIN_GAINTRIM | chan_extract_gainflags(cfg->flags),
			NO_MIRROR);

		def->offset = cal->cal_offset;
		def->setexc(def, cfg->flags & FLAG_ACTRANSDUCER, cfg->excitation, cfg->phase, NO_MIRROR);
		def->set_txdrzero(def, cfg->txdrzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);
		def->set_refzero(def, def->refzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);

		/* Set the linearisation map */

		def->write_map(def, (cfg->flags & FLAG_LINEARISE) >> BIT_LINEARISE, def->map, def->filename, FALSE, NO_MIRROR);

		/* Set filter 1 characteristic */

		set_filter(def, FILTER_ALL, 0, def->filter1.type & FILTER_ENABLE, def->filter1.type & FILTER_TYPEMASK, def->filter1.order, def->filter1.freq, def->filter1.bandwidth, NO_MIRROR);

		/* Set the fault mask */

		def->writefaultmask(def, cfg->faultmask, NO_MIRROR);

#endif

		/* Update the shared channel definition */

		update_shared_channel(def);

	}

#ifndef TEST8CHAN

	/* Configure the SV output */

	if (!(flags & NO_SV)) {

		for (sv = 0; sv < 1; sv++) {

			chandef* def = &miscchaninfo[slot_status[slot].basemiscchan + sv];

			cal_sv* cal;
			cfg_sv* cfg;

			i = &def->hw.i_2gp1sv;
			cal = &def->cal.u.c_sv;
			cfg = &def->cfg.i_sv;

			/* Set offset */

			def->offset = cal->cal_offset;

			/* Update channel flags */

			def->set_flags(def, cfg->flags, 0xFFFFFFFF, NO_UPDATECLAMP, NO_MIRROR);

			/* Set the valve type and current (for current driven valves), configure the dither output and
			   initialise the DAC output. The call to set_current has the SETGAIN_RETAIN_GAINTRIM flag set
			   since this operates in the same way as the set_gain() function for other types.
			*/

			def->set_current(def, cfg->svtype, cfg->current, SETGAIN_RETAIN_GAINTRIM, NO_MIRROR);
			def->set_dither(def, cfg->dither_enable, cfg->dither_ampl, cfg->dither_freq, NO_MIRROR);
			def->set_balance(def, cfg->balance, NO_MIRROR);
			def->writedac(def, 0.0);
			*((int16_t*)(def->outputptr)) = 0;

			/* Set the fault mask */

			def->writefaultmask(def, cfg->faultmask, NO_MIRROR);

			/* Update the shared channel definition */

			update_shared_channel(def);
		}
	}

#endif

	/* Update channel counts */

	if (flags & DO_ALLOCATE) {
		totalinchan += 2;
		physinchan += 2;
		*numinchan = 2;
		*numoutchan = 0;
#ifndef TEST8CHAN
		if (!(flags & NO_SV)) totalmiscchan += 1;
		if (!(flags & NO_SV)) *nummiscchan = 1;
#endif
	}

#ifndef TEST8CHAN

	/* Enable the monitor and SV DACs */

	hw->u.wr.dacen |= DAC_EN;

#endif

	return(status & (FLAG_BADLIST | FLAG_OLDLIST));
}



/********************************************************************************
  FUNCTION NAME     : hw_2gp1sv_save
  FUNCTION DETAILS  : Save the 2GP1SV channel settings to EEPROM or
					  battery-backed RAM if required.

					  If a bad list or an old list format was detected on any
					  channels, then all of the channels must be updated.

					  On entry:

					  slot		Slot number being saved.

					  status	System configuration status. This indicates if
								a full save operation is required because an
								old or bad format list was detected during
								initialisation.

********************************************************************************/

void hw_2gp1sv_save(int slot, uint32_t status)
{
	int input;
	int sv;
	chandef* def;

	/* Update the input channels */

	for (input = 0; input < 2; input++) {
		def = &inchaninfo[slot_status[slot].baseinchan + input];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

	/* Update the SV channel */

	for (sv = 0; sv < 1; sv++) {
		def = &miscchaninfo[slot_status[slot].basemiscchan + sv];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

}



/********************************************************************************
  FUNCTION NAME     : hw_2tx_save
  FUNCTION DETAILS  : Save the 2TX channel settings to EEPROM or
					  battery-backed RAM if required.

					  If a bad list or an old list format was detected on any
					  channels, then all of the channels must be updated.

					  On entry:

					  slot		Slot number being saved.

					  status	System configuration status. This indicates if
								a full save operation is required because an
								old or bad format list was detected during
								initialisation.

********************************************************************************/

void hw_2tx_save(int slot, uint32_t status)
{
	int input;
	chandef* def;

	/* Update the input channels */

	for (input = 0; input < 2; input++) {
		def = &inchaninfo[slot_status[slot].baseinchan + input];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

}

