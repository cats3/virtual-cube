/********************************************************************************
 * MODULE NAME       : hw_4ad2da.c												*
 * PROJECT			 : Control Cube												*
 * MODULE DETAILS    : Interface routines for 4AD2DA card (or main board		*
 *					   equivalent functions).									*
 *																				*
 ********************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <stdint.h>
#include "porting.h"

#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"



 /********************************************************************************
   FUNCTION NAME     : hw_4ad2da_1w_init
   FUNCTION DETAILS  : Initialise 1-wire port.
 ********************************************************************************/

void hw_4ad2da_1w_init(info_4ad2da* info)
{
	//hw_4ad2da* base;

	//base = info->hw;
	//base->u.wr.onewdat = ONEW_RESET;	/* Reset 1-wire bus */
}



/********************************************************************************
  FUNCTION NAME     : hw_4ad2da_1w_reset
  FUNCTION DETAILS  : Reset all devices on 1-wire bus.

					  On exit:

					  Returns zero if a device is present on the 1-wire bus.

********************************************************************************/

int hw_4ad2da_1w_reset(info_4ad2da* info)
{
	//hw_4ad2da* base;
	//int present;

	//base = info->hw;

	//while (base->u.rd.onewdat & ONEW_BUSY);	/* Wait for not busy */

	//base->u.wr.onewdat = ONEW_RESET;		/* Reset 1-wire bus */

	//while (base->u.rd.onewdat & ONEW_BUSY);	/* Wait for completion */

	//present = (base->u.rd.onewdat & ONEW_PRES) ? 1 : 0;	/* Read presence flag */

	//return(present);
	return (0);
}



/********************************************************************************
  FUNCTION NAME     : hw_4ad2da_1w_write
  FUNCTION DETAILS  : Write a byte to the 1-wire bus.
********************************************************************************/

void hw_4ad2da_1w_write(info_4ad2da* info, uint8_t data)
{
	//hw_4ad2da* base;
	//int n;

	//base = info->hw;

	//n = 8;
	//while (n) {
	//	while (base->u.rd.onewdat & ONEW_BUSY);				/* Wait for not busy */
	//	base->u.wr.onewdat = ONEW_WRITE0 | (data & 0x01);	/* Write bit to 1-wire bus */
	//	data = data >> 1;
	//	n--;
	//}

}



/********************************************************************************
  FUNCTION NAME     : hw_4ad2da_1w_read
  FUNCTION DETAILS  : Read a byte from the 1-wire bus.
********************************************************************************/

uint8_t hw_4ad2da_1w_read(info_4ad2da* info)
{
	//hw_4ad2da* base;
	//uint8_t data = 0;
	//int n;

	//base = info->hw;

	//n = 8;
	//while (n) {
	//	while (base->u.rd.onewdat & ONEW_BUSY);	/* Wait for not busy */
	//	base->u.wr.onewdat = ONEW_READ;			/* Initiate read operation */
	//	while (base->u.rd.onewdat & ONEW_BUSY);	/* Wait for completion */
	//	data = data >> 1;
	//	if (base->u.rd.onewdat & ONEW_DATA) {	/* Test 1-wire input */
	//		data |= 0x80;						/* Set received data bit if required */
	//	}
	//	n--;
	//}

	//return(data);						/* Return data value */
	return (0);
}



/********************************************************************************
  FUNCTION NAME     : hw_4ad2da_led_ctrl
  FUNCTION DETAILS  : Control LEDs on 4AD2DA card.
********************************************************************************/

void hw_4ad2da_led_ctrl(chandef* def, int state)
{
	fprintf(stdout, "%s: [%d]\n", __FUNCTION__, state);
	//info_4ad2da* i;
	//uint8_t newstate;

	//i = &def->hw.i_4ad2da;	/* 4AD2DA specific information 	*/

	//newstate = *(i->ledstate) & ~(i->ledmask);	/* Clear current LED state 	*/
	//newstate |= (state) ? (i->ledmask) : 0;		/* Update with new state	*/

	//*(i->ledstate) = newstate;

	//if (hw_4ad2da_1w_reset(i)) return;	/* Return if nothing present on bus 	*/

	///* When sending the state to the DS2408 a zero output turns the LED on.
	//   Therefore we must send the inverse of the newstate value.
	//*/

	//hw_4ad2da_1w_write(i, 0xCC);		/* Send SKIP ROM command				*/
	//hw_4ad2da_1w_write(i, 0x5A);		/* Send CHANNEL ACCESS WRITE command	*/
	//hw_4ad2da_1w_write(i, ~newstate);	/* Send data							*/
	//hw_4ad2da_1w_write(i, newstate);	/* Send inverted data					*/

	///* Check for 0xAA byte indicating successful write, then read back PIO
	//   state to check for errors.
	//*/

	//if (hw_4ad2da_1w_read(i) == 0xAA) {
	//	if (hw_4ad2da_1w_read(i) == ~newstate) {
	//	}
	//}

	//hw_4ad2da_1w_reset(i);	/* Reset 1-wire bus */
}



#if 0

/********************************************************************************
  FUNCTION NAME     : hw_4ad2da_read_adc
  FUNCTION DETAILS  : Read the current ADC result for the selected input
					  channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

float hw_4ad2da_read_adc(chandef* def)
{
	return(*((float*)(def->outputptr)));
}

#endif



/* Table to convert from channel number in the range 0 to 5 into the bit position
   required to drive the LED. Note that channels 0,1,2,3 correspond to the four
   input channels, whilst channels 4 and 5 correspond to the two output channels.
*/

int hw4ad2da_led_conversion[] = { 0,1,3,4,2,5 };

/****************************************************
* Table of handler functions for mainboard channels *
****************************************************/

void* hw4ad2da_ad_fntable[] = {
  &read_chan_status,				/* Fn  0. Ptr to readstatus function 				*/
  &chan_ad_set_pointer,				/* Fn  1. Ptr to setpointer function				*/
  &chan_ad_get_pointer,				/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  &save_chan_calibration,			/* Fn  6. Ptr to savecalibration function 			*/
  &restore_chan_calibration,		/* Fn  7. Ptr to restorecalibration function 		*/
  &flush_chan_calibration,			/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function	 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  &chan_ad_set_gain,				/* Fn 13. Ptr to setgain function 					*/
  &chan_ad_read_gain,				/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  &hw_4ad2da_led_ctrl,				/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  &chan_read_adc,					/* Fn 23. Ptr to readadc function 					*/
  &set_filter,						/* Fn 24. Ptr to setfilter function	 				*/
  &read_filter,						/* Fn 25. Ptr to readfilter function 				*/
  NULL,								/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function 			*/
  &chan_ad_set_caltable,			/* Fn 28. Ptr to set_caltable function 				*/
  &chan_ad_read_caltable,			/* Fn 29. Ptr to read_caltable function 			*/
  &chan_ad_set_calgain,				/* Fn 30. Ptr to set_calgain function 				*/
  &chan_ad_read_calgain,			/* Fn 31. Ptr to read_calgain function 				*/
  &chan_ad_set_caloffset,			/* Fn 32. Ptr to set_caloffset function 			*/
  &chan_ad_read_caloffset,			/* Fn 33. Ptr to read_caloffset function 			*/
  &chan_set_cnet_output,			/* Fn 34. Ptr to set_cnet_slot function 			*/
  &chan_ad_set_gaintrim,			/* Fn 35. Ptr to set_gaintrim function 				*/
  &chan_ad_read_gaintrim,			/* Fn 36. Ptr to read_gaintrim function 			*/
  &set_ip_offsettrim,				/* Fn 37. Ptr to set_offsettrim function 			*/
  &read_ip_offsettrim,				/* Fn 38. Ptr to read_offsettrim function 			*/
  &chan_ad_set_txdrzero,			/* Fn 39. Ptr to set_txdrzero function				*/
  &chan_ad_read_txdrzero,			/* Fn 40. Ptr to read_txdrzero function				*/
  &read_peakinfo,					/* Fn 41. Ptr to readpeakinfo function 				*/
  &reset_peaks,						/* Fn 42. Ptr to resetpeak function 				*/
  &chan_ad_set_flags,				/* Fn 43. Ptr to set_flags function 				*/
  &chan_ad_read_flags,				/* Fn 44. Ptr to read_flags function 				*/
  &chan_ad_write_map,				/* Fn 45. Ptr to write_map function 				*/
  &chan_ad_read_map,				/* Fn 46. Ptr to read_map function 					*/
  &chan_ad_set_refzero,				/* Fn 47. Ptr to set_refzero function 				*/
  &read_refzero,					/* Fn 48. Ptr to read_refzero function 				*/
  &undo_refzero,					/* Fn 49. Ptr to undo_refzero function 				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  NULL,								/* Fn 52. Ptr to tedsid function 					*/
  NULL,								/* Fn 53. Ptr to set_source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  NULL,								/* Fn 58. Ptr to set info function					*/
  NULL,								/* Fn 59. Ptr to read info function					*/
  NULL,								/* Fn 60. Ptr to write output function				*/
  NULL,								/* Fn 61. Ptr to read input function				*/
  NULL,								/* Fn 62. Ptr to read output function				*/
  &chan_ad_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_ad_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  &chan_ad_txdrzero_zero,			/* Fn 72. Ptr to txdrzero_zero function				*/
  &chan_read_cyclecount,			/* Fn 73. Ptr to readcycle function					*/
  &reset_cycles,					/* Fn 74. Ptr to resetcycles function 				*/
  &chan_ad_set_peak_threshold,		/* Fn 75. Ptr to setpkthreshold function 			*/
  &chan_ad_read_peak_threshold,		/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_ad_dummy_write_faultmask,	/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_ad_dummy_read_faultstate,	/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  &chan_ad_refzero_zero,			/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
};

void* hw4ad2da_da_fntable[] = {
  &read_chan_status,				/* Fn  0. Ptr to readstatus function 				*/
  &chan_da_set_pointer,				/* Fn  1. Ptr to setpointer function				*/
  &chan_da_get_pointer,				/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  &save_chan_calibration,			/* Fn  6. Ptr to savecalibration function 			*/
  &restore_chan_calibration,		/* Fn  7. Ptr to restorecalibration function 		*/
  &flush_chan_calibration,			/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  &chan_da_set_gain,				/* Fn 13. Ptr to setgain function 					*/
  &chan_da_read_gain,				/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  &hw_4ad2da_led_ctrl,				/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  NULL,								/* Fn 23. Ptr to readadc function 					*/
  &set_filter,						/* Fn 24. Ptr to setfilter function 				*/
  &read_filter,						/* Fn 25. Ptr to readfilter function 				*/
  &chan_write_dac,					/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function 			*/
  &chan_da_set_caltable,			/* Fn 28. Ptr to set_caltable function 				*/
  &chan_da_read_caltable,			/* Fn 29. Ptr to read_caltable function 			*/
  &chan_da_set_calgain,				/* Fn 30. Ptr to set_calgain function 				*/
  &chan_da_read_calgain,			/* Fn 31. Ptr to read_calgain function 				*/
  &chan_da_set_caloffset,			/* Fn 32. Ptr to set_caloffset function 			*/
  &chan_da_read_caloffset,			/* Fn 33. Ptr to read_caloffset function 			*/
  &chan_set_cnet_input,				/* Fn 34. Ptr to set_cnet_slot function 			*/
  &chan_da_set_gaintrim,			/* Fn 35. Ptr to set_gaintrim function 				*/
  &chan_da_read_gaintrim,			/* Fn 36. Ptr to read_gaintrim function 			*/
  &set_op_offsettrim,				/* Fn 37. Ptr to set_offsettrim function	 		*/
  &read_op_offsettrim,				/* Fn 38. Ptr to read_offsettrim function 			*/
  NULL,								/* Fn 39. Ptr to set_txdrzero function				*/
  NULL,								/* Fn 40. Ptr to read_txdrzero function				*/
  NULL,								/* Fn 41. Ptr to readpeak function 					*/
  NULL,								/* Fn 42. Ptr to resetpeak function 				*/
  &chan_da_set_flags,				/* Fn 43. Ptr to set_flags function 				*/
  &chan_da_read_flags,				/* Fn 44. Ptr to read_flags function 				*/
  NULL,								/* Fn 45. Ptr to write_map function 				*/
  NULL,								/* Fn 46. Ptr to read_map function 					*/
  NULL,								/* Fn 47. Ptr to set_refzero function 				*/
  NULL,								/* Fn 48. Ptr to read_refzero function				*/
  NULL,								/* Fn 49. Ptr to undo_refzero function				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  NULL,								/* Fn 52. Ptr to tedsid function 					*/
  &chan_set_source,					/* Fn 53. Ptr to set_source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  NULL,								/* Fn 58. Ptr to set info function					*/
  NULL,								/* Fn 59. Ptr to read info function					*/
  NULL,								/* Fn 60. Ptr to write output function				*/
  NULL,								/* Fn 61. Ptr to read input function				*/
  NULL,								/* Fn 62. Ptr to read output function				*/
  &chan_da_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_da_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  NULL,								/* Fn 72. Ptr to txdrzero_zero function				*/
  NULL,								/* Fn 73. Ptr to readcycle function					*/
  NULL,								/* Fn 74. Ptr to resetcycles function 				*/
  NULL,								/* Fn 75. Ptr to setpkthreshold function 			*/
  NULL,								/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_da_write_faultmask,			/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_da_read_faultstate,			/* Fn 78. Ptr to readfaultstate function 			*/
  NULL,								/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  NULL,								/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  NULL,								/* Fn 81. Ptr to setfullscale function				*/
  NULL,								/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};



/********************************************************************************
  FUNCTION NAME     : hw_4ad2da_install
  FUNCTION DETAILS  : Install 4AD2DA hardware on the selected channel(s).

					  On entry:

					  slot			Slot number being installed.

					  numinchan		Points to the variable holding the number of
									input channels installed for this slot.

					  numoutchan	Points to the variable holding the number of
									output channels installed for this slot.

					  nummiscchan	Points to the variable holding the number of
									miscellaneous channels installed for this slot.

					  flags			Controls if channel allocation is performed.
									If not, then only initialisation operations
									are performed.
********************************************************************************/

uint32_t hw_4ad2da_install(int slot, int* numinchan, int* numoutchan, int* nummiscchan,
	hw_4ad2da* hw, int flags)
{
	info_4ad2da* i;
	uint32_t eecal_addr = CHAN_EE_CALSTART;		/* Start of channel EEPROM calibration area	  */
	uint32_t eecfg_addr = CHAN_EEBANK_DATASTART;	/* Start of channel EEPROM configuration area */
	uint32_t eerdcfg_addr = CHAN_EEBANK_DATASTART;	/* Address for reading EEPROM configuration	  */
	int input;
	int output;
	uint8_t* workspace;
	uint32_t status = 0;
	chandef* def;

	/***************************************************************************
	* Perform initial allocation of channels and setting of default parameters *
	***************************************************************************/

	if (flags & DO_ALLOCATE) {

		/* The workspace is used by all inputs on the 2DA4AD board, so determine the
		   address of the workspace area of the first input to be allocated and use
		   that for all input workspaces.
		*/

		workspace = inchaninfo[totalinchan].hw.i_4ad2da.work;
		memset(workspace, 0, WSIZE_4AD2DA);

	}

	/* Allocate the four ADC inputs */

	for (input = 0; input < 4; input++) {

		def = &inchaninfo[slot_status[slot].baseinchan + input];

		/* Set defaults for high-level ADC input channel */

		chan_ad_default(def, (totalinchan + input));

		if (flags & DO_ALLOCATE) {

			i = &def->hw.i_4ad2da;

			/* Define local hardware/workspace settings */

			i->hw = hw;									/* Pointer to mainboard hardware base	*/
			i->input = input;								/* Input channel number on mainboard    */
			i->ledmask = 1 << hw4ad2da_led_conversion[input];	/* Mask for channel ident LED 		    */
			i->ledstate = &workspace[1];						/* Pointer to local workspace		    */

			init_fntable(def, hw4ad2da_ad_fntable);	/* Set pointers to control functions */

			SETDEF(slot, TYPE_ADC, input, (CHANTYPE_INPUT | (totalinchan + input)));

			/* Allocate space in EEPROM/BBSRAM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
			def->eecal_start = eecal_addr; eecal_addr += SAVED_CHANCAL_SIZE; def->eecal_size = SAVED_CHANCAL_SIZE;

			/* Allocate space in DSP B memory for linearisation map */

			if (!(def->mapptr = (float*)alloc_dspb_general(sizeof(def->map)))) diag_error(DIAG_MALLOC);

			//    cfg->gain = 1.0;			/* Default gain settings */
			//    cfg->gaintrim = 1.0;

				/* Define the source and destination addresses for the channel */

			switch (input) {
			case 0:
				def->sourceptr = &(hw->u.rd.adc1);
				break;
			case 1:
				def->sourceptr = &(hw->u.rd.adc2);
				break;
			case 2:
				def->sourceptr = &(hw->u.rd.adc3);
				break;
			case 3:
				def->sourceptr = &(hw->u.rd.adc4);
				break;
			}
			def->outputptr = cnet_get_slotaddr_out(totalinchan + input, PROC_DSPB);

		}

		/* Restore current calibration and configuration data */

		def->status = restore_chan_calibration(def);
		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

	}

	/* Allocate the two DAC outputs */

	for (output = 0; output < 2; output++) {

		def = &outchaninfo[slot_status[slot].baseoutchan + output];

		/* Set defaults for high-level DAC output channel */

		chan_da_default(def, (totaloutchan + output));

		if (flags & DO_ALLOCATE) {

			//  cfg_da *cfg;

			i = &def->hw.i_4ad2da;
			//  cfg = &def->cfg.i_da;

				/* Define local hardware/workspace settings */

			i->hw = hw;										/* Pointer to mainboard hardware base	*/
			i->output = output;	 								/* Output channel number on mainboard   */
			i->ledmask = 1 << hw4ad2da_led_conversion[output + 4];	/* Mask for channel ident LED 		    */
			i->ledstate = &workspace[1];							/* Pointer to local workspace		    */

			init_fntable(def, hw4ad2da_da_fntable);	/* Set pointers to control functions */

			SETDEF(slot, TYPE_DAC, output, (CHANTYPE_OUTPUT | (totaloutchan + output)));

			/* Allocate space in EEPROM/BBSRAM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
			def->eecal_start = eecal_addr; eecal_addr += SAVED_CHANCAL_SIZE; def->eecal_size = SAVED_CHANCAL_SIZE;

			/* Allocate space in DSP B memory for linearisation map */

			if (!(def->mapptr = (float*)alloc_dspb_general(sizeof(def->map)))) diag_error(DIAG_MALLOC);

			/* Define the source and destination addresses for the channel */

		// def->sourceptr = cnet_get_slotaddr(192 + *totaloutchan, PROC_DSPB);
			def->sourceptr = NULL;
			def->outputptr = (int*)((output == 0) ? &(hw->u.wr.dac0) : &(hw->u.wr.dac1));

		}

		/* Restore current calibration and configuration data */

		def->status = restore_chan_calibration(def);
		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

	}

	/***************************************************************************
	* Perform hardware configuration                                           *
	***************************************************************************/

	/* Configure the four ADC inputs */

	for (input = 0; input < 4; input++) {

		chandef* def;
		cal_ad* cal;
		cfg_ad* cfg;

		def = &inchaninfo[slot_status[slot].baseinchan + input];
		cal = &def->cal.u.c_ad;
		cfg = &def->cfg.i_ad;

		/* Trap for illegal negative gaintrim value caused by upgrade from version
		   prior to v1.0.0099. Replace by value of 1.0.
		*/

		if (cfg->neg_gaintrim < 0.5) cfg->neg_gaintrim = 1.0;

		/* Restore the user configuration settings */

		/* Update channel flags. Force simulation mode off and disable cyclic event generation. */

		def->set_flags(def, (cfg->flags & ~(FLAG_SIMULATION | FLAG_CYCEVENT)), 0xFFFFFFFF, NO_UPDATECLAMP, NO_MIRROR);

		def->setgain(def, cfg->gain,
			SETGAIN_RETAIN_GAINTRIM | chan_extract_gainflags(cfg->flags),
			NO_MIRROR);

		def->offset = cal->cal_offset;
		def->set_txdrzero(def, cfg->txdrzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);
		def->set_refzero(def, def->refzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);

		/* Set the linearisation map */

		def->write_map(def, (cfg->flags & FLAG_LINEARISE) >> BIT_LINEARISE, def->map, def->filename, FALSE, NO_MIRROR);

		/* Set filter 1 characteristic */

		set_filter(def, FILTER_ALL, 0, def->filter1.type & FILTER_ENABLE, def->filter1.type & FILTER_TYPEMASK, def->filter1.order, def->filter1.freq, def->filter1.bandwidth, NO_MIRROR);

		/* Update the shared channel definition */

		update_shared_channel(def);

	}

	/* Configure the two DAC outputs */

	for (output = 0; output < 2; output++) {

		chandef* def;
		cal_da* cal;
		cfg_da* cfg;

		def = &outchaninfo[slot_status[slot].baseoutchan + output];
		cal = &def->cal.u.c_da;
		cfg = &def->cfg.i_da;

		/* Set gain and offset */

	  //  def->setgain(def, cfg->gain, SETGAIN_RETAIN_GAINTRIM, NO_MIRROR);
		def->set_gaintrim(def, &cfg->gaintrim, NULL, FALSE, NO_MIRROR);
		def->offset = cal->cal_offset;

		/* Update channel flags */

		def->set_flags(def, cfg->flags, 0xFFFFFFFF, NO_UPDATECLAMP, NO_MIRROR);

		/* Set the range */

	  //  def->range = 1.0 / cfg->range;

		/* Initialise DAC output */

		def->writedac(def, 0.0);
		*((int16_t*)(def->outputptr)) = 0;

		/* Update the shared channel definition */

		update_shared_channel(def);

	}

	/* Update channel counts */

	if (flags & DO_ALLOCATE) {
		totalinchan += 4;
		physinchan += 4;
		totaloutchan += 2;
		*numinchan = 4;
		*numoutchan = 2;
		*nummiscchan = 0;
	}

	/* Enable the output DACs */

	hw->u.wr.dacen |= DAC_EN;

	return(status & (FLAG_BADLIST | FLAG_OLDLIST));
}



/********************************************************************************
  FUNCTION NAME     : hw_4ad2da_save
  FUNCTION DETAILS  : Save the 4AD2DA channel settings to EEPROM or
					  battery-backed RAM if required.

					  If a bad list or an old list format was detected on any
					  channels, then all of the channels must be updated.

					  On entry:

					  slot		Slot number being saved.

					  status	System configuration status. This indicates if
								a full save operation is required because an
								old or bad format list was detected during
								initialisation.

********************************************************************************/

void hw_4ad2da_save(int slot, uint32_t status)
{
	int input;
	int output;
	chandef* def;

	/* Save the input channels */

	for (input = 0; input < 4; input++) {
		def = &inchaninfo[slot_status[slot].baseinchan + input];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

	/* Save the output channels */

	for (output = 0; output < 2; output++) {
		def = &outchaninfo[slot_status[slot].baseoutchan + output];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

}
