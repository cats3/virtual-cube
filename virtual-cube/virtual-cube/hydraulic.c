/********************************************************************************
 * MODULE NAME       : hydraulic.c												*
 * PROJECT			 : Control Cube												*
 * MODULE DETAILS    : Hydraulic control routines.								*
 ********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>

#include <stdint.h>
#include "porting.h"


 /* The following two lines are required to allow Events.h to compile */

#define SUPGEN
#define Parameter void

#include "shared.h"
#include "hydraulic.h"
#include "defines.h"
#include "nonvol.h"
#include "asmcode.h"
#include "cnet.h"
#include "system.h"
#include "eventrep.h"
#include "HardwareIndependent/Events.h"
#include "controller.h"
#include "asmcode.h"

//#include "lck.h"

extern volatile uint32_t prd_enable;
extern int CtrlGetSafeToStart(void);

static void hyd_drive(uint8_t set, uint8_t clr);
static void hyd_broadcast_request(uint32_t request);
static int hyd_get_raw_global_status(void);
uint32_t hyd_check_request(uint32_t request);
static void hyd_build_masks(void);
static void hyd_unload_0(void);
static void hyd_unload_1(void);
static void hyd_unload_2(void);
static void hyd_stop_2(void);
static void hyd_unload_3(void);
static int grp_get_status(void);

/* States for emergency stop sense */

#define ESTOP_INVALID	0						/* E-stop invalid state		*/
#define ESTOP_OFF1		1						/* E-stop off state 1		*/
#define ESTOP_OFF2		2						/* E-stop off state 2		*/
#define ESTOP_ON		3						/* E-stop on but unchecked	*/
#define ESTOP_ONCHECK	4						/* E-stop on and validated	*/

static uint32_t estop_state = ESTOP_INVALID;		/* Emergency stop state								*/
static uint32_t estop_ctr[4] = { 0,0,0,0 };			/* Counters for emergency stop filters				*/
static uint32_t estop_off_valid = 0;				/* Flags indicating validated off states			*/

static uint16_t hyd_fault_mask = 0;				/* Mask for hydraulic fault extraction				*/
static uint16_t hyd_fault_mask2 = 0;				/* Mask for rev H board hydraulic fault extraction	*/
static uint16_t hyd_fault_valid_on = 0;			/* Valid hydraulic state e-stop on					*/
static uint16_t hyd_fault_valid_off1 = 0;			/* Valid hydraulic state e-stop off	(combination 1)	*/
static uint16_t hyd_fault_valid_off2 = 0;			/* Valid hydraulic state e-stop off	(combination 2)	*/
static uint16_t hyd_off1_valid = 0;				/* Flags for off state 1 indication					*/
static uint16_t hyd_off2_valid = 0;				/* Flags for off state 2 indication					*/

static uint8_t  hyd_op_state = 0;
static uint8_t  hyd_ip_state = 0;
static uint8_t  hyd_ip_mask = 0;
static uint8_t  hyd_op_mask = 0;
static uint8_t  hyd_op_state2 = 0;					/* Output drive for state 2 						*/
static uint8_t  hyd_op_state3 = 0;					/* Output drive for state 3 						*/
static uint8_t  hyd_op_state4 = 0;					/* Output drive for state 4 						*/
static uint8_t  hyd_set = 0;
static uint8_t  hyd_clr = 0;
static uint32_t filt_ip_state = 0;					/* Filter for external hydraulic state inputs 		*/

static uint32_t hyd_local_state = 0;				/* Local hydraulic state							*/

static uint32_t hyd_estop_slave = 0;				/* Emergency stop relay state						*/
static uint32_t hyd_group = 0;						/* Hydraulic group number							*/

static uint32_t hyd_sp_tracking_control = TRUE;	/* Outer loop setpoint tracking state control flag	*/
static uint32_t hyd_sp_tracking = TRUE;			/* Outer loop setpoint tracking state				*/
static uint32_t hyd_dissipation_timer = 0;			/* Hydraulic pressure dissipation timer				*/

uint32_t hyd_io_state = 0;							/* Raw hydraulic I/O state							*/

/* Local and master state machine controls */

static uint32_t hyd_istate = 0;					/* Internal hydraulic state 						*/
static uint32_t hyd_mstate = 0;					/* Master hydraulic state 							*/
static uint32_t hyd_irequest = 0;					/* Requested internal hydraulic state				*/
static uint32_t hyd_mrequest = 0;					/* Requested master hydraulic state					*/
static int   hyd_timer = 0;						/* Timer for hydraulic timeout delays				*/
static int   sim_timer = 0;						/* Timer for hydraulic simulation delays			*/

/* Global status and request variables */

uint32_t global_hyd_request_out = 0;				/* Global hydraulic request output to CNet			*/
uint32_t global_hyd_request_in = 0;				/* Global hydraulic request input from CNet			*/
uint32_t global_hyd_status_out = 0;				/* Global hydraulic status output to CNet			*/
uint32_t global_hyd_request_ctr = 0;				/* Counter for hydraulic request output auto clear	*/

/* Bit mask for controlling event reporting */

int event_hyd_bitmask = 0;						/* Event report request bitmask						*/

/* Group status */

uint32_t global_grp_status_out = 0;				/* Global group status out to CNet					*/

static uint32_t local_grp_status = 0;				/* Local group status								*/
static uint32_t grp_flag_hydinvalid = 0;			/* Local hydraulic invalid flag						*/
static uint32_t grp_flag_shuntcal = 0;				/* Local shuntcal active flag						*/
static uint32_t grp_flag_invalidpressure = 0;		/* Local invalid pressure state flag				*/
static uint32_t grp_flag_estopfault = 0;			/* Local e-stop fault flag							*/
static uint32_t grp_flag_guardfault = 0;			/* Local guard fault flag							*/
//static uint32_t grp_flag_txdrfault = 0;			/* Local transducer fault flag						*/

/* Digital input states */

uint16_t dig_ip_state = 0;							/* Digital input state (note 16 bit to match exp
												   card hardware register)							*/

												   /* Hydraulic mode/group configuration */

#define HYD_EXTRACT_MODE(x)  (((x) >> HYD_MODE_SHIFT) & 0x03)
#define HYD_EXTRACT_GROUP(x) (((x) >> HYD_GROUP_SHIFT) & 0x03)

/* Inputs used to monitor type 0,2 hydraulic systems */

#define HYD_IP_ESTOP		((hyd_ip_state & 0x01) && (estop_state == ESTOP_ONCHECK))
#define HYD_IP_1			(0x02)
#define HYD_IP_2			(0x04)
#define HYD_IP_3			(0x08)

/* Outputs used to drive type 0,2 hydraulic systems */

#define HYD_OP_1			(0x02)
#define HYD_OP_2			(0x04)
#define HYD_OP_3			(0x08)

/* Table and functions used to convert raw I/O into sensible bit patterns for hydraulic control */

const uint8_t hyd_map_op[] = { 0x00, 0x00, 0x08, 0x08, 0x02, 0x02, 0x0A, 0x0A, 0x01, 0x01, 0x09, 0x09, 0x03, 0x03, 0x0B, 0x0B,
						   0x80, 0x80, 0x88, 0x88, 0x82, 0x82, 0x8A, 0x8A, 0x81, 0x81, 0x89, 0x89, 0x83, 0x83, 0x8B, 0x8B };

#define HYD_CONVERT_OP(x) (hyd_map_op[x & 0x1F])
#define HYD_CONVERT_IP(x) (((x & 0x04) << 1) | ((x & 0x30) >> 3) | ((x & 0x02) >> 1))

#define HYD_GBL_STATE(x)  ((CNET_MEM_READ[CNET_DATASLOT_HYDSTATUS] >> ((x)-1)) & 1)

/* The following macro converts a state number into a state mask suitable for transmission
   as part of the CNet hydraulic status.

	State	Mask
	-----	----
	0		00001111
	1		00011110
	2		00111100
	3		01111000
	4		11110000

	The lower four bits are in inverted form, which means that when ORed by the CNet transfer,
	only the "lowest" state will be visible. This allows the master to monitor that all slaves
	reached the required state.

	The upper four bits are in non-inverted form and allow the "highest" state to be determined.
	Currently this part of the status byte is not used, but is intended to allow checking if
	any illegal state exists when initiating hydraulic loading.
*/

#define GEN_STATEMASK(x)  (0x0F << (x))

/* Delay period for master hydraulic timeout. The master state machine waits for this much longer than
   the programmed timeout period to allow for the slaves to generate their default acknowledgement
   before the master times out.
*/

#define HYD_MASTER_DELAY  40

/* Number of consecutive fault conditions that must be detected before the fault state is
   confirmed.
*/

#define HYD_FAULT_TIME	  50	/* 1 second fault confirmation delay */
#define ESTOP_CTR_MAX	  65535	/* Maximum e-stop state counter		 */

/********************************************************************************
  FUNCTION NAME   	: hyd_get_status
  FUNCTION DETAILS  : Returns the current local hydraulic status.

					  The 32 bit word returned indicates the hydraulic status
					  in the following form:

					  Bit 31	SP tracking

								This bit is set when the hydraulics are in the
								off state and setpoint tracking should be
								enabled. The tracking state is enabled after a
								programmable delay after the hydraulics shuts
								off.

					  Bits 0-7	Hydraulic state

								The current hydraulic state as follows:

								0	E-stop disabled
								1	Pressure off
								2	Pilot pressure/Motor running
								3	Low pressure
								4	High pressure/Pressure on

********************************************************************************/

uint32_t hyd_get_status(void)
{
	int state = HYD_ACK_0;

	/* Convert local state flags to hydraulic state */

	if ((~hyd_local_state >> 3) & 1) state = HYD_ACK_4;
	else if ((~hyd_local_state >> 2) & 1) state = HYD_ACK_3;
	else if ((~hyd_local_state >> 1) & 1) state = HYD_ACK_2;
	else if ((~hyd_local_state >> 0) & 1) state = HYD_ACK_1;

	return((hyd_sp_tracking ? 0x80000000 : 0) | state);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_get_global_status
  FUNCTION DETAILS  : Returns the current global hydraulic status.

					  The 32 bit word returned indicates the hydraulic status
					  in the following form:

					  Bits 0-7	Code indicating hydraulic state

								The current hydraulic state as follows:

								0	E-stop disabled
								1	Pressure off
								2	Pilot pressure/Motor running
								3	Low pressure
								4	High pressure/Pressure on

					  Bit 8		Emergency stop invalid

								Indicates that the emergency stop chain has detected a
								fault (or it has not yet been validated).

					  Bit 9		Master emergency stop not validated

								Indicates that the master emergency stop circuit has
								not been validated.

					  Bit 10	Hand controller emergency stop not validated

********************************************************************************/

uint32_t hyd_get_global_status(void)
{
	int status;
	int ack = HYD_ACK_0;

	status = hyd_get_raw_global_status();	/* Get raw global hydraulic status */

	/* Convert to hydraulic state */

	if ((status >> 3) & 1) ack = HYD_ACK_4;
	else if ((status >> 2) & 1) ack = HYD_ACK_3;
	else if ((status >> 1) & 1) ack = HYD_ACK_2;
	else if ((status >> 0) & 1) ack = HYD_ACK_1;

	if ((estop_state == ESTOP_INVALID) || (estop_state == ESTOP_ON)) ack |= HYD_ACK_INVALID;
	if (!(estop_off_valid & ESTOP_OFF1)) ack |= HYD_ACK_INVALID_MASTER;
	if (!(estop_off_valid & ESTOP_OFF2)) ack |= HYD_ACK_INVALID_HC;

	return(ack);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_unload
  FUNCTION DETAILS  : Causes the hydraulic system to unload pressure immediately.
********************************************************************************/

void hyd_unload(void)
{
	switch (snvbs->hyd_type & HYD_TYPE_MASK) {
	case 0:	/* Direct hydraulic control */
		hyd_unload_0();
		break;
	case 1:	/* Hydraulic simulation */
		hyd_unload_1();
		break;
	case 2:	/* Pump control */
		hyd_unload_2();
		break;
	case 3:	/* Pump simulation */
		hyd_unload_3();
		break;
	}
	hyd_broadcast_request(REQUEST_GLBL_SHUTDOWN);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_set_dissipation
  FUNCTION DETAILS  : Define hydraulic dissipation time
********************************************************************************/

uint32_t hyd_set_dissipation(uint32_t time)
{
	uint32_t d_time;

	if ((hyd_get_status() & HYD_STATUS_MASK) >= HYD_ACK_ON) return(HYDRAULICS_ACTIVE);

	d_time = time / 2;
	SNV_UPDATE(hyd_dissipation, d_time);
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_read_dissipation
  FUNCTION DETAILS  : Read hydraulic dissipation time
********************************************************************************/

uint32_t hyd_read_dissipation(void)
{
	return(snvbs->hyd_dissipation * 2);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_set_name
  FUNCTION DETAILS  : Define hydraulic name
********************************************************************************/

uint32_t hyd_set_name(char* name)
{
	if ((hyd_get_status() & HYD_STATUS_MASK) >= HYD_ACK_ON) return(HYDRAULICS_ACTIVE);

	memcpy(snvbs->hyd_name, name, sizeof(snvbs->hyd_name));
	CtrlSnvCB(&snvbs->hyd_name, sizeof(snvbs->hyd_name));
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_read_name
  FUNCTION DETAILS  : Read hydraulic name
********************************************************************************/

void hyd_read_name(char* name)
{
	memcpy(name, snvbs->hyd_name, sizeof(snvbs->hyd_name));
}



/********************************************************************************
  FUNCTION NAME   	: hyd_initialise
  FUNCTION DETAILS  : Initialise hydraulic interface.
********************************************************************************/

void hyd_initialise(void)
{
	//uint32_t hwflags;

	hyd_op_state = 0;
	dsp_spi_send(0);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_init_hw_masks
  FUNCTION DETAILS  : Initialise hydraulic control masks to suit hardware and
					  wiring variants and hydraulic type options.

					  The loom_type variable gives information on the the
					  internal wiring loom used to connect the back panel to
					  the main board. On revision F & G systems, there was an
					  error in the design that can be corrected by means of a
					  loom change. However, the loom change requires some
					  firmware support to work correctly. The code in case 1
					  below is present so that the option to change the loom
					  is available if required.

					  On revision H and later systems, the design error was
					  corrected.

					  In all cases loom type 2 is been used to override the
					  validation of the hand controller e-stop.

  To aid in understanding the operation of the emergency stop sensing, the
  following diagrams and tables show the connections and behaviour of both
  types of emergency stop wiring as present on Rev F & G systems and on
  Rev H and later systems.

  O in the diagram represents a switch contact and EMxH/EMxL represent
  monitor points.

  Rev F & G
  ---------

	  +24V

  ES  O

		EM1H

  HC  O

		EM2H

	  RLY

		EM1L

  HC  O

		EM2L

  ES  O

	  0V24

  HC1		HC2		ES1		ES2			EM1H	EM1L	EM2H	EM2L
  ---		---		---		---			----	----	----	----

  Closed	Closed	Closed	Closed		On		On		On		On

  Closed	Closed	Open	Open		Off		Off		Off		Off

  Open		Open	Closed	Closed		On		Off		Off		On

  Open		Open	Open	Open		Off		Off		Off		Off




  Rev H and later
  ---------------

	  +24V

  HC  O

		EM2H

  ES  O

		EM1H

	  RLY

		EM1L

  ES  O

		EM2L

  HC  O

	  0V24

  HC1		HC2		ES1		ES2			EM1H	EM1L	EM2H	EM2L
  ---		---		---		---			----	----	----	----

  Closed	Closed	Closed	Closed		On		On		On		On

  Closed	Closed	Open	Open		Off		Off		On		On

  Open		Open	Closed	Closed		Off		Off		Off		Off

  Open		Open	Open	Open		Off		Off		Off		Off


********************************************************************************/

void hyd_init_hw_masks(void)
{
	uint32_t hwflags;
	uint32_t hyd_type = snvbs->hyd_type;
	//uint32_t loom_type = (hpi_read4(HC_STATUS) & HC_LOOMMASK) >> 24;
	uint32_t loom_type = 2;		//TODO: hand controller support
	uint32_t hwver;

	hwver = get_hw_version(NULL, &hwflags);

	/* Initialise masks for e-stop state extraction */

	if (hwflags & HW_FLAG_REVF) {

		/* Revision F or later hardware */


		/* For revision H or later hardware we set the upper byte of
		   the loom type variable since the hardware changes require
		   different processing.
		*/

		if (hwver > 0x07) loom_type |= 0x0100;

		switch (loom_type) {

		case 0x0000: /* Loom type 0 (incorrect operation with hand controller & master/slave) */

			hyd_fault_mask = ES_RELAY_SLAVE |
				EM2L_SENSE | EM2H_SENSE |
				EM1H_SENSE | EM1L_SENSE |
				EM_SENSE;

			hyd_fault_mask2 = hyd_fault_mask;

			hyd_fault_valid_on = EM2L_SENSE | EM2H_SENSE | 	/* Valid ON state	*/
				EM1H_SENSE | EM1L_SENSE |
				EM_SENSE;

			/* Set the valid OFF state 1 depending on whether the one channel e-stop flag has been set */

			if (hyd_type & HYD_1CHAN_ESTOP) {
				hyd_fault_valid_off1 = ES_RELAY_SLAVE | EM1L_SENSE | EM2L_SENSE;
			}
			else {
				hyd_fault_valid_off1 = ES_RELAY_SLAVE;
			}

			/* Set the valid OFF state 2 */

			hyd_fault_valid_off2 = ES_RELAY_SLAVE | EM1H_SENSE | EM2L_SENSE;

			hyd_off1_valid = ESTOP_OFF1;
			hyd_off2_valid = ESTOP_OFF2;

			break;

		case 0x0001: /* Loom type 1 (re-wiring of hand controller e-stop for correct slave operation) */

			hyd_fault_mask = ES_RELAY_SLAVE |
				EM2L_SENSE | EM2H_SENSE |
				EM1H_SENSE | EM1L_SENSE |
				EM_SENSE;

			hyd_fault_mask2 = hyd_fault_mask;

			hyd_fault_valid_on = EM2L_SENSE | EM2H_SENSE | 	/* Valid ON state	*/
				EM1H_SENSE | EM1L_SENSE |
				EM_SENSE;


			/* Set the valid OFF state 1 depending on whether the one channel e-stop flag has been set */

			if (hyd_type & HYD_1CHAN_ESTOP) {
				hyd_fault_valid_off1 = ES_RELAY_SLAVE | EM1H_SENSE | EM1L_SENSE | EM2L_SENSE;
			}
			else {
				hyd_fault_valid_off1 = ES_RELAY_SLAVE | EM1H_SENSE | EM2L_SENSE;
			}

			/* Set the valid OFF state 2 */

			hyd_fault_valid_off2 = ES_RELAY_SLAVE;

			hyd_off1_valid = ESTOP_OFF1;
			hyd_off2_valid = ESTOP_OFF2;

			break;

		case 0x0002: /* Disable validation of e-stop on hand controller to prevent problems caused by some
						"unfortunate" assumptions made by Zwick regarding e-stop behaviour. On systems
						where the hand controller e-stop must be wired into an external chain, Zwick had
						bypassed the hand controller e-stop inputs preventing normal validation from
						operating properly.
					 */

			hyd_fault_mask = ES_RELAY_SLAVE |
				EM2L_SENSE | EM2H_SENSE |
				EM1H_SENSE | EM1L_SENSE |
				EM_SENSE;

			hyd_fault_mask2 = hyd_fault_mask;

			hyd_fault_valid_on = EM2L_SENSE | EM2H_SENSE | 	/* Valid ON state	*/
				EM1H_SENSE | EM1L_SENSE |
				EM_SENSE;


			/* Set the valid OFF state 1 depending on whether the one channel e-stop flag has been set */

			if (hyd_type & HYD_1CHAN_ESTOP) {
				hyd_fault_valid_off1 = ES_RELAY_SLAVE | EM1L_SENSE | EM2L_SENSE;
			}
			else {
				hyd_fault_valid_off1 = ES_RELAY_SLAVE;
			}

			/* Set the valid OFF state 2 */

			hyd_fault_valid_off2 = ES_RELAY_SLAVE;

			hyd_off1_valid = ESTOP_OFF1 | ESTOP_OFF2;
			hyd_off2_valid = ESTOP_OFF2;

			break;

		case 0x0102: /* Rev H or later motherboard version of above special case
						0x0002 to disable validation of e-stop on hand controller.
					 */
		case 0x0100: /* Rev H or later mainboard */

			hyd_fault_mask = ES_RELAY_SLAVE |
				EM2L_SENSE | EM2H_SENSE |
				EM1H_SENSE | EM1L_SENSE |
				EM_SENSE;

			/* The hand controller e-stop now affects the state of the EM_SENSE and ES_RELAY_SLAVE
			   inputs (because of possible interaction with the FAULT line if the PULLUP input is
			   interrupted). Since we check their operation when validating the main e-stop switch
			   we can ignore them here. So set the hyd_fault_mask2 state to suit.
			*/

			hyd_fault_mask2 = EM2L_SENSE | EM2H_SENSE |
				EM1H_SENSE | EM1L_SENSE;

			hyd_fault_valid_on = EM2L_SENSE | EM2H_SENSE | 	/* Valid ON state	*/
				EM1H_SENSE | EM1L_SENSE |
				EM_SENSE;


			/* Set the valid OFF state 1 depending on whether the one channel e-stop flag has been set */

			if (hyd_type & HYD_1CHAN_ESTOP) {
				hyd_fault_valid_off1 = ES_RELAY_SLAVE | EM2H_SENSE | EM1L_SENSE | EM2L_SENSE;
			}
			else {
				hyd_fault_valid_off1 = ES_RELAY_SLAVE | EM2H_SENSE | EM2L_SENSE;
			}

			/* Set the valid OFF state 2 */

			hyd_fault_valid_off2 = 0;

			hyd_off1_valid = ESTOP_OFF1;
			hyd_off2_valid = ESTOP_OFF2;

			break;

		}

	}
	else {

		/* Not revision F hardware */

		hyd_fault_mask = ES_RELAY_SLAVE | EM_SENSE;
		hyd_fault_mask2 = hyd_fault_mask;
		hyd_fault_valid_on = EM_SENSE;			/* Valid ON state 	*/
		hyd_fault_valid_off1 = ES_RELAY_SLAVE;	/* Valid OFF state (combination 1)		*/
		hyd_fault_valid_off2 = ES_RELAY_SLAVE;	/* Valid OFF state (combination 2)		*/
		hyd_off1_valid = ESTOP_OFF1 | ESTOP_OFF2;	/* E-stop & hand controller validated	*/
		hyd_off2_valid = ESTOP_OFF2;				/* Hand controller validated			*/
	}
}



#define HYD_FLAG_MASK (HYD_1CHAN_ESTOP | HYD_HC_NOVALID | HYD_DIS_FORCE_SETUP | HYD_DIS_HIGH_PRES | HYD_SOFT_STOP)

/********************************************************************************
  FUNCTION NAME   	: hyd_set_mode
  FUNCTION DETAILS  : Define hydraulic mode
********************************************************************************/

uint32_t hyd_set_mode(uint32_t mode, uint32_t group, uint32_t type)
{
	uint32_t hyd_type = snvbs->hyd_type;
	uint32_t err;

	if (err = system_check_allowed(0, ALLOW_PR_ANY)) return(err);

	hyd_type = (hyd_type & ~HYD_MODE_MASK) | (mode << HYD_MODE_SHIFT);
	hyd_type = (hyd_type & ~HYD_GROUP_MASK) | (group << HYD_GROUP_SHIFT);
	hyd_type = (hyd_type & ~HYD_TYPE_MASK) | (type & HYD_TYPE_MASK);
	hyd_type = (hyd_type & ~HYD_FLAG_MASK) | (type & HYD_FLAG_MASK);

	SNV_UPDATE(hyd_type, hyd_type);

	hyd_init_hw_masks();	/* Update masks for e-stop state extraction */
	hyd_build_masks();		/* Update masks for hydraulic I/O mappings	*/

	/* For simulation modes, set the estop state to indicate that the estop has
	   been checked. This allows simulation mode to operate correctly without the
	   requirement to validate the estop operation. For non-simulation modes
	   force the estop state back to invalid which requires the user to validate
	   the estop before normal operation can be resumed.
	*/

	estop_state = (type & HYD_SIM_MASK) ? ESTOP_ONCHECK : ESTOP_INVALID;

	/* Inform the control loop code of the change in simulation mode */

	CtrlSetSimMode(type & HYD_SIM_MASK);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_read_mode
  FUNCTION DETAILS  : Read hydraulic mode
********************************************************************************/

void hyd_read_mode(uint32_t* mode, uint32_t* group, uint32_t* type)
{
	if (mode) *mode = HYD_EXTRACT_MODE(snvbs->hyd_type);
	if (group) *group = HYD_EXTRACT_GROUP(snvbs->hyd_type);
	//if (type) *type = (snvbs->hyd_type & HYD_TYPE_MASK) | (snvbs->hyd_type & HYD_FLAG_MASK);
	if (type) *type = (snvbs->hyd_type & (HYD_TYPE_MASK | HYD_FLAG_MASK));
}



/********************************************************************************
  FUNCTION NAME   	: hyd_read_guard_mode
  FUNCTION DETAILS  : Read guard mode
						Sets bit 0 of mode to Disable force setup flag
						Sets bit 1 of mode to Disable high pressure flag
********************************************************************************/

void hyd_read_guard_mode(uint32_t* mode)
{
	if (mode) *mode = ((snvbs->hyd_type) & (HYD_DIS_FORCE_SETUP | HYD_DIS_HIGH_PRES));
}



/********************************************************************************
  FUNCTION NAME   	: hyd_read_simulation_mode
  FUNCTION DETAILS  : Read hydraulic simulation mode state
********************************************************************************/

uint32_t hyd_read_simulation_mode(void)
{
	return(snvbs->hyd_type & HYD_SIM_MASK);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_modify_op
  FUNCTION DETAILS  : Modifies the state of the hydraulic outputs.

					  Returns current state of hydraulic outputs. Note that
					  since the updating of the outputs occur asynchronously
					  the returned value will not reflect the latest changes.
********************************************************************************/

uint8_t hyd_modify_op(uint8_t set, uint8_t clr)
{
	atomic_set1(&hyd_set, set);
	atomic_set1(&hyd_clr, clr);
	return(hyd_op_state);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_read_ip
  FUNCTION DETAILS  : Reads the state of the hydraulic inputs.

					  Returns updated state of hydraulic outputs.
********************************************************************************/

uint8_t hyd_read_ip(void)
{
	return(hyd_ip_state);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_set_io_definition
  FUNCTION DETAILS  : Define hydraulic i/o
********************************************************************************/

uint32_t hyd_set_io_definition(uint32_t ident, uint32_t flags, char* label, uint32_t timeout)
{
	if ((hyd_get_status() & HYD_STATUS_MASK) >= HYD_ACK_ON) return(HYDRAULICS_ACTIVE);

	if (ident & 0x0100) {
		SNV_UPDATE(hyd_output[ident & 0x03].flags, flags);
		memcpy(snvbs->hyd_output[ident & 0x03].label, label, sizeof(snvbs->hyd_output[0].label));
		CtrlSnvCB(&snvbs->hyd_output[ident & 0x03].label, sizeof(snvbs->hyd_output[ident & 0x03].label));
		SNV_UPDATE(hyd_output[ident & 0x03].timeout, timeout / 2);

	}
	else {
		SNV_UPDATE(hyd_input[ident & 0x03].flags, flags);
		memcpy(snvbs->hyd_input[ident & 0x03].label, label, sizeof(snvbs->hyd_input[0].label));
		CtrlSnvCB(&snvbs->hyd_input[ident & 0x03].label, sizeof(snvbs->hyd_input[ident & 0x03].label));
		SNV_UPDATE(hyd_input[ident & 0x03].timeout, timeout / 2);
	}
	hyd_build_masks();
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_read_io_definition
  FUNCTION DETAILS  : Read hydraulic i/o definition
********************************************************************************/

void hyd_read_io_definition(uint32_t ident, uint32_t* flags, char* label, uint32_t* timeout)
{
	if (ident & 0x0100) {
		if (flags) *flags = snvbs->hyd_output[ident & 0x03].flags;
		if (label) memcpy(label, snvbs->hyd_output[ident & 0x03].label, sizeof(snvbs->hyd_output[0].label));
		if (timeout) *timeout = snvbs->hyd_output[ident & 0x03].timeout * 2;
	}
	else {
		if (flags) *flags = snvbs->hyd_input[ident & 0x03].flags;
		if (label) memcpy(label, snvbs->hyd_input[ident & 0x03].label, sizeof(snvbs->hyd_input[0].label));
		if (timeout) *timeout = snvbs->hyd_input[ident & 0x03].timeout * 2;
	}
}



/********************************************************************************
  FUNCTION NAME   	: hyd_get_valid
  FUNCTION DETAILS  : Checks if the current hydraulic configuration is valid.
********************************************************************************/

uint32_t hyd_get_valid(void)
{
	uint32_t n_inputs = 0;
	uint32_t n_outputs = 0;
	uint32_t n;

	/* Slaves do not need to have any enabled inputs/outputs, so any
	   configuration is valid.
	*/

	if (!(snvbs->hyd_type & HYD_MODE_MASTER)) return(NO_ERROR);

	for (n = 0; n < 3; n++) {
		if (snvbs->hyd_input[n].flags & HYD_FLAG_AVAILABLE) n_inputs++;
		if (snvbs->hyd_output[n].flags & HYD_FLAG_AVAILABLE) n_outputs++;
	}

	return((n_outputs) ? NO_ERROR : INVALID_HYDRAULIC);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_check_request
  FUNCTION DETAILS  : Check if a requested hydraulic state is valid.
********************************************************************************/

uint32_t hyd_check_request(uint32_t request)
{
	uint32_t status;

	/* Trap special case of hydraulic shutdown request. This is always valid
	   so return NO_ERROR.
	*/

	if ((request & REQUEST_SHUTDOWN) == REQUEST_SHUTDOWN) return(NO_ERROR);

	/* If requested state is OFF then no ESTOP is not an error */

	if (((request & REQUEST_SHUTDOWN) == REQUEST_OFF) &&
		((hyd_get_global_status() & HYD_STATUS_MASK) == HYD_ACK_0)) return(NO_ERROR);

	/* If requested state is lower than current state, then ignore error states */

	if ((request & REQUEST_SHUTDOWN) < (hyd_get_status() & HYD_STATUS_MASK)) return(NO_ERROR);

	/* For non-simulation modes, check the validity of the emergency stop system and
	   confirm no errors from other group members.
	*/

	if (!(snvbs->hyd_type & HYD_SIM_MASK)) {

		/* Check e-stop validity */

		switch (estop_state) {
		case ESTOP_INVALID: return(ESTOP_FAULT);
		case ESTOP_ON: return(ESTOP_CHECK);
		case ESTOP_OFF1:
		case ESTOP_OFF2:
			return(NO_ESTOP);
		}

		/* Check if any error states exist on any members of the group */

		if ((status = grp_get_status()) & (GRP_HYDINVALID | GRP_SHUNTCAL | GRP_STARTINHIBIT | GRP_INVALIDPRESSURE | GRP_TXDRFAULT)) {
			if (status & GRP_HYDINVALID) return(INVALID_HYDRAULIC);
			if (status & GRP_SHUNTCAL) return(SHUNTCAL_ACTIVE);
			if (status & GRP_STARTINHIBIT) {

				/* Start inhibit state is only an error if the current state is off */

				if ((hyd_get_status() & HYD_STATUS_MASK) == HYD_ACK_1) return(START_INHIBIT);
			}
			if (status & GRP_INVALIDPRESSURE) return(INVALID_PRESSURE);
			if (status & GRP_TXDRFAULT) return(TXDR_FAULT);
		}

		if ((hyd_get_global_status() & HYD_STATUS_MASK) == HYD_ACK_0) return(NO_ESTOP);
	}

	/* Check if guard will let you go into high pressure */

	/* Guard states */
#define GUARD_NOSUPPORT	0				/* Guard input not supported					*/
#define GUARD_OPEN		1				/* Guard open									*/
#define GUARD_CLOSED	2				/* Guard closed									*/
#define GUARD_FAULT		3				/* Guard fault condition detected				*/

	if ((request >= REQUEST_4) && (HYD_DIS_HIGH_PRES & snvbs->hyd_type) && ((hw_get_guard_state() == GUARD_OPEN) || (hw_get_guard_state() == GUARD_FAULT))) { //If( (requesting high pressure) and (flag is set) and (guard is open or faulted) )
		return(GUARD_INPUT);
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_read_state
  FUNCTION DETAILS  : Read hydraulic state
********************************************************************************/

void hyd_read_state(uint32_t* state, uint32_t* ack)
{
	uint32_t req;

	/* Get the current request state, converting a shutdown request into an off
	   request for compatibility with the acknowledge state.
	*/

	req = (hyd_mrequest == REQUEST_SHUTDOWN) ? REQUEST_OFF : hyd_mrequest;

	if (state) *state = (hyd_sp_tracking ? 0x80000000 : 0) |
		((hyd_irequest << 24) & 0x7F) |
		(hyd_mrequest << 16) |
		(hyd_istate << 8) |
		hyd_mstate;
	if (ack) *ack = hyd_get_global_status() | (req << 16);
}



/********************************************************************************
  FUNCTION NAME   	: get_estop_state
  FUNCTION DETAILS  : Get e-stop state.
********************************************************************************/

void get_estop_state(uint32_t* estop, uint32_t* slave, uint32_t* state)
{
	if (estop) *estop = HYD_IP_ESTOP;
	if (slave) *slave = !hyd_estop_slave;
	if (state) *state = estop_state;
}



/********************************************************************************
  FUNCTION NAME   	: hyd_control
  FUNCTION DETAILS  : Control hydraulic state
********************************************************************************/

uint32_t hyd_control(uint32_t req)
{
	uint32_t err;

	if (err = hyd_check_request(req)) return(err);
	hyd_mrequest = req;
	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_get_raw_io_state
  FUNCTION DETAILS  : Read current raw I/O state
********************************************************************************/

uint32_t hyd_get_raw_io_state(void)
{
	return(hyd_io_state);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_control_initialise
  FUNCTION DETAILS  : Initialise hydraulic control
********************************************************************************/

void hyd_control_initialise(void)
{
	estop_state = (snvbs->hyd_type & HYD_SIM_MASK) ? ESTOP_ONCHECK : ESTOP_INVALID;
	hyd_modify_op(0, (HYD_OP_1 | HYD_OP_2 | HYD_OP_3));
	hyd_drive(0, (HYD_OP_1 | HYD_OP_2 | HYD_OP_3));
	hyd_build_masks();

	/* Inform the control loop code of the initial simulation mode state */

	CtrlSetSimMode(snvbs->hyd_type & HYD_SIM_MASK);
}



/********************************************************************************
*********************************************************************************
* 																				*
* INTERNAL HYDRAULIC CONTROL FUNCTIONS											*
*																				*
* These functions are not visible outside of the hydraulic module.				*
*																				*
*********************************************************************************
********************************************************************************/



/* States used to control hyd_output_drive routine */

#define OP_OFF			0x01	/* Output off									*/
#define OP_DELAY		0x02	/* Output on, delay for no ack					*/
#define OP_ABORTDELAY	0x03	/* Output off, aborting with switch off delay	*/
#define OP_ON			0x04	/* Output on									*/
#define OP_OFFDELAY		0x05	/* Output turn off delay						*/
#define OP_RESET		0x00	/* Output resetting								*/

#define OP_ONMASK		0x04	/* Mask to extract on state						*/

static int hyd_orequest = 0;	/* Output request flags 						*/
static int hyd_iack = 0;		/* Input acknowledgement flags					*/

/* State and delay values for hyd_output_drive function */

static int hyd_odelay[3] = { 0,0,0 };
static int hyd_ostate[3] = { OP_RESET, OP_RESET, OP_RESET };

/* Table of mask values used to access request/acknowledge flags in hyd_orequest and
   hyd_iack variables.
*/

static const int rmask[3] = { HYD_OP_1, HYD_OP_2, HYD_OP_3 };	/* Output drive bits 		*/
static const int amask[3] = { HYD_IP_1, HYD_IP_2, HYD_IP_3 };	/* Input acknowledge bits 	*/

/********************************************************************************
  FUNCTION NAME   	: hyd_output_drive
  FUNCTION DETAILS  : Drives the physical hydraulic outputs based on request
					  signals from the internal state machines.

					  Implements switch off delay where selected by control
					  flags for each output.

********************************************************************************/

static void hyd_output_drive(void)
{
	int n;

	if (!HYD_IP_ESTOP) {
		hyd_modify_op(0, HYD_OP_1 | HYD_OP_2 | HYD_OP_3);

		/* E-stop pressed or in an invalid state. Ensure that all outputs are turned off and
		   clear all switch off delay timers. Hydraulic input state flags are read from the
		   real inputs or are forced to OFF if no input state is available.
		*/

		for (n = 0; n < 3; n++) {
			hyd_odelay[n] = 0;
			hyd_ostate[n] = OP_OFF;

			if (snvbs->hyd_input[n].flags & HYD_FLAG_AVAILABLE) {
				if (hyd_ip_state & (HYD_IP_1 << n)) hyd_iack |= amask[n]; else hyd_iack &= ~amask[n];
			}
			else {
				hyd_iack &= ~amask[n];
			}
		}
		return;
	}

	for (n = 0; n < 3; n++) {
		switch (hyd_ostate[n]) {
		case OP_OFF:
			if (hyd_orequest & rmask[n]) {
				hyd_odelay[n] = snvbs->hyd_input[n].timeout * 4;
				if (snvbs->hyd_output[n].flags & HYD_FLAG_AVAILABLE) {
					hyd_modify_op(HYD_OP_1 << n, 0);
					hyd_ostate[n] = (snvbs->hyd_input[n].flags & HYD_FLAG_AVAILABLE) ? OP_ON : OP_DELAY;
				}
				else {
					hyd_ostate[n] = OP_DELAY;
				}
			}
			break;
		case OP_DELAY:
			if (!(hyd_orequest & rmask[n])) {
				if (snvbs->hyd_output[n].flags & HYD_FLAG_OFFDELAY) {
					hyd_odelay[n] = snvbs->hyd_output[n].timeout * 4;
					hyd_ostate[n] = OP_ABORTDELAY;
				}
				else {
					hyd_modify_op(0, HYD_OP_1 << n);
					hyd_ostate[n] = OP_OFF;
				}
			}
			else if (hyd_odelay[n] == 0) hyd_ostate[n] = OP_ON;
			break;
		case OP_ABORTDELAY:
		case OP_OFFDELAY:
			if (hyd_orequest & rmask[n]) {
				hyd_odelay[n] = snvbs->hyd_input[n].timeout * 4;
				if (snvbs->hyd_output[n].flags & HYD_FLAG_AVAILABLE) {
					hyd_modify_op(HYD_OP_1 << n, 0);
					hyd_ostate[n] = (snvbs->hyd_input[n].flags & HYD_FLAG_AVAILABLE) ? OP_ON : OP_DELAY;
				}
				else {
					hyd_ostate[n] = OP_ON;
				}
			}
			else {
				if (hyd_odelay[n] == 0) {
					hyd_modify_op(0, HYD_OP_1 << n);
					hyd_ostate[n] = OP_OFF;
				}
			}
			break;
		case OP_ON:
			if (!(hyd_orequest & rmask[n])) {
				if (snvbs->hyd_output[n].flags & HYD_FLAG_OFFDELAY) {
					hyd_odelay[n] = snvbs->hyd_output[n].timeout * 4;
					hyd_ostate[n] = OP_OFFDELAY;
				}
				else {
					hyd_modify_op(0, HYD_OP_1 << n);
					hyd_ostate[n] = OP_OFF;
				}
			}
			break;
		default:
			hyd_modify_op(0, HYD_OP_1 << n);
			hyd_odelay[n] = 0;
			hyd_ostate[n] = OP_OFF;
			break;
		}

		if (hyd_odelay[n]) hyd_odelay[n]--;

		if (snvbs->hyd_input[n].flags & HYD_FLAG_AVAILABLE) {
			if (hyd_ip_state & (HYD_IP_1 << n)) hyd_iack |= amask[n]; else hyd_iack &= ~amask[n];
		}
		else {
			if (hyd_ostate[n] & OP_ONMASK) hyd_iack |= amask[n]; else hyd_iack &= ~amask[n];
		}
	}
}



/********************************************************************************
  FUNCTION NAME   	: hyd_drive
  FUNCTION DETAILS  : Drives the logical hydraulic outputs.
********************************************************************************/

static void hyd_drive(uint8_t set, uint8_t clr)
{
	if (clr & HYD_OP_1) hyd_orequest &= ~HYD_OP_1;
	else if (set & HYD_OP_1) hyd_orequest |= HYD_OP_1;
	if (clr & HYD_OP_2) hyd_orequest &= ~HYD_OP_2;
	else if (set & HYD_OP_2) hyd_orequest |= HYD_OP_2;
	if (clr & HYD_OP_3) hyd_orequest &= ~HYD_OP_3;
	else if (set & HYD_OP_3) hyd_orequest |= HYD_OP_3;
}



/********************************************************************************
  FUNCTION NAME   	: hyd_build_masks
  FUNCTION DETAILS  : Build masks corresponding to the available input/output
					  controls.
********************************************************************************/

static void hyd_build_masks(void)
{
	uint32_t n;

	hyd_ip_mask = 0x01;
	hyd_op_mask = 0;
	for (n = 0; n < 3; n++) {
		hyd_ip_mask |= (snvbs->hyd_input[n].flags & HYD_FLAG_AVAILABLE) ? (0x02 << n) : 0;
		hyd_op_mask |= (snvbs->hyd_output[n].flags & HYD_FLAG_AVAILABLE) ? (0x02 << n) : 0;
	}

	/* Default mappings from internal identifiers to logical output states */

	hyd_op_state2 = HYD_OP_1;
	hyd_op_state3 = HYD_OP_2;
	hyd_op_state4 = HYD_OP_3;

	/* For hydraulic systems, when the low pressure output is not available it is replaced
	   automatically by the high pressure output.
	*/

	if (((snvbs->hyd_type & HYD_TYPE_MASK) == HYD_TYPE_HYD) &&
		(!(snvbs->hyd_output[1].flags & HYD_FLAG_AVAILABLE))) hyd_op_state3 |= HYD_OP_3;

	hyd_group = HYD_EXTRACT_GROUP(snvbs->hyd_type);

	/* Update local hydraulic invalid flag */

	grp_flag_hydinvalid = ((hyd_get_valid() == INVALID_HYDRAULIC) ? GRP_HYDINVALID : 0);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_broadcast_status
  FUNCTION DETAILS  : Broadcast current hydraulic status for master or slave
					  devices only.

					  Status is broadcast using the correct byte for the
					  selected group.
********************************************************************************/

static void hyd_broadcast_status(void)
{
	global_hyd_status_out = (snvbs->hyd_type & HYD_MODE_STANDALONE) ? 0 : (hyd_local_state << (hyd_group * 8));
}



/********************************************************************************
  FUNCTION NAME   	: hyd_broadcast_request
  FUNCTION DETAILS  : Broadcast hydraulic request for master or standalone
					  devices.

					  For standalone devices, the request is copied directly
					  to the local request variable.

					  For master devices, the request is broadcast using the
					  correct byte for the selected group.

					  Shutdown requests are handled separately and will be
					  broadcast irrespective of the mode and group settings.
********************************************************************************/

static void hyd_broadcast_request(uint32_t request)
{
	if (request == REQUEST_GLBL_SHUTDOWN) {

		/* Treat shutdown requests as a special case and force them to be broadcast
		   irrespective of the mode and group settings. Force local master and internal
		   requests to off state.
		*/

		global_hyd_request_out = request;
		hyd_mrequest = hyd_irequest = 0;
	}
	else {
		global_hyd_request_out = (snvbs->hyd_type & HYD_MODE_MASTER) ? (request << (hyd_group * 8)) : 0;
		if (snvbs->hyd_type & HYD_MODE_STANDALONE) hyd_irequest = request;
	}

	/* Slaves must set request duration counter so that request can be automatically cleared */

	if (!(snvbs->hyd_type & HYD_MODE_MASTER)) global_hyd_request_ctr = GLOBAL_HYD_REQUEST_DURATION;
}



/********************************************************************************
  FUNCTION NAME   	: hyd_get_raw_global_status
  FUNCTION DETAILS  : Get current raw hydraulic status. Checks for standalone
					  operation and handles accordingly.

					  Raw status is in the form of a bit mask as used for CNet
					  group status.
********************************************************************************/

static int hyd_get_raw_global_status(void)
{
	int status;

	if (snvbs->hyd_type & HYD_MODE_STANDALONE) {
		status = ~hyd_local_state;
	}
	else {
		status = (~CNET_MEM_READ[CNET_DATASLOT_HYDSTATUS] >> (hyd_group * 8)) & 0xFF;
	}
	return(status);
}



/********************************************************************************
  FUNCTION NAME   	: hyd_chk_hard_stop
  FUNCTION DETAILS  : Called from the master state machine to force a hard stop
					  if a timeout has occurred when requesting pressure on a
					  pump system.

					  Without this check, an abort causes the request to change
					  to REQUEST_OFF state, which is then treated as a normal
					  manual off request and may result in a soft stop occurring.

********************************************************************************/

static void hyd_chk_hard_stop(void)
{
	if (((snvbs->hyd_type & HYD_TYPE_MASK) == 2) && (snvbs->hyd_type & HYD_SOFT_STOP)) {
		hyd_drive(0, HYD_OP_3);
	}
}



/* Define overall hydraulic states */

#define STATE_NO_ESTOP			0
#define STATE_OFF				1
#define STATE_2_WAIT			4
#define STATE_2_ON				5
#define STATE_3_WAIT			7
#define STATE_3_ON				8
#define STATE_4_WAIT			10
#define STATE_4_ON				11

/********************************************************************************
  FUNCTION NAME   	: hyd_master
  FUNCTION DETAILS  : Overall hydraulic control state machine.
********************************************************************************/

static void hyd_master(void)
{
	uint32_t status;
	static uint32_t hyd_ip_state1;
	static uint32_t hyd_ip_state2;
	static uint32_t hyd_ip_state3;
	static uint32_t hyd_ip_state4;

	status = hyd_get_raw_global_status();	/* Get global hydraulic status */

	hyd_ip_state1 = ((status >> 0) & 1);
	hyd_ip_state2 = ((status >> 1) & 1);
	hyd_ip_state3 = ((status >> 2) & 1);
	hyd_ip_state4 = ((status >> 3) & 1);

	if (hyd_mrequest < REQUEST_2) {

		/* Request for unload, so turn off all outputs. */

		hyd_broadcast_request(REQUEST_OFF);
	}

	if (!hyd_ip_state1) {
		hyd_mstate = STATE_NO_ESTOP;
		hyd_mrequest = REQUEST_OFF;
		hyd_broadcast_request(REQUEST_OFF);
	}

	switch (hyd_mstate) {
	case STATE_NO_ESTOP:
		if (hyd_ip_state1) { hyd_mstate = STATE_OFF; hyd_mrequest = REQUEST_OFF; hyd_broadcast_request(REQUEST_OFF); eventlog_log(LogTypeHydMasterTransition, 1, 0); }
		break;
	case STATE_OFF:
		if (hyd_mrequest > REQUEST_OFF) {
			hyd_timer = (snvbs->hyd_input[0].timeout * 4) + HYD_MASTER_DELAY;
			hyd_mstate = STATE_2_WAIT;
			hyd_broadcast_request(REQUEST_2);
		}
		break;
	case STATE_2_WAIT:

		/* Wait for an acknowledgement or until the time period has expired, in which case
		   the operation is aborted.
		*/

		if (hyd_timer) {
			if (hyd_ip_state2) hyd_mstate = STATE_2_ON;
		}
		else {
			hyd_mstate = STATE_OFF;
			hyd_mrequest = REQUEST_OFF;
			hyd_broadcast_request(REQUEST_OFF);
			eventlog_log(LogTypeHydMasterTransition, 2, 0);
		}
		break;
	case STATE_2_ON:
		if (!hyd_ip_state2) { hyd_mstate = STATE_OFF; hyd_mrequest = REQUEST_OFF; hyd_broadcast_request(REQUEST_OFF); eventlog_log(LogTypeHydMasterTransition, 3, 0); }
		else if (hyd_mrequest > REQUEST_2) {
			hyd_timer = (snvbs->hyd_input[1].timeout * 4) + HYD_MASTER_DELAY;
			hyd_mstate = STATE_3_WAIT;
			hyd_broadcast_request(REQUEST_3);
		}
		break;
	case STATE_3_WAIT:
		if (!hyd_ip_state2) { hyd_mstate = STATE_OFF; hyd_mrequest = REQUEST_OFF; hyd_broadcast_request(REQUEST_OFF); eventlog_log(LogTypeHydMasterTransition, 4, 0); }
		else if (hyd_mrequest == REQUEST_2) {
			hyd_mstate = STATE_2_ON;
			hyd_broadcast_request(REQUEST_2);
		}
		else {

			/* Wait for an acknowledgement or until the time period has expired, in which case
			   the operation is aborted.
			*/

			if (hyd_timer) {
				if (hyd_ip_state3) hyd_mstate = STATE_3_ON;
			}
			else {
				hyd_mstate = STATE_OFF;
				hyd_mrequest = REQUEST_OFF;
				hyd_broadcast_request(REQUEST_OFF);
				eventlog_log(LogTypeHydMasterTransition, 5, 0);
				hyd_chk_hard_stop();
			}
		}
		break;
	case STATE_3_ON:
		if (!hyd_ip_state2 || !hyd_ip_state3) { hyd_mstate = STATE_OFF; hyd_mrequest = REQUEST_OFF; hyd_broadcast_request(REQUEST_OFF); eventlog_log(LogTypeHydMasterTransition, 6, 0); }
		else if (hyd_mrequest == REQUEST_2) {
			hyd_mstate = STATE_2_ON;
			hyd_broadcast_request(REQUEST_2);
		}
		else if (hyd_mrequest == REQUEST_4) {
			hyd_timer = (snvbs->hyd_input[2].timeout * 4) + HYD_MASTER_DELAY;
			hyd_mstate = STATE_4_WAIT;
			hyd_broadcast_request(REQUEST_4);
		}
		break;
	case STATE_4_WAIT:
		if (!hyd_ip_state2 || !hyd_ip_state3) { hyd_mstate = STATE_OFF; hyd_mrequest = REQUEST_OFF; hyd_broadcast_request(REQUEST_OFF); eventlog_log(LogTypeHydMasterTransition, 7, 0); }
		else if (hyd_mrequest == REQUEST_2) {
			hyd_mstate = STATE_2_ON;
			hyd_broadcast_request(REQUEST_2);
			eventlog_log(LogTypeHydMasterTransition, 8, 0);
		}
		else if (hyd_mrequest == REQUEST_3) {
			hyd_mstate = STATE_3_ON;
			hyd_broadcast_request(REQUEST_3);
			eventlog_log(LogTypeHydMasterTransition, 9, 0);
		}
		else {

			/* Wait for an acknowledgement or until the time period has expired, in which case
			   the operation is aborted.
			*/

			if (hyd_timer) {
				if (hyd_ip_state4) hyd_mstate = STATE_4_ON;
			}
			else {
				hyd_mstate = STATE_OFF;
				hyd_mrequest = REQUEST_OFF;
				hyd_broadcast_request(REQUEST_OFF);
				eventlog_log(LogTypeHydMasterTransition, 10, 0);
				hyd_chk_hard_stop();
			}
		}
		break;
	case STATE_4_ON:
		if (!hyd_ip_state2 || !hyd_ip_state3 || !hyd_ip_state4) { hyd_mstate = STATE_OFF; hyd_mrequest = REQUEST_OFF; hyd_broadcast_request(REQUEST_OFF); eventlog_log(LogTypeHydMasterTransition, 11, 0); }
		else if (hyd_mrequest == REQUEST_2) {
			hyd_mstate = STATE_2_ON;
			hyd_broadcast_request(REQUEST_2);
			eventlog_log(LogTypeHydMasterTransition, 12, 0);
		}
		else if (hyd_mrequest == REQUEST_3) {
			hyd_mstate = STATE_3_ON;
			hyd_broadcast_request(REQUEST_3);
			eventlog_log(LogTypeHydMasterTransition, 13, 0);
		}
		break;
	}
	if (hyd_timer) hyd_timer--;
}

/********************************************************************************
  FUNCTION NAME   	: hyd_ack
  FUNCTION DETAILS  : Generate overall hydraulic acknowledgement signal based
					  on global hydraulic status.

					  For master or slave devices, the status is obtained from
					  the CNet hydraulic status slot.

					  For standalone devices, the status is obtained from the
					  local status variable.
********************************************************************************/

static void hyd_ack(void)
{
	uint32_t state;
	uint32_t req;
	static uint32_t prev_hyd_state = HYD_ACK_0;
	static uint32_t prev_hyd_tracking_state = 0;

#define STATEMASK 

	/* Get the current request state, converting a shutdown request into an off
	   request for compatibility with the acknowledge state.
	*/

	req = (hyd_mrequest == REQUEST_SHUTDOWN) ? REQUEST_OFF : hyd_mrequest;

	/* Get local hydraulic status including tracking mode */

	state = ((hyd_get_global_status() & HYD_STATUS_MASK) | (hyd_get_status() & HYD_TRACKING_MASK) | (req << 16));

	/* Detect changes in hydraulic state excluding tracking mode flag. Send event report and make an entry in the event log */

	if ((state ^ prev_hyd_state) & HYD_EVENT_MASK) {

		/* Hydraulic state change has occurred */

		eventlog_entry log;
		uint32_t block[3];

		/* Build event report */

		if (event_hyd_bitmask) {
			event_report* report = (event_report*)&block;
			report->length = offsetof(event_report, data) + sizeof(int);
			report->type = EVENT_HYDSTATE;
			block[2] = state;
			event_send_report(event_hyd_bitmask, report, report->length);
		}

		/* Build eventlog entry for non-tracking mode status changes only */

		if ((state ^ prev_hyd_state) & HYD_STATUS_MASK) {
			log.type = LogTypeHydraulicStateChange;
			log.parameter1 = state;
			log.parameter2 = 0;
			log.timestamp_hi = cnet_time_hi;
			log.timestamp_lo = cnet_time_lo;
			eventlog_write(&log);
		}
		prev_hyd_state = state;
	}

	/* Detect changes in tracking mode state and send event report */

	if ((state ^ prev_hyd_tracking_state) & HYD_TRACKING_MASK) {

		/* Hydraulic state change has occurred */

		uint32_t block[3];

		/* Build event report */

		if (event_hyd_bitmask) {
			event_report* report = (event_report*)&block;
			report->length = offsetof(event_report, data) + sizeof(int);
			report->type = EVENT_HYDTRACK;
			block[2] = state & HYD_TRACKING_MASK;
			event_send_report(event_hyd_bitmask, report, report->length);
		}

		prev_hyd_tracking_state = state;
	}

}



/* Define hydraulic states for type 0 hydraulic systems */

#define STATE0_NO_ESTOP			0
#define STATE0_OFF				1
#define STATE0_2_WAIT			3
#define STATE0_2_ON				4
#define STATE0_3_WAIT			6
#define STATE0_3_ON				7
#define STATE0_4_OFF			8	/* Used when switching to LP with HPLPOFF flag set */
#define STATE0_4_WAIT			9
#define STATE0_4_ON				10

/********************************************************************************
  FUNCTION NAME   	: hyd_unload_0
  FUNCTION DETAILS  : Unload type 0 hydraulic system.
********************************************************************************/

static void hyd_unload_0(void)
{
	hyd_drive(0, (HYD_OP_1 | HYD_OP_2 | HYD_OP_3));
	hyd_istate = STATE0_OFF;
	hyd_irequest = REQUEST_OFF;
}

/********************************************************************************
  FUNCTION NAME   	: hyd_control_0
  FUNCTION DETAILS  : Hydraulic control state machine for type 0 hydraulic
					  system (hydraulic control station).
********************************************************************************/

static void hyd_control_0(void)
{
	uint32_t hyd_ip_state2;
	uint32_t hyd_ip_state3;
	uint32_t hyd_ip_state4;

	hyd_ip_state2 = (hyd_iack & HYD_IP_1) ? 1 : 0;
	hyd_ip_state3 = (hyd_iack & HYD_IP_2) ? 1 : 0;
	hyd_ip_state4 = (hyd_iack & HYD_IP_3) ? 1 : 0;

	if (!HYD_IP_ESTOP) {
		hyd_istate = STATE0_NO_ESTOP;
	}
	else if (hyd_irequest < REQUEST_2) {
		hyd_unload_0();
	}

	switch (hyd_istate) {
	case STATE0_NO_ESTOP:
		hyd_drive(0, (HYD_OP_1 | HYD_OP_2 | HYD_OP_3));
		if (HYD_IP_ESTOP) { hyd_istate = STATE0_OFF; }
		break;
	case STATE0_OFF:
		if ((hyd_irequest > REQUEST_OFF) && CtrlGetSafeToStart()) {
			hyd_drive(hyd_op_state2, 0);
			hyd_istate = STATE0_2_WAIT;
		}
		break;
	case STATE0_2_WAIT:
		if (hyd_ip_state2) hyd_istate = STATE0_2_ON;
		break;
	case STATE0_2_ON:
		if (!hyd_ip_state2) { hyd_unload_0(); eventlog_log(LogTypeHydSlaveIpOff, 4, 0); }
		else if (hyd_irequest > REQUEST_2) {
			hyd_drive(hyd_op_state3, 0);
			hyd_istate = STATE0_3_WAIT;
		}
		break;
	case STATE0_3_WAIT:
		if (!hyd_ip_state2) { hyd_unload_0(); eventlog_log(LogTypeHydSlaveIpOff, 6, 0); }
		else if (hyd_irequest < REQUEST_3) {
			hyd_drive(0, hyd_op_state3);
			hyd_istate = STATE0_2_ON;
		}
		else if (hyd_ip_state3) hyd_istate = STATE0_3_ON;
		break;
	case STATE0_3_ON:
		if (!hyd_ip_state2 || !hyd_ip_state3) { hyd_unload_0(); eventlog_log(LogTypeHydSlaveIpOff, 7, 0); }
		else if (hyd_irequest < REQUEST_3) {
			hyd_drive(0, hyd_op_state3);
			hyd_istate = STATE0_2_ON;
		}
		else if (hyd_irequest == REQUEST_4) {
			hyd_drive(hyd_op_state4, 0);
			hyd_istate = STATE0_4_WAIT;
		}
		break;
	case STATE0_4_WAIT:
		if (!hyd_ip_state2 || !hyd_ip_state3) { hyd_unload_0(); eventlog_log(LogTypeHydSlaveIpOff, 9, 0); }
		else if (hyd_irequest < REQUEST_4) {
			hyd_drive(0, hyd_op_state4);
			hyd_istate = STATE0_3_ON;
		}
		else if (hyd_ip_state4) hyd_istate = STATE0_4_ON;
		break;
	case STATE0_4_ON:
		if (snvbs->hyd_output[1].flags & HYD_FLAG_HPLPOFF) hyd_ip_state3 |= hyd_ip_state4;
		hyd_drive(0, ((snvbs->hyd_output[1].flags & HYD_FLAG_HPLPOFF) ? HYD_OP_2 : 0)); /* Note the use of HYD_OP_2 instead of
																						   hyd_op_state3 here. Using hyd_op_state3
																						   when HPLPOFF flag is set also causes
																						   HP output to turn off, which is wrong.
																						*/
		if (!hyd_ip_state2 || !hyd_ip_state3 || !hyd_ip_state4) { hyd_unload_0(); eventlog_log(LogTypeHydSlaveIpOff, 10, 0); }
		else if (hyd_irequest < REQUEST_4) {
			if (snvbs->hyd_output[1].flags & HYD_FLAG_HPLPOFF) {
				hyd_drive(hyd_op_state3, hyd_op_state4);
				hyd_istate = STATE0_4_OFF;
			}
			else {
				hyd_drive(0, hyd_op_state4);
				hyd_istate = STATE0_3_ON;
			}
		}
		break;
	case STATE0_4_OFF:
		if (!hyd_ip_state2) { hyd_unload_0(); eventlog_log(LogTypeHydSlaveIpOff, 8, 0); }
		else if (hyd_irequest < REQUEST_3) {
			hyd_drive(0, hyd_op_state3);
			hyd_istate = STATE0_2_ON;
		}
		else if (hyd_ip_state3) hyd_istate = STATE0_3_ON;
		break;
	}

	/* Generate hydraulic acknowledgement signal. The state3 acknowledgement includes
	   a check for the STATE0_4_OFF condition. This allows the LP signal to be artificially
	   flagged whilst switching from HP to LP when the HPLPOFF flags is set.
	*/

	if (!HYD_IP_ESTOP) hyd_local_state = GEN_STATEMASK(0);
	else if (hyd_ip_state4) hyd_local_state = GEN_STATEMASK(4);
	else if (hyd_ip_state3 || (hyd_istate == STATE0_4_OFF)) hyd_local_state = GEN_STATEMASK(3);
	else if (hyd_ip_state2) hyd_local_state = GEN_STATEMASK(2);
	else hyd_local_state = GEN_STATEMASK(1);

	grp_flag_invalidpressure = 0;
}



static const uint32_t hyd0_sp_track_lookup[] = {
	TRUE,	/* STATE0_NO_ESTOP 	*/
	TRUE,	/* STATE0_OFF		*/
	FALSE,	/* Not used			*/
	TRUE,	/* STATE0_2_WAIT	*/
	TRUE,	/* STATE0_2_ON		*/
	FALSE,	/* Not used			*/
	FALSE,	/* STATE0_3_WAIT	*/
	FALSE,	/* STATE0_3_ON		*/
	FALSE,	/* STATE0_4_OFF		*/
	FALSE,	/* STATE0_4_WAIT	*/
	FALSE,	/* STATE0_4_ON		*/
};

/********************************************************************************
  FUNCTION NAME   	: hyd_track_0
  FUNCTION DETAILS  : Generate hydraulic tracking mode control signal for type 0
					  hydraulic system (hydraulic control station).
********************************************************************************/

static void hyd_track_0(void)
{
	hyd_sp_tracking_control = (hyd_istate > STATE0_4_ON) ? TRUE : hyd0_sp_track_lookup[hyd_istate];
}



/* Define hydraulic simulation states for type 1 hydraulic systems (hydraulic
   control station simulation)
*/

#define STATE1_NO_ESTOP			0
#define STATE1_OFF				1
#define STATE1_2_WAIT			2
#define STATE1_2_ON				3
#define STATE1_3_WAIT			4
#define STATE1_3_ON				5
#define STATE1_4_WAIT			6 
#define STATE1_4_ON				7

#define TYPE1_DELAY				200		/* Simulation delay when moving between states (1 second) */

/********************************************************************************
  FUNCTION NAME   	: hyd_unload_1
  FUNCTION DETAILS  : Unload type 1 hydraulic system (hydraulic control station
					  simulation).
********************************************************************************/

static void hyd_unload_1(void)
{
	hyd_istate = STATE1_OFF;
	hyd_irequest = REQUEST_OFF;
}

/********************************************************************************
  FUNCTION NAME   	: hyd_control_1
  FUNCTION DETAILS  : Hydraulic control state machine for type 1 hydraulic
					  system (hydraulic control station simulation).
********************************************************************************/

static void hyd_control_1(void)
{
	if (hyd_irequest < REQUEST_2) {
		hyd_unload_1();
	}

	switch (hyd_istate) {
	case STATE1_NO_ESTOP:
		hyd_istate = STATE1_OFF;
		break;
	case STATE1_OFF:
		if ((hyd_irequest > REQUEST_OFF) && CtrlGetSafeToStart()) {
			sim_timer = snvbs->hyd_input[0].timeout * 4;
			hyd_istate = STATE1_2_WAIT;
		}
		break;
	case STATE1_2_WAIT:
		if (!sim_timer) hyd_istate = STATE1_2_ON;
		break;
	case STATE1_2_ON:
		if (hyd_irequest > REQUEST_2) {
			sim_timer = snvbs->hyd_input[1].timeout * 4;
			hyd_istate = STATE1_3_WAIT;
		}
		break;
	case STATE1_3_WAIT:
		if (hyd_irequest == REQUEST_2) {
			hyd_istate = STATE1_2_ON;
		}
		else {
			if (!sim_timer) hyd_istate = STATE1_3_ON;
		}
		break;
	case STATE1_3_ON:
		if (hyd_irequest == REQUEST_2) {
			hyd_istate = STATE1_2_ON;
		}
		else if (hyd_irequest == REQUEST_4) {
			sim_timer = snvbs->hyd_input[2].timeout * 4;
			hyd_istate = STATE1_4_WAIT;
		}
		break;
	case STATE1_4_WAIT:
		if (hyd_irequest == REQUEST_2) {
			hyd_istate = STATE1_2_ON;
		}
		else if (hyd_irequest == REQUEST_3) {
			hyd_istate = STATE1_3_ON;
		}
		else {
			if (!sim_timer) hyd_istate = STATE1_4_ON;
		}
		break;
	case STATE1_4_ON:
		if (hyd_irequest == REQUEST_2) {
			hyd_istate = STATE1_2_ON;
		}
		else if (hyd_irequest == REQUEST_3) {
			hyd_istate = STATE1_3_ON;
		}
		break;
	}
	if (sim_timer) sim_timer--;

	/* Generate hydraulic acknowledgement signal */

	switch (hyd_istate) {
	case STATE1_NO_ESTOP:
		hyd_local_state = GEN_STATEMASK(0);
		break;
	case STATE1_OFF:
	case STATE1_2_WAIT:
		hyd_local_state = GEN_STATEMASK(1);
		break;
	case STATE1_2_ON:
	case STATE1_3_WAIT:
		hyd_local_state = GEN_STATEMASK(2);
		break;
	case STATE1_3_ON:
	case STATE1_4_WAIT:
		hyd_local_state = GEN_STATEMASK(3);
		break;
	case STATE1_4_ON:
		hyd_local_state = GEN_STATEMASK(4);
		break;
	}

	grp_flag_invalidpressure = 0;
}



static const uint32_t hyd1_sp_track_lookup[] = {
	TRUE,	/* STATE1_NO_ESTOP 	*/
	TRUE,	/* STATE1_OFF		*/
	TRUE,	/* STATE1_2_WAIT	*/
	TRUE,	/* STATE1_2_ON		*/
	FALSE,	/* STATE1_3_WAIT	*/
	FALSE,	/* STATE1_3_ON		*/
	FALSE,	/* STATE1_4_WAIT	*/
	FALSE,	/* STATE1_4_ON		*/
};

/********************************************************************************
  FUNCTION NAME   	: hyd_track_1
  FUNCTION DETAILS  : Generate hydraulic tracking mode control signal for type 1
					  hydraulic system (hydraulic control station simulation).
********************************************************************************/

static void hyd_track_1(void)
{
	hyd_sp_tracking_control = (hyd_istate > STATE1_4_ON) ? TRUE : hyd1_sp_track_lookup[hyd_istate];
}



/* Define hydraulic states for type 2 hydraulic systems (pump) */

#define STATE2_NO_ESTOP			0
#define STATE2_OFF				1
#define STATE2_2_DELAY			2
#define STATE2_2_WAIT			3
#define STATE2_2_ON				4
#define STATE2_4_DELAY			8
#define STATE2_4_WAIT			9
#define STATE2_4_ON				10

/********************************************************************************
  FUNCTION NAME   	: hyd_unload_2
  FUNCTION DETAILS  : Unload type 2 hydraulic system (pump).
********************************************************************************/

static void hyd_unload_2(void)
{
	hyd_drive(0, (HYD_OP_1 | HYD_OP_2 | HYD_OP_3));
	hyd_istate = STATE2_OFF;
	hyd_irequest = REQUEST_OFF;
}



/********************************************************************************
  FUNCTION NAME   	: hyd_stop_2
  FUNCTION DETAILS  : Perform stop operation on type 2 hydraulic system (pump).
********************************************************************************/

static void hyd_stop_2(void)
{
	if (snvbs->hyd_type & HYD_SOFT_STOP) {
		hyd_drive(0, (HYD_OP_1 | HYD_OP_2)); /* Keep pressure output state */
	}
	else {
		hyd_drive(0, (HYD_OP_1 | HYD_OP_2 | HYD_OP_3));
	}
	hyd_istate = STATE2_OFF;
	hyd_irequest = REQUEST_OFF;
}



/********************************************************************************
  FUNCTION NAME   	: hyd_abort_2
  FUNCTION DETAILS  : Perform abort operation on type 2 hydraulic system (pump).

					  Operates as hyd_stop_2 except that pressure output state
					  is turned off.

					  This call is used when a requested state is not achieved
					  during hydraulic sequencing or when an unexpected change
					  in state occurs.

********************************************************************************/

static void hyd_abort_2(void)
{
	hyd_drive(0, (HYD_OP_1 | HYD_OP_2 | HYD_OP_3));
	hyd_istate = STATE2_OFF;
	hyd_irequest = REQUEST_OFF;
}



/********************************************************************************
  FUNCTION NAME   	: hyd_control_2
  FUNCTION DETAILS  : Hydraulic control state machine for type 2 hydraulic
					  system (pump).
********************************************************************************/

static void hyd_control_2(void)
{
	uint32_t hyd_ip_state2;
	uint32_t hyd_ip_state4;
	uint32_t err = 0;
	uint32_t softstop = snvbs->hyd_type & HYD_SOFT_STOP;

	/* Determine the state of the pressure input as follows:

	   If an external input is available, then use it.

	   Otherwise, if soft stop operation is enabled, then the state is
	   the logical AND of the motor running state and the simulated input.
	   state. This ensures, that the pressure state appears to be OFF when
	   the motor is stopped.

	   Else, if soft stop operation is not enabled, then the state is the
	   simulated input state.

	*/

	hyd_ip_state2 = (hyd_iack & HYD_IP_1) ? 1 : 0;

	if (snvbs->hyd_input[2].flags & HYD_FLAG_AVAILABLE) {
		hyd_ip_state4 = ((hyd_iack & HYD_IP_3) != 0);
	}
	else {
		hyd_ip_state4 = softstop ? ((hyd_iack & HYD_IP_3) && hyd_ip_state2) : ((hyd_iack & HYD_IP_3) != 0);
	}

	if (!HYD_IP_ESTOP) {
		hyd_drive(0, HYD_OP_1 | HYD_OP_2 | HYD_OP_3);
		hyd_istate = STATE2_NO_ESTOP;
	}
	else if (hyd_irequest < REQUEST_2) {
		hyd_stop_2();
	}

	switch (hyd_istate) {
	case STATE2_NO_ESTOP:
		hyd_drive(0, (HYD_OP_1 | HYD_OP_2 | HYD_OP_3));
		if (HYD_IP_ESTOP) { hyd_istate = STATE2_OFF; }
		break;
	case STATE2_OFF:
		if (hyd_ip_state4) err = GRP_INVALIDPRESSURE;
		if ((hyd_irequest > REQUEST_OFF) && !hyd_ip_state4 && CtrlGetSafeToStart()) {
			hyd_drive(0, hyd_op_state4);		/* Negate PRESSURE output  */
			hyd_drive(hyd_op_state2, 0);		/* Assert STOP output  */
			hyd_drive(hyd_op_state3, 0);		/* Assert START output */
			hyd_istate = STATE2_2_WAIT;
		}
		break;
	case STATE2_2_WAIT:
		if (hyd_ip_state2) {
			hyd_drive(0, hyd_op_state3);		/* Negate START output */
			hyd_istate = STATE2_2_ON;
		}
		break;
	case STATE2_2_ON: /* Motor running */
		if (!hyd_ip_state2) { hyd_abort_2(); }
		else if (hyd_irequest > REQUEST_2) {
			hyd_drive(hyd_op_state4, 0);		/* Enable PRESSURE output */
			hyd_istate = STATE2_4_WAIT;
		}
		break;
	case STATE2_4_WAIT:
		if (!hyd_ip_state2) { hyd_abort_2(); }
		else if (hyd_irequest == REQUEST_2) {
			hyd_drive(0, hyd_op_state4);		/* Negate PRESSURE output */
			hyd_istate = STATE2_2_ON;
		}
		else {
			if (hyd_ip_state4) hyd_istate = STATE2_4_ON;
		}
		break;
	case STATE2_4_ON: /* Pressure on */
		if (!hyd_ip_state2 || !hyd_ip_state4) { hyd_abort_2(); }
		else if (hyd_irequest == REQUEST_2) {
			hyd_drive(0, hyd_op_state4);		/* Negate PRESSURE output */
			hyd_istate = STATE2_2_ON;
		}
		break;
	}

	/* Generate hydraulic acknowledgement signal */

	if (!HYD_IP_ESTOP) hyd_local_state = GEN_STATEMASK(0);			/* E-stop off	 */
	else if (hyd_ip_state4) hyd_local_state = GEN_STATEMASK(4);		/* Pressure on 	 */
	else if (hyd_ip_state2) hyd_local_state = GEN_STATEMASK(2);		/* Motor running */
	else hyd_local_state = GEN_STATEMASK(1);						/* Off			 */

	grp_flag_invalidpressure = err;
}



static const uint32_t hyd2_sp_track_lookup[] = {
	TRUE,	/* STATE2_NO_ESTOP 	*/
	TRUE,	/* STATE2_OFF		*/
	TRUE,	/* Not used			*/
	TRUE,	/* STATE2_2_WAIT	*/
	TRUE,	/* STATE2_2_ON		*/
	FALSE,	/* Not used			*/
	FALSE,	/* Not used			*/
	FALSE,	/* Not used			*/
	FALSE,	/* Not used			*/
	FALSE,	/* STATE2_4_WAIT	*/
	FALSE,	/* STATE2_4_ON		*/
};



/********************************************************************************
  FUNCTION NAME   	: hyd_track_2
  FUNCTION DETAILS  : Generate hydraulic tracking mode control signal for type 2
					  hydraulic system (pump).
********************************************************************************/

static void hyd_track_2(void)
{
	hyd_sp_tracking_control = (hyd_istate > STATE2_4_ON) ? TRUE : hyd2_sp_track_lookup[hyd_istate];
}



/* Define pump simulation states */

#define STATE3_NO_ESTOP			0
#define STATE3_OFF				1
#define STATE3_2_WAIT			2
#define STATE3_2_ON				3
#define STATE3_4_ON				7

#define TYPE3_DELAY				200		/* Simulation delay when moving between states (1 second) */

/********************************************************************************
  FUNCTION NAME   	: hyd_unload_3
  FUNCTION DETAILS  : Unload type 3 hydraulic system (pump simulation).
********************************************************************************/

static void hyd_unload_3(void)
{
	hyd_istate = STATE3_OFF;
	hyd_irequest = REQUEST_OFF;
}

/********************************************************************************
  FUNCTION NAME   	: hyd_control_3
  FUNCTION DETAILS  : Hydraulic control state machine for type 3 hydraulic
					  system (pump simulation).
********************************************************************************/

static void hyd_control_3(void)
{
	if (hyd_irequest < REQUEST_2) {
		hyd_unload_3();
	}

	switch (hyd_istate) {
	case STATE3_NO_ESTOP:
		hyd_istate = STATE3_OFF;
		break;
	case STATE3_OFF:
		if ((hyd_irequest > REQUEST_OFF) && CtrlGetSafeToStart()) {
			sim_timer = snvbs->hyd_input[0].timeout * 4;
			hyd_istate = STATE3_2_WAIT;
		}
		break;
	case STATE3_2_WAIT: /* Wait for motor running */
		if (!sim_timer) hyd_istate = STATE3_2_ON;
		break;
	case STATE3_2_ON:
		if (hyd_irequest > REQUEST_2) {
			hyd_istate = STATE3_4_ON;
		}
		break;
	case STATE3_4_ON:
		if (hyd_irequest == REQUEST_2) {
			hyd_istate = STATE3_2_ON;
		}
		break;
	}
	if (sim_timer) sim_timer--;

	/* Generate hydraulic acknowledgement signal */

	switch (hyd_istate) {
	case STATE1_NO_ESTOP:
		hyd_local_state = GEN_STATEMASK(0);
		break;
	case STATE1_OFF:
	case STATE1_2_WAIT:
		hyd_local_state = GEN_STATEMASK(1);
		break;
	case STATE1_2_ON:
		hyd_local_state = GEN_STATEMASK(2);
		break;
	case STATE1_4_ON:
		hyd_local_state = GEN_STATEMASK(4);
		break;
	}
	grp_flag_invalidpressure = 0;
}



static const uint32_t hyd3_sp_track_lookup[] = {
	TRUE,	/* STATE3_NO_ESTOP 	*/
	TRUE,	/* STATE3_OFF		*/
	TRUE,	/* STATE3_2_WAIT	*/
	TRUE,	/* STATE3_2_ON		*/
	FALSE,	/* Not used			*/
	FALSE,	/* Not used			*/
	FALSE,	/* Not used			*/
	FALSE,	/* STATE3_4_ON		*/
};

/********************************************************************************
  FUNCTION NAME   	: hyd_track_3
  FUNCTION DETAILS  : Generate hydraulic tracking mode control signal for type 3
					  hydraulic system (pump simulation).
********************************************************************************/

static void hyd_track_3(void)
{
	hyd_sp_tracking_control = (hyd_istate > STATE3_4_ON) ? TRUE : hyd3_sp_track_lookup[hyd_istate];
}



/* Mask for hydraulic inputs in local I/O data */

#define HYD_IP_MASK	(EM_SENSE |	HIGH_SENSE | PILOT_SENSE | LOW_SENSE)

#define NEWSTATE(x)	{ int s;							\
					  estop_state = (x); 				\
					  for(s=0; s<ESTOP_ONCHECK; s++)	\
					    estop_ctr[s] = 0; 				\
					}

#define HC_STATUS 	0x34	/* DSP B address of hand controller status word	 	 */

/********************************************************************************
  FUNCTION NAME   	: hyd_update
  FUNCTION DETAILS  : Performs control of hydraulic interface. Called at regular
					  intervals from timer interrupt.

					  Depending on the setting of the state variable, different
					  tasks are performed. This allows the routine to be called
					  at faster rates without overloading the I2C bus with too
					  many unnecessary reads of the external input states.

					  HYD_MONITOR

					  Reads the hydraulic status via the I2C bus and updates
					  the hydraulic outputs based on state changes.

					  HYD_UPDATE

					  Bypasses the reading of the hydraulic status and only
					  performs updates of the hydraulic outputs.

********************************************************************************/

void hyd_update(int state)
{
	uint32_t ringsize;
	uint32_t position;
	uint32_t hyd_type = snvbs->hyd_type;
	uint32_t estop = ESTOP_INVALID;
	uint32_t hwflags;
	uint32_t n;
	static uint32_t prev_hyd_irequest = 0;

	/* If timer interrupt is not enabled, force hydraulic outputs to OFF state
	   and return immediately.
	*/

	if (prd_enable == FALSE) {
		dsp_spi_send(0);
		return;
	}

	cnet_get_info(&ringsize, &position);

	if (state == HYD_MONITOR) {

		/* Update the hydraulic input state */

		if (dsp_i2c_free()) {
			uint16_t io_state;
			hyd_io_state = io_state = local_io(0, 0);

			/* Generate a digital input state suitable for writing to a CNet slot for data acquisition */

			dig_ip_state = (hyd_io_state >> BITPOS_GPIN1) & slot_status[0].digip_mask;

			/* Simple filter to clean up hydraulic state */

			filt_ip_state = (filt_ip_state << 8) | (uint32_t)(HYD_CONVERT_IP(io_state & HYD_IP_MASK));
			hyd_ip_state = (uint8_t)((filt_ip_state | (filt_ip_state >> 8) | (filt_ip_state >> 16) | (filt_ip_state >> 24)) & 0xFF);

			//    hyd_ip_state = HYD_CONVERT_IP(io_state & HYD_IP_MASK);

				/* Check the state of the e-stop input, the e-stop relay slave contact and the
				   e-stop 1 and 2 sense inputs (revision F hardware only).

				   Test for valid states and process fault condition if required.

				   Note that the io_state is inverted so that when the slave contact is
				   open (e-stop closed), the input reads zero.

				   Perform a cross check of the operation of the hydraulic safety relay. This code
				   checks the state of the slave contacts and compares them with the expected state
				   based on the emergency stop input. Any discrepancy indicates a possible fault
				   condition and is indicated by a diagnostic indication.

				   To prevent false indications due to race conditions, the error state must exist
				   for at least 50 consecutive samples (1 second) before the fault condition is
				   indicated.

				*/

			if ((io_state & hyd_fault_mask) == hyd_fault_valid_on) estop = ESTOP_ON;
			else if ((io_state & hyd_fault_mask) == hyd_fault_valid_off1) estop = ESTOP_OFF1;
			else if ((io_state & hyd_fault_mask2) == hyd_fault_valid_off2) estop = ESTOP_OFF2;
			else estop = ESTOP_INVALID;

			/* Update state filter counters */

			for (n = 0; n < ESTOP_ONCHECK; n++) {
				if (estop == n) {
					if (estop_ctr[n] < ESTOP_CTR_MAX) estop_ctr[n]++;
				}
				else {
					if (estop_ctr[n]) estop_ctr[n]--;
				}
			}

			if (hyd_type & HYD_SIM_MASK) {

				/* Force to valid state for simulation operation */

				estop_state = ESTOP_ONCHECK;
				estop_off_valid = hyd_off1_valid | hyd_off2_valid;
			}
			else {

				/* Else process the state machine for the emergency stop input */

				switch (estop_state) {
				case ESTOP_INVALID:

					/* The initial state of estop_off_valid depends on the type of hardware and the
					   presence of a hand controller.

					   For hardware prior to revision F, validation is not required, so set to
					   hyd_off1_valid | hyd_off2_valid to indicate that validation has already taken
					   place.

					   For hardware version F and later, if there is a hand controller connected, then
					   the initial state depends on whether the hand controller e-stop requires validation.
					   This is tested by checking the loom type, which is used to detect when an
					   adaptor cable is connected to inhibit hand controller e-stop validation. If
					   validation is inhibited, the initial state is set to hyd_off2_valid, otherwise
					   it is set to zero.
					*/

					get_hw_version(NULL, &hwflags);
					if (hwflags & HW_FLAG_REVF) {
//						uint32_t loom_type = (hpi_read4(HC_STATUS) & HC_LOOMMASK) >> 24;
						uint32_t loom_type = 2;		// TODO: hand controller support
						if ((loom_type == 2) || (hyd_type & HYD_HC_NOVALID)) {

							/* Adaptor cable fitted to inhibit hand controller e-stop validation, or option
							   flag set in hydraulic type word, so set initial state to hyd_off2_valid.
							*/

							estop_off_valid = hyd_off2_valid;
						}
						else {
							//TODO: hand controller support
							//estop_off_valid = (hpi_read4(HC_STATUS) & HC_PRESENTMASK) ? 0 : hyd_off2_valid;
						}
					}
					else {
						estop_off_valid = hyd_off1_valid | hyd_off2_valid;
					}

					switch (estop) {
					case ESTOP_ON:
					case ESTOP_OFF1:
					case ESTOP_OFF2:
						if (estop_ctr[estop] >= HYD_FAULT_TIME) NEWSTATE(estop);
						break;
					}
					break;
				case ESTOP_ON:
					switch (estop) {
					case ESTOP_INVALID:
					case ESTOP_OFF1:
					case ESTOP_OFF2:
						if (estop_ctr[estop] >= HYD_FAULT_TIME) NEWSTATE(estop);
						break;
					case ESTOP_ON:
						if (estop_off_valid == (ESTOP_OFF2 | ESTOP_OFF1)) NEWSTATE(ESTOP_ONCHECK);
						break;
					}
					break;
				case ESTOP_OFF1:
					estop_off_valid |= hyd_off1_valid;

					/* If no hand controller present, then force the off2 valid state flag
					   to be set, allowing validation to continue.
					*/
					// TODO: hand controller support
//					if (!(hpi_read4(HC_STATUS) & HC_PRESENTMASK)) estop_off_valid |= hyd_off2_valid;
					switch (estop) {
					case ESTOP_INVALID:
					case ESTOP_OFF2:
					case ESTOP_ON:
						if (estop_ctr[estop] >= HYD_FAULT_TIME) NEWSTATE(estop);
						break;
					}
					break;
				case ESTOP_OFF2:
					estop_off_valid |= hyd_off2_valid;
					switch (estop) {
					case ESTOP_INVALID:
					case ESTOP_OFF1:
					case ESTOP_ON:
						if (estop_ctr[estop] >= HYD_FAULT_TIME) NEWSTATE(estop);
						break;
					}
					break;
				case ESTOP_ONCHECK:
					switch (estop) {
					case ESTOP_INVALID:
					case ESTOP_OFF1:
					case ESTOP_OFF2:
						if (estop_ctr[estop] >= HYD_FAULT_TIME) NEWSTATE(estop);
						break;
					}
					break;
				default:
					NEWSTATE(ESTOP_INVALID);
					break;
				}
			}
		}

	}

	/* If the CNet state is standalone (ringsize <= 1), then override hydraulic type and
	   select standalone mode.
	*/

	if (ringsize <= 1) hyd_type = (hyd_type & ~HYD_MODE_MASK) | HYD_MODE_STANDALONE;

	/* Operate the state machine required for the selected hydraulic type */

	switch (hyd_type & HYD_TYPE_MASK) {
	case 0:	/* Direct hydraulic control */
		hyd_control_0();
		hyd_track_0();
		break;
	case 1:	/* Hydraulic simulation */
		hyd_control_1();
		hyd_track_1();
		break;
	case 2:	/* Pump control */
		hyd_control_2();
		hyd_track_2();
		break;
	case 3:	/* Pump simulation */
		hyd_control_3();
		hyd_track_3();
		break;
	}

	/* Drive physical outputs */

	hyd_output_drive();

	/* Update status on CNet */

	hyd_broadcast_status();

	/* Slaves must clear global requests once the programmed duration has elapsed */

	if (!(hyd_type & HYD_MODE_MASTER)) {
		if (global_hyd_request_ctr) {
			if (--global_hyd_request_ctr == 0) global_hyd_request_out = 0;
		}
	}

	/* For master or slave devices, copy the global hydraulic request state to the local state machine.
	   The correct request byte is selected based on the hydraulic group. A request value of 0xFF is
	   detected and converted to zero causing a shutdown.

	   For standalone devices, check for the specific case of a global hydraulic request having the
	   value REQUEST_GLBL_SHUTDOWN. This must be acted on even for standalone systems.
	*/

	if (!(hyd_type & HYD_MODE_STANDALONE)) {
		hyd_irequest = (global_hyd_request_in >> (hyd_group * 8)) & 0xFF;
		if (hyd_irequest == REQUEST_SHUTDOWN) hyd_irequest = hyd_mrequest = 0;
	}
	else {
		if (global_hyd_request_in == REQUEST_GLBL_SHUTDOWN) hyd_irequest = hyd_mrequest = 0;
	}

	/* Log a LogTypeHydUnloadReq event if the request state has changed to a state of
	   REQUEST_OFF or below.
	*/

	if ((prev_hyd_irequest != hyd_irequest) && (hyd_irequest <= REQUEST_OFF)) {
		eventlog_log(LogTypeHydUnloadReq, 0, 0);
	}
	prev_hyd_irequest = hyd_irequest;

	/* For hydraulic master or standalone devices, operate the state machine for the master */

	if ((hyd_type & (HYD_MODE_MASK)) || (ringsize <= 1)) hyd_master();

	/* Generate hydraulic status information */

	hyd_ack();

	/* Check for activation of the emergency stop switch. When activated, the hydraulic
	   outputs are forced to the off state. This provides a redundant operation in case
	   of failure of the hydraulic safety relay circuit.
	*/

	if (!HYD_IP_ESTOP) {

		/* Emergency stop has been switched off, so shutdown hydraulic outputs */

		dsp_spi_send(0);
		hyd_op_state = 0;
	}
	else {

		/* Check if an output state change has been requested and update the hardware outputs */

		if (hyd_set | hyd_clr) {
			hyd_op_state &= ~hyd_clr;
			hyd_op_state |= hyd_set;
			hyd_set = 0;
			hyd_clr = 0;
			dsp_spi_send(HYD_CONVERT_OP(hyd_op_state));
		}

	}

	/* Process timer for main pressure setpoint tracking. The tracking mode is disabled when
	   either the hydraulic state machine is requesting low or high pressure, or when low or high
	   pressure has been acknowledged.
	*/

	/* CHECK THIS */

	if ((hyd_sp_tracking_control == FALSE) || ((hyd_get_status() & HYD_STATUS_MASK) > HYD_ACK_2)) {
		hyd_dissipation_timer = snvbs->hyd_dissipation * 4;
		hyd_sp_tracking = FALSE;
	}
	else {
		if (hyd_dissipation_timer) {
			hyd_dissipation_timer--;


			/* If the dissipation timer has expired and the hydraulic type is pump and
			   soft stop is enabled, then negate the pressure output in case it had been left
			   enabled after a previous soft stop operation.
			*/

			if ((hyd_dissipation_timer == 0) && ((hyd_type & HYD_TYPE_MASK) == 2) && (hyd_type & HYD_SOFT_STOP)) {
				hyd_drive(0, HYD_OP_3);
			}
		}
		hyd_sp_tracking = (hyd_dissipation_timer) ? FALSE : TRUE;
	}

	CtrlTSRPoll();
}



static char* statename[8] = { "INVALID", "OFF1", "OFF2", "ON", "ONCHECK", "", "", "" };
void netprintf(int s, char* fmt, ...);

/********************************************************************************
  FUNCTION NAME   	: hyd_show_debug
  FUNCTION DETAILS  : Show hydraulic debug information.
********************************************************************************/

void hyd_show_debug(int skt)
{
	int n;
	uint32_t estop = ESTOP_INVALID;
	uint32_t hwflags;
	uint32_t hwver = get_hw_version(NULL, &hwflags);
//	uint32_t loom_type = (hpi_read4(HC_STATUS) & HC_LOOMMASK) >> 24;
	//TODO: hand controller support
	uint32_t loom_type = 2;

	if (hwflags & HW_FLAG_REVF) {
		if (hwver > 0x07) loom_type |= 0x0100;
	}
	netprintf(skt, "Loom Type : %08X\n\r", loom_type);

	if ((hyd_io_state & hyd_fault_mask) == hyd_fault_valid_on) estop = ESTOP_ON;
	else if ((hyd_io_state & hyd_fault_mask) == hyd_fault_valid_off1) estop = ESTOP_OFF1;
	else if ((hyd_io_state & hyd_fault_mask2) == hyd_fault_valid_off2) estop = ESTOP_OFF2;
	else estop = ESTOP_INVALID;

	netprintf(skt, "EStop state: %s current estop: %s validated: %s %s\n\r", statename[estop_state & 7],
		statename[estop & 0x07], (estop_off_valid & 1) ? "Off1" : "", (estop_off_valid & 2) ? "Off2" : "");

	netprintf(skt, "Hyd I/O state       : %04X %s %s %s %s %s %s\n\r", hyd_io_state, ((hyd_io_state & EM1H_SENSE) ? "EM1H" : "    "),
		((hyd_io_state & EM1L_SENSE) ? "EM1L" : "    "),
		((hyd_io_state & EM2H_SENSE) ? "EM2H" : "    "),
		((hyd_io_state & EM2L_SENSE) ? "EM2L" : "    "),
		((hyd_io_state & ES_RELAY_SLAVE) ? "ES_SLAVE" : "        "),
		((hyd_io_state & EM_SENSE) ? "EM_SENSE" : "       "));
	netprintf(skt, "Hyd fault mask      : %04X\n\r", hyd_fault_mask);
	netprintf(skt, "Hyd fault mask2     : %04X\n\r", hyd_fault_mask2);
	netprintf(skt, "Hyd fault valid on  : %04X\n\r", hyd_fault_valid_on);
	netprintf(skt, "Hyd fault valid off1: %04X\n\r", hyd_fault_valid_off1);
	netprintf(skt, "Hyd fault valid off2: %04X\n\r", hyd_fault_valid_off2);

	for (n = 0; n < ESTOP_ONCHECK; n++) {
		netprintf(skt, "State %s ctr %d\n\r", statename[n], estop_ctr[n]);
	}

	netprintf(skt, "Hyd type: %08X\n\r", snvbs->hyd_type);
}

/********************************************************************************
  FUNCTION NAME   	: hyd_set_type
  FUNCTION DETAILS  : Set hydraulic type.
********************************************************************************/

void hyd_set_type(int skt, int new_hyd_type)
{

	netprintf(skt, "Original Hyd type: 0x%08X\n\r", snvbs->hyd_type);
	if (new_hyd_type) {
		snvbs->hyd_type = new_hyd_type;
		SNV_UPDATE(hyd_type, new_hyd_type);
	}
	netprintf(skt, "New Hyd type: 0x%08X\n\r", snvbs->hyd_type);
}

/********************************************************************************
  FUNCTION NAME   	: hyd_read_mrequest
  FUNCTION DETAILS  : Returns hydraulic master request.
********************************************************************************/

uint32_t hyd_read_mrequest()
{
	return hyd_mrequest;
}

/********************************************************************************
*********************************************************************************
* 																				*
* GROUP CONTROL FUNCTIONS														*
*																				*
*********************************************************************************
********************************************************************************/


/********************************************************************************
  FUNCTION NAME   	: grp_update_status
  FUNCTION DETAILS  : Updates group status flags. Called at regular intervals
					  from timer interrupt.
********************************************************************************/

void grp_update_status(void)
{
	int status;

	status = grp_flag_hydinvalid;
	status |= grp_flag_shuntcal;
	status |= ((!CtrlGetSafeToStart()) ? GRP_STARTINHIBIT : 0);
	status |= grp_flag_invalidpressure;
	status |= (slot_txdrfault ? GRP_TXDRFAULT : 0);
	status |= grp_flag_estopfault;
	status |= grp_flag_guardfault;

	local_grp_status = status;
	global_grp_status_out = (snvbs->hyd_type & HYD_MODE_STANDALONE) ? 0 : (status << (hyd_group * 8));
}



/********************************************************************************
  FUNCTION NAME   	: grp_get_status
  FUNCTION DETAILS  : Get current group status flags. Checks for standalone
					  operation and handles accordingly.
********************************************************************************/

static int grp_get_status(void)
{
	int status;

	if (snvbs->hyd_type & HYD_MODE_STANDALONE) {
		status = local_grp_status;
	}
	else {
		status = (CNET_MEM_READ[CNET_DATASLOT_GRPSTATUS] >> (hyd_group * 8)) & 0xFF;
	}
	return(status);
}



/********************************************************************************
  FUNCTION NAME   	: grp_update_shuntcal
  FUNCTION DETAILS  : Called when a change of state of a shunt calibration
					  flag has occurred.
********************************************************************************/

void grp_update_shuntcal(int state)
{
	grp_flag_shuntcal = (state) ? GRP_SHUNTCAL : 0;
}
