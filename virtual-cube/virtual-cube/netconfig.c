/********************************************************************************
 * MODULE NAME       : netconfig.c												*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Network configuration functions.							*
 ********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <stdint.h>
#include "porting.h"

 /* The following two lines are required to allow Events.h to compile */

#define SUPGEN
#define Parameter void

#include "hardware.h"
#include "eeprom.h"
#include "bootp.h"
#include "netconfig.h"
#include "HardwareIndependent\Events.h"

/* Select whether to log DHCP operations to the event log */

#undef LOG_DHCP

/* Define states for configuration state machine */

#define NOLINK			0
#define WAITLINK		1
#define DISABLED		2
#define DHCP_START		3
#define DHCP_INIT		4
#define DHCP_SELECT		5
#define DHCP_REQUEST	6
#define BOOTP_START		7
#define BOOTP_INIT		8
#define BOOTP_WAIT		9
#define LL_INIT			10
#define LL_WAITPROBE	11
#define LL_PROBE		12
#define LL_WAITANNOUNCE	13
#define LL_CLAIM		14
#define MANUAL			15
#define DHCP_BOUND		16
#define DHCP_RENEW		17
#define DHCP_REBIND		18
#define LL_DEFEND		19
#define LL_ATTACK		20
#define COMPLETE		21

#define ETHERNET_TYPE   1       /* Ethernet hardware type */
#define ETHERNET_LEN    6       /* Ethernet hardware byte length */
#define ETHERLEN		6


//#define DHCP_RESPONSE_TIMEOUT		20	/* Time to wait for each DHCP response, in 0.1 sec increments 				 */
#define DHCP_INIT_RESPONSE_TIMEOUT	40	/* Initial time to wait for DHCP response, in 0.1 sec increments			 */
#define DHCP_RETRIES				4	/* Number of timeouts before DHCP configuration is abandoned 	 			 */

#define BOOTP_RESPONSE_TIMEOUT		20	/* Time to wait for each BOOTP response, in 0.1 sec increments 				 */
#define BOOTP_TIMEOUT				5	/* Number of BOOTP_RESPONSE_TIMEOUTs before BOOTP configuration is abandoned */

#define CLKRATE						10	/* Number of calls to netconfig code per second								 */

/* Link-local address configuration constants (taken from RFC3927) */

#define PROBE_WAIT				1	/* Delay period to begin probing											 */
#define PROBE_NUM            	3   /* Number of probe packets													 */
#define PROBE_MIN            	1 	/* Minimum delay till repeated probe										 */
#define PROBE_MAX            	2 	/* Maximum delay till repeated probe										 */
#define ANNOUNCE_WAIT        	2 	/* Delay before announcing													 */
#define ANNOUNCE_NUM         	2   /* Number of announcement packets											 */
#define ANNOUNCE_INTERVAL    	2 	/* Time between announcement packets										 */
#define MAX_CONFLICTS       	10  /* Max conflicts before rate limiting										 */
#define RATE_LIMIT_INTERVAL 	60 	/* Delay between successive attempts										 */
#define DEFEND_INTERVAL     	10 	/* Minimum interval between defensive ARPs									 */

/* New configuration constant added to support multiple defences of an address */

#define MAX_ATTACKS 	      	3   /* Max attacks before giving up on defence of address						 */

#define LINKLOCAL_ADDR          0xA9FE0000	/* Start of link-local address range 169.254.0.0 	*/

/* Macro for generating ARP probe delay */

#define PROBEDELAY				((PROBE_MIN * CLKRATE) + abs(rand32()) % ((PROBE_MAX - PROBE_MIN) * CLKRATE))


static int netconfig_flags = CONFIG_DHCP;	/* Default network configuration mode 				*/
static uint32_t netconfig_delay = 0;			/* Inhibit delay for network configuration restart  */
static int state = NOLINK;					/* Initial network configuration state				*/
static int transaction = 0;					/* Network configuration transaction code			*/

static uint8_t macaddr[ETHERLEN];
static int txskt;
static int arp_sock;
static struct sockaddr_in client;


static int lease_timer = 0xFFFFFFFF;
static int renew_timer = 0xFFFFFFFF;
static int rebind_timer = 0xFFFFFFFF;
static int seconds_timer = 10;
int dhcp_retries = 0;

static uint32_t ipaddr_none = 0;
static uint32_t netmask_none = 0;

static int data_true = 1;

static u_char null_addr[ETHERLEN] = { 0,0,0,0,0,0 };	/* Null MAC address for ARP probe	*/

typedef union {										/* Address being probed				*/
	u_char c[4];
	uint32_t  i;
} ipaddr;

static ipaddr probe_ip = { 0,0,0,0 };
static const ipaddr llnetmask = { 0xFF, 0xFF, 0x00, 0x00 };
int (*arp_intercept)(struct ether_arp* ea) = NULL;	/* Pointer to ARP intercept function */

extern int ip_claim;								/* Address claimed flag				 */
extern int rxarp(struct ether_arp* ea);				/* ARP interception function		 */



/* Function declarations */

void config_interface(uint32_t* addr, uint32_t* netmask);
void netconfig_open_sockets(void);
void netconfig_manual(void);
void pick(ipaddr* ip);
int gentimeout(int retries);
void set_interface_up(void);
void set_interface_down(void);

extern int rand32(void);


/* Structures for setting new IP addresses */

static struct ifreq new_addr = { "ea0", {16, AF_INET, 0, 0, 0x00, 0x00, 0x00, 0x00} };
static struct ifreq new_broadaddr = { "ea0", {16, AF_INET, 0, 0, 0xFF, 0xFF, 0xFF, 0xFF} };
static struct ifreq new_mask = { "ea0", {16, AF_INET, 0, 0, 0x00, 0x00, 0x00, 0x00} };


/********************************************************************************
  FUNCTION NAME     : netconfig_complete
  FUNCTION DETAILS  : Return state of netconfig operation.
********************************************************************************/

int netconfig_complete(void)
{
	return((state >= DHCP_BOUND) ? 1 : 0);
}


/********************************************************************************
  FUNCTION NAME     : netconfig_initialise
  FUNCTION DETAILS  : Initialise network configuration routine ready for
					  operation.
********************************************************************************/

void netconfig_initialise(void)
{
	config_network config;

	netconfig_flags = CONFIG_DHCP;
	state = NOLINK;

	read_mac_address(macaddr);	/* Read our MAC address */

	/* Initialise pseudo random number generator from MAC address and current timer value */

	seed(((macaddr[ETHERLEN - 4] << 24) |
		(macaddr[ETHERLEN - 3] << 16) |
		(macaddr[ETHERLEN - 2] << 8) |
		(macaddr[ETHERLEN - 1] << 0)) + TIMER_getCount(hTimer1));

	netconfig_flags = CONFIG_DHCP;
	if (!dsp_1w_readpage(PAGE_NETCONFIG, (uint8_t*)&config, sizeof(config), CHK_CHKSUM)) {
		if (config.source <= CONFIG_NONE) netconfig_flags = config.source;
	}

	/* Initialise transaction ID */

	transaction = rand32();
	netconfig_open_sockets();
}



/********************************************************************************
  FUNCTION NAME     : netconfig_open_sockets
  FUNCTION DETAILS  : Create and open sockets for network configuration routine.
********************************************************************************/

void netconfig_open_sockets(void)
{
	txskt = socket(AF_INET, SOCK_DGRAM, 0, NULL);
	if (txskt < 0) {
		LOG_printf(&trace, "Error opening request socket");
		diag_error(DIAG_SOCKET);
	}

	arp_sock = socket(AF_INET, SOCK_DGRAM, 0, NULL);
	if (arp_sock < 0) {
		LOG_printf(&trace, "Error opening ARP socket");
		diag_error(DIAG_SOCKET);
	}

	/* Create local address structure */

	client.sin_len = sizeof(struct sockaddr_in);
	client.sin_family = AF_INET;
	client.sin_addr.s_addr = INADDR_ANY;
	client.sin_port = htons(68);

	/* Enable non-blocking operation */

	socketioctl(txskt, FIONBIO, &data_true, NULL);

	/* Allow broadcast on socket */

	setsockopt(txskt, SOL_SOCKET, SO_BROADCAST, &data_true, sizeof(int), NULL);

	/* Allow reuse of port and address */

	setsockopt(txskt, SOL_SOCKET, SO_REUSEPORT, &data_true, sizeof(int), NULL);

	bind(txskt, (struct sockaddr*)&client, sizeof(client), NULL);
}



/********************************************************************************
  FUNCTION NAME     : netconfig_close_sockets
  FUNCTION DETAILS  : Create and open sockets for network configuration routine.
********************************************************************************/

void netconfig_close_sockets(void)
{
	socketclose(txskt, NULL);
	socketclose(arp_sock, NULL);
}



/********************************************************************************
  FUNCTION NAME     : netconfig_connect_client
  FUNCTION DETAILS  : Connect client to server.
********************************************************************************/

void netconfig_connect_client(int s, struct sockaddr_in* server)
{
	struct sockaddr_in client;

	client.sin_len = sizeof(struct sockaddr_in);
	client.sin_family = AF_INET;
	client.sin_addr.s_addr = (server) ? server->sin_addr.s_addr : INADDR_ANY;
	client.sin_port = htons(68);

	connect(s, (struct sockaddr*)&server, sizeof(server), NULL);
	bind(s, (struct sockaddr*)&client, sizeof(client), NULL);
}



/********************************************************************************
  FUNCTION NAME     : netconfig_connect_server
  FUNCTION DETAILS  : Connect client to server.
********************************************************************************/

void netconfig_connect_server(int s, struct sockaddr_in* serverid)
{
	struct sockaddr_in server;

	server.sin_len = sizeof(struct sockaddr_in);
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = (serverid) ? serverid->sin_addr.s_addr : INADDR_ANY;
	server.sin_port = htons(67);

	connect(s, (struct sockaddr*)&server, sizeof(server), NULL);
}



/********************************************************************************
  FUNCTION NAME     : netconfig_restart
  FUNCTION DETAILS  : Restart network configuration.
********************************************************************************/

void netconfig_restart(uint32_t delay)
{
	config_network config;

	netconfig_flags = CONFIG_DHCP;
	if (!dsp_1w_readpage(PAGE_NETCONFIG, (uint8_t*)&config, sizeof(config), CHK_CHKSUM)) {
		if (config.source <= CONFIG_NONE) netconfig_flags = config.source;
	}
	state = NOLINK;
	netconfig_delay = delay;
}



/* Time delay before the link is considered down when the network cable has
   been unplugged.
*/

#define LINKDOWNDELAY	(30 * 10)

/********************************************************************************
  FUNCTION NAME     : netconfig
  FUNCTION DETAILS  : Perform configuration of network interface using
					  either DHCP, BOOTP, manual configuration or automatically
					  selected link-local address.

					  This routine is called at regular intervals of 0.1s by
					  the tcpip_timerintr routine.
********************************************************************************/

void netconfig(void)
{
	static int try = 0;
	static int dhcptimeout = 0;
	static int retries = 0;
	static int nprobes = 0;
	static int nclaims = 0;
	static int delay = 0;
	static int conflicts = 0;
	static int ratelimit = 0;
	static int attacks = 0;
	static int linkdowntimer = LINKDOWNDELAY;
	int link;
	int response;
	static struct sockaddr_in serverid;
	static struct sockaddr_in clientid;
	struct sockaddr_in addr;
	struct sockaddr_in netmask;
	int lease;
	int err;

	if (--seconds_timer == 0) {
		seconds_timer = 10;
		if (renew_timer && (renew_timer != 0xFFFFFFFF)) renew_timer--;
		if (rebind_timer && (rebind_timer != 0xFFFFFFFF)) rebind_timer--;
		if (lease_timer && (lease_timer != 0xFFFFFFFF)) lease_timer--;
	}

	link = smc_linkstatus();

	if (state != NOLINK) {
		if (!link) {
			if (linkdowntimer && (--linkdowntimer == 0)) {
				state = NOLINK;
				set_interface_down();
			}
		}
		else {
			linkdowntimer = LINKDOWNDELAY;
		}
	}

	switch (state) {

	case NOLINK:
		if (link) {
			set_interface_up();
			netconfig_delay = 10;
			state = WAITLINK;
		}
		break;

	case WAITLINK:
		if (netconfig_delay) { netconfig_delay--; break; }	/* Inhibit operation */
		if (link) {
			set_interface_up();
			config_interface(&ipaddr_none, &netmask_none);
			netconfig_delay = 10;
			state = DISABLED;
		}
		break;

	case DISABLED:
		if (netconfig_delay) { netconfig_delay--; break; }	/* Inhibit operation */
		config_interface(&ipaddr_none, &netmask_none);
		dhcp_retries = 0;
		switch (netconfig_flags) {
			//      case CONFIG_BOOTP:
			//   	    state = BOOTP_START;	/* Start BOOTP operation */
			//        break;
		case CONFIG_MANUAL:
			state = MANUAL; 	  	/* Perform manual configuration */
			break;
		default:
			state = DHCP_START; 	/* Start DHCP operation */
			break;
		}
		break;

	case DHCP_START:
		retries = 0;
		dhcptimeout = gentimeout(0);

		/* Fall through to DHCP initialisation code */

	case DHCP_INIT:
#ifdef LOG_DHCP
		eventlog_log(LogTypeDHCPEvent, 1, 0);
#endif
		transaction = rand32();
		err = dhcp_discover(txskt, transaction, macaddr);
		try = 0;
		state = DHCP_SELECT;
		break;

	case DHCP_SELECT:
		response = dhcp_response(txskt, transaction, macaddr, &serverid, NULL, &addr, &netmask);
		if (response == DHCP_DHCPOFFER) {

#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 2, 0);
#endif

			/* May want to check here if offered address is valid by means of an ARP probe */

			transaction = rand32();
			dhcp_request(txskt, transaction, macaddr, &serverid, NULL, &addr, TRUE);
			retries = 0;
			try = 0;
			dhcptimeout = gentimeout(0);
			state = DHCP_REQUEST;
		}
		else {
			/* Error receiving response */
			if (++try >= dhcptimeout) {
#ifdef LOG_DHCP
				eventlog_log(LogTypeDHCPEvent, 3, 0);
#endif
				if (++retries >= DHCP_RETRIES)
					state = LL_INIT;		/* Give up with DHCP, select link-local address instead */
				else {
					dhcptimeout = gentimeout(retries);
					state = DHCP_INIT;	/* Try again */
				}
				dhcp_retries++;
			}
		}
		break;

	case DHCP_REQUEST:
		response = dhcp_response(txskt, transaction, macaddr, NULL, &lease, &addr, &netmask);
		if (response == DHCP_DHCPACK) {
#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 4, 0);
#endif
			renew_timer = (lease / 2);
			rebind_timer = ((lease / 8) * 7);
			lease_timer = lease;
			config_interface(&addr.sin_addr.s_addr, &netmask.sin_addr.s_addr);
			clientid = addr;
			state = DHCP_BOUND;
			dhcptimeout = gentimeout(0);
			retries = 0;
			try = 0;
		}
		else if (response == DHCP_DHCPNAK) {
#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 5, 0);
#endif
			retries = 0;
			state = DHCP_INIT; /* Negative acknowledge from server, so restart configuration */
		}
		else {
			/* Error receiving response */
			if (++try >= dhcptimeout) {
#ifdef LOG_DHCP
				eventlog_log(LogTypeDHCPEvent, 6, 0);
#endif
				if (++retries >= DHCP_RETRIES)
					state = LL_INIT;			/* Give up with DHCP, select link-local address instead */
				else {
#ifdef LOG_DHCP
					eventlog_log(LogTypeDHCPEvent, 7, 0);
#endif
					transaction = rand32();
					dhcp_request(txskt, transaction, macaddr, &serverid, INADDR_ANY, &addr, TRUE); /* Try again */
					dhcptimeout = gentimeout(retries);
					state = DHCP_SELECT;
					dhcp_retries++;
				}
			}
		}
		break;

	case DHCP_BOUND:
		if (renew_timer == 0) {
#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 8, 0);
#endif
			transaction = rand32();
			dhcp_request(txskt, transaction, macaddr, &serverid, &clientid, &clientid, FALSE); /* Send request to renew lease */
			retries = 0;
			try = 0;
			state = DHCP_RENEW;
		}
		break;

	case DHCP_RENEW:
		response = dhcp_response(txskt, transaction, macaddr, NULL, &lease, &addr, &netmask);
		if (response == DHCP_DHCPACK) {
#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 9, 0);
#endif
			renew_timer = (lease / 2);
			rebind_timer = ((lease / 8) * 7);
			lease_timer = lease;
			state = DHCP_BOUND;
		}
		else if (response == DHCP_DHCPNAK) {
#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 10, 0);
#endif
			state = DHCP_INIT; /* Negative acknowledge from server, so restart configuration */
			retries = 0;
		}
		else if (retries < DHCP_RETRIES) { /* No response from server, but rebind time not reached,
											  so retry the request.
										   */
			if (++try >= dhcptimeout) {
#ifdef LOG_DHCP
				eventlog_log(LogTypeDHCPEvent, 11, 0);
#endif
				if (++retries < DHCP_RETRIES) {
					transaction = rand32();
					dhcp_request(txskt, transaction, macaddr, &serverid, &clientid, &clientid, FALSE); /* Send request to renew lease */
					dhcptimeout = gentimeout(retries);
					try = 0;
					dhcp_retries++;
				}
			}
		}
		else if (rebind_timer == 0) {	/* No response from server and rebind time reached. Send
										   rebind request and change state.
										*/
#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 12, 0);
#endif
			transaction = rand32();
			dhcp_request(txskt, transaction, macaddr, INADDR_ANY, &clientid, &clientid, TRUE); /* Send broadcast request to renew lease */
			retries = 0;
			try = 0;
			dhcptimeout = gentimeout(0);
			state = DHCP_REBIND;
		}
		break;

	case DHCP_REBIND:
		response = dhcp_response(txskt, transaction, macaddr, NULL, &lease, &addr, &netmask);
		if (response == DHCP_DHCPACK) {
#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 13, 0);
#endif
			renew_timer = (lease / 2);
			rebind_timer = ((lease / 8) * 7);
			lease_timer = lease;
			state = DHCP_BOUND;
		}
		else if (response == DHCP_DHCPNAK) {
#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 14, 0);
#endif
			state = DHCP_INIT; /* Negative acknowledge from server, so restart configuration */
		}
		else if (retries < DHCP_RETRIES) { /* No response from server, so retry the request */
			if (++try >= dhcptimeout) {
#ifdef LOG_DHCP
				eventlog_log(LogTypeDHCPEvent, 15, 0);
#endif
				if (++retries < DHCP_RETRIES) {
					transaction = rand32();
					dhcp_request(txskt, transaction, macaddr, INADDR_ANY, &clientid, &clientid, TRUE); /* Send broadcast request to renew lease */
					dhcptimeout = gentimeout(retries);
					try = 0;
					dhcp_retries++;
				}
			}
		}
		else if (lease_timer == 0) { /* No response from server, lease expired */
#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 16, 0);
#endif
			retries = 0;
			state = DHCP_INIT; /* Lease has expired */
		}
		break;

	case BOOTP_START:
		retries = 0;

		/* Fall through to BOOTP initialisation code */

	case BOOTP_INIT:
		transaction = rand32();
		bootp_request(txskt, transaction, macaddr);
		state = BOOTP_WAIT;
		break;

	case BOOTP_WAIT:
		response = bootp_response(txskt, transaction, macaddr, &addr, &netmask);
		if (response) {
			config_interface(&addr.sin_addr.s_addr, &netmask.sin_addr.s_addr);
			state = COMPLETE;
		}
		else {
			/* Error receiving response */
			if (++try >= BOOTP_RESPONSE_TIMEOUT) {
				if (++retries >= BOOTP_TIMEOUT)
					state = DHCP_START;	/* Give up with BOOTP, try DHCP instead */
				else
					state = BOOTP_INIT;	/* Try again */
			}
		}
		break;

	case LL_INIT:
		arp_intercept = &rxarp;							/* Claim ARP handler 	   */
		delay = abs(rand32()) % (PROBE_WAIT * CLKRATE);	/* Initialise probe delay  */
		state = LL_WAITPROBE;
		break;

	case LL_WAITPROBE:
		if (delay)
			delay--;
		else {
			pick(&probe_ip);	/* Pick initial IP address */
			nprobes = 0;	 	/* Reset probe counter     */
			ip_claim = 0;	 	/* Reset IP claim flag     */
			conflicts = 0;	/* Reset conflicts count   */
			ratelimit = 0;	/* Reset rate limiting	   */
			state = LL_PROBE;
		}
		break;

	case LL_PROBE:
		if (ip_claim) {

			/* If the maximum number of conflicts has been reached, then rate limiting
			   of the ARP probes comes into effect.
			*/

			if (++conflicts >= MAX_CONFLICTS) {
				conflicts = MAX_CONFLICTS;
				ratelimit = RATE_LIMIT_INTERVAL * CLKRATE;
			}

			/* Pick a new address to probe */

			pick(&probe_ip);
			nprobes = 0; 	/* Reset probe counter */
			ip_claim = 0;	/* Reset IP claim flag */
		}
		if (delay)
			delay--;
		else {
#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 17, 0);
#endif
			arp(arp_sock, 0, macaddr, probe_ip.i, null_addr); /* Send ARP probe	*/
			delay = (ratelimit) ? ratelimit : PROBEDELAY;
			if (++nprobes >= PROBE_NUM) {
				ip_claim = 0;	/* Reset IP claim flag */
				delay = ANNOUNCE_WAIT * CLKRATE;
				state = LL_WAITANNOUNCE;
			}
		}
		break;

	case LL_WAITANNOUNCE:
		if (ip_claim) {

#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 18, 0);
#endif

			/* If the maximum number of conflicts has been reached, then rate limiting
			   of the ARP probes comes into effect.
			*/

			if (++conflicts >= MAX_CONFLICTS) {
				conflicts = MAX_CONFLICTS;
				ratelimit = RATE_LIMIT_INTERVAL * CLKRATE;
			}

			/* Pick a new address to probe */

			pick(&probe_ip);
			nprobes = 0; 	/* Reset probe counter */
			ip_claim = 0;	/* Reset IP claim flag */
			delay = (ratelimit) ? ratelimit : PROBEDELAY;
			state = LL_PROBE;
		}
		if (delay)
			delay--;
		else {

			/* Announcement delay has expired, so we can begin announcing our IP address */

			delay = 0;
			nclaims = 0;	/* Reset claim counter */
			state = LL_CLAIM;
		}
		break;

		/* The probed IP address is clear, so we can claim it */

	case LL_CLAIM:
		if (ip_claim) {

#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 19, 0);
#endif

			/* If the maximum number of conflicts has been reached, then rate limiting
			   of the ARP probes comes into effect.
			*/

			if (++conflicts >= MAX_CONFLICTS) {
				conflicts = MAX_CONFLICTS;
				ratelimit = RATE_LIMIT_INTERVAL * CLKRATE;
			}

			/* Pick a new address to probe */

			pick(&probe_ip);
			ip_claim = 0;
			state = LL_PROBE;
			break;
		}
		if (delay)
			delay--;
		else {
			if (++nclaims > ANNOUNCE_NUM) {
				/* No other device has claimed this address, so we can use it */
#ifdef LOG_DHCP
				eventlog_log(LogTypeDHCPEvent, 20, 0);
#endif
				config_interface(&probe_ip.i, (uint32_t*)&llnetmask.i);
				ip_claim = 0;
				nclaims = 0;
				delay = 0;
				state = LL_DEFEND;
			}
			else {
				arp(arp_sock, probe_ip.i, macaddr, probe_ip.i, macaddr); /* Send ARP claim */
				delay = ANNOUNCE_INTERVAL * CLKRATE;
			}
		}
		break;

	case LL_DEFEND:

		/* Check if we're being attacked and defend if required (ARP claim is sent
		   automatically by low-level network code, so we don't need to bother).
		 */

		if (ip_claim) {
#ifdef LOG_DHCP
			eventlog_log(LogTypeDHCPEvent, 21, 0);
#endif
			ip_claim = 0;
			delay = DEFEND_INTERVAL * CLKRATE;
			attacks++;
			state = LL_ATTACK; /* We're being attacked */
		}
		break;

	case LL_ATTACK:

		if (--delay) {

			/* If we're attacked more than MAX_ATTACKS times within the DEFEND_INTERVAL, then relinquish this
			   address and claim another.
			*/

			if (ip_claim) {
#ifdef LOG_DHCP
				eventlog_log(LogTypeDHCPEvent, 22, 0);
#endif
				ip_claim = 0;
				if (++attacks >= MAX_ATTACKS) {
#ifdef LOG_DHCP
					eventlog_log(LogTypeDHCPEvent, 23, 0);
#endif
					pick(&probe_ip);
					attacks = 0;
					delay = 0;
					state = LL_WAITPROBE;
				}
			}
		}
		else {

			/* Previous attacks are forgotten after the DEFEND_INTERVAL has elapsed */

			attacks = 0;
			state = LL_DEFEND;
		}
		break;

	case MANUAL:

#ifdef LOG_DHCP
		eventlog_log(LogTypeDHCPEvent, 24, 0);
#endif
		netconfig_manual(); /* Configure from manual configuration */
		state = COMPLETE;
		break;

	case COMPLETE:
		break;

	default:
		state = NOLINK;
		break;
	}
}



/********************************************************************************
  FUNCTION NAME     : netconfig_manual
  FUNCTION DETAILS  : Configure network from manual configuration data.
********************************************************************************/

void netconfig_manual(void)
{
	config_network config = { CONFIG_MANUAL, {0,0,0,0}, {255,255,255,0} };
	uint32_t ipaddr;
	uint32_t netmask;

	if (!dsp_1w_readpage(PAGE_NETCONFIG, (uint8_t*)&config, sizeof(config), CHK_CHKSUM)) {
		memcpy(&ipaddr, config.ipaddr, sizeof(uint32_t));
		memcpy(&netmask, config.subnetmask, sizeof(uint32_t));
		config_interface(&ipaddr, &netmask);
	}
}



/********************************************************************************
  FUNCTION NAME     : netconfig_setconfig
  FUNCTION DETAILS  : Set network configuration.
********************************************************************************/

int netconfig_setconfig(uint32_t update, uint32_t source, uint8_t* ipaddr, uint8_t* subnetmask)
{
	config_network config;
	int err;

	config.source = source;
	memcpy(&config.ipaddr, ipaddr, 4);
	memcpy(&config.subnetmask, subnetmask, 4);

	if (err = dsp_1w_writepage(PAGE_NETCONFIG, (uint8_t*)&config, sizeof(config), WR_CHKSUM)) {
		return(err);
	}

	dsp_delay(100000);	/* Delay while one-wire write operation completes */

	if (update) {
		netconfig_restart(update);
	}

	return(0);
}



static struct sockaddr_in default_addr = { 16, AF_INET, 0, 0 };
static struct sockaddr_in default_mask = { 0, AF_INET, 0, 0 };

/********************************************************************************
  FUNCTION NAME     : config_interface
  FUNCTION DETAILS  : Configure interface with IP address and netmask.
********************************************************************************/

void config_interface(uint32_t* addr, uint32_t* netmask)
{
	int s;
	uint8_t rtbuf[sizeof(struct rt_msghdr) + (8 * sizeof(struct sockaddr))];
	struct rt_msghdr* rt;
	struct sockaddr* p;
	struct sockaddr_in ifaddr = { 16, AF_INET, 0, 0, {0,0,0,0,0,0,0,0} };
	int err;

	if (!addr || !netmask) return;

	eventlog_log(LogTypeNetworkConfig, *addr, *netmask);

	memcpy(&new_addr.ifr_addr.sa_data[2], addr, sizeof(uint32_t));
	memcpy(&new_mask.ifr_addr.sa_data[2], netmask, sizeof(uint32_t));

	/* Redefine the interface address to match the programmed IP address */

	s = socket(AF_INET, SOCK_DGRAM, 0, NULL);
	if (s < 0) {
		LOG_printf(&trace, "Error config interface");
		diag_error(DIAG_SOCKET);
	}

	socketioctl(s, SIOCSIFADDR, (int*)&new_addr, NULL);
	socketioctl(s, SIOCSIFBRDADDR, (int*)&new_broadaddr, NULL);
	socketioctl(s, SIOCSIFNETMASK, (int*)&new_mask, NULL);

	socketclose(s, NULL);

	/* If this is configuring a non-zero IP address, add a default route for this interface */

	if (*addr) {

		rt = (struct rt_msghdr*)&rtbuf;

		memset(rt, 0, sizeof(rtbuf));
		rt->rtm_version = RTM_VERSION;
		rt->rtm_type = RTM_ADD;
		rt->rtm_flags = RTF_UP | RTF_STATIC;
		rt->rtm_addrs = RTA_DST | RTA_GATEWAY | RTA_NETMASK;

		memcpy(&ifaddr.sin_addr, addr, sizeof(struct in_addr));

		p = (struct sockaddr*)(rt + 1);

		memcpy(p++, &default_addr, sizeof(struct sockaddr));
		memcpy(p++, &ifaddr, sizeof(struct sockaddr));
		memcpy(p++, &default_mask, sizeof(struct sockaddr));

		rt->rtm_msglen = (uint8_t*)p - (uint8_t*)rt;

		s = socket(AF_ROUTE, SOCK_RAW, 0, NULL);
		if (s < 0) {
			LOG_printf(&trace, "Error config route");
			diag_error(DIAG_SOCKET);
		}

		err = socketwrite(s, rt, rt->rtm_msglen, NULL);

		socketclose(s, NULL);

	}

}



/* Structures for reading IP addresses */

static struct ifreq reqaddr = { "ea0", {16, AF_INET, 0, 0, 0x00, 0x00, 0x00, 0x00} };
static struct ifreq reqmask = { "ea0", {16, AF_INET, 0, 0, 0xFF, 0xFF, 0xFF, 0x00} };

/********************************************************************************
  FUNCTION NAME     : read_interface
  FUNCTION DETAILS  : Read interface IP address and netmask.
********************************************************************************/

void read_interface(uint32_t* addr, uint32_t* netmask)
{
	int s;

	/* Read the interface address */

	s = socket(AF_INET, SOCK_DGRAM, 0, NULL);
	if (s < 0) {
		LOG_printf(&trace, "Error config");
		diag_error(DIAG_SOCKET);
	}

	socketioctl(s, SIOCGIFADDR, (int*)&reqaddr, NULL);
	socketioctl(s, SIOCGIFNETMASK, (int*)&reqmask, NULL);

	if (addr) memcpy(addr, &reqaddr.ifr_addr.sa_data[2], sizeof(uint32_t));
	if (netmask) memcpy(netmask, &reqmask.ifr_addr.sa_data[2], sizeof(uint32_t));

	socketclose(s, NULL);
}



/* Structures for setting/reading interface flags */

static struct ifreq reqflags = { "ea0", {0} };

/********************************************************************************
  FUNCTION NAME     : set_interface_down
  FUNCTION DETAILS  : Set the state of the interface to down.
********************************************************************************/

void set_interface_down(void)
{
	int s;

	s = socket(AF_INET, SOCK_DGRAM, 0, NULL);
	if (s < 0) {
		LOG_printf(&trace, "Error config");
		diag_error(DIAG_SOCKET);
	}

	socketioctl(s, SIOCGIFFLAGS, (int*)&reqflags, NULL);
	if (reqflags.ifr_ifru.ifru_flags & IFF_UP) {
		reqflags.ifr_ifru.ifru_flags &= IFF_UP;
		socketioctl(s, SIOCSIFFLAGS, (int*)&reqflags, NULL);
	}
	socketclose(s, NULL);
}



/********************************************************************************
  FUNCTION NAME     : set_interface_up
  FUNCTION DETAILS  : Set the state of the interface to up.
********************************************************************************/

void set_interface_up(void)
{
	int s;

	s = socket(AF_INET, SOCK_DGRAM, 0, NULL);
	if (s < 0) {
		LOG_printf(&trace, "Error config");
		diag_error(DIAG_SOCKET);
	}

	socketioctl(s, SIOCGIFFLAGS, (int*)&reqflags, NULL);
	if (!(reqflags.ifr_ifru.ifru_flags & IFF_UP)) {
		reqflags.ifr_ifru.ifru_flags |= IFF_UP;
		socketioctl(s, SIOCSIFFLAGS, (int*)&reqflags, NULL);
	}
	socketclose(s, NULL);
}



/********************************************************************************
  FUNCTION NAME   	: rxarp
  FUNCTION DETAILS  : Received an ARP packet.
********************************************************************************/

int rxarp(struct ether_arp* ea)
{
	LOG_printf(&trace, "RX ARP");
	if (memcmp(ea->arp_spa, &probe_ip.c, sizeof(probe_ip)) == 0) {
		/* Address already claimed */
		LOG_printf(&trace, "Address claimed");
		ip_claim = TRUE;
	}
	return(0);
}



/********************************************************************************
  FUNCTION NAME   	: pick
  FUNCTION DETAILS  : Pick a trial IP address.

					  Addresses are in the group specified by LINKLOCAL_ADDR
					  (usually 169.254.x.x) and are in the range 169.254.1.0 to
					  169.254.254.255.

********************************************************************************/

void pick(ipaddr* ip)
{
	ip->i = htonl(LINKLOCAL_ADDR | ((abs(rand32()) % 0xFE00) + 0x0100));
}



#define TIMEOUT_SCALE ((int)(0x7FFFFFFFL / 10))

/********************************************************************************
  FUNCTION NAME     : gentimeout
  FUNCTION DETAILS  : Generate DHCP timeout value.
********************************************************************************/

int gentimeout(int retries)
{
	int timeout;

	timeout = DHCP_INIT_RESPONSE_TIMEOUT << retries;
	timeout += (rand32() / TIMEOUT_SCALE);

	return(timeout);
}




