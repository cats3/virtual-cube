/********************************************************************************
 * MODULE NAME       : hw_2dig.c												*
 * PROJECT			 : Control Cube												*
 * MODULE DETAILS    : Interface routines for 2Dig card.						*
 *																				*
 ********************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <stdint.h>
#include "porting.h"


#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"
#include "controller.h"


 /********************************************************************************
   FUNCTION NAME     : hw_2dig_1w_init
   FUNCTION DETAILS  : Initialise 1-wire port.
 ********************************************************************************/

void hw_2dig_1w_init(info_2dig* info, uint32_t bus)
{
	//hw_2dig* base;

	//base = info->hw;
	//if (bus) {
	//	base->u.wr.teds2 = ONEW_INIT;			/* Initialise 1-wire controller */
	//	while (base->u.rd.teds2 & ONEW_BUSY);	/* Wait for not busy */
	//	base->u.wr.teds2 = 0;

	//	base->u.wr.teds2 = ONEW_RESET;			/* Reset TEDS2 1-wire bus */
	//}
	//else {
	//	base->u.wr.teds1 = ONEW_INIT;			/* Initialise 1-wire controller */
	//	while (base->u.rd.teds1 & ONEW_BUSY);	/* Wait for not busy */
	//	base->u.wr.teds1 = 0;

	//	base->u.wr.teds1 = ONEW_RESET;			/* Reset TEDS1 1-wire bus */
	//}
}



/********************************************************************************
  FUNCTION NAME     : hw_2dig_1w_reset
  FUNCTION DETAILS  : Reset all devices on 1-wire bus.

					  On exit:

					  Returns zero if a device is present on the 1-wire bus.

********************************************************************************/

int hw_2dig_1w_reset(info_2dig* info, uint32_t bus)
{
	//hw_2dig* base;
	//int present;

	//base = info->hw;

	//if (bus) {

	//	/* Access TEDS bus 2 */

	//	while (base->u.rd.teds2 & ONEW_BUSY);	/* Wait for not busy */
	//	base->u.wr.teds2 = ONEW_RESET;			/* Reset 1-wire bus */
	//	while (base->u.rd.teds2 & ONEW_BUSY);	/* Wait for completion */
	//	present = (base->u.rd.teds2 & ONEW_PRES) ? 1 : 0;	/* Read presence flag */

	//}
	//else {

	//	/* Access TEDS bus 1 */

	//	while (base->u.rd.teds1 & ONEW_BUSY);	/* Wait for not busy */
	//	base->u.wr.teds1 = ONEW_RESET;			/* Reset 1-wire bus */
	//	while (base->u.rd.teds1 & ONEW_BUSY);	/* Wait for completion */
	//	present = (base->u.rd.teds1 & ONEW_PRES) ? 1 : 0;	/* Read presence flag */

	//}

	//return(present);
	return 0;
}



/********************************************************************************
  FUNCTION NAME     : hw_2dig_1w_write
  FUNCTION DETAILS  : Write a byte to the 1-wire bus.

					  On entry:

					  bus	Determines which TEDS bus to access.

********************************************************************************/

void hw_2dig_1w_write(info_2dig* info, uint32_t bus, uint8_t data)
{
	//hw_2dig* base;
	//int n;

	//base = info->hw;

	//n = 8;

	//if (bus) {

	//	/* Access TEDS bus 2 */

	//	while (n) {
	//		while (base->u.rd.teds2 & ONEW_BUSY);			/* Wait for not busy */
	//		base->u.wr.teds2 = ONEW_WRITE0 | (data & 0x01);	/* Write bit to 1-wire bus */
	//		data = data >> 1;
	//		n--;
	//	}

	//}
	//else {

	//	/* Access TEDS bus 1 */

	//	while (n) {
	//		while (base->u.rd.teds1 & ONEW_BUSY);			/* Wait for not busy */
	//		base->u.wr.teds1 = ONEW_WRITE0 | (data & 0x01);	/* Write bit to 1-wire bus */
	//		data = data >> 1;
	//		n--;
	//	}

	//}
}



/********************************************************************************
  FUNCTION NAME     : hw_2dig_1w_read
  FUNCTION DETAILS  : Read a byte from the 1-wire bus.

					  On entry:

					  bus	Determines which TEDS bus to access.

********************************************************************************/

uint8_t hw_2dig_1w_read(info_2dig* info, uint32_t bus)
{
	//hw_2dig* base;
	//uint8_t data = 0;
	//int n;

	//base = info->hw;

	//if (bus) {

	//	/* Access TEDS bus 2 */

	//	n = 8;
	//	while (n) {
	//		while (base->u.rd.teds2 & ONEW_BUSY);	/* Wait for not busy */
	//		base->u.wr.teds2 = ONEW_READ;			/* Initiate read operation */
	//		while (base->u.rd.teds2 & ONEW_BUSY);	/* Wait for completion */
	//		data = data >> 1;
	//		if (base->u.rd.teds2 & ONEW_DATA) {		/* Test 1-wire input */
	//			data |= 0x80;						/* Set received data bit if required */
	//		}
	//		n--;
	//	}

	//}
	//else {

	//	/* Access TEDS bus 1 */

	//	n = 8;
	//	while (n) {
	//		while (base->u.rd.teds1 & ONEW_BUSY);	/* Wait for not busy */
	//		base->u.wr.teds1 = ONEW_READ;			/* Initiate read operation */
	//		while (base->u.rd.teds1 & ONEW_BUSY);	/* Wait for completion */
	//		data = data >> 1;
	//		if (base->u.rd.teds1 & ONEW_DATA) {		/* Test 1-wire input */
	//			data |= 0x80;						/* Set received data bit if required */
	//		}
	//		n--;
	//	}

	//}
	//return(data);						/* Return data value */
	return(0);
}



/********************************************************************************
  FUNCTION NAME     : hw_2dig_tedsid
  FUNCTION DETAILS  : Read ID of attached TEDS device.
********************************************************************************/

int hw_2dig_tedsid(chandef* def, uint32_t* lo, uint32_t* hi)
{
	//info_2dig* i;
	//uint32_t bus;
	struct rom {
		union {
			uint32_t w;
			uint8_t  b[4];
		} lo;
		union {
			uint32_t w;
			uint8_t  b[4];
		} hi;
	} rom;

	//i = &def->hw.i_2dig;	/* 2DIG specific information 	*/
	//bus = i->input;

	//if (hw_2dig_1w_reset(i, bus)) return(NO_1WIRE);

	//hw_2dig_1w_write(i, bus, 0x33);		/* Send READ_ROM command */

	///* Read 8 byte ROM code from device */

	//rom.hi.b[3] = hw_2dig_1w_read(i, bus);
	//rom.hi.b[2] = hw_2dig_1w_read(i, bus);
	//rom.hi.b[1] = hw_2dig_1w_read(i, bus);
	//rom.hi.b[0] = hw_2dig_1w_read(i, bus);

	//rom.lo.b[3] = hw_2dig_1w_read(i, bus);
	//rom.lo.b[2] = hw_2dig_1w_read(i, bus);
	//rom.lo.b[1] = hw_2dig_1w_read(i, bus);
	//rom.lo.b[0] = hw_2dig_1w_read(i, bus);

	/* Store into supplied variables */

	rom.hi.w = 0xdeadbeef;
	rom.lo.w = 0x55aa55aa;

	*hi = rom.hi.w;
	*lo = rom.lo.w;

	return(dsp_1w_checkcrc(rom.lo.w, rom.hi.w));
}



/********************************************************************************
  FUNCTION NAME     : hw_2dig_led_ctrl
  FUNCTION DETAILS  : Control LEDs on 2DIG card.
********************************************************************************/

void hw_2dig_led_ctrl(chandef* def, int state)
{
	printf("led:%d|%d", def->identifier, state);
	//fprintf(stdout, "%s: [%d]\n", __FUNCTION__, state);
	//info_2dig* i;
	//hw_2dig* base;
	//uint8_t newstate;

	//i = &def->hw.i_2dig;	/* 2DIG specific information 	*/
	//base = i->hw;			/* Hardware base address 		*/

	//newstate = *(i->ledstate) & ~(i->ledmask);	/* Clear current LED state 	*/
	//newstate |= (state) ? (i->ledmask) : 0;		/* Update with new state	*/

	//*(i->ledstate) = newstate;
	//base->u.wr.leds = *(i->ledstate);
}



/********************************************************************************
  FUNCTION NAME     : hw_2dig_ctr_reset
  FUNCTION DETAILS  : Reset quadrature encoder counters on 2DIG card.
********************************************************************************/

void hw_2dig_ctr_reset(chandef* def)
{
	fprintf(stdout, "%s: \n", __FUNCTION__);

	//info_2dig* i;

	//i = &def->hw.i_2dig;	/* 2DIG specific information */

	//if (EXTSLOTCHAN) {
	//	i->hw->u.wr.reset2 = 0x0001;
	//	i->hw->u.wr.reset2 = 0x0000;
	//}
	//else {
	//	i->hw->u.wr.reset1 = 0x0001;
	//	i->hw->u.wr.reset1 = 0x0000;
	//}
}



/* Reference clock and sample rate for digital transducer card */

#define REFCLK 	(4096.0 * 400 * 24)
#define FS		4096.0

/********************************************************************************
  FUNCTION NAME     : hw_set_2dig_txdr_config
  FUNCTION DETAILS  : Write configuration settings for the selected digital
					  transducer channel.

					  ssi_clkperiod		Period of SSI clock in ns

					  flags				Bit 0	Transducer type 0 = Encoder
																1 = SSI
										Bit 1	Override automatic datarate
												calculation and use supplied
												ssi_datarate value
										Bit 2	Encoding (SSI)	0 = Binary
																1 = Gray

********************************************************************************/

void hw_set_2dig_txdr_config(chandef* def, uint32_t flags, uint32_t ssi_datalen,
	uint32_t ssi_clkperiod, uint32_t* conf_datarate)
{
	fprintf(stdout, "%s: [%d]\n", __FUNCTION__, flags);

	//info_2dig* i;
	//hw_2dig* hw;
	//uint16_t* rega;
	//uint16_t* regb;
	//uint32_t ssi_datarate = 0;
	//float clkperiod = (float)ssi_clkperiod * 1e-9;
	//float fsrange;
	//char* units;

	//i = &def->hw.i_2dig;	/* 2DIG specific information 	*/
	//hw = i->hw;

	//if (flags & DIGTXDR_TYPE_SSI) {

	//	/* For SSI transducers, if the automatic datarate calculation is not overridden,
	//	   then calculate the required clock period to match the specified data length.

	//	   The value of 32us is to provide a delay between samples read from the
	//	   transducer. The recommended minimum is 16us, but we use double that to
	//	   be sure.
	//	*/

	//	if (!(flags & DIGTXDR_SETRATE)) {
	//		clkperiod = ((1 / FS) - 32e-6) / (ssi_datalen + 1);
	//	}

	//	/* Calculate the data rate parameter to configure the hardware */

	//	ssi_datarate = (uint32_t)((clkperiod * REFCLK) / 2.0);
	//}

	//if (ssi_datarate > 1023) ssi_datarate = 1023;
	//if (conf_datarate) *conf_datarate = ssi_datarate;

	//rega = (EXTSLOTCHAN == 0) ? &(hw->u.wr.ctrl1a) : &(hw->u.wr.ctrl2a);
	//regb = (EXTSLOTCHAN == 0) ? &(hw->u.wr.ctrl1b) : &(hw->u.wr.ctrl2b);

	//*rega = flags;
	//*regb = ((ssi_datalen & 0x003F) << 10) | (ssi_datarate & 0x03FF);

	///* Get the current full scale range for this channel and use that to set the bit scale
	//   factor for the transducer.
	//*/

	//if (getFullScaleUnits(EXTCHAN, &fsrange, &units)) def->setfullscale(def, fsrange);
}



#if 0

/********************************************************************************
  FUNCTION NAME     : hw_read_2dig_txdr_config
  FUNCTION DETAILS  : Read configuration settings for the selected digital
					  transducer channel.
********************************************************************************/

void hw_read_2dig_txdr_config(chandef* def, uint32_t* flags, uint32_t* ssi_datalen,
	uint32_t* ssi_datarate)
{
	info_2dig* i;
	uint16_t* reg;
	uint16_t config;

	i = &def->hw.i_2dig;	/* 2DIG specific information 	*/

	reg = (EXTSLOTCHAN == 0) ? &(i->hw->u.rd.ctrl1) : &(i->hw->u.rd.ctrl2);
	config = *reg;

	if (flags) *flags = (config & DIGTXDR_TYPEMASK) ? 0x0001 : 0x0000;
	if (ssi_datalen) *ssi_datalen = (config >> 9) & 0x001F;
	if (ssi_datarate) *ssi_datarate = (uint32_t)(((float)((config & 0x01FF) * 2) / REFCLK) * 1e9);
}

#endif



/*******************************************************
* Table of handler functions for digital txdr channels *
*******************************************************/

void* hw2dig_fntable[] = {
  &read_chan_status,				/* Fn  0. Ptr to readstatus function 				*/
  &chan_ad_set_pointer,				/* Fn  1. Ptr to setpointer function				*/
  &chan_ad_get_pointer,				/* Fn  2. Ptr to getpointer function				*/
  &save_chan_config,				/* Fn  3. Ptr to saveconfig function 				*/
  &restore_chan_config,				/* Fn  4. Ptr to restoreconfig function 			*/
  &flush_chan_config,				/* Fn  5. Ptr to flushconfig function 				*/
  &save_chan_calibration,			/* Fn  6. Ptr to savecalibration function 			*/
  &restore_chan_calibration,		/* Fn  7. Ptr to restorecalibration function 		*/
  &flush_chan_calibration,			/* Fn  8. Ptr to flushcalibration function 			*/
  NULL,								/* Fn  9. Ptr to setexc function 					*/
  NULL,								/* Fn 10. Ptr to readexc function 					*/
  NULL,								/* Fn 11. Ptr to setexccal function	 				*/
  NULL,								/* Fn 12. Ptr to readexccal function 				*/
  &chan_ad_set_gain,				/* Fn 13. Ptr to setgain function 					*/
  &chan_ad_read_gain,				/* Fn 14. Ptr to readgain function 					*/
  NULL,								/* Fn 15. Ptr to setbalance function 				*/
  NULL,								/* Fn 16. Ptr to readbalance function 				*/
  NULL,								/* Fn 17. Ptr to calctrl function 					*/
  &hw_2dig_led_ctrl,				/* Fn 18. Ptr to ledctrl function 					*/
  NULL,								/* Fn 19. Ptr to setrawexc function 				*/
  NULL,								/* Fn 20. Ptr to enableexc function 				*/
  NULL,								/* Fn 21. Ptr to setrawgain function 				*/
  NULL,								/* Fn 22. Ptr to readrawgain function 				*/
  &chan_read_adc,					/* Fn 23. Ptr to readadc function 					*/
  &set_filter,						/* Fn 24. Ptr to setfilter function	 				*/
  &read_filter,						/* Fn 25. Ptr to readfilter function 				*/
  NULL,								/* Fn 26. Ptr to writedac function 					*/
  NULL,								/* Fn 27. Ptr to setrawcurrent function 			*/
  &chan_ad_set_caltable,			/* Fn 28. Ptr to set_caltable function 				*/
  &chan_ad_read_caltable,			/* Fn 29. Ptr to read_caltable function 			*/
  &chan_ad_set_calgain,				/* Fn 30. Ptr to set_calgain function 				*/
  &chan_ad_read_calgain,			/* Fn 31. Ptr to read_calgain function 				*/
  &chan_ad_set_caloffset,			/* Fn 32. Ptr to set_caloffset function 			*/
  &chan_ad_read_caloffset,			/* Fn 33. Ptr to read_caloffset function 			*/
  &chan_set_cnet_output,			/* Fn 34. Ptr to set_cnet_slot function 			*/
  &chan_ad_set_gaintrim,			/* Fn 35. Ptr to set_gaintrim function 				*/
  &chan_ad_read_gaintrim,			/* Fn 36. Ptr to read_gaintrim function 			*/
  &set_ip_offsettrim,				/* Fn 37. Ptr to set_offsettrim function 			*/
  &read_ip_offsettrim,				/* Fn 38. Ptr to read_offsettrim function 			*/
  &chan_dt_set_txdrzero,			/* Fn 39. Ptr to set_txdrzero function				*/
  &chan_dt_read_txdrzero,			/* Fn 40. Ptr to read_txdrzero function				*/
  &read_peakinfo,					/* Fn 41. Ptr to readpeakinfo function 				*/
  &reset_peaks,						/* Fn 42. Ptr to resetpeak function 				*/
  &chan_dt_set_flags,				/* Fn 43. Ptr to set_flags function 				*/
  &chan_ad_read_flags,				/* Fn 44. Ptr to read_flags function 				*/
  &chan_ad_write_map,				/* Fn 45. Ptr to write_map function 				*/
  &chan_ad_read_map,				/* Fn 46. Ptr to read_map function 					*/
  &chan_dt_set_refzero,				/* Fn 47. Ptr to set_refzero function 				*/
  &read_refzero,					/* Fn 48. Ptr to read_refzero function 				*/
  &undo_refzero,					/* Fn 49. Ptr to undo_refzero function 				*/
  NULL,								/* Fn 50. Ptr to set_current function				*/
  NULL,								/* Fn 51. Ptr to read_current function				*/
  &hw_2dig_tedsid,					/* Fn 52. Ptr to tedsid function 					*/
  NULL,								/* Fn 53. Ptr to set_source function				*/
  NULL,								/* Fn 54. Ptr to set dither function				*/
  NULL,								/* Fn 55. Ptr to read dither function				*/
  NULL,								/* Fn 56. Ptr to set balance function				*/
  NULL,								/* Fn 57. Ptr to read balance function				*/
  NULL,								/* Fn 58. Ptr to set info function					*/
  NULL,								/* Fn 59. Ptr to read info function					*/
  NULL,								/* Fn 60. Ptr to write output function				*/
  NULL,								/* Fn 61. Ptr to read input function				*/
  NULL,								/* Fn 62. Ptr to read output function				*/
  &chan_ad_write_userstring,		/* Fn 63. Ptr to write user string function			*/
  &chan_ad_read_userstring,			/* Fn 64. Ptr to read user string function			*/
  NULL,								/* Fn 65. Ptr to setgaugetype function 				*/
  NULL,								/* Fn 66. Ptr to readgaugetype function 			*/
  NULL,								/* Fn 67. Ptr to setoffset function 				*/
  NULL,								/* Fn 68. Ptr to readoffset function 				*/
  NULL,								/* Fn 69. Ptr to startzero function 				*/
  NULL,								/* Fn 70. Ptr to setgaugeinfo function 				*/
  NULL,								/* Fn 71. Ptr to readgaugeinfo function 			*/
  &chan_dt_txdrzero_zero,			/* Fn 72. Ptr to txdrzero_zero function				*/
  &chan_read_cyclecount,			/* Fn 73. Ptr to readcycle function					*/
  &reset_cycles,					/* Fn 74. Ptr to resetcycles function 				*/
  &chan_ad_set_peak_threshold,		/* Fn 75. Ptr to setpkthreshold function 			*/
  &chan_ad_read_peak_threshold,		/* Fn 76. Ptr to readpkthreshold function 			*/
  &chan_dt_write_faultmask,			/* Fn 77. Ptr to writefaultmask function 			*/
  &chan_dt_read_faultstate,			/* Fn 78. Ptr to readfaultstate function 			*/
  &chan_dt_write_config,			/* Fn 79. Ptr to setdigtxdrconfig function 			*/
  &chan_dt_read_config,				/* Fn 80. Ptr to readdigtxdrconfig function 		*/
  &chan_dt_write_fullscale,			/* Fn 81. Ptr to setfullscale function				*/
  &chan_dt_refzero_zero,			/* Fn 82. Ptr to refzero_zero function				*/
  NULL,								/* Fn 83. Ptr to do_zero function					*/
  NULL,								/* Fn 84. Ptr to sethpfilter function				*/
  NULL,								/* Fn 85. Ptr to readhpfilter function				*/
  NULL,								/* Fn 86. Ptr to setcalzeroentry function			*/
  NULL,								/* Fn 87. Ptr to readcalzeroentry function			*/
};



/********************************************************************************
  FUNCTION NAME     : hw_2dig_install
  FUNCTION DETAILS  : Install 2DIG hardware on the selected channel(s).

					  On entry:

					  slot			Slot number being installed.

					  numinchan		Points to the variable holding the number of
									input channels installed for this slot.

					  numoutchan	Points to the variable holding the number of
									output channels installed for this slot.

					  nummiscchan	Points to the variable holding the number of
									miscellaneous channels installed for this slot.

					  flags			Controls if channel allocation is performed.
									If not, then only initialisation operations
									are performed.
********************************************************************************/

uint32_t hw_2dig_install(int slot, int* numinchan, int* numoutchan, int* nummiscchan,
	hw_2dig* hw, int flags)
{
	info_2dig* i;
	uint32_t eecal_addr = CHAN_EE_CALSTART;		/* Start of channel EEPROM calibration area	  */
	uint32_t eecfg_addr = CHAN_EEBANK_DATASTART;	/* Start of channel EEPROM configuration area */
	uint32_t eerdcfg_addr = CHAN_EEBANK_DATASTART;	/* Address for reading EEPROM configuration	  */
	int input;
	uint8_t* workspace;
	uint32_t status = 0;
	chandef* def;

	/***************************************************************************
	* Perform initial allocation of channels and setting of default parameters *
	***************************************************************************/

	if (flags & DO_ALLOCATE) {

		/* The workspace is used by all inputs on the mainboard, so determine the
		   address of the workspace area of the first input to be allocated and use
		   that for all input workspaces.
		*/

		workspace = inchaninfo[totalinchan].hw.i_2dig.work;
		memset(workspace, 0, WSIZE_2DIG);

	}

	/* Allocate the two transducer inputs */

	for (input = 0; input < 2; input++) {

		def = &inchaninfo[slot_status[slot].baseinchan + input];

		/* Set defaults for digital txdr input channel */

		chan_dt_default(def, (totalinchan + input));

		if (flags & DO_ALLOCATE) {

			i = &def->hw.i_2dig;

			/* Define local hardware/workspace settings */

			i->hw = hw;						/* Pointer to hardware base				*/
			i->input = input;					/* Input channel number on mainboard    */
			i->ledmask = 1 << input;				/* Mask for channel ident LED 		    */
			i->ledstate = &workspace[1];			/* Pointer to local workspace		    */

			init_fntable(def, hw2dig_fntable);		/* Set pointers to control functions 	*/

			SETDEF(slot, TYPE_DIGTXDR, input, (CHANTYPE_INPUT | (totalinchan + input)));

			/* Allocate space in EEPROM/BBSRAM for configuration and calibration data */

			ALLOCATE_EEPROM(def->eecfg_start, &eecfg_addr, def->eesavelist);
			def->eecal_start = eecal_addr; eecal_addr += SAVED_CHANCAL_SIZE; def->eecal_size = SAVED_CHANCAL_SIZE;

			/* Allocate space in DSP B memory for linearisation map */

			if (!(def->mapptr = (float*)alloc_dspb_general(sizeof(def->map)))) diag_error(DIAG_MALLOC);

			/* Define the source and destination addresses for the channel and
			   remove any reset input from the quadrature counter.
			*/

			switch (input) {
			case 0:
				def->sourceptr = &(hw->u.rd.txdr1);
				hw->u.wr.reset1 = 0x0000;
				break;
			case 1:
				def->sourceptr = &(hw->u.rd.txdr2);
				hw->u.wr.reset2 = 0x0000;
				break;
			}
			def->outputptr = cnet_get_slotaddr_out(totalinchan + input, PROC_DSPB);

			/* Initialise the TEDS interface */

			hw_2dig_1w_init(i, input);

		}

		/* Restore current calibration and configuration data */

		def->status = restore_chan_calibration(def);
		if (!(flags & DO_FLUSH)) {
			def->status |= restore_chan_eeconfig(def, &eerdcfg_addr, def->eesavetable, def->eeformat, DO_RESTORE);
		}
		status |= def->status;

	}

	/***************************************************************************
	* Perform hardware configuration                                           *
	***************************************************************************/

	/* Configure the two digital txdr inputs */

	for (input = 0; input < 2; input++) {

		chandef* def;
		cal_digtxdr* cal;
		cfg_digtxdr* cfg;
		int chan = slot_status[slot].baseinchan + input;
		float fsrange;
		char* units;

		def = &inchaninfo[chan];
		cal = &def->cal.u.c_digtxdr;
		cfg = &def->cfg.i_digtxdr;

		/* Trap for illegal negative gaintrim value caused by upgrade from version
		   prior to v1.0.0099. Replace by value of 1.0.
		*/

		if (cfg->neg_gaintrim < 0.5) cfg->neg_gaintrim = 1.0;

		/* Restore the user configuration settings */
		
		/* Update channel flags. Force simulation mode off and disable cyclic event generation. */

		def->set_flags(def, (cfg->flags & ~(FLAG_SIMULATION | FLAG_CYCEVENT)), 0xFFFFFFFF, NO_UPDATECLAMP, NO_MIRROR);


		hw_set_2dig_txdr_config(def, cfg->hwflags, cfg->ssi_datalen, cfg->ssi_clkperiod, NULL);

		def->setgain(def, cfg->gain,
			SETGAIN_RETAIN_GAINTRIM | chan_extract_gainflags(cfg->flags),
			NO_MIRROR);

		/* Get the current full scale range for this channel and use that to set the bit scale
		   factor for the transducer.
		*/

		if (getFullScaleUnits(chan, &fsrange, &units)) def->setfullscale(def, fsrange);

		def->offset = cal->cal_offset;
		def->set_txdrzero(def, cfg->txdrzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);
		def->set_refzero(def, def->refzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);

		/* Set the linearisation map */

		def->write_map(def, (cfg->flags & FLAG_LINEARISE) >> BIT_LINEARISE, def->map, def->filename, FALSE, NO_MIRROR);

		/* Set filter 1 characteristic */

		set_filter(def, FILTER_ALL, 0, def->filter1.type & FILTER_ENABLE, def->filter1.type & FILTER_TYPEMASK, def->filter1.order, def->filter1.freq, def->filter1.bandwidth, NO_MIRROR);

		/* Update the shared channel definition */

		update_shared_channel(def);

	}

	/* Update channel counts */

	if (flags & DO_ALLOCATE) {
		totalinchan += 2;
		physinchan += 2;
		*numinchan = 2;
		*numoutchan = 0;
		*nummiscchan = 0;
	}

	return(status & (FLAG_BADLIST | FLAG_OLDLIST));
}



/********************************************************************************
  FUNCTION NAME     : hw_post_2dig_install
  FUNCTION DETAILS  : Post initialisation installation of 2DIG hardware.

					  On entry:

					  slot			Slot number being installed.

					  flags			Controls initialisation operation.
********************************************************************************/

uint32_t hw_2dig_post_install(int slot, hw_2dig* hw, int flags)
{
	int input;

	/* Configure the two digital txdr inputs */

	for (input = 0; input < 2; input++) {

		chandef* def;
		cal_digtxdr* cal;
		cfg_digtxdr* cfg;
		int chan = slot_status[slot].baseinchan + input;
		float fsrange;
		char* units;

		def = &inchaninfo[chan];
		cal = &def->cal.u.c_digtxdr;
		cfg = &def->cfg.i_digtxdr;

		/* Get the current full scale range for this channel and use that to set the bit scale
		   factor for the transducer.
		*/

		if (getFullScaleUnits(chan, &fsrange, &units)) def->setfullscale(def, fsrange);

		def->offset = cal->cal_offset;
		def->set_txdrzero(def, cfg->txdrzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);
		def->set_refzero(def, def->refzero, FALSE, NO_UPDATECLAMP, NO_MIRROR);

		/* Update the shared channel definition */

		update_shared_channel(def);
	}

	return(0);
}



/********************************************************************************
  FUNCTION NAME     : hw_2dig_save
  FUNCTION DETAILS  : Save the 2DIG channel settings to EEPROM or
					  battery-backed RAM if required.

					  If a bad list or an old list format was detected on any
					  channels, then all of the channels must be updated.

					  On entry:

					  slot		Slot number being saved.

					  status	System configuration status. This indicates if
								a full save operation is required because an
								old or bad format list was detected during
								initialisation.

********************************************************************************/

void hw_2dig_save(int slot, uint32_t status)
{
	int input;
	chandef* def;

	/* Save the input channels */

	for (input = 0; input < 2; input++) {
		def = &inchaninfo[slot_status[slot].baseinchan + input];
		if ((status & (FLAG_BADLIST | FLAG_OLDLIST)) || (def->status & FLAG_BADCFG)) {
			save_chan_eeconfig(def, def->eesavetable, def->eeformat);
		}
	}

}
