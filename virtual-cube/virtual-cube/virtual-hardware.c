// virtual-hardware.c

#include "virtual-hardware.h"
#include "hardware.h"
#include "virtual-cube-config.h"

#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>

#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]))

enum eeproms {
	EEPROM_A0 = 0,
	EEPROM_ONEWIRE,
	EEPROM_AE,
	EEPROM_SLOT1,
	EEPROM_SLOT2,
	EEPROM_SLOT3,
	MAX_EEPROMS,
};

static struct virtual_eeprom* eeproms[MAX_EEPROMS];

void* shared_memory;
static void setup_shared_memory(void)
{
	int fd = open("/dev/zero", O_RDWR);
	if (fd < 0)
		diag_error(errno);

	/* allocate 1Mib of memory for sharing */
	shared_memory = mmap(NULL, 0x100000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (shared_memory < 0)
		diag_error(errno);
	close(fd);
}

static void setup_eeproms(void)
{
	memset(eeproms, 0, ARRAY_SIZE(eeproms));
	eeproms[EEPROM_ONEWIRE] = virtual_eeprom_open("eeprom_onewire.dat", 1024, 1);
	eeproms[EEPROM_A0] = virtual_eeprom_open("eeprom_a0.dat", 65536, 0xa0);
	eeproms[EEPROM_AE] = virtual_eeprom_open("eeprom_ae.dat", 65536, 0xae);
	eeproms[EEPROM_SLOT1] = virtual_eeprom_open("eeprom_slot1.dat", 65536, 0xa2);
	eeproms[EEPROM_SLOT2] = virtual_eeprom_open("eeprom_slot2.dat", 65536, 0xa4);
	eeproms[EEPROM_SLOT3] = virtual_eeprom_open("eeprom_slot3.dat", 65536, 0xa6);
}

static int get_slot_num_from_eeprom(enum eeproms eeprom)
{
	int slot = 0;
	switch (eeprom)
	{
	case EEPROM_SLOT1:
		slot = 1;
		break;
	case EEPROM_SLOT2:
		slot = 2;
		break;
	case EEPROM_SLOT3:
		slot = 3;
		break;
	default:
		break;
	}
	return slot;
}

static void fix_slot_eeprom(enum eeproms eeprom)
{
	int slot = get_slot_num_from_eeprom(eeprom);

	if (eeprom >= MAX_EEPROMS)
		return;

	if (slot > 0)
	{
		uint8_t id[] = { config_get_slot_type(slot), 0xaa, 0x55, 0x00, 0xff };
		virtual_eeprom_write_block(eeproms[eeprom], 0, (uint8_t*)id, 5, 0);
	}
}

void vh_initialise(void)
{
	setup_eeproms();
	fix_slot_eeprom(EEPROM_SLOT1);
	fix_slot_eeprom(EEPROM_SLOT2);
	fix_slot_eeprom(EEPROM_SLOT3);
	setup_shared_memory();
}

struct virtual_eeprom* vh_get_onewire_eeprom(void)
{
	return eeproms[EEPROM_ONEWIRE];
}

struct virtual_eeprom* vh_get_eeprom_by_address(uint8_t address)
{
	struct virtual_eeprom* eeprom = NULL;
	int i;
	for (i = 0; i < MAX_EEPROMS; i++)
	{
		if (eeproms[i] != NULL)
		{
			if (eeproms[i]->addr == address)
			{
				eeprom = eeproms[i];
				break;
			}
		}
	}
	return eeprom;
}
