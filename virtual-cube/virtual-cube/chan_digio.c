/********************************************************************************
 * MODULE NAME       : chan_digio.c												*
 * MODULE DETAILS    : Routines to support a digital I/O channel.				*
 ********************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

 //#include <std.h>
 //#include <log.h>

#include "defines.h"
//#include "ccubecfg.h"
#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"



/******************************************************************
* List of entries in chandef structure that are saved into EEPROM *
*																  *
* Entries 0 to 3 must be compatible across all channel types.	  *
*																  *
******************************************************************/

static configsave chan_io_eelist_type0[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 4	*/
  {0,0}
};

/* Type 1 list added user string area. */

static configsave chan_io_eelist_type1[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 4	*/
  {offsetof(chandef, userstring), 128},							/* Entry 5 */
  {0,0}
};

#define CURRENT_CHAN_IO_EELIST_TYPE 1

static configsave* chan_io_eelist[] = {
  chan_io_eelist_type0,
  chan_io_eelist_type1,
};



/********************************************************************************
  FUNCTION NAME     : chan_io_initlist
  FUNCTION DETAILS  : Initialise configsave list for digital I/O channel.
********************************************************************************/

void chan_io_initlist(void)
{
	init_savelist(chan_io_eelist[CURRENT_CHAN_IO_EELIST_TYPE]);
}



/********************************************************************************
  FUNCTION NAME     : chan_io_default
  FUNCTION DETAILS  : Load default values for a digital I/O channel.
********************************************************************************/

void chan_io_default(chandef* def, uint32_t chanid, uint32_t flags)
{
	cfg_io* cfg;
	char name[32];
	char state0[32];
	char state1[32];

	cfg = &def->cfg.i_io;

	/* Define save information */

	def->eeformat = CURRENT_CHAN_IO_EELIST_TYPE;
	def->eesavelist = chan_io_eelist[CURRENT_CHAN_IO_EELIST_TYPE];
	def->eesavetable = chan_io_eelist;

	sprintf(name, "Digital I/O %d", chanid);
	sprintf(state0, "Off");
	sprintf(state1, "On");

	memcpy(cfg->name, name, sizeof(cfg->name));
	memcpy(cfg->state0, state0, sizeof(cfg->name));
	memcpy(cfg->state1, state1, sizeof(cfg->name));
	cfg->flags = flags;
}



/********************************************************************************
  FUNCTION NAME     : chan_io_read_info
  FUNCTION DETAILS  : Read the information for the selected digital I/O channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_io_read_info(chandef* def, uint32_t* flags, char* name, char* state0, char* state1)
{
	if (flags) *flags = def->cfg.i_io.flags;
	if (name) memcpy(name, def->cfg.i_io.name, 32);
	if (state0) memcpy(state0, def->cfg.i_io.state0, 32);
	if (state1) memcpy(state1, def->cfg.i_io.state1, 32);
}



/********************************************************************************
  FUNCTION NAME     : chan_io_set_io_info
  FUNCTION DETAILS  : Write the definition for the selected digital I/O channel.

					  On entry:

					  def		Points to the channel definition structure
					  name		Points to the name string

********************************************************************************/

void chan_io_set_info(chandef* def, uint32_t flags, char* name, char* state0, char* state1, int mirror)
{
	uint32_t mask = 1 << EXTSLOTCHAN;
	def->cfg.i_io.flags = (def->cfg.i_io.flags & ~DIGIO_INVERT) | (flags & DIGIO_INVERT);
	slot_status[EXTSLOTNUM].digip_invert = (slot_status[EXTSLOTNUM].digip_invert & ~mask) | ((flags & DIGIO_INVERT) ? mask : 0);
	if (name) memcpy(def->cfg.i_io.name, name, 32);
	if (state0) memcpy(def->cfg.i_io.state0, state0, 32);
	if (state1) memcpy(def->cfg.i_io.state1, state1, 32);

	/* Mirror into EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 4);	/* Mirror cfg area		*/
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_io_write_output
  FUNCTION DETAILS  : Write the selected digital I/O channel output.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_io_write_output(chandef* def, uint32_t output)
{
	uint32_t op = (output & DIGIO_INVERT) ^ (def->cfg.i_io.flags & DIGIO_INVERT);
	def->hw.i_digio.hw->u.wr.output = (def->hw.i_digio.hw->u.rd.output & ~def->cfg.i_io.mask) |
		((op) ? def->cfg.i_io.mask : 0);
}



/********************************************************************************
  FUNCTION NAME     : chan_io_read_input
  FUNCTION DETAILS  : Read the selected digital I/O channel input.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_io_read_input(chandef* def, uint32_t* input)
{
	if (input) *input = ((def->hw.i_digio.hw->u.rd.input & def->cfg.i_io.mask) ? 1 : 0) ^
		(def->cfg.i_io.flags & DIGIO_INVERT);
}



/********************************************************************************
  FUNCTION NAME     : chan_io_read_output
  FUNCTION DETAILS  : Read the selected digital I/O channel output.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_io_read_output(chandef* def, uint32_t* output)
{
	if (output) *output = (def->hw.i_digio.hw->u.rd.output & def->cfg.i_io.mask) ? 1 : 0;
}



/********************************************************************************
  FUNCTION NAME     : chan_io_write_userstring
  FUNCTION DETAILS  : Write user string for the selected digital I/O channel.

					  On entry:

					  string	Points to the start of the user string.
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_io_write_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string, int mirror)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(&def->userstring[start], string, length);
	if (mirror) {
		if (string) mirror_chan_eeconfig(def, 5);	/* Mirror user string */
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_io_read_userstring
  FUNCTION DETAILS  : Read user string for the selected digital I/O channel.
********************************************************************************/

void chan_io_read_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(string, &def->userstring[start], length);
}



/********************************************************************************
  FUNCTION NAME     : chan_io_write_faultmask
  FUNCTION DETAILS  : Write fault mask value for the selected digital I/O
					  channel.

					  On entry:

					  mask		Mask flags.
									Bit 0	Reserved
									Bit 1	Reserved
									Bit 2	Drive current fault enable
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_io_write_faultmask(chandef* def, uint32_t mask, int mirror)
{
}



/********************************************************************************
  FUNCTION NAME     : chan_io_read_faultstate
  FUNCTION DETAILS  : Read fault state for the selected digital I/O channel.
********************************************************************************/

uint32_t chan_io_read_faultstate(chandef* def, uint32_t* capabilities, uint32_t* mask)
{
	if (capabilities) *capabilities = 0;
	if (mask) *mask = 0;
	return(0);
}
