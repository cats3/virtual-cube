/********************************************************************************
 * MODULE NAME       : chan_da.c												*
 * MODULE DETAILS    : Routines to support a high-level DAC channel.			*
 ********************************************************************************/

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdint.h>

#include <math.h>

 //#include <std.h>
 //#include <log.h>

#include "defines.h"
//#include "ccubecfg.h"
#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"
#include "asmcode.h"


/******************************************************************
* List of entries in chandef structure that are saved into EEPROM *
*																  *
* Entries 0 to 3 must be compatible across all channel types.	  *
*																  *
******************************************************************/

static configsave chan_da_eelist_type0[] = {
  {offsetof(chandef, offset), sizeof(int)},			/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},	/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},		/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},		/* Entry 3	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},	/* Entry 4	*/
  {0,0}
};

/* Type 1 list added user string area. */

static configsave chan_da_eelist_type1[] = {
  {offsetof(chandef, offset), sizeof(int)},			/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},	/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},		/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},		/* Entry 3	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},	/* Entry 4	*/
  {offsetof(chandef, userstring), 128},				/* Entry 5 */
  {0,0}
};

#define CURRENT_CHAN_DA_EELIST_TYPE 1

static configsave* chan_da_eelist[] = {
  chan_da_eelist_type0,
  chan_da_eelist_type1,
};



/********************************************************************************
  FUNCTION NAME     : chan_da_initlist
  FUNCTION DETAILS  : Initialise configsave list for high-level DAC output
					  channel.
********************************************************************************/

void chan_da_initlist(void)
{
	init_savelist(chan_da_eelist[CURRENT_CHAN_DA_EELIST_TYPE]);
}



/********************************************************************************
  FUNCTION NAME     : chan_da_default
  FUNCTION DETAILS  : Load default values for the selected high-level DAC
					  output channel.
********************************************************************************/

void chan_da_default(chandef* def, uint32_t chanid)
{
	cfg_da* cfg;
	cal_da* cal;

	cfg = &def->cfg.i_da;
	cal = &def->cal.u.c_da;

	/* Define save information */

	def->eeformat = CURRENT_CHAN_DA_EELIST_TYPE;
	def->eesavelist = chan_da_eelist[CURRENT_CHAN_DA_EELIST_TYPE];
	def->eesavetable = chan_da_eelist;

	cal->cal_offset = 0;
	cal->cal_gain = 1.0;

	cfg->flags = 0;
	cfg->gaintrim = 1.0;	/* Default gaintrim 			*/
	cfg->gain = 1.0;		/* Default gain 				*/

	/* Set reserved locations to zoero */

	cfg->reserved1 = 0;
	cfg->reserved2 = 0;
	cfg->reserved3 = 0;
	cfg->reserved4 = 0;

	def->pos_gaintrim = GAINTRIM_SCALE;
	def->neg_gaintrim = GAINTRIM_SCALE;

	def->filter1.type = 0;
	def->filter1.order = 0;
	def->filter1.freq = 0.0;
	def->filter1.bandwidth = 0.0;
}



/********************************************************************************
  FUNCTION NAME     : chan_da_set_pointer
  FUNCTION DETAILS  : Set the source pointer for the selected high-level DAC
					  output channel.

					  On entry:

					  def		Points to the channel definition structure
					  ptr		Address to set source pointer

********************************************************************************/

void chan_da_set_pointer(chandef* def, int* ptr)
{
	def->sourceptr = ptr;

	update_shared_channel_parameter(def, offsetof(chandef, sourceptr), sizeof(float*));
}



/********************************************************************************
  FUNCTION NAME     : chan_da_get_pointer
  FUNCTION DETAILS  : Get the source pointer for the selected high-level DAC
					  output channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

int* chan_da_get_pointer(chandef* def)
{
	return(def->sourceptr);
}



/********************************************************************************
  FUNCTION NAME     : chan_da_set_caltable
  FUNCTION DETAILS  : Sets a calibration table entry for the selected high-level
					  DAC output channel.
********************************************************************************/

void chan_da_set_caltable(chandef* def, int entry, float value)
{
}



/********************************************************************************
  FUNCTION NAME     : chan_ad_read_caltable
  FUNCTION DETAILS  : Reads a calibration table entry for the selected
					  high-level DAC output channel.
********************************************************************************/

float chan_da_read_caltable(chandef* def, int entry)
{
	return(0.0);
}



/********************************************************************************
  FUNCTION NAME     : chan_da_set_gain
  FUNCTION DETAILS  : Set the gain for the selected high-level DAC output
					  channel.

					  On entry:

					  def		Points to the channel definition structure
					  gain		Required gain value
					  flags		Bit 0 : Reserved
								Bit 1 : Retain gain trim value
										0 = Reset gain trim value to 1.0
										1 = Retain existing gain trim value
								Bit 2 : Reserved

********************************************************************************/

void chan_da_set_gain(chandef* def, float gain, uint32_t flags, int mirror)
{
#if 0

	cal_da* cal;
	cfg_da* cfg;
	float gaintrim;

	cal = &def->cal.u.c_da;	/* DA specific calibration		*/
	cfg = &def->cfg.i_da;	/* DA specific configuration	*/

	/* Save new gain setting and reset gain trim */

	cfg->gain = gain;
	if (!(flags & SETGAIN_RETAIN_GAINTRIM)) cfg->gaintrim = 1.0;

	/* Calculate the required gain trim */

	gaintrim = gain / cal->cal_gain;
	if (gaintrim > 3.999) gaintrim = 3.999;

	/* Update fine software gain trim. */

	def->gaintrim = (uint32_t)(cfg->gaintrim * gaintrim * (float)GAINTRIM_SCALE);

	/* Mirror to EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 1);		/* Mirror pos gaintrim value	*/
		mirror_chan_eeconfig(def, 4);		/* Mirror cfg area				*/
	}

	update_shared_channel_parameter(def, offsetof(chandef, gaintrim), sizeof(int));

#endif
}



/********************************************************************************
  FUNCTION NAME     : chan_da_read_gain
  FUNCTION DETAILS  : Read the gain for the selected high-level DAC output
					  channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_da_read_gain(chandef* def, float* gain, uint32_t* flags)
{
#if 0

	cfg_da* cfg;

	cfg = &def->cfg.i_da;	/* DA specific configuration	*/

	if (gain) *gain = cfg->gain;

#endif

	if (gain) *gain = 1.0F;
	if (flags) *flags = 0;
}



/********************************************************************************
  FUNCTION NAME     : chan_da_set_gaintrim
  FUNCTION DETAILS  : Set the gaintrim value for the selected high-level DAC
					  output channel.

					  On entry:

					  def			Points to the channel definition structure
					  pos_gaintrim	Pointer to required positive gaintrim value
					  neg_gaintrim	Pointer to required negative gaintrim value
					  calmode		Flag set when calibration mode operating.
									Disables calculation of overall trim and
									forces use of supplied value.

********************************************************************************/

void chan_da_set_gaintrim(chandef* def, float* pos_gaintrim, float* neg_gaintrim, int calmode, int mirror)
{
	cal_da* cal;
	cfg_da* cfg;
	float trim;
	float ptrim;
	//float ntrim;

	cal = &def->cal.u.c_da;	/* DA specific calibration		*/
	cfg = &def->cfg.i_da;	/* DA specific configuration	*/

	if (pos_gaintrim) {
		ptrim = *pos_gaintrim;
		if (ptrim > 3.999) ptrim = 3.999;
		cfg->gaintrim = ptrim;
	}

	if (!calmode) {

		/* Re-calculate current gain value to get current hardware gain trim value */

		trim = 1.0F / cal->cal_gain;

		/* Combine hardware trim with user trim to produce an overall trim value */

		ptrim = trim * ptrim;
	}

	if (ptrim > 3.999) ptrim = 3.999;

	/* Update fine software gain trim. */

	if (pos_gaintrim) {
		def->pos_gaintrim = (uint32_t)(ptrim * (float)GAINTRIM_SCALE);
		update_shared_channel_parameter(def, offsetof(chandef, pos_gaintrim), sizeof(int));
		if (mirror) mirror_chan_eeconfig(def, 1);		/* Mirror pos gaintrim value	*/
	}

	/* Mirror config to EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 4);		/* Mirror cfg area				*/
	}

}



/********************************************************************************
  FUNCTION NAME     : chan_da_read_gaintrim
  FUNCTION DETAILS  : Read the gaintrim value for the selected high-level DAC
					  output channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_da_read_gaintrim(chandef* def, float* pos_gaintrim, float* neg_gaintrim)
{
	cfg_da* cfg;

	cfg = &def->cfg.i_da;	/* DA specific configuration	*/

	if (pos_gaintrim) *pos_gaintrim = cfg->gaintrim;
	if (neg_gaintrim) *neg_gaintrim = cfg->gaintrim;
}



/********************************************************************************
  FUNCTION NAME     : chan_da_set_calgain
  FUNCTION DETAILS  : Sets the calibration gain for the selected high-level DAC
					  output channel.
********************************************************************************/

void chan_da_set_calgain(chandef* def, float gain)
{
	cal_da* c;

	c = &def->cal.u.c_da;	/* DA specific calibration	*/
	c->cal_gain = gain;
}



/********************************************************************************
  FUNCTION NAME     : chan_da_read_calgain
  FUNCTION DETAILS  : Reads the calibration gain for the selected high-level DAC
					  output channel.
********************************************************************************/

float chan_da_read_calgain(chandef* def)
{
	cal_da* c;

	c = &def->cal.u.c_da;	/* DA specific calibration	*/
	return(c->cal_gain);
}



/********************************************************************************
  FUNCTION NAME     : chan_da_set_caloffset
  FUNCTION DETAILS  : Sets the calibration offset for the selected high-level
					  DAC output channel.
********************************************************************************/

void chan_da_set_caloffset(chandef* def, float offset)
{
	cal_da* cal;

	cal = &def->cal.u.c_da;	/* DA specific calibration	*/

	cal->cal_offset = (int)((offset * (float)0x08000000UL) / 1.1F);
	def->offset = cal->cal_offset;
	update_shared_channel_parameter(def, offsetof(chandef, offset), sizeof(int));
}



/********************************************************************************
  FUNCTION NAME     : chan_da_read_caloffset
  FUNCTION DETAILS  : Reads the calibration offset for a high-level DAC channel.
********************************************************************************/

float chan_da_read_caloffset(chandef* def)
{
	cal_da* cal;

	cal = &def->cal.u.c_da;	/* DA specific calibration	*/

	return((((float)cal->cal_offset) * 1.1F) / (float)0x08000000UL);
}



/********************************************************************************
  FUNCTION NAME     : chan_da_set_flags
  FUNCTION DETAILS  : Modify the control flags for the selected high-level DAC
					  channel.
********************************************************************************/

void chan_da_set_flags(chandef* def, int set, int clr, int updateclamp, int mirror)
{
	uint32_t istate;
	cfg_da* cfg;
	uint32_t oldstate;
	uint32_t newstate;

	cfg = &def->cfg.i_da;		/* DA specific configuration	*/


	/* Determine the new flag state by setting and clearing bits as specified by
		set and clr parameters. Note that the simulation and disable flags in the
		chandef structure may have been modified by KO code and this will not be
		reflected in the cfg structure. Therefore, we must read their states directly
		from the chandef structure.

		Similarly, the virtual channel flag is stored in the chandef structure, but is
		not present in the config structure. Therefore, it must also be read directly
		from the chandef structure.
	*/

	oldstate = (cfg->flags & ~(FLAG_SIMULATION | FLAG_VIRTUAL | FLAG_DISABLE)) | (def->ctrl & (FLAG_SIMULATION | FLAG_VIRTUAL | FLAG_DISABLE));
	newstate = (oldstate & ~clr) | set;

	/* Update configuration data and channel control flags. Need to protect against interrupts
	   between updating DSP A memory and copying to DSP B memory.
	*/

	cfg->flags = newstate;

	istate = disable_int(); /* Disable interrupts whilst updating */

	def->ctrl = newstate;

	/* Update transducer flags setting in shared channel definition */

	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(int));

	restore_int(istate);

	/* Mirror into EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 4);	/* Mirror cfg area		*/
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_da_read_flags
  FUNCTION DETAILS  : Read the control flags for the selected high-level DAC
					  channel.
********************************************************************************/

int chan_da_read_flags(chandef* def)
{
	cfg_da* cfg;

	cfg = &def->cfg.i_da;		/* DA specific configuration	*/

	return(cfg->flags);
}



/********************************************************************************
  FUNCTION NAME     : chan_da_write_userstring
  FUNCTION DETAILS  : Write user string for the selected high-level DAC channel.

					  On entry:

					  string	Points to the start of the user string.
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_da_write_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string, int mirror)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(&def->userstring[start], string, length);
	if (mirror) {
		if (string) mirror_chan_eeconfig(def, 5);	/* Mirror user string */
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_da_read_userstring
  FUNCTION DETAILS  : Read user string for the selected high-level DAC channel.
********************************************************************************/

void chan_da_read_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(string, &def->userstring[start], length);
}



/********************************************************************************
  FUNCTION NAME     : chan_da_write_faultmask
  FUNCTION DETAILS  : Write fault mask value for the selected high-level DAC
					  channel.

					  On entry:

					  mask		Mask flags.
									Bit 0	Reserved
									Bit 1	Reserved
									Bit 2	Drive current fault enable
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_da_write_faultmask(chandef* def, uint32_t mask, int mirror)
{
}



/********************************************************************************
  FUNCTION NAME     : chan_da_read_faultstate
  FUNCTION DETAILS  : Read fault state for the selected high-level DAC channel.
********************************************************************************/

uint32_t chan_da_read_faultstate(chandef* def, uint32_t* capabilities, uint32_t* mask)
{
	if (capabilities) *capabilities = 0;
	if (mask) *mask = 0;
	return(0);
}
