/********************************************************************************
 * MODULE NAME       : chan_gp.c												*
 * MODULE DETAILS    : Routines to support a general-purpose transducer 		*
 *					   channel.													*
 ********************************************************************************/

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

#include <stdint.h>
#include "porting.h"

#include "defines.h"
//#include "ccubecfg.h"
#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"
#include "asmcode.h"


 /* Define fixed resistor values in transducer fine gain trim circuit */

#define VALUE_R1	10000.0
#define VALUE_RB1	20000.0
#define VALUE_RA	1210.0
#define VALUE_R3	90900.0

/* Define full-scale excitation DAC output */

#define	EXCDAC_FS	0x1FFF

/* Define AC transducer gain scaling. This multiplies the requested gain by a factor of
   1.1102 to compensate for the ratio between the average value of a sine wave signal
   (as measured by the ADC circuit) and the RMS value, which is the required value.
*/

#define AC_GAIN_SCALE	1.1102

/* Define AC transducer excitation scale factor. This is required to compensate
   for rolloff in the output amplifier at 5kHz. Factor is used when channel has been
   calibrated using v0.61 of the calibration software where caltable entry 0x2FE is used
   as a flag to enable this extra scaling.
*/

#define AC_EXC_SCALE	1.0091

/* Raw fullscale value.

   For Control Cube systems each raw sample is made up from the sum of
   16 x 16-bit samples. The sum is left shifted to align with the MSB,
   therefore the fullscale value is 0x80000000.
*/

#define RAW_FS 0x80000000UL

/******************************************************************
* List of entries in chandef structure that are saved into EEPROM *
*																  *
* Entries 0 to 3 must be compatible across all channel types.	  *
*																  *
******************************************************************/

static configsave chan_gp_eelist_type0[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 4	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 5	*/
  {0,0}
};

/* Type 1 list added filename for linearisation map table. */

static configsave chan_gp_eelist_type1[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 4	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 5	*/
  {offsetof(chandef, filename), 128},							/* Entry 6	*/
  {0,0}
};

/* Type 2 list added user string area. */

static configsave chan_gp_eelist_type2[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 4	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 5	*/
  {offsetof(chandef, filename), 128},							/* Entry 6	*/
  {offsetof(chandef, userstring), 128},							/* Entry 7	*/
  {0,0}
};

/* Type 3 list added negative gain trim value. */

static configsave chan_gp_eelist_type3[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 4	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 5	*/
  {offsetof(chandef, filename), 128},							/* Entry 6	*/
  {offsetof(chandef, userstring), 128},							/* Entry 7	*/
  {offsetof(chandef, neg_gaintrim), sizeof(int)},				/* Entry 8	*/
  {0,0}
};

/* Type 4 list added cyclic threshold value. */

static configsave chan_gp_eelist_type4[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 4	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 5	*/
  {offsetof(chandef, filename), 128},							/* Entry 6	*/
  {offsetof(chandef, userstring), 128},							/* Entry 7	*/
  {offsetof(chandef, neg_gaintrim), sizeof(int)},				/* Entry 8	*/
  {offsetof(chandef, cycwindow), sizeof(float)},				/* Entry 9	*/
  {0,0}
};

#define CURRENT_CHAN_GP_EELIST_TYPE 4

static configsave* chan_gp_eelist[] = {
  chan_gp_eelist_type0,
  chan_gp_eelist_type1,
  chan_gp_eelist_type2,
  chan_gp_eelist_type3,
  chan_gp_eelist_type4,
};



/********************************************************************************
  FUNCTION NAME     : chan_gp_initlist
  FUNCTION DETAILS  : Initialise configsave list for general-purpose transducer
					  channel.
********************************************************************************/

void chan_gp_initlist(void)
{
	init_savelist(chan_gp_eelist[CURRENT_CHAN_GP_EELIST_TYPE]);
}



/* Default gain determining values */

#define CAL_G20		19.7		/* Default gain value for x20 range					*/
#define CAL_G400	385.6		/* Default gain value for x400 range				*/
#define CAL_RP		100000.0	/* Default value for fine gain trimpot				*/

/********************************************************************************
  FUNCTION NAME     : chan_gp_default
  FUNCTION DETAILS  : Load default values for a general-purpose transducer
					  channel.
********************************************************************************/

void chan_gp_default(chandef* def, uint32_t chanid)
{
	cfg_gp* cfg;
	cal_gp* cal;
	int n;
	float g;

	cfg = &def->cfg.i_gp;
	cal = &def->cal.u.c_gp;

	/* Define save information */

	def->eeformat = CURRENT_CHAN_GP_EELIST_TYPE;
	def->eesavelist = chan_gp_eelist[CURRENT_CHAN_GP_EELIST_TYPE];
	def->eesavetable = chan_gp_eelist;

	/* Build default calibration table */

	for (n = 0; n < 256; n++) {
		g = calc_fine_gain(CAL_RP, n);
		cal->gaintable[n] = g;
		cal->gaintable[256 + n] = g * CAL_G20;
		cal->gaintable[512 + n] = g * CAL_G400;
	}

	/* Define default offset value */

	cal->cal_offset = 0;

	/* Define default excitation calibration values */

	cal->exc_zero = 0.0;
	cal->exc_fs = 20.0;

	cfg->flags = FLAG_CYCWINDOW;
	cfg->pos_gaintrim = 1.0;	/* Default positive gaintrim 	*/
	cfg->neg_gaintrim = 1.0;	/* Default negative gaintrim 	*/
	cfg->gain = 1.0;			/* Default gain 				*/
	cfg->txdrzero = 0.0;		/* Default transducer zero		*/
	cfg->excitation = 0.0;		/* Default excitation		 	*/
	cfg->phase = 0.0;			/* Default phase 				*/

	/* Set reserved locations to zero */

	cfg->faultmask = 0;
	cfg->reserved1 = 0;
	cfg->reserved2 = 0;
	def->filename[0] = '\0';

	def->pos_gaintrim = GAINTRIM_SCALE;
	def->neg_gaintrim = GAINTRIM_SCALE;
	def->txdrzero = 0;
	def->cycwindow = 0.005F; 	/* Default threshold 0.5% 		*/

	/* Set default saturation detection values. These are based on experimental
	   values measured on actual hardware when set to a gain of x1, adjusted by
	   a small safety margin.
	*/

	def->satpos = 472000;
	def->satneg = -472000;

	/* Define default filter1 settings */

	def->filter1.type = FILTER_DISABLE | FILTER_LOWPASS;
	def->filter1.order = 2;
	def->filter1.freq = 500.0;
	def->filter1.bandwidth = 0.0;
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_pointer
  FUNCTION DETAILS  : Set the output pointer for the selected general-purpose
					  transducer channel.

					  On entry:

					  def		Points to the channel definition structure
					  ptr		Address to set output pointer

********************************************************************************/

void chan_gp_set_pointer(chandef* def, int* ptr)
{
	def->outputptr = ptr;

	update_shared_channel_parameter(def, offsetof(chandef, outputptr), sizeof(float*));
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_get_pointer
  FUNCTION DETAILS  : Get the output pointer for the selected general-purpose
					  transducer channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

int* chan_gp_get_pointer(chandef* def)
{
	return(def->outputptr);
}



/********************************************************************************
  FUNCTION NAME     : calc_fine_gain
  FUNCTION DETAILS  : Calculate the gain value for a given trim pot setting.

					  On entry:

					  rp		Calibration value for rp
					  potset	Current pot setting 0 to 255

					  On exit:

					  Returns calculated gain value

********************************************************************************/

float calc_fine_gain(float rp, int potset)
{
	float rb;
	float r2;
	float g;

	r2 = (((float)potset) / 255.0) * rp;
	rb = (rp - r2) + VALUE_RB1;
	g = (VALUE_R3 / VALUE_R1) * ((VALUE_RA + ((VALUE_RA * r2) / rb) + r2) / (VALUE_R3 + VALUE_RA + ((VALUE_RA * r2) / rb) + r2));

	return(g * 5.0);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_calc_gain_settings
  FUNCTION DETAILS  : Calculate the required gain settings corresponding to a
					  requested overall gain value for the selected
					  general-purpose transducer channel.

					  On entry:

					  def		Points to the channel definition structure
					  gain		Required gain value
					  flags		Bit 0 : Sensitivity/Gain
										0 = Gain
										1 = Sensitivity	(ignored by chanproc)

********************************************************************************/

static void chan_gp_calc_gain_settings(chandef* def, float gain, int* coarse, int* fine,
	float* gaintrim)
{
	cal_gp* cal;
	float hwgain;
	int n;
	int entry;
	float maxgain;
	int maxentry;
	float trim;

	cal = &def->cal.u.c_gp;	/* GP specific calibration */

	/* For AC transducers, divide the hardware gain by 1.414 to
	   prevent clipping at the peaks.
	*/

	if (def->ctrl & FLAG_ACTRANSDUCER) gain = gain / 1.414;

	hwgain = gain;

	/* If calibration table type is type 0x0001, then adjust hardware gain
	   to be 0.95 of that requested. This ensures that the fullscale output
	   of the amplifier stage remains clear of the amplifier/ADC limits and
	   prevents saturation at values less than 110% of the required fullscale.
	*/

	if (*((int*)&cal->gaintable[0x2FF]) == 0x00000001) {
		hwgain = gain * 0.95;
	}

	/* Find upper bound on gain to limit search */

	maxgain = 0.0F;
	maxentry = 0;
	for (n = 0; n < 0x2FF; n++) {
		if (cal->gaintable[n] > maxgain) {
			maxgain = cal->gaintable[n];
			maxentry = n;
		}
	}

	/* Find nearest lower gain from available gain settings. Search only
	   up to maximum gain setting, as found during search. Each possible
	   first stage gain option is tested separately, with gains that are
	   too low for that setting being ignored. This avoids the possibility
	   of saturating the first stage with valid inputs.
	*/

	entry = 0;

	/* Test x1 gain range */

	for (n = 0; n <= 0x0FF; n++) {
		if (n > maxentry) break;
		//  if (cal->gaintable[n] < 1.0) continue; /* Skip if gain < 1.0 */
		if (cal->gaintable[n] < hwgain) entry = n;
	}

	/* Test x20 gain range */

	for (n = 256; n <= 0x1FF; n++) {
		if (n > maxentry) break;
		if (cal->gaintable[n] < 20.0) continue; /* Skip if gain < 20.0 */
		if (cal->gaintable[n] < hwgain) entry = n;
	}

	/* Test x400 gain range */

	for (n = 512; n <= 0x2FE; n++) {
		if (n > maxentry) break;
		if (cal->gaintable[n] < 400.0) continue; /* Skip if gain < 400.0 */
		if (cal->gaintable[n] < hwgain) entry = n;
	}

	/* Calculate actual hardware gain */

	hwgain = cal->gaintable[entry];

	/* Update fine software gain trim. For AC transducers the trim gain is
	   scaled to allow for the difference between average value and the RMS value
	   and also by 1.414 to compensate for the reduced hardware gain.
	*/

	trim = gain / hwgain;
	if (def->ctrl & FLAG_ACTRANSDUCER) trim *= (AC_GAIN_SCALE * 1.414);
	if (trim > 3.999) trim = 3.999;

	if (coarse) *coarse = entry >> 8;
	if (fine) *fine = entry & 0xFF;
	if (gaintrim) *gaintrim = trim;
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_gain
  FUNCTION DETAILS  : Set the gain for the selected general-purpose transducer
					  channel.

					  On entry:

					  def		Points to the channel definition structure
					  gain		Required gain value
					  flags		Bits 2,0 : Sensitivity/Gain/Factor
										00 = Gain
										01 = Sensitivity
										10 = Gauge factor / bridge factor (Signal Cube)
								Bit 1 : Retain gain trim value
										0 = Reset gain trim value to 1.0
										1 = Retain existing gain trim value

********************************************************************************/

void chan_gp_set_gain(chandef* def, float gain, uint32_t flags, int mirror)
{
	cfg_gp* cfg;
	float gaintrim;
	int coarse;
	int fine;

	cfg = &def->cfg.i_gp;	/* GP specific configuration */

	/* Save new gain setting and reset gain trim */

	cfg->gain = gain;
	if (!(flags & SETGAIN_RETAIN_GAINTRIM)) {
		cfg->pos_gaintrim = 1.0;
		cfg->neg_gaintrim = 1.0;
	}

	/* Update configuration flags */

	cfg->flags = (cfg->flags & ~FLAG_GAINTYPE) | chan_merge_gainflags(flags);

	chan_gp_calc_gain_settings(def, gain, &coarse, &fine, &gaintrim);

	def->setrawgain(def, coarse, fine);
	def->pos_gaintrim = (uint32_t)(cfg->pos_gaintrim * gaintrim * (float)GAINTRIM_SCALE);
	def->neg_gaintrim = (uint32_t)(cfg->neg_gaintrim * gaintrim * (float)GAINTRIM_SCALE);

	/* Mirror into EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 1);		/* Mirror pos gaintrim value	*/
		mirror_chan_eeconfig(def, 8);		/* Mirror neg gaintrim value	*/
		mirror_chan_eeconfig(def, 5);		/* Mirror cfg area				*/
	}

	update_shared_channel_parameter(def, offsetof(chandef, pos_gaintrim), sizeof(int) * 2);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_gain
  FUNCTION DETAILS  : Read the gain for the selected general-purpose transducer
					  channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_gp_read_gain(chandef* def, float* gain, uint32_t* flags)
{
	cfg_gp* cfg;

	cfg = &def->cfg.i_gp;	/* GP specific configuration	*/
	if (gain) *gain = cfg->gain;
	if (flags) *flags = chan_extract_gainflags(cfg->flags);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_gaintrim
  FUNCTION DETAILS  : Set the gaintrim value for the selected general-purpose
					  transducer channel.

					  On entry:

					  def			Points to the channel definition structure
					  pos_gaintrim	Pointer to required positive gaintrim value
					  neg_gaintrim	Pointer to required negative gaintrim value
					  calmode		Flag set when calibration mode operating.
									Disables calculation of overall trim and
									forces use of supplied value.

********************************************************************************/

void chan_gp_set_gaintrim(chandef* def, float* pos_gaintrim, float* neg_gaintrim, int calmode, int mirror)
{
	cfg_gp* cfg;
	float trim;
	float ptrim;
	float ntrim;

	cfg = &def->cfg.i_gp;	/* GP specific configuration	*/

	if (pos_gaintrim) {
		ptrim = *pos_gaintrim;
		if (ptrim > 3.999) ptrim = 3.999;
		cfg->pos_gaintrim = ptrim;
	}

	if (neg_gaintrim) {
		ntrim = *neg_gaintrim;
		if (ntrim > 3.999) ntrim = 3.999;
		cfg->neg_gaintrim = ntrim;
	}

	if (!calmode) {

		/* Re-calculate current gain value to get current hardware gain trim value */

		chan_gp_calc_gain_settings(def, cfg->gain, NULL, NULL, &trim);

		/* Combine hardware trim with user trim to produce an overall trim value */

		ptrim = trim * ptrim;
		ntrim = trim * ntrim;
	}

	if (ptrim > 3.999) ptrim = 3.999;
	if (ntrim > 3.999) ntrim = 3.999;

	if (pos_gaintrim) {
		def->pos_gaintrim = (uint32_t)(ptrim * (float)GAINTRIM_SCALE);
		update_shared_channel_parameter(def, offsetof(chandef, pos_gaintrim), sizeof(int));
		if (mirror) mirror_chan_eeconfig(def, 1);		/* Mirror pos gaintrim value	*/
	}
	if (neg_gaintrim) {
		def->neg_gaintrim = (uint32_t)(ntrim * (float)GAINTRIM_SCALE);
		update_shared_channel_parameter(def, offsetof(chandef, neg_gaintrim), sizeof(int));
		if (mirror) mirror_chan_eeconfig(def, 8);		/* Mirror neg gaintrim value	*/
	}

	/* Mirror config to EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 5); 	/* Mirror cfg area				*/
	}

}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_gaintrim
  FUNCTION DETAILS  : Read the gaintrim value for the selected general-purpose
					  transducer channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_gp_read_gaintrim(chandef* def, float* pos_gaintrim, float* neg_gaintrim)
{
	cfg_gp* cfg;

	cfg = &def->cfg.i_gp;	/* GP specific configuration	*/

	if (pos_gaintrim) *pos_gaintrim = cfg->pos_gaintrim;
	if (neg_gaintrim) *neg_gaintrim = cfg->neg_gaintrim;
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_refzero
  FUNCTION DETAILS  : Set the reference zero value for the selected
					  general-purpose transducer channel.

					  On entry:

					  track		Flag indicates if control loop should be put
								into tracking mode to adjust setpoint values
								based on offset changes.
********************************************************************************/

int chan_gp_set_refzero(chandef* def, float refzero, int track, int updateclamp, int mirror)
{
	cfg_gp* cfg = &def->cfg.i_gp;	/* GP specific configuration */

	/* Validate the supplied reference zero offset */

	if (chan_validate_zero(def, def->txdrzero, refzero) != NO_ERROR) return(ZERO_RANGE);

	/* Inhibit command clamping while we update the offset */

	chan_update_command_clamp(def, 0.0F, 0.0F, TRUE, updateclamp);

	/* Change the reference zero offset */

	chan_set_refzero(def, refzero, track, mirror);

	/* Update the command clamp */

	chan_update_command_clamp(def, cfg->txdrzero, def->refzero, FALSE, updateclamp);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_txdrzero
  FUNCTION DETAILS  : Set the transducer zero value for the selected
					  general-purpose transducer channel.

					  On entry:

					  def		Points to the channel definition structure
					  txdrzero	Required transducer zero value

********************************************************************************/

int chan_gp_set_txdrzero(chandef* def, float txdrzero, int track, int updateclamp, int mirror)
{
	cfg_gp* cfg;
	int zero;
	float zeroscale;
	int gaintrim;

	cfg = &def->cfg.i_gp;	/* GP specific configuration */

	/* Transducer zero value must be processed to calculate the required offset
	   that needs to be applied at the input to the channel processing path.
	   This must include the effects of:

	   a) Positive/negative calibration gain
	   b) Asymmetrical transducer scaling
	   c) Polarity inversion
	   d) Unipolar transducer scaling

	   Note that when determining whether to use the positive or negative gain trim
	   value in the scaling calculation, the test is reversed from the expected sense
	   because the txdrzero value being used is the required offset to achieve zero
	   and so has the opposite polarity to the actual input signal.

	*/

	/* Select appropriate gaintrim value based on polarity and 2-point cal flag */

	gaintrim = (cfg->flags & FLAG_2PTCAL) ?
		(((txdrzero * ((cfg->flags & FLAG_POLARITY) ? -1.0F : 1.0F)) < 0.0F) ? def->pos_gaintrim : def->neg_gaintrim) :
		def->pos_gaintrim;

	zeroscale = ((float)gaintrim / (float)GAINTRIM_SCALE) *
		((cfg->flags & FLAG_ASYMMETRICAL) ? 0.5F : 1.0F) *
		((cfg->flags & FLAG_POLARITY) ? -1.0F : 1.0F) *
		((cfg->flags & FLAG_UNIPOLAR) ? 2.0F : 1.0F);

	zero = (int)(((txdrzero / zeroscale) * (float)RAW_FS) / 1.1F);

	/* Validate the supplied transducer zero offset */

	if (chan_validate_zero(def, zero, def->refzero) != NO_ERROR) return(ZERO_RANGE);

	cfg->txdrzero = txdrzero;

	/* Inhibit command clamping while we update the offset */

	chan_update_command_clamp(def, 0.0F, 0.0F, TRUE, updateclamp);

	/* Change the transducer zero offset */

	update_txdrzero(def, zero, track);

	/* Update the command clamp */

	chan_update_command_clamp(def, cfg->txdrzero, def->refzero, FALSE, updateclamp);

	if (mirror) {
		mirror_chan_eeconfig(def, 5); 	/* Mirror cfg area		*/
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_txdrzero
  FUNCTION DETAILS  : Read the transducer zero value for the selected
					  general-purpose transducer channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

float chan_gp_read_txdrzero(chandef* def)
{
	cfg_gp* cfg;

	cfg = &def->cfg.i_gp;	/* GP specific configuration	*/

	return(cfg->txdrzero);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_txdrzero_zero
  FUNCTION DETAILS  : Zero the current transducer value using the transducer zero
					  offset.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

int chan_gp_txdrzero_zero(chandef* def, int track, int updateclamp, int mirror)
{
	cfg_gp* cfg;
	int zero;
	int gaintrim;
	float fb;
	float fbscale;
	float fbpol;
	float rawfb;

	cfg = &def->cfg.i_gp;	/* GP specific configuration */

	/* Should disable interrupts around this code */

	fb = chan_read_zerofiltop(def) - def->refzero;	// Calculate input value excluding ref zero

	/* Determine actual polarity of raw feedback signal, taking into account any polarity
	   inversion that has been applied.
	*/

	fbpol = fb * ((cfg->flags & FLAG_POLARITY) ? -1.0F : 1.0F);

	/* Select appropriate gaintrim value based on polarity and 2-point cal flag */

	gaintrim = (cfg->flags & FLAG_2PTCAL) ?
		((fbpol >= 0.0F) ? def->pos_gaintrim : def->neg_gaintrim) :
		def->pos_gaintrim;

	/* Feedback value must be processed to calculate the actual raw feedback signal
	   that is present at the input to the channel processing path.
	   This must include the effects of:

	   a) Positive/negative calibration gain
	   b) Asymmetrical transducer scaling
	   c) Polarity inversion
	   d) Unipolar transducer scaling

	*/

	fbscale = ((float)gaintrim / (float)GAINTRIM_SCALE) *
		((cfg->flags & FLAG_ASYMMETRICAL) ? 0.5 : 1.0) *
		((cfg->flags & FLAG_POLARITY) ? -1.0 : 1.0) *
		((cfg->flags & FLAG_UNIPOLAR) ? 2.0 : 1.0);

	rawfb = fb / fbscale;

	zero = (int)((rawfb * (float)RAW_FS) / 1.1F);

	/* Validate the calculated transducer zero offset */

	if (chan_validate_zero(def, (def->txdrzero - zero), 0.0F) != NO_ERROR) return(ZERO_RANGE);

	/* Inhibit command clamping while we update the offset */

	chan_update_command_clamp(def, 0.0F, 0.0F, TRUE, updateclamp);

	def->set_refzero(def, 0.0F, TRUE, UPDATECLAMP, MIRROR);	// Remove ref zero offset

	/* Change the transducer zero offset */

	update_txdrzero(def, (def->txdrzero - zero), track);
	cfg->txdrzero = cfg->txdrzero - fb;

	/* Update the command clamp */

	chan_update_command_clamp(def, cfg->txdrzero, def->refzero, FALSE, updateclamp);

	if (mirror) {
		mirror_chan_eeconfig(def, 5); 	/* Mirror cfg area		*/
	}

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_refzero_zero
  FUNCTION DETAILS  : Zero the current transducer value using the reference zero
					  offset.

					  On entry:

					  def		Points to the channel definition structure

					  track		Flag indicates if control loop should be put
								into tracking mode to adjust setpoint values
								based on offset changes.

********************************************************************************/

int chan_gp_refzero_zero(chandef* def, int track, int updateclamp, int mirror)
{
	return(chan_gp_set_refzero(def, def->refzero - chan_read_zerofiltop(def), TRUE, UPDATECLAMP, MIRROR));
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_excitation
  FUNCTION DETAILS  : Set the excitation for the selected general-purpose
					  transducer channel.

					  On entry:

					  def		Points to the channel definition structure
					  type		Excitation type DC/AC
					  volt		Excitation voltage
					  phase		Excitation phase shift in degrees

********************************************************************************/

void chan_gp_set_excitation(chandef* def, int type, float volt, float phase,
	int mirror)
{
	cal_gp* cal;
	cfg_gp* cfg;
	int ampl;		/* Raw excitation amplitude */
	int offset;		/* Raw excitation offset	*/
	float ac_exc_scale;

	cal = &def->cal.u.c_gp;	/* GP specific calibration		*/
	cfg = &def->cfg.i_gp;	/* GP specific configuration	*/

	/* Save excitation settings */

	cfg->excitation = volt;
	cfg->phase = phase;
	cfg->flags = (cfg->flags & ~FLAG_ACTRANSDUCER) | ((type) ? FLAG_ACTRANSDUCER : 0);

	/* If AC excitation rolloff scale compensation has been enabled by the
		calibration software, then apply the extra scale factor to compensate
		for rolloff in the excitation amplifiers.
	*/

	ac_exc_scale = (*((int*)&cal->gaintable[0x2FE]) == 0x00000003) ? AC_EXC_SCALE : 1.0F;

	/* For AC transducers the excitation is set as an RMS voltage. This must be
	   converted to a peak value for programming the excitation table.
	*/

	if (type) volt = volt * 1.414 * ac_exc_scale;

	/* Determine raw DAC value based on required voltage output */

	ampl = (int)((volt / cal->exc_fs) * EXCDAC_FS);
	offset = (int)((cal->exc_zero / cal->exc_fs) * EXCDAC_FS);

	def->setrawexc(def, type, ampl, phase, offset);
	def->enableexc(def, TRUE);

	/* Update gain in case the transducer type has changed. */

	def->setgain(def, cfg->gain,
		SETGAIN_RETAIN_GAINTRIM | chan_extract_gainflags(cfg->flags),
		NO_MIRROR);

	/* Mirror into EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 1); /* Mirror pos gaintrim value	*/
		mirror_chan_eeconfig(def, 5);	/* Mirror cfg area				*/
	}

	update_shared_channel_parameter(def, offsetof(chandef, pos_gaintrim), sizeof(int) * 2);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_excitation
  FUNCTION DETAILS  : Reads the current excitation setting for the selected
					  general-purpose transducer channel.

					  On entry:

					  def		Points to the channel definition structure
					  type		Ptr to location to store excitation type DC/AC
					  volt		Ptr to location to store excitation voltage
					  phase		Ptr to location to store phase shift in degrees

********************************************************************************/

void chan_gp_read_excitation(chandef* def, int* type, float* volt, float* phase)
{
	cfg_gp* cfg;

	cfg = &def->cfg.i_gp;		/* GP specific configuration	*/

	if (type) *type = cfg->flags & FLAG_ACTRANSDUCER;
	if (volt) *volt = cfg->excitation;
	if (phase) *phase = cfg->phase;
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_caltable
  FUNCTION DETAILS  : Sets a calibration table entry for a GP channel.
********************************************************************************/

void chan_gp_set_caltable(chandef* def, int entry, float value)
{
	cal_gp* cal;

	cal = &def->cal.u.c_gp;	/* GP specific calibration	*/
	cal->gaintable[entry] = value;
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_caltable
  FUNCTION DETAILS  : Reads a calibration table entry for a GP channel.
********************************************************************************/

float chan_gp_read_caltable(chandef* def, int entry)
{
	cal_gp* cal;

	cal = &def->cal.u.c_gp;		/* GP specific calibration	*/
	return(cal->gaintable[entry]);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_calgain
  FUNCTION DETAILS  : Sets the calibration gain for a GP channel.

					  Note this is a dummy function since the general-purpose
					  channels have a table of gain values (one for each
					  hardware gain setting), rather than a single value.
********************************************************************************/

void chan_gp_set_calgain(chandef* def, float gain)
{
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_calgain
  FUNCTION DETAILS  : Reads the calibration gain for a GP channel.

					  Note this is a dummy function since the general-purpose
					  channels have a table of gain values (one for each
					  hardware gain setting), rather than a single value.
********************************************************************************/

float chan_gp_read_calgain(chandef* def)
{
	return(1.0);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_caloffset
  FUNCTION DETAILS  : Sets the calibration offset for a GP channel.
********************************************************************************/

void chan_gp_set_caloffset(chandef* def, float offset)
{
	cal_gp* cal;

	cal = &def->cal.u.c_gp;	/* GP specific calibration	*/

	cal->cal_offset = (int)((offset * (float)RAW_FS) / 1.1F);
	def->offset = cal->cal_offset;
	update_shared_channel_parameter(def, offsetof(chandef, offset), sizeof(int));
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_caloffset
  FUNCTION DETAILS  : Reads the calibration offset for a GP channel.
********************************************************************************/

float chan_gp_read_caloffset(chandef* def)
{
	cal_gp* cal;

	cal = &def->cal.u.c_gp;		/* GP specific calibration	*/

	return((((float)cal->cal_offset) * 1.1F) / (float)RAW_FS);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_exccal
  FUNCTION DETAILS  : Set the excitation calibration values for the selected
					  general-purpose transducer channel.

					  On entry:

					  def		Points to the channel definition structure
					  exc_zero	Excitation zero calibration value
					  exc_fs	Excitation full scale calibration value

********************************************************************************/

void chan_gp_set_exccal(chandef* def, uint32_t flags, float exc_zero, float exc_fs)
{
	cal_gp* cal;

	cal = &def->cal.u.c_gp;	/* GP specific calibration	*/

	cal->exc_zero = exc_zero;
	cal->exc_fs = exc_fs;
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_exccal
  FUNCTION DETAILS  : Reads the current excitation calibration values for the
					  selected general-purpose transducer channel.

					  On entry:

					  def		Points to the channel definition structure
					  exc_zero	Ptr to location to store zero calibration
					  exc_fs	Ptr to location to store full-scale calibration

********************************************************************************/

void chan_gp_read_exccal(chandef* def, uint32_t flags, float* exc_zero, float* exc_fs)
{
	cal_gp* cal;

	cal = &def->cal.u.c_gp;	/* GP specific calibration	*/

	if (exc_zero) *exc_zero = cal->exc_zero;
	if (exc_fs) *exc_fs = cal->exc_fs;
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_flags
  FUNCTION DETAILS  : Modify the control flags for the selected general-purpose
					  transducer channel.
********************************************************************************/

void chan_gp_set_flags(chandef* def, int set, int clr, int updateclamp, int mirror)
{
	uint32_t istate;
	cfg_gp* cfg;
	uint32_t oldstate;
	uint32_t newstate;

	cfg = &def->cfg.i_gp; /* GP specific configuration */

	/* Determine the new flag state by setting and clearing bits as specified by
	   set and clr parameters. Note that the simulation and disable flags in the
	   chandef structure may have been modified by KO code and this will not be
	   reflected in the cfg structure. Therefore, we must read their states directly
	   from the chandef structure.

	   Similarly, the virtual channel flag is stored in the chandef structure, but is
	   not present in the config structure. Therefore, it must also be read directly
	   from the chandef structure.
	*/

	oldstate = (cfg->flags & ~(FLAG_SIMULATION | FLAG_VIRTUAL | FLAG_DISABLE)) | (def->ctrl & (FLAG_SIMULATION | FLAG_VIRTUAL | FLAG_DISABLE));
	newstate = (oldstate & ~clr) | set;

	/* Update configuration data and channel control flags. Need to protect against interrupts
	   between updating DSP A memory and copying to DSP B memory.
	*/

	cfg->flags = newstate;

	istate = disable_int(); /* Disable interrupts whilst updating */

	def->ctrl = newstate;

	/* Update transducer flags setting in shared channel definition */

	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(int));

	restore_int(istate);

	/* If the asymmetrical, unipolar or polarity flags have changed then update the
	   command clamp values.
	*/

	if ((newstate ^ oldstate) & (FLAG_ASYMMETRICAL | FLAG_UNIPOLAR | FLAG_POLARITY))
		chan_update_command_clamp(def, cfg->txdrzero, def->refzero, FALSE, updateclamp);

	/* Mirror into EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 5);	/* Mirror cfg area		*/
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_flags
  FUNCTION DETAILS  : Read the control flags for the selected general-purpose
					  transducer channel.
********************************************************************************/

int chan_gp_read_flags(chandef* def)
{
	cfg_gp* cfg;

	cfg = &def->cfg.i_gp;		/* GP specific configuration	*/

	return(cfg->flags);
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_write_map
  FUNCTION DETAILS  : Write a linearisation map table entry for the selected
					  general-purpose channel.

					  On entry:

					  flags		Control flags.
									Bit 0,1	Linearisation mode:
										0 		 = Disabled
										Non-zero = Enabled
					  value		Points to the start of linearisation data
								  to copy into definition. If NULL, then
								  no data is copied.
					  filename	Points to the start of the linearisation
								table filename. If NULL then no filename
								is copied.
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_gp_write_map(chandef* def, uint32_t flags, float* value, char* filename,
	int flush, int mirror)
{
	if (value) memcpy(def->map, value, sizeof(def->map));
	if (filename) memcpy(def->filename, filename, sizeof(def->filename));
	def->cfg.i_gp.flags = (def->cfg.i_gp.flags & ~FLAG_LINEARISE) | ((flags & 0x03) ? FLAG_LINEARISE : 0);
	def->ctrl = (def->ctrl & ~FLAG_LINEARISE) | ((flags & 0x03) ? FLAG_LINEARISE : 0);
	if (mirror) {
		mirror_chan_eeconfig(def, 5);					/* Mirror chandef configuration data     */
		if (value) mirror_chan_eeconfig(def, 4);		/* Mirror chandef linearisation table    */
		if (filename) mirror_chan_eeconfig(def, 6);	/* Mirror chandef linearisation filename */
	}
	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(uint32_t));
	if (value) {
		hpi_blockwrite((uint8_t*)def->mapptr, (uint8_t*)def->map, sizeof(float) * MAPSIZE);	/* Copy map to DSP B */
		if (flush) flush_map(def);														/* Flush cache area	 */
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_map
  FUNCTION DETAILS  : Read a linearisation map table entry for the selected
					  general-purpose channel.
********************************************************************************/

void chan_gp_read_map(chandef* def, uint32_t* flags, float* value, char* filename)
{
	if (flags) *flags = (def->cfg.i_gp.flags & FLAG_LINEARISE) >> BIT_LINEARISE;
	if (value) memcpy(value, def->map, sizeof(float) * MAPSIZE);			/* Copy map */
	if (filename) memcpy(filename, def->filename, sizeof(def->filename));	/* Copy filename */
}




/********************************************************************************
  FUNCTION NAME     : chan_gp_write_userstring
  FUNCTION DETAILS  : Write user string for the selected general-purpose channel.

					  On entry:

					  string	Points to the start of the user string.
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_gp_write_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string, int mirror)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(&def->userstring[start], string, length);
	if (mirror) {
		if (string) mirror_chan_eeconfig(def, 7);	/* Mirror user string */
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_userstring
  FUNCTION DETAILS  : Read user string for the selected general-purpose channel.
********************************************************************************/

void chan_gp_read_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(string, &def->userstring[start], length);
}



/*******************************************************************************
 FUNCTION NAME	   : chan_gp_set_peak_threshold
 FUNCTION DETAILS  : Set threshold parameter for peak detection.

					 On entry:

					 flags			Flags:
										Bit  30		Enable local channel threshold
					 threshold		Threshold value for peak detection

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_gp_set_peak_threshold(chandef* def, uint32_t flags, float threshold, int mirror)
{
	cfg_gp* cfg;

	cfg = &def->cfg.i_gp; /* GP specific configuration */

	cfg->flags = (cfg->flags & ~FLAG_CYCWINDOW) | ((flags & 0x40000000) ? FLAG_CYCWINDOW : 0);
	def->ctrl = (def->ctrl & ~FLAG_CYCWINDOW) | ((flags & 0x40000000) ? FLAG_CYCWINDOW : 0);
	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(uint32_t));
	def->cycwindow = threshold;
	update_shared_channel_parameter(def, offsetof(chandef, cycwindow), sizeof(float));
	if (mirror) {
		mirror_chan_eeconfig(def, 9);	/* Mirror cyclic window value */
		mirror_chan_eeconfig(def, 5);	/* Mirror cfg area		*/
	}
}



/*******************************************************************************
 FUNCTION NAME	   : chan_gp_read_peak_threshold
 FUNCTION DETAILS  : Read threshold parameter for peak detection.

					 On exit:

					 flags			Flags:
										Bit  30		Enable local channel threshold
					 threshold		Threshold value for peak detection

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_gp_read_peak_threshold(chandef* def, uint32_t* flags, float* threshold)
{
	if (flags) *flags = (def->ctrl & FLAG_CYCWINDOW) ? 0x40000000 : 0;
	if (threshold) *threshold = def->cycwindow;
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_write_faultmask
  FUNCTION DETAILS  : Write fault mask value for the selected general-purpose
					  channel.

					  On entry:

					  mask		Mask flags.
									Bit 0	Input fault enable
									Bit 1	Excitation sense fault enable
									Bit 2	Excitation fault enable
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_gp_write_faultmask(chandef* def, uint32_t mask, int mirror)
{
	uint32_t m;
	int lp;		/* Local channel number on card */
	int slot;

	/* Modify the mask in the channel defintion */

	def->cfg.i_gp.faultmask = mask;

	/* Get the slot number and the local channel number on the card */

	slot = EXTSLOTNUM;
	lp = EXTSLOTCHAN;

	/* Modify the mask in the slot table */

	m = slot_status[slot].faultmask;
	slot_status[slot].faultmask = m & ~(0x07 << (lp * 3)) | ((mask & 0x07) << (lp * 3));

	if (mirror) {
		mirror_chan_eeconfig(def, 5);	/* Mirror chandef configuration data     */
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_read_faultstate
  FUNCTION DETAILS  : Read fault state for the selected general-purpose channel.
********************************************************************************/

uint32_t chan_gp_read_faultstate(chandef* def, uint32_t* capabilities, uint32_t* mask)
{
	uint32_t state;
	hw_mboard* base;

	/* Read the current fault state. Since all fault registers are at the same offset
	   we can use the hw_mboard structure as a generic means to access them.
	*/

	base = def->hw.i_mboard.hw;
	state = (uint32_t)base->u.rd.fault;

	/* The fault capability word depends on the board revision. Check if any fault
	   detection is available by testing bit 15 of the fault register.
	*/

	if (capabilities) *capabilities = (state & 0x8000) ? (FAULT_INPUT | FAULT_SENSE | FAULT_DRIVE) : 0;
	if (mask) *mask = def->cfg.i_gp.faultmask;

	/* Extract the fault bits corresponding to the selected channel */

	return(((state & 0x7FFF) >> (EXTSLOTCHAN * 3)) & 0x07);
}
