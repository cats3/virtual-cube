/********************************************************************************
 * MODULE NAME       : chan_vt.c												*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Routines to support a virtual channel					*
 ********************************************************************************/

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

#include <stdint.h>
#include "porting.h"


#include "defines.h"
//#include "ccubecfg.h"
#include "hardware.h"
#include "channel.h"
#include "cnet.h"
#include "shared.h"
#include "asmcode.h"


 /******************************************************************
 * List of entries in chandef structure that are saved into EEPROM *
 *																  *
 * Entries 0 to 3 must be compatible across all channel types.	  *
 *																  *
 ******************************************************************/

static configsave chan_vt_eelist_type0[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 4	*/
  {0,0}
};

/* Type 1 list added user string area. */

static configsave chan_vt_eelist_type1[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 4	*/
  {offsetof(chandef, userstring), 128},							/* Entry 5 */
  {0,0}
};

/* Type 2 list added negative gain trim value. */

static configsave chan_vt_eelist_type2[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 4	*/
  {offsetof(chandef, userstring), 128},							/* Entry 5 	*/
  {offsetof(chandef, neg_gaintrim), sizeof(int)},				/* Entry 6	*/
  {0,0}
};

/* Type 3 list added linearisation table. */

static configsave chan_vt_eelist_type3[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 4	*/
  {offsetof(chandef, userstring), 128},							/* Entry 5 	*/
  {offsetof(chandef, neg_gaintrim), sizeof(int)},				/* Entry 6	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 7	*/
  {offsetof(chandef, filename), 128},							/* Entry 8	*/
  {0,0}
};

/* Type 4 list added cyclic threshold value. */

static configsave chan_vt_eelist_type4[] = {
  {offsetof(chandef, offset), sizeof(int)},						/* Entry 0	*/
  {offsetof(chandef, pos_gaintrim), sizeof(int)},				/* Entry 1	*/
  {offsetof(chandef, refzero), sizeof(float)},					/* Entry 2	*/
  {offsetof(chandef, filter1), sizeof(filter)},					/* Entry 3 	*/
  {offsetof(chandef, cfg), sizeof(union cfginfo)},				/* Entry 4	*/
  {offsetof(chandef, userstring), 128},							/* Entry 5 	*/
  {offsetof(chandef, neg_gaintrim), sizeof(int)},				/* Entry 6	*/
  {offsetof(chandef, map), (sizeof(float) * MAPSIZE)},			/* Entry 7	*/
  {offsetof(chandef, filename), 128},							/* Entry 8	*/
  {offsetof(chandef, cycwindow), sizeof(float)},				/* Entry 9	*/
  {0,0}
};

#define CURRENT_CHAN_VT_EELIST_TYPE 4

static configsave* chan_vt_eelist[] = {
  chan_vt_eelist_type0,
  chan_vt_eelist_type1,
  chan_vt_eelist_type2,
  chan_vt_eelist_type3,
  chan_vt_eelist_type4,
};



/********************************************************************************
  FUNCTION NAME     : chan_vt_initlist
  FUNCTION DETAILS  : Initialise configsave list for virtual channel.
********************************************************************************/

void chan_vt_initlist(void)
{
	init_savelist(chan_vt_eelist[CURRENT_CHAN_VT_EELIST_TYPE]);
}



/********************************************************************************
  FUNCTION NAME     : chan_vt_default
  FUNCTION DETAILS  : Load default values for a virtual channel.
********************************************************************************/

void chan_vt_default(chandef* def, uint32_t chanid)
{
	cfg_vt* cfg;

	cfg = &def->cfg.i_vt;

	/* Define save information */

	def->eeformat = CURRENT_CHAN_VT_EELIST_TYPE;
	def->eesavelist = chan_vt_eelist[CURRENT_CHAN_VT_EELIST_TYPE];
	def->eesavetable = chan_vt_eelist;

	cfg->flags = FLAG_CYCWINDOW;
	cfg->pos_gaintrim = 1.0;	/* Default positive gaintrim 	*/
	cfg->neg_gaintrim = 1.0;	/* Default negative gaintrim 	*/
	cfg->gain = 1.0;			/* Default gain 				*/
	cfg->txdrzero = 0.0;		/* Default transducer zero		*/

	/* Set reserved locations to zero */

	cfg->reserved1 = 0;
	cfg->reserved2 = 0;
	cfg->reserved3 = 0;

	def->pos_gaintrim = GAINTRIM_SCALE;
	def->neg_gaintrim = GAINTRIM_SCALE;
	def->txdrzero = 0;
	def->cycwindow = 0.005F; 	/* Default threshold 0.5%		*/

	/* Set default saturation detection values */

	def->satpos = 0x7FFFFFFF;
	def->satneg = -0x7FFFFFFF;

	/* Define default filter1 settings */

	def->filter1.type = FILTER_DISABLE | FILTER_LOWPASS;
	def->filter1.order = 2;
	def->filter1.freq = 500.0;
	def->filter1.bandwidth = 0.0;
}



/********************************************************************************
  FUNCTION NAME     : chan_vt_set_flags
  FUNCTION DETAILS  : Modify the control flags for the selected virtual channel.
********************************************************************************/

void chan_vt_set_flags(chandef* def, int set, int clr, int updateclamp, int mirror)
{
	uint32_t istate;
	cfg_vt* cfg;
	uint32_t oldstate;
	uint32_t newstate;

	cfg = &def->cfg.i_vt; /* Virtual transducer specific configuration */

	/* Determine the new flag state by setting and clearing bits as specified by
	   set and clr parameters. Note that the simulation and disable flags in the
	   chandef structure may have been modified by KO code and this will not be
	   reflected in the cfg structure. Therefore, we must read their states directly
	   from the chandef structure.

	   Similarly, the virtual channel flag is stored in the chandef structure, but is
	   not present in the config structure. Therefore, it must also be read directly
	   from the chandef structure.
	*/

	oldstate = (cfg->flags & ~(FLAG_SIMULATION | FLAG_VIRTUAL | FLAG_DISABLE)) | (def->ctrl & (FLAG_SIMULATION | FLAG_VIRTUAL | FLAG_DISABLE));
	newstate = (oldstate & ~clr) | set;

	/* Update configuration data and channel control flags. Need to protect against interrupts
	   between updating DSP A memory and copying to DSP B memory.
	*/

	cfg->flags = newstate;

	istate = disable_int(); /* Disable interrupts whilst updating */

	def->ctrl = newstate;

	/* Update transducer flags setting in shared channel definition */

	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(int));

	restore_int(istate);

	/* If the asymmetrical, unipolar or polarity flags have changed then update the
	   command clamp values.
	*/

	if ((newstate ^ oldstate) & (FLAG_ASYMMETRICAL | FLAG_UNIPOLAR | FLAG_POLARITY))
		chan_update_command_clamp(def, cfg->txdrzero, def->refzero, FALSE, updateclamp);

	/* Mirror into EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 4);	/* Mirror cfg area		*/
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_vt_read_flags
  FUNCTION DETAILS  : Read the control flags for the selected virtual channel.
********************************************************************************/

int chan_vt_read_flags(chandef* def)
{
	cfg_vt* cfg;

	cfg = &def->cfg.i_vt;		/* Virtual transducer specific configuration	*/

	return(cfg->flags);
}



/********************************************************************************
  FUNCTION NAME     : chan_vt_write_map
  FUNCTION DETAILS  : Write a linearisation map table for the selected virtual
					  channel.

					  On entry:

					  flags		Control flags.
									Bit 0,1	Linearisation mode:
										0 		 = Disabled
										Non-zero = Enabled
					  value		Points to the start of linearisation data
								  to copy into definition. If NULL, then
								  no data is copied.
					  filename	Points to the start of the linearisation
								table filename. If NULL then no filename
								is copied.
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_vt_write_map(chandef* def, uint32_t flags, float* value, char* filename,
	int flush, int mirror)
{
	if (value) memcpy(def->map, value, sizeof(def->map));
	if (filename) memcpy(def->filename, filename, sizeof(def->filename));
	def->cfg.i_vt.flags = (def->cfg.i_vt.flags & ~FLAG_LINEARISE) | ((flags & 0x03) ? FLAG_LINEARISE : 0);
	def->ctrl = (def->ctrl & ~FLAG_LINEARISE) | ((flags & 0x03) ? FLAG_LINEARISE : 0);
	if (mirror) {
		mirror_chan_eeconfig(def, 4);					/* Mirror chandef configuration data     */
		if (value) mirror_chan_eeconfig(def, 7);		/* Mirror chandef linearisation table    */
		if (filename) mirror_chan_eeconfig(def, 8);	/* Mirror chandef linearisation filename */
	}
	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(uint32_t));
	if (value) {
#if (defined(CONTROLCUBE) || defined(AICUBE))
		hpi_blockwrite((uint8_t*)def->mapptr, (uint8_t*)def->map, sizeof(float) * MAPSIZE);	/* Copy map to DSP B */
		if (flush) flush_map(def);														/* Flush cache area	 */
#endif
#ifdef SIGNALCUBE
		memcpy(def->map, value, sizeof(def->map));
#endif
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_vt_read_map
  FUNCTION DETAILS  : Read a linearisation map table for the selected virtual
					  channel.
********************************************************************************/

void chan_vt_read_map(chandef* def, uint32_t* flags, float* value, char* filename)
{
	if (flags) *flags = (def->cfg.i_vt.flags & FLAG_LINEARISE) >> BIT_LINEARISE;
	if (value) memcpy(value, def->map, sizeof(float) * MAPSIZE);			/* Copy map */
	if (filename) memcpy(filename, def->filename, sizeof(def->filename));	/* Copy filename */
}



/********************************************************************************
  FUNCTION NAME     : chan_vt_set_gaintrim
  FUNCTION DETAILS  : Set the gaintrim value for the selected virtual channel.

					  On entry:

					  def			Points to the channel definition structure
					  pos_gaintrim	Pointer to required positive gaintrim value
					  neg_gaintrim	Pointer to required negative gaintrim value
					  calmode		Flag set when calibration mode operating.
									Disables calculation of overall trim and
									forces use of supplied value.

********************************************************************************/

void chan_vt_set_gaintrim(chandef* def, float* pos_gaintrim, float* neg_gaintrim, int calmode, int mirror)
{
	cfg_vt* cfg;
	float ptrim;
	float ntrim;

	cfg = &def->cfg.i_vt;	/* Virtual specific configuration */

	if (pos_gaintrim) {
		ptrim = *pos_gaintrim;
		if (ptrim > 3.999) ptrim = 3.999;
		cfg->pos_gaintrim = ptrim;
		def->pos_gaintrim = (uint32_t)(ptrim * (float)GAINTRIM_SCALE);
		update_shared_channel_parameter(def, offsetof(chandef, pos_gaintrim), sizeof(int));
		if (mirror) mirror_chan_eeconfig(def, 1);		/* Mirror pos gaintrim value	*/
	}

	if (neg_gaintrim) {
		ntrim = *neg_gaintrim;
		if (ntrim > 3.999) ntrim = 3.999;
		cfg->neg_gaintrim = ntrim;
		def->neg_gaintrim = (uint32_t)(ntrim * (float)GAINTRIM_SCALE);
		update_shared_channel_parameter(def, offsetof(chandef, neg_gaintrim), sizeof(int));
		if (mirror) mirror_chan_eeconfig(def, 6);		/* Mirror neg gaintrim value	*/
	}

	/* Mirror config to EEPROM if required */

	if (mirror) {
		mirror_chan_eeconfig(def, 4); 	/* Mirror cfg area				*/
	}

}



/********************************************************************************
  FUNCTION NAME     : chan_vt_read_gaintrim
  FUNCTION DETAILS  : Read the gaintrim value for the selected virtual channel.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

void chan_vt_read_gaintrim(chandef* def, float* pos_gaintrim, float* neg_gaintrim)
{
	cfg_vt* cfg;

	cfg = &def->cfg.i_vt;	/* Virtual specific configuration */

	if (pos_gaintrim) *pos_gaintrim = cfg->pos_gaintrim;
	if (neg_gaintrim) *neg_gaintrim = cfg->neg_gaintrim;
}



/********************************************************************************
  FUNCTION NAME     : chan_gp_set_refzero
  FUNCTION DETAILS  : Set the reference zero value for the selected virtual
					  channel.

					  On entry:

					  track		Flag indicates if control loop should be put
								into tracking mode to adjust setpoint values
								based on offset changes.
********************************************************************************/

int chan_vt_set_refzero(chandef* def, float refzero, int track, int updateclamp, int mirror)
{
	cfg_vt* cfg = &def->cfg.i_vt;	/* VT specific configuration	*/

	/* Validate the supplied reference zero offset */

	if (chan_validate_zero(def, 0, refzero) != NO_ERROR) return(ZERO_RANGE);

	/* Inhibit command clamping while we update the offset */

	chan_update_command_clamp(def, 0.0F, 0.0F, TRUE, updateclamp);

	/* Change the reference zero offset */

	chan_set_refzero(def, refzero, track, mirror);

	/* Update the command clamp */

	chan_update_command_clamp(def, cfg->txdrzero, def->refzero, FALSE, updateclamp);

	return(NO_ERROR);
}



/********************************************************************************
  FUNCTION NAME     : chan_vt_refzero_zero
  FUNCTION DETAILS  : Zero the current transducer value using the reference zero
					  offset.

					  On entry:

					  def		Points to the channel definition structure

********************************************************************************/

int chan_vt_refzero_zero(chandef* def, int track, int updateclamp, int mirror)
{
	return(chan_vt_set_refzero(def, def->refzero - chan_read_zerofiltop(def), TRUE, UPDATECLAMP, MIRROR));
}



/********************************************************************************
  FUNCTION NAME     : chan_vt_write_userstring
  FUNCTION DETAILS  : Write user string for the selected virtual channel.

					  On entry:

					  string	Points to the start of the user string.
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_vt_write_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string, int mirror)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(&def->userstring[start], string, length);
	if (mirror) {
		if (string) mirror_chan_eeconfig(def, 5);	/* Mirror user string */
	}
}



/********************************************************************************
  FUNCTION NAME     : chan_vt_read_userstring
  FUNCTION DETAILS  : Read user string for the selected virtual channel.
********************************************************************************/

void chan_vt_read_userstring(chandef* def, uint32_t start, uint32_t length,
	char* string)
{
	if (start > (sizeof(def->userstring) - 1)) start = sizeof(def->userstring) - 1;
	if ((start + length) > sizeof(def->userstring)) length = sizeof(def->userstring) - start;
	if (string) memcpy(string, &def->userstring[start], length);
}



/*******************************************************************************
 FUNCTION NAME	   : chan_vt_set_peak_threshold
 FUNCTION DETAILS  : Set threshold parameter for peak detection.

					 On entry:

					 flags			Flags:
										Bit  30		Enable local channel threshold
					 threshold		Threshold value for peak detection

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_vt_set_peak_threshold(chandef* def, uint32_t flags, float threshold, int mirror)
{
	cfg_vt* cfg;

	cfg = &def->cfg.i_vt; /* VT specific configuration */

	cfg->flags = (cfg->flags & ~FLAG_CYCWINDOW) | ((flags & 0x40000000) ? FLAG_CYCWINDOW : 0);
	def->ctrl = (def->ctrl & ~FLAG_CYCWINDOW) | ((flags & 0x40000000) ? FLAG_CYCWINDOW : 0);
	update_shared_channel_parameter(def, offsetof(chandef, ctrl), sizeof(uint32_t));
	def->cycwindow = threshold;
	update_shared_channel_parameter(def, offsetof(chandef, cycwindow), sizeof(float));
	if (mirror) {
		mirror_chan_eeconfig(def, 9);	/* Mirror cyclic window value */
		mirror_chan_eeconfig(def, 4);	/* Mirror cfg area		*/
	}
}



/*******************************************************************************
 FUNCTION NAME	   : chan_vt_read_peak_threshold
 FUNCTION DETAILS  : Read threshold parameter for peak detection.

					 On exit:

					 flags			Flags:
										Bit  30		Enable local channel threshold
					 threshold		Threshold value for peak detection

********************************************************************************
 TASK OR INTERRUPT SOURCE :
********************************************************************************
 RESOURCE USAGE :
********************************************************************************
 Called From :
 Calls:
*******************************************************************************/

void chan_vt_read_peak_threshold(chandef* def, uint32_t* flags, float* threshold)
{
	if (flags) *flags = (def->ctrl & FLAG_CYCWINDOW) ? 0x40000000 : 0;
	if (threshold) *threshold = def->cycwindow;
}



/********************************************************************************
  FUNCTION NAME     : chan_vt_write_faultmask
  FUNCTION DETAILS  : Write fault mask value for the selected virtual channel.

					  On entry:

					  mask		Mask flags.
									Bit 0	Reserved
									Bit 1	Reserved
									Bit 2	Drive current fault enable
					  mirror	Controls mirroring of data into EEPROM.
********************************************************************************/

void chan_vt_write_faultmask(chandef* def, uint32_t mask, int mirror)
{
}



/********************************************************************************
  FUNCTION NAME     : chan_vt_read_faultstate
  FUNCTION DETAILS  : Read fault state for the selected virtual channel.
********************************************************************************/

uint32_t chan_vt_read_faultstate(chandef* def, uint32_t* capabilities, uint32_t* mask)
{
	if (capabilities) *capabilities = 0;
	if (mask) *mask = 0;
	return(0);
}

