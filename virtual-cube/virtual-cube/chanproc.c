#include <stdint.h>
#include <limits.h>
#include <math.h>

#include "asmcode.h"
#include "porting.h"	/* _itof and _ftoi */
#include "channel.h"
#include "shared.h"		/* dspb_shared_chandata */
#include "cnet.h"


int tp_add_to_queue(uint32_t CnetTsLo, uint32_t CnetTsHi, uint32_t tpCount, uint32_t tpError);

static float integer_filter(struct simple_chandef* chan, float v)
{
	float* delta = chan->filter1.delta;					/* first delta is D1 */
	float delta_previous = chan->filter1.delta[3];	/* previous delta is D0 */
	float* coeff = chan->filter1.coeff;					/* first coefficient is B2*/

	float y = 0;
	int n;
	for (n = 0; n < 3; n++)
	{
		v = v - delta[n];
		y = y + delta[n] * (*coeff++);
		delta[n] = delta[n] + (delta_previous * (*coeff++));
		delta_previous = delta[n];
	}
	v = v - delta[n];
	y = y + delta[n] * (*coeff++);
	delta[n] = delta[n] + (v * (*coeff++));
	y = y + v * (*coeff);
	return y;
}

#define SCALE_SIM	((float)0x7FFFFFFF/1.1f)
#define SCALE_IP	(1.1f/0x40000000)		/* 1.1/(2^30) */
#define SCALE_FRAC	(1.0f/0x800000)			/* 1 / (2^23) */

static float do_linearise(struct simple_chandef* chan, int v)
{
	float* map = chan->mapptr;
	int value = v + 0x40000000;		/* convert full-scale negative to zero */
	int index = (value >> 23) & 0xFF;

	float first_entry = chan->mapptr[index];
	float second_entry = chan->mapptr[index + 1];
	float frac = (float)(value & 0x007FFFFF) * SCALE_FRAC;		/* extract fractional part - least 23 bits */
	float diff = second_entry - first_entry;
	float delta = frac * diff;
	return (delta + first_entry);
}

typedef union float_hex {
	float f;
	uint32_t h;
} float_hex;

static void zero_filter(struct simple_chandef* chan, float v)
{
	float_hex num_0, num_1, den_1;
	num_0.h = 0x3BF96A18;
	num_1.h = 0x3BF96A18;
	den_1.h = 0xBF7C1A58;

	float y = chan->zerofilt + (num_0.f * v);	/* y0 = prev + b0*x0 */
	chan->zerofiltop = y;
	chan->zerofilt = (num_1.f * v) - (den_1.f * y);
}

/* credit : https://locklessinc.com/articles/sat_arithmetic/ */
static int32_t sat_add_int32(int32_t x, int32_t y)
{
	uint32_t ux = x;
	uint32_t uy = y;
	uint32_t res = ux + uy;

	/* Calculate overflowed result. (Don't change the sign bit of ux) */
	ux = (ux >> 31) + INT_MAX;

	/* Force compiler to use cmovns instruction */
	if ((int32_t)((ux ^ uy) | ~(uy ^ res)) >= 0)
	{
		res = ux;
	}
	return res;
}

static int32_t sshl(int32_t a, uint8_t b)
{
	uint32_t mask = 0x80000000;
	int32_t res = a << b;
	if (b > 31)
		return 0;
	while (b)
	{
		mask >>= 1;
		mask |= 0x80000000;
		b--;
	}
	if ((((uint32_t)a & mask) == 0) || ((uint32_t)a & mask) == mask)
		return res;
	else
	{
		if (a > 0)
			return INT_MAX;
		else
			return INT_MIN;
	}
}

// bit numbers corresponding to flags in ctrl word
#define	BIT_ACTXDR			0	//  AC transducer type(ignored by chanproc)
#define	BIT_INVERT			1	//  Invert channel
#define	BIT_RSTUPK			2	//  Reset maximum reading
#define	BIT_ENAUTORST		3	//  Enable auto reset
#define	BIT_2PTCAL			4	//  Two - point calibration mode
//#define	BIT_LINEARISE		5	//  Linearisation table enabled
#define	BIT_UNIPOLAR		6	//  Select unipolar mode
#define	BIT_GAINSENS		7	//  Select gain / sensitivity mode(ignored by chanproc)
#define	BIT_SIMULATE		8	//  Enable simulation mode
#define	BIT_VIRTUAL			9	//  Enable virtual transducer mode
#define	BIT_RSTLPK			10	//  Reset minimum reading
#define	BIT_TXDRTYPE		11	//  Transducer type(ignored by chanproc)
								//  bits 11 - 15
								//  0 = Generic DC(load cell, strain gauge)
								//  1 = LVDT
								//  2 = LVIT
#define	BIT_ASYMMETRIC		16	//  Asymmetric transducer
#define	BIT_MAPFLUSH		18	//  Flush map cache
#define	BIT_SHUNTCAL		19	//  Shunt calibration active
#define	BIT_CTRDIRN			20	//  Cycle counter direction flag
#define	BIT_RSTCYCCTR		21	//  Reset cycle counter
#define	BIT_CYCWINDOW		22	//  Use local cyclic window value
#define	BIT_ACCOUPLED		23	//  AC coupled(4ACC txdr only)
#define	BIT_NEGSHUNTCAL		24	//  Positive or negative shunt calibration
#define	BIT_CYCEVENT		25	//  Generate event on cyclic peak detection
#define	BIT_DISABLE			26	//  Disable channel(from KO code)
#define	BIT_GBCYC			27	//  Use GenBus for cycle detection

// masks for flags in ctrl word (extra than those from channel.h)
#define	FLAG_CTRDIRN		1 << BIT_CTRDIRN		//  Cycle counter direction flag


// bit numbers corresponding to flags in the status word
#define BIT_DIRN		4
#define BIT_SATURATION	5
#define BIT_PKDET		6
#define BIT_TRDET		7

// masks for flags in status word
#define FLAG_DIRN		(1 << BIT_DIRN)
//#define FLAG_SATURATION (1 << BIT_SATURATION)
#define FLAG_PKDET		(1 << BIT_PKDET)
#define FLAG_TRDET		(1 << BIT_TRDET)


struct GenGenBus {			// Generator bus taken from Generator.h
	uint32_t xcode;
	float fade;
	float delta;
	float theta;
	float span;
	float setPointInc;
};

static inline void direction_changed(struct simple_chandef* chan, const float v, const float old_pk)
{
	uint32_t direction_flag = chan->status & FLAG_DIRN;

	if (chan->ctrl & FLAG_CYCEVENT)
		tp_add_to_queue(0, 0, _ftoi(v), 0x7FC00100);
	chan->pk = v;
	chan->status ^= FLAG_DIRN;

	// do cycle count direction and direction flag match?
	if ((chan->ctrl & FLAG_CTRDIRN) && direction_flag)
	{
		chan->meas_period = (uint32_t)((int32_t)cnet_time_lo - (int32_t)chan->prev_timer);
		chan->prev_timer = cnet_time_lo;
	}

	if (!(chan->gbptr && (chan->ctrl & FLAG_GBCYC)))
	{
		if (direction_flag)
		{
			chan->status |= FLAG_TRDET;
			chan->min = old_pk;
		}
		else
		{
			chan->status |= FLAG_PKDET;
			chan->max = old_pk;
		}
	}
}

static void cycle_detection(struct simple_chandef* chan, const float v)
{
	uint32_t direction_flag = chan->status & FLAG_DIRN;
	uint32_t peak_timeout_period = dspb_shared_chandata->cycle_timeout;

	float old_pk = chan->pk;
	float delta = old_pk - v;

	float cyc_window = (chan->ctrl & FLAG_CYCWINDOW) ? chan->cycwindow : dspb_shared_chandata->cycle_window;

	if (direction_flag == 0)
		delta = -delta;

	if (delta < 0)
		chan->pk = v;

	if (delta > cyc_window)
		direction_changed(chan, v, old_pk);

	struct GenGenBus* genbus_ptr = (struct GenGenBus*)chan->gbptr;

	if (genbus_ptr && (chan->ctrl & FLAG_GBCYC))
	{
		chan->status &= ~(FLAG_PKDET | FLAG_TRDET);		/* using GenBus for cycle detection, so clear PKDET and TRDET flags */
		if (v > chan->max) chan->max = v;
		if (v < chan->min) chan->min = v;
		//if (chan->chanid == 0)
		//	printf("cyc: max: %3.5f, min: %3.5f, v: %3.5f\n", chan->max, chan->min, v);

		if (genbus_ptr->theta < chan->prev_theta)
			chan->status |= (FLAG_PKDET | FLAG_TRDET);	/* end of cycle detected, so set PKDET and TRDET flags */
		chan->prev_theta = genbus_ptr->theta;
	}

	//;
	//; At this point the PKDET flag is set if the peak of a cycle has just been detectedand
	//	; the TRDET flag is set if the trough of a cycle has just been detected.
	//	;
	//; When using the GenBus theta value to detect cycles, both PKDETand TRDET will be set at
	//	; the end of a cycle so that both peakand trough values will be updated from the measured
	//	; maxand min values at this point.
	//	;

	if (chan->status & FLAG_PKDET)
	{
		chan->peak = chan->max;
		chan->max = v;
	}
	if (chan->status & FLAG_TRDET)
	{
		chan->trough = chan->min;
		chan->min = v;
	}

	uint32_t flag = (chan->ctrl & FLAG_CTRDIRN) ? chan->status & FLAG_TRDET : chan->status & FLAG_PKDET;
	if (flag)
		chan->cyc_counter++;

	if ((peak_timeout_period == 0) || (chan->status & (FLAG_PKDET | FLAG_TRDET)))
	{
		// no timeout
		// timeout is disabled or peak or trough has been detected
		chan->timeout = peak_timeout_period;
	}
	else
	{
		if (chan->timeout > 0)
			chan->timeout--;
		if (chan->timeout == 0)
		{
			//if (chan->chanid == 0)
			//	printf("reset\n");
			chan->peak = v;
			chan->trough = v;
		}
	}
}

static void update_channel(struct simple_chandef* chan, const float v, const uint32_t control)
{
	uint32_t combined_ctrl = chan->ctrl | control;
	if (combined_ctrl & FLAG_RSTCYCCTR)
		chan->cyc_counter = 0;

	chan->status &= ~(FLAG_PKDET | FLAG_TRDET);

	chan->maximum = (combined_ctrl & FLAG_RSTUPK) ? v : fmaxf(chan->maximum, v);
	chan->minimum = (combined_ctrl & FLAG_RSTLPK) ? v : fminf(chan->minimum, v);

	if (chan->outputptr)
		*chan->outputptr = _ftoi(v);

	chan->value = v;
	chan->filtered = v;
}

static int get_gaintrim(struct simple_chandef* chan, const float v)
{
	int gaintrim = chan->pos_gaintrim;
	if ((chan->ctrl & FLAG_2PTCAL) && (v < 0))
		gaintrim = chan->neg_gaintrim;
	return (gaintrim);
}

static int process_virtual_channel(struct simple_chandef* chan)
{
	/* gain trim calculation */
	float input = (chan->sourceptr) ? _itof(*chan->sourceptr) : 0.0f;

	/* gaintrim is in 3.29 format (integer) - convert to float */
	float gain = ((float)get_gaintrim(chan, input) / (float)(1 << 29));
	input = input * gain * SCALE_SIM;

	int filtered = (int)integer_filter(chan, input);
	return (filtered >> 1);
}

static int process_simulation_channel(struct simple_chandef* chan)
{
	float input = (chan->simptr) ? *chan->simptr : 0.0f;
	float gain = (input < 0) ? chan->simscalen : chan->simscalep;

	// force non-inverted mode in simulation mode
	uint32_t ctrl = chan->ctrl & (~FLAG_POLARITY);

	input = input * gain * SCALE_SIM;
	int filtered = (int)integer_filter(chan, input);

	filtered = sat_add_int32(filtered, chan->txdrzero);
	filtered = ((int64_t)filtered * get_gaintrim(chan, input)) >> 32;
	filtered = sshl(filtered, 2);

	int32_t uni_offset = 0;
	if (ctrl & FLAG_UNIPOLAR)
	{
		uni_offset = 0x3A2E8BA2;
		filtered = sshl(filtered, 1);
	}
	if (ctrl & FLAG_POLARITY)
	{
		filtered = -filtered;
		filtered = sat_add_int32(filtered, uni_offset);
	}
	else
	{
		filtered = sat_add_int32(filtered, -uni_offset);
	}
	if (ctrl & FLAG_ASYMMETRICAL)
		filtered = filtered >> 1;
	return (filtered);
}

static float linearise_channel(struct simple_chandef* chan, int v)
{
	int clip = 0x3fffffff;
	float output;
	if (v > clip) v = clip;
	clip = -clip;
	if (v < clip) v = clip;
	if (chan->ctrl & FLAG_LINEARISE)
		output = do_linearise(chan, v);
	else
		output = (float)v * SCALE_IP;
	return (output);
}

void inchanproc(int channel_index, int channels)
{
	struct simple_chandef* chan;
	float processed_value;
	uint32_t control;
	int tmp;

	while (channels > 0)
	{
		chan = &dspb_shared_chandata->inchaninfo[channel_index++];
		control = dspb_shared_chandata->global_control;

		if (control & FLAG_AUTORST)
		{
			if (++dspb_shared_chandata->autorst_ctr > dspb_shared_chandata->autorst_int)
			{
				control |= (FLAG_RSTLPK | FLAG_RSTUPK);
				dspb_shared_chandata->autorst_ctr = 0;
			}
		}

		if (chan->ctrl & FLAG_VIRTUAL)
		{
			tmp = process_virtual_channel(chan);
		}
		else if (chan->ctrl & FLAG_SIMULATION)
		{
			tmp = process_simulation_channel(chan);
		}
		else
		{
			/* normal channel */
			tmp = 0;
		}

		processed_value = chan->refzero + linearise_channel(chan, tmp);
		zero_filter(chan, processed_value);
		cycle_detection(chan, processed_value);
		update_channel(chan, processed_value, control);
		channels--;
	}
}

#define SCALE_OP	((2^30)/1.1f)
#define DAC_CLIP	0x7FFF
void outchanproc(void)
{
	struct simple_chandef* chan;
	int channels = dspb_shared_chandata->totaloutchan;
	int chan_index = 0;
	float value;
	int dac_output;
	while (channels > 0)
	{
		chan = &dspb_shared_chandata->outchaninfo[chan_index++];
		value = (chan->sourceptr) ? _itof(*chan->sourceptr) : chan->value;
		if (chan->ctrl & FLAG_POLARITY)
			value = -value;
		dac_output = _ftoi(value);
		value = value * SCALE_OP;

		dac_output = (((int64_t)_ftoi(value) * chan->pos_gaintrim) >> 32);
		dac_output = (dac_output + chan->offset) >> 12;
		if (dac_output > DAC_CLIP)
			dac_output = DAC_CLIP;
		if (dac_output < -DAC_CLIP)
			dac_output = -DAC_CLIP;

		if (chan->outputptr)
			*chan->outputptr = dac_output;
		channels--;
	}
}

void miscchanproc(void)
{
	struct simple_chandef* chan;
	int channels = dspb_shared_chandata->totalmiscchan;
	int chan_index = 0;
	float value;
	while (channels > 0)
	{
		chan = &dspb_shared_chandata->miscchaninfo[chan_index++];
		if (chan->sourceptr)
			value = _itof(*chan->sourceptr);
		else
			value = chan->value;
		value += chan->refzero;

		// missing dither stage
		chan->filtered = value;		/* same as SVMONITOR */
		channels--;
	}
}

void finalproc(void)
{

}
