/********************************************************************************
  MODULE NAME   	: msgtable
  PROJECT			: Control Cube / Signal Cube
  MODULE DETAILS  	: Table of message handler functions.
********************************************************************************/

#include "msgstruct.h"
#include "msghandler.h"
#include "controller.h"
#include "system.h"

/********************************************************************************

  I M P O R T A N T   N O T E

  When adding messages to the message table, please ensure that both tables
  are modified.

  The old table is further down this file.

********************************************************************************/



msgdef new_msgtable[] = 
{

	/* Error messages */

	{NULL,0},													/* Msg 0x0000	Reserved											*/
	{NULL,0},													/* Msg 0x0001	Error - Illegal message								*/
	{NULL,0},													/* Msg 0x0002	Error - Illegal channel								*/
	{NULL,0},													/* Msg 0x0003	Error - Illegal function							*/
	{NULL,0},													/* Msg 0x0004	Error - Operation failed							*/
	{NULL,0},													/* Msg 0x0005	Error - Illegal parameter							*/
	{NULL,0},													/* Msg 0x0006	Error - CNet timeout								*/
	{NULL,0},													/* Msg 0x0007	Error - Illegal destination							*/
	{NULL,0},													/* Msg 0x0008	Error - No stream									*/
	{NULL,0},													/* Msg 0x0009	Error - message too long							*/
	{NULL,0},													/* Msg 0x000A	Reserved											*/
	{NULL,0},													/* Msg 0x000B	Reserved											*/
	{NULL,0},													/* Msg 0x000C	Reserved											*/
	{NULL,0},													/* Msg 0x000D	Reserved											*/
	{NULL,0},													/* Msg 0x000E	Reserved											*/
	{NULL,0},													/* Msg 0x000F	Reserved											*/
	{NULL,0},													/* Msg 0x0010	Reserved											*/
	{NULL,0},													/* Msg 0x0011	Reserved											*/
	{NULL,0},													/* Msg 0x0012	Reserved											*/
	{NULL,0},													/* Msg 0x0013	Reserved											*/
	{NULL,0},													/* Msg 0x0014	Reserved											*/
	{NULL,0},													/* Msg 0x0015	Reserved											*/
	{NULL,0},													/* Msg 0x0016	Reserved											*/
	{NULL,0},													/* Msg 0x0017	Reserved											*/
	{NULL,0},													/* Msg 0x0018	Reserved											*/
	{NULL,0},													/* Msg 0x0019	Reserved											*/
	{NULL,0},													/* Msg 0x001A	Reserved											*/
	{NULL,0},													/* Msg 0x001B	Reserved											*/
	{NULL,0},													/* Msg 0x001C	Reserved											*/
	{NULL,0},													/* Msg 0x001D	Reserved											*/
	{NULL,0},													/* Msg 0x001E	Reserved											*/
	{NULL,0},													/* Msg 0x001F	Reserved											*/
	{NULL,0},													/* Msg 0x0020	Reserved											*/
	{NULL,0},													/* Msg 0x0021	Reserved											*/
	{NULL,0},													/* Msg 0x0022	Reserved											*/
	{NULL,0},													/* Msg 0x0023	Reserved											*/
	{NULL,0},													/* Msg 0x0024	Reserved											*/
	{NULL,0},													/* Msg 0x0025	Reserved											*/
	{NULL,0},													/* Msg 0x0026	Reserved											*/
	{NULL,0},													/* Msg 0x0027	Reserved											*/
	{NULL,0},													/* Msg 0x0028	Reserved											*/
	{NULL,0},													/* Msg 0x0029	Reserved											*/
	{NULL,0},													/* Msg 0x002A	Reserved											*/
	{NULL,0},													/* Msg 0x002B	Reserved											*/
	{NULL,0},													/* Msg 0x002C	Reserved											*/
	{NULL,0},													/* Msg 0x002D	Reserved											*/
	{NULL,0},													/* Msg 0x002E	Reserved											*/
	{NULL,0},													/* Msg 0x002F	Reserved											*/
	{NULL,0},													/* Msg 0x0030	Reserved											*/
	{NULL,0},													/* Msg 0x0031	Reserved											*/
	{NULL,0},													/* Msg 0x0032	Reserved											*/
	{NULL,0},													/* Msg 0x0033	Reserved											*/
	{NULL,0},													/* Msg 0x0034	Reserved											*/
	{NULL,0},													/* Msg 0x0035	Reserved											*/
	{NULL,0},													/* Msg 0x0036	Reserved											*/
	{NULL,0},													/* Msg 0x0037	Reserved											*/
	{NULL,0},													/* Msg 0x0038	Reserved											*/
	{NULL,0},													/* Msg 0x0039	Reserved											*/
	{NULL,0},													/* Msg 0x003A	Reserved											*/
	{NULL,0},													/* Msg 0x003B	Reserved											*/
	{NULL,0},													/* Msg 0x003C	Reserved											*/
	{NULL,0},													/* Msg 0x003D	Reserved											*/
	{NULL,0},													/* Msg 0x003E	Reserved											*/
	{NULL,0},													/* Msg 0x003F	Reserved											*/
	{NULL,0},													/* Msg 0x0040	Reserved											*/
	{NULL,0},													/* Msg 0x0041	Reserved											*/
	{NULL,0},													/* Msg 0x0042	Reserved											*/
	{NULL,0},													/* Msg 0x0043	Reserved											*/
	{NULL,0},													/* Msg 0x0044	Reserved											*/
	{NULL,0},													/* Msg 0x0045	Reserved											*/
	{NULL,0},													/* Msg 0x0046	Reserved											*/
	{NULL,0},													/* Msg 0x0047	Reserved											*/
	{NULL,0},													/* Msg 0x0048	Reserved											*/
	{NULL,0},													/* Msg 0x0049	Reserved											*/
	{NULL,0},													/* Msg 0x004A	Reserved											*/
	{NULL,0},													/* Msg 0x004B	Reserved											*/
	{NULL,0},													/* Msg 0x004C	Reserved											*/
	{NULL,0},													/* Msg 0x004D	Reserved											*/
	{NULL,0},													/* Msg 0x004E	Reserved											*/
	{NULL,0},													/* Msg 0x004F	Reserved											*/
	{NULL,0},													/* Msg 0x0050	Reserved											*/
	{NULL,0},													/* Msg 0x0051	Reserved											*/
	{NULL,0},													/* Msg 0x0052	Reserved											*/
	{NULL,0},													/* Msg 0x0053	Reserved											*/
	{NULL,0},													/* Msg 0x0054	Reserved											*/
	{NULL,0},													/* Msg 0x0055	Reserved											*/
	{NULL,0},													/* Msg 0x0056	Reserved											*/
	{NULL,0},													/* Msg 0x0057	Reserved											*/
	{NULL,0},													/* Msg 0x0058	Reserved											*/
	{NULL,0},													/* Msg 0x0059	Reserved											*/
	{NULL,0},													/* Msg 0x005A	Reserved											*/
	{NULL,0},													/* Msg 0x005B	Reserved											*/
	{NULL,0},													/* Msg 0x005C	Reserved											*/
	{NULL,0},													/* Msg 0x005D	Reserved											*/
	{NULL,0},													/* Msg 0x005E	Reserved											*/
	{NULL,0},													/* Msg 0x005F	Reserved											*/
	{NULL,0},													/* Msg 0x0060	Reserved											*/
	{NULL,0},													/* Msg 0x0061	Reserved											*/	
	{NULL,0},													/* Msg 0x0062	Reserved											*/
	{NULL,0},													/* Msg 0x0063	Reserved											*/
	{NULL,0},													/* Msg 0x0064	Reserved											*/
	{NULL,0},													/* Msg 0x0065	Reserved											*/
	{NULL,0},													/* Msg 0x0066	Reserved											*/
	{NULL,0},													/* Msg 0x0067	Reserved											*/
	{NULL,0},													/* Msg 0x0068	Reserved											*/
	{NULL,0},													/* Msg 0x0069	Reserved											*/
	{NULL,0},													/* Msg 0x006A	Reserved											*/
	{NULL,0},													/* Msg 0x006B	Reserved											*/
	{NULL,0},													/* Msg 0x006C	Reserved											*/
	{NULL,0},													/* Msg 0x006D	Reserved											*/
	{NULL,0},													/* Msg 0x006E	Reserved											*/
	{NULL,0},													/* Msg 0x006F	Reserved											*/
	{NULL,0},													/* Msg 0x0070	Reserved											*/
	{NULL,0},													/* Msg 0x0071	Reserved											*/
	{NULL,0},													/* Msg 0x0072	Reserved											*/	
	{NULL,0},													/* Msg 0x0073	Reserved											*/
	{NULL,0},													/* Msg 0x0074	Reserved											*/
	{NULL,0},													/* Msg 0x0075	Reserved											*/
	{NULL,0},													/* Msg 0x0076	Reserved											*/
	{NULL,0},													/* Msg 0x0077	Reserved											*/
	{NULL,0},													/* Msg 0x0078	Reserved											*/
	{NULL,0},													/* Msg 0x0079	Reserved											*/
	{NULL,0},													/* Msg 0x007A	Reserved											*/
	{NULL,0},													/* Msg 0x007B	Reserved											*/
	{NULL,0},													/* Msg 0x007C	Reserved											*/
	{NULL,0},													/* Msg 0x007D	Reserved											*/
	{NULL,0},													/* Msg 0x007E	Reserved											*/
	{NULL,0},													/* Msg 0x007F	Reserved											*/
	{NULL,0},													/* Msg 0x0080	Reserved											*/
	{NULL,0},													/* Msg 0x0081	Reserved											*/
	{NULL,0},													/* Msg 0x0082	Reserved											*/
	{NULL,0},													/* Msg 0x0083	Reserved											*/
	{NULL,0},													/* Msg 0x0084	Reserved											*/
	{NULL,0},													/* Msg 0x0085	Reserved											*/
	{NULL,0},													/* Msg 0x0086	Reserved											*/
	{NULL,0},													/* Msg 0x0087	Reserved											*/
	{NULL,0},													/* Msg 0x0088	Reserved											*/
	{NULL,0},													/* Msg 0x0089	Reserved											*/
	{NULL,0},													/* Msg 0x008A	Reserved											*/
	{NULL,0},													/* Msg 0x008B	Reserved											*/
	{NULL,0},													/* Msg 0x008C	Reserved											*/
	{NULL,0},													/* Msg 0x008D	Reserved											*/
	{NULL,0},													/* Msg 0x008E	Reserved											*/
	{NULL,0},													/* Msg 0x008F	Reserved											*/
	{NULL,0},													/* Msg 0x0090	Reserved											*/
	{NULL,0},													/* Msg 0x0091	Reserved											*/
	{NULL,0},													/* Msg 0x0092	Reserved											*/
	{NULL,0},													/* Msg 0x0093	Reserved											*/
	{NULL,0},													/* Msg 0x0094	Reserved											*/
	{NULL,0},													/* Msg 0x0095	Reserved											*/
	{NULL,0},													/* Msg 0x0096	Reserved											*/
	{NULL,0},													/* Msg 0x0097	Reserved											*/
	{NULL,0},													/* Msg 0x0098	Reserved											*/
	{NULL,0},													/* Msg 0x0099	Reserved											*/
	{NULL,0},													/* Msg 0x009A	Reserved											*/
	{NULL,0},													/* Msg 0x009B	Reserved											*/
	{NULL,0},													/* Msg 0x009C	Reserved											*/
	{NULL,0},													/* Msg 0x009D	Reserved											*/
	{NULL,0},													/* Msg 0x009E	Reserved											*/
	{NULL,0},													/* Msg 0x009F	Reserved											*/
	{NULL,0},													/* Msg 0x00A0	Reserved											*/
	{NULL,0},													/* Msg 0x00A1	Reserved											*/
	{NULL,0},													/* Msg 0x00A2	Reserved											*/
	{NULL,0},													/* Msg 0x00A3	Reserved											*/
	{NULL,0},													/* Msg 0x00A4	Reserved											*/
	{NULL,0},													/* Msg 0x00A5	Reserved											*/
	{NULL,0},													/* Msg 0x00A6	Reserved											*/
	{NULL,0},													/* Msg 0x00A7	Reserved											*/
	{NULL,0},													/* Msg 0x00A8	Reserved											*/
	{NULL,0},													/* Msg 0x00A9	Reserved											*/
	{NULL,0},													/* Msg 0x00AA	Reserved											*/
	{NULL,0},													/* Msg 0x00AB	Reserved											*/
	{NULL,0},													/* Msg 0x00AC	Reserved											*/
	{NULL,0},													/* Msg 0x00AD	Reserved											*/
	{NULL,0},													/* Msg 0x00AE	Reserved											*/
	{NULL,0},													/* Msg 0x00AF	Reserved											*/
	{NULL,0},													/* Msg 0x00B0	Reserved											*/
	{NULL,0},													/* Msg 0x00B1	Reserved											*/
	{NULL,0},													/* Msg 0x00B2	Reserved											*/
	{NULL,0},													/* Msg 0x00B3	Reserved											*/
	{NULL,0},													/* Msg 0x00B4	Reserved											*/
	{NULL,0},													/* Msg 0x00B5	Reserved											*/
	{NULL,0},													/* Msg 0x00B6	Reserved											*/
	{NULL,0},													/* Msg 0x00B7	Reserved											*/
	{NULL,0},													/* Msg 0x00B8	Reserved											*/	
	{NULL,0},													/* Msg 0x00B9	Reserved											*/
	{NULL,0},													/* Msg 0x00BA	Reserved											*/
	{NULL,0},													/* Msg 0x00BB	Reserved											*/
	{NULL,0},													/* Msg 0x00BC	Reserved											*/
	{NULL,0},													/* Msg 0x00BD	Reserved											*/
	{NULL,0},													/* Msg 0x00BE	Reserved											*/
	{NULL,0},													/* Msg 0x00BF	Reserved											*/
	{NULL,0},													/* Msg 0x00C0	Reserved											*/
	{NULL,0},													/* Msg 0x00C1	Reserved											*/
	{NULL,0},													/* Msg 0x00C2	Reserved											*/
	{NULL,0},													/* Msg 0x00C3	Reserved											*/
	{NULL,0},													/* Msg 0x00C4	Reserved											*/
	{NULL,0},													/* Msg 0x00C5	Reserved											*/
	{NULL,0},													/* Msg 0x00C6	Reserved											*/
	{NULL,0},													/* Msg 0x00C7	Reserved											*/
	{NULL,0},													/* Msg 0x00C8	Reserved											*/
	{NULL,0},													/* Msg 0x00C9	Reserved											*/
	{NULL,0},													/* Msg 0x00CA	Reserved											*/
	{NULL,0},													/* Msg 0x00CB	Reserved											*/
	{NULL,0},													/* Msg 0x00CC	Reserved											*/
	{NULL,0},													/* Msg 0x00CD	Reserved											*/
	{NULL,0},													/* Msg 0x00CE	Reserved											*/
	{NULL,0},													/* Msg 0x00CF	Reserved											*/
	{NULL,0},													/* Msg 0x00D0	Reserved											*/
	{NULL,0},													/* Msg 0x00D1	Reserved											*/
	{NULL,0},													/* Msg 0x00D2	Reserved											*/
	{NULL,0},													/* Msg 0x00D3	Reserved											*/
	{NULL,0},													/* Msg 0x00D4	Reserved											*/
	{NULL,0},													/* Msg 0x00D5	Reserved											*/
	{NULL,0},													/* Msg 0x00D6	Reserved											*/
	{NULL,0},													/* Msg 0x00D7	Reserved											*/
	{NULL,0},													/* Msg 0x00D8	Reserved											*/
	{NULL,0},													/* Msg 0x00D9	Reserved											*/
	{NULL,0},													/* Msg 0x00DA	Reserved											*/
	{NULL,0},													/* Msg 0x00DB	Reserved											*/
	{NULL,0},													/* Msg 0x00DC	Reserved											*/
	{NULL,0},													/* Msg 0x00DD	Reserved											*/
	{NULL,0},													/* Msg 0x00DE	Reserved											*/
	{NULL,0},													/* Msg 0x00DF	Reserved											*/
	{NULL,0},													/* Msg 0x00E0	Reserved											*/
	{NULL,0},													/* Msg 0x00E1	Reserved											*/
	{NULL,0},													/* Msg 0x00E2	Reserved											*/
	{NULL,0},													/* Msg 0x00E3	Reserved											*/
	{NULL,0},													/* Msg 0x00E4	Reserved											*/
	{NULL,0},													/* Msg 0x00E5	Reserved											*/	
	{NULL,0},													/* Msg 0x00E6	Reserved											*/
	{NULL,0},													/* Msg 0x00E7	Reserved											*/
	{NULL,0},													/* Msg 0x00E8	Reserved											*/
	{NULL,0},													/* Msg 0x00E9	Reserved											*/
	{NULL,0},													/* Msg 0x00EA	Reserved											*/
	{NULL,0},													/* Msg 0x00EB	Reserved											*/
	{NULL,0},													/* Msg 0x00EC	Reserved											*/
	{NULL,0},													/* Msg 0x00ED	Reserved											*/
	{NULL,0},													/* Msg 0x00EE	Reserved											*/
	{NULL,0},													/* Msg 0x00EF	Reserved											*/
	{NULL,0},													/* Msg 0x00F0	Reserved											*/
	{NULL,0},													/* Msg 0x00F1	Reserved											*/
	{NULL,0},													/* Msg 0x00F2	Reserved											*/
	{NULL,0},													/* Msg 0x00F3	Reserved											*/
	{NULL,0},													/* Msg 0x00F4	Reserved											*/
	{NULL,0},													/* Msg 0x00F5	Reserved											*/
	{NULL,0},													/* Msg 0x00F6	Reserved											*/
	{NULL,0},													/* Msg 0x00F7	Reserved											*/
	{NULL,0},													/* Msg 0x00F8	Reserved											*/
	{NULL,0},													/* Msg 0x00F9	Reserved											*/
	{NULL,0},													/* Msg 0x00FA	Reserved											*/
	{NULL,0},													/* Msg 0x00FB	Reserved											*/
	{NULL,0},													/* Msg 0x00FC	Reserved											*/
	{NULL,0},													/* Msg 0x00FD	Reserved											*/
	{NULL,0},													/* Msg 0x00FE	Reserved											*/
	{NULL,0},													/* Msg 0x00FF	Reserved											*/
	
	/* Loader messages */
	
	{fn_read_firmware_version,0},								/* Msg 0x0100	Read firmware version								*/
	{NULL,0},													/* Msg 0x0101	Return firmware version								*/
	{fn_read_device_type,0},									/* Msg 0x0102	Read device type									*/
	{NULL,0},													/* Msg 0x0103	Return device type									*/
	{fn_erase_flash,0},											/* Msg 0x0104	Erase flash											*/
	{fn_read_erase_status,0},									/* Msg 0x0105	Read erase status									*/
	{NULL,0},													/* Msg 0x0106	Return erase status									*/
	{fn_prog_flash,0},											/* Msg 0x0107	Program flash										*/
	{NULL,0},													/* Msg 0x0108	Return program status								*/
	{fn_erase_flash_block,0},									/* Msg 0x0109	Erase flash block									*/
	{fn_read_cnet_info,0},										/* Msg 0x010A	Read CNet info										*/
	{NULL,0},													/* Msg 0x010B	Return CNet info									*/
	{fn_set_ip_address,0},										/* Msg 0x010C	Set IP address										*/
	{fn_read_ip_address,0},										/* Msg 0x010D	Read IP address										*/
	{NULL,0},													/* Msg 0x010E	Return IP address									*/
	{fn_hw_restart,0},											/* Msg 0x010F	Restart hardware									*/
	{fn_fw_validate,0},											/* Msg 0x0110	Validate firmware									*/
	{NULL,0},													/* Msg 0x0111	Return firmware validation state					*/
	{fn_testmode_set_address,0},								/* Msg 0x0112	Set address											*/
	{fn_testmode_write8,0},										/* Msg 0x0113	Write 8-bit data									*/
	{fn_testmode_write16,0},									/* Msg 0x0114	Write 16-bit data									*/
	{fn_testmode_write32,0},									/* Msg 0x0115	Write 32-bit data									*/
	{fn_testmode_read8,0},										/* Msg 0x0116	Read 8-bit data										*/
	{NULL,0},													/* Msg 0x0117	Return 8-bit data									*/
	{fn_testmode_read16,0},										/* Msg 0x0118	Read 16-bit data									*/
	{NULL,0},													/* Msg 0x0119	Return 16-bit data									*/
	{fn_testmode_read32,0},										/* Msg 0x011A	Read 32-bit data									*/
	{NULL,0},													/* Msg 0x011B	Return 32-bit data									*/
	{fn_testmode_i2c_start,0},									/* Msg 0x011C	Generate I2C start									*/
	{fn_testmode_i2c_restart,0},								/* Msg 0x011D	Generate I2C restart								*/
	{fn_testmode_i2c_stop,0},									/* Msg 0x011E	Generate I2C stop									*/
	{fn_testmode_i2c_send,0},									/* Msg 0x011F	Send I2C data										*/
	{NULL,0},													/* Msg 0x0120	Return I2C send ack									*/
	{fn_testmode_i2c_read,0},									/* Msg 0x0121	Read I2C data										*/
	{NULL,0},													/* Msg 0x0122	Return I2C data										*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_testmode_dspb_i2c_start,0},								/* Msg 0x0123	Generate DSPB I2C start								*/
	{fn_testmode_dspb_i2c_restart,0},							/* Msg 0x0124	Generate DSPB I2C restart							*/
	{fn_testmode_dspb_i2c_stop,0},								/* Msg 0x0125	Generate DSPB I2C stop								*/
	{fn_testmode_dspb_i2c_send,0},								/* Msg 0x0126	Send DSPB I2C data									*/
	{NULL,0},													/* Msg 0x0127	Return DSPB I2C ack									*/
	{fn_testmode_dspb_i2c_read,0},								/* Msg 0x0128	Read DSPB I2C data									*/
	{NULL,0},													/* Msg 0x0129	Return DSPB I2C data								*/
#else
	{NULL,0},													/* Msg 0x0123	Generate DSPB I2C start								*/
	{NULL,0},													/* Msg 0x0124	Generate DSPB I2C restart							*/
	{NULL,0},													/* Msg 0x0125	Generate DSPB I2C stop								*/
	{NULL,0},													/* Msg 0x0126	Send DSPB I2C data									*/
	{NULL,0},													/* Msg 0x0127	Return DSPB I2C ack									*/
	{NULL,0},													/* Msg 0x0128	Read DSPB I2C data									*/
	{NULL,0},													/* Msg 0x0129	Return DSPB I2C data								*/
#endif
	{fn_testmode_onew_init,0},									/* Msg 0x012A	Initialise 1-wire interface							*/
	{fn_testmode_onew_reset,0},									/* Msg 0x012B	Reset 1-wire interface								*/
	{NULL,0},													/* Msg 0x012C	Return 1-wire reset status							*/
	{fn_testmode_onew_send,0},									/* Msg 0x012D	Send data via 1-wire interface						*/
	{fn_testmode_onew_read,0},									/* Msg 0x012E	Read data via 1-wire interface						*/
	{NULL,0},													/* Msg 0x012F	Return 1-wire data									*/
	{fn_testmode_enable_watchdog,0},							/* Msg 0x0130	Enable watchdog										*/
	{fn_testmode_disable_watchdog,0},							/* Msg 0x0131	Disable watchdog									*/
	{fn_read_mac_address,0},									/* Msg 0x0132	Read MAC address									*/
	{NULL,0},													/* Msg 0x0133	Return MAC address									*/
	{fn_read_firmware_information,0},							/* Msg 0x0134	Read firmware information							*/
	{NULL,0},													/* Msg 0x0135	Return firmware information							*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_testmode_write32b,0},									/* Msg 0x0136	Write DSP B 32-bit data								*/
	{fn_testmode_read32b,0},									/* Msg 0x0137	Read DSP B 32-bit data								*/
	{NULL,0},													/* Msg 0x0138	Return DSP B 32-bit data							*/
#else
	{NULL,0},													/* Msg 0x0136	Write DSP B 32-bit data								*/
	{NULL,0},													/* Msg 0x0137	Read DSP B 32-bit data								*/
	{NULL,0},													/* Msg 0x0138	Return DSP B 32-bit data							*/
#endif
	{NULL,0},													/* Msg 0x0139	Reserved											*/
	{NULL,0},													/* Msg 0x013A	Reserved											*/
	{NULL,0},													/* Msg 0x013B	Reserved											*/
	{NULL,0},													/* Msg 0x013C	Reserved											*/
	{NULL,0},													/* Msg 0x013D	Reserved											*/
	{NULL,0},													/* Msg 0x013E	Reserved											*/
	{NULL,0},													/* Msg 0x013F	Reserved											*/
	{NULL,0},													/* Msg 0x0140	Reserved											*/
	{NULL,0},													/* Msg 0x0141	Reserved											*/
	{NULL,0},													/* Msg 0x0142	Reserved											*/
	{NULL,0},													/* Msg 0x0143	Reserved											*/
	{NULL,0},													/* Msg 0x0144	Reserved											*/
	{NULL,0},													/* Msg 0x0145	Reserved											*/
	{NULL,0},													/* Msg 0x0146	Reserved											*/
	{NULL,0},													/* Msg 0x0147	Reserved											*/
	{NULL,0},													/* Msg 0x0148	Reserved											*/
	{NULL,0},													/* Msg 0x0149	Reserved											*/
	{NULL,0},													/* Msg 0x014A	Reserved											*/
	{NULL,0},													/* Msg 0x014B	Reserved											*/
	{NULL,0},													/* Msg 0x014C	Reserved											*/
	{NULL,0},													/* Msg 0x014D	Reserved											*/
	{NULL,0},													/* Msg 0x014E	Reserved											*/
	{NULL,0},													/* Msg 0x014F	Reserved											*/
	{NULL,0},													/* Msg 0x0150	Reserved											*/
	{NULL,0},													/* Msg 0x0151	Reserved											*/
	{NULL,0},													/* Msg 0x0152	Reserved											*/
	{NULL,0},													/* Msg 0x0153	Reserved											*/
	{NULL,0},													/* Msg 0x0154	Reserved											*/
	{NULL,0},													/* Msg 0x0155	Reserved											*/
	{NULL,0},													/* Msg 0x0156	Reserved											*/
	{NULL,0},													/* Msg 0x0157	Reserved											*/
	{NULL,0},													/* Msg 0x0158	Reserved											*/
	{NULL,0},													/* Msg 0x0159	Reserved											*/
	{NULL,0},													/* Msg 0x015A	Reserved											*/
	{NULL,0},													/* Msg 0x015B	Reserved											*/
	{NULL,0},													/* Msg 0x015C	Reserved											*/
	{NULL,0},													/* Msg 0x015D	Reserved											*/
	{NULL,0},													/* Msg 0x015E	Reserved											*/
	{NULL,0},													/* Msg 0x015F	Reserved											*/
	{NULL,0},													/* Msg 0x0160	Reserved											*/
	{NULL,0},													/* Msg 0x0161	Reserved											*/
	{NULL,0},													/* Msg 0x0162	Reserved											*/
	{NULL,0},													/* Msg 0x0163	Reserved											*/
	{NULL,0},													/* Msg 0x0164	Reserved											*/
	{NULL,0},													/* Msg 0x0165	Reserved											*/
	{NULL,0},													/* Msg 0x0166	Reserved											*/
	{NULL,0},													/* Msg 0x0167	Reserved											*/
	{NULL,0},													/* Msg 0x0168	Reserved											*/
	{NULL,0},													/* Msg 0x0169	Reserved											*/
	{NULL,0},													/* Msg 0x016A	Reserved											*/
	{NULL,0},													/* Msg 0x016B	Reserved											*/
	{NULL,0},													/* Msg 0x016C	Reserved											*/
	{NULL,0},													/* Msg 0x016D	Reserved											*/
	{NULL,0},													/* Msg 0x016E	Reserved											*/
	{NULL,0},													/* Msg 0x016F	Reserved											*/
	{NULL,0},													/* Msg 0x0170	Reserved											*/
	{NULL,0},													/* Msg 0x0171	Reserved											*/
	{NULL,0},													/* Msg 0x0172	Reserved											*/
	{NULL,0},													/* Msg 0x0173	Reserved											*/
	{NULL,0},													/* Msg 0x0174	Reserved											*/
	{NULL,0},													/* Msg 0x0175	Reserved											*/
	{NULL,0},													/* Msg 0x0176	Reserved											*/
	{NULL,0},													/* Msg 0x0177	Reserved											*/
	{NULL,0},													/* Msg 0x0178	Reserved											*/
	{NULL,0},													/* Msg 0x0179	Reserved											*/
	{NULL,0},													/* Msg 0x017A	Reserved											*/
	{NULL,0},													/* Msg 0x017B	Reserved											*/
	{NULL,0},													/* Msg 0x017C	Reserved											*/
	{NULL,0},													/* Msg 0x017D	Reserved											*/
	{NULL,0},													/* Msg 0x017E	Reserved											*/
	{NULL,0},													/* Msg 0x017F	Reserved											*/
	{NULL,0},													/* Msg 0x0180	Reserved											*/
	{NULL,0},													/* Msg 0x0181	Reserved											*/
	{NULL,0},													/* Msg 0x0182	Reserved											*/
	{NULL,0},													/* Msg 0x0183	Reserved											*/
	{NULL,0},													/* Msg 0x0184	Reserved											*/
	{NULL,0},													/* Msg 0x0185	Reserved											*/
	{NULL,0},													/* Msg 0x0186	Reserved											*/
	{NULL,0},													/* Msg 0x0187	Reserved											*/
	{NULL,0},													/* Msg 0x0188	Reserved											*/
	{NULL,0},													/* Msg 0x0189	Reserved											*/
	{NULL,0},													/* Msg 0x018A	Reserved											*/
	{NULL,0},													/* Msg 0x018B	Reserved											*/
	{NULL,0},													/* Msg 0x018C	Reserved											*/
	{NULL,0},													/* Msg 0x018D	Reserved											*/
	{NULL,0},													/* Msg 0x018E	Reserved											*/
	{NULL,0},													/* Msg 0x018F	Reserved											*/
	{NULL,0},													/* Msg 0x0190	Reserved											*/
	{NULL,0},													/* Msg 0x0191	Reserved											*/
	{NULL,0},													/* Msg 0x0192	Reserved											*/
	{NULL,0},													/* Msg 0x0193	Reserved											*/
	{NULL,0},													/* Msg 0x0194	Reserved											*/
	{NULL,0},													/* Msg 0x0195	Reserved											*/
	{NULL,0},													/* Msg 0x0196	Reserved											*/
	{NULL,0},													/* Msg 0x0197	Reserved											*/
	{NULL,0},													/* Msg 0x0198	Reserved											*/
	{NULL,0},													/* Msg 0x0199	Reserved											*/
	{NULL,0},													/* Msg 0x019A	Reserved											*/
	{NULL,0},													/* Msg 0x019B	Reserved											*/
	{NULL,0},													/* Msg 0x019C	Reserved											*/
	{NULL,0},													/* Msg 0x019D	Reserved											*/
	{NULL,0},													/* Msg 0x019E	Reserved											*/
	{NULL,0},													/* Msg 0x019F	Reserved											*/
	{NULL,0},													/* Msg 0x01A0	Reserved											*/
	{NULL,0},													/* Msg 0x01A1	Reserved											*/
	{NULL,0},													/* Msg 0x01A2	Reserved											*/
	{NULL,0},													/* Msg 0x01A3	Reserved											*/
	{NULL,0},													/* Msg 0x01A4	Reserved											*/
	{NULL,0},													/* Msg 0x01A5	Reserved											*/
	{NULL,0},													/* Msg 0x01A6	Reserved											*/
	{NULL,0},													/* Msg 0x01A7	Reserved											*/
	{NULL,0},													/* Msg 0x01A8	Reserved											*/
	{NULL,0},													/* Msg 0x01A9	Reserved											*/
	{NULL,0},													/* Msg 0x01AA	Reserved											*/
	{NULL,0},													/* Msg 0x01AB	Reserved											*/
	{NULL,0},													/* Msg 0x01AC	Reserved											*/
	{NULL,0},													/* Msg 0x01AD	Reserved											*/
	{NULL,0},													/* Msg 0x01AE	Reserved											*/
	{NULL,0},													/* Msg 0x01AF	Reserved											*/
	{NULL,0},													/* Msg 0x01B0	Reserved											*/
	{NULL,0},													/* Msg 0x01B1	Reserved											*/
	{NULL,0},													/* Msg 0x01B2	Reserved											*/
	{NULL,0},													/* Msg 0x01B3	Reserved											*/
	{NULL,0},													/* Msg 0x01B4	Reserved											*/
	{NULL,0},													/* Msg 0x01B5	Reserved											*/
	{NULL,0},													/* Msg 0x01B6	Reserved											*/
	{NULL,0},													/* Msg 0x01B7	Reserved											*/
	{NULL,0},													/* Msg 0x01B8	Reserved											*/
	{NULL,0},													/* Msg 0x01B9	Reserved											*/
	{NULL,0},													/* Msg 0x01BA	Reserved											*/
	{NULL,0},													/* Msg 0x01BB	Reserved											*/
	{NULL,0},													/* Msg 0x01BC	Reserved											*/
	{NULL,0},													/* Msg 0x01BD	Reserved											*/
	{NULL,0},													/* Msg 0x01BE	Reserved											*/
	{NULL,0},													/* Msg 0x01BF	Reserved											*/
	{NULL,0},													/* Msg 0x01C0	Reserved											*/
	{NULL,0},													/* Msg 0x01C1	Reserved											*/
	{NULL,0},													/* Msg 0x01C2	Reserved											*/
	{NULL,0},													/* Msg 0x01C3	Reserved											*/
	{NULL,0},													/* Msg 0x01C4	Reserved											*/
	{NULL,0},													/* Msg 0x01C5	Reserved											*/
	{NULL,0},													/* Msg 0x01C6	Reserved											*/
	{NULL,0},													/* Msg 0x01C7	Reserved											*/
	{NULL,0},													/* Msg 0x01C8	Reserved											*/
	{NULL,0},													/* Msg 0x01C9	Reserved											*/
	{NULL,0},													/* Msg 0x01CA	Reserved											*/
	{NULL,0},													/* Msg 0x01CB	Reserved											*/
	{NULL,0},													/* Msg 0x01CC	Reserved											*/
	{NULL,0},													/* Msg 0x01CD	Reserved											*/
	{NULL,0},													/* Msg 0x01CE	Reserved											*/
	{NULL,0},													/* Msg 0x01CF	Reserved											*/
	{NULL,0},													/* Msg 0x01D0	Reserved											*/
	{NULL,0},													/* Msg 0x01D1	Reserved											*/
	{NULL,0},													/* Msg 0x01D2	Reserved											*/
	{NULL,0},													/* Msg 0x01D3	Reserved											*/
	{NULL,0},													/* Msg 0x01D4	Reserved											*/
	{NULL,0},													/* Msg 0x01D5	Reserved											*/
	{NULL,0},													/* Msg 0x01D6	Reserved											*/
	{NULL,0},													/* Msg 0x01D7	Reserved											*/
	{NULL,0},													/* Msg 0x01D8	Reserved											*/
	{NULL,0},													/* Msg 0x01D9	Reserved											*/
	{NULL,0},													/* Msg 0x01DA	Reserved											*/
	{NULL,0},													/* Msg 0x01DB	Reserved											*/
	{NULL,0},													/* Msg 0x01DC	Reserved											*/
	{NULL,0},													/* Msg 0x01DD	Reserved											*/
	{NULL,0},													/* Msg 0x01DE	Reserved											*/
	{NULL,0},													/* Msg 0x01DF	Reserved											*/
	{NULL,0},													/* Msg 0x01E0	Reserved											*/
	{NULL,0},													/* Msg 0x01E1	Reserved											*/
	{NULL,0},													/* Msg 0x01E2	Reserved											*/
	{NULL,0},													/* Msg 0x01E3	Reserved											*/
	{NULL,0},													/* Msg 0x01E4	Reserved											*/
	{NULL,0},													/* Msg 0x01E5	Reserved											*/
	{NULL,0},													/* Msg 0x01E6	Reserved											*/
	{NULL,0},													/* Msg 0x01E7	Reserved											*/
	{NULL,0},													/* Msg 0x01E8	Reserved											*/
	{NULL,0},													/* Msg 0x01E9	Reserved											*/
	{NULL,0},													/* Msg 0x01EA	Reserved											*/
	{NULL,0},													/* Msg 0x01EB	Reserved											*/
	{NULL,0},													/* Msg 0x01EC	Reserved											*/
	{NULL,0},													/* Msg 0x01ED	Reserved											*/
	{NULL,0},													/* Msg 0x01EE	Reserved											*/
	{NULL,0},													/* Msg 0x01EF	Reserved											*/
	{NULL,0},													/* Msg 0x01F0	Reserved											*/
	{NULL,0},													/* Msg 0x01F1	Reserved											*/
	{NULL,0},													/* Msg 0x01F2	Reserved											*/
	{NULL,0},													/* Msg 0x01F3	Reserved											*/
	{NULL,0},													/* Msg 0x01F4	Reserved											*/
	{NULL,0},													/* Msg 0x01F5	Reserved											*/
	{NULL,0},													/* Msg 0x01F6	Reserved											*/
	{NULL,0},													/* Msg 0x01F7	Reserved											*/
	{NULL,0},													/* Msg 0x01F8	Reserved											*/
	{NULL,0},													/* Msg 0x01F9	Reserved											*/
	{NULL,0},													/* Msg 0x01FA	Reserved											*/
	{NULL,0},													/* Msg 0x01FB	Reserved											*/
	{NULL,0},													/* Msg 0x01FC	Reserved											*/
	{NULL,0},													/* Msg 0x01FD	Reserved											*/
	{NULL,0},													/* Msg 0x01FE	Reserved											*/
	{NULL,0},  													/* Msg 0x01FF	Reserved											*/
	            
#ifndef LOADER	
	            
	/* Parameter class messages */

	{MsgGetHandle,0},											/* Msg 0x0200	Obtain parameter handle								*/
	{MsgGetParType,0},											/* Msg 0x0201	Determine parameter type							*/
	{MsgGetParFlags,0},											/* Msg 0x0202 	Determine parameter flags							*/
	{MsgGetParMin4,0},											/* Msg 0x0203	Determine parameter minimum (payload<=4)			*/
	{MsgGetParMax4,0},											/* Msg 0x0204	Determine parameter maximum (payload<=4)			*/
	{MsgSetParValue4,0},										/* Msg 0x0205	Set parameter value (payload<=4)					*/
	{MsgGetParValue4,0},										/* Msg 0x0206	Get parameter value (payload<=4)					*/
	{MsgSetParValue256,0},										/* Msg 0x0207	Set parameter value (payload<=256)					*/
	{MsgGetParValue256,0},										/* Msg 0x0208	Get parameter value (payload<=256)					*/
	{MsgParSpecial256,0},										/* Msg 0x0209	Perform parameter special function (payload<=256)	*/
	{MsgGetParMember256,0},										/* Msg 0x020A	Get parameter sub-member name (payload<=256)		*/
	{fn_send_parameter_string,0},								/* Msg 0x020B	Send parameter string								*/
	{NULL,0},													/* Msg 0x020C	Return parameter string								*/
	{MsgGetParLive4,0},											/* Msg 0x020D	Get live parameter value							*/
	{MsgGetParUnits256,0},										/* Msg 0x020E	Get parameter units									*/
	{MsgAbortAdjustment,0},										/* Msg 0x020F	Abort adjustment									*/
	{MsgGetParAddr4,0},											/* Msg 0x0210	Get parameter DSPB address							*/
	{MsgSetEvent,0},											/* Msg 0x0211	Set parameter event report options					*/
	{MsgSetInstant,0},											/* Msg 0x0212	Set parameter value instant							*/
	{NULL,0},													/* Msg 0x0213	Reserved											*/
	{NULL,0},													/* Msg 0x0214	Reserved											*/
	{NULL,0},													/* Msg 0x0215	Reserved											*/
	{NULL,0},													/* Msg 0x0216	Reserved											*/
	{NULL,0},													/* Msg 0x0217	Reserved											*/
	{NULL,0},													/* Msg 0x0218	Reserved											*/
	{NULL,0},													/* Msg 0x0219	Reserved											*/
	{NULL,0},													/* Msg 0x021A	Reserved											*/
	{NULL,0},													/* Msg 0x021B	Reserved											*/
	{NULL,0},													/* Msg 0x021C	Reserved											*/
	{NULL,0},													/* Msg 0x021D	Reserved											*/
	{NULL,0},													/* Msg 0x021E	Reserved											*/
	{NULL,0},													/* Msg 0x021F	Reserved											*/
	{NULL,0},													/* Msg 0x0220	Reserved											*/
	{NULL,0},													/* Msg 0x0221	Reserved											*/
	{NULL,0},													/* Msg 0x0222	Reserved											*/
	{NULL,0},													/* Msg 0x0223	Reserved											*/
	{NULL,0},													/* Msg 0x0224	Reserved											*/
	{NULL,0},													/* Msg 0x0225	Reserved											*/
	{NULL,0},													/* Msg 0x0226	Reserved											*/
	{NULL,0},													/* Msg 0x0227	Reserved											*/
	{NULL,0},													/* Msg 0x0228	Reserved											*/
	{NULL,0},													/* Msg 0x0229	Reserved											*/
	{NULL,0},													/* Msg 0x022A	Reserved											*/
	{NULL,0},													/* Msg 0x022B	Reserved											*/
	{NULL,0},													/* Msg 0x022C	Reserved											*/
	{NULL,0},													/* Msg 0x022D	Reserved											*/
	{NULL,0},													/* Msg 0x022E	Reserved											*/
	{NULL,0},													/* Msg 0x022F	Reserved											*/
	{NULL,0},													/* Msg 0x0230	Reserved											*/
	{NULL,0},													/* Msg 0x0231	Reserved											*/
	{NULL,0},													/* Msg 0x0232	Reserved											*/
	{NULL,0},													/* Msg 0x0233	Reserved											*/
	{NULL,0},													/* Msg 0x0234	Reserved											*/
	{NULL,0},													/* Msg 0x0235	Reserved											*/
	{NULL,0},													/* Msg 0x0236	Reserved											*/
	{NULL,0},													/* Msg 0x0237	Reserved											*/
	{NULL,0},													/* Msg 0x0238	Reserved											*/
	{NULL,0},													/* Msg 0x0239	Reserved											*/
	{NULL,0},													/* Msg 0x023A	Reserved											*/
	{NULL,0},													/* Msg 0x023B	Reserved											*/
	{NULL,0},													/* Msg 0x023C	Reserved											*/
	{NULL,0},													/* Msg 0x023D	Reserved											*/
	{NULL,0},													/* Msg 0x023E	Reserved											*/
	{NULL,0},													/* Msg 0x023F	Reserved											*/
	{NULL,0},													/* Msg 0x0240	Reserved											*/
	{NULL,0},													/* Msg 0x0241	Reserved											*/
	{NULL,0},													/* Msg 0x0242	Reserved											*/
	{NULL,0},													/* Msg 0x0243	Reserved											*/
	{NULL,0},													/* Msg 0x0244	Reserved											*/
	{NULL,0},													/* Msg 0x0245	Reserved											*/
	{NULL,0},													/* Msg 0x0246	Reserved											*/
	{NULL,0},													/* Msg 0x0247	Reserved											*/
	{NULL,0},													/* Msg 0x0248	Reserved											*/
	{NULL,0},													/* Msg 0x0249	Reserved											*/
	{NULL,0},													/* Msg 0x024A	Reserved											*/
	{NULL,0},													/* Msg 0x024B	Reserved											*/
	{NULL,0},													/* Msg 0x024C	Reserved											*/
	{NULL,0},													/* Msg 0x024D	Reserved											*/
	{NULL,0},													/* Msg 0x024E	Reserved											*/
	{NULL,0},													/* Msg 0x024F	Reserved											*/
	{NULL,0},													/* Msg 0x0250	Reserved											*/
	{NULL,0},													/* Msg 0x0251	Reserved											*/
	{NULL,0},													/* Msg 0x0252	Reserved											*/
	{NULL,0},													/* Msg 0x0253	Reserved											*/
	{NULL,0},													/* Msg 0x0254	Reserved											*/
	{NULL,0},													/* Msg 0x0255	Reserved											*/
	{NULL,0},													/* Msg 0x0256	Reserved											*/
	{NULL,0},													/* Msg 0x0257	Reserved											*/
	{NULL,0},													/* Msg 0x0258	Reserved											*/
	{NULL,0},													/* Msg 0x0259	Reserved											*/
	{NULL,0},													/* Msg 0x025A	Reserved											*/
	{NULL,0},													/* Msg 0x025B	Reserved											*/
	{NULL,0},													/* Msg 0x025C	Reserved											*/
	{NULL,0},													/* Msg 0x025D	Reserved											*/
	{NULL,0},													/* Msg 0x025E	Reserved											*/
	{NULL,0},													/* Msg 0x025F	Reserved											*/
	{NULL,0},													/* Msg 0x0260	Reserved											*/
	{NULL,0},													/* Msg 0x0261	Reserved											*/
	{NULL,0},													/* Msg 0x0262	Reserved											*/
	{NULL,0},													/* Msg 0x0263	Reserved											*/
	{NULL,0},													/* Msg 0x0264	Reserved											*/
	{NULL,0},													/* Msg 0x0265	Reserved											*/
	{NULL,0},													/* Msg 0x0266	Reserved											*/
	{NULL,0},													/* Msg 0x0267	Reserved											*/
	{NULL,0},													/* Msg 0x0268	Reserved											*/
	{NULL,0},													/* Msg 0x0269	Reserved											*/
	{NULL,0},													/* Msg 0x026A	Reserved											*/
	{NULL,0},													/* Msg 0x026B	Reserved											*/
	{NULL,0},													/* Msg 0x026C	Reserved											*/
	{NULL,0},													/* Msg 0x026D	Reserved											*/
	{NULL,0},													/* Msg 0x026E	Reserved											*/
	{NULL,0},													/* Msg 0x026F	Reserved											*/
	{NULL,0},													/* Msg 0x0270	Reserved											*/
	{NULL,0},													/* Msg 0x0271	Reserved											*/
	{NULL,0},													/* Msg 0x0272	Reserved											*/
	{NULL,0},													/* Msg 0x0273	Reserved											*/
	{NULL,0},													/* Msg 0x0274	Reserved											*/
	{NULL,0},													/* Msg 0x0275	Reserved											*/
	{NULL,0},													/* Msg 0x0276	Reserved											*/
	{NULL,0},													/* Msg 0x0277	Reserved											*/
	{NULL,0},													/* Msg 0x0278	Reserved											*/
	{NULL,0},													/* Msg 0x0279	Reserved											*/
	{NULL,0},													/* Msg 0x027A	Reserved											*/
	{NULL,0},													/* Msg 0x027B	Reserved											*/
	{NULL,0},													/* Msg 0x027C	Reserved											*/
	{NULL,0},													/* Msg 0x027D	Reserved											*/
	{NULL,0},													/* Msg 0x027E	Reserved											*/
	{NULL,0},													/* Msg 0x027F	Reserved											*/
	{NULL,0},													/* Msg 0x0280	Reserved											*/
	{NULL,0},													/* Msg 0x0281	Reserved											*/
	{NULL,0},													/* Msg 0x0282	Reserved											*/
	{NULL,0},													/* Msg 0x0283	Reserved											*/
	{NULL,0},													/* Msg 0x0284	Reserved											*/
	{NULL,0},													/* Msg 0x0285	Reserved											*/
	{NULL,0},													/* Msg 0x0286	Reserved											*/
	{NULL,0},													/* Msg 0x0287	Reserved											*/
	{NULL,0},													/* Msg 0x0288	Reserved											*/
	{NULL,0},													/* Msg 0x0289	Reserved											*/
	{NULL,0},													/* Msg 0x028A	Reserved											*/
	{NULL,0},													/* Msg 0x028B	Reserved											*/
	{NULL,0},													/* Msg 0x028C	Reserved											*/
	{NULL,0},													/* Msg 0x028D	Reserved											*/
	{NULL,0},													/* Msg 0x028E	Reserved											*/
	{NULL,0},													/* Msg 0x028F	Reserved											*/
	{NULL,0},													/* Msg 0x0290	Reserved											*/
	{NULL,0},													/* Msg 0x0291	Reserved											*/
	{NULL,0},													/* Msg 0x0292	Reserved											*/
	{NULL,0},													/* Msg 0x0293	Reserved											*/
	{NULL,0},													/* Msg 0x0294	Reserved											*/
	{NULL,0},													/* Msg 0x0295	Reserved											*/
	{NULL,0},													/* Msg 0x0296	Reserved											*/
	{NULL,0},													/* Msg 0x0297	Reserved											*/
	{NULL,0},													/* Msg 0x0298	Reserved											*/
	{NULL,0},													/* Msg 0x0299	Reserved											*/
	{NULL,0},													/* Msg 0x029A	Reserved											*/
	{NULL,0},													/* Msg 0x029B	Reserved											*/
	{NULL,0},													/* Msg 0x029C	Reserved											*/
	{NULL,0},													/* Msg 0x029D	Reserved											*/
	{NULL,0},													/* Msg 0x029E	Reserved											*/
	{NULL,0},													/* Msg 0x029F	Reserved											*/
	{NULL,0},													/* Msg 0x02A0	Reserved											*/
	{NULL,0},													/* Msg 0x02A1	Reserved											*/
	{NULL,0},													/* Msg 0x02A2	Reserved											*/
	{NULL,0},													/* Msg 0x02A3	Reserved											*/
	{NULL,0},													/* Msg 0x02A4	Reserved											*/
	{NULL,0},													/* Msg 0x02A5	Reserved											*/
	{NULL,0},													/* Msg 0x02A6	Reserved											*/
	{NULL,0},													/* Msg 0x02A7	Reserved											*/
	{NULL,0},													/* Msg 0x02A8	Reserved											*/
	{NULL,0},													/* Msg 0x02A9	Reserved											*/
	{NULL,0},													/* Msg 0x02AA	Reserved											*/
	{NULL,0},													/* Msg 0x02AB	Reserved											*/
	{NULL,0},													/* Msg 0x02AC	Reserved											*/
	{NULL,0},													/* Msg 0x02AD	Reserved											*/
	{NULL,0},													/* Msg 0x02AE	Reserved											*/
	{NULL,0},													/* Msg 0x02AF	Reserved											*/
	{NULL,0},													/* Msg 0x02B0	Reserved											*/
	{NULL,0},													/* Msg 0x02B1	Reserved											*/
	{NULL,0},													/* Msg 0x02B2	Reserved											*/
	{NULL,0},													/* Msg 0x02B3	Reserved											*/
	{NULL,0},													/* Msg 0x02B4	Reserved											*/
	{NULL,0},													/* Msg 0x02B5	Reserved											*/
	{NULL,0},													/* Msg 0x02B6	Reserved											*/
	{NULL,0},													/* Msg 0x02B7	Reserved											*/
	{NULL,0},													/* Msg 0x02B8	Reserved											*/
	{NULL,0},													/* Msg 0x02B9	Reserved											*/
	{NULL,0},													/* Msg 0x02BA	Reserved											*/
	{NULL,0},													/* Msg 0x02BB	Reserved											*/
	{NULL,0},													/* Msg 0x02BC	Reserved											*/
	{NULL,0},													/* Msg 0x02BD	Reserved											*/
	{NULL,0},													/* Msg 0x02BE	Reserved											*/
	{NULL,0},													/* Msg 0x02BF	Reserved											*/
	{NULL,0},													/* Msg 0x02C0	Reserved											*/
	{NULL,0},													/* Msg 0x02C1	Reserved											*/
	{NULL,0},													/* Msg 0x02C2	Reserved											*/
	{NULL,0},													/* Msg 0x02C3	Reserved											*/
	{NULL,0},													/* Msg 0x02C4	Reserved											*/
	{NULL,0},													/* Msg 0x02C5	Reserved											*/
	{NULL,0},													/* Msg 0x02C6	Reserved											*/
	{NULL,0},													/* Msg 0x02C7	Reserved											*/
	{NULL,0},													/* Msg 0x02C8	Reserved											*/
	{NULL,0},													/* Msg 0x02C9	Reserved											*/
	{NULL,0},													/* Msg 0x02CA	Reserved											*/
	{NULL,0},  													/* Msg 0x02CB	Reserved											*/
	{NULL,0},													/* Msg 0x02CC	Reserved											*/
	{NULL,0},													/* Msg 0x02CD	Reserved											*/
	{NULL,0},													/* Msg 0x02CE	Reserved											*/
	{NULL,0},													/* Msg 0x02CF	Reserved											*/
	{NULL,0},													/* Msg 0x02D0	Reserved											*/
	{NULL,0},													/* Msg 0x02D1	Reserved											*/
	{NULL,0},													/* Msg 0x02D2	Reserved											*/
	{NULL,0},													/* Msg 0x02D3	Reserved											*/
	{NULL,0},													/* Msg 0x02D4	Reserved											*/
	{NULL,0},													/* Msg 0x02D5	Reserved											*/
	{NULL,0},													/* Msg 0x02D6	Reserved											*/
	{NULL,0},													/* Msg 0x02D7	Reserved											*/
	{NULL,0},													/* Msg 0x02D8	Reserved											*/
	{NULL,0},													/* Msg 0x02D9	Reserved											*/
	{NULL,0},													/* Msg 0x02DA	Reserved											*/
	{NULL,0},													/* Msg 0x02DB	Reserved											*/
	{NULL,0},													/* Msg 0x02DC	Reserved											*/
	{NULL,0},													/* Msg 0x02DD	Reserved											*/
	{NULL,0},													/* Msg 0x02DE	Reserved											*/
	{NULL,0},													/* Msg 0x02DF	Reserved											*/
	{NULL,0},													/* Msg 0x02E0	Reserved											*/
	{NULL,0},													/* Msg 0x02E1	Reserved											*/
	{NULL,0},													/* Msg 0x02E2	Reserved											*/
	{NULL,0},													/* Msg 0x02E3	Reserved											*/
	{NULL,0},													/* Msg 0x02E4	Reserved											*/
	{NULL,0},													/* Msg 0x02E5	Reserved											*/
	{NULL,0},													/* Msg 0x02E6	Reserved											*/
	{NULL,0},													/* Msg 0x02E7	Reserved											*/
	{NULL,0},													/* Msg 0x02E8	Reserved											*/
	{NULL,0},													/* Msg 0x02E9	Reserved											*/
	{NULL,0},													/* Msg 0x02EA	Reserved											*/
	{NULL,0},													/* Msg 0x02EB	Reserved											*/
	{NULL,0},													/* Msg 0x02EC	Reserved											*/
	{NULL,0},													/* Msg 0x02ED	Reserved											*/
	{NULL,0},													/* Msg 0x02EE	Reserved											*/
	{NULL,0},													/* Msg 0x02EF	Reserved											*/
	{NULL,0},													/* Msg 0x02F0	Reserved											*/
	{NULL,0},													/* Msg 0x02F1	Reserved											*/
	{NULL,0},													/* Msg 0x02F2	Reserved											*/
	{NULL,0},													/* Msg 0x02F3	Reserved											*/
	{NULL,0},													/* Msg 0x02F4	Reserved											*/
	{NULL,0},													/* Msg 0x02F5	Reserved											*/
	{NULL,0},													/* Msg 0x02F6	Reserved											*/
	{NULL,0},													/* Msg 0x02F7	Reserved											*/
	{NULL,0},													/* Msg 0x02F8	Reserved											*/
	{NULL,0},													/* Msg 0x02F9	Reserved											*/
	{NULL,0},													/* Msg 0x02FA	Reserved											*/
	{NULL,0},													/* Msg 0x02FB	Reserved											*/
	{NULL,0},													/* Msg 0x02FC	Reserved											*/
	{NULL,0},													/* Msg 0x02FD	Reserved											*/
	{NULL,0},													/* Msg 0x02FE	Reserved											*/
	{NULL,0},													/* Msg 0x02FF	Reserved											*/
	
	/* Standard messages */
	
	{fn_set_excitation,SECURITY_CALIBRATOR},					/* Msg 0x0300	Set excitation										*/
	{fn_read_excitation,0},										/* Msg 0x0301	Read excitation										*/
	{NULL,0},													/* Msg 0x0302	Return excitation									*/
	{fn_set_gain,SECURITY_CALIBRATOR},							/* Msg 0x0303	Set gain											*/
	{fn_read_gain,0},											/* Msg 0x0304	Read gain											*/
	{NULL,0},													/* Msg 0x0305	Return gain											*/
	{NULL,0},													/* Msg 0x0306	Not used											*/
	{fn_set_cal,SECURITY_OPERATOR},								/* Msg 0x0307	Set shunt calibration state							*/
	{fn_set_led,SECURITY_OPERATOR},								/* Msg 0x0308	Set LED state										*/
	{fn_set_filter,SECURITY_OPERATOR},							/* Msg 0x0309	Set filter											*/
	{fn_read_filter,0},											/* Msg 0x030A	Read filter											*/
	{NULL,0},													/* Msg 0x030B	Return filter										*/
	{fn_read_ip_channel,0},										/* Msg 0x030C	Read input channel									*/
	{NULL,0},													/* Msg 0x030D	Return input channel								*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_write_op_channel,SECURITY_ADMINISTRATOR},				/* Msg 0x030E	Write output channel								*/
	{fn_set_hydraulic_output,SECURITY_ADMINISTRATOR},			/* Msg 0x030F	Set hydraulic output								*/
	{fn_read_hydraulic_output,0},								/* Msg 0x0310	Read hydraulic output								*/
	{NULL,0},													/* Msg 0x0311	Return hydraulic output								*/
	{fn_read_hydraulic_input,0},								/* Msg 0x0312	Read hydraulic input								*/
	{NULL,0},													/* Msg 0x0313	Return hydraulic input								*/
#else
	{NULL,0},													/* Msg 0x030E	Write output channel								*/
	{NULL,0},													/* Msg 0x030F	Set hydraulic output								*/
	{NULL,0},													/* Msg 0x0310	Read hydraulic output								*/
	{NULL,0},													/* Msg 0x0311	Return hydraulic output								*/
	{NULL,0},													/* Msg 0x0312	Read hydraulic input								*/
	{NULL,0},													/* Msg 0x0313	Return hydraulic input								*/
#endif
	{fn_read_slot_configuration,0},								/* Msg 0x0314	Read slot configuration								*/
	{NULL,0},													/* Msg 0x0315	Return slot configuration							*/
	{fn_read_chan_information,0},								/* Msg 0x0316	Read channel information							*/
	{NULL,0},													/* Msg 0x0317	Return channel information							*/
	{fn_set_raw_gain,SECURITY_ADMINISTRATOR},					/* Msg 0x0318	Set raw gain										*/
	{fn_read_raw_gain,0},										/* Msg 0x0319	Read raw gain										*/
	{NULL,0},													/* Msg 0x031A	Return raw gain										*/
	{fn_set_calgain,SECURITY_ADMINISTRATOR},					/* Msg 0x031B	Set calibration gain table value					*/
	{fn_read_calgain,0},										/* Msg 0x031C	Read calibration gain table value					*/
	{NULL,0},													/* Msg 0x031D	Return calibration gain table value					*/
	{fn_set_caloffset,SECURITY_ADMINISTRATOR},					/* Msg 0x031E	Set calibration offset value						*/
	{fn_read_caloffset,0},										/* Msg 0x031F	Read calibration offset value 						*/
	{NULL,0},													/* Msg 0x0320	Return calibration offset value						*/
	{fn_save_chan_config,SECURITY_CALIBRATOR},					/* Msg 0x0321	Save channel configuration							*/
	{fn_restore_chan_config,SECURITY_CALIBRATOR},				/* Msg 0x0322	Restore channel configuration						*/
	{fn_save_chan_calibration,SECURITY_ADMINISTRATOR},			/* Msg 0x0323	Save channel calibration							*/
	{fn_restore_chan_calibration,SECURITY_ADMINISTRATOR},		/* Msg 0x0324	Restore channel calibration							*/
	{fn_read_cnet_channel,0},									/* Msg 0x0325	Read CNet channel									*/
	{NULL,0},													/* Msg 0x0326	Return CNet channel									*/
	{fn_set_cnet_slot,0},										/* Msg 0x0327	Set CNet slot										*/
	{NULL,0},													/* Msg 0x0328	Not used											*/
	{fn_read_cnet_status,0},									/* Msg 0x0329	Read CNet status									*/
	{NULL,0},													/* Msg 0x032A	Return CNet status									*/
	{fn_set_exc_cal,SECURITY_CALIBRATOR},						/* Msg 0x032B	Set excitation calibration							*/
	{fn_read_exc_cal,0},										/* Msg 0x032C	Read excitation	calibration							*/
	{NULL,0},													/* Msg 0x032D	Return excitation calibration						*/
	{fn_set_gaintrim,SECURITY_CALIBRATOR},						/* Msg 0x032E	Set gain trim value									*/
	{fn_read_gaintrim,0},										/* Msg 0x032F	Read gain trim value 								*/
	{NULL,0},													/* Msg 0x0330	Return gain trim value								*/
	{fn_flush_chan_config,SECURITY_ADMINISTRATOR},				/* Msg 0x0331	Flush channel configuration							*/
	{fn_flush_chan_calibration,SECURITY_ADMINISTRATOR},			/* Msg 0x0332	Flush channel calibration							*/
	{fn_read_chan_status,0},									/* Msg 0x0333	Read channel status									*/
	{NULL,0},													/* Msg 0x0334	Return channel status								*/
	{NULL,0},													/* Msg 0x0335	Set channel limit value								*/
	{NULL,0},													/* Msg 0x0336	Read channel limit value							*/
	{NULL,0},													/* Msg 0x0337	Return channel limit value							*/
	{NULL,0},													/* Msg 0x0338	Clear channel limit flags							*/
	{fn_set_chan_flags,SECURITY_CALIBRATOR},					/* Msg 0x0339	Set channel flags									*/
	{fn_read_chan_flags,0},										/* Msg 0x033A	Read channel flags									*/
	{NULL,0},													/* Msg 0x033B	Return channel flags								*/
	{fn_read_peaks,0},											/* Msg 0x033C	Read channel peaks									*/
	{NULL,0},													/* Msg 0x033D	Return channel peaks								*/
	{fn_reset_peaks,0},											/* Msg 0x033E	Reset channel peak readers							*/
	{fn_write_map,SECURITY_CALIBRATOR},							/* Msg 0x033F	Write linearisation map entry						*/
	{fn_read_map,0},											/* Msg 0x0340	Read linearisation map entry						*/
	{NULL,0},													/* Msg 0x0341	Return linearisation map entry						*/
	{fn_set_refzero,SECURITY_OPERATOR},							/* Msg 0x0342	Write channel reference zero value					*/
	{fn_read_refzero,0},										/* Msg 0x0343	Read channel reference zero value					*/
	{NULL,0},													/* Msg 0x0344	Return channel reference zero value					*/

#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_set_sv_range,SECURITY_CALIBRATOR},						/* Msg 0x0345	Set SV current range								*/
	{fn_read_sv_range,0},										/* Msg 0x0346	Read SV current range								*/
	{NULL,0},													/* Msg 0x0347	Return SV current range								*/
	{fn_define_hydraulic_io,SECURITY_CALIBRATOR},				/* Msg 0x0348	Set hydraulic I/O definition						*/
	{fn_read_hydraulic_io,0},									/* Msg 0x0349	Read hydraulic I/O definition						*/
	{NULL,0},													/* Msg 0x034A	Return hydraulic I/O definition						*/
	{fn_set_raw_current,SECURITY_ADMINISTRATOR},				/* Msg 0x034B	Set raw current adjustment							*/
	{fn_read_hydraulic_state,0},								/* Msg 0x034C	Read hydraulic state								*/
	{NULL,0},													/* Msg 0x034D	Return hydraulic state								*/
#else
	{NULL,0},													/* Msg 0x0345	Set SV current range								*/
	{NULL,0},													/* Msg 0x0346	Read SV current range								*/
	{NULL,0},													/* Msg 0x0347	Return SV current range								*/
	{NULL,0},													/* Msg 0x0348	Set hydraulic I/O definition						*/
	{NULL,0},													/* Msg 0x0349	Read hydraulic I/O definition						*/
	{NULL,0},													/* Msg 0x034A	Return hydraulic I/O definition						*/
	{NULL,0},													/* Msg 0x034B	Set raw current adjustment							*/
	{NULL,0},													/* Msg 0x034C	Read hydraulic state								*/
	{NULL,0},													/* Msg 0x034D	Return hydraulic state								*/
#endif

	{fn_set_raw_excitation,SECURITY_ADMINISTRATOR},				/* Msg 0x034E	Set raw excitation									*/

#if (defined(CONTROLCUBE) || defined(AICUBE))	
	{fn_hydraulic_control,SECURITY_CALIBRATOR},					/* Msg 0x034F	Control hydraulic state								*/
#else
	{NULL,0},													/* Msg 0x034F	Control hydraulic state								*/
#endif
	{fn_read_sysinfo,SECURITY_OPERATOR},						/* Msg 0x0350	Read system information								*/
	{NULL,0},													/* Msg 0x0351	Return system information							*/

	{fn_set_caltable,SECURITY_ADMINISTRATOR},					/* Msg 0x0352	Set calibration table value							*/
	{fn_read_caltable,0},										/* Msg 0x0353	Read calibration table value						*/
	{NULL,0},													/* Msg 0x0354	Return calibration table value						*/

	{fn_read_serno,0},											/* Msg 0x0355	Read controller serial number						*/
	{NULL,0},													/* Msg 0x0356	Return serial number								*/
	{fn_define_output_list,SECURITY_OPERATOR},					/* Msg 0x0357	Define realtime output channel list					*/
	{fn_define_input_list,0},									/* Msg 0x0358	Define realtime input channel list					*/
	{fn_read_total_buffer,0},									/* Msg 0x0359	Read total buffer space								*/
	{NULL,0},													/* Msg 0x035A	Return total buffer space							*/
	{fn_read_output_buffer,0},									/* Msg 0x035B	Read output buffer space							*/
	{NULL,0},													/* Msg 0x035C	Return output buffer space							*/
	{fn_read_input_buffer,0},									/* Msg 0x035D	Read input buffer space								*/
	{NULL,0},													/* Msg 0x035E	Return input buffer space							*/
	{fn_define_output_buffer,SECURITY_OPERATOR},				/* Msg 0x035F	Define output buffer space							*/
	{fn_define_input_buffer,SECURITY_OPERATOR},					/* Msg 0x0360	Define input buffer space							*/
	{fn_read_buffer_status,0},									/* Msg 0x0361	Read buffer status									*/
	{NULL,0},													/* Msg 0x0362	Return buffer status								*/
	{fn_clear_error_flags,SECURITY_OPERATOR},					/* Msg 0x0363	Clear error flags									*/
	{fn_control_realtime_transfer,SECURITY_OPERATOR},			/* Msg 0x0364	Control realtime transfer							*/
	{fn_pause_realtime_transfer,SECURITY_OPERATOR},				/* Msg 0x0365	Pause realtime transfer								*/
	{fn_resume_realtime_transfer,SECURITY_OPERATOR},			/* Msg 0x0366	Resume realtime transfer							*/
	{fn_transfer_realtime_data,SECURITY_OPERATOR},				/* Msg 0x0367	Transfer realtime data								*/
	{NULL,0},													/* Msg 0x0368	Return realtime data								*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_send_kinet_message,SECURITY_OPERATOR},					/* Msg 0x0369	Send KiNet message									*/
	{NULL,0},													/* Msg 0x036A	Return KiNet message								*/
#else
	{NULL,0},													/* Msg 0x0369	Send KiNet message									*/
	{NULL,0},													/* Msg 0x036A	Return KiNet message								*/
#endif
	{fn_define_system_name,SECURITY_CALIBRATOR},				/* Msg 0x036B	Define system name									*/
	{fn_read_system_name,0},									/* Msg 0x036C	Read system name									*/
	{NULL,0},													/* Msg 0x036D	Return system name									*/
	{fn_define_comm_timeout,SECURITY_OPERATOR},					/* Msg 0x036E	Define comm timeout									*/
	{fn_read_comm_timeout,0},									/* Msg 0x036F	Read comm timeout									*/
	{NULL,0},													/* Msg 0x0370	Return comm timeout									*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_set_kinet_mapping,SECURITY_OPERATOR},					/* Msg 0x0371	Set KiNet mapping									*/
	{fn_read_kinet_mapping,0},									/* Msg 0x0372	Read KiNet mapping									*/
	{NULL,0},													/* Msg 0x0373	Return KiNet mapping								*/
#else
	{NULL,0},													/* Msg 0x0371	Set KiNet mapping									*/
	{NULL,0},													/* Msg 0x0372	Read KiNet mapping									*/
	{NULL,0},													/* Msg 0x0373	Return KiNet mapping								*/
#endif
	{NULL,0},													/* Msg 0x0374	Not used											*/
	{NULL,0},													/* Msg 0x0375	Not used											*/
	{NULL,0},													/* Msg 0x0376	Not used											*/
	{fn_set_cnet_maptable,SECURITY_OPERATOR},					/* Msg 0x0377	Set CNet map table									*/
	{fn_read_cnet_maptable,0},									/* Msg 0x0378	Read CNet map table									*/
	{NULL,0},													/* Msg 0x0379	Return CNet map table								*/
	{fn_set_rt_stop_type,SECURITY_OPERATOR},					/* Msg 0x037A	Set realtime stop type								*/
	{fn_read_rt_stop_type,0},									/* Msg 0x037B	Read realtime stop type								*/
	{NULL,0},													/* Msg 0x037C	Return realtime stop type							*/
	{fn_set_rt_stop_parameters,SECURITY_OPERATOR},				/* Msg 0x037D	Set realtime stop parameters						*/
	{fn_read_rt_stop_parameters,0},								/* Msg 0x037E	Read realtime stop parameters						*/
	{NULL,0},													/* Msg 0x037F	Return realtime stop parameters						*/
	{NULL,0},													/* Msg 0x0380	Set limit persistance								*/
	{NULL,0},													/* Msg 0x0381	Read limit persistance								*/
	{NULL,0},													/* Msg 0x0382	Return limit persistance							*/
	{NULL,0},													/* Msg 0x0383	Set limit action									*/
	{NULL,0},													/* Msg 0x0384	Read limit action									*/
	{NULL,0},													/* Msg 0x0385	Return limit action									*/
	{NULL,0},													/* Msg 0x0386	Enable limits										*/
	{NULL,0},													/* Msg 0x0387	Disable limits										*/
	{NULL,0},													/* Msg 0x0388	Read limit enable									*/
	{NULL,0},													/* Msg 0x0389	Return limit enable									*/
	{fn_reset_all_peaks,SECURITY_OPERATOR},						/* Msg 0x038A	Reset all peaks										*/
	{fn_set_reset_parameters,SECURITY_OPERATOR},				/* Msg 0x038B	Set reset parameters								*/
	{fn_read_reset_parameters,0},								/* Msg 0x038C	Read reset parameters								*/
	{NULL,0},													/* Msg 0x038D	Return reset parameters								*/
	{fn_set_sv_dither,SECURITY_CALIBRATOR},						/* Msg 0x038E	Set dither											*/
	{fn_read_sv_dither,0},										/* Msg 0x038F	Read dither											*/
	{NULL,0},													/* Msg 0x0390	Return dither										*/
	{fn_set_sv_balance,SECURITY_CALIBRATOR},					/* Msg 0x0391	Set valve balance									*/
	{fn_read_sv_balance,0},										/* Msg 0x0392	Read valve balance									*/
	{NULL,0},													/* Msg 0x0393	Return valve balance								*/
	{fn_read_chan_memptrs,0},									/* Msg 0x0394	Read chan definition memory pointers				*/
	{NULL,0},													/* Msg 0x0395	Return chan definition memory pointers				*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_set_kinet_flags,SECURITY_OPERATOR},						/* Msg 0x0396	Set KiNet flags										*/
	{fn_read_kinet_flags,0},									/* Msg 0x0397	Read KiNet flags									*/
	{NULL,0},													/* Msg 0x0398	Return KiNet flags									*/
#else
	{NULL,0},													/* Msg 0x0396	Set KiNet flags										*/
	{NULL,0},													/* Msg 0x0397	Read KiNet flags									*/
	{NULL,0},													/* Msg 0x0398	Return KiNet flags									*/
#endif
	{fn_set_realtime_config,SECURITY_OPERATOR},					/* Msg 0x0399	Set realtime config									*/
	{fn_read_realtime_config,0},								/* Msg 0x039A	Read realtime config								*/
	{NULL,0},													/* Msg 0x039B	Return realtime config								*/
	{fn_read_output_list,0},									/* Msg 0x039C	Read realtime output channel list					*/
	{NULL,0},													/* Msg 0x039D	Return realtime output channel list					*/
	{fn_read_input_list,0},										/* Msg 0x039E	Read realtime input channel list					*/
	{NULL,0},													/* Msg 0x039F	Return realtime input channel list					*/
	{fn_refzero_zero,SECURITY_OPERATOR},						/* Msg 0x03A0	Reference zero										*/
	{fn_set_serno,SECURITY_ADMINISTRATOR},						/* Msg 0x03A1	Set controller serial number						*/
	{fn_read_nv_state,0},										/* Msg 0x03A2	Read NV state										*/
	{NULL,0},													/* Msg 0x03A3	Return NV state										*/
	{fn_set_panel_config,SECURITY_ADMINISTRATOR},				/* Msg 0x03A4	Set panel configuration table						*/
	{fn_read_panel_config,0},									/* Msg 0x03A5	Read panel configuration table						*/
	{NULL,0},													/* Msg 0x03A6	Return panel configuration table					*/
	{fn_nv_control,SECURITY_ADMINISTRATOR},						/* Msg 0x03A7	Control NV operation								*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_define_hydraulic_mode,SECURITY_CALIBRATOR},				/* Msg 0x03A8	Define hydraulic mode								*/
	{fn_read_hydraulic_mode,0},									/* Msg 0x03A9	Read hydraulic mode									*/
	{NULL,0},													/* Msg 0x03AA	Return hydraulic mode								*/

	{fn_set_digio_info,SECURITY_OPERATOR},						/* Msg 0x03AB	Set digital I/O info								*/
	{fn_read_digio_info,0},										/* Msg 0x03AC	Read digital I/O info								*/
	{NULL,0},													/* Msg 0x03AD	Return digital I/O info								*/
	{fn_write_digio_output,SECURITY_OPERATOR},					/* Msg 0x03AE	Write digital I/O output							*/
	{fn_read_digio_input,0},									/* Msg 0x03AF	Read digital I/O input								*/
	{NULL,0},													/* Msg 0x03B0	Return digital I/O input							*/
	{fn_read_digio_output,0},									/* Msg 0x03B1	Read digital I/O output								*/
	{NULL,0},													/* Msg 0x03B2	Return digital I/O output							*/
#else
	{NULL,0},													/* Msg 0x03A8	Define hydraulic mode								*/
	{NULL,0},													/* Msg 0x03A9	Read hydraulic mode									*/
	{NULL,0},													/* Msg 0x03AA	Return hydraulic mode								*/

	{NULL,0},													/* Msg 0x03AB	Set digital I/O info								*/
	{NULL,0},													/* Msg 0x03AC	Read digital I/O info								*/
	{NULL,0},													/* Msg 0x03AD	Return digital I/O info								*/
	{NULL,0},													/* Msg 0x03AE	Write digital I/O output							*/
	{NULL,0},													/* Msg 0x03AF	Read digital I/O input								*/
	{NULL,0},													/* Msg 0x03B0	Return digital I/O input							*/
	{NULL,0},													/* Msg 0x03B1	Read digital I/O output								*/
	{NULL,0},													/* Msg 0x03B2	Return digital I/O output							*/
#endif

#ifdef SIGNALCUBE
	{fn_set_gauge_type,SECURITY_CALIBRATOR},					/* Msg 0x03B3	Set gauge type (Signal Cube only)					*/
	{fn_read_gauge_type,SECURITY_OPERATOR},						/* Msg 0x03B4	Read gauge type	(Signal Cube only)					*/
	{NULL,0},													/* Msg 0x03B5	Return gauge type (Signal Cube only)				*/
	{fn_set_offset,SECURITY_OPERATOR},							/* Msg 0x03B6	Set offset (Signal Cube only)						*/
	{fn_read_offset,SECURITY_OPERATOR},							/* Msg 0x03B7	Read offset	(Signal Cube only)						*/
	{NULL,0},													/* Msg 0x03B8	Return offset (Signal Cube only)					*/
#else
	{NULL,0},													/* Msg 0x03B3	Set gauge type (Signal Cube only)					*/
	{NULL,0},													/* Msg 0x03B4	Read gauge type	(Signal Cube only)					*/
	{NULL,0},													/* Msg 0x03B5	Return gauge type (Signal Cube only)				*/
	{NULL,0},													/* Msg 0x03B6	Set offset (Signal Cube only)						*/
	{NULL,0},													/* Msg 0x03B7	Read offset	(Signal Cube only)						*/
	{NULL,0},													/* Msg 0x03B8	Return offset (Signal Cube only)					*/
#endif

	{fn_chan_override,SECURITY_ADMINISTRATOR},		 			/* Msg 0x03B9	Channel override									*/
	{fn_define_rig_name,SECURITY_CALIBRATOR},					/* Msg 0x03BA	Define rig name										*/
	{fn_read_rig_name,0},										/* Msg 0x03BB	Read rig name										*/
	{NULL,0},													/* Msg 0x03BC	Return rig name										*/
#ifdef SIGNALCUBE
	{fn_set_active_gauges,SECURITY_CALIBRATOR},					/* Msg 0x03BD	Set active gauge count (Signal Cube only)			*/
	{fn_read_active_gauges,SECURITY_OPERATOR},					/* Msg 0x03BE	Read active gauge count	(Signal Cube only)			*/
	{NULL,0},													/* Msg 0x03BF	Return active gauge count (Signal Cube only)		*/
#else
	{NULL,0},													/* Msg 0x03BD	Set active gauge count (Signal Cube only)			*/
	{NULL,0},													/* Msg 0x03BE	Read active gauge count	(Signal Cube only)			*/
	{NULL,0},													/* Msg 0x03BF	Return active gauge count (Signal Cube only)		*/
#endif
	{fn_read_eventlog_status,0},								/* Msg 0x03C0	Read eventlog status								*/
	{NULL,0},													/* Msg 0x03C1	Return eventlog status								*/
	{fn_flush_eventlog,0},										/* Msg 0x03C2	Flush eventlog										*/
	{fn_read_eventlog_entry,0},									/* Msg 0x03C3	Read eventlog entry									*/
	{NULL,0},													/* Msg 0x03C4	Return eventlog entry								*/
	{fn_read_timestamp,0},										/* Msg 0x03C5	Read timestamp										*/
	{NULL,0},													/* Msg 0x03C6	Return timestamp									*/

	{fn_read_cnet_valid_range,0},								/* Msg 0x03C7	Read CNet valid range								*/
	{NULL,0},													/* Msg 0x03C8	Return CNet valid range								*/
	
	{fn_set_txdrzero,SECURITY_CALIBRATOR},						/* Msg 0x03C9	Set transducer zero value							*/
	{fn_read_txdrzero,0},										/* Msg 0x03CA	Read transducer zero value							*/
	{NULL,0},													/* Msg 0x03CB	Return transducer zero value						*/
	{fn_txdrzero_zero,SECURITY_CALIBRATOR},						/* Msg 0x03CC	Zero transducer										*/

	{fn_set_filter_freq,0},										/* Msg 0x03CD	Set filter frequency								*/
	
	{fn_set_security_level,0},									/* Msg 0x03CE	Set security level									*/
	{fn_read_security_level,0},									/* Msg 0x03CF	Read security level									*/
	{NULL,0},													/* Msg 0x03D0	Return security level								*/

#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_set_hydraulic_dissipation,SECURITY_CALIBRATOR},			/* Msg 0x03D1	Set hydraulic dissipation time						*/
	{fn_read_hydraulic_dissipation,0},							/* Msg 0x03D2	Read hydraulic dissipation time						*/
	{NULL,0},													/* Msg 0x03D3	Return hydraulic dissipation time					*/
#else
	{NULL,0},													/* Msg 0x03D1	Set hydraulic dissipation time						*/
	{NULL,0},													/* Msg 0x03D2	Read hydraulic dissipation time						*/
	{NULL,0},													/* Msg 0x03D3	Return hydraulic dissipation time					*/
#endif

#ifdef SIGNALCUBE
	{fn_start_zero,SECURITY_OPERATOR},							/* Msg 0x03D4	Start zero operation (Signal Cube only)				*/
	{fn_read_zero_progress,SECURITY_OPERATOR},					/* Msg 0x03D5	Read set zero progress (Signal Cube only)			*/
	{NULL,0},													/* Msg 0x03D6	Return set zero progress (Signal Cube only)			*/
#else
	{NULL,0},													/* Msg 0x03D4	Start zero operation (Signal Cube only)				*/
	{NULL,0},													/* Msg 0x03D5	Read set offset progress (Signal Cube only)			*/
	{NULL,0},													/* Msg 0x03D6	Return set offset progress (Signal Cube only)		*/
#endif

#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_set_hydraulic_name,SECURITY_CALIBRATOR},				/* Msg 0x03D7	Set hydraulic name									*/
	{fn_read_hydraulic_name,0},									/* Msg 0x03D8	Read hydraulic name									*/
	{NULL,0},													/* Msg 0x03D9	Return hydraulic name								*/
#else
	{NULL,0},													/* Msg 0x03D7	Set hydraulic name									*/
	{NULL,0},													/* Msg 0x03D8	Read hydraulic name									*/
	{NULL,0},													/* Msg 0x03D9	Return hydraulic name								*/
#endif

	{fn_set_userdata,SECURITY_OPERATOR},						/* Msg 0x03DA	Set user data area									*/
	{fn_read_userdata,0},										/* Msg 0x03DB	Read user data area									*/
	{NULL,0},													/* Msg 0x03DC	Return user data area								*/

	{fn_set_chanstring,SECURITY_OPERATOR},						/* Msg 0x03DD	Set channel user string area						*/
	{fn_read_chanstring,0},										/* Msg 0x03DE	Read channel user string area						*/
	{NULL,0},													/* Msg 0x03DF	Return channel user string area						*/

	{fn_read_ip_channelblock,0},								/* Msg 0x03E0	Read input channel block 							*/
	{NULL,0},													/* Msg 0x03E1	Return input channel block							*/

	{fn_set_cnet_timeout,SECURITY_OPERATOR},					/* Msg 0x03E2	Set CNet timeout									*/
	{fn_read_cnet_timeout,0},									/* Msg 0x03E3	Read CNet timeout									*/
	{NULL,0},													/* Msg 0x03E4	Return CNet timeout									*/

#ifdef SIGNALCUBE
	{fn_set_gauge_info,SECURITY_CALIBRATOR},					/* Msg 0x03E5	Set gauge information (Signal Cube only)			*/
	{fn_read_gauge_info,0},										/* Msg 0x03E6	Read gauge information (Signal Cube only)			*/
	{NULL,0},													/* Msg 0x03E7	Return gauge information (Signal Cube only)			*/
#else
	{NULL,0},													/* Msg 0x03E5	Set gauge information (Signal Cube only)			*/
	{NULL,0},													/* Msg 0x03E6	Read gauge information (Signal Cube only)			*/
	{NULL,0},													/* Msg 0x03E7	Return gauge information (Signal Cube only)			*/
#endif
	{fn_undo_refzero,SECURITY_OPERATOR},						/* Msg 0x03E8	Undo reference zero									*/
	{fn_clear_all_leds,SECURITY_OPERATOR},						/* Msg 0x03E9	Clear all LEDs										*/
	{fn_set_peak_threshold,SECURITY_OPERATOR},					/* Msg 0x03EA	Set peak/trough detection threshold					*/
	{fn_read_peak_threshold,SECURITY_OPERATOR},					/* Msg 0x03EB	Read peak/trough detection threshold				*/
	{NULL,0},													/* Msg 0x03EC	Return peak/trough detection threshold				*/

	{fn_open_event_connection,SECURITY_OPERATOR},				/* Msg 0x03ED	Open event connection								*/
	{NULL,0},													/* Msg 0x03EE	Return event connection								*/
	{fn_close_event_connection,SECURITY_OPERATOR},				/* Msg 0x03EF	Close event connection								*/
	{fn_add_event_report,SECURITY_OPERATOR},					/* Msg 0x03F0	Add event report									*/
	{fn_remove_event_report,SECURITY_OPERATOR},	  				/* Msg 0x03F1	Remove event report									*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_send_hc_instruction,SECURITY_OPERATOR},					/* Msg 0x03F2	Send hand controller instruction					*/
	{fn_read_hc_keystate,SECURITY_OPERATOR},					/* Msg 0x03F3	Read hand controller key state						*/
	{NULL,0},													/* Msg 0x03F4	Return hand controller key state					*/
#else
	{NULL,0},													/* Msg 0x03F2	Send hand controller instruction					*/
	{NULL,0},													/* Msg 0x03F3	Read hand controller key state						*/
	{NULL,0},													/* Msg 0x03F4	Return hand controller key state					*/
#endif /* CONTROLCUBE */

	{fn_set_semaphore,0},										/* Msg 0x03F5	Set semaphore state									*/
	{fn_read_semaphore,0},										/* Msg 0x03F6	Read semaphore state								*/
	{NULL,0},													/* Msg 0x03F7	Return semaphore state								*/

	{fn_set_channel_source,0},									/* Msg 0x03F8	Set channel source pointer							*/

	{fn_set_neg_gaintrim,SECURITY_CALIBRATOR},					/* Msg 0x03F9	Set negative gain trim value						*/
	{fn_read_neg_gaintrim,0},									/* Msg 0x03FA	Read negative gain trim value 						*/
	{NULL,0},													/* Msg 0x03FB	Return negative gain trim value						*/
	{fn_read_ip_cyclecount,0},									/* Msg 0x03FC	Read input channel cycle count						*/
	{NULL,0},													/* Msg 0x03FD	Return input channel cycle count					*/
	{fn_read_ip_cyclecountblock,0},								/* Msg 0x03FE	Read input channel cycle count block				*/
	{NULL,0},													/* Msg 0x03FF	Return input channel cycle count block				*/
	{fn_reset_cyclecount,0},									/* Msg 0x0400	Reset input channel cycle count						*/
	{fn_reset_all_cyclecounts,0},								/* Msg 0x0401	Reset all input channel cycle counts				*/
	{fn_set_fault_mask,SECURITY_CALIBRATOR},					/* Msg 0x0402	Set fault mask										*/
	{fn_read_fault_state,0},									/* Msg 0x0403	Read fault state									*/
	{NULL,0},													/* Msg 0x0404	Return fault state									*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_read_guard_state,0},									/* Msg 0x0405	Read guard state									*/
	{NULL,0},													/* Msg 0x0406	Return guard state									*/
#else
	{NULL,0},													/* Msg 0x0405	Read guard state									*/
	{NULL,0},													/* Msg 0x0406	Return guard state									*/
#endif
	{fn_read_peak_threshold_ext,SECURITY_OPERATOR},				/* Msg 0x0407	Read peak/trough detection threshold (extended)		*/
	{NULL,0},													/* Msg 0x0408	Return peak/trough detection threshold (extended)	*/
	{fn_read_full_caltable,SECURITY_OPERATOR},					/* Msg 0x0409	Read full calibration table							*/
	{NULL,0},													/* Msg 0x040A	Return full calibration table						*/

	{fn_set_digtxdr_config,0},									/* Msg 0x040B	Set digital transducer configuration				*/
	{fn_read_digtxdr_config,0},									/* Msg 0x040C	Read digital transducer configuration				*/
	{NULL,0},													/* Msg 0x040D	Return digital transducer configuration				*/

#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_set_simulation_mode,0},									/* Msg 0x040E	Set simulation mode state							*/
	{fn_read_simulation_mode,0},								/* Msg 0x040F	Read simulation mode state							*/
	{NULL,0},													/* Msg 0x0410	Return simulation mode state						*/
#else
	{NULL,0},													/* Msg 0x040E	Set simulation mode state							*/
	{NULL,0},													/* Msg 0x040F	Read simulation mode state							*/
	{NULL,0},													/* Msg 0x0410	Return simulation mode state						*/
#endif

	{fn_set_peak_timeout,0},									/* Msg 0x0411	Set peak detection timeout							*/
	{fn_read_peak_timeout,0},									/* Msg 0x0412	Read peak detection timout							*/
	{NULL,0},													/* Msg 0x0413	Return peak detection timeout						*/
#ifdef SIGNALCUBE
	{fn_set_hpfilter_config,SECURITY_CALIBRATOR},				/* Msg 0x0414	Set HP filter config (Signal Cube only)				*/
	{fn_read_hpfilter_config,0},								/* Msg 0x0415	Read HP filter config (Signal Cube only)			*/
	{NULL,0},													/* Msg 0x0416	Return HP filter config (Signal Cube only)			*/
	{fn_set_calzero_entry,SECURITY_ADMINISTRATOR},				/* Msg 0x0417	Set calibration zero entry (Signal Cube only)		*/
	{fn_read_calzero_entry,0},									/* Msg 0x0418	Read calibration zero entry (Signal Cube only)		*/
	{NULL,0},													/* Msg 0x0419	Return calibration zero entry (Signal Cube only)	*/
#else
	{NULL,0},													/* Msg 0x0414	Set HP filter config (Signal Cube only)				*/
	{NULL,0},													/* Msg 0x0415	Read HP filter config (Signal Cube only)			*/
	{NULL,0},													/* Msg 0x0416	Return HP filter config (Signal Cube only)			*/
	{NULL,0},													/* Msg 0x0417	Set calibration zero entry (Signal Cube only)		*/
	{NULL,0},													/* Msg 0x0418	Read calibration zero entry (Signal Cube only)		*/
	{NULL,0},													/* Msg 0x0419	Return calibration zero entry (Signal Cube only)	*/
#endif

#if (defined(CONTROLCUBE) || defined(AICUBE))
	{fn_read_runtime,0},										/* Msg 0x041A	Read system runtime (Control Cube only)				*/
	{NULL,0},													/* Msg 0x041B	Return system runtime (Control Cube only)			*/
#else
	{NULL,0},													/* Msg 0x041A	Read system runtime (Control Cube only)				*/
	{NULL,0},													/* Msg 0x041B	Return system runtime (Control Cube only)			*/
#endif

	{fn_define_comm_timeout_ext,SECURITY_OPERATOR},				/* Msg 0x041C	Define comm timeout (extended)						*/
	{fn_read_comm_timeout_ext,0},								/* Msg 0x041D	Read comm timeout (extended)						*/
	{NULL,0},													/* Msg 0x041E	Return comm timeout	(extended)						*/
	{fn_set_comm_closing,SECURITY_OPERATOR},					/* Msg 0x041F	Indicate connection is closing						*/
	{fn_read_comm_id,SECURITY_OPERATOR},						/* Msg 0x0420	Read comm id										*/
	{NULL,0},													/* Msg 0x0421	Return comm id										*/
	{fn_force_comm_close,SECURITY_OPERATOR},					/* Msg 0x0422	Force comm channel to close							*/
	
	{fn_read_ip_channelblock_ext,0},							/* Msg 0x0423	Read input channel block (extended)					*/
	{NULL,0},													/* Msg 0x0424	Return input channel block (extended)				*/

#endif /* LOADER */	
	
};

int(* const messagelist[])(uint8_t *msg, uint8_t *reply, uint32_t *msglength, uint32_t *replylength, void *conn) = 
{

	/* Error messages */

	NULL,								/* Msg 0x0000	Reserved											*/
	NULL,								/* Msg 0x0001	Error - Illegal message								*/
	NULL,								/* Msg 0x0002	Error - Illegal channel								*/
	NULL,								/* Msg 0x0003	Error - Illegal function							*/
	NULL,								/* Msg 0x0004	Error - Operation failed							*/
	NULL,								/* Msg 0x0005	Error - Illegal parameter							*/
	NULL,								/* Msg 0x0006	Error - CNet timeout								*/
	NULL,								/* Msg 0x0007	Error - Illegal destination							*/
	NULL,								/* Msg 0x0008	Error - No stream									*/
	NULL,								/* Msg 0x0009	Error - message too long							*/
	NULL,								/* Msg 0x000A	Reserved											*/
	NULL,								/* Msg 0x000B	Reserved											*/
	NULL,								/* Msg 0x000C	Reserved											*/
	NULL,								/* Msg 0x000D	Reserved											*/
	NULL,								/* Msg 0x000E	Reserved											*/
	NULL,								/* Msg 0x000F	Reserved											*/
	NULL,								/* Msg 0x0010	Reserved											*/
	NULL,								/* Msg 0x0011	Reserved											*/
	NULL,								/* Msg 0x0012	Reserved											*/
	NULL,								/* Msg 0x0013	Reserved											*/
	NULL,								/* Msg 0x0014	Reserved											*/
	NULL,								/* Msg 0x0015	Reserved											*/
	NULL,								/* Msg 0x0016	Reserved											*/
	NULL,								/* Msg 0x0017	Reserved											*/
	NULL,								/* Msg 0x0018	Reserved											*/
	NULL,								/* Msg 0x0019	Reserved											*/
	NULL,								/* Msg 0x001A	Reserved											*/
	NULL,								/* Msg 0x001B	Reserved											*/
	NULL,								/* Msg 0x001C	Reserved											*/
	NULL,								/* Msg 0x001D	Reserved											*/
	NULL,								/* Msg 0x001E	Reserved											*/
	NULL,								/* Msg 0x001F	Reserved											*/
	NULL,								/* Msg 0x0020	Reserved											*/
	NULL,								/* Msg 0x0021	Reserved											*/
	NULL,								/* Msg 0x0022	Reserved											*/
	NULL,								/* Msg 0x0023	Reserved											*/
	NULL,								/* Msg 0x0024	Reserved											*/
	NULL,								/* Msg 0x0025	Reserved											*/
	NULL,								/* Msg 0x0026	Reserved											*/
	NULL,								/* Msg 0x0027	Reserved											*/
	NULL,								/* Msg 0x0028	Reserved											*/
	NULL,								/* Msg 0x0029	Reserved											*/
	NULL,								/* Msg 0x002A	Reserved											*/
	NULL,								/* Msg 0x002B	Reserved											*/
	NULL,								/* Msg 0x002C	Reserved											*/
	NULL,								/* Msg 0x002D	Reserved											*/
	NULL,								/* Msg 0x002E	Reserved											*/
	NULL,								/* Msg 0x002F	Reserved											*/
	NULL,								/* Msg 0x0030	Reserved											*/
	NULL,								/* Msg 0x0031	Reserved											*/
	NULL,								/* Msg 0x0032	Reserved											*/
	NULL,								/* Msg 0x0033	Reserved											*/
	NULL,								/* Msg 0x0034	Reserved											*/
	NULL,								/* Msg 0x0035	Reserved											*/
	NULL,								/* Msg 0x0036	Reserved											*/
	NULL,								/* Msg 0x0037	Reserved											*/
	NULL,								/* Msg 0x0038	Reserved											*/
	NULL,								/* Msg 0x0039	Reserved											*/
	NULL,								/* Msg 0x003A	Reserved											*/
	NULL,								/* Msg 0x003B	Reserved											*/
	NULL,								/* Msg 0x003C	Reserved											*/
	NULL,								/* Msg 0x003D	Reserved											*/
	NULL,								/* Msg 0x003E	Reserved											*/
	NULL,								/* Msg 0x003F	Reserved											*/
	NULL,								/* Msg 0x0040	Reserved											*/
	NULL,								/* Msg 0x0041	Reserved											*/
	NULL,								/* Msg 0x0042	Reserved											*/
	NULL,								/* Msg 0x0043	Reserved											*/
	NULL,								/* Msg 0x0044	Reserved											*/
	NULL,								/* Msg 0x0045	Reserved											*/
	NULL,								/* Msg 0x0046	Reserved											*/
	NULL,								/* Msg 0x0047	Reserved											*/
	NULL,								/* Msg 0x0048	Reserved											*/
	NULL,								/* Msg 0x0049	Reserved											*/
	NULL,								/* Msg 0x004A	Reserved											*/
	NULL,								/* Msg 0x004B	Reserved											*/
	NULL,								/* Msg 0x004C	Reserved											*/
	NULL,								/* Msg 0x004D	Reserved											*/
	NULL,								/* Msg 0x004E	Reserved											*/
	NULL,								/* Msg 0x004F	Reserved											*/
	NULL,								/* Msg 0x0050	Reserved											*/
	NULL,								/* Msg 0x0051	Reserved											*/
	NULL,								/* Msg 0x0052	Reserved											*/
	NULL,								/* Msg 0x0053	Reserved											*/
	NULL,								/* Msg 0x0054	Reserved											*/
	NULL,								/* Msg 0x0055	Reserved											*/
	NULL,								/* Msg 0x0056	Reserved											*/
	NULL,								/* Msg 0x0057	Reserved											*/
	NULL,								/* Msg 0x0058	Reserved											*/
	NULL,								/* Msg 0x0059	Reserved											*/
	NULL,								/* Msg 0x005A	Reserved											*/
	NULL,								/* Msg 0x005B	Reserved											*/
	NULL,								/* Msg 0x005C	Reserved											*/
	NULL,								/* Msg 0x005D	Reserved											*/
	NULL,								/* Msg 0x005E	Reserved											*/
	NULL,								/* Msg 0x005F	Reserved											*/
	NULL,								/* Msg 0x0060	Reserved											*/
	NULL,								/* Msg 0x0061	Reserved											*/	
	NULL,								/* Msg 0x0062	Reserved											*/
	NULL,								/* Msg 0x0063	Reserved											*/
	NULL,								/* Msg 0x0064	Reserved											*/
	NULL,								/* Msg 0x0065	Reserved											*/
	NULL,								/* Msg 0x0066	Reserved											*/
	NULL,								/* Msg 0x0067	Reserved											*/
	NULL,								/* Msg 0x0068	Reserved											*/
	NULL,								/* Msg 0x0069	Reserved											*/
	NULL,								/* Msg 0x006A	Reserved											*/
	NULL,								/* Msg 0x006B	Reserved											*/
	NULL,								/* Msg 0x006C	Reserved											*/
	NULL,								/* Msg 0x006D	Reserved											*/
	NULL,								/* Msg 0x006E	Reserved											*/
	NULL,								/* Msg 0x006F	Reserved											*/
	NULL,								/* Msg 0x0070	Reserved											*/
	NULL,								/* Msg 0x0071	Reserved											*/
	NULL,								/* Msg 0x0072	Reserved											*/	
	NULL,								/* Msg 0x0073	Reserved											*/
	NULL,								/* Msg 0x0074	Reserved											*/
	NULL,								/* Msg 0x0075	Reserved											*/
	NULL,								/* Msg 0x0076	Reserved											*/
	NULL,								/* Msg 0x0077	Reserved											*/
	NULL,								/* Msg 0x0078	Reserved											*/
	NULL,								/* Msg 0x0079	Reserved											*/
	NULL,								/* Msg 0x007A	Reserved											*/
	NULL,								/* Msg 0x007B	Reserved											*/
	NULL,								/* Msg 0x007C	Reserved											*/
	NULL,								/* Msg 0x007D	Reserved											*/
	NULL,								/* Msg 0x007E	Reserved											*/
	NULL,								/* Msg 0x007F	Reserved											*/
	NULL,								/* Msg 0x0080	Reserved											*/
	NULL,								/* Msg 0x0081	Reserved											*/
	NULL,								/* Msg 0x0082	Reserved											*/
	NULL,								/* Msg 0x0083	Reserved											*/
	NULL,								/* Msg 0x0084	Reserved											*/
	NULL,								/* Msg 0x0085	Reserved											*/
	NULL,								/* Msg 0x0086	Reserved											*/
	NULL,								/* Msg 0x0087	Reserved											*/
	NULL,								/* Msg 0x0088	Reserved											*/
	NULL,								/* Msg 0x0089	Reserved											*/
	NULL,								/* Msg 0x008A	Reserved											*/
	NULL,								/* Msg 0x008B	Reserved											*/
	NULL,								/* Msg 0x008C	Reserved											*/
	NULL,								/* Msg 0x008D	Reserved											*/
	NULL,								/* Msg 0x008E	Reserved											*/
	NULL,								/* Msg 0x008F	Reserved											*/
	NULL,								/* Msg 0x0090	Reserved											*/
	NULL,								/* Msg 0x0091	Reserved											*/
	NULL,								/* Msg 0x0092	Reserved											*/
	NULL,								/* Msg 0x0093	Reserved											*/
	NULL,								/* Msg 0x0094	Reserved											*/
	NULL,								/* Msg 0x0095	Reserved											*/
	NULL,								/* Msg 0x0096	Reserved											*/
	NULL,								/* Msg 0x0097	Reserved											*/
	NULL,								/* Msg 0x0098	Reserved											*/
	NULL,								/* Msg 0x0099	Reserved											*/
	NULL,								/* Msg 0x009A	Reserved											*/
	NULL,								/* Msg 0x009B	Reserved											*/
	NULL,								/* Msg 0x009C	Reserved											*/
	NULL,								/* Msg 0x009D	Reserved											*/
	NULL,								/* Msg 0x009E	Reserved											*/
	NULL,								/* Msg 0x009F	Reserved											*/
	NULL,								/* Msg 0x00A0	Reserved											*/
	NULL,								/* Msg 0x00A1	Reserved											*/
	NULL,								/* Msg 0x00A2	Reserved											*/
	NULL,								/* Msg 0x00A3	Reserved											*/
	NULL,								/* Msg 0x00A4	Reserved											*/
	NULL,								/* Msg 0x00A5	Reserved											*/
	NULL,								/* Msg 0x00A6	Reserved											*/
	NULL,								/* Msg 0x00A7	Reserved											*/
	NULL,								/* Msg 0x00A8	Reserved											*/
	NULL,								/* Msg 0x00A9	Reserved											*/
	NULL,								/* Msg 0x00AA	Reserved											*/
	NULL,								/* Msg 0x00AB	Reserved											*/
	NULL,								/* Msg 0x00AC	Reserved											*/
	NULL,								/* Msg 0x00AD	Reserved											*/
	NULL,								/* Msg 0x00AE	Reserved											*/
	NULL,								/* Msg 0x00AF	Reserved											*/
	NULL,								/* Msg 0x00B0	Reserved											*/
	NULL,								/* Msg 0x00B1	Reserved											*/
	NULL,								/* Msg 0x00B2	Reserved											*/
	NULL,								/* Msg 0x00B3	Reserved											*/
	NULL,								/* Msg 0x00B4	Reserved											*/
	NULL,								/* Msg 0x00B5	Reserved											*/
	NULL,								/* Msg 0x00B6	Reserved											*/
	NULL,								/* Msg 0x00B7	Reserved											*/
	NULL,								/* Msg 0x00B8	Reserved											*/	
	NULL,								/* Msg 0x00B9	Reserved											*/
	NULL,								/* Msg 0x00BA	Reserved											*/
	NULL,								/* Msg 0x00BB	Reserved											*/
	NULL,								/* Msg 0x00BC	Reserved											*/
	NULL,								/* Msg 0x00BD	Reserved											*/
	NULL,								/* Msg 0x00BE	Reserved											*/
	NULL,								/* Msg 0x00BF	Reserved											*/
	NULL,								/* Msg 0x00C0	Reserved											*/
	NULL,								/* Msg 0x00C1	Reserved											*/
	NULL,								/* Msg 0x00C2	Reserved											*/
	NULL,								/* Msg 0x00C3	Reserved											*/
	NULL,								/* Msg 0x00C4	Reserved											*/
	NULL,								/* Msg 0x00C5	Reserved											*/
	NULL,								/* Msg 0x00C6	Reserved											*/
	NULL,								/* Msg 0x00C7	Reserved											*/
	NULL,								/* Msg 0x00C8	Reserved											*/
	NULL,								/* Msg 0x00C9	Reserved											*/
	NULL,								/* Msg 0x00CA	Reserved											*/
	NULL,								/* Msg 0x00CB	Reserved											*/
	NULL,								/* Msg 0x00CC	Reserved											*/
	NULL,								/* Msg 0x00CD	Reserved											*/
	NULL,								/* Msg 0x00CE	Reserved											*/
	NULL,								/* Msg 0x00CF	Reserved											*/
	NULL,								/* Msg 0x00D0	Reserved											*/
	NULL,								/* Msg 0x00D1	Reserved											*/
	NULL,								/* Msg 0x00D2	Reserved											*/
	NULL,								/* Msg 0x00D3	Reserved											*/
	NULL,								/* Msg 0x00D4	Reserved											*/
	NULL,								/* Msg 0x00D5	Reserved											*/
	NULL,								/* Msg 0x00D6	Reserved											*/
	NULL,								/* Msg 0x00D7	Reserved											*/
	NULL,								/* Msg 0x00D8	Reserved											*/
	NULL,								/* Msg 0x00D9	Reserved											*/
	NULL,								/* Msg 0x00DA	Reserved											*/
	NULL,								/* Msg 0x00DB	Reserved											*/
	NULL,								/* Msg 0x00DC	Reserved											*/
	NULL,								/* Msg 0x00DD	Reserved											*/
	NULL,								/* Msg 0x00DE	Reserved											*/
	NULL,								/* Msg 0x00DF	Reserved											*/
	NULL,								/* Msg 0x00E0	Reserved											*/
	NULL,								/* Msg 0x00E1	Reserved											*/
	NULL,								/* Msg 0x00E2	Reserved											*/
	NULL,								/* Msg 0x00E3	Reserved											*/
	NULL,								/* Msg 0x00E4	Reserved											*/
	NULL,								/* Msg 0x00E5	Reserved											*/	
	NULL,								/* Msg 0x00E6	Reserved											*/
	NULL,								/* Msg 0x00E7	Reserved											*/
	NULL,								/* Msg 0x00E8	Reserved											*/
	NULL,								/* Msg 0x00E9	Reserved											*/
	NULL,								/* Msg 0x00EA	Reserved											*/
	NULL,								/* Msg 0x00EB	Reserved											*/
	NULL,								/* Msg 0x00EC	Reserved											*/
	NULL,								/* Msg 0x00ED	Reserved											*/
	NULL,								/* Msg 0x00EE	Reserved											*/
	NULL,								/* Msg 0x00EF	Reserved											*/
	NULL,								/* Msg 0x00F0	Reserved											*/
	NULL,								/* Msg 0x00F1	Reserved											*/
	NULL,								/* Msg 0x00F2	Reserved											*/
	NULL,								/* Msg 0x00F3	Reserved											*/
	NULL,								/* Msg 0x00F4	Reserved											*/
	NULL,								/* Msg 0x00F5	Reserved											*/
	NULL,								/* Msg 0x00F6	Reserved											*/
	NULL,								/* Msg 0x00F7	Reserved											*/
	NULL,								/* Msg 0x00F8	Reserved											*/
	NULL,								/* Msg 0x00F9	Reserved											*/
	NULL,								/* Msg 0x00FA	Reserved											*/
	NULL,								/* Msg 0x00FB	Reserved											*/
	NULL,								/* Msg 0x00FC	Reserved											*/
	NULL,								/* Msg 0x00FD	Reserved											*/
	NULL,								/* Msg 0x00FE	Reserved											*/
	NULL,								/* Msg 0x00FF	Reserved											*/
	
	/* Loader messages */
	
	fn_read_firmware_version,			/* Msg 0x0100	Read firmware version								*/
	NULL,								/* Msg 0x0101	Return firmware version								*/
	fn_read_device_type,				/* Msg 0x0102	Read device type									*/
	NULL,								/* Msg 0x0103	Return device type									*/
	fn_erase_flash,						/* Msg 0x0104	Erase flash											*/
	fn_read_erase_status,				/* Msg 0x0105	Read erase status									*/
	NULL,								/* Msg 0x0106	Return erase status									*/
	fn_prog_flash,						/* Msg 0x0107	Program flash										*/
	NULL,								/* Msg 0x0108	Return program status								*/
	fn_erase_flash_block,				/* Msg 0x0109	Erase flash block									*/
	fn_read_cnet_info,					/* Msg 0x010A	Read CNet info										*/
	NULL,								/* Msg 0x010B	Return CNet info									*/
	fn_set_ip_address,					/* Msg 0x010C	Set IP address										*/
	fn_read_ip_address,					/* Msg 0x010D	Read IP address										*/
	NULL,								/* Msg 0x010E	Return IP address									*/
	fn_hw_restart,						/* Msg 0x010F	Restart hardware									*/
	fn_fw_validate,						/* Msg 0x0110	Validate firmware									*/
	NULL,								/* Msg 0x0111	Return firmware validation state					*/
	fn_testmode_set_address,			/* Msg 0x0112	Set address											*/
	fn_testmode_write8,					/* Msg 0x0113	Write 8-bit data									*/
	fn_testmode_write16,				/* Msg 0x0114	Write 16-bit data									*/
	fn_testmode_write32,				/* Msg 0x0115	Write 32-bit data									*/
	fn_testmode_read8,					/* Msg 0x0116	Read 8-bit data										*/
	NULL,								/* Msg 0x0117	Return 8-bit data									*/
	fn_testmode_read16,					/* Msg 0x0118	Read 16-bit data									*/
	NULL,								/* Msg 0x0119	Return 16-bit data									*/
	fn_testmode_read32,					/* Msg 0x011A	Read 32-bit data									*/
	NULL,								/* Msg 0x011B	Return 32-bit data									*/
	fn_testmode_i2c_start,				/* Msg 0x011C	Generate I2C start									*/
	fn_testmode_i2c_restart,			/* Msg 0x011D	Generate I2C restart								*/
	fn_testmode_i2c_stop,				/* Msg 0x011E	Generate I2C stop									*/
	fn_testmode_i2c_send,				/* Msg 0x011F	Send I2C data										*/
	NULL,								/* Msg 0x0120	Return I2C send ack									*/
	fn_testmode_i2c_read,				/* Msg 0x0121	Read I2C data										*/
	NULL,								/* Msg 0x0122	Return I2C data										*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_testmode_dspb_i2c_start,			/* Msg 0x0123	Generate DSPB I2C start								*/
	fn_testmode_dspb_i2c_restart,		/* Msg 0x0124	Generate DSPB I2C restart							*/
	fn_testmode_dspb_i2c_stop,			/* Msg 0x0125	Generate DSPB I2C stop								*/
	fn_testmode_dspb_i2c_send,			/* Msg 0x0126	Send DSPB I2C data									*/
	NULL,								/* Msg 0x0127	Return DSPB I2C ack									*/
	fn_testmode_dspb_i2c_read,			/* Msg 0x0128	Read DSPB I2C data									*/
	NULL,								/* Msg 0x0129	Return DSPB I2C data								*/
#else
	NULL,								/* Msg 0x0123	Generate DSPB I2C start								*/
	NULL,								/* Msg 0x0124	Generate DSPB I2C restart							*/
	NULL,								/* Msg 0x0125	Generate DSPB I2C stop								*/
	NULL,								/* Msg 0x0126	Send DSPB I2C data									*/
	NULL,								/* Msg 0x0127	Return DSPB I2C ack									*/
	NULL,								/* Msg 0x0128	Read DSPB I2C data									*/
	NULL,								/* Msg 0x0129	Return DSPB I2C data								*/
#endif
	fn_testmode_onew_init,				/* Msg 0x012A	Initialise 1-wire interface							*/
	fn_testmode_onew_reset,				/* Msg 0x012B	Reset 1-wire interface								*/
	NULL,								/* Msg 0x012C	Return 1-wire reset status							*/
	fn_testmode_onew_send,				/* Msg 0x012D	Send data via 1-wire interface						*/
	fn_testmode_onew_read,				/* Msg 0x012E	Read data via 1-wire interface						*/
	NULL,								/* Msg 0x012F	Return 1-wire data									*/
	fn_testmode_enable_watchdog,		/* Msg 0x0130	Enable watchdog										*/
	fn_testmode_disable_watchdog,		/* Msg 0x0131	Disable watchdog									*/
	fn_read_mac_address,				/* Msg 0x0132	Read MAC address									*/
	NULL,								/* Msg 0x0133	Return MAC address									*/
	fn_read_firmware_information,		/* Msg 0x0134	Read firmware information							*/
	NULL,								/* Msg 0x0135	Return firmware information							*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_testmode_write32b,				/* Msg 0x0136	Write DSP B 32-bit data								*/
	fn_testmode_read32b,				/* Msg 0x0137	Read DSP B 32-bit data								*/
	NULL,								/* Msg 0x0138	Return DSP B 32-bit data							*/
#else
	NULL,								/* Msg 0x0136	Write DSP B 32-bit data								*/
	NULL,								/* Msg 0x0137	Read DSP B 32-bit data								*/
	NULL,								/* Msg 0x0138	Return DSP B 32-bit data							*/
#endif
	NULL,								/* Msg 0x0139	Reserved											*/
	NULL,								/* Msg 0x013A	Reserved											*/
	NULL,								/* Msg 0x013B	Reserved											*/
	NULL,								/* Msg 0x013C	Reserved											*/
	NULL,								/* Msg 0x013D	Reserved											*/
	NULL,								/* Msg 0x013E	Reserved											*/
	NULL,								/* Msg 0x013F	Reserved											*/
	NULL,								/* Msg 0x0140	Reserved											*/
	NULL,								/* Msg 0x0141	Reserved											*/
	NULL,								/* Msg 0x0142	Reserved											*/
	NULL,								/* Msg 0x0143	Reserved											*/
	NULL,								/* Msg 0x0144	Reserved											*/
	NULL,								/* Msg 0x0145	Reserved											*/
	NULL,								/* Msg 0x0146	Reserved											*/
	NULL,								/* Msg 0x0147	Reserved											*/
	NULL,								/* Msg 0x0148	Reserved											*/
	NULL,								/* Msg 0x0149	Reserved											*/
	NULL,								/* Msg 0x014A	Reserved											*/
	NULL,								/* Msg 0x014B	Reserved											*/
	NULL,								/* Msg 0x014C	Reserved											*/
	NULL,								/* Msg 0x014D	Reserved											*/
	NULL,								/* Msg 0x014E	Reserved											*/
	NULL,								/* Msg 0x014F	Reserved											*/
	NULL,								/* Msg 0x0150	Reserved											*/
	NULL,								/* Msg 0x0151	Reserved											*/
	NULL,								/* Msg 0x0152	Reserved											*/
	NULL,								/* Msg 0x0153	Reserved											*/
	NULL,								/* Msg 0x0154	Reserved											*/
	NULL,								/* Msg 0x0155	Reserved											*/
	NULL,								/* Msg 0x0156	Reserved											*/
	NULL,								/* Msg 0x0157	Reserved											*/
	NULL,								/* Msg 0x0158	Reserved											*/
	NULL,								/* Msg 0x0159	Reserved											*/
	NULL,								/* Msg 0x015A	Reserved											*/
	NULL,								/* Msg 0x015B	Reserved											*/
	NULL,								/* Msg 0x015C	Reserved											*/
	NULL,								/* Msg 0x015D	Reserved											*/
	NULL,								/* Msg 0x015E	Reserved											*/
	NULL,								/* Msg 0x015F	Reserved											*/
	NULL,								/* Msg 0x0160	Reserved											*/
	NULL,								/* Msg 0x0161	Reserved											*/
	NULL,								/* Msg 0x0162	Reserved											*/
	NULL,								/* Msg 0x0163	Reserved											*/
	NULL,								/* Msg 0x0164	Reserved											*/
	NULL,								/* Msg 0x0165	Reserved											*/
	NULL,								/* Msg 0x0166	Reserved											*/
	NULL,								/* Msg 0x0167	Reserved											*/
	NULL,								/* Msg 0x0168	Reserved											*/
	NULL,								/* Msg 0x0169	Reserved											*/
	NULL,								/* Msg 0x016A	Reserved											*/
	NULL,								/* Msg 0x016B	Reserved											*/
	NULL,								/* Msg 0x016C	Reserved											*/
	NULL,								/* Msg 0x016D	Reserved											*/
	NULL,								/* Msg 0x016E	Reserved											*/
	NULL,								/* Msg 0x016F	Reserved											*/
	NULL,								/* Msg 0x0170	Reserved											*/
	NULL,								/* Msg 0x0171	Reserved											*/
	NULL,								/* Msg 0x0172	Reserved											*/
	NULL,								/* Msg 0x0173	Reserved											*/
	NULL,								/* Msg 0x0174	Reserved											*/
	NULL,								/* Msg 0x0175	Reserved											*/
	NULL,								/* Msg 0x0176	Reserved											*/
	NULL,								/* Msg 0x0177	Reserved											*/
	NULL,								/* Msg 0x0178	Reserved											*/
	NULL,								/* Msg 0x0179	Reserved											*/
	NULL,								/* Msg 0x017A	Reserved											*/
	NULL,								/* Msg 0x017B	Reserved											*/
	NULL,								/* Msg 0x017C	Reserved											*/
	NULL,								/* Msg 0x017D	Reserved											*/
	NULL,								/* Msg 0x017E	Reserved											*/
	NULL,								/* Msg 0x017F	Reserved											*/
	NULL,								/* Msg 0x0180	Reserved											*/
	NULL,								/* Msg 0x0181	Reserved											*/
	NULL,								/* Msg 0x0182	Reserved											*/
	NULL,								/* Msg 0x0183	Reserved											*/
	NULL,								/* Msg 0x0184	Reserved											*/
	NULL,								/* Msg 0x0185	Reserved											*/
	NULL,								/* Msg 0x0186	Reserved											*/
	NULL,								/* Msg 0x0187	Reserved											*/
	NULL,								/* Msg 0x0188	Reserved											*/
	NULL,								/* Msg 0x0189	Reserved											*/
	NULL,								/* Msg 0x018A	Reserved											*/
	NULL,								/* Msg 0x018B	Reserved											*/
	NULL,								/* Msg 0x018C	Reserved											*/
	NULL,								/* Msg 0x018D	Reserved											*/
	NULL,								/* Msg 0x018E	Reserved											*/
	NULL,								/* Msg 0x018F	Reserved											*/
	NULL,								/* Msg 0x0190	Reserved											*/
	NULL,								/* Msg 0x0191	Reserved											*/
	NULL,								/* Msg 0x0192	Reserved											*/
	NULL,								/* Msg 0x0193	Reserved											*/
	NULL,								/* Msg 0x0194	Reserved											*/
	NULL,								/* Msg 0x0195	Reserved											*/
	NULL,								/* Msg 0x0196	Reserved											*/
	NULL,								/* Msg 0x0197	Reserved											*/
	NULL,								/* Msg 0x0198	Reserved											*/
	NULL,								/* Msg 0x0199	Reserved											*/
	NULL,								/* Msg 0x019A	Reserved											*/
	NULL,								/* Msg 0x019B	Reserved											*/
	NULL,								/* Msg 0x019C	Reserved											*/
	NULL,								/* Msg 0x019D	Reserved											*/
	NULL,								/* Msg 0x019E	Reserved											*/
	NULL,								/* Msg 0x019F	Reserved											*/
	NULL,								/* Msg 0x01A0	Reserved											*/
	NULL,								/* Msg 0x01A1	Reserved											*/
	NULL,								/* Msg 0x01A2	Reserved											*/
	NULL,								/* Msg 0x01A3	Reserved											*/
	NULL,								/* Msg 0x01A4	Reserved											*/
	NULL,								/* Msg 0x01A5	Reserved											*/
	NULL,								/* Msg 0x01A6	Reserved											*/
	NULL,								/* Msg 0x01A7	Reserved											*/
	NULL,								/* Msg 0x01A8	Reserved											*/
	NULL,								/* Msg 0x01A9	Reserved											*/
	NULL,								/* Msg 0x01AA	Reserved											*/
	NULL,								/* Msg 0x01AB	Reserved											*/
	NULL,								/* Msg 0x01AC	Reserved											*/
	NULL,								/* Msg 0x01AD	Reserved											*/
	NULL,								/* Msg 0x01AE	Reserved											*/
	NULL,								/* Msg 0x01AF	Reserved											*/
	NULL,								/* Msg 0x01B0	Reserved											*/
	NULL,								/* Msg 0x01B1	Reserved											*/
	NULL,								/* Msg 0x01B2	Reserved											*/
	NULL,								/* Msg 0x01B3	Reserved											*/
	NULL,								/* Msg 0x01B4	Reserved											*/
	NULL,								/* Msg 0x01B5	Reserved											*/
	NULL,								/* Msg 0x01B6	Reserved											*/
	NULL,								/* Msg 0x01B7	Reserved											*/
	NULL,								/* Msg 0x01B8	Reserved											*/
	NULL,								/* Msg 0x01B9	Reserved											*/
	NULL,								/* Msg 0x01BA	Reserved											*/
	NULL,								/* Msg 0x01BB	Reserved											*/
	NULL,								/* Msg 0x01BC	Reserved											*/
	NULL,								/* Msg 0x01BD	Reserved											*/
	NULL,								/* Msg 0x01BE	Reserved											*/
	NULL,								/* Msg 0x01BF	Reserved											*/
	NULL,								/* Msg 0x01C0	Reserved											*/
	NULL,								/* Msg 0x01C1	Reserved											*/
	NULL,								/* Msg 0x01C2	Reserved											*/
	NULL,								/* Msg 0x01C3	Reserved											*/
	NULL,								/* Msg 0x01C4	Reserved											*/
	NULL,								/* Msg 0x01C5	Reserved											*/
	NULL,								/* Msg 0x01C6	Reserved											*/
	NULL,								/* Msg 0x01C7	Reserved											*/
	NULL,								/* Msg 0x01C8	Reserved											*/
	NULL,								/* Msg 0x01C9	Reserved											*/
	NULL,								/* Msg 0x01CA	Reserved											*/
	NULL,								/* Msg 0x01CB	Reserved											*/
	NULL,								/* Msg 0x01CC	Reserved											*/
	NULL,								/* Msg 0x01CD	Reserved											*/
	NULL,								/* Msg 0x01CE	Reserved											*/
	NULL,								/* Msg 0x01CF	Reserved											*/
	NULL,								/* Msg 0x01D0	Reserved											*/
	NULL,								/* Msg 0x01D1	Reserved											*/
	NULL,								/* Msg 0x01D2	Reserved											*/
	NULL,								/* Msg 0x01D3	Reserved											*/
	NULL,								/* Msg 0x01D4	Reserved											*/
	NULL,								/* Msg 0x01D5	Reserved											*/
	NULL,								/* Msg 0x01D6	Reserved											*/
	NULL,								/* Msg 0x01D7	Reserved											*/
	NULL,								/* Msg 0x01D8	Reserved											*/
	NULL,								/* Msg 0x01D9	Reserved											*/
	NULL,								/* Msg 0x01DA	Reserved											*/
	NULL,								/* Msg 0x01DB	Reserved											*/
	NULL,								/* Msg 0x01DC	Reserved											*/
	NULL,								/* Msg 0x01DD	Reserved											*/
	NULL,								/* Msg 0x01DE	Reserved											*/
	NULL,								/* Msg 0x01DF	Reserved											*/
	NULL,								/* Msg 0x01E0	Reserved											*/
	NULL,								/* Msg 0x01E1	Reserved											*/
	NULL,								/* Msg 0x01E2	Reserved											*/
	NULL,								/* Msg 0x01E3	Reserved											*/
	NULL,								/* Msg 0x01E4	Reserved											*/
	NULL,								/* Msg 0x01E5	Reserved											*/
	NULL,								/* Msg 0x01E6	Reserved											*/
	NULL,								/* Msg 0x01E7	Reserved											*/
	NULL,								/* Msg 0x01E8	Reserved											*/
	NULL,								/* Msg 0x01E9	Reserved											*/
	NULL,								/* Msg 0x01EA	Reserved											*/
	NULL,								/* Msg 0x01EB	Reserved											*/
	NULL,								/* Msg 0x01EC	Reserved											*/
	NULL,								/* Msg 0x01ED	Reserved											*/
	NULL,								/* Msg 0x01EE	Reserved											*/
	NULL,								/* Msg 0x01EF	Reserved											*/
	NULL,								/* Msg 0x01F0	Reserved											*/
	NULL,								/* Msg 0x01F1	Reserved											*/
	NULL,								/* Msg 0x01F2	Reserved											*/
	NULL,								/* Msg 0x01F3	Reserved											*/
	NULL,								/* Msg 0x01F4	Reserved											*/
	NULL,								/* Msg 0x01F5	Reserved											*/
	NULL,								/* Msg 0x01F6	Reserved											*/
	NULL,								/* Msg 0x01F7	Reserved											*/
	NULL,								/* Msg 0x01F8	Reserved											*/
	NULL,								/* Msg 0x01F9	Reserved											*/
	NULL,								/* Msg 0x01FA	Reserved											*/
	NULL,								/* Msg 0x01FB	Reserved											*/
	NULL,								/* Msg 0x01FC	Reserved											*/
	NULL,								/* Msg 0x01FD	Reserved											*/
	NULL,								/* Msg 0x01FE	Reserved											*/
	NULL,								/* Msg 0x01FF	Reserved											*/
	
#ifndef LOADER	
	
	/* Parameter class messages */
	
	MsgGetHandle,						/* Msg 0x0200	Obtain parameter handle								*/
	MsgGetParType,						/* Msg 0x0201	Determine parameter type							*/
	MsgGetParFlags,						/* Msg 0x0202 	Determine parameter flags							*/
	MsgGetParMin4,						/* Msg 0x0203	Determine parameter minimum (payload<=4)			*/
	MsgGetParMax4,						/* Msg 0x0204	Determine parameter maximum (payload<=4)			*/
	MsgSetParValue4,					/* Msg 0x0205	Set parameter value (payload<=4)					*/
	MsgGetParValue4,					/* Msg 0x0206	Get parameter value (payload<=4)					*/
	MsgSetParValue256,					/* Msg 0x0207	Set parameter value (payload<=256)					*/
	MsgGetParValue256,					/* Msg 0x0208	Get parameter value (payload<=256)					*/
	MsgParSpecial256,					/* Msg 0x0209	Perform parameter special function (payload<=256)	*/
	MsgGetParMember256,					/* Msg 0x020A	Get parameter sub-member name (payload<=256)		*/
	fn_send_parameter_string,			/* Msg 0x020B	Send parameter string								*/
	NULL,								/* Msg 0x020C	Return parameter string								*/
	MsgGetParLive4,						/* Msg 0x020D	Get live parameter value							*/
	MsgGetParUnits256,					/* Msg 0x020E	Get parameter units									*/
	MsgAbortAdjustment,					/* Msg 0x020F	Abort adjustment									*/
	MsgGetParAddr4,						/* Msg 0x0210	Get parameter DSPB address							*/
	MsgSetEvent,						/* Msg 0x0211	Set parameter event report options					*/
	MsgSetInstant,						/* Msg 0x0212	Set parameter value instant							*/
	NULL,								/* Msg 0x0213	Reserved											*/
	NULL,								/* Msg 0x0214	Reserved											*/
	NULL,								/* Msg 0x0215	Reserved											*/
	NULL,								/* Msg 0x0216	Reserved											*/
	NULL,								/* Msg 0x0217	Reserved											*/
	NULL,								/* Msg 0x0218	Reserved											*/
	NULL,								/* Msg 0x0219	Reserved											*/
	NULL,								/* Msg 0x021A	Reserved											*/
	NULL,								/* Msg 0x021B	Reserved											*/
	NULL,								/* Msg 0x021C	Reserved											*/
	NULL,								/* Msg 0x021D	Reserved											*/
	NULL,								/* Msg 0x021E	Reserved											*/
	NULL,								/* Msg 0x021F	Reserved											*/
	NULL,								/* Msg 0x0220	Reserved											*/
	NULL,								/* Msg 0x0221	Reserved											*/
	NULL,								/* Msg 0x0222	Reserved											*/
	NULL,								/* Msg 0x0223	Reserved											*/
	NULL,								/* Msg 0x0224	Reserved											*/
	NULL,								/* Msg 0x0225	Reserved											*/
	NULL,								/* Msg 0x0226	Reserved											*/
	NULL,								/* Msg 0x0227	Reserved											*/
	NULL,								/* Msg 0x0228	Reserved											*/
	NULL,								/* Msg 0x0229	Reserved											*/
	NULL,								/* Msg 0x022A	Reserved											*/
	NULL,								/* Msg 0x022B	Reserved											*/
	NULL,								/* Msg 0x022C	Reserved											*/
	NULL,								/* Msg 0x022D	Reserved											*/
	NULL,								/* Msg 0x022E	Reserved											*/
	NULL,								/* Msg 0x022F	Reserved											*/
	NULL,								/* Msg 0x0230	Reserved											*/
	NULL,								/* Msg 0x0231	Reserved											*/
	NULL,								/* Msg 0x0232	Reserved											*/
	NULL,								/* Msg 0x0233	Reserved											*/
	NULL,								/* Msg 0x0234	Reserved											*/
	NULL,								/* Msg 0x0235	Reserved											*/
	NULL,								/* Msg 0x0236	Reserved											*/
	NULL,								/* Msg 0x0237	Reserved											*/
	NULL,								/* Msg 0x0238	Reserved											*/
	NULL,								/* Msg 0x0239	Reserved											*/
	NULL,								/* Msg 0x023A	Reserved											*/
	NULL,								/* Msg 0x023B	Reserved											*/
	NULL,								/* Msg 0x023C	Reserved											*/
	NULL,								/* Msg 0x023D	Reserved											*/
	NULL,								/* Msg 0x023E	Reserved											*/
	NULL,								/* Msg 0x023F	Reserved											*/
	NULL,								/* Msg 0x0240	Reserved											*/
	NULL,								/* Msg 0x0241	Reserved											*/
	NULL,								/* Msg 0x0242	Reserved											*/
	NULL,								/* Msg 0x0243	Reserved											*/
	NULL,								/* Msg 0x0244	Reserved											*/
	NULL,								/* Msg 0x0245	Reserved											*/
	NULL,								/* Msg 0x0246	Reserved											*/
	NULL,								/* Msg 0x0247	Reserved											*/
	NULL,								/* Msg 0x0248	Reserved											*/
	NULL,								/* Msg 0x0249	Reserved											*/
	NULL,								/* Msg 0x024A	Reserved											*/
	NULL,								/* Msg 0x024B	Reserved											*/
	NULL,								/* Msg 0x024C	Reserved											*/
	NULL,								/* Msg 0x024D	Reserved											*/
	NULL,								/* Msg 0x024E	Reserved											*/
	NULL,								/* Msg 0x024F	Reserved											*/
	NULL,								/* Msg 0x0250	Reserved											*/
	NULL,								/* Msg 0x0251	Reserved											*/
	NULL,								/* Msg 0x0252	Reserved											*/
	NULL,								/* Msg 0x0253	Reserved											*/
	NULL,								/* Msg 0x0254	Reserved											*/
	NULL,								/* Msg 0x0255	Reserved											*/
	NULL,								/* Msg 0x0256	Reserved											*/
	NULL,								/* Msg 0x0257	Reserved											*/
	NULL,								/* Msg 0x0258	Reserved											*/
	NULL,								/* Msg 0x0259	Reserved											*/
	NULL,								/* Msg 0x025A	Reserved											*/
	NULL,								/* Msg 0x025B	Reserved											*/
	NULL,								/* Msg 0x025C	Reserved											*/
	NULL,								/* Msg 0x025D	Reserved											*/
	NULL,								/* Msg 0x025E	Reserved											*/
	NULL,								/* Msg 0x025F	Reserved											*/
	NULL,								/* Msg 0x0260	Reserved											*/
	NULL,								/* Msg 0x0261	Reserved											*/
	NULL,								/* Msg 0x0262	Reserved											*/
	NULL,								/* Msg 0x0263	Reserved											*/
	NULL,								/* Msg 0x0264	Reserved											*/
	NULL,								/* Msg 0x0265	Reserved											*/
	NULL,								/* Msg 0x0266	Reserved											*/
	NULL,								/* Msg 0x0267	Reserved											*/
	NULL,								/* Msg 0x0268	Reserved											*/
	NULL,								/* Msg 0x0269	Reserved											*/
	NULL,								/* Msg 0x026A	Reserved											*/
	NULL,								/* Msg 0x026B	Reserved											*/
	NULL,								/* Msg 0x026C	Reserved											*/
	NULL,								/* Msg 0x026D	Reserved											*/
	NULL,								/* Msg 0x026E	Reserved											*/
	NULL,								/* Msg 0x026F	Reserved											*/
	NULL,								/* Msg 0x0270	Reserved											*/
	NULL,								/* Msg 0x0271	Reserved											*/
	NULL,								/* Msg 0x0272	Reserved											*/
	NULL,								/* Msg 0x0273	Reserved											*/
	NULL,								/* Msg 0x0274	Reserved											*/
	NULL,								/* Msg 0x0275	Reserved											*/
	NULL,								/* Msg 0x0276	Reserved											*/
	NULL,								/* Msg 0x0277	Reserved											*/
	NULL,								/* Msg 0x0278	Reserved											*/
	NULL,								/* Msg 0x0279	Reserved											*/
	NULL,								/* Msg 0x027A	Reserved											*/
	NULL,								/* Msg 0x027B	Reserved											*/
	NULL,								/* Msg 0x027C	Reserved											*/
	NULL,								/* Msg 0x027D	Reserved											*/
	NULL,								/* Msg 0x027E	Reserved											*/
	NULL,								/* Msg 0x027F	Reserved											*/
	NULL,								/* Msg 0x0280	Reserved											*/
	NULL,								/* Msg 0x0281	Reserved											*/
	NULL,								/* Msg 0x0282	Reserved											*/
	NULL,								/* Msg 0x0283	Reserved											*/
	NULL,								/* Msg 0x0284	Reserved											*/
	NULL,								/* Msg 0x0285	Reserved											*/
	NULL,								/* Msg 0x0286	Reserved											*/
	NULL,								/* Msg 0x0287	Reserved											*/
	NULL,								/* Msg 0x0288	Reserved											*/
	NULL,								/* Msg 0x0289	Reserved											*/
	NULL,								/* Msg 0x028A	Reserved											*/
	NULL,								/* Msg 0x028B	Reserved											*/
	NULL,								/* Msg 0x028C	Reserved											*/
	NULL,								/* Msg 0x028D	Reserved											*/
	NULL,								/* Msg 0x028E	Reserved											*/
	NULL,								/* Msg 0x028F	Reserved											*/
	NULL,								/* Msg 0x0290	Reserved											*/
	NULL,								/* Msg 0x0291	Reserved											*/
	NULL,								/* Msg 0x0292	Reserved											*/
	NULL,								/* Msg 0x0293	Reserved											*/
	NULL,								/* Msg 0x0294	Reserved											*/
	NULL,								/* Msg 0x0295	Reserved											*/
	NULL,								/* Msg 0x0296	Reserved											*/
	NULL,								/* Msg 0x0297	Reserved											*/
	NULL,								/* Msg 0x0298	Reserved											*/
	NULL,								/* Msg 0x0299	Reserved											*/
	NULL,								/* Msg 0x029A	Reserved											*/
	NULL,								/* Msg 0x029B	Reserved											*/
	NULL,								/* Msg 0x029C	Reserved											*/
	NULL,								/* Msg 0x029D	Reserved											*/
	NULL,								/* Msg 0x029E	Reserved											*/
	NULL,								/* Msg 0x029F	Reserved											*/
	NULL,								/* Msg 0x02A0	Reserved											*/
	NULL,								/* Msg 0x02A1	Reserved											*/
	NULL,								/* Msg 0x02A2	Reserved											*/
	NULL,								/* Msg 0x02A3	Reserved											*/
	NULL,								/* Msg 0x02A4	Reserved											*/
	NULL,								/* Msg 0x02A5	Reserved											*/
	NULL,								/* Msg 0x02A6	Reserved											*/
	NULL,								/* Msg 0x02A7	Reserved											*/
	NULL,								/* Msg 0x02A8	Reserved											*/
	NULL,								/* Msg 0x02A9	Reserved											*/
	NULL,								/* Msg 0x02AA	Reserved											*/
	NULL,								/* Msg 0x02AB	Reserved											*/
	NULL,								/* Msg 0x02AC	Reserved											*/
	NULL,								/* Msg 0x02AD	Reserved											*/
	NULL,								/* Msg 0x02AE	Reserved											*/
	NULL,								/* Msg 0x02AF	Reserved											*/
	NULL,								/* Msg 0x02B0	Reserved											*/
	NULL,								/* Msg 0x02B1	Reserved											*/
	NULL,								/* Msg 0x02B2	Reserved											*/
	NULL,								/* Msg 0x02B3	Reserved											*/
	NULL,								/* Msg 0x02B4	Reserved											*/
	NULL,								/* Msg 0x02B5	Reserved											*/
	NULL,								/* Msg 0x02B6	Reserved											*/
	NULL,								/* Msg 0x02B7	Reserved											*/
	NULL,								/* Msg 0x02B8	Reserved											*/
	NULL,								/* Msg 0x02B9	Reserved											*/
	NULL,								/* Msg 0x02BA	Reserved											*/
	NULL,								/* Msg 0x02BB	Reserved											*/
	NULL,								/* Msg 0x02BC	Reserved											*/
	NULL,								/* Msg 0x02BD	Reserved											*/
	NULL,								/* Msg 0x02BE	Reserved											*/
	NULL,								/* Msg 0x02BF	Reserved											*/
	NULL,								/* Msg 0x02C0	Reserved											*/
	NULL,								/* Msg 0x02C1	Reserved											*/
	NULL,								/* Msg 0x02C2	Reserved											*/
	NULL,								/* Msg 0x02C3	Reserved											*/
	NULL,								/* Msg 0x02C4	Reserved											*/
	NULL,								/* Msg 0x02C5	Reserved											*/
	NULL,								/* Msg 0x02C6	Reserved											*/
	NULL,								/* Msg 0x02C7	Reserved											*/
	NULL,								/* Msg 0x02C8	Reserved											*/
	NULL,								/* Msg 0x02C9	Reserved											*/
	NULL,								/* Msg 0x02CA	Reserved											*/
	NULL,								/* Msg 0x02CB	Reserved											*/
	NULL,								/* Msg 0x02CC	Reserved											*/
	NULL,								/* Msg 0x02CD	Reserved											*/
	NULL,								/* Msg 0x02CE	Reserved											*/
	NULL,								/* Msg 0x02CF	Reserved											*/
	NULL,								/* Msg 0x02D0	Reserved											*/
	NULL,								/* Msg 0x02D1	Reserved											*/
	NULL,								/* Msg 0x02D2	Reserved											*/
	NULL,								/* Msg 0x02D3	Reserved											*/
	NULL,								/* Msg 0x02D4	Reserved											*/
	NULL,								/* Msg 0x02D5	Reserved											*/
	NULL,								/* Msg 0x02D6	Reserved											*/
	NULL,								/* Msg 0x02D7	Reserved											*/
	NULL,								/* Msg 0x02D8	Reserved											*/
	NULL,								/* Msg 0x02D9	Reserved											*/
	NULL,								/* Msg 0x02DA	Reserved											*/
	NULL,								/* Msg 0x02DB	Reserved											*/
	NULL,								/* Msg 0x02DC	Reserved											*/
	NULL,								/* Msg 0x02DD	Reserved											*/
	NULL,								/* Msg 0x02DE	Reserved											*/
	NULL,								/* Msg 0x02DF	Reserved											*/
	NULL,								/* Msg 0x02E0	Reserved											*/
	NULL,								/* Msg 0x02E1	Reserved											*/
	NULL,								/* Msg 0x02E2	Reserved											*/
	NULL,								/* Msg 0x02E3	Reserved											*/
	NULL,								/* Msg 0x02E4	Reserved											*/
	NULL,								/* Msg 0x02E5	Reserved											*/
	NULL,								/* Msg 0x02E6	Reserved											*/
	NULL,								/* Msg 0x02E7	Reserved											*/
	NULL,								/* Msg 0x02E8	Reserved											*/
	NULL,								/* Msg 0x02E9	Reserved											*/
	NULL,								/* Msg 0x02EA	Reserved											*/
	NULL,								/* Msg 0x02EB	Reserved											*/
	NULL,								/* Msg 0x02EC	Reserved											*/
	NULL,								/* Msg 0x02ED	Reserved											*/
	NULL,								/* Msg 0x02EE	Reserved											*/
	NULL,								/* Msg 0x02EF	Reserved											*/
	NULL,								/* Msg 0x02F0	Reserved											*/
	NULL,								/* Msg 0x02F1	Reserved											*/
	NULL,								/* Msg 0x02F2	Reserved											*/
	NULL,								/* Msg 0x02F3	Reserved											*/
	NULL,								/* Msg 0x02F4	Reserved											*/
	NULL,								/* Msg 0x02F5	Reserved											*/
	NULL,								/* Msg 0x02F6	Reserved											*/
	NULL,								/* Msg 0x02F7	Reserved											*/
	NULL,								/* Msg 0x02F8	Reserved											*/
	NULL,								/* Msg 0x02F9	Reserved											*/
	NULL,								/* Msg 0x02FA	Reserved											*/
	NULL,								/* Msg 0x02FB	Reserved											*/
	NULL,								/* Msg 0x02FC	Reserved											*/
	NULL,								/* Msg 0x02FD	Reserved											*/
	NULL,								/* Msg 0x02FE	Reserved											*/
	NULL,								/* Msg 0x02FF	Reserved											*/
	
	/* Standard messages */
	
	fn_set_excitation,					/* Msg 0x0300	Set excitation										*/
	fn_read_excitation,					/* Msg 0x0301	Read excitation										*/
	NULL,								/* Msg 0x0302	Return excitation									*/
	fn_set_gain,						/* Msg 0x0303	Set gain											*/
	fn_read_gain,						/* Msg 0x0304	Read gain											*/
	NULL,								/* Msg 0x0305	Return gain											*/
	NULL,								/* Msg 0x0306	Not used											*/
	fn_set_cal,							/* Msg 0x0307	Set shunt calibration state							*/
	fn_set_led,							/* Msg 0x0308	Set LED state										*/
	fn_set_filter,						/* Msg 0x0309	Set filter											*/
	fn_read_filter,						/* Msg 0x030A	Read filter											*/
	NULL,								/* Msg 0x030B	Return filter										*/
	fn_read_ip_channel,					/* Msg 0x030C	Read input channel									*/
	NULL,								/* Msg 0x030D	Return input channel								*/
	fn_write_op_channel,				/* Msg 0x030E	Write output channel								*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_set_hydraulic_output,			/* Msg 0x030F	Set hydraulic output								*/
	fn_read_hydraulic_output,			/* Msg 0x0310	Read hydraulic output								*/
	NULL,								/* Msg 0x0311	Return hydraulic output								*/
	fn_read_hydraulic_input,			/* Msg 0x0312	Read hydraulic input								*/
	NULL,								/* Msg 0x0313	Return hydraulic input								*/
#else
	NULL,								/* Msg 0x030F	Set hydraulic output								*/
	NULL,								/* Msg 0x0310	Read hydraulic output								*/
	NULL,								/* Msg 0x0311	Return hydraulic output								*/
	NULL,								/* Msg 0x0312	Read hydraulic input								*/
	NULL,								/* Msg 0x0313	Return hydraulic input								*/
#endif
	fn_read_slot_configuration,			/* Msg 0x0314	Read slot configuration								*/
	NULL,								/* Msg 0x0315	Return slot configuration							*/
	fn_read_chan_information,			/* Msg 0x0316	Read channel information							*/
	NULL,								/* Msg 0x0317	Return channel information							*/
	fn_set_raw_gain,					/* Msg 0x0318	Set raw gain										*/
	fn_read_raw_gain,					/* Msg 0x0319	Read raw gain										*/
	NULL,								/* Msg 0x031A	Return raw gain										*/
	fn_set_calgain,						/* Msg 0x031B	Set calibration gain table value					*/
	fn_read_calgain,					/* Msg 0x031C	Read calibration gain table value					*/
	NULL,								/* Msg 0x031D	Return calibration gain table value					*/
	fn_set_caloffset,					/* Msg 0x031E	Set calibration offset value						*/
	fn_read_caloffset,					/* Msg 0x031F	Read calibration offset value 						*/
	NULL,								/* Msg 0x0320	Return calibration offset value						*/
	fn_save_chan_config,				/* Msg 0x0321	Save channel configuration							*/
	fn_restore_chan_config,				/* Msg 0x0322	Restore channel configuration						*/
	fn_save_chan_calibration,			/* Msg 0x0323	Save channel calibration							*/
	fn_restore_chan_calibration,		/* Msg 0x0324	Restore channel calibration							*/
	fn_read_cnet_channel,				/* Msg 0x0325	Read CNet channel									*/
	NULL,								/* Msg 0x0326	Return CNet channel									*/
	fn_set_cnet_slot,					/* Msg 0x0327	Set CNet slot										*/
	NULL,								/* Msg 0x0328	Not used											*/
	fn_read_cnet_status,				/* Msg 0x0329	Read CNet status									*/
	NULL,								/* Msg 0x032A	Return CNet status									*/
	fn_set_exc_cal,						/* Msg 0x032B	Set excitation calibration							*/
	fn_read_exc_cal,					/* Msg 0x032C	Read excitation	calibration							*/
	NULL,								/* Msg 0x032D	Return excitation calibration						*/
	fn_set_gaintrim,					/* Msg 0x032E	Set gain trim value									*/
	fn_read_gaintrim,					/* Msg 0x032F	Read gain trim value 								*/
	NULL,								/* Msg 0x0330	Return gain trim value								*/
	fn_flush_chan_config,				/* Msg 0x0331	Flush channel configuration							*/
	fn_flush_chan_calibration,			/* Msg 0x0332	Flush channel calibration							*/
	fn_read_chan_status,				/* Msg 0x0333	Read channel status									*/
	NULL,								/* Msg 0x0334	Return channel status								*/
	NULL,								/* Msg 0x0335	Set channel limit value								*/
	NULL,								/* Msg 0x0336	Read channel limit value							*/
	NULL,								/* Msg 0x0337	Return channel limit value							*/
	NULL,								/* Msg 0x0338	Clear channel limit flags							*/
	fn_set_chan_flags,					/* Msg 0x0339	Set channel flags									*/
	fn_read_chan_flags,					/* Msg 0x033A	Read channel flags									*/
	NULL,								/* Msg 0x033B	Return channel flags								*/
	fn_read_peaks,						/* Msg 0x033C	Read channel peaks									*/
	NULL,								/* Msg 0x033D	Return channel peaks								*/
	fn_reset_peaks,						/* Msg 0x033E	Reset channel peak readers							*/
	fn_write_map,						/* Msg 0x033F	Write linearisation map entry						*/
	fn_read_map,						/* Msg 0x0340	Read linearisation map entry						*/
	NULL,								/* Msg 0x0341	Return linearisation map entry						*/
	fn_set_refzero,						/* Msg 0x0342	Write channel refzero value							*/
	fn_read_refzero,					/* Msg 0x0343	Read channel refzero value							*/
	NULL,								/* Msg 0x0344	Return channel tare value							*/

#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_set_sv_range,					/* Msg 0x0345	Set SV current range								*/
	fn_read_sv_range,					/* Msg 0x0346	Read SV current range								*/
	NULL,								/* Msg 0x0347	Return SV current range								*/
	fn_define_hydraulic_io,				/* Msg 0x0348	Set hydraulic I/O definition						*/
	fn_read_hydraulic_io,				/* Msg 0x0349	Read hydraulic I/O definition						*/
	NULL,								/* Msg 0x034A	Return hydraulic I/O definition						*/
	fn_set_raw_current,					/* Msg 0x034B	Set raw current adjustment							*/
	
	fn_read_hydraulic_state,			/* Msg 0x034C	Read hydraulic state								*/
	NULL,								/* Msg 0x034D	Return hydraulic state								*/
#else
	NULL,								/* Msg 0x0345	Set SV current range								*/
	NULL,								/* Msg 0x0346	Read SV current range								*/
	NULL,								/* Msg 0x0347	Return SV current range								*/
	NULL,								/* Msg 0x0348	Set hydraulic I/O definition						*/
	NULL,								/* Msg 0x0349	Read hydraulic I/O definition						*/
	NULL,								/* Msg 0x034A	Return hydraulic I/O definition						*/
	NULL,								/* Msg 0x034B	Set raw current adjustment							*/
	
	NULL,								/* Msg 0x034C	Read hydraulic state								*/
	NULL,								/* Msg 0x034D	Return hydraulic state								*/
#endif

	fn_set_raw_excitation,				/* Msg 0x034E	Set raw excitation									*/

#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_hydraulic_control,				/* Msg 0x034F	Control hydraulic state								*/
#else
	NULL,								/* Msg 0x034F	Control hydraulic state								*/
#endif

	fn_read_sysinfo,					/* Msg 0x0350	Read system information								*/
	NULL,								/* Msg 0x0351	Return system information							*/

	fn_set_caltable,					/* Msg 0x0352	Set calibration table value							*/
	fn_read_caltable,					/* Msg 0x0353	Read calibration table value						*/
	NULL,								/* Msg 0x0354	Return calibration table value						*/

	fn_read_serno,						/* Msg 0x0355	Read controller serial number						*/
	NULL,								/* Msg 0x0356	Return serial number								*/
	fn_define_output_list,				/* Msg 0x0357	Define realtime output channel list					*/
	fn_define_input_list,				/* Msg 0x0358	Define realtime input channel list					*/
	fn_read_total_buffer,				/* Msg 0x0359	Read total buffer space								*/
	NULL,								/* Msg 0x035A	Return total buffer space							*/
	fn_read_output_buffer,				/* Msg 0x035B	Read output buffer space							*/
	NULL,								/* Msg 0x035C	Return output buffer space							*/
	fn_read_input_buffer,				/* Msg 0x035D	Read input buffer space								*/
	NULL,								/* Msg 0x035E	Return input buffer space							*/
	fn_define_output_buffer,			/* Msg 0x035F	Define output buffer space							*/
	fn_define_input_buffer,				/* Msg 0x0360	Define input buffer space							*/
	fn_read_buffer_status,				/* Msg 0x0361	Read buffer status									*/
	NULL,								/* Msg 0x0362	Return buffer status								*/
	fn_clear_error_flags,				/* Msg 0x0363	Clear error flags									*/
	fn_control_realtime_transfer,		/* Msg 0x0364	Control realtime transfer							*/
	fn_pause_realtime_transfer,			/* Msg 0x0365	Pause realtime transfer								*/
	fn_resume_realtime_transfer,		/* Msg 0x0366	Resume realtime transfer							*/
	fn_transfer_realtime_data,			/* Msg 0x0367	Transfer realtime data								*/
	NULL,								/* Msg 0x0368	Return realtime data								*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_send_kinet_message,				/* Msg 0x0369	Send KiNet message									*/
	NULL,								/* Msg 0x036A	Return KiNet message								*/
#else
	NULL,								/* Msg 0x0369	Send KiNet message									*/
	NULL,								/* Msg 0x036A	Return KiNet message								*/
#endif

	fn_define_system_name,				/* Msg 0x036B	Define system name									*/
	fn_read_system_name,				/* Msg 0x036C	Read system name									*/
	NULL,								/* Msg 0x036D	Return system name									*/
	fn_define_comm_timeout,				/* Msg 0x036E	Define comm timeout									*/
	fn_read_comm_timeout,				/* Msg 0x036F	Read comm timeout									*/
	NULL,								/* Msg 0x0370	Return comm timeout									*/

#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_set_kinet_mapping,				/* Msg 0x0371	Set KiNet mapping									*/
	fn_read_kinet_mapping,				/* Msg 0x0372	Read KiNet mapping									*/
	NULL,								/* Msg 0x0373	Return KiNet mapping								*/
#else
	NULL,								/* Msg 0x0371	Set KiNet mapping									*/
	NULL,								/* Msg 0x0372	Read KiNet mapping									*/
	NULL,								/* Msg 0x0373	Return KiNet mapping								*/
#endif

	NULL,								/* Msg 0x0374	Not used											*/
	NULL,								/* Msg 0x0375	Not used											*/
	NULL,								/* Msg 0x0376	Not used											*/
	fn_set_cnet_maptable,				/* Msg 0x0377	Set CNet map table									*/
	fn_read_cnet_maptable,				/* Msg 0x0378	Read CNet map table									*/
	NULL,								/* Msg 0x0379	Return CNet map table								*/
	fn_set_rt_stop_type,				/* Msg 0x037A	Set realtime stop type								*/
	fn_read_rt_stop_type,				/* Msg 0x037B	Read realtime stop type								*/
	NULL,								/* Msg 0x037C	Return realtime stop type							*/
	fn_set_rt_stop_parameters,			/* Msg 0x037D	Set realtime stop parameters						*/
	fn_read_rt_stop_parameters,			/* Msg 0x037E	Read realtime stop parameters						*/
	NULL,								/* Msg 0x037F	Return realtime stop parameters						*/
	NULL,								/* Msg 0x0380	Set limit persistance								*/
	NULL,								/* Msg 0x0381	Read limit persistance								*/
	NULL,								/* Msg 0x0382	Return limit persistance							*/
	NULL,								/* Msg 0x0383	Set limit action									*/
	NULL,								/* Msg 0x0384	Read limit action									*/
	NULL,								/* Msg 0x0385	Return limit action									*/
	NULL,								/* Msg 0x0386	Enable limits										*/
	NULL,								/* Msg 0x0387	Disable limits										*/
	NULL,								/* Msg 0x0388	Read limit enable									*/
	NULL,								/* Msg 0x0389	Return limit enable									*/
	fn_reset_all_peaks,					/* Msg 0x038A	Reset all peaks										*/
	fn_set_reset_parameters,			/* Msg 0x038B	Set reset parameters								*/
	fn_read_reset_parameters,			/* Msg 0x038C	Read reset parameters								*/
	NULL,								/* Msg 0x038D	Return reset parameters								*/
	fn_set_sv_dither,					/* Msg 0x038E	Set dither											*/
	fn_read_sv_dither,					/* Msg 0x038F	Read dither											*/
	NULL,								/* Msg 0x0390	Return dither										*/
	fn_set_sv_balance,					/* Msg 0x0391	Set valve balance									*/
	fn_read_sv_balance,					/* Msg 0x0392	Read valve balance									*/
	NULL,								/* Msg 0x0393	Return valve balance								*/
	fn_read_chan_memptrs,				/* Msg 0x0394	Read chan definition memory pointers				*/
	NULL,								/* Msg 0x0395	Return chan definition memory pointers				*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_set_kinet_flags,					/* Msg 0x0396	Set KiNet flags										*/
	fn_read_kinet_flags,				/* Msg 0x0397	Read KiNet flags									*/
	NULL,								/* Msg 0x0398	Return KiNet flags									*/
#else
	NULL,								/* Msg 0x0396	Set KiNet flags										*/
	NULL,								/* Msg 0x0397	Read KiNet flags									*/
	NULL,								/* Msg 0x0398	Return KiNet flags									*/
#endif

	fn_set_realtime_config,				/* Msg 0x0399	Set realtime config									*/
	fn_read_realtime_config,			/* Msg 0x039A	Read realtime config								*/
	NULL,								/* Msg 0x039B	Return realtime config								*/
	fn_read_output_list,				/* Msg 0x039C	Read realtime output channel list					*/
	NULL,								/* Msg 0x039D	Return realtime output channel list					*/
	fn_read_input_list,					/* Msg 0x039E	Read realtime input channel list					*/
	NULL,								/* Msg 0x039F	Return realtime input channel list					*/
	fn_refzero_zero,					/* Msg 0x03A0	Reference zero										*/
	fn_set_serno,						/* Msg 0x03A1	Set controller serial number						*/
	fn_read_nv_state,					/* Msg 0x03A2	Read NV state										*/
	NULL,								/* Msg 0x03A3	Return NV state										*/
	fn_set_panel_config,				/* Msg 0x03A4	Set panel configuration table						*/
	fn_read_panel_config,				/* Msg 0x03A5	Read panel configuration table						*/
	NULL,								/* Msg 0x03A6	Return panel configuration table					*/
	fn_nv_control,						/* Msg 0x03A7	Control NV operation								*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_define_hydraulic_mode,			/* Msg 0x03A8	Define hydraulic mode								*/
	fn_read_hydraulic_mode,				/* Msg 0x03A9	Read hydraulic mode									*/
	NULL,								/* Msg 0x03AA	Return hydraulic mode								*/

	fn_set_digio_info,					/* Msg 0x03AB	Set digital I/O info								*/
	fn_read_digio_info,					/* Msg 0x03AC	Read digital I/O info								*/
	NULL,								/* Msg 0x03AD	Return digital I/O info								*/
	fn_write_digio_output,				/* Msg 0x03AE	Write digital I/O output							*/
	fn_read_digio_input,				/* Msg 0x03AF	Read digital I/O input								*/
	NULL,								/* Msg 0x03B0	Return digital I/O input							*/
	fn_read_digio_output,				/* Msg 0x03B1	Read digital I/O output								*/
	NULL,								/* Msg 0x03B2	Return digital I/O output							*/
#else
	NULL,								/* Msg 0x03A8	Define hydraulic mode								*/
	NULL,								/* Msg 0x03A9	Read hydraulic mode									*/
	NULL,								/* Msg 0x03AA	Return hydraulic mode								*/

	NULL,								/* Msg 0x03AB	Set digital I/O info								*/
	NULL,								/* Msg 0x03AC	Read digital I/O info								*/
	NULL,								/* Msg 0x03AD	Return digital I/O info								*/
	NULL,								/* Msg 0x03AE	Write digital I/O output							*/
	NULL,								/* Msg 0x03AF	Read digital I/O input								*/
	NULL,								/* Msg 0x03B0	Return digital I/O input							*/
	NULL,								/* Msg 0x03B1	Read digital I/O output								*/
	NULL,								/* Msg 0x03B2	Return digital I/O output							*/
#endif

#ifdef SIGNALCUBE
	fn_set_gauge_type,					/* Msg 0x03B3	Set gauge type (Signal Cube only)					*/
	fn_read_gauge_type,					/* Msg 0x03B4	Read gauge type	(Signal Cube only)					*/
	NULL,								/* Msg 0x03B5	Return gauge type (Signal Cube only)				*/
	fn_set_offset,						/* Msg 0x03B6	Set offset (Signal Cube only)						*/
	fn_read_offset,						/* Msg 0x03B7	Read offset	(Signal Cube only)						*/
	NULL,								/* Msg 0x03B8	Return offset (Signal Cube only)					*/
#else
	NULL,								/* Msg 0x03B3	Set gauge type (Signal Cube only)					*/
	NULL,								/* Msg 0x03B4	Read gauge type	(Signal Cube only)					*/
	NULL,								/* Msg 0x03B5	Return gauge type (Signal Cube only)				*/
	NULL,								/* Msg 0x03B6	Set offset (Signal Cube only)						*/
	NULL,								/* Msg 0x03B7	Read offset	(Signal Cube only)						*/
	NULL,								/* Msg 0x03B8	Return offset (Signal Cube only)					*/
#endif

	fn_chan_override,		 			/* Msg 0x03B9	Channel override									*/
	fn_define_rig_name,					/* Msg 0x03BA	Define rig name										*/
	fn_read_rig_name,					/* Msg 0x03BB	Read rig name										*/
	NULL,								/* Msg 0x03BC	Return rig name										*/

#ifdef SIGNALCUBE
	fn_set_active_gauges,				/* Msg 0x03BD	Set active gauge count (Signal Cube only)			*/
	fn_read_active_gauges,				/* Msg 0x03BE	Read active gauge count	(Signal Cube only)			*/
	NULL,								/* Msg 0x03BF	Return active gauge count (Signal Cube only)		*/
#else
	NULL,								/* Msg 0x03BD	Set active gauge count (Signal Cube only)			*/
	NULL,								/* Msg 0x03BE	Read active gauge count	(Signal Cube only)			*/
	NULL,								/* Msg 0x03BF	Return active gauge count (Signal Cube only)		*/
#endif
	fn_read_eventlog_status,			/* Msg 0x03C0	Read eventlog status								*/
	NULL,								/* Msg 0x03C1	Return eventlog status								*/
	fn_flush_eventlog,					/* Msg 0x03C2	Flush eventlog										*/
	fn_read_eventlog_entry,				/* Msg 0x03C3	Read eventlog entry									*/
	NULL,								/* Msg 0x03C4	Return eventlog entry								*/
	fn_read_timestamp,					/* Msg 0x03C5	Read timestamp										*/
	NULL,								/* Msg 0x03C6	Return timestamp									*/

	fn_read_cnet_valid_range,			/* Msg 0x03C7	Read CNet valid range								*/
	NULL,								/* Msg 0x03C8	Return CNet valid range								*/
	
	fn_set_txdrzero,					/* Msg 0x03C9	Set transducer zero value							*/
	fn_read_txdrzero,					/* Msg 0x03CA	Read transducer zero value							*/
	NULL,								/* Msg 0x03CB	Return transducer zero value						*/
	fn_txdrzero_zero,					/* Msg 0x03CC	Zero transducer										*/

	fn_set_filter_freq,					/* Msg 0x03CD	Set filter frequency								*/
	
	fn_set_security_level,				/* Msg 0x03CE	Set security level									*/
	fn_read_security_level,				/* Msg 0x03CF	Read security level									*/
	NULL,								/* Msg 0x03D0	Return security level								*/

#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_set_hydraulic_dissipation,		/* Msg 0x03D1	Set hydraulic dissipation time						*/
	fn_read_hydraulic_dissipation,		/* Msg 0x03D2	Read hydraulic dissipation time						*/
	NULL,								/* Msg 0x03D3	Return hydraulic dissipation time					*/
#else
	NULL,								/* Msg 0x03D1	Set hydraulic dissipation time						*/
	NULL,								/* Msg 0x03D2	Read hydraulic dissipation time						*/
	NULL,								/* Msg 0x03D3	Return hydraulic dissipation time					*/
#endif

#ifdef SIGNALCUBE
	fn_start_zero,						/* Msg 0x03D4	Start zero operation (Signal Cube only)				*/
	fn_read_zero_progress,				/* Msg 0x03D5	Read set zero progress (Signal Cube only)			*/
	NULL,								/* Msg 0x03D6	Return set zero progress (Signal Cube only)			*/
#else
	NULL,								/* Msg 0x03D4	Start zero operation (Signal Cube only)				*/
	NULL,								/* Msg 0x03D5	Read set offset progress (Signal Cube only)			*/
	NULL,								/* Msg 0x03D6	Return set offset progress (Signal Cube only)		*/
#endif

#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_set_hydraulic_name,				/* Msg 0x03D7	Set hydraulic name									*/
	fn_read_hydraulic_name,				/* Msg 0x03D8	Read hydraulic name									*/
	NULL,								/* Msg 0x03D9	Return hydraulic name								*/
#else
	NULL,								/* Msg 0x03D7	Set hydraulic name									*/
	NULL,								/* Msg 0x03D8	Read hydraulic name									*/
	NULL,								/* Msg 0x03D9	Return hydraulic name								*/
#endif

	fn_set_userdata,					/* Msg 0x03DA	Set user data area									*/
	fn_read_userdata,					/* Msg 0x03DB	Read user data area									*/
	NULL,								/* Msg 0x03DC	Return user data area								*/

	fn_set_chanstring,					/* Msg 0x03DD	Set channel user string area						*/
	fn_read_chanstring,					/* Msg 0x03DE	Read channel user string area						*/
	NULL,								/* Msg 0x03DF	Return channel user string area						*/

	fn_read_ip_channelblock,			/* Msg 0x03E0	Read input channel block 							*/
	NULL,								/* Msg 0x03E1	Return input channel block							*/

	fn_set_cnet_timeout,				/* Msg 0x03E2	Set CNet timeout									*/
	fn_read_cnet_timeout,				/* Msg 0x03E3	Read CNet timeout									*/
	NULL,								/* Msg 0x03E4	Return CNet timeout									*/

#ifdef SIGNALCUBE
	fn_set_gauge_info,					/* Msg 0x03E5	Set gauge information (Signal Cube only)			*/
	fn_read_gauge_info,					/* Msg 0x03E6	Read gauge information (Signal Cube only)			*/
	NULL,								/* Msg 0x03E7	Return gauge information (Signal Cube only)			*/
#else
	NULL,								/* Msg 0x03E5	Set gauge information (Signal Cube only)			*/
	NULL,								/* Msg 0x03E6	Read gauge information (Signal Cube only)			*/
	NULL,								/* Msg 0x03E7	Return gauge information (Signal Cube only)			*/
#endif
	fn_undo_refzero,					/* Msg 0x03E8	Undo reference zero									*/
	fn_clear_all_leds,					/* Msg 0x03E9	Clear all LEDs										*/
	fn_set_peak_threshold,				/* Msg 0x03EA	Set peak/trough detection threshold					*/
	fn_read_peak_threshold,				/* Msg 0x03EB	Read peak/trough detection threshold				*/
	NULL,								/* Msg 0x03EC	Return peak/trough detection threshold				*/

	fn_open_event_connection,			/* Msg 0x03ED	Open event connection								*/
	NULL,								/* Msg 0x03EE	Return event connection								*/
	fn_close_event_connection,			/* Msg 0x03EF	Close event connection								*/
	fn_add_event_report,				/* Msg 0x03F0	Add event report									*/
	fn_remove_event_report,				/* Msg 0x03F1	Remove event report									*/

#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_send_hc_instruction,				/* Msg 0x03F2	Send hand controller instruction					*/
	fn_read_hc_keystate,				/* Msg 0x03F3	Read hand controller key state						*/
	NULL,								/* Msg 0x03F4	Return hand controller key state					*/
#else
	NULL,								/* Msg 0x03F2	Send hand controller instruction					*/
	NULL,								/* Msg 0x03F3	Read hand controller key state						*/
	NULL,								/* Msg 0x03F4	Return hand controller key state					*/
#endif

	fn_set_semaphore,					/* Msg 0x03F5	Set semaphore state									*/
	fn_read_semaphore,					/* Msg 0x03F6	Read semaphore state								*/
	NULL,								/* Msg 0x03F7	Return semaphore state								*/

	fn_set_channel_source,				/* Msg 0x03F8	Set channel source pointer							*/

	fn_set_neg_gaintrim,				/* Msg 0x03F9	Set negative  gain trim value						*/
	fn_read_neg_gaintrim,				/* Msg 0x03FA	Read negative gain trim value 						*/
	NULL,								/* Msg 0x03FB	Return negative gain trim value						*/

	fn_read_ip_cyclecount,				/* Msg 0x03FC	Read input channel cycle count						*/
	NULL,								/* Msg 0x03FD	Return input channel cycle count					*/
	fn_read_ip_cyclecountblock,			/* Msg 0x03FE	Read input channel cycle count block				*/
	NULL,								/* Msg 0x03FF	Return input channel cycle count block				*/
	fn_reset_cyclecount,				/* Msg 0x0400	Reset input channel cycle count						*/
	fn_reset_all_cyclecounts,			/* Msg 0x0401	Reset all input channel cycle counts				*/

	fn_set_fault_mask,					/* Msg 0x0402	Set fault mask										*/
	fn_read_fault_state,				/* Msg 0x0403	Read fault state									*/
	NULL,								/* Msg 0x0404	Return fault state									*/
#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_read_guard_state,				/* Msg 0x0405	Read guard state									*/
	NULL,								/* Msg 0x0406	Return guard state									*/
#else
	NULL,								/* Msg 0x0405	Read guard state									*/
	NULL,								/* Msg 0x0406	Return guard state									*/
#endif

	fn_read_peak_threshold_ext,			/* Msg 0x0407	Read peak/trough detection threshold (extended)		*/
	NULL,								/* Msg 0x0408	Return peak/trough detection threshold (extended)	*/

	fn_read_full_caltable,				/* Msg 0x0409	Read full calibration table							*/
	NULL,								/* Msg 0x040A	Return full calibration table						*/

	fn_set_digtxdr_config,				/* Msg 0x040B	Set digital transducer configuration				*/
	fn_read_digtxdr_config,				/* Msg 0x040C	Read digital transducer configuration				*/
	NULL,								/* Msg 0x040D	Return digital transducer configuration				*/

#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_set_simulation_mode,				/* Msg 0x040E	Set simulation mode state							*/
	fn_read_simulation_mode,			/* Msg 0x040F	Read simulation mode state							*/
	NULL,								/* Msg 0x0410	Return simulation mode state						*/
#else
	NULL,								/* Msg 0x040E	Set simulation mode state							*/
	NULL,								/* Msg 0x040F	Read simulation mode state							*/
	NULL,								/* Msg 0x0410	Return simulation mode state						*/
#endif

	fn_set_peak_timeout,				/* Msg 0x0411	Set peak detection timeout							*/
	fn_read_peak_timeout,				/* Msg 0x0412	Read peak detection timout							*/
	NULL,								/* Msg 0x0413	Return peak detection timeout						*/

#ifdef SIGNALCUBE
	fn_set_hpfilter_config,				/* Msg 0x0414	Set HP filter config (Signal Cube only)				*/
	fn_read_hpfilter_config,			/* Msg 0x0415	Read HP filter config (Signal Cube only)			*/
	NULL,								/* Msg 0x0416	Return HP filter config (Signal Cube only)			*/
	fn_set_calzero_entry,				/* Msg 0x0417	Set calibration zero entry (Signal Cube only)		*/
	fn_read_calzero_entry,				/* Msg 0x0418	Read calibration zero entry (Signal Cube only)		*/
	NULL,								/* Msg 0x0419	Return calibration zero entry (Signal Cube only)	*/
#else
	NULL,								/* Msg 0x0414	Set HP filter config (Signal Cube only)				*/
	NULL,								/* Msg 0x0415	Read HP filter config (Signal Cube only)			*/
	NULL,								/* Msg 0x0416	Return HP filter config (Signal Cube only)			*/
	NULL,								/* Msg 0x0417	Set calibration zero entry (Signal Cube only)		*/
	NULL,								/* Msg 0x0418	Read calibration zero entry (Signal Cube only)		*/
	NULL,								/* Msg 0x0419	Return calibration zero entry (Signal Cube only)	*/
#endif

#if (defined(CONTROLCUBE) || defined(AICUBE))
	fn_read_runtime,					/* Msg 0x041A	Read system runtime (Control Cube only)				*/
	NULL,								/* Msg 0x041B	Return system runtime (Control Cube only)			*/
#else
	NULL,								/* Msg 0x041A	Read system runtime (Control Cube only)				*/
	NULL,								/* Msg 0x041B	Return system runtime (Control Cube only)			*/
#endif

	fn_define_comm_timeout_ext,			/* Msg 0x041C	Define comm timeout (extended)						*/
	fn_read_comm_timeout_ext,			/* Msg 0x041D	Read comm timeout (extended)						*/
	NULL,								/* Msg 0x041E	Return comm timeout	(extended)						*/
	fn_set_comm_closing,				/* Msg 0x041F	Indicate connection is closing						*/
	fn_read_comm_id,					/* Msg 0x0420	Read comm id										*/
	NULL,								/* Msg 0x0421	Return comm id										*/
	fn_force_comm_close,				/* Msg 0x0422	Force comm channel to close							*/

	fn_read_ip_channelblock_ext,		/* Msg 0x0423	Read input channel block (extended)					*/
	NULL,								/* Msg 0x0424	Return input channel block (extended)				*/

#endif /* LOADER */	
	
};
