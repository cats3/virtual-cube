/********************************************************************************
 * MODULE NAME       : shared.c													*
 * PROJECT			 : Control Cube												*
 * MODULE DETAILS    : Code to support access to shared data areas				*
 ********************************************************************************/

#include <stddef.h>
#include <stdlib.h>

#include <stdint.h>
#include "porting.h"

#include "channel.h"
#include "system.h"
#include "shared.h"
#include "nonvol.h"

#ifdef _DSPA

#include "hardware.h"
#include "asmcode.h"

#endif

 /* For DSP B, force the shared structure into its own linker section which
	can be placed into on-chip RAM. The variable shared_chandata points to the shared
	structure.
 */

#ifdef _DSPB

//#pragma DATA_SECTION(shared_structure, ".onchip")
//
//shared shared_structure;
//shared* shared_chandata = &shared_structure;

#endif

/* For DSP A, create a local shared structure pointed to by shareptr. The variable
   dspb_shared_chandata points to the address in DSP B on-chip RAM where the real shared
   structure resides.
*/

#ifdef _DSPA

shared shared_structure;
shared* shared_chandata = &shared_structure;
shared* dspb_shared_chandata = NULL;		/* Pointer to start of channel processing shared data area */
void* dspb_shared_c3appdata = NULL;		/* Pointer to start of C3AppData shared area 			   */

#endif



#ifdef _DSPA

/********************************************************************************
  FUNCTION NAME     : shared_initialise
  FUNCTION DETAILS  : Initialise the shared memory area.
********************************************************************************/

void shared_initialise(void)
{
	simple_chandef* no_req = NULL;
	uint32_t timeout = 4096 * 10;

	update_shared(offsetof(shared, cycle_window), &snvbs->cycle_window, sizeof(snvbs->cycle_window));
	update_shared(offsetof(shared, cycle_timeout), &timeout, sizeof(uint32_t));
	update_shared(offsetof(shared, pk_rst_count), &snvbs->pk_rst_count, sizeof(snvbs->pk_rst_count));
	set_reset_parameters(0, 0.0, 0, RESTORE);
	update_shared(offsetof(shared, txdrzero_chan), &no_req, sizeof(simple_chandef*));
	update_shared(offsetof(shared, refzero_chan), &no_req, sizeof(simple_chandef*));
}



/********************************************************************************
  FUNCTION NAME     : get_channel_base
  FUNCTION DETAILS  : Get the base address of the selected channel in the
					  shared memory area of DSP B.

					  On entry:

					  def		Pointer to channel definition

********************************************************************************/

uint8_t* get_channel_base(chandef* def)
{
	int chanstart;
	int chan;

	if (def == NULL)
		return (NULL);

	chan = def->chanid;	/* Get channel number */

	switch (chan & CHANTYPE_MASK) {
	case CHANTYPE_INPUT:
		chanstart = offsetof(struct shared, inchaninfo);
		break;
	case CHANTYPE_OUTPUT:
		chanstart = offsetof(struct shared, outchaninfo);
		break;
	case CHANTYPE_MISC:
		chanstart = offsetof(struct shared, miscchaninfo);
		break;
	}

	int offset = chanstart + (CHANNUM(chan) * sizeof(struct simple_chandef));
	uint8_t* base = (uint8_t*)dspb_shared_chandata + offset;
	return(base);
	//return((uint8_t*)((int)dspb_shared_chandata + chanstart + (CHANNUM(chan) * sizeof(struct simple_chandef))));
}



/********************************************************************************
  FUNCTION NAME     : get_shared_channel_parameter_address
  FUNCTION DETAILS  : Get the address of the selected parameter within the
					  specified shared channel memory.

					  On entry:

					  chan		Channel number to be updated
					  param		Offset of parameter

********************************************************************************/

uint8_t* get_shared_channel_parameter_address(chandef* def, int param)
{
	uint8_t* dst;

	dst = get_channel_base(def) + param;
	return(dst);
}



/********************************************************************************
  FUNCTION NAME     : update_shared_channel
  FUNCTION DETAILS  : Update the selected shared channel.

					  On entry:

					  chan		Channel number to be updated

********************************************************************************/

void update_shared_channel(chandef* def)
{
	uint8_t* dst;

	dst = get_channel_base(def);

	hpi_blockwrite(dst, (uint8_t*)def, sizeof(simple_chandef));
}



/********************************************************************************
  FUNCTION NAME     : update_shared_channel_parameter
  FUNCTION DETAILS  : Update the shared channel entry for a selected channel
					  from the local memory.

					  On entry:

					  chan		Channel number to be updated
					  offset	Offset into shared channel structure
					  len		Length of data area to update

********************************************************************************/

void update_shared_channel_parameter(chandef* def, int offset, int len)
{
	uint8_t* dst;
	uint8_t* src;

	src = ((uint8_t*)def) + offset;

	dst = get_channel_base(def) + offset;

	hpi_blockwrite(dst, src, len);
}



/********************************************************************************
  FUNCTION NAME     : update_shared
  FUNCTION DETAILS  : Update a defined shared parameter in DSP B memory.

					  On entry:

					  dst		The offset of the start of the destination area
								from the start of the shared structure.
					  src		Pointer to the source memory area in DSP A to
								be copied.
					  len		The length of the area to be updated.

********************************************************************************/

void update_shared(int dst, void* src, int len)
{
	uint8_t* dptr;

	dptr = ((uint8_t*)dspb_shared_chandata) + dst;

	hpi_blockwrite(dptr, src, len);
}



/********************************************************************************
  FUNCTION NAME     : read_shared
  FUNCTION DETAILS  : Read a defined shared parameter from DSP B memory.

					  On entry:

					  dst		Pointer to the destination memory area in DSP A
								where the copied data will be stored.
					  src		The offset of the start of the source area in
								DSP B from the start of the shared structure.
					  len		The length of the area to be read.

********************************************************************************/

void read_shared(void* dst, int src, int len)
{
	uint8_t* sptr;

	sptr = ((uint8_t*)dspb_shared_chandata) + src;

	hpi_blockread(dst, sptr, len);
}



/********************************************************************************
  FUNCTION NAME     : modify_shared
  FUNCTION DETAILS  : Modify a defined shared parameter in DSP B memory.

					  On entry:

					  src		The offset of the start of the area to be
								modified from the start of the shared structure.
					  set		Mask to set bits as required
					  clr		Mask to clear bits as required

********************************************************************************/

void modify_shared(int src, uint32_t clr, uint32_t set)
{
	uint8_t* sptr;

	sptr = ((uint8_t*)dspb_shared_chandata) + src;
	hpi_modify4((uint32_t*)sptr, clr, set, NULL);
}



/********************************************************************************
  FUNCTION NAME     : read_shared_channel_parameter
  FUNCTION DETAILS  : Read the shared channel entry for a selected channel in
					  DSP B memory and copy into the DSP A local memory.

					  On entry:

					  chan		Channel number to be read
					  offset	Offset into shared channel structure
					  len		Length of data area to read

********************************************************************************/

void read_shared_channel_parameter(chandef* def, int offset, int len)
{
	uint8_t* dst;
	uint8_t* src;

	dst = ((uint8_t*)def) + offset;
	src = get_channel_base(def) + offset;

	hpi_blockread(dst, src, len);
}



/********************************************************************************
  FUNCTION NAME     : modify_shared_channel_parameter
  FUNCTION DETAILS  : Modify a shared channel entry for a selected channel in
					  DSP B memory.

					  On entry:

					  chan		Channel number to be read
					  offset	Offset into shared channel structure
					  set		Mask to set bits as required
					  clr		Mask to clear bits as required

********************************************************************************/

void modify_shared_channel_parameter(chandef* def, int offset, uint32_t clr, uint32_t set)
{
	uint8_t* dst;
	uint8_t* src;

	dst = ((uint8_t*)def) + offset;
	src = get_channel_base(def) + offset;

	hpi_modify4((uint32_t*)src, clr, set, (uint32_t*)dst);
}

#endif



#ifdef _DSPB

/********************************************************************************
  FUNCTION NAME     : get_channel_base
  FUNCTION DETAILS  : Get the base address of the selected channel in the
					  shared memory area of DSP B.

					  On entry:

					  chan		Channel number

********************************************************************************/

chandef* get_channel_base_dspb(int chan)
{
	int chanstart;

	switch (chan & CHANTYPE_MASK) {
	case CHANTYPE_INPUT:
		chanstart = offsetof(struct shared, inchaninfo);
		break;
	case CHANTYPE_OUTPUT:
		chanstart = offsetof(struct shared, outchaninfo);
		break;
	case CHANTYPE_MISC:
		chanstart = offsetof(struct shared, miscchaninfo);
		break;
	}

	return((chandef*)(shared_chandata + chanstart + (CHANNUM(chan) * sizeof(struct simple_chandef))));
}

#endif
