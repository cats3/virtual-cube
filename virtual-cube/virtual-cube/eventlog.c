/********************************************************************************
 * MODULE NAME       : eventlog.c												*
 * PROJECT			 : Control Cube / Signal Cube								*
 * MODULE DETAILS    : Event log routines										*
 ********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>

#include <stdint.h>
#include "porting.h"

#include "defines.h"
#include "hardware.h"
#include "shared.h"
#include "channel.h"
#include "cnet.h"
#include "eventlog.h"



const uint32_t zero = 0;

/********************************************************************************
  FUNCTION NAME     : eventlog_init
  FUNCTION DETAILS  : Initialise the event log.
********************************************************************************/

void eventlog_init(void)
{
	uint32_t used;
	uint32_t corrupt = FALSE;

	/* Ensure current event log parameters are consistent */

	if ((eventlog_ptr->inptr >= (EVENTLOG_SIZE * sizeof(eventlog_entry))) ||
		(eventlog_ptr->outptr >= (EVENTLOG_SIZE * sizeof(eventlog_entry)))) {

		/* Illegal pointer values, so assume log is corrupt */

		corrupt = TRUE;
	}
	else {

		/* Check if event log free space is consistent with the pointer values */

		used = EVENTLOG_SIZE - 1 - eventlog_ptr->free;
		if ((eventlog_ptr->outptr + (used * sizeof(eventlog_entry))) % (EVENTLOG_SIZE * sizeof(eventlog_entry)) != eventlog_ptr->inptr) corrupt = TRUE;
	}

#ifdef SIGNALCUBE

	corrupt = TRUE;	/* Signal Cube has volatile event log */

#endif

/* If log corrupt, then reset all pointers */

	if (corrupt) {
		eventlog_ptr->free = EVENTLOG_SIZE - 1;
		eventlog_ptr->inptr = 0;
		eventlog_ptr->outptr = 0;
	}
}



/********************************************************************************
  FUNCTION NAME     : eventlog_status
  FUNCTION DETAILS  : Read the event log status.
********************************************************************************/

void eventlog_status(uint32_t* entries)
{
	uint32_t n;

	n = eventlog_ptr->inptr - eventlog_ptr->outptr;
	n = n % (EVENTLOG_SIZE * sizeof(eventlog_entry));

	if (entries) *entries = (n / (sizeof(eventlog_entry)));
}



/********************************************************************************
  FUNCTION NAME     : eventlog_write
  FUNCTION DETAILS  : Write an entry into the event log.

					  Returns LOG_FULL error if event log is full.

********************************************************************************/

uint32_t eventlog_write(eventlog_entry* entry)
{
	uint32_t istate;
	uint8_t* ptr;

	// TODO: HWI_disable?
	//istate = HWI_disable();
	if (eventlog_ptr->free) {

		/* Space available in event log, so copy into log memory */

		ptr = (uint8_t*)&eventlog_ptr->log[0] + eventlog_ptr->inptr;
		memcpy(ptr, entry, sizeof(eventlog_entry));

		/* Adjust eventlog input pointer and wraparound if necessary */

		eventlog_ptr->inptr += sizeof(eventlog_entry);
		if (eventlog_ptr->inptr >= (EVENTLOG_SIZE * sizeof(eventlog_entry))) {
			eventlog_ptr->inptr = 0;
		}
		eventlog_ptr->free--;
		//HWI_restore(istate);
		return(NO_ERROR);
	}
	//HWI_restore(istate);
	return(LOG_FULL);
}



/********************************************************************************
  FUNCTION NAME     : eventlog_read
  FUNCTION DETAILS  : Read an entry from the event log.

					  Returns zero if no entries present.

********************************************************************************/

uint32_t eventlog_read(eventlog_entry* entry)
{
	uint32_t istate;
	uint8_t* ptr;

	if (eventlog_ptr->inptr != eventlog_ptr->outptr) {
		ptr = (uint8_t*)&eventlog_ptr->log + eventlog_ptr->outptr;
		memcpy(entry, ptr, sizeof(eventlog_entry));
		// TODO: HWI_disable?
		//istate = HWI_disable();

		/* Adjust eventlog input pointer and wraparound if necessary */

		eventlog_ptr->outptr += sizeof(eventlog_entry);
		if (eventlog_ptr->outptr >= (EVENTLOG_SIZE * sizeof(eventlog_entry))) {
			eventlog_ptr->outptr = 0;
		}
		eventlog_ptr->free++;
		//HWI_restore(istate);
		return(TRUE);
	}
	return(0);
}



/********************************************************************************
  FUNCTION NAME     : eventlog_read_block
  FUNCTION DETAILS  : Read all entries from the event log into a memory block.

					  Returns number of entries copied.

********************************************************************************/

uint32_t eventlog_read_block(eventlog_entry* block)
{
	uint32_t istate;
	uint8_t* ptr;
	uint8_t* dst = (uint8_t*)block;
	uint32_t entries = 0;

	if (block == NULL) return(0);

	while (eventlog_ptr->inptr != eventlog_ptr->outptr) {
		ptr = (uint8_t*)&eventlog_ptr->log + eventlog_ptr->outptr;
		memcpy(dst, ptr, sizeof(eventlog_entry));
		dst += sizeof(eventlog_entry);
		entries++;

		/* Disable interrupts whilst updating eventlog pointers */

		// TODO: HWI_disable?
		//istate = HWI_disable();

		/* Adjust eventlog input pointer and wraparound if necessary */

		eventlog_ptr->outptr += sizeof(eventlog_entry);
		if (eventlog_ptr->outptr >= (EVENTLOG_SIZE * sizeof(eventlog_entry))) {
			eventlog_ptr->outptr = 0;
		}
		eventlog_ptr->free++;
		//HWI_restore(istate);
	}
	return(entries);
}



/********************************************************************************
  FUNCTION NAME     : eventlog_flush
  FUNCTION DETAILS  : Flush the event log.
********************************************************************************/

void eventlog_flush(void)
{
	uint32_t istate;

	/* Set eventlog output pointer to equal the input pointer and reset the free size.
	   Interrupts are disabled whilst manipulating the pointers.
	*/

	// TODO: HWI_disable?
	//istate = HWI_disable();
	eventlog_ptr->outptr = eventlog_ptr->inptr;
	eventlog_ptr->free = EVENTLOG_SIZE - 1;
	//HWI_restore(istate);
}



/********************************************************************************
  FUNCTION NAME     : eventlog_log
  FUNCTION DETAILS  : Write an event with two parameters to the event log.

					  Returns LOG_FULL error if event log is full.
********************************************************************************/

uint32_t eventlog_log(uint32_t type, uint32_t p1, uint32_t p2)
{
	eventlog_entry log;

	log.type = type;
	log.parameter1 = p1;
	log.parameter2 = p2;
	log.timestamp_hi = cnet_time_hi;
	log.timestamp_lo = cnet_time_lo;
	return(eventlog_write(&log));
}
