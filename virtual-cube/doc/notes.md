# Changing firmware to 1.4.31 - 19 May 2022

## Overview
Using winmerge between 1.4.30 and 1.4.31 firmware as supplied by Simon Newey.

## Files requiring changes
- HardwareIndependent/generator.h

# Changing firmware to 1.4.30 - 31 January 2022

## Overview
Using winmerge between 1.4.28 and 1.4.30 firmware as supplied by Simon Newey.



# Changing firmware to 1.4.28 - 14 September 2021

## Overview
Using winmerge between 1.4.22 and 1.4.28 firmware as supplied by Simon Newey.

## Files skipped
Skipping these files as they aren't in the virtual Cube build
- AICube.cpp
- hw_canbus.c
